#include "QmVcScrollbar.h"

QMvcScrollBar::QMvcScrollBar(QWidget *parent) : QScrollBar(parent)
{
  QString style;

  style = "QScrollBar::handle { ";
  style += "background: #AAAAAA; ";
  style += "border: 1px solid #000000; ";
  style += "width: 90px; ";
  style += "} ";

  style += "QScrollBar:vertical { ";
  style += "border: 1px solid black; ";
  style += "margin: 70px 0px 70px 0; ";
  style += "width: 90px; ";
  style += "} ";

  style += "QScrollBar::up-arrow:vertical,QScrollBar::down-arrow:vertical { ";
  style += "width: 90px; ";
  style += "height: 70px; ";
  style += "} ";

  style += "QScrollBar::sub-line:vertical { ";
  style += "width: 90px; ";
  style += "height: 70px; ";
  style += "subcontrol-position: top; ";
  style += "subcontrol-origin: margin; ";
  style += "} ";

  style += "QScrollBar::add-line:vertical { ";
  style += "width: 90px; ";
  style += "height: 70px; ";
  style += "subcontrol-position: bottom; ";
  style += "subcontrol-origin: margin; ";
  style += "}";

  setStyleSheet(style);
}


void QMvcScrollBar::mousePressEvent(QMouseEvent *e)
{
  QScrollBar::mousePressEvent(e);

  if (this->minimum() != this->maximum()) {
    emit this->MouseButtonPressed();
  }
}

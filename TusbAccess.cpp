/*===========================================================================*
 * File        : Tusbaccess.cpp                                                                                                                                    *
 * Project     : MVC0012 - MC-TC                                                                                                                          *
 * Description : USB memory stick access routines.                                                                                    *
 *===========================================================================*/

#include "TusbAccess.h"
#include <QTextStream>
#include <QProcess>
#include <QFile>
#include <QDir>
#include "ImageDef.h"
#include "TfileHandler.h"
#include "GlobalItems.h"
#include <QString>
#include <QList>

/*----- USB filesystem path defines -----*/
#define USB_DEFAULT_MOUNT_PATH "/media/sda1"
#define USB_DEFAULT_DEVICE_PATH "/dev/sda"
#define USB_DEFAULT_DETECT_INTERVAL 3000
#define USB_REMOVE_SYNC_DELAY 1000

TUsbAccess::TUsbAccess(QObject *parent) : QObject(parent)
{
  _gbi = 0;
  usbPresent = false;
  isRemoving = false;
  removeState = 0;

  // Default path
  mountPath = USB_DEFAULT_MOUNT_PATH;
  devicePath = USB_DEFAULT_DEVICE_PATH;
  subFolder = USB_BACKUP_FOLDER;

  /*----- USB poll timer is disabled ! @@RH -----*/
  #ifdef UIT
  connect(&usbTim,SIGNAL(timeout()),this,SLOT(timUpdate()));
  usbTim.setInterval(USB_DEFAULT_DETECT_INTERVAL);
  usbTim.setSingleShot(false);
  usbTim.start();
  removeTim.setSingleShot(true);
  removeTim.setInterval(USB_REMOVE_SYNC_DELAY);
  removeTim.stop();
  connect(&removeTim,SIGNAL(timeout()),this,SLOT(timRemove()));
  #endif
}


void TUsbAccess::SetGBI(QObject *gbi)
{
  _gbi = gbi;
}


bool TUsbAccess::DirExists(const QString &path)
{
  // Check for a certain dir on the USB stick
  QString dir = mountPath + path;
  QDir *checkDir = new QDir(dir);
  bool res = checkDir->exists();
  delete checkDir;
  return res;
}


int TUsbAccess::DirCreate(const QString &path)
{

  int retVal;
  QString dir = mountPath + path;
  if (DirExists(path)) {
    retVal = 1;
  }
  else {
    QDir *mkDir = new QDir(dir);
    if (mkDir->mkpath(mkDir->path())) {
      //if (DirExists(path))
      SyncUSB();                                                                // if directory was created, sync the filesystem of the USB stick
      delete mkDir;                                                             // Free the QDir mkDir object
      retVal = 0;
    }
    else {
      delete mkDir;                                                             // Free the QDir mkDir object
      retVal = -1;
    }
  }
  return(retVal);
}


void TUsbAccess::SetUsbMountPath(const QString &path)
{
  mountPath = path;
}


void TUsbAccess::SetUsbFolder(const QString &path)
{
  if (path != subFolder) {
    subFolder = path;
    DirCreate(subFolder);
  }
}


void TUsbAccess::SyncUSB()
{
  /*
  Sync will synchronize the USB file system.
  If sync is triggerd during removal, the filesystem of the usb stick is also unmounted.
  */
  QProcess proc;
  proc.start("sync");
  proc.waitForFinished(-1);

  if (isRemoving) {
    QString path = "umount -l "+mountPath;
    proc.start(path);
    proc.waitForFinished(-1);
    if (USB_REMOVE_READY_DELAY > 0) {
      removeState = 1;
      removeTim.setInterval(USB_REMOVE_READY_DELAY);
      removeTim.start();
    }
    else {
      emit ReadyForRemove();
    }
  }
}


bool TUsbAccess::ScanFileForFirmware(const QString &fileName, QString * version)
{
  TFileHandler fh;
  if (_gbi != 0) {
    fh.SetGBI(_gbi);
    bool res = fh.CheckUpdateArchive(fileName,version);
    return res;
  }
  return false;
}


void TUsbAccess::ScanForFirmware()
{
  QDir path;

  //@@ hier komt ie
  // Trigger some timer event
  if (UsbPresent()) {
    // Scan directories for firmware
    path.setPath(mountPath + subFolder + USB_DEFAULT_FIRMWARE_PATH);
    if (path.exists()) {
      QString filter = "*.mvcu";
      //path.setFilter(QDir::Files | QDir::NoSymLinks);
      //            path.setSorting(QDir::Size | QDir::Reversed);
      emit UsbFirmwareStartScan();                                              //leegt tabel

      QFileInfoList list = path.entryInfoList(QStringList(filter),QDir::Files | QDir::NoSymLinks);

      if (list.size() > 0) {
        QString file;
        QString fullFilePath;
        QString version;
        //@@@ hier komt ie niet
        for (int i=0; i<list.size(); i++) {
          file = list.at(i).fileName();
          fullFilePath = mountPath + subFolder + USB_DEFAULT_FIRMWARE_PATH +((globalItems*)_gbi)->fileSeparator+ file;
          if (ScanFileForFirmware(fullFilePath,&version)) {
            //found = true;
            emit UsbFirmwareFound(file,version);
          }
        }

      }
      list.clear();
    }
  }
  path.setPath("/");

}


int TUsbAccess::StartFirmwareUpdate(const QString &file)
{
  TFileHandler fh;
  if (_gbi != 0) {
    /*---- Update archive -----*/
    fh.SetGBI(_gbi);
    if (fh.StartUpdaterApp(file)) {
      return 0;
    }
    else {
      return 1;
    }
  }
  else {
    return -1;
  }
}


bool TUsbAccess::UsbPresent()
{
  bool devExists, pathExists;

  devExists = QFile::exists(devicePath);

  pathExists = QFile::exists(mountPath);
  if (pathExists) {
  }
  if (devExists && pathExists) {
    if (!(usbPresent)) {
      isRemoving = false;
      usbPresent = true;
      emit UsbDetected();
      removeState = 0;
    }
    return true;
  }
  else {
    if (usbPresent) {
      usbPresent = false;
      emit UsbRemoved();
      removeState = 0;
    }
    else {
      if ((!devExists) && pathExists) {
        // Invalid path is available. Abort logging and remove invalid path
        usbPresent = false;
        removeState = 2;                                                        // Delay and erase invalid directory
        removeTim.start();
      }
    }
    return false;
  }
}


void TUsbAccess::timUpdate()
{
  UsbPresent();
}


void TUsbAccess::timRemove()
{
  QProcess proc;
  QString cmd;
  switch (removeState) {
    case 0:
      SyncUSB();
      break;
    case 1:
      removeTim.stop();
      emit ReadyForRemove();
      break;

    case 2:
      removeTim.stop();
      cmd = "rm -r "+mountPath;
      proc.start(cmd);
      proc.waitForFinished(-1);
      removeState = 3;
      removeTim.start();
      break;

    case 3:
      removeTim.stop();
      if (QFile::exists(mountPath)) {
        /*---- USB directory is still present ----*/
      }
      else {
        /*----- USB directory successfully removed ----*/
      }
      break;
  }
}


void TUsbAccess::PrepareForRemove()
{
  // Signal other processes to stop file operations.
  emit RemovingUSB();
  isRemoving = true;
  removeState = 0;
  // Delay to give other actions the chance to complete/abort their filehandling.
  removeTim.setInterval(USB_REMOVE_SYNC_DELAY);
  removeTim.setSingleShot(true);
  removeTim.start();
}


QString TUsbAccess::basePath()
{
  return mountPath;
}


QString TUsbAccess::SubFolder()
{
  return subFolder;
}


QString TUsbAccess::FullUsbPath()
{
  return (mountPath + subFolder);
}

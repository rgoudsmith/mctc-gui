/*================================*/
/* File        : Rout.cpp         */
/* Description : Various routines */
/*================================*/

#define ROUT

#include "Rout.h"
#include "Common.h"
#include <QHostAddress>
#include <QNetworkInterface>
#include <QStringList>
#include "math.h"
#include <iostream>
#include "Glob.h"
#include "Form_MainMenu.h"
#include <QCoreApplication>

/*----- Data log mirror structure -----*/
typedef struct
{
  float dosSet;
  float dosAct;
  float actRpm;
  float weight;
  byte vidProdSts;
  float tachoAct;
  float metTimeCalc;
  byte mc24On;
  byte output[4];
  byte alarmNr;
} tLogMirror;

/*----- Log data types -----*/
#define BYTE_TYPE             0
#define WORD_TYPE             1
#define INT_TYPE              2
#define FLOAT_TYPE            3

/*----- Control task log event codes -----*/
#define LOG_EVENT_RPM         0                                                 /* RPM */
#define LOG_EVENT_ON_OFF      1                                                 /* On/Off */
#define LOG_EVENT_COLCAP_SET  2                                                 /* Set colcap gr/s */
#define LOG_EVENT_COLCAP_ACT  3                                                 /* Act colcap gr/s */
#define LOG_EVENT_WEIGHT      4                                                 /* Weight */
#define LOG_EVENT_METER_TIME  5                                                 /* Metering time */
#define LOG_EVENT_TACHO       6                                                 /* Tacho */
#define LOG_EVENT_STATUS      7                                                 /* Status */
#define LOG_EVENT_FILL_VALVE  8                                                 /* Fill valve output */
#define LOG_EVENT_ALARM       9                                                 /* Alarm number */

#define MAX_LOG_LINES         5000                                              /* Maximum number of log lines */

/*------------------*/
/* Public variables */
/*------------------*/
int Rout::actLcdBrightNess;                                                     /* Actual LCD brightness value */
int Rout::cpuRun;                                                               /* system run counter */
QString Rout::ipAddress;                                                        /* eth0 IP address in ascii format */
QString Rout::macAddress;                                                       /* eth0 MAC address in ascii format */
byte Rout::ip[4];                                                               /* eth0 IP address in word format */
word Rout::mac[3];                                                              /* eth0 MAC address in word format */
tAlmLog Rout::almLog;                                                           /* Alarm log buffer */
tProdData Rout::prodData;                                                       /* Production data buffer */
bool Rout::keyLock;                                                             /* Keyboard lock */
QString Rout::usbPath;                                                          /* USB device path */
tRstConst Rout::rstCons[CFG_MAX_UNIT_COUNT];                                    /* Reset consumption flags */
QString Rout::appPath;                                                          /* Global application path */
bool Rout::homeScreenActive;                                                    /* Home screen active flag */
bool Rout::mcWeightEditModeActive;                                              /* McWeight level edit mode active */
byte Rout::homeScreenMultiUnitActive;                                           /* Home screen multiunit active */
bool Rout::almDispMode;

/*-----------------*/
/* Local variables */
/*-----------------*/
tLogMirror logMirror[CFG_MAX_UNIT_COUNT];                                       /* Mirror registers for control task data log */
QMessageBox *msgBox;                                                            /* Popup message box */
int maxUnits;                                                                   /* Number of connected units */
bool autoStartOneShot;                                                          /* Auto start one-shot */

word Rout::SwapWord(word value)
{
  /*-------------*/
  /* Swap a word */
  /*-------------*/
  return ((value & 0x00FF) * 0x100) + ((value & 0xFF00) >> 8);
}


bool Rout::ImperialMode()
{
  /*-----------------------------*/
  /* Return imperial mode active */
  /*-----------------------------*/
  return(glob->mstCfg.imperialMode == TRUE);
}


void Rout::SetUnits()
{
  /*--------------------------------*/
  /* Setup metric or imperial units */
  /*--------------------------------*/

  if (!Rout::ImperialMode()) {
    glob->shotUnits = UNITS_MET_G_S;
    glob->extrUnits = UNITS_MET_KG_H;
    glob->weightUnits = UNITS_MET_G;
  }
  else {
    glob->shotUnits = UNITS_IMP_G_S;
    glob->extrUnits = UNITS_IMP_KG_H;
    glob->weightUnits = UNITS_IMP_G;
  }

  glob->timerUnits = UNITS_SEC;
  glob->tachoUnits = UNITS_VOLT;
  glob->rpmUnits = "rpm";                                                       // Translated via LanguageUpdate()
}


void Rout::ConfigureNetwork()
{
  /*---------------------------------------------------------------------------*/
  /* Configure Network                                                         */
  /* Example:                                                                  */
  /* ifconfig eth0 192.168.16.210                                              */
  /* ifconfig eth0 192.168.1.102 netmask 255.255.255.0 broadcast 192.168.1.255 */
  /* route add default gw 192.168.16.154                                       */
  /*---------------------------------------------------------------------------*/
  QProcess proc;
  QString cmdStr;

  /*--- Check IP address ---*/
  if ((glob->mstCfg.IPAddress[3] ==0) && (glob->mstCfg.IPAddress[2]==0) && (glob->mstCfg.IPAddress[1]==0) && (glob->mstCfg.IPAddress[0]==0)) {
    qDebug() << "Using network configuration as defined in '/etc/network/interfaces'";
  }
  /*--- Static IP address ---*/
  else {
    /*--- Set ip address ---*/
    cmdStr = "ifconfig eth0 ";
    cmdStr += QString::number(glob->mstCfg.IPAddress[3])+ "." + QString::number(glob->mstCfg.IPAddress[2])+ "." + QString::number(glob->mstCfg.IPAddress[1])+ "." + QString::number(glob->mstCfg.IPAddress[0]);

    /*--- Netmask ---*/
    cmdStr += " netmask ";
    cmdStr += QString::number(glob->mstCfg.Netmask[3])+ "." + QString::number(glob->mstCfg.Netmask[2])+ "." + QString::number(glob->mstCfg.Netmask[1])+ "." + QString::number(glob->mstCfg.Netmask[0]);
    qDebug() << "Command: " << cmdStr;
    proc.start(cmdStr);                                                         /* Execute command */
    proc.waitForFinished();                                                     /* Wait for command finished */

    /*--- Set gateway ---*/
    cmdStr = "route add default gw ";
    cmdStr += QString::number(glob->mstCfg.Gateway[3])+ "." + QString::number(glob->mstCfg.Gateway[2])+ "." + QString::number(glob->mstCfg.Gateway[1])+ "." + QString::number(glob->mstCfg.Gateway[0]);
    qDebug() << "Command: " << cmdStr;
    proc.start(cmdStr);                                                         /* Execute command */
    proc.waitForFinished();                                                     /* Wait for command finished */
  }
}


void Rout::LcdBrightNess(int value)
{
  /*--------------------------------------------------------*/
  /* Setup the LCD brightness between 0 (Min) and 100 (Max) */
  /*--------------------------------------------------------*/
  QFile fp;
  QString fileData;

  /*----- Detect brightness change -----*/
  if (value != actLcdBrightNess) {
    actLcdBrightNess = value;

    fp.setFileName("/sys/class/backlight/pwm-backlight.0/brightness");

    /*----- Range check ----*/
    if (value < 0) {
      value =0;
    }
    else if (value > 100) {
      value = 100;
    }

    fileData = QString::number(100-value);                                      /* 0 = Max, 100 = Min */
    if (fp.open(QFile::ReadWrite)) {
      if (fp.write(fileData.toAscii().data()) >= 0) {
      }
      fp.close();
    }
  }
}


byte mc30AlmTab[MAX_USER_ALARM] =
{
  /*-------------------------------------------*/
  /* MC-TC to MC30 alarm code conversion table */
  /*-------------------------------------------*/
  0xFF,                                                                         /* 0: NO_ALARM */
  2,                                                                            /* 1: ALM_INDEX_FILL_SYSTEM 1 */
  0xFF,                                                                         /* 2 : ALM_INDEX_LO_LEVEL (Not supported in MC-30  */
  0xFF,                                                                         /* 3 : ALM_INDEX_LO_LO_LEVEL (Not supported in MC-30  */
  6,                                                                            /* 4 : ALM_INDEX_HI_HI_LEVEL */
  7,                                                                            /* 5 : ALM_INDEX_MIN_MOTOR_SPEED */
  3,                                                                            /* 6 : ALM_INDEX_MAX_MOTOR_SPEED */
  4,                                                                            /* 7 : ALM_INDEX_DEV_QR */
  0xFF,                                                                         /* 8 : ALM_INDEX_DEV (Not supported in MC-30  */
  0xFF                                                                          /* 9 : ALM_INDEX_MASTER_SLAVE_COM (Not supported in MC-30  */
};

byte Rout::GetMc30AlarmCode (byte alarmNr)
{
  /*-------------------------------------------------*/
  /* Convert a MC-TC alarm code to a MC30 alarm code */
  /*-------------------------------------------------*/
  if (alarmNr >= MAX_USER_ALARM) {                                              /* Range check */
    return(0xFF);
  }
  else {
    return (mc30AlmTab[alarmNr]);                                               /* Return MC-30 alarm code */
  }
}


void Rout::ReadEth0Data(void)
{
  /*------------------------------------*/
  /* Read ETH0 MacAdress and IP address */
  /*------------------------------------*/
  QStringList macList;
  QStringList ipList;
  bool ok;

  foreach(QNetworkInterface interface, QNetworkInterface::allInterfaces()) {
    if (interface.flags().testFlag(QNetworkInterface::IsRunning)) {

      foreach (QNetworkAddressEntry entry, interface.addressEntries()) {
        if ( interface.hardwareAddress() != "00:00:00:00:00:00" && entry.ip().toString().contains(".")) {

          if (interface.name() == "eth0") {
            ipAddress = entry.ip().toString();                                  /* IP adress [ASCII] */
            macAddress = interface.hardwareAddress();                           /* MAC address [ ASCII] */

            /*----- Split string -> aa:bb:cc:dd:ee:ff into seprate byte strings -> aa bb cc dd ee ff -----*/
            macList=macAddress.split(":");

            /*----- Convert byte strings into array with 3 MAC words -----*/
            mac[0] = (macList[0].toInt(&ok,16)*0x100) + macList[1].toInt(&ok,16);
            mac[1] = (macList[2].toInt(&ok,16)*0x100) + macList[3].toInt(&ok,16);
            mac[2] = (macList[4].toInt(&ok,16)*0x100) + macList[5].toInt(&ok,16);

            /*----- Split string -> 192.168.1.1 into seprate byte strings -> aa bb cc dd -----*/
            ipList=ipAddress.split(".");

            /*----- Convert byte strings into array with 4 IP words -----*/
            ip[0] = ipList[0].toInt(&ok,10);
            ip[1] = ipList[1].toInt(&ok,10);
            ip[2] = ipList[2].toInt(&ok,10);
            ip[3] = ipList[3].toInt(&ok,10);

            qDebug() << "Ip address " << ipAddress << "  Mac Address " << macAddress;
          }
        }
      }
    }
  }
}


void Rout::WriteAlarmLogFile(void)
{
  /*--------------------------*/
  /* Write the alarm log file */
  /*--------------------------*/
  QFile logFile("/opt/MovaColor/log/alm.log");

  if (logFile.open(QIODevice::ReadWrite)) {
    logFile.reset();
    logFile.write((char*)&almLog,sizeof(tAlmLog));
    logFile.close();
    Rout::SyncFile();
  }
}


void Rout::ReadAlarmLogFile(void)
{
  /*--------------------------*/
  /* Read the alarm log file */
  /*--------------------------*/
  QFile logFile("/opt/MovaColor/log/alm.log");

  if (logFile.open(QIODevice::ReadWrite)) {
    logFile.reset();
    logFile.read((char*)&almLog,sizeof(tAlmLog));
    logFile.close();
  }
}


void Rout::SyncFile(void)
{
  /*-------------------*/
  /* Sync file command */
  /*-------------------*/
  /* Make all changes done to all files actually appear on disk.  */
  sync();
}


void WriteLogLine(byte unitNr, int eventCode,float newVal, float oldVal)
{
  /*-----------------------------------------------------------------*/
  /* Append log line to the log file.                                */
  /* The first line in the log file contains the number of log lines */
  /*-----------------------------------------------------------------*/
  QFile logFile("/opt/MovaColor/log/mctc-gui.log");
  QString dataLogLine;
  QByteArray logLines;
  int logLineLen;
  int logLineNr;

  logFile.setFileName("/opt/MovaColor/log/mctc-gui.log");

  /*----- Fetch system date/time information -----*/
  /* Date */
  dataLogLine = QDateTime::currentDateTime().toString("yyyy-MM-dd").rightJustified(10)+";";
  /* Time */
  dataLogLine += QDateTime::currentDateTime().toString("hh:mm:ss").rightJustified(9)+";";

  /*----- Unit number -----*/
  dataLogLine += QString::number(unitNr).rightJustified(3)+";";

  /*----- Event code -----*/
  dataLogLine += QString::number(eventCode).rightJustified(3)+";";

  /*----- New value -----*/
  dataLogLine += QString::number(newVal,'f',5).rightJustified(12)+";";

  /*----- Old value -----*/
  dataLogLine += QString::number(oldVal,'f',5).rightJustified(12)+";";

  /*----- Add line terminator -----*/
  dataLogLine += "\n";

  /*----- Fetch log line length -----*/
  logLineLen = dataLogLine.length();

  /*----- Replace the floating point decimal point in a comma -----*/
  dataLogLine = dataLogLine.replace(".",",");

  /*----- Read the number of log entries from the fist line ----*/
  if (logFile.open(QIODevice::ReadWrite)) {

    /*----- Check for empty file -----*/
    if (logFile.size() == 0) {
      logLineNr = 0;                                                            /* Reset logline */
    }
    else {
      /*----- Read number of log lines from the first line -----*/
      logLines = logFile.readLine(5);
      logLineNr = logLines.toInt();
    }
    /*----- Update the number of log lines and write it back to the first line -----*/
    logLineNr++;                                                                /* Update */

    /*----- Range check on maximum number of log line entries -----*/
    if (logLineNr > MAX_LOG_LINES) {
      logLineNr = 1;
    }

    logFile.reset();                                                            /* Reset file pointer to zero */
    logFile.write(QString::number(logLineNr).rightJustified(4).toAscii());
    logFile.write("\n");

    /*----- Close file -----*/
    logFile.close();
  }

  /*----- Append the new log line data to the file -----*/
  if (logFile.open(QIODevice::ReadWrite)) {                                     /* Skip first line which containt the actual line number */

    logFile.seek(5+((logLineNr-1)*logLineLen));

    logFile.write(dataLogLine.toAscii().data());

    /*----- Close file,  the event log file is not 'sync' ed !! -----*/
    logFile.close();
  }
}


void CheckDev (byte dataType, int eventCode, byte unitNr, void *act, void *mirror, float pct, float tresHold)
{
  /*-------------------------------------------------------------*/
  /* Check the deviation between the actual and the mirror value */
  /*-------------------------------------------------------------*/
  float devPct,actVal,mirrorVal;

  /*----- Fetch actual and mirror data -----*/
  switch(dataType) {

  case BYTE_TYPE :
    actVal = *(byte*)act;
    mirrorVal = *(byte*)mirror;
    break;

  case WORD_TYPE :
    actVal = *(word*)act;
    mirrorVal = *(word*)mirror;
    break;

  case INT_TYPE :
    actVal = *(int*)act;
    mirrorVal = *(int*)mirror;
    break;

  case FLOAT_TYPE :
    actVal = *(float*)act;
    mirrorVal = *(float*)mirror;
    break;
  }

  /*----- Calculate deviation percentage -----*/
  devPct = (fabs(actVal-mirrorVal) / mirrorVal) * 100;

  /*----- Compare act deviation with max allowed deviation -----*/
  /*----- Dev pct 0 means log on change -----*/

  if ((devPct > pct) || ((pct == 0) && (actVal != mirrorVal))) {

    /*----- Deviation detected, write data log line -----*/

    /* If treshold == 0 or actVal above treshold */
    if ((tresHold == 0) || (actVal >= tresHold)) {
      WriteLogLine(unitNr,eventCode,actVal,mirrorVal);
    }

    /*----- Set new mirror value -----*/
    switch(dataType) {

    case BYTE_TYPE :
      *(byte*)mirror = *(byte*)act;
      break;

    case WORD_TYPE :
      *(word*)mirror = *(word*)act;
      break;

    case INT_TYPE :
      *(int*)mirror = *(int*)act;
      break;

    case FLOAT_TYPE :
      *(float*)mirror = *(float*)act;
      break;
    }

  }
}


void Rout::LogGuiMsg(tMsgGui *msg)
{
  /*-------------------------------------------------------*/
  /* Log handler for message send from GUI to Control task */
  /*-------------------------------------------------------*/

  /*----- Fetch number of connected units form the message -----*/
  maxUnits = msg->common.slavesConnected;
}


void Rout::LogControlMsg(tMsgControl *msg)
{
  /*-------------------------------------------------------*/
  /* Log handler for message send from Control to GUI task */
  /*-------------------------------------------------------*/
  int i;

  /*----- Check data change -----*/
  for (i=0; i<maxUnits; i++) {

    /*----- On/ off -----*/
    CheckDev(BYTE_TYPE,LOG_EVENT_ON_OFF,i,&msg->data[i].mc24On,&logMirror[i].mc24On,0,0);

    /*----- RPM -----*/
    CheckDev(FLOAT_TYPE,LOG_EVENT_RPM,i,&msg->data[i].actRpm,&logMirror[i].actRpm,1,0);

    /*----- Set colcap gr/s -----*/
    CheckDev(FLOAT_TYPE,LOG_EVENT_COLCAP_SET,i,&msg->data[i].dosSet,&logMirror[i].dosSet,5,0);

    /*----- Act colcap gr/s -----*/
    CheckDev(FLOAT_TYPE,LOG_EVENT_COLCAP_ACT,i,&msg->data[i].dosAct,&logMirror[i].dosAct,10,0);

    /*----- Weight -----*/
    CheckDev(FLOAT_TYPE,LOG_EVENT_WEIGHT,i,&msg->data[i].weight,&logMirror[i].weight,10,0);

    /*----- Status -----*/
    CheckDev(BYTE_TYPE,LOG_EVENT_STATUS,i,&msg->data[i].vidProdSts,&logMirror[i].vidProdSts,0,0);

    /*----- Measuring time act -----*/
    CheckDev(FLOAT_TYPE,LOG_EVENT_METER_TIME,i,&msg->data[i].metTimeCalc,&logMirror[i].metTimeCalc,2,0);

    /*----- Tacho volt -----*/
    CheckDev(FLOAT_TYPE,LOG_EVENT_TACHO,i,&msg->data[i].tachoAct,&logMirror[i].tachoAct,25,0.25);

    /*----- Fill valve output -----*/
    CheckDev(BYTE_TYPE,LOG_EVENT_FILL_VALVE,i,&msg->data[i].ioData.output[1],&logMirror[i].output[1],0,0);

    /*----- Alarm code -----*/
    CheckDev(BYTE_TYPE,LOG_EVENT_ALARM,i,&msg->data[i].alarmNr,&logMirror[i].alarmNr,0,0);

  }

}


void Rout::WriteProdData(void)
{
  /*--------------------------------*/
  /* Write the production data file */
  /*--------------------------------*/
  QFile prodFile("/opt/MovaColor/settings/proddata.cfg");

  if (prodFile.open(QIODevice::ReadWrite)) {
    prodFile.reset();
    prodFile.write((char*)&prodData,sizeof(tProdData));
    prodFile.close();
    Rout::SyncFile();
  }
}


void Rout::ReadProdData(void)
{
  /*---------------------------------*/
  /* Read the production status file */
  /*---------------------------------*/
  QFile prodFile("/opt/MovaColor/settings/proddata.cfg");

  if (prodFile.open(QIODevice::ReadWrite)) {
    prodFile.reset();
    prodFile.read((char*)&prodData,sizeof(tProdData));
    prodFile.close();
  }

  /*----- ProdData file not found -----*/
  else {
    /*----- Clear structure an write it to file -----*/
    memset(&prodData,0,sizeof(tProdData));
    WriteProdData();
  }
}


bool Rout::BacklitOn()
{
  /*-----------------------------------------------------------------*/
  /* Test the status of the backlit and switch it on when is was off */
  /*-----------------------------------------------------------------*/
  bool retVal;

  retVal = FALSE;
  if (Rout::actLcdBrightNess == 10) {
    Rout::LcdBrightNess(100);                                                   /* Backlit on */
    retVal = TRUE;
  }

  /*----- Re-trigger backlit timer -----*/
  ((MainMenu*)(glob->menu))->SetHomeTimerInterval(glob->mstCfg.MvcSettings.agent.screenTime.actVal);
  return retVal;
}


void Rout::ShowMsg(QString title, QString msgTxt)
{
  /*-----------------------------*/
  /* Show a popup message window */
  /*-----------------------------*/
  msgBox = new(QMessageBox);
  msgBox->setWindowFlags(Qt::Window | Qt::CustomizeWindowHint | Qt::WindowTitleHint);
  msgBox->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
  msgBox->setWindowTitle(title);
  msgBox->setStandardButtons(0);
  msgBox->setFont(*(glob->baseFont));
  msgBox->setText(msgTxt);
  QCoreApplication::processEvents();
  msgBox->show();
  QCoreApplication::processEvents();
}


void Rout::CloseMsg()
{
  /*----------------------------------*/
  /* Close and delete the message box */
  /*----------------------------------*/
  delete msgBox;
}


eMcDeviceType Rout::ActUnitType()
{
  /*----------------------------*/
  /* Fetch the active unit type */
  /*-----------------------------*/
  return (glob->sysCfg[glob->ActUnitIndex()].deviceType);
}


bool Rout::TwinMode()
{
  /*----------------------------------------------------*/
  /* Detect TWIN mode ( 2 units configured as Balance ) */
  /*----------------------------------------------------*/
  bool retVal;

  retVal = ((glob->mstCfg.slaveCount == 1) &&
            (glob->sysCfg[0].deviceType == mcdtBalance) &&
            (glob->sysCfg[1].deviceType == mcdtBalance));

  return(retVal);
}


void Rout::AutoStart()
{
  /*------------------*/
  /* System autostart */
  /*------------------*/
  if (glob->mstCfg.autoStart) {                                                 /* Auto start enabled ? */
    if (!autoStartOneShot) {                                                    /* Already performed ? */
      autoStartOneShot = true;                                                  /* Set first start done */
      Rout::ReadProdData();                                                     /* Read production data file */

      /*----- Start units if they where on -----*/
      if (Rout::prodData.onOff) {
        glob->MstSts_SetSysStart(true);                                         /* Start units */
      }
    }
  }
}


void Rout::ResetAlarms()
{
  /*-------------------------------------*/
  /* Reset alarm for all connected units */
  /*-------------------------------------*/
  int i;

  for (i=0; i <= glob->mstCfg.slaveCount; i++) {
    glob->sysSts[i].alarmSts.reset = true;
  }
}


float Rout::KgToLbs(float kg)
{
  /*----------------------*/
  /* Kg to Lbs conversion */
  /*---------- -----------*/
  return (kg*2.20462);
}


float Rout::LbsToKg(float lbs)
{
  /*----------------------*/
  /* Kg to Lbs conversion */
  /*----------------------*/
  return (lbs/2.20462);
}


float Rout::GrToLbs(float gr)
{
  /*----------------------*/
  /* Gr to Lbs conversion */
  /*----------------------*/
  return (gr*2.20462*3.6);
}


float Rout::LbsToGr(float lbs)
{
  /*----------------------*/
  /* Lbs to gr conversion */
  /*----------------------*/
  return (lbs/0.00220462);
}


float Rout::LbsHToGrS(float lbsH)
{
  /*------------- ------------*/
  /* Lbs/h to gr/s conversion */
  /*--------------------------*/
  return (lbsH/2.20462/3.6);
}


float Rout::GrSToLbsH(float grS)
{
  /*------------- ------------*/
  /* Gr/s to Lbs/s conversion */
  /*--------------------------*/
  return (grS*2.20462*3.6);
}


bool Rout::McWeightPresent()
{
  /*--------------------------- ---------------------*/
  /* Check if a McWeight is configured in the system */
  /*-------------------------------------------------*/
  int i;

  for (i=0;i<glob->mstCfg.slaveCount+1;i++) {
    if (glob->sysCfg[i].deviceType == mcdtWeight) {
      return true;
    }
  }
  return false;
}


bool Rout::MultiUnitHomeScreen() {
  /*---------------------------------------------------*/
  /* Detect if a multi unit home screen mode is active */
  /*---------------------------------------------------*/
  return  (!(glob->mstCfg.slaveCount == 0) || (glob->mstSts.isTwin));
}


#include "Form_EventLogItem.h"
#include "ui_Form_EventLogItem.h"
#include "Rout.h"

#define EVENT_ITEM_LARGE_HEIGHT 105
#define EVENT_ITEM_SMALL_HEIGHT 82

#define EVENT_ITEM_NOT_INITIALIZED (-8)

//background-color: rgb(239,239,239)
#define EVENT_ITEM_NORMAL_STYLE "QGroupBox{border-color: black; border: 1px solid; border-radius:3px;}"
#define EVENT_ITEM_SELECTED_STYLE "QGroupBox{border-color: black; border: 3px solid; border-radius:3px;}"

EventLogItemFrm::EventLogItemFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::EventLogItemFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);
  eventID = -1;
  item.unitIdx = EVENT_ITEM_NOT_INITIALIZED;
  setFixedHeight(EVENT_ITEM_LARGE_HEIGHT);
  this->setStyleSheet(EVENT_ITEM_NORMAL_STYLE);
}


EventLogItemFrm::~EventLogItemFrm()
{
  delete ui;
}


void EventLogItemFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;
  SetFont();
}


void EventLogItemFrm::SetFont()
{
  ui->label->setFont(*(glob->baseFont));
  ui->lbCode->setFont(*(glob->baseFont));
  ui->lbDate->setFont(*(glob->baseFont));
  ui->lbTime->setFont(*(glob->baseFont));
  ui->lbDevice->setFont(*(glob->baseFont));
  ui->lbIcon->setFont(*(glob->baseFont));
}


void EventLogItemFrm::DisplayData()
{
  /*------------------------*/
  /* Display event log item */
  /*------------------------*/
  QString txt;
  QDateTime tim;
  int msgType = -1;

  if (item.unitIdx != EVENT_ITEM_NOT_INITIALIZED) {

    /*----- Display unit name -----*/
    if (item.unitIdx >= 0) {
      ui->lbDevice->setText(glob->sysCfg[item.unitIdx].MC_ID);
    }
    else {
      ui->lbDevice->setText(tr("sys"));
    }

    /*----- Display alarm number -----*/
    ui->lbCode->setText(QString::number(item.alarmNr));

    /*----- display alarm time -----*/
    tim.setTime_t(item.dateTim);
    ui->lbDate->setText(tim.date().toString(FORMAT_DATE_LONG));
    ui->lbTime->setText(tim.time().toString(FORMAT_TIME_SHORT));

    /*----- Display message type -----*/
    msgType = item.msgType;
    switch(msgType) {
      case 0:                                                                   // warning
        ui->lbIcon->setText(tr("warning"));
        break;
      case 1:                                                                   // alarm
        ui->lbIcon->setText(tr("alarm"));
        break;
      default:
        ui->lbIcon->setText(tr("event"));
        break;
    }
    /*----- Display event text -----*/
    glob->GetEventText(item.alarmNr,item.msgType,item.newValue,item.oldValue,&txt);
    ui->label->setText(txt);
  }
}


void EventLogItemFrm::SetData(EventLogItemStruct *dataStruct)
{
  if (dataStruct != 0) {
    item.eventID = dataStruct->eventID;
    item.alarmNr = dataStruct->alarmNr;
    item.unitIdx = dataStruct->unitIdx;
    item.msgType = dataStruct->msgType;
    item.newValue = dataStruct->newValue;
    item.oldValue = dataStruct->oldValue;
    item.dateTim = dataStruct->dateTim;

    DisplayData();
  }
}

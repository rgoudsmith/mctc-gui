#include "Form_DateTimeEditor.h"
#include "ui_Form_DateTimeEditor.h"
#include "QDateTime"
#include "Form_MainMenu.h"
#include "Rout.h"

DateTimeEditorFrm::DateTimeEditorFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::DateTimeEditorFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);
  popActive = false;

  hour = 0;
  minute = 0;
  day = 0;
  month = 0;
  year = 0;

  newHour = 0;
  newMinute = 0;
  newDay = 0;
  newMonth = 0;
  newYear = 0;

  /*----- Disable windows title bar -----*/
  this->setWindowFlags(Qt::CustomizeWindowHint);

  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));

  connect(ui->btHourUp,SIGNAL(clicked()),this,SLOT(HourUpClicked()));
  connect(ui->btHourDown,SIGNAL(clicked()),this,SLOT(HourDownClicked()));

  connect(ui->btMinUp,SIGNAL(clicked()),this,SLOT(MinuteUpClicked()));
  connect(ui->btMinDown,SIGNAL(clicked()),this,SLOT(MinuteDownClicked()));

  connect(ui->btDayUp,SIGNAL(clicked()),this,SLOT(DayUpClicked()));
  connect(ui->btDayDown,SIGNAL(clicked()),this,SLOT(DayDownClicked()));
  connect(ui->btMonUp,SIGNAL(clicked()),this,SLOT(MonthUpClicked()));
  connect(ui->btMonDown,SIGNAL(clicked()),this,SLOT(MonthDownClicked()));
  connect(ui->btYearUp,SIGNAL(clicked()),this,SLOT(YearUpClicked()));
  connect(ui->btYearDown,SIGNAL(clicked()),this,SLOT(YearDownClicked()));
}


DateTimeEditorFrm::~DateTimeEditorFrm()
{
  delete ui;
}


void DateTimeEditorFrm::changeEvent(QEvent *e)
{
  QBaseWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      LanguageUpdate();
      break;
    default:
      break;
  }
}


void DateTimeEditorFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;
  LanguageUpdate();
}


void DateTimeEditorFrm::LanguageUpdate()
{
  glob->SetButtonImage(ui->btCancel,itSelection,selCancel);
  glob->SetButtonImage(ui->btOk,itSelection,selOk);

  // Date buttons
  glob->SetButtonImage(ui->btMonUp,itGeneral,genArrowKeyUp);
  glob->SetButtonImage(ui->btMonDown,itGeneral,genArrowKeyDown);

  glob->SetButtonImage(ui->btDayUp,itGeneral,genArrowKeyUp);
  glob->SetButtonImage(ui->btDayDown,itGeneral,genArrowKeyDown);

  glob->SetButtonImage(ui->btYearUp,itGeneral,genArrowKeyUp);
  glob->SetButtonImage(ui->btYearDown,itGeneral,genArrowKeyDown);

  // Time buttons
  glob->SetButtonImage(ui->btHourUp,itGeneral,genArrowKeyUp);
  glob->SetButtonImage(ui->btHourDown,itGeneral,genArrowKeyDown);

  glob->SetButtonImage(ui->btMinUp,itGeneral,genArrowKeyUp);
  glob->SetButtonImage(ui->btMinDown,itGeneral,genArrowKeyDown);

  ui->edDay->setFont(*(glob->baseFont));
  ui->edMonth->setFont(*(glob->baseFont));
  ui->edYear->setFont(*(glob->baseFont));
  ui->edHours->setFont(*(glob->baseFont));
  ui->edMinutes->setFont(*(glob->baseFont));
}


void DateTimeEditorFrm::ConvertDateTime(QString &txt, int val, bool isYear)
{
  txt = QString::number(val);
  if (isYear) {
    while (txt.length() < 4) {
      txt = "0"+txt;
    }
  }
  else {
    while (txt.length() < 2) {
      txt = "0"+txt;
    }
  }
}


void DateTimeEditorFrm::SetCurrentDateTime()
{
  QTime tim = QDateTime::currentDateTime().time();

  hour = tim.hour();
  minute = tim.minute();

  newHour = hour;
  newMinute = minute;
  ui->edHours->setText(QString::number(hour));
  ui->edMinutes->setText(QString::number(minute));

  QDate dat = QDateTime::currentDateTime().date();

  day = dat.day();
  month = dat.month();
  year = dat.year();

  newDay = day;
  newMonth = month;
  newYear = year;
  ui->edDay->setText(QString::number(newDay));
  ui->edMonth->setText(QString::number(newMonth));
  ui->edYear->setText(QString::number(year));

}


void DateTimeEditorFrm::EncodeDateTime()
{
  /*------------------------------*/
  /* Build the date/time string   */
  /*                              */
  /* Format : yyyy.mm.dd-uu:mm:ss */
  /*------------------------------*/
  QString val;
  QString timeStr,dateStr;

  bool change = false;

  /*----- Detect time change -----*/
  if ((minute != newMinute) || (hour != newHour)) {
    ConvertDateTime(val,newHour,false);
    timeStr = val;
    timeStr += ":";
    ConvertDateTime(val,newMinute,false);
    timeStr += val;
    timeStr += ":00";

    change = true;
  }
  else {
    ConvertDateTime(val,QDateTime::currentDateTime().time().hour(),false);
    timeStr = val;
    timeStr += ":";
    ConvertDateTime(val,QDateTime::currentDateTime().time().minute(),false);
    timeStr += val;
    timeStr += ":00";
  }

  /*----- Detect date change -----*/
  if ((day != newDay) || ((month != newMonth) || (year != newYear))) {
    ConvertDateTime(val,newYear,true);
    dateStr = val;
    dateStr += ".";
    ConvertDateTime(val,newMonth,false);
    dateStr += val;
    dateStr += ".";
    ConvertDateTime(val,newDay,false);
    dateStr += val;
    dateStr += "-";
    change = true;
  }
  else {
    ConvertDateTime(val,QDateTime::currentDateTime().date().year(),true);
    dateStr = val;
    dateStr += ".";
    ConvertDateTime(val,QDateTime::currentDateTime().date().month(),false);
    dateStr += val;
    dateStr += ".";
    ConvertDateTime(val,QDateTime::currentDateTime().date().day(),false);
    dateStr += val;
    dateStr += "-";
  }
  if (change) {
    dateStr = dateStr + timeStr;

        /*----- Signal -----*/
    emit DateTimeChanged(dateStr);

    // Update local variables
    year = newYear;
    month = newMonth;
    day = newDay;
    hour = newHour;
    minute = newMinute;
  }
}


void DateTimeEditorFrm::CancelClicked()
{
  glob->mstSts.popupActive = popActive;
  close();
}


void DateTimeEditorFrm::OkClicked()
{
  EncodeDateTime();
  switch(ui->pageControl->currentIndex()) {
    /*----- Time entry mode active -----*/
    case 0:
      CancelClicked();
      break;
      /*----- Date entry mode active -----*/
    case 1:
      ui->pageControl->setCurrentIndex(0);                                      // Switch to time entry mode
      break;
  }
}


void DateTimeEditorFrm::ShowDialog()
{
  ui->pageControl->setCurrentIndex(1);                                          // Set date entry mode
  if (!(this->isVisible())) {
    popActive = glob->mstSts.popupActive;
  }

  if ((glob->menu) != 0) {
    ((MainMenu*)(glob->menu))->FoldMenu();
  }
  SetCurrentDateTime();
  show();
}


void DateTimeEditorFrm::MinuteUpClicked()
{
  newMinute++;
  if (newMinute >= 60) {
    newMinute = 0;
  }
  ui->edMinutes->setText(QString::number(newMinute));
}


void DateTimeEditorFrm::MinuteDownClicked()
{
  newMinute--;
  if (newMinute < 0) {
    newMinute = 59;
  }
  ui->edMinutes->setText(QString::number(newMinute));
}


void DateTimeEditorFrm::HourUpClicked()
{
  newHour++;
  if (newHour >= 24) {
    newHour = 0;
  }
  ui->edHours->setText(QString::number(newHour));
}


void DateTimeEditorFrm::HourDownClicked()
{
  newHour--;
  if (newHour < 0) {
    newHour = 23;
  }
  ui->edHours->setText(QString::number(newHour));
}


void DateTimeEditorFrm::DayUpClicked()
{
  newDay++;

  switch(newMonth) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      if (newDay > 31) {
        newDay = 1;
      }
      break;

    case 2:
      if (newDay > 29) {
        newDay = 1;
      }
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      if (newDay > 30) {
        newDay = 1;
      }
      break;
  }

  ui->edDay->setText(QString::number(newDay));
}


void DateTimeEditorFrm::DayDownClicked()
{
  newDay--;

  if (newDay < 1) {
    switch(newMonth) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        newDay = 31;
        break;

      case 2:
        newDay = 29;
        break;
      case 4:
      case 6:
      case 9:
      case 11:
        newDay = 30;
        break;
    }
  }

  ui->edDay->setText(QString::number(newDay));
}


void DateTimeEditorFrm::MonthUpClicked()
{
  newMonth++;
  if (newMonth > 12) {
    newMonth = 1;
  }

  ui->edMonth->setText(QString::number(newMonth));
}


void DateTimeEditorFrm::MonthDownClicked()
{
  newMonth--;

  if (newMonth < 1) {
    newMonth = 12;
  }

  ui->edMonth->setText(QString::number(newMonth));
}


void DateTimeEditorFrm::YearUpClicked()
{
  newYear++;
  if (newYear > 3000) {
    newYear = 2000;
  }
  ui->edYear->setText(QString::number(newYear));
}


void DateTimeEditorFrm::YearDownClicked()
{
  newYear--;
  if (newYear < 2000) {
    newYear = 3000;
  }
  ui->edYear->setText(QString::number(newYear));
}

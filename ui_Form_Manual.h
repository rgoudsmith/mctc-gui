/********************************************************************************
** Form generated from reading UI file 'Form_Manual.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_MANUAL_H
#define UI_FORM_MANUAL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ManualFrm
{
public:
    QVBoxLayout *verticalLayout_11;
    QHBoxLayout *horizontalLayout_15;
    QVBoxLayout *verticalLayout_14;
    QHBoxLayout *horizontalLayout_16;
    QSpacerItem *horizontalSpacer_19;
    QLabel *DeviceId;
    QLabel *unitName;
    QSpacerItem *horizontalSpacer_20;
    QHBoxLayout *horizontalLayout_11;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_7;
    QSpacerItem *horizontalSpacer_5;
    QHBoxLayout *horizontalLayout_7;
    QHBoxLayout *horizontalLayout_19;
    QPushButton *btMode;
    QVBoxLayout *verticalLayout_12;
    QLabel *lbSts;
    QHBoxLayout *horizontalLayout_18;
    QSpacerItem *horizontalSpacer_2;
    QLabel *lbImg;
    QSpacerItem *horizontalSpacer_24;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_5;
    QLineEdit *edRpm;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *horizontalSpacer_15;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label;
    QLabel *lbWeight;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_13;
    QLabel *lbTacho;
    QHBoxLayout *horizontalLayout_3;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QGroupBox *groupBox_7;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout;
    QLabel *lbInp1Sts;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_6;
    QLabel *lbInp1Img;
    QSpacerItem *horizontalSpacer_11;
    QGroupBox *groupBox_8;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout_5;
    QLabel *lbInp2Sts;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_12;
    QLabel *lbInp2Img;
    QSpacerItem *horizontalSpacer_13;
    QGroupBox *groupBox_9;
    QVBoxLayout *verticalLayout_8;
    QVBoxLayout *verticalLayout_9;
    QLabel *lbInp3Sts;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_14;
    QLabel *lbInp3Img;
    QSpacerItem *horizontalSpacer_16;
    QGroupBox *groupBox_10;
    QVBoxLayout *verticalLayout_10;
    QVBoxLayout *verticalLayout_13;
    QLabel *lbInp4Sts;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *horizontalSpacer_17;
    QLabel *lbInp4Img;
    QSpacerItem *horizontalSpacer_18;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_20;
    QVBoxLayout *verticalLayout_21;
    QHBoxLayout *horizontalLayout_30;
    QPushButton *btOutput2;
    QVBoxLayout *verticalLayout_22;
    QLabel *lbOut2Sts;
    QHBoxLayout *horizontalLayout_31;
    QSpacerItem *horizontalSpacer_8;
    QLabel *lbOut2Img;
    QSpacerItem *horizontalSpacer_31;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_18;
    QVBoxLayout *verticalLayout_17;
    QHBoxLayout *horizontalLayout_28;
    QPushButton *btOutput1;
    QVBoxLayout *verticalLayout_19;
    QLabel *lbOut1Sts;
    QHBoxLayout *horizontalLayout_29;
    QSpacerItem *horizontalSpacer_7;
    QLabel *lbOut1Img;
    QSpacerItem *horizontalSpacer_30;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout_23;
    QVBoxLayout *verticalLayout_24;
    QHBoxLayout *horizontalLayout_32;
    QPushButton *btOutput3;
    QVBoxLayout *verticalLayout_25;
    QLabel *lbOut3Sts;
    QHBoxLayout *horizontalLayout_33;
    QSpacerItem *horizontalSpacer_9;
    QLabel *lbOut3Img;
    QSpacerItem *horizontalSpacer_32;
    QGroupBox *groupBox_6;
    QVBoxLayout *verticalLayout_26;
    QVBoxLayout *verticalLayout_27;
    QHBoxLayout *horizontalLayout_34;
    QPushButton *btOutput4;
    QVBoxLayout *verticalLayout_28;
    QLabel *lbOut4Sts;
    QHBoxLayout *horizontalLayout_35;
    QSpacerItem *horizontalSpacer_10;
    QLabel *lbOut4Img;
    QSpacerItem *horizontalSpacer_33;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *btOk;

    void setupUi(QWidget *ManualFrm)
    {
        if (ManualFrm->objectName().isEmpty())
            ManualFrm->setObjectName(QString::fromUtf8("ManualFrm"));
        ManualFrm->resize(650, 549);
        ManualFrm->setCursor(QCursor(Qt::BlankCursor));
        ManualFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout_11 = new QVBoxLayout(ManualFrm);
        verticalLayout_11->setContentsMargins(0, 0, 0, 0);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        horizontalSpacer_19 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_16->addItem(horizontalSpacer_19);

        DeviceId = new QLabel(ManualFrm);
        DeviceId->setObjectName(QString::fromUtf8("DeviceId"));

        horizontalLayout_16->addWidget(DeviceId);

        unitName = new QLabel(ManualFrm);
        unitName->setObjectName(QString::fromUtf8("unitName"));

        horizontalLayout_16->addWidget(unitName);

        horizontalSpacer_20 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_16->addItem(horizontalSpacer_20);


        verticalLayout_14->addLayout(horizontalLayout_16);


        horizontalLayout_15->addLayout(verticalLayout_14);


        verticalLayout_11->addLayout(horizontalLayout_15);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_7 = new QLabel(ManualFrm);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_6->addWidget(label_7);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);


        verticalLayout_7->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        btMode = new QPushButton(ManualFrm);
        btMode->setObjectName(QString::fromUtf8("btMode"));
        btMode->setMinimumSize(QSize(90, 49));
        btMode->setMaximumSize(QSize(90, 49));
        btMode->setText(QString::fromUtf8("mode"));

        horizontalLayout_19->addWidget(btMode);

        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setSpacing(0);
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        lbSts = new QLabel(ManualFrm);
        lbSts->setObjectName(QString::fromUtf8("lbSts"));
        lbSts->setMinimumSize(QSize(40, 0));
        lbSts->setText(QString::fromUtf8("[sts]"));
        lbSts->setAlignment(Qt::AlignCenter);

        verticalLayout_12->addWidget(lbSts);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(0);
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_18->addItem(horizontalSpacer_2);

        lbImg = new QLabel(ManualFrm);
        lbImg->setObjectName(QString::fromUtf8("lbImg"));
        lbImg->setMinimumSize(QSize(30, 30));
        lbImg->setMaximumSize(QSize(30, 30));
        lbImg->setText(QString::fromUtf8("img"));

        horizontalLayout_18->addWidget(lbImg);

        horizontalSpacer_24 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_18->addItem(horizontalSpacer_24);


        verticalLayout_12->addLayout(horizontalLayout_18);


        horizontalLayout_19->addLayout(verticalLayout_12);


        horizontalLayout_7->addLayout(horizontalLayout_19);


        verticalLayout_7->addLayout(horizontalLayout_7);


        horizontalLayout_11->addLayout(verticalLayout_7);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_2 = new QLabel(ManualFrm);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_4->addWidget(label_2);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        horizontalLayout_4->addItem(verticalSpacer_3);


        verticalLayout_2->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        edRpm = new QLineEdit(ManualFrm);
        edRpm->setObjectName(QString::fromUtf8("edRpm"));
        edRpm->setMinimumSize(QSize(110, 70));
        edRpm->setMaximumSize(QSize(110, 70));
        edRpm->setText(QString::fromUtf8(""));
        edRpm->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(edRpm);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_4);


        verticalLayout_2->addLayout(horizontalLayout_5);


        horizontalLayout_11->addLayout(verticalLayout_2);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_15);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label = new QLabel(ManualFrm);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_12->addWidget(label);

        lbWeight = new QLabel(ManualFrm);
        lbWeight->setObjectName(QString::fromUtf8("lbWeight"));
        lbWeight->setText(QString::fromUtf8("TextLabel"));
        lbWeight->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_12->addWidget(lbWeight);


        verticalLayout_6->addLayout(horizontalLayout_12);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        label_13 = new QLabel(ManualFrm);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        horizontalLayout_17->addWidget(label_13);

        lbTacho = new QLabel(ManualFrm);
        lbTacho->setObjectName(QString::fromUtf8("lbTacho"));
        lbTacho->setText(QString::fromUtf8("TextLabel"));
        lbTacho->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_17->addWidget(lbTacho);


        verticalLayout_6->addLayout(horizontalLayout_17);


        horizontalLayout_11->addLayout(verticalLayout_6);


        verticalLayout_11->addLayout(horizontalLayout_11);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        groupBox_2 = new QGroupBox(ManualFrm);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        groupBox_7 = new QGroupBox(groupBox_2);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        groupBox_7->setAlignment(Qt::AlignCenter);
        verticalLayout_3 = new QVBoxLayout(groupBox_7);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lbInp1Sts = new QLabel(groupBox_7);
        lbInp1Sts->setObjectName(QString::fromUtf8("lbInp1Sts"));
        lbInp1Sts->setMinimumSize(QSize(40, 0));
        lbInp1Sts->setText(QString::fromUtf8("[sts]"));
        lbInp1Sts->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lbInp1Sts);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_6);

        lbInp1Img = new QLabel(groupBox_7);
        lbInp1Img->setObjectName(QString::fromUtf8("lbInp1Img"));
        lbInp1Img->setMinimumSize(QSize(30, 30));
        lbInp1Img->setMaximumSize(QSize(30, 30));
        lbInp1Img->setText(QString::fromUtf8("img"));

        horizontalLayout->addWidget(lbInp1Img);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_11);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_3->addLayout(verticalLayout);


        gridLayout_2->addWidget(groupBox_7, 0, 1, 1, 1);

        groupBox_8 = new QGroupBox(groupBox_2);
        groupBox_8->setObjectName(QString::fromUtf8("groupBox_8"));
        groupBox_8->setAlignment(Qt::AlignCenter);
        verticalLayout_4 = new QVBoxLayout(groupBox_8);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        lbInp2Sts = new QLabel(groupBox_8);
        lbInp2Sts->setObjectName(QString::fromUtf8("lbInp2Sts"));
        lbInp2Sts->setMinimumSize(QSize(40, 0));
        lbInp2Sts->setText(QString::fromUtf8("[sts]"));
        lbInp2Sts->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(lbInp2Sts);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_12);

        lbInp2Img = new QLabel(groupBox_8);
        lbInp2Img->setObjectName(QString::fromUtf8("lbInp2Img"));
        lbInp2Img->setMinimumSize(QSize(30, 30));
        lbInp2Img->setMaximumSize(QSize(30, 30));
        lbInp2Img->setText(QString::fromUtf8("img"));

        horizontalLayout_8->addWidget(lbInp2Img);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_13);


        verticalLayout_5->addLayout(horizontalLayout_8);


        verticalLayout_4->addLayout(verticalLayout_5);


        gridLayout_2->addWidget(groupBox_8, 0, 3, 1, 1);

        groupBox_9 = new QGroupBox(groupBox_2);
        groupBox_9->setObjectName(QString::fromUtf8("groupBox_9"));
        groupBox_9->setAlignment(Qt::AlignCenter);
        verticalLayout_8 = new QVBoxLayout(groupBox_9);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        lbInp3Sts = new QLabel(groupBox_9);
        lbInp3Sts->setObjectName(QString::fromUtf8("lbInp3Sts"));
        lbInp3Sts->setMinimumSize(QSize(40, 0));
        lbInp3Sts->setText(QString::fromUtf8("[sts]"));
        lbInp3Sts->setAlignment(Qt::AlignCenter);

        verticalLayout_9->addWidget(lbInp3Sts);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_14);

        lbInp3Img = new QLabel(groupBox_9);
        lbInp3Img->setObjectName(QString::fromUtf8("lbInp3Img"));
        lbInp3Img->setMinimumSize(QSize(30, 30));
        lbInp3Img->setMaximumSize(QSize(30, 30));
        lbInp3Img->setText(QString::fromUtf8("img"));

        horizontalLayout_9->addWidget(lbInp3Img);

        horizontalSpacer_16 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_16);


        verticalLayout_9->addLayout(horizontalLayout_9);


        verticalLayout_8->addLayout(verticalLayout_9);


        gridLayout_2->addWidget(groupBox_9, 1, 1, 1, 1);

        groupBox_10 = new QGroupBox(groupBox_2);
        groupBox_10->setObjectName(QString::fromUtf8("groupBox_10"));
        groupBox_10->setAlignment(Qt::AlignCenter);
        verticalLayout_10 = new QVBoxLayout(groupBox_10);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        lbInp4Sts = new QLabel(groupBox_10);
        lbInp4Sts->setObjectName(QString::fromUtf8("lbInp4Sts"));
        lbInp4Sts->setMinimumSize(QSize(40, 0));
        lbInp4Sts->setText(QString::fromUtf8("[sts]"));
        lbInp4Sts->setAlignment(Qt::AlignCenter);

        verticalLayout_13->addWidget(lbInp4Sts);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalSpacer_17 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_17);

        lbInp4Img = new QLabel(groupBox_10);
        lbInp4Img->setObjectName(QString::fromUtf8("lbInp4Img"));
        lbInp4Img->setMinimumSize(QSize(30, 30));
        lbInp4Img->setMaximumSize(QSize(30, 30));
        lbInp4Img->setText(QString::fromUtf8("img"));

        horizontalLayout_10->addWidget(lbInp4Img);

        horizontalSpacer_18 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_18);


        verticalLayout_13->addLayout(horizontalLayout_10);


        verticalLayout_10->addLayout(verticalLayout_13);


        gridLayout_2->addWidget(groupBox_10, 1, 3, 1, 1);


        horizontalLayout_3->addWidget(groupBox_2);

        groupBox = new QGroupBox(ManualFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        groupBox_4 = new QGroupBox(groupBox);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setAlignment(Qt::AlignCenter);
        verticalLayout_20 = new QVBoxLayout(groupBox_4);
        verticalLayout_20->setContentsMargins(1, 1, 1, 1);
        verticalLayout_20->setObjectName(QString::fromUtf8("verticalLayout_20"));
        verticalLayout_21 = new QVBoxLayout();
        verticalLayout_21->setObjectName(QString::fromUtf8("verticalLayout_21"));
        horizontalLayout_30 = new QHBoxLayout();
        horizontalLayout_30->setObjectName(QString::fromUtf8("horizontalLayout_30"));
        btOutput2 = new QPushButton(groupBox_4);
        btOutput2->setObjectName(QString::fromUtf8("btOutput2"));
        btOutput2->setMinimumSize(QSize(90, 49));
        btOutput2->setMaximumSize(QSize(90, 49));

        horizontalLayout_30->addWidget(btOutput2);

        verticalLayout_22 = new QVBoxLayout();
        verticalLayout_22->setSpacing(0);
        verticalLayout_22->setObjectName(QString::fromUtf8("verticalLayout_22"));
        lbOut2Sts = new QLabel(groupBox_4);
        lbOut2Sts->setObjectName(QString::fromUtf8("lbOut2Sts"));
        lbOut2Sts->setMinimumSize(QSize(40, 0));
        lbOut2Sts->setText(QString::fromUtf8("[sts]"));
        lbOut2Sts->setAlignment(Qt::AlignCenter);

        verticalLayout_22->addWidget(lbOut2Sts);

        horizontalLayout_31 = new QHBoxLayout();
        horizontalLayout_31->setSpacing(0);
        horizontalLayout_31->setObjectName(QString::fromUtf8("horizontalLayout_31"));
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_31->addItem(horizontalSpacer_8);

        lbOut2Img = new QLabel(groupBox_4);
        lbOut2Img->setObjectName(QString::fromUtf8("lbOut2Img"));
        lbOut2Img->setMinimumSize(QSize(30, 30));
        lbOut2Img->setMaximumSize(QSize(30, 30));
        lbOut2Img->setText(QString::fromUtf8("img"));

        horizontalLayout_31->addWidget(lbOut2Img);

        horizontalSpacer_31 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_31->addItem(horizontalSpacer_31);


        verticalLayout_22->addLayout(horizontalLayout_31);


        horizontalLayout_30->addLayout(verticalLayout_22);


        verticalLayout_21->addLayout(horizontalLayout_30);


        verticalLayout_20->addLayout(verticalLayout_21);


        gridLayout->addWidget(groupBox_4, 0, 2, 1, 1);

        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setAlignment(Qt::AlignCenter);
        verticalLayout_18 = new QVBoxLayout(groupBox_3);
        verticalLayout_18->setContentsMargins(1, 1, 1, 1);
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        horizontalLayout_28 = new QHBoxLayout();
        horizontalLayout_28->setObjectName(QString::fromUtf8("horizontalLayout_28"));
        btOutput1 = new QPushButton(groupBox_3);
        btOutput1->setObjectName(QString::fromUtf8("btOutput1"));
        btOutput1->setMinimumSize(QSize(90, 49));
        btOutput1->setMaximumSize(QSize(90, 49));
        btOutput1->setText(QString::fromUtf8("1"));

        horizontalLayout_28->addWidget(btOutput1);

        verticalLayout_19 = new QVBoxLayout();
        verticalLayout_19->setSpacing(0);
        verticalLayout_19->setObjectName(QString::fromUtf8("verticalLayout_19"));
        lbOut1Sts = new QLabel(groupBox_3);
        lbOut1Sts->setObjectName(QString::fromUtf8("lbOut1Sts"));
        lbOut1Sts->setMinimumSize(QSize(40, 0));
        lbOut1Sts->setText(QString::fromUtf8("[sts]"));
        lbOut1Sts->setAlignment(Qt::AlignCenter);

        verticalLayout_19->addWidget(lbOut1Sts);

        horizontalLayout_29 = new QHBoxLayout();
        horizontalLayout_29->setSpacing(0);
        horizontalLayout_29->setObjectName(QString::fromUtf8("horizontalLayout_29"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_29->addItem(horizontalSpacer_7);

        lbOut1Img = new QLabel(groupBox_3);
        lbOut1Img->setObjectName(QString::fromUtf8("lbOut1Img"));
        lbOut1Img->setMinimumSize(QSize(30, 30));
        lbOut1Img->setMaximumSize(QSize(30, 30));
        lbOut1Img->setText(QString::fromUtf8("img"));

        horizontalLayout_29->addWidget(lbOut1Img);

        horizontalSpacer_30 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_29->addItem(horizontalSpacer_30);


        verticalLayout_19->addLayout(horizontalLayout_29);


        horizontalLayout_28->addLayout(verticalLayout_19);


        verticalLayout_17->addLayout(horizontalLayout_28);


        verticalLayout_18->addLayout(verticalLayout_17);


        gridLayout->addWidget(groupBox_3, 0, 0, 1, 1);

        groupBox_5 = new QGroupBox(groupBox);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setAlignment(Qt::AlignCenter);
        verticalLayout_23 = new QVBoxLayout(groupBox_5);
        verticalLayout_23->setContentsMargins(1, 1, 1, 1);
        verticalLayout_23->setObjectName(QString::fromUtf8("verticalLayout_23"));
        verticalLayout_24 = new QVBoxLayout();
        verticalLayout_24->setObjectName(QString::fromUtf8("verticalLayout_24"));
        horizontalLayout_32 = new QHBoxLayout();
        horizontalLayout_32->setObjectName(QString::fromUtf8("horizontalLayout_32"));
        btOutput3 = new QPushButton(groupBox_5);
        btOutput3->setObjectName(QString::fromUtf8("btOutput3"));
        btOutput3->setMinimumSize(QSize(90, 49));
        btOutput3->setMaximumSize(QSize(90, 49));
        btOutput3->setText(QString::fromUtf8("3"));

        horizontalLayout_32->addWidget(btOutput3);

        verticalLayout_25 = new QVBoxLayout();
        verticalLayout_25->setSpacing(0);
        verticalLayout_25->setObjectName(QString::fromUtf8("verticalLayout_25"));
        lbOut3Sts = new QLabel(groupBox_5);
        lbOut3Sts->setObjectName(QString::fromUtf8("lbOut3Sts"));
        lbOut3Sts->setMinimumSize(QSize(40, 0));
        lbOut3Sts->setText(QString::fromUtf8("[sts]"));
        lbOut3Sts->setAlignment(Qt::AlignCenter);

        verticalLayout_25->addWidget(lbOut3Sts);

        horizontalLayout_33 = new QHBoxLayout();
        horizontalLayout_33->setSpacing(0);
        horizontalLayout_33->setObjectName(QString::fromUtf8("horizontalLayout_33"));
        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_33->addItem(horizontalSpacer_9);

        lbOut3Img = new QLabel(groupBox_5);
        lbOut3Img->setObjectName(QString::fromUtf8("lbOut3Img"));
        lbOut3Img->setMinimumSize(QSize(30, 30));
        lbOut3Img->setMaximumSize(QSize(30, 30));
        lbOut3Img->setText(QString::fromUtf8("img"));

        horizontalLayout_33->addWidget(lbOut3Img);

        horizontalSpacer_32 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_33->addItem(horizontalSpacer_32);


        verticalLayout_25->addLayout(horizontalLayout_33);


        horizontalLayout_32->addLayout(verticalLayout_25);


        verticalLayout_24->addLayout(horizontalLayout_32);


        verticalLayout_23->addLayout(verticalLayout_24);


        gridLayout->addWidget(groupBox_5, 2, 0, 1, 1);

        groupBox_6 = new QGroupBox(groupBox);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setAlignment(Qt::AlignCenter);
        verticalLayout_26 = new QVBoxLayout(groupBox_6);
        verticalLayout_26->setContentsMargins(1, 1, 1, 1);
        verticalLayout_26->setObjectName(QString::fromUtf8("verticalLayout_26"));
        verticalLayout_27 = new QVBoxLayout();
        verticalLayout_27->setObjectName(QString::fromUtf8("verticalLayout_27"));
        horizontalLayout_34 = new QHBoxLayout();
        horizontalLayout_34->setObjectName(QString::fromUtf8("horizontalLayout_34"));
        btOutput4 = new QPushButton(groupBox_6);
        btOutput4->setObjectName(QString::fromUtf8("btOutput4"));
        btOutput4->setMinimumSize(QSize(90, 49));
        btOutput4->setMaximumSize(QSize(90, 49));
        btOutput4->setText(QString::fromUtf8("4"));

        horizontalLayout_34->addWidget(btOutput4);

        verticalLayout_28 = new QVBoxLayout();
        verticalLayout_28->setSpacing(0);
        verticalLayout_28->setObjectName(QString::fromUtf8("verticalLayout_28"));
        lbOut4Sts = new QLabel(groupBox_6);
        lbOut4Sts->setObjectName(QString::fromUtf8("lbOut4Sts"));
        lbOut4Sts->setMinimumSize(QSize(40, 0));
        lbOut4Sts->setText(QString::fromUtf8("[sts]"));
        lbOut4Sts->setAlignment(Qt::AlignCenter);

        verticalLayout_28->addWidget(lbOut4Sts);

        horizontalLayout_35 = new QHBoxLayout();
        horizontalLayout_35->setSpacing(0);
        horizontalLayout_35->setObjectName(QString::fromUtf8("horizontalLayout_35"));
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_35->addItem(horizontalSpacer_10);

        lbOut4Img = new QLabel(groupBox_6);
        lbOut4Img->setObjectName(QString::fromUtf8("lbOut4Img"));
        lbOut4Img->setMinimumSize(QSize(30, 30));
        lbOut4Img->setMaximumSize(QSize(30, 30));
        lbOut4Img->setText(QString::fromUtf8("img"));

        horizontalLayout_35->addWidget(lbOut4Img);

        horizontalSpacer_33 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_35->addItem(horizontalSpacer_33);


        verticalLayout_28->addLayout(horizontalLayout_35);


        horizontalLayout_34->addLayout(verticalLayout_28);


        verticalLayout_27->addLayout(horizontalLayout_34);


        verticalLayout_26->addLayout(verticalLayout_27);


        gridLayout->addWidget(groupBox_6, 2, 2, 1, 1);


        horizontalLayout_3->addWidget(groupBox);


        verticalLayout_11->addLayout(horizontalLayout_3);

        verticalSpacer_2 = new QSpacerItem(20, 48, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_11->addItem(verticalSpacer_2);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        btOk = new QPushButton(ManualFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout_2->addWidget(btOk);


        verticalLayout_11->addLayout(horizontalLayout_2);


        retranslateUi(ManualFrm);

        QMetaObject::connectSlotsByName(ManualFrm);
    } // setupUi

    void retranslateUi(QWidget *ManualFrm)
    {
        DeviceId->setText(QApplication::translate("ManualFrm", "Device ID : ", 0, QApplication::UnicodeUTF8));
        unitName->setText(QApplication::translate("ManualFrm", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("ManualFrm", "Manual mode:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("ManualFrm", "Motor speed:", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ManualFrm", "Act Weight:", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("ManualFrm", "Act Tacho:", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("ManualFrm", "Input status:", 0, QApplication::UnicodeUTF8));
        groupBox_7->setTitle(QApplication::translate("ManualFrm", "START", 0, QApplication::UnicodeUTF8));
        groupBox_8->setTitle(QApplication::translate("ManualFrm", "1", 0, QApplication::UnicodeUTF8));
        groupBox_9->setTitle(QApplication::translate("ManualFrm", "2", 0, QApplication::UnicodeUTF8));
        groupBox_10->setTitle(QApplication::translate("ManualFrm", "3", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("ManualFrm", "Output control:", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("ManualFrm", "2", 0, QApplication::UnicodeUTF8));
        btOutput2->setText(QApplication::translate("ManualFrm", "2", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("ManualFrm", "1", 0, QApplication::UnicodeUTF8));
        groupBox_5->setTitle(QApplication::translate("ManualFrm", "3", 0, QApplication::UnicodeUTF8));
        groupBox_6->setTitle(QApplication::translate("ManualFrm", "4", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(ManualFrm);
    } // retranslateUi

};

namespace Ui {
    class ManualFrm: public Ui_ManualFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_MANUAL_H

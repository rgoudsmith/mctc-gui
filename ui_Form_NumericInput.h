/********************************************************************************
** Form generated from reading UI file 'Form_NumericInput.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_NUMERICINPUT_H
#define UI_FORM_NUMERICINPUT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStackedWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NumericInputFrm
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QStackedWidget *pages;
    QWidget *page;
    QPushButton *btLeft;
    QPushButton *btInc;
    QPushButton *btRight;
    QLabel *InputValue;
    QPushButton *btDec;
    QLabel *lbInput;
    QWidget *page_2;
    QHBoxLayout *horizontalLayout_3;
    QGridLayout *gridLayout;
    QLabel *inputValue;
    QPushButton *bt7;
    QPushButton *bt8;
    QPushButton *bt9;
    QPushButton *btBack;
    QPushButton *bt4;
    QPushButton *bt5;
    QPushButton *bt6;
    QPushButton *btClear;
    QPushButton *bt1;
    QPushButton *bt2;
    QPushButton *bt3;
    QPushButton *btSign;
    QPushButton *bt0;
    QPushButton *btDot;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btSwitch;
    QSpacerItem *horizontalSpacer;
    QPushButton *btOk;
    QPushButton *btCancel;

    void setupUi(QWidget *NumericInputFrm)
    {
        if (NumericInputFrm->objectName().isEmpty())
            NumericInputFrm->setObjectName(QString::fromUtf8("NumericInputFrm"));
        NumericInputFrm->setWindowModality(Qt::ApplicationModal);
        NumericInputFrm->resize(370, 386);
        NumericInputFrm->setMinimumSize(QSize(370, 386));
        NumericInputFrm->setMaximumSize(QSize(370, 462));
        NumericInputFrm->setCursor(QCursor(Qt::BlankCursor));
        NumericInputFrm->setWindowTitle(QString::fromUtf8("Numeric Input"));
        verticalLayout_2 = new QVBoxLayout(NumericInputFrm);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox = new QGroupBox(NumericInputFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox{border: 5px solid black; border-radius: 7px; background: white}"));
        groupBox->setAlignment(Qt::AlignCenter);
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(9, 0, 9, 9);
        pages = new QStackedWidget(groupBox);
        pages->setObjectName(QString::fromUtf8("pages"));
        pages->setStyleSheet(QString::fromUtf8("#page{background:white;} #page_2{background: white;}"));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        btLeft = new QPushButton(page);
        btLeft->setObjectName(QString::fromUtf8("btLeft"));
        btLeft->setGeometry(QRect(5, 81, 90, 70));
        btLeft->setMinimumSize(QSize(90, 70));
        btLeft->setMaximumSize(QSize(90, 70));
        btLeft->setText(QString::fromUtf8("Left"));
        btInc = new QPushButton(page);
        btInc->setObjectName(QString::fromUtf8("btInc"));
        btInc->setGeometry(QRect(94, 12, 161, 70));
        btInc->setMinimumSize(QSize(161, 70));
        btInc->setMaximumSize(QSize(161, 70));
        btInc->setText(QString::fromUtf8("Inc"));
        btInc->setAutoRepeat(true);
        btInc->setAutoRepeatDelay(500);
        btInc->setAutoRepeatInterval(70);
        btInc->setDefault(false);
        btRight = new QPushButton(page);
        btRight->setObjectName(QString::fromUtf8("btRight"));
        btRight->setGeometry(QRect(253, 80, 90, 70));
        btRight->setMinimumSize(QSize(90, 70));
        btRight->setMaximumSize(QSize(90, 70));
        btRight->setText(QString::fromUtf8("Right"));
        InputValue = new QLabel(page);
        InputValue->setObjectName(QString::fromUtf8("InputValue"));
        InputValue->setGeometry(QRect(94, 81, 161, 70));
        InputValue->setMaximumSize(QSize(170, 70));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        InputValue->setPalette(palette);
        QFont font;
        font.setPointSize(20);
        font.setBold(true);
        font.setWeight(75);
        InputValue->setFont(font);
        InputValue->setAutoFillBackground(false);
        InputValue->setStyleSheet(QString::fromUtf8("border: 1px solid black; border-radius: 3px;"));
        InputValue->setFrameShape(QFrame::Box);
        InputValue->setFrameShadow(QFrame::Plain);
        InputValue->setText(QString::fromUtf8("TextLabel"));
        InputValue->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        btDec = new QPushButton(page);
        btDec->setObjectName(QString::fromUtf8("btDec"));
        btDec->setGeometry(QRect(94, 150, 161, 70));
        btDec->setMinimumSize(QSize(161, 70));
        btDec->setMaximumSize(QSize(161, 70));
        btDec->setText(QString::fromUtf8("Dec"));
        btDec->setAutoRepeat(true);
        btDec->setAutoRepeatDelay(500);
        btDec->setAutoRepeatInterval(70);
        lbInput = new QLabel(page);
        lbInput->setObjectName(QString::fromUtf8("lbInput"));
        lbInput->setGeometry(QRect(233, 120, 16, 22));
        lbInput->setMinimumSize(QSize(5, 0));
        lbInput->setMaximumSize(QSize(16, 16777215));
        lbInput->setFrameShape(QFrame::HLine);
        lbInput->setLineWidth(4);
        lbInput->setMidLineWidth(0);
        lbInput->setText(QString::fromUtf8(""));
        lbInput->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        pages->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        horizontalLayout_3 = new QHBoxLayout(page_2);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        inputValue = new QLabel(page_2);
        inputValue->setObjectName(QString::fromUtf8("inputValue"));
        inputValue->setMinimumSize(QSize(310, 30));
        inputValue->setMaximumSize(QSize(310, 40));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::Base, brush);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush);
        inputValue->setPalette(palette1);
        inputValue->setFont(font);
        inputValue->setAutoFillBackground(false);
        inputValue->setStyleSheet(QString::fromUtf8("border: 1px solid black; border-radius: 3px;"));
        inputValue->setFrameShape(QFrame::Box);
        inputValue->setText(QString::fromUtf8("0"));
        inputValue->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(inputValue, 0, 0, 1, 4);

        bt7 = new QPushButton(page_2);
        bt7->setObjectName(QString::fromUtf8("bt7"));
        bt7->setMinimumSize(QSize(70, 70));
        bt7->setMaximumSize(QSize(70, 70));
        QFont font1;
        font1.setPointSize(30);
        font1.setBold(true);
        font1.setWeight(75);
        bt7->setFont(font1);
        bt7->setText(QString::fromUtf8("7"));

        gridLayout->addWidget(bt7, 1, 0, 1, 1);

        bt8 = new QPushButton(page_2);
        bt8->setObjectName(QString::fromUtf8("bt8"));
        bt8->setMinimumSize(QSize(70, 70));
        bt8->setMaximumSize(QSize(70, 70));
        bt8->setFont(font1);
        bt8->setText(QString::fromUtf8("8"));

        gridLayout->addWidget(bt8, 1, 1, 1, 1);

        bt9 = new QPushButton(page_2);
        bt9->setObjectName(QString::fromUtf8("bt9"));
        bt9->setMinimumSize(QSize(70, 70));
        bt9->setMaximumSize(QSize(70, 70));
        bt9->setFont(font1);
        bt9->setText(QString::fromUtf8("9"));

        gridLayout->addWidget(bt9, 1, 2, 1, 1);

        btBack = new QPushButton(page_2);
        btBack->setObjectName(QString::fromUtf8("btBack"));
        btBack->setMinimumSize(QSize(70, 70));
        btBack->setMaximumSize(QSize(70, 70));
        btBack->setText(QString::fromUtf8("<---"));

        gridLayout->addWidget(btBack, 1, 3, 1, 1);

        bt4 = new QPushButton(page_2);
        bt4->setObjectName(QString::fromUtf8("bt4"));
        bt4->setMinimumSize(QSize(70, 70));
        bt4->setMaximumSize(QSize(70, 70));
        bt4->setFont(font1);
        bt4->setText(QString::fromUtf8("4"));

        gridLayout->addWidget(bt4, 2, 0, 1, 1);

        bt5 = new QPushButton(page_2);
        bt5->setObjectName(QString::fromUtf8("bt5"));
        bt5->setMinimumSize(QSize(70, 70));
        bt5->setMaximumSize(QSize(70, 70));
        bt5->setFont(font1);
        bt5->setText(QString::fromUtf8("5"));

        gridLayout->addWidget(bt5, 2, 1, 1, 1);

        bt6 = new QPushButton(page_2);
        bt6->setObjectName(QString::fromUtf8("bt6"));
        bt6->setMinimumSize(QSize(70, 70));
        bt6->setMaximumSize(QSize(70, 70));
        bt6->setFont(font1);
        bt6->setText(QString::fromUtf8("6"));

        gridLayout->addWidget(bt6, 2, 2, 1, 1);

        btClear = new QPushButton(page_2);
        btClear->setObjectName(QString::fromUtf8("btClear"));
        btClear->setMinimumSize(QSize(70, 70));
        btClear->setMaximumSize(QSize(70, 70));
        btClear->setText(QString::fromUtf8("Clear"));

        gridLayout->addWidget(btClear, 2, 3, 1, 1);

        bt1 = new QPushButton(page_2);
        bt1->setObjectName(QString::fromUtf8("bt1"));
        bt1->setMinimumSize(QSize(70, 70));
        bt1->setMaximumSize(QSize(70, 70));
        bt1->setFont(font1);
        bt1->setText(QString::fromUtf8("1"));

        gridLayout->addWidget(bt1, 3, 0, 1, 1);

        bt2 = new QPushButton(page_2);
        bt2->setObjectName(QString::fromUtf8("bt2"));
        bt2->setMinimumSize(QSize(70, 70));
        bt2->setMaximumSize(QSize(70, 70));
        bt2->setFont(font1);
        bt2->setText(QString::fromUtf8("2"));

        gridLayout->addWidget(bt2, 3, 1, 1, 1);

        bt3 = new QPushButton(page_2);
        bt3->setObjectName(QString::fromUtf8("bt3"));
        bt3->setMinimumSize(QSize(70, 70));
        bt3->setMaximumSize(QSize(70, 70));
        bt3->setFont(font1);
        bt3->setText(QString::fromUtf8("3"));

        gridLayout->addWidget(bt3, 3, 2, 1, 1);

        btSign = new QPushButton(page_2);
        btSign->setObjectName(QString::fromUtf8("btSign"));
        btSign->setMinimumSize(QSize(70, 70));
        btSign->setMaximumSize(QSize(70, 70));
        btSign->setText(QString::fromUtf8("+/-"));

        gridLayout->addWidget(btSign, 3, 3, 1, 1);

        bt0 = new QPushButton(page_2);
        bt0->setObjectName(QString::fromUtf8("bt0"));
        bt0->setMinimumSize(QSize(70, 70));
        bt0->setMaximumSize(QSize(70, 70));
        bt0->setFont(font1);
        bt0->setText(QString::fromUtf8("0"));

        gridLayout->addWidget(bt0, 4, 1, 1, 1);

        btDot = new QPushButton(page_2);
        btDot->setObjectName(QString::fromUtf8("btDot"));
        btDot->setMinimumSize(QSize(70, 70));
        btDot->setMaximumSize(QSize(70, 70));
        btDot->setFont(font1);
        btDot->setText(QString::fromUtf8("."));

        gridLayout->addWidget(btDot, 4, 2, 1, 1);


        horizontalLayout_3->addLayout(gridLayout);

        pages->addWidget(page_2);

        verticalLayout->addWidget(pages);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(9);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, -1, -1, 0);
        btSwitch = new QPushButton(groupBox);
        btSwitch->setObjectName(QString::fromUtf8("btSwitch"));
        btSwitch->setMinimumSize(QSize(90, 70));
        btSwitch->setMaximumSize(QSize(90, 70));
        btSwitch->setText(QString::fromUtf8("SWITCH"));

        horizontalLayout_2->addWidget(btSwitch);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        btOk = new QPushButton(groupBox);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout_2->addWidget(btOk);

        btCancel = new QPushButton(groupBox);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("Cancel"));

        horizontalLayout_2->addWidget(btCancel);


        verticalLayout->addLayout(horizontalLayout_2);


        verticalLayout_2->addWidget(groupBox);


        retranslateUi(NumericInputFrm);

        pages->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(NumericInputFrm);
    } // setupUi

    void retranslateUi(QWidget *NumericInputFrm)
    {
        groupBox->setTitle(QString());
        Q_UNUSED(NumericInputFrm);
    } // retranslateUi

};

namespace Ui {
    class NumericInputFrm: public Ui_NumericInputFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_NUMERICINPUT_H

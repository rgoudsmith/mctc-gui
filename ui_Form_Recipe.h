/********************************************************************************
** Form generated from reading UI file 'Form_Recipe.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_RECIPE_H
#define UI_FORM_RECIPE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QScrollArea>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RecipeFrm
{
public:
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QLabel *lbCaption;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_11;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_10;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_8;
    QLineEdit *edRcpName;
    QSpacerItem *horizontalSpacer_3;
    QLabel *lbRcpSelect;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *btRcpSelect;
    QSpacerItem *horizontalSpacer_7;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_9;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_6;
    QLineEdit *edVar1;
    QLineEdit *edVar2;
    QLineEdit *edRcpText;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *ReplacementSpacer;
    QHBoxLayout *horizontalLayout_12;
    QLabel *lbDevice;
    QLabel *lbMaterial;
    QLabel *label_3;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QPushButton *btSave;
    QPushButton *btRemove;
    QPushButton *btCancel;

    void setupUi(QWidget *RecipeFrm)
    {
        if (RecipeFrm->objectName().isEmpty())
            RecipeFrm->setObjectName(QString::fromUtf8("RecipeFrm"));
        RecipeFrm->resize(800, 526);
        RecipeFrm->setCursor(QCursor(Qt::BlankCursor));
        RecipeFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout_7 = new QVBoxLayout(RecipeFrm);
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        lbCaption = new QLabel(RecipeFrm);
        lbCaption->setObjectName(QString::fromUtf8("lbCaption"));
        QFont font;
        font.setPointSize(35);
        font.setBold(true);
        font.setWeight(75);
        lbCaption->setFont(font);
        lbCaption->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(lbCaption);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_7->addLayout(horizontalLayout_2);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(0);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        groupBox = new QGroupBox(RecipeFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox {border: 2px solid black; border-radius: 3px; }"));
        groupBox->setTitle(QString::fromUtf8(""));
        horizontalLayout = new QHBoxLayout(groupBox);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_8);

        edRcpName = new QLineEdit(groupBox);
        edRcpName->setObjectName(QString::fromUtf8("edRcpName"));
        edRcpName->setMinimumSize(QSize(420, 70));
        edRcpName->setMaximumSize(QSize(420, 70));
        edRcpName->setCursor(QCursor(Qt::BlankCursor));
        edRcpName->setFocusPolicy(Qt::NoFocus);
        edRcpName->setText(QString::fromUtf8(""));
        edRcpName->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(edRcpName);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        lbRcpSelect = new QLabel(groupBox);
        lbRcpSelect->setObjectName(QString::fromUtf8("lbRcpSelect"));
        QFont font1;
        font1.setPointSize(18);
        lbRcpSelect->setFont(font1);

        horizontalLayout->addWidget(lbRcpSelect);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_6);

        btRcpSelect = new QPushButton(groupBox);
        btRcpSelect->setObjectName(QString::fromUtf8("btRcpSelect"));
        btRcpSelect->setMinimumSize(QSize(90, 70));
        btRcpSelect->setMaximumSize(QSize(90, 70));
        btRcpSelect->setText(QString::fromUtf8("RcpSel"));

        horizontalLayout->addWidget(btRcpSelect);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_7);


        horizontalLayout_10->addWidget(groupBox);


        verticalLayout_6->addLayout(horizontalLayout_10);

        groupBox_2 = new QGroupBox(RecipeFrm);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setTitle(QString::fromUtf8(""));
        horizontalLayout_9 = new QHBoxLayout(groupBox_2);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));

        horizontalLayout_9->addLayout(verticalLayout_4);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(5, -1, -1, -1);
        edVar1 = new QLineEdit(groupBox_2);
        edVar1->setObjectName(QString::fromUtf8("edVar1"));
        edVar1->setMinimumSize(QSize(130, 70));
        edVar1->setMaximumSize(QSize(130, 70));
        edVar1->setCursor(QCursor(Qt::BlankCursor));
        edVar1->setFocusPolicy(Qt::NoFocus);
        edVar1->setText(QString::fromUtf8(""));
        edVar1->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(edVar1);

        edVar2 = new QLineEdit(groupBox_2);
        edVar2->setObjectName(QString::fromUtf8("edVar2"));
        edVar2->setMinimumSize(QSize(130, 70));
        edVar2->setMaximumSize(QSize(130, 70));
        edVar2->setCursor(QCursor(Qt::BlankCursor));
        edVar2->setFocusPolicy(Qt::NoFocus);
        edVar2->setText(QString::fromUtf8(""));
        edVar2->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(edVar2);

        edRcpText = new QLineEdit(groupBox_2);
        edRcpText->setObjectName(QString::fromUtf8("edRcpText"));
        edRcpText->setMinimumSize(QSize(240, 70));
        edRcpText->setMaximumSize(QSize(240, 70));
        edRcpText->setFont(font1);
        edRcpText->setCursor(QCursor(Qt::BlankCursor));
        edRcpText->setFocusPolicy(Qt::NoFocus);
        edRcpText->setText(QString::fromUtf8(""));
        edRcpText->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(edRcpText);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);

        ReplacementSpacer = new QSpacerItem(0, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(ReplacementSpacer);


        verticalLayout_5->addLayout(horizontalLayout_6);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalLayout_12->setContentsMargins(-1, 12, -1, -1);
        lbDevice = new QLabel(groupBox_2);
        lbDevice->setObjectName(QString::fromUtf8("lbDevice"));
        lbDevice->setMinimumSize(QSize(200, 0));
        lbDevice->setMaximumSize(QSize(200, 16777215));
        QFont font2;
        font2.setPointSize(18);
        font2.setBold(false);
        font2.setWeight(50);
        lbDevice->setFont(font2);
        lbDevice->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        horizontalLayout_12->addWidget(lbDevice);

        lbMaterial = new QLabel(groupBox_2);
        lbMaterial->setObjectName(QString::fromUtf8("lbMaterial"));
        lbMaterial->setMinimumSize(QSize(200, 0));
        lbMaterial->setMaximumSize(QSize(200, 16777215));
        lbMaterial->setFont(font2);
        lbMaterial->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        horizontalLayout_12->addWidget(lbMaterial);

        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(110, 0));
        label_3->setMaximumSize(QSize(110, 16777215));

        horizontalLayout_12->addWidget(label_3);


        verticalLayout_5->addLayout(horizontalLayout_12);

        scrollArea = new QScrollArea(groupBox_2);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 647, 214));
        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout_5->addWidget(scrollArea);


        horizontalLayout_9->addLayout(verticalLayout_5);


        verticalLayout_6->addWidget(groupBox_2);


        horizontalLayout_11->addLayout(verticalLayout_6);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        btSave = new QPushButton(RecipeFrm);
        btSave->setObjectName(QString::fromUtf8("btSave"));
        btSave->setMinimumSize(QSize(90, 70));
        btSave->setMaximumSize(QSize(90, 70));
        btSave->setText(QString::fromUtf8("Save"));

        verticalLayout->addWidget(btSave);

        btRemove = new QPushButton(RecipeFrm);
        btRemove->setObjectName(QString::fromUtf8("btRemove"));
        btRemove->setMinimumSize(QSize(90, 70));
        btRemove->setMaximumSize(QSize(90, 70));
        btRemove->setText(QString::fromUtf8("Remove"));

        verticalLayout->addWidget(btRemove);

        btCancel = new QPushButton(RecipeFrm);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("Cancel"));

        verticalLayout->addWidget(btCancel);


        horizontalLayout_11->addLayout(verticalLayout);


        verticalLayout_7->addLayout(horizontalLayout_11);


        retranslateUi(RecipeFrm);

        QMetaObject::connectSlotsByName(RecipeFrm);
    } // setupUi

    void retranslateUi(QWidget *RecipeFrm)
    {
        lbCaption->setText(QApplication::translate("RecipeFrm", "Recipe", 0, QApplication::UnicodeUTF8));
        lbRcpSelect->setText(QApplication::translate("RecipeFrm", "Select  : ", 0, QApplication::UnicodeUTF8));
        lbDevice->setText(QApplication::translate("RecipeFrm", "Device ID:", 0, QApplication::UnicodeUTF8));
        lbMaterial->setText(QApplication::translate("RecipeFrm", "Material:", 0, QApplication::UnicodeUTF8));
        label_3->setText(QString());
        Q_UNUSED(RecipeFrm);
    } // retranslateUi

};

namespace Ui {
    class RecipeFrm: public Ui_RecipeFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_RECIPE_H

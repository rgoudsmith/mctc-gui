#ifndef OVERLAYMENU_H
#define OVERLAYMENU_H

#include <QtGui/QWidget>
#include <QPushButton>
#include <QTimer>
#include "Form_QbaseWidget.h"

namespace Ui
{
  class OverlayMenu;
}


class OverlayMenu : public QBaseWidget
{
  Q_OBJECT
    public:
    OverlayMenu(QWidget *parent = 0);
    ~OverlayMenu();
    QPushButton * GetMenuButton(int);
    void HideMenuOnClick(bool);
    void FormInit();
    void Reposition(int w, int h);
    void SetKeyLockButtonImage();

  protected:
    void changeEvent(QEvent *e);
    void showEvent(QShowEvent *);

  private:
    Ui::OverlayMenu *m_ui;
    bool hideMenuOnClick;
    bool isVisible;
    bool firstShow;
    int parentW,parentH;
    void SetMenuLength(int);

  public slots:
    void DisplayMenu();
    void FoldMenu();
    void SetBtLoginCaption(const QString &caption);
    void SetBtConfigCaption(const QString &caption);
    void SetBtAlarmCaption(const QString &caption);
    void SetBtLearnCaption(const QString &caption);
    void SetBtUsbCaption(const QString &caption);

    // Convert a button-click to a MenuButtonClicked signal with the corresponding integer value as parameter.
    void BtLoginClicked();
    void BtConfigClicked();
    void BtAlarmClicked();
    void BtLearnClicked();
    void BtUsbClicked();
    void BtKeyLockClicked();
    void BtConsumptionClicked();

    void UserChanged(eUserType);

    signals:
    void MenuBtLoginClicked();
    void MenuBtConfigClicked();
    void MenuBtAlarmClicked();
    void MenuBtLearnClicked();
    void MenuBtUsbClicked();
    void MenuBtKeyLockClicked();
    void MenuButtonClicked(eWindowIdx);
    void MstCfg_ProdModeChanged(eMode);
};
#endif                                                                          // OVERLAYMENU_H

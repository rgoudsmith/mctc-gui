/****************************************************************************
** Meta object code from reading C++ file 'CalibrationPopup.h'
**
** Created: Wed Feb 6 11:14:52 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "CalibrationPopup.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CalibrationPopup.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CalibrationPopup[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   17,   17,   17, 0x05,
      38,   17,   17,   17, 0x05,

 // slots: signature, parameters, type, tag, flags
      57,   17,   17,   17, 0x08,
      69,   17,   17,   17, 0x08,
      85,   17,   17,   17, 0x08,
     100,   17,   17,   17, 0x08,
     114,   17,   17,   17, 0x08,
     132,   17,   17,   17, 0x0a,
     153,  151,   17,   17, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CalibrationPopup[] = {
    "CalibrationPopup\0\0PrimeSelected(bool)\0"
    "SelectionMade(int)\0OkClicked()\0"
    "CancelClicked()\0AbortClicked()\0"
    "SaveClicked()\0ContinueClicked()\0"
    "SetActivePage(int)\0,\0PrimeActiveChanged(bool,int)\0"
};

const QMetaObject CalibrationPopup::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_CalibrationPopup,
      qt_meta_data_CalibrationPopup, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CalibrationPopup::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CalibrationPopup::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CalibrationPopup::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CalibrationPopup))
        return static_cast<void*>(const_cast< CalibrationPopup*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int CalibrationPopup::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: PrimeSelected((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: SelectionMade((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: OkClicked(); break;
        case 3: CancelClicked(); break;
        case 4: AbortClicked(); break;
        case 5: SaveClicked(); break;
        case 6: ContinueClicked(); break;
        case 7: SetActivePage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: PrimeActiveChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void CalibrationPopup::PrimeSelected(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CalibrationPopup::SelectionMade(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE

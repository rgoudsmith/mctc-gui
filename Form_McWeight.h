#ifndef FORM_MCWEIGHT_H
#define FORM_MCWEIGHT_H

#include <QWidget>
#include <QLineEdit>
#include "Rout.h"
#include "ComShare.h"
#include "Form_NumericInput.h"
#include "Form_QbaseWidget.h"

namespace Ui
{
  class McWeightFrm;
}


class McWeightFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit McWeightFrm(QWidget *parent = 0);
    ~McWeightFrm();
    void FormInit();

  private:
    Ui::McWeightFrm *ui;
    NumericInputFrm * numInput;

  protected:
    void paintEvent(QPaintEvent *e);
    void resizeEvent(QResizeEvent *e);
    void showEvent(QShowEvent *e);
    bool eventFilter(QObject *, QEvent *);
    void DisplayImage(QPainter *p);
    void DrawLevels(QPainter *p);
    void SetEditFieldData();

    signals:
    void SysCfg_SaveConfigToFile(int);

  private slots:
    void OkClicked();
    void CancelClicked();
    void ManualFillPressed();
    void ManualFillReleased();
    void FillSettingsClicked();

  public slots:
    void ComMsgReceived(tMsgControlUnit,int);
    void ValueReturned(float value);
};
#endif                                                                          // FORM_MCWEIGHT_H

#ifndef IMAGEDEF_H
#define IMAGEDEF_H

#include "Common.h"
#include "ComShare.h"

#define MAX_USER_ALARM 10
#define MAX_DIG_OUT 4
#define MAX_DIG_IN 4

// String lengths
#define MAX_LENGTH_FILENAME_INPUT 26
#define MAX_LENGTH_FILENAME (MAX_LENGTH_FILENAME_INPUT+4)
#define MAX_LENGTH_RECIPE_DESCRIPTION 30
#define MAX_LENGTH_MC_ID 10
#define MAX_LENGTH_PASSWD 10
#define ALARM_NO_ALARM 0

//#define CFG_MAX_UNIT_COUNT 4

#define SYRINX_USER

/*----- MVC System display types -----*/
#define DISP_TYPE_SINGLE_UNIT           0                                       /* Single unit (idx = 0) */
#define DISP_TYPE_TWIN                  1                                       /* Twin unit */
#define DISP_TYPE_MULTI_UNIT            2                                       /* Single unit from multi units (idx = dispIdx) */

enum eBuzzerStatus
{
  buzNone,
  buzBeep,
  buzNoAccess,
  buzCount
};

enum eSystemStatus
{
  sysIdle,
  sysStarting,
  sysStarted,
  sysStopping,
  sysAborted,
  sysPrime,
  sysCount
};

enum eCurveMode
{
  cmodManual,
  cmodAuto,
  cmodCount
};

enum msgType
{
  msgtNone,
  msgtWarning,
  msgtAlarm,
  msgtError,
  msgtConfirm,
  msgtAccept,
  msgtConfig,
  msgtSave,
  msgtCount
};

enum eLedStatus
{
  lstsOff,
  lstsOn,
  lstsBlink,
  lstsCount
};

enum eMcDeviceType
{
  mcdtNone,
  mcdtWeight,
  mcdtBalance,
  mcdtBalanceHO,
  mcdtBalanceRegrind,
  mcdtBalanceHORegrind,
  mcdtCount
};

enum eMvcDispState
{
  mvcDSminimum,
  mvcDSmedium,
  mvcDSmaximum,
  mvcDSpreCal,
  mvcDSpreCal2
};

enum eImgTypes
{
  itDosTool,
  itMotor,
  itGranulate,
  itFillSys,
  itType,
  itMode,
  itGraviRpm,
  itUserSelect,
  itGeneral,
  itMenu,
  itSelection,
  itActive,
  itLanguages,
  itConfig,
  itFileHandle,
  itDeviceType,
  itKnifeGate,
  itMcwType,
  itBalHOType,
  itAlarmWarning,                         /* Plaats een nieuw image type voor itAlarmWarning */
  itCount
};

enum eDosTool
{
  dtGX,
  dtGLX,
  dtHX,
  dtA30,
  dtA20,
  dtA15,
  dtA50,
  dtCount
};

enum eMotor
{
  mtLT,
  mtHT,
  mtCount
};

enum eGranulate
{
  gtNormal,
  gtMicro,
  gtCount
};

enum eMcwtype
{
  mcw100,
  mcw500,
  mcw1000,
  mcwCount
};

enum eBalHOtype
{
  balHO100,
  balHO500,
  balHO1000,
  balHOCount
};


enum eFillSys
{
  fsNone,
  fsME,
  fsMV,
  fsEX,
  fsRG,
  fsCount
};

enum eType
{
  ttRelay,
  ttTimer,
  ttTacho,
  ttCount
};

enum eMode
{
  modInject,
  modExtrude,
  modCount
};

enum eGraviRpm
{
  grRpm,
  grGravi,
  grCount
};

enum eGeneral
{
  genNoImg,
  genInfo,
  genHopper,
  genKeyboard,
  genUSB,
  genKeyNum1,
  genKeyNum2,
  genArrowLeft,
  genArrowRight,
  genArrowInc,
  genArrowDec,
  genArrowKeyUp,
  genArrowKeyDown,
  genArrowKeyLeft,
  genArrowKeyRight,
  genFillManual,
  genUnitOn,
  genUnitOff,
  genLedGreen,
  genLedRed,
  genInpBackspace,
  genInpBackspace2,
  genInpClear,
  genInpClear2,
  genLearnSave,
  genLearnSaved,
  genCount
};

enum eMenu
{
  mnButtonOn,
  mnButtonOff,
  mnPrime,
  mnPrimeOff,
  mnBack,
  mnHome,
  mnHomeOff,
  mnHome2,
  mnHome2Off,
  mnAlarmOn,
  mnAlarmOff,
  mnMenu,
  mnLock,
  mnUnlock,
  mnLogin,
  mnTrend,
  mnLearn,
  mnEventLog,
  mnAlarmCfg,
  mnUnitSelect,
  mnLockIndicator,
  mnConsumption,
  mnCount
};

enum eSelection
{
  selOk,
  selCancel,
  selCount
};

enum eActive
{
  actYes,
  actNo,
  actCount
};

enum eKnifeGate
{
  knifeGateOff,
  knifeGateOn,
  knifeGateCount
};

enum eConfig
{
  cfgConfig,
  cfgAdvConfig,
  cfgMaster,
  cfgNetworkSetup,
  cfgFillSystem,
  cfgLevels,
  cfgCal_Menu,
  cfgCal_Calib,
  cfgCal_Zero,
  cfgCal_Check,
  cfgTol_Menu,
  cfgRcp_Menu,
  cfgRcp_Save_Menu,
  cfgMultiUnit,
  cfgCount
};

enum eLanguages
{
  lanUK,
  lang1,
  lang2,
  lang3,
  lang4,
  lang5,
  lang6,
  lang7,
  lang8,
  lang9,
  lang10,
  lang11,
  lang12,
  lang13,
  lang14,
  lang15,
  lang16,
  lang17,
  lanCount
};

enum eAlmWarn
{
  awAlarm,
  awAlarmBig,
  awWarning,
  awWarningBig,
  awCount
};

/*----- Window types -----*/
enum eWindowIdx
{
  wiNone,
  wiLogin,
  wiUnitCfgStd,
  wiAlarmHistory,
  wiAlarmActual,
  wiTrending,
  wiMaterialCalib,
  wiMaterialCalibOnline,
  wiSave,
  wiHome,                                                                       /* Home screen = production screen */
  wiBack,
  wiAlarm,
  wiLevels,
  wiAdvLevels,
  wiTolerances,
  wiCalibSelect,
  wiAdvConfig2,
  wiSysCalib,
  wiPrime,
  wiUnitSelect,
  wiSplash,
  wiError,
  wiMaterials,
  wiWeightCheck,
  wiRcpSelect,
  wiRcpConfig,
  wiIpEditor,
  wiSysSettings,
  wiBalanceDiag,
  wiManualIo,
  wiUSB,
  wiUSBTransfer,
  wiAlarmConfig,
  wiConsumption,
  wiRegrindConfig,
  wiMcWeightSettings,
  wiHomeMultiUnit,
  wiUnitModeConfig,
  wiCount
};

//#define TEST_PAGE wiUSB

enum eEnumType
{
  etUserSelect,
  etUser,
  etDosTool,
  etMotor,
  etGranulate,
  etProdType,
  etInpType,
  etGraviRpm,
  etFillSystem,
  etLanguage,
  etYesNo,
  etOnOff,
  etMcStatus,
  etMcwType,
  etBalHOType,
  etCount
};

enum eUserSelectType
{
  usOperator,
  usTooling,
  usSupervisor,
  usCount
};

enum eUserType
{
  utNone,
  utOperator,
  utTooling,
  utSupervisor,
  utSupervisorBackdoor,
  utAgent,
  utMovaColor,
  utSyrinx,
  utCount
};

enum eCalState
{
  csNone,
  csCalibration,
  csZero
};

enum eCalCommand
{
  calNone,
  calStart,
  calAbort,
  calPause,
  calContinue,
  calRefPlaced,
  calDone
};

enum eCalStatus
{
  calsIdle,
  calsBusy,
  calsWaitForRef,
  calsBusy2,
  calsReady,
  calsPause,
  calsErr
};

enum eMCStatus
{
  mcsNone,
  mcsOff,
  mcsStandby,
  mcsDosing,
  mcsFilling,
  mcsManFill,
  mcsUnloading
};

enum eFileHandle
{
  fhNew,
  fhRename,
  fhSave,
  fhDelete,
  fhDeleteAll,
  fhSearch,
  fhCount
};

/* Communication structs for MVC settings and Agent Settings */
#ifdef UIT
struct DeviationStruct
{
  unsigned char corDev;
  float corFac1;
  float corFac2;
  unsigned char corN1;
  unsigned char corN2;
  unsigned char corMax;
};

struct ConfigStruct
{
  unsigned char operatingMode;
  unsigned char prodType;
  unsigned char InjInputMode;
  unsigned char ExtInputMode;
  unsigned char InjRelFilt;
  float MetDevPct;
  float MaxMotorSpeed;
  unsigned char MotorCurrent;
  unsigned char MotorMicroStepRpm;
  unsigned char MotorConn6Wire;
  float RunContactOnDly;
  float RunContactOffDly;
  unsigned char RunContactMode;
};

struct AlarmStruct
{
  unsigned char AlmCfg[MAX_USER_ALARM];
};

struct FillSystemStruct
{
  unsigned char FillOnOff;
  unsigned char FillSystem;
  float FillAlmTime;
  float FillTimeMV;
  float FillEmptyTime;
  unsigned char FillAlmMode;
  unsigned char FillAlmCycles;
  unsigned short AfterFillDelay;                                                // MvDelay
};

struct LevelStruct
{
  float hopperEmptyWeight;                                                      // LoLo level
  float hopperSensorFill;                                                       // Lo level
  float hopperHiLevel;
  float hopperHiHiLevel;
};

struct ManFillDetectStruct
{
  float BalFillStart;
  float BalFillStartHl;
  float BalFillReady;
  unsigned char BalFillTime;
  unsigned char BalLidOffTime;
  float BalLidOffWeight;
};

struct ParmConsStruct
{
  float ConMeasTime;                                                            // Consumption update time
};

struct BalanceStruct
{
  DeviationStruct Dev[5];
  unsigned char ActUpdate;
  float CurveGain;
  float ColPctSetDev;
  float ExtCapPctRel;
  float ExtCapSampleTime;
  float DevB4Hyst;
  unsigned short RpmCorFac1;
  unsigned short RpmCorFac2;
  unsigned short DevThresholdInj;
  unsigned short DevThresholdExt;
  /* Measurement time */
  unsigned char MinTime;
  unsigned short MinTimeB4;
  unsigned short MaxTime;
  float TimeCorFac;
  /* Deviation Alarm */
  unsigned char DevAlmPct;
  unsigned char DevAlmRetry;
  unsigned char DevAlmPctB4;
  unsigned char DevAlmRetryB4;
  /* Deviation Alarm QR */
  unsigned char DevAlmPctQR;
  unsigned char DevAlmRetryQR;
  unsigned char DevAlmBandNrQR;
};

struct PreCalParmStruct
{
  float CalDev;
  unsigned char CalAlmCycle;
  unsigned char TestTime1;
  unsigned char TestTime2;
};

struct WeightStruct
{
  float fullScale;
  float refWeight;
  unsigned char motionBand;
  unsigned char motionDelay;
  unsigned char weightFiltFac;
  unsigned char weightFastFiltFac;
};

struct AppParamStruct
{
  ConfigStruct sys;
  WeightStruct weight;
  AlarmStruct alm;
  LevelStruct levels;
  FillSystemStruct fillSystem;
  ManFillDetectStruct manFillDet;
  ParmConsStruct cons;
  BalanceStruct bal;
  PreCalParmStruct preCal;
};
#endif

/* Internal structures */

struct ProcessDiagStruct
{
  unsigned char metRstHystCnt;
  unsigned char bandNr;
  unsigned char inBand;
  unsigned char outBand;
  unsigned char bandCnt;
  unsigned char corSts;
  unsigned char mcSts;
  unsigned char putIst;
  unsigned char getIst;
  unsigned char mcStsQR;
  unsigned char putIstQR;
  unsigned char getIstQR;

  float metTimeAct;
  float metTimeMeas;
  float metTimeCalc;
  float colCapIst;
  float vidColDevPct;
  float colDevPctRel;
  float corCycle;
  float vidCorrection;
  float rpmCorr;
  float corFacAdjust;
  float minTimeCorFac;
  float sampleLen;
  float timeLeft;
  float sampleLenQR;
  float timeLeftQR;
};

struct SettingsValueFloat
{
  float actVal;
  float minVal;
  float maxVal;
  float defVal;
};

struct SettingsValueInt
{
  int actVal;
  int minVal;
  int maxVal;
  int defVal;
};

struct SettingsValueByte
{
  unsigned char actVal;
  unsigned char minVal;
  unsigned char maxVal;
  unsigned char defVal;
};

struct SettingCalibStruct
{
  SettingsValueByte testLt05g;
  SettingsValueByte testGt05g;
  SettingsValueFloat weightCylinder;
  SettingsValueFloat weightAuger;
  SettingsValueByte steps;
  SettingsValueInt timeout;
  SettingsValueByte alarmCycles;
};

struct BandDefStruct
{
  SettingsValueByte corDev;
  SettingsValueFloat corFac1;
  SettingsValueFloat corFac2;
  SettingsValueByte corN1;
  SettingsValueByte corN2;
  SettingsValueByte corMax;
};

struct SettingAgentStruct
{
  SettingsValueInt fullScale;
  SettingCalibStruct calib;
  SettingsValueInt fillTime;
  SettingsValueFloat fillSpeed;
  SettingsValueFloat tachoCorrection;
  SettingsValueFloat extCapTime;
  SettingsValueFloat extCapDev;
  SettingsValueInt afterFillDelay;
  SettingsValueByte inputFilter;
  SettingsValueFloat MetTimeDev;
  SettingsValueFloat curveGain;
  SettingsValueFloat minGraviCap;
  SettingsValueByte inputDelay;
  SettingsValueFloat runContactOnDelay;
  SettingsValueFloat runContactOffDelay;
  SettingsValueByte logInterval;
  SettingsValueFloat colPctSetDev;
  SettingsValueByte devAlarmQr;
  SettingsValueByte motorSixWires;
  SettingsValueFloat maxMotorSpeed;
};

struct SettingsWeightStruct
{
  SettingsValueFloat motionBand;
  SettingsValueFloat motionDelay;
  SettingsValueByte weightFilter;
  SettingsValueByte weightFilterFast;
  SettingsValueFloat refWeight;
};

struct SettingsBalanceStruct
{
  BandDefStruct Bands[5];
  SettingsValueByte measureMinTime;
  SettingsValueInt measureBand4MinTime;
  SettingsValueInt measureMaxTime;
  SettingsValueFloat measureTimeCorr;
  SettingsValueFloat measureConsTime;
  SettingsValueInt devThreshInj;
  SettingsValueInt devThreshExt;
  SettingsValueFloat devB4Hyst;
  SettingsValueInt rpmCorrFac1;
  SettingsValueInt rpmCorrFac2;
  SettingsValueFloat fillStart;
  SettingsValueFloat fillStartHL;
  SettingsValueFloat fillReady;
  SettingsValueByte fillTime;
  SettingsValueFloat lidOffWeight;
  SettingsValueByte lidOffTime;
  SettingsValueByte devAlmPct;
  SettingsValueByte devAlmPctB4;
  SettingsValueByte devAlmRetry;
  SettingsValueByte devAlmRetryB4;
  SettingsValueByte devAlmRetryQR;
  SettingsValueByte devAlmPctQR;
  SettingsValueByte devAlmBandNrQR;
  SettingsValueByte actUpdate;
};

struct SettingAgentMstStruct
{
  SettingsValueInt screenTime;
};

struct SettingMvcMstStruct
{
  SettingAgentMstStruct agent;
};

struct SettingAlarmStruct
{
  SettingsValueByte almConfig[MAX_USER_ALARM];
};

struct SettingMvcStruct
{
  SettingAgentStruct agent;
  SettingsWeightStruct weight;
  SettingsBalanceStruct balance;
  SettingAlarmStruct alarm;
};

struct LogLineStruct
{
  bool newShotIndic;
  bool writing;
  bool standStill;
  unsigned int unitIdx;
  unsigned int dateTime;
  int actSts;
  int alarmCode;
  float dosSet;
  float dosAct;
  float rpm;
  float tachoAct;
  float hopperWeight;
  float measTimeAct;
  float measTimeDos;
  int bandNr;
  float sampleLength;
  float rpmCorFac;
  bool fillingValve;
};

/*====================*/
/* Recipe definitions */
/*====================*/

/*----- Recipe line -----*/
struct RecipeLineStruct
{
  char matName[(MAX_LENGTH_FILENAME+1)];                                        // Materialname + extension
  unsigned char matIsDefault;                                                   // Is this a default material?
  float corFac;                                                                 // Correction factor of the used material. Correction factor of the material itself is only used if recipeline corfac is not known yet (== 0)
  float colPct;                                                                 // Color percentage of the unit
};

/*----- Recipe -----*/
struct RecipeStruct
{
  unsigned char swVersion;
  char name[(MAX_LENGTH_FILENAME+1)];
  char text[(MAX_LENGTH_RECIPE_DESCRIPTION+1)];
  unsigned char mode;
  unsigned char input;
  float var1;
  float var2;
  unsigned char devCount;
  RecipeLineStruct lines[CFG_MAX_UNIT_COUNT];
  int chksum;
};

struct FileInfo
{
  int headerID;
  int appID;
  char appName[12];
  char version[20];
};

#define UNIQUE_ID 9876789
// Update archive header
struct FileInfoStruct
{
  unsigned long ID;
  int softVersion[4];
};

/*----- Application constants -----*/
#define APP_HEADER_ID  1234321
#define APP_HEADER_APPID 1
#define APP_HEADER_APPNAME   "MVC0012"
#define APP_HEADER_VERSION APP_VERSION                                          // Version is defined in "MVC0012.pro" file
#define BALANCE_PROCESS_FILE "/opt/MovaColor/MC_TC-arm"

#define FORMAT_DATETIME "dd/MM/yyyy hh:mm"
#define FORMAT_DATE_LONG "dd/MM/yyyy"
#define FORMAT_DATE_SHORT "dd/MM"
#define FORMAT_TIME_LONG "hh:mm:ss"
#define FORMAT_TIME_SHORT "hh:mm"

#define CLIP_FLOAT 99999999                                                     // Float waarde voor dos.act. Wordt vervangen voor CLIP_FLOAT_STR op het display.
#define CLIP_FLOAT_STR "xxxxxx"
#define TIMER_CLOSE_OVERLAY 6000                                                // Delay. Overlay menu sluit na 6 seconden automatisch.
//#define COM_TIMER_INTERVAL 333
#define COM_TIMER_INTERVAL 100
#define IMG_PIX_BORDER 5                                                        // Default = 5 (Border voor afbeeldingen op knoppen)

#define PRIME_TIM_VAL 500
#define PRECAL_PRIME_TIME 15
#define PRECAL_PRIME_RPM 50
#define LOG_TIME_INTERVAL (COM_TIMER_INTERVAL/2)
#define USB_REMOVE_READY_DELAY 15000

#define INPUT_MAX_TACHO 30.0
#define INPUT_MAX_RPM 200.0
#define INPUT_MAX_EXTCAP 9999.9
#define INPUT_MAX_SHOTWEIGHT 99999.9
#define INPUT_MAX_SHOTTIME 999.9

#define ADVCONF_SCROLL_SPEED 75
#define ADVCONF_SCROLL_WIDTH_OFFSET 2
#define ADVCONF_SCROLL_HEIGHT_OFFSET 6

#define EVENTLOG_SCROLL_SPEED 75

#define GEN_SETTINGS_PATH "/settings"
#define GEN_SETTINGS_FILENAME "system.cfg"
#define GEN_SETTINGS_EXTENSION ".cfg"

#define MATERIAL_EXTENSION ".mat"
#define MATERIAL_FOLDER "/materials"
#define MATERIAL_DEFAULT_FOLDER "/defaults"
#define MATERIAL_EMPTY_NAME "[name]"

#define RECIPE_EXTENSION ".rcp"
#define RECIPE_DEFAULT_FOLDER "/recipes"
#define RECIPE_SELECT_TEXT "[select]"

#define USB_BACKUP_FOLDER "/MC_TC"
#define TEMP_FOLDER "/temp"
#define UPDATE_INFO_FILE "archive.info"

#define LOG_FOLDER "/log"
#define EVENT_LOG_FILENAME "event.log"
#define EVENT_LOG_MAX_LINES 10000
#define EVENT_LOG_MAX_VISIBLE_LINES 50
#define EVENT_LOG_CURRENT_VERSION 1

#define PROCES_LOG_FILENAME "mctc-gui.log"
#define PROCES_LOG_MAX_LINES 10                                                 // Test
#define PROCES_LOG_CURRENT_VERSION 1
#define LOG_INTERNAL_LOWEST_VALUE 30                                            // Lowest allowed log interval to use internal memory, below this number USB logging is only available

#ifdef UIT
#define IO_PIN_BUZZER 42
#define IO_PIN_POWER_LED 38
#define IO_PIN_INPUT_LED 37
#define IO_PIN_ALARM_LED 36
#define IO_PIN_BLINK_TIME 200
#endif

#define SPLASH_WINDOW_TIME   180  /* [SECONDS] */                              // Splash window display time in ms for Embedded device

/*----- Metric units (weight, etc) -----*/
#define UNITS_MET_KG_H "kg/h"
#define UNITS_MET_G_S "g/s"
#define UNITS_MET_G "g"

/*----- Imperial units (weight, etc) -----*/
#define UNITS_IMP_KG_H "lb/h"
#define UNITS_IMP_G_S "lb/h"
#define UNITS_IMP_G "lb"

/*----- Common units -----*/
#define UNITS_SEC "s"
#define UNITS_PERC "%"
#define UNITS_VOLT "V"
// RPM units are the only units hardcoded into global.c so they can be translated easily

// MVC System image paths
#define SYSTEM_SINGLE_IMG     "/images/systemSINGLE2.png"
#define SYSTEM_DUO_IMG        "/images/systemTWIN2.png"
#define SYSTEM_DOSING_IMG     "/images/systemDOS.png"
#define SYSTEM_MC_WEIGHT_IMG  "/images/systemMcWeight.png"
#define SYSTEM_MC_HO          "/images/systemMcHighOut.png"

// Dosing Tool image paths
#define DT_GX_IMG "/images/scr_GX.png"
#define DT_GLX_IMG "/images/scr_GLX.png"
#define DT_HX_IMG "/images/scr_HX.png"
#define DT_A30_IMG "/images/scr_A30.png"
#define DT_A20_IMG "/images/scr_A20.png"
#define DT_A15_IMG "/images/scr_A15.png"

// Motor Type image paths
#define MT_LT_IMG "/images/eng_LT.png"
#define MT_HT_IMG "/images/eng_HT.png"

// Granulate image paths
#define GT_NG_IMG "/images/no_img.png"
#define GT_MG_IMG "/images/no_img.png"

// Fill System image paths
#define FS_NONE_IMG "/images/fs_NONE.png"
#define FS_ME_IMG "/images/fs_ME.png"
#define FS_MV_IMG "/images/fs_MV.png"
#define FS_EX_IMG "/images/fs_EX.png"

// Type image paths
#define TT_TIMER_IMG "/images/type_timer.png"
#define TT_RELAY_IMG "/images/type_relay.png"
#define TT_TACHO_IMG "/images/type_tacho.png"
#define TT_MCID_IMG "/images/type_mcid.png"

// Mode image paths
#define MOD_INJ_IMG "/images/mode_inj.png"
#define MOD_EXT_IMG "/images/mode_ext.png"

#define GRAVI_RPM_GRAVI_IMG "/images/gravRpm_gravi.png"
#define GRAVI_RPM_RPM_IMG   "/images/gravRpm_rpm.png"

// Selectable user paths
#define US_OPERATOR_IMG "/images/user1_simple.png"
#define US_TOOLING_IMG "/images/user2_simple.png"
#define US_SUPERVISOR_IMG "/images/user3_simple.png"

// General images
#define GENERAL_NO_IMG  "/images/no_img.png"
#define GENERAL_INFO "/images/info2.png"
#define GENERAL_HOPPER "/images/hopper.png"
#define GENERAL_KNIFEGATE_ON "/images/knifegate_on.png"
#define GENERAL_KNIFEGATE_OFF "/images/knifegate_off.png"
#define GENERAL_KEYBOARD "/images/keyboard.png"
#define GENERAL_USB "/images/usb.png"
#define GENERAL_KEY_NUM1 "/images/key_arrows.png"
#define GENERAL_KEY_NUM2 "/images/key_numeric.png"
#define GENERAL_ARROW_LEFT "/images/arrowLeft.png"
#define GENERAL_ARROW_RIGHT "/images/arrowRight.png"
#define GENERAL_ARROW_INC "/images/arrowInc.png"
#define GENERAL_ARROW_DEC "/images/arrowDec.png"
#define GENERAL_ARROW_KEY_UP    "/images/arrow_key_up.png"
#define GENERAL_ARROW_KEY_DOWN  "/images/arrow_key_down.png"
#define GENERAL_ARROW_KEY_LEFT  "/images/arrow_key_left.png"
#define GENERAL_ARROW_KEY_RIGHT "/images/arrow_key_right.png"
#define GENERAL_BUTTON_ACCEPT "/images/butAccept.png"
#define GENERAL_BUTTON_CANCEL "/images/butCancel.png"
#define GENERAL_FILL_MANUAL "/images/fill_man.png"
#define GENERAL_UNIT_ON   "/images/butUnitOn.png"
#define GENERAL_UNIT_OFF "/images/butUnitOff.png"
#define GENERAL_LED_GREEN "/images/ledGreen.png"
#define GENERAL_LED_RED "/images/ledRed.png"
#define GENERAL_BUTTON_BACKSPACE "/images/inp_backspace.png"
#define GENERAL_BUTTON_BACKSPACE2 "/images/inp_backspace2.png"
#define GENERAL_BUTTON_CLEAR "/images/inp_clear.png"
#define GENERAL_BUTTON_CLEAR2 "/images/inp_clear2.png"
#define GENERAL_LEARN_SAVE "/images/learn_save.png"
//#define GENERAL_LEARN_SAVED "/images/learn_saved.png"
#define GENERAL_LEARN_SAVED "/images/butAccept.png"

#define DEVICE_UNIT_HO               "/images/unit_McHighOut.png"
#define DEVICE_UNIT_WEIGHT           "/images/unit_McWeight.png"
#define DEVICE_UNIT_BALANCE          "/images/unit_McBalance.png"
#define DEVICE_UNIT_LIQUID           "/images/unit_Liquid.png"
#define DEVICE_UNIT_BALANCE_REGRIND  "/images/unit_McBalance_RG.png"
#define DEVICE_UNIT_HO_REGRIND       "/images/unit_McHighOut_RG.png"

//Menu buttons
#define MENU_BUTTON_ON      "/images/butOn.png"
#define MENU_BUTTON_OFF     "/images/butOff.png"
#define MENU_PRIME          "/images/prime.png"
#define MENU_PRIME_OFF      "/images/primeOff.png"
#define MENU_BACK           "/images/back.png"
#define MENU_HOME           "/images/home.png"
#define MENU_HOME_OFF       "/images/homeOff.png"
#define MENU_HOME2          "/images/home2.png"
#define MENU_HOME2_OFF      "/images/home2off.png"
#define MENU_ALM_ON         "/images/warningON.png"
#define MENU_ALM_OFF        "/images/warningOFF.png"
#define MENU_MENU           "/images/menu.png"
#define MENU_LOCK           "/images/lockon.png"
#define MENU_UNLOCK         "/images/lockoff.png"
#define MENU_LOGIN          "/images/login.png"
#define MENU_TREND          "/images/trending.png"
#define MENU_LEARN          "/images/learn.png"
#define MENU_EVENT_LOG      "/images/event_log.png"
#define MENU_ALARM_CFG      "/images/alarm_config.png"
#define MENU_LOCK_INDIC     "/images/lockon_indicator.png"
#define MENU_CONSUMPTION    "/images/consumption.png"

#define SEL_OK                  "/images/butAccept.png"
#define SEL_CANCEL              "/images/butCancel.png"

#define ACT_YES                 "/images/no_img.png"                            // Empty image. Text is rendered in the image by global.SetButtonImage()
#define ACT_NO                  "/images/no_img.png"                            // Empty image. Text is rendered in the image by global.SetButtonImage()

// Config icons
#define CFG_CONFIG_IMG          "/images/cfg_config.png"                        // Unit configuration
#define CFG_ADV_CONFIG_IMG      "/images/cfg_adv_config.png"                    // Advanced configuration
#define CFG_NETWORK_SETUP_IMG   "/images/cfg_net_setup.png"                     // Network configuration
#define CFG_MASTER_SETUP_IMG    "/images/cfg_master.png"
#define CFG_FILL_SYSTEM_IMG     "/images/cfg_fill_system.png"                   // Fill system configuration
#define CFG_LEVELS_IMG          "/images/cfg_levels.png"                        // Hopper levels
#define CFG_CAL_MENU            "/images/cal_menu.png"                          // Icon for Loadcell calibration menu
#define CFG_CAL_CALIB           "/images/cal_calib.png"                         // Loadcell calibration (calib)
#define CFG_CAL_ZERO            "/images/cal_zero.png"                          // Loadcell calibration (Zero)
#define CFG_CAL_CHECK           "/images/cal_check.png"                         // Weight check
#define CFG_TOL_MENU            "/images/tol_menu.png"                          // Tollerances
#define CFG_RCP_MENU            "/images/recipe.png"                            // Recipe
#define CFG_RCP_SAVE_MENU       "/images/recipe_save.png"                       // Save recipe
#define CFG_MULTI_UNIT          "/images/multiCompConfig.png"                   // Multi unit config

// Language selection icons
#define FLAGS_UK_IMG            "/images/_fl_UK.png"
#define FLAGS_LANG_1_IMG        "/images/_fl_lang1.png"
#define FLAGS_LANG_2_IMG        "/images/_fl_lang2.png"
#define FLAGS_LANG_3_IMG        "/images/_fl_lang3.png"
#define FLAGS_LANG_4_IMG        "/images/_fl_lang4.png"
#define FLAGS_LANG_5_IMG        "/images/_fl_lang5.png"
#define FLAGS_LANG_6_IMG        "/images/_fl_lang6.png"
#define FLAGS_LANG_7_IMG        "/images/_fl_lang7.png"
#define FLAGS_LANG_8_IMG        "/images/_fl_lang8.png"
#define FLAGS_LANG_9_IMG        "/images/_fl_lang9.png"
#define FLAGS_LANG_10_IMG       "/images/_fl_lang10.png"
#define FLAGS_LANG_11_IMG       "/images/_fl_lang11.png"
#define FLAGS_LANG_12_IMG       "/images/_fl_lang12.png"
#define FLAGS_LANG_13_IMG       "/images/_fl_lang13.png"
#define FLAGS_LANG_14_IMG       "/images/_fl_lang14.png"
#define FLAGS_LANG_15_IMG       "/images/_fl_lang15.png"
#define FLAGS_LANG_16_IMG       "/images/_fl_lang16.png"
#define FLAGS_LANG_17_IMG       "/images/_fl_lang17.png"

// Translation files
#define LANGUAGE_FILE_LANG_1    "mvc_lang1.qm"
#define LANGUAGE_FILE_LANG_2    "mvc_lang2.qm"
#define LANGUAGE_FILE_LANG_3    "mvc_lang3.qm"
#define LANGUAGE_FILE_LANG_4    "mvc_lang4.qm"
#define LANGUAGE_FILE_LANG_5    "mvc_lang5.qm"
#define LANGUAGE_FILE_LANG_6    "mvc_lang6.qm"
#define LANGUAGE_FILE_LANG_7    "mvc_lang7.qm"
#define LANGUAGE_FILE_LANG_8    "mvc_lang8.qm"
#define LANGUAGE_FILE_LANG_9    "mvc_lang9.qm"
#define LANGUAGE_FILE_LANG_10   "mvc_lang10.qm"
#define LANGUAGE_FILE_LANG_11   "mvc_lang11.qm"
#define LANGUAGE_FILE_LANG_12   "mvc_lang12.qm"
#define LANGUAGE_FILE_LANG_13   "mvc_lang13.qm"
#define LANGUAGE_FILE_LANG_14   "mvc_lang14.qm"
#define LANGUAGE_FILE_LANG_15   "mvc_lang15.qm"
#define LANGUAGE_FILE_LANG_16   "mvc_lang16.qm"
#define LANGUAGE_FILE_LANG_17   "mvc_lang17.qm"


// File handling icons
#define FH_NEW            "/images/file_new.png"
#define FH_RENAME         "/images/file_rename.png"
#define FH_SAVE           "/images/file_save.png"
#define FH_DELETE         "/images/file_delete.png"
#define FH_DELETE_ALL     "/images/file_delete_all.png"
#define FH_SEARCH         "/images/file_search.png"

#define AW_ALARM          "/images/alarm.png"
#define AW_ALARM_BIG      "/images/alarmgroot.png"
#define AW_WARNING        "/images/warning.png"
#define AW_WARNING_BIG    "/images/warninggroot.png"

// Alarm text index
// User alarms (configureerbaar warning/error)
#define ALM_INDEX_FILL_SYSTEM 1
#define ALM_INDEX_LO_LEVEL 2
#define ALM_INDEX_LO_LO_LEVEL 3
#define ALM_INDEX_HI_HI_LEVEL 4
#define ALM_INDEX_MIN_MOTOR_SPEED 5
#define ALM_INDEX_MAX_MOTOR_SPEED 6
#define ALM_INDEX_DEV_QR 7
#define ALM_INDEX_DEV 8
#define ALM_INDEX_MASTER_SLAVE_COM 9

// Warnings (nu nog fixed)
#define ALM_INDEX_VALVE_NOT_CLOSED 50

// System alarms (non-configurable)
#define ALM_INDEX_MOTOR_CON_4WIRE  100
#define ALM_INDEX_MOTOR_CON_6WIRE  101
#define ALM_INDEX_LOADCELL_CON 102

// Text for production display
#define TXT_INDEX_LEARN_BUSY 1
#define TXT_INDEX_LEARN_READY 2

// Config events (for event log)
#define TXT_INDEX_COLOR_PCT_CHANGED 10
#define TXT_INDEX_SHOTWEIGHT_CHANGED 11
#define TXT_INDEX_DOSTIME_CHANGED 12
#define TXT_INDEX_RPM_CHANGED 13
#define TXT_INDEX_EXTCAP_CHANGED 14
#define TXT_INDEX_TACHOMAX_CHANGED 15

#define TXT_INDEX_USER_CHANGED 20
#define TXT_INDEX_FILLSYS_CHANGED 21
#define TXT_INDEX_MOTOR_CHANGED 22
#define TXT_INDEX_DOSTOOL_CHANGED 23
#define TXT_INDEX_GRANULATE_CHANGED 24
#define TXT_INDEX_GRAVIRPM_CHANGED 25
#define TXT_INDEX_SYSTEM_START 26

#define TXT_INDEX_HOPPER_LOW_CHANGED 30
#define TXT_INDEX_HOPPER_LOW_LOW_CHANGED 31

#define TXT_INDEX_MANFILL_START 40
#define TXT_INDEX_FILL_START 41
#define TXT_INDEX_FILLALM_MODE_CHANGED 42
#define TXT_INDEX_FILLCYCLES_CHANGED 43
#define TXT_INDEX_ME_FILLTIME_CHANGED 44
#define TXT_INDEX_ME_ALARMTIME_CHANGED 45
#define TXT_INDEX_MV_FILLTIME_CHANGED 46
#define TXT_INDEX_MV_EMPTYTIME_CHANGED 47

#define TXT_INDEX_CONS1_RESET 50
#define TXT_INDEX_CONS2_RESET 51

#define TXT_INDEX_DEV_ALM_B4 60
#define TXT_INDEX_LOADCELL_CALIBRATED 61
#define TXT_INDEX_CALDEV_SP_CHANGED 62

// Mst Config events texts (for event log)
#define TXT_INDEX_DATETIME_CHANGED 70
#define TXT_INDEX_PRODMODE_CHANGED 71
#define TXT_INDEX_INPTYPE_CHANGED 72
#define TXT_INDEX_LANGUAGE_CHANGED 73
#define TXT_INDEX_AUTOSTART_CHANGED 74
#define TXT_INDEX_RCPENABLE_CHANGED 75
#define TXT_INDEX_STARTUP_USER_CHANGED 76
#define TXT_INDEX_FACTORY_DEFAULTS 77
#define TXT_INDEX_FACTORY_DEFAULTS_SUCCESSFUL 78
#define TXT_INDEX_CONFIRM_FACTORY_DEFAULTS 79

// Other message texts (for screen display)
#define TXT_INDEX_PROD_SETTINGS_ERROR 100
#define TXT_INDEX_DEF_MAT_FILE_NOT_FOUND 101
#define TXT_INDEX_MAT_FILE_NO_READ 102
#define TXT_INDEX_MAT_FILE_NOT_FOUND 103
#define TXT_INDEX_MAT_SAVED 104
#define TXT_INDEX_INTERNAL_LOG_NOT_POSSIBLE 105

static const FileInfo fileInfo = {APP_HEADER_ID,APP_HEADER_APPID,APP_HEADER_APPNAME,APP_HEADER_VERSION};
#endif                                                                          // IMAGEDEF_H

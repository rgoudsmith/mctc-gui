#ifndef LEARNONLINEITEMFRM_H
#define LEARNONLINEITEMFRM_H

#include "Form_QbaseWidget.h"
#include "Form_Keyboard.h"

namespace Ui
{
  class LearnOnlineItemFrm;
}


class LearnOnlineItemFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit LearnOnlineItemFrm(QWidget *parent = 0);
    ~LearnOnlineItemFrm();
    void SetKeyboard(KeyboardFrm *);
    void FormInit(int);

  protected:
    void showEvent(QShowEvent *);

  private:
    Ui::LearnOnlineItemFrm *ui;
    bool eventFilter(QObject *, QEvent *);

    KeyboardFrm * keyInput;
    int formNr;
    int unitIndex;
    bool waitForInput;

  private slots:
    void SaveButtonClicked();
    void StoreMaterialData();
    void StoreRecipeLineCorFac();
    void ShowInput();
    void SetMaterialName();

  public slots:
    void ResultReceived(int);
    void TextReceived(const QString &);
    void MaterialChanged(const QString&, int);
    void DeviceIdChanged(const QString &, int);
};
#endif                                                                          // LEARNONLINEITEMFRM_H

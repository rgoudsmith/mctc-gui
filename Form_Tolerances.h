#ifndef TolerancesFrm_H
#define TolerancesFrm_H

#include <QtGui/QWidget>
#include "Form_QbaseWidget.h"
#include "Form_Selection.h"
#include "Form_Keyboard.h"
#include "Form_NumericInput.h"

namespace Ui
{
  class TolerancesFrm;
}


class TolerancesFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    TolerancesFrm(QWidget *parent = 0);
    ~TolerancesFrm();
    void FormInit();

  protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *);
    void showEvent(QShowEvent *);
    bool eventFilter(QObject *, QEvent *);

  private:
    Ui::TolerancesFrm *m_ui;
    NumericInputFrm * numKeys;
    int lineNr;
    float devAlarm;
    float calibDev;
    void SetButtonImages();

  public slots:
    void DevAlmChanged(float,int);
    void CalDevChanged(float,int);
    void DevAlmClicked();
    void CalDevClicked();
    void UnitValReceived(float);
    void LanguageChanged(eLanguages);
    void OkClicked();
    void CancelClicked();

    signals:
    void DisplayWindow(eWindowIdx);
};
#endif                                                                          // TolerancesFrm_H

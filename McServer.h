/*===========================================================================*
 * File        : McServer.h                                                  *
 * Project     : MVC0012 - MC-TC                                             *
 * Description : McLan TCP/IP server defines.                                *
 *===========================================================================*/

#ifndef MC_SERVER_H
#define MC_SERVER_H

#include <QtNetwork>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include "Glob.h"
#include "Common.h"

/*----- Production status in video codes from MC-30 -----*/
#define VID_PROD_STS_OFF         0                                              /* "Off" */
#define VID_PROD_STS_STANDBY     1                                              /* "Standby" */
#define VID_PROD_STS_DOSING      2                                              /* "Dosing" */
#define VID_PROD_STS_FILLING     3                                              /* "Filling" */
#define VID_PROD_STS_MAN_FILLING 4                                              /* "Manual filling" */

/*----- McLan parameter structure -----*/
struct sTcpIpMsg
{
  byte cmd;                                                                     /* 0=Read; 1=Write */
  byte groupNr;
  byte paramNr;
  float value;
} __attribute__ ((packed));
typedef struct sTcpIpMsg tTcpIpMsg;

class McServer: public QObject
{
  Q_OBJECT
    public:
    McServer(QObject * parent = 0);
    void FormInit();
    ~McServer();
  public slots:
    void NewConnection();
    void TcpRead();
    void TcpDisConnected();
  private:
    QTcpServer server;
    QTcpSocket* client;
    //  global * _gbi;
};
#endif                                                                          // MC_SERVER_H

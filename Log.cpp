/*===========================================================================*
 * File        : LogServer.cpp                                               *
 * Project     : MVC0012 - MC-TC                                             *
 * Description : TCP/IP server.                                              *
 *===========================================================================*/

#include "LogServer.h"
#include <iostream>

typedef unsigned char byte;
typedef unsigned short word;

Server::Server(QObject* parent): QObject(parent)
{
  /*---------------------*/
  /* Sever:: Constructor */
  /*---------------------*/
  connect(&server, SIGNAL(newConnection()), this, SLOT(NewConnection()));

  /*----- Listen to Log port -----*/
  server.listen(QHostAddress::Any, 30014);
}


Server::~Server()
{
  /*--------------------*/
  /* Sever:: Destructor */
  /*--------------------*/
  server.close();
}


void Server::NewConnection()
{
  /*---------------------*/
  /* Setup client socket */
  /*---------------------*/
  client = server.nextPendingConnection();

  /*----- Connect readyReady to TCP Read handler -----*/
  connect(client, SIGNAL(readyRead()), this, SLOT(TcpRead()));
  connect(client, SIGNAL(disconnected()), this, SLOT(TcpDisConnected()));
}


int cnt;

void Server::TcpRead()
{
  /*-----------------------*/
  /* TCP Read data handler */
  /*-----------------------*/
  char rxBuf[10];
  tTcpIpMsg *p;

  QTcpSocket * socket = qobject_cast<QTcpSocket *>(sender());
  if (socket == 0) return;

  /*----- Read data from client -----*/
  if (socket->bytesAvailable() <= sizeof(rxBuf)) {                              /* Check message length */
    socket->read(rxBuf, socket->bytesAvailable());                              /* Message data to rxBuf */

    /*----- Fetch data -----*/
    p = (tTcpIpMsg*)&rxBuf;

    /*----- Group 0, Parm 0 = Fill system mode -----*/
    if ((p->groupNr == 0) && (p->paramNr == 0)) {
      p->value = 1;
    }
    /*----- Group 0, Parm 1 = Fill on/off -----*/
    else if ((p->groupNr == 0) && (p->paramNr == 1)) {
      p->value = 0;
    }
    /*----- Group 1, Parm 0 = Production status -----*/
    else if ((p->groupNr == 1) && (p->paramNr == 0)) {
      p->value = 2;                                                             // Dosing
    }
    /*----- Group 1, Parm 1 = Alarm nr. -----*/
    else if ((p->groupNr == 1) && (p->paramNr == 1)) {
      p->value = 0;
    }
    /*----- Group 1, Parm 3 = Act motor rpm -----*/
    else if ((p->groupNr == 1) && (p->paramNr == 3)) {
      p->value = 51.2;
    }
    /*----- Group 1, Parm 4 = Weight -----*/
    else if ((p->groupNr == 1) && (p->paramNr == 4)) {
      p->value = 1275.3;
    }
    /*----- Group 1, Parm 5 = Col.cap. set -----*/
    else if ((p->groupNr == 1) && (p->paramNr == 5)) {
      p->value = 1.5;
    }
    /*----- Group 1, Parm 6 = Dos time act -----*/
    else if ((p->groupNr == 1) && (p->paramNr == 6)) {
      p->value = 36;
    }
    /*----- Group 1, Parm 7 = Fill valve open/closed -----*/
    else if ((p->groupNr == 1) && (p->paramNr == 7)) {
      p->value = 0;
    }
    /*----- Group 1, Parm 8 = Col.cap. act -----*/
    else if ((p->groupNr == 1) && (p->paramNr == 8)) {
      p->value = 1.75;
    }
    /*----- Group 1, Parm 9 = Col.consumption -----*/
    else if ((p->groupNr == 1) && (p->paramNr == 9)) {
      p->value = 25000;
    }
    /*----- Group 1, Parm 11 = Col day consumption -----*/
    else if ((p->groupNr == 1) && (p->paramNr == 11)) {
      p->value = 750;
    }
    /*----- Group 2, Parm 1 = Color pct set act -----*/
    else if ((p->groupNr == 2) && (p->paramNr == 1)) {
      p->value = 1.5;
    }
    /*----- Group 2, Parm 2 = Shot weight -----*/
    else if ((p->groupNr == 2) && (p->paramNr == 2)) {
      p->value = 10.0;
    }
    /*----- Group 2, Parm 3 = Dos time set -----*/
    else if ((p->groupNr == 2) && (p->paramNr == 3)) {
      p->value = 51.5;
    }
    /*----- Group 2, Parm 4 = RPM set -----*/
    else if ((p->groupNr == 2) && (p->paramNr == 4)) {
      p->value = 50.0;
    }
    /*----- Group 3, Parm 16 = Control mode -----*/
    else if ((p->groupNr == 3) && (p->paramNr == 16)) {
      p->value = 1;                                                             // Gravi
    }
    /*----- Group 3, Parm 17 = Production type -----*/
    else if ((p->groupNr == 3) && (p->paramNr == 17)) {
      p->value = 0;                                                             // Inj. moulding
    }
    /*----- Group 3, Parm 18 = Production ext. mode -----*/
    else if ((p->groupNr == 3) && (p->paramNr == 18)) {
      p->value = 0;                                                             // Ext. relay
    }
    /*----- Group 3, Parm 19 = Production inj. mode -----*/
    else if ((p->groupNr == 3) && (p->paramNr == 19)) {
      p->value = 1;                                                             // Inj. relay
    }
    else {
      qDebug() << ">>>>>>>>>> Msg Error <<<<<<<<<< " << p->groupNr << p->paramNr;
    }

    /*----- Send reply message -----*/
    socket->write(rxBuf,sizeof(tTcpIpMsg));
  }
}


void Server::TcpDisConnected()
{
  /*------------------------*/
  /* TCP Disconnect handler */
  /*------------------------*/
  QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());

  if (socket == 0) return;

  //  client->removeOne(socket);
  socket->deleteLater();
}

#include "Form_RecipeItem.h"
#include "ui_Form_RecipeItem.h"
#include "Rout.h"

RecipeItemFrm::RecipeItemFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::RecipeItemFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);
  unitIdx = -1;
  numInput = 0;
  rcpBuf = 0;

  ui->edColPct->installEventFilter(this);
  ui->edMaterial->installEventFilter(this);
}


RecipeItemFrm::~RecipeItemFrm()
{
  numInput = 0;
  delete ui;
}


void RecipeItemFrm::SetUnitIndex(int idx)
{
  if (idx != unitIdx) {
    unitIdx = idx;
    ui->lbDevID->setText(glob->sysCfg[idx].MC_ID);
    //    ConnectSignals();
    //    UpdateAllItems();
  }
}


void RecipeItemFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  if (numInput != 0) {
    numInput->FormInit();
  }

  ui->lbDevID->setFont(*(glob->baseFont));
  ui->edColPct->setFont(*(glob->baseFont));
  ui->edMaterial->setFont(*(glob->baseFont));

  connect(glob,SIGNAL(SysCfg_IDChanged(QString,int)),this,SLOT(DeviceIdChanged(QString,int)));
}


bool RecipeItemFrm::eventFilter(QObject *o, QEvent *e)
{
  if (e->type() == QEvent::MouseButtonPress) {
    if (numInput != 0) {
      if (o == ui->edColPct) {
        if (rcpBuf != 0) {
          if ((glob->GetUser() >= utTooling) && (glob->mstSts.sysActive == false)) {
            numInput->SetDisplay(true,glob->formats.PercentageFmt.precision,rcpBuf->lines[unitIdx].colPct,0,100,(unitIdx+1000));
            KeyBeep();
            numInput->show();
          }
        }
      }

      if (o == ui->edMaterial) {
        if ((glob->GetUser() >= utTooling) && (glob->mstSts.sysActive == false)) {
          KeyBeep();
          emit MaterialClicked(unitIdx);
        }
      }
    }
  }
  return QBaseWidget::eventFilter(o,e);
}


void RecipeItemFrm::SetColPct(float val)
{
  if (rcpBuf != 0) {
    rcpBuf->lines[unitIdx].colPct = val;
    /*----- Display value on screen -----*/
    QString txt;
    txt = QString::number(val,glob->formats.PercentageFmt.format.toAscii(),glob->formats.PercentageFmt.precision)+QString(" %");
    ui->edColPct->setText(txt);
  }
}


void RecipeItemFrm::UpdateFields()
{
  DeviceIdChanged(glob->sysCfg[unitIdx].MC_ID,unitIdx);
  MaterialReceived(rcpBuf->lines[unitIdx].sMatName,rcpBuf->lines[unitIdx].matIsDefault,unitIdx);
  SetColPct(rcpBuf->lines[unitIdx].colPct);
}


void RecipeItemFrm::SetRecipe(RecipeBufStruct* rcp,int idx)
{
  rcpBuf = rcp;
  unitIdx = idx;

  if (rcpBuf != 0) {
    UpdateFields();
  }
}


void RecipeItemFrm::DeviceIdChanged(const QString &id,int idx)
{

  // test
  unitIdx = idx;

  if (idx == unitIdx) {
    ui->lbDevID->setText(id);

    if (rcpBuf != 0) {
      // test
      MaterialReceived(rcpBuf->lines[idx].sMatName,rcpBuf->lines[idx].matIsDefault,idx);
      SetColPct(rcpBuf->lines[idx].colPct);
    }

  }
}


void RecipeItemFrm::MaterialReceived(const QString &mat,bool def, int idx)
{
  if (idx == unitIdx) {
    if (rcpBuf != 0) {
      rcpBuf->lines[unitIdx].sMatName = mat;
      memset((rcpBuf->lines[unitIdx].matName),0x00,MAX_LENGTH_FILENAME);
      memcpy((rcpBuf->lines[unitIdx].matName),(rcpBuf->lines[unitIdx].sMatName.toAscii()),(rcpBuf->lines[unitIdx].sMatName.length()));
      rcpBuf->lines[unitIdx].matIsDefault = def;
      /*----- Display material name -----*/
      QString material = mat;
      glob->GetDispMaterial(&material);
      if (def) {
        glob->GetDefaultMaterialName(&material,unitIdx,true);
      }

      if ((material.isEmpty()) || (material.isNull())) {
        /*----- material is empty... display [select] -----*/
        ui->edMaterial->setText(RECIPE_SELECT_TEXT);
      }
      else {
        ui->edMaterial->setText(material);
      }
    }
  }
}


void RecipeItemFrm::ValueReceived(float val,int idx)
{
  if ((idx-1000) == unitIdx) {
    SetColPct(val);
  }
}


void RecipeItemFrm::SetNumInput(NumericInputFrm *inp)
{
  if (numInput != 0) {
    disconnect(numInput,SIGNAL(InputValue(float,int)),this,SLOT(ValueReceived(float,int)));
  }

  if (inp != 0) {
    numInput = inp;
    numInput->FormInit();
    connect(numInput,SIGNAL(InputValue(float,int)),this,SLOT(ValueReceived(float,int)));
  }
}

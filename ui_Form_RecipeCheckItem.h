/********************************************************************************
** Form generated from reading UI file 'Form_RecipeCheckItem.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_RECIPECHECKITEM_H
#define UI_FORM_RECIPECHECKITEM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RecipeCheckItemFrm
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *grpBox;
    QHBoxLayout *horizontalLayout;
    QLabel *lbDevId;
    QLabel *lbCurTool;
    QLabel *lbRcpTool;

    void setupUi(QWidget *RecipeCheckItemFrm)
    {
        if (RecipeCheckItemFrm->objectName().isEmpty())
            RecipeCheckItemFrm->setObjectName(QString::fromUtf8("RecipeCheckItemFrm"));
        RecipeCheckItemFrm->resize(400, 92);
        RecipeCheckItemFrm->setCursor(QCursor(Qt::BlankCursor));
        RecipeCheckItemFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout = new QVBoxLayout(RecipeCheckItemFrm);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        grpBox = new QGroupBox(RecipeCheckItemFrm);
        grpBox->setObjectName(QString::fromUtf8("grpBox"));
        grpBox->setTitle(QString::fromUtf8(""));
        horizontalLayout = new QHBoxLayout(grpBox);
        horizontalLayout->setContentsMargins(2, 2, 2, 2);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lbDevId = new QLabel(grpBox);
        lbDevId->setObjectName(QString::fromUtf8("lbDevId"));
        lbDevId->setMaximumSize(QSize(120, 16777215));
        lbDevId->setText(QString::fromUtf8("devID"));

        horizontalLayout->addWidget(lbDevId);

        lbCurTool = new QLabel(grpBox);
        lbCurTool->setObjectName(QString::fromUtf8("lbCurTool"));
        lbCurTool->setMinimumSize(QSize(90, 70));
        lbCurTool->setMaximumSize(QSize(90, 70));
        lbCurTool->setText(QString::fromUtf8("curTool"));

        horizontalLayout->addWidget(lbCurTool);

        lbRcpTool = new QLabel(grpBox);
        lbRcpTool->setObjectName(QString::fromUtf8("lbRcpTool"));
        lbRcpTool->setMinimumSize(QSize(90, 70));
        lbRcpTool->setMaximumSize(QSize(90, 70));
        lbRcpTool->setText(QString::fromUtf8("rcpTool"));

        horizontalLayout->addWidget(lbRcpTool);


        verticalLayout->addWidget(grpBox);


        retranslateUi(RecipeCheckItemFrm);

        QMetaObject::connectSlotsByName(RecipeCheckItemFrm);
    } // setupUi

    void retranslateUi(QWidget *RecipeCheckItemFrm)
    {
        Q_UNUSED(RecipeCheckItemFrm);
    } // retranslateUi

};

namespace Ui {
    class RecipeCheckItemFrm: public Ui_RecipeCheckItemFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_RECIPECHECKITEM_H

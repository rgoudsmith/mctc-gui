#ifndef RECIPECHECKPOPUPFRM_H
#define RECIPECHECKPOPUPFRM_H

#include <QList>
#include <QVBoxLayout>
#include "Form_QbaseWidget.h"
#include "ImageDef.h"

namespace Ui
{
  class RecipeCheckPopupFrm;
}


class RecipeCheckPopupFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit RecipeCheckPopupFrm(QWidget *parent = 0);
    ~RecipeCheckPopupFrm();
    void FormInit();
    bool ProcessCheck();

  protected:
    void changeEvent(QEvent *);

  private:
    Ui::RecipeCheckPopupFrm *ui;
    RecipeStruct recipe;
    void LanguageUpdate();

  private slots:
    void OkClicked();
    void CancelClicked();

  public slots:
    void CheckRecipe(RecipeStruct *);
    void SetRecipe(RecipeStruct *);
    void ApplyRecipe();

    signals:
    void RecipeOK();
};
#endif                                                                          // RECIPECHECKPOPUPFRM_H

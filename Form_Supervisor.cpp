#include "Form_Supervisor.h"
#include "ui_Form_Supervisor.h"
#include "Form_MainMenu.h"

SuperVisorFrm::SuperVisorFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::SuperVisorFrm)
{
  ui->setupUi(this);
  numInput = 0;

  numInput = new NumericInputFrm();
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
  numInput->hide();

  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(ui->btOutput1,SIGNAL(clicked()),this,SLOT(Output1Clicked()));
  connect(ui->btOutput2,SIGNAL(clicked()),this,SLOT(Output2Clicked()));
  connect(ui->btOutput3,SIGNAL(clicked()),this,SLOT(Output3Clicked()));
  connect(ui->btOutput4,SIGNAL(clicked()),this,SLOT(Output4Clicked()));
  connect(ui->btMode,SIGNAL(clicked()),this,SLOT(ManModeClicked()));

  ui->edRpm->installEventFilter(this);
}


SuperVisorFrm::~SuperVisorFrm()
{
  delete numInput;
  delete ui;
}


void SuperVisorFrm::showEvent(QShowEvent *e)
{
  QWidget::showEvent(e);
  // SetOutputStatus for output 0-MAX_DIG_OUT
  for (int i=0; i<MAX_DIG_OUT; i++) {
    SetOutputStatus(i,_gbi->ActUnitIndex());
  }
  // SetInputStatus for input 1-4 (or 0-3)
  for (int i=0; i<MAX_DIG_IN; i++) {
    SetInputStatus(i,_gbi->ActUnitIndex());
  }
  // Set ManIoStatus
  DisplayManualMode();
}


bool SuperVisorFrm::eventFilter(QObject *o, QEvent *e)
{
  if (_gbi != 0) {
    if (e->type() == QEvent::MouseButtonPress) {
      if (o == ui->edRpm) {
        if (numInput != 0) {
          float val = _gbi->sysSts[_gbi->ActUnitIndex()].manIO.motorSpeed;
          numInput->SetDisplay(true,1,val,0,200);
          KeyBeep();
          numInput->show();
        }
      }
    }
  }
  return QBaseWidget::eventFilter(o,e);
}


void SuperVisorFrm::SetGBI(globalItems * gbi)
{
  QBaseWidget::SetGBI(gbi);
  if (numInput != 0) {
    numInput->SetGBI(_gbi);
    connect(numInput,SIGNAL(InputValue(float)),this,SLOT(ValueReceived(float)));
  }

  if (_gbi != 0) {
    _gbi->SetButtonImage(ui->btOk,itSelection,selOk);
    _gbi->SetButtonImage(ui->btOutput1,itGeneral,genUnitOff,0);
    _gbi->SetButtonImage(ui->btOutput2,itGeneral,genUnitOff,0);
    _gbi->SetButtonImage(ui->btOutput3,itGeneral,genUnitOff,0);
    _gbi->SetButtonImage(ui->btOutput4,itGeneral,genUnitOff,0);

    for (int i=0; i<MAX_DIG_OUT; i++) {
      SetOutputStatus(i,_gbi->ActUnitIndex());
    }
    _gbi->SetButtonImage(ui->btMode,itGeneral,genUnitOff,0);
    _gbi->SetLabelImage(ui->lbImg,itGeneral,genLedRed);
    ui->lbSts->setText("OFF");

    SetManualMode(false,_gbi->ActUnitIndex());
    SetOutput(false,0,_gbi->ActUnitIndex());
    SetOutput(false,1,_gbi->ActUnitIndex());
    SetOutput(false,2,_gbi->ActUnitIndex());
    SetOutput(false,3,_gbi->ActUnitIndex());

    ui->lbSts->setFont(*(_gbi->baseFont));

    ui->lbInp1Img->setFont(*(_gbi->baseFont));
    ui->lbInp1Sts->setFont(*(_gbi->baseFont));
    ui->lbInp2Img->setFont(*(_gbi->baseFont));
    ui->lbInp2Sts->setFont(*(_gbi->baseFont));
    ui->lbInp3Img->setFont(*(_gbi->baseFont));
    ui->lbInp3Sts->setFont(*(_gbi->baseFont));
    ui->lbInp4Img->setFont(*(_gbi->baseFont));
    ui->lbInp4Sts->setFont(*(_gbi->baseFont));

    ui->groupBox->setFont(*(_gbi->baseFont));
    ui->groupBox_2->setFont(*(_gbi->baseFont));
    ui->groupBox_3->setFont(*(_gbi->baseFont));
    ui->groupBox_4->setFont(*(_gbi->baseFont));
    ui->groupBox_5->setFont(*(_gbi->baseFont));
    ui->groupBox_6->setFont(*(_gbi->baseFont));
    ui->groupBox_7->setFont(*(_gbi->baseFont));
    ui->groupBox_8->setFont(*(_gbi->baseFont));
    ui->groupBox_9->setFont(*(_gbi->baseFont));
    ui->groupBox_10->setFont(*(_gbi->baseFont));

    ui->label->setFont(*(_gbi->baseFont));
    ui->label_2->setFont(*(_gbi->baseFont));
    ui->label_7->setFont(*(_gbi->baseFont));
    ui->label_13->setFont(*(_gbi->baseFont));

    ui->lbWeight->setFont(*(_gbi->baseFont));
    ui->lbTacho->setFont(*(_gbi->baseFont));

    ui->edRpm->setFont(*(_gbi->baseFont));
    SetRpmText(0,_gbi->ActUnitIndex());
    SetTacho(0,_gbi->ActUnitIndex());

    connect(_gbi,SIGNAL(ComReceived(MsgDataIn,int)),this,SLOT(ComMsgReceived(MsgDataIn,int)));
    connect(_gbi,SIGNAL(SysSts_MassChanged(float,int)),this,SLOT(WeightChanged(float,int)));
    connect(_gbi,SIGNAL(SysSts_TachoChanged(float,int)),this,SLOT(TachoChanged(float,int)));
  }
}


void SuperVisorFrm::SetManualMode(bool active,int idx)
{
  if (_gbi != 0) {
    if (active != _gbi->sysSts[idx].manIO.active) {
      _gbi->sysSts[idx].manIO.active = active;
    }
  }
}


void SuperVisorFrm::DisplayManualMode()
{
  if (_gbi->sysSts[_gbi->ActUnitIndex()].manIO.active) {
    _gbi->SetButtonImage(ui->btMode,itGeneral,genUnitOn,0);
    _gbi->SetLabelImage(ui->lbImg,itGeneral,genLedGreen);
    ui->lbSts->setText("ON");
  }
  else {
    _gbi->SetButtonImage(ui->btMode,itGeneral,genUnitOff,0);
    _gbi->SetLabelImage(ui->lbImg,itGeneral,genLedRed);
    ui->lbSts->setText("OFF");
  }
}


void SuperVisorFrm::DisableOutputs(int idx)
{
  if (_gbi != 0) {
    for (int i=0; i<MAX_DIG_OUT; i++) {
      _gbi->sysSts[idx].manIO.outputs[i] = false;
    }
  }
}


void SuperVisorFrm::SetOutput(bool act,int io,int idx)
{
  if (_gbi != 0) {
    if (_gbi->sysSts[idx].manIO.outputs[io] != act) {
      _gbi->sysSts[idx].manIO.outputs[io] = act;
    }
  }
}


void SuperVisorFrm::ToggleOutput(int io, int idx)
{
  if (_gbi != 0) {
    if (_gbi->sysSts[idx].manIO.outputs[io] == true) {
      SetOutput(false,io,idx);
    }
    else {
      SetOutput(true,io,idx);
    }
  }
}


void SuperVisorFrm::SetRPM(float rpm,int idx)
{
  if (_gbi != 0) {
    if (_gbi->sysSts[idx].manIO.motorSpeed != rpm) {
      _gbi->sysSts[idx].manIO.motorSpeed = rpm;
      SetRpmText(rpm,idx);
    }
  }
}


void SuperVisorFrm::SetRpmText(float val, int)
{
  if (_gbi != 0) {
    QString txt;
    txt = QString::number(val,_gbi->formats.RpmFmt.format.toAscii(),_gbi->formats.RpmFmt.precision) + QString(" ") + _gbi->rpmUnits;
    ui->edRpm->setText(txt);
  }
}


void SuperVisorFrm::SetWeight(float val, int)
{
  if (_gbi != 0) {
    QString txt;
    txt = QString::number(val,_gbi->formats.WeightFmt.format.toAscii(),_gbi->formats.WeightFmt.precision) + QString(" ") + _gbi->weightUnits;
    ui->lbWeight->setText(txt);
  }
}


void SuperVisorFrm::SetTacho(float val, int)
{
  if (_gbi != 0) {
    QString txt;
    txt = QString::number(val,_gbi->formats.TachoFmt.format.toAscii(),_gbi->formats.TachoFmt.precision)+QString(" ") + _gbi->tachoUnits;
    ui->lbTacho->setText(txt);
  }
}


// Slots
void SuperVisorFrm::WeightChanged(float weight, int idx)
{
  if (_gbi != 0) {
    if (idx == _gbi->ActUnitIndex()) {
      SetWeight(weight,idx);
    }
  }
}


void SuperVisorFrm::TachoChanged(float tacho,int idx)
{
  if (_gbi != 0) {
    if (idx == _gbi->ActUnitIndex()) {
      SetTacho(tacho,idx);
    }
  }
}


void SuperVisorFrm::ValueReceived(float val)
{
  if (_gbi != 0) {
    SetRPM(val,_gbi->ActUnitIndex());
  }
}


void SuperVisorFrm::SetButtonImage(QPushButton *btn, int io, int idx)
{
  if (_gbi != 0) {
    bool act;
    if ((io >= 0) && (io < MAX_DIG_OUT)) {
      act = _gbi->sysSts[idx].manIO.outputs[io];
      if (act) {
        _gbi->SetButtonImage(btn,itGeneral,genUnitOn,0);
      }
      else {
        _gbi->SetButtonImage(btn,itGeneral,genUnitOff,0);
      }
    }
  }
}


void SuperVisorFrm::SetLabelOutputImage(QLabel *lbl, int io, int idx)
{
  if (_gbi != 0) {
    bool act;
    bool manAct;

    if ((io >= 0) && (io < MAX_DIG_OUT)) {
      act = _gbi->sysSts[idx].manIO.outputs[io];
      manAct = _gbi->sysSts[idx].manIO.active;

      if ((act) && (manAct)) {
        _gbi->SetLabelImage(lbl,itGeneral,genLedGreen);
      }
      else {
        _gbi->SetLabelImage(lbl,itGeneral,genLedRed);
      }
    }
  }
}


void SuperVisorFrm::SetLabelOutputStatus(QLabel *lbl, int io, int idx)
{
  if (_gbi != 0) {
    bool act;
    bool manAct;

    if ((io >= 0) && (io < MAX_DIG_OUT)) {
      act = _gbi->sysSts[idx].manIO.outputs[io];
      manAct = _gbi->sysSts[idx].manIO.active;

      if ((act) && (manAct)) {
        lbl->setText("ON");
      }
      else {
        lbl->setText("OFF");
      }
    }
  }
}


void SuperVisorFrm::SetOutputStatus(int io, int idx)
{
  if (_gbi != 0) {
    if (io < MAX_DIG_OUT) {
      switch(io) {
        case 0:
          SetButtonImage(ui->btOutput1,io,idx);
          SetLabelOutputImage(ui->lbOut1Img,io,idx);
          SetLabelOutputStatus(ui->lbOut1Sts,io,idx);
          break;
        case 1:
          SetButtonImage(ui->btOutput2,io,idx);
          SetLabelOutputImage(ui->lbOut2Img,io,idx);
          SetLabelOutputStatus(ui->lbOut2Sts,io,idx);
          break;
        case 2:
          SetButtonImage(ui->btOutput3,io,idx);
          SetLabelOutputImage(ui->lbOut3Img,io,idx);
          SetLabelOutputStatus(ui->lbOut3Sts,io,idx);
          break;
        case 3:
          SetButtonImage(ui->btOutput4,io,idx);
          SetLabelOutputImage(ui->lbOut4Img,io,idx);
          SetLabelOutputStatus(ui->lbOut4Sts,io,idx);
          break;
      }
    }
  }
}


void SuperVisorFrm::SetLabelInputImage(QLabel *lbl, int io, int idx)
{
  if (_gbi != 0) {
    bool act;

    if ((io >= 0) && (io < MAX_DIG_IN)) {
      act = _gbi->sysSts[idx].manIO.inputs[io];

      if (act) {
        _gbi->SetLabelImage(lbl,itGeneral,genLedGreen);
      }
      else {
        _gbi->SetLabelImage(lbl,itGeneral,genLedRed);
      }
    }
  }
}


void SuperVisorFrm::SetLabelInputStatus(QLabel *lbl, int io, int idx)
{
  if (_gbi != 0) {
    bool act;

    if ((io >= 0) && (io < MAX_DIG_IN)) {
      act = _gbi->sysSts[idx].manIO.inputs[io];

      if (act) {
        lbl->setText("ON");
      }
      else {
        lbl->setText("OFF");
      }
    }
  }
}


void SuperVisorFrm::SetInputStatus(int io, int idx)
{
  if (_gbi != 0) {
    if (io < MAX_DIG_OUT) {
      switch(io) {
        case 0:
          SetLabelInputImage(ui->lbInp1Img,io,idx);
          SetLabelInputStatus(ui->lbInp1Sts,io,idx);
          break;
        case 1:
          SetLabelInputImage(ui->lbInp2Img,io,idx);
          SetLabelInputStatus(ui->lbInp2Sts,io,idx);
          break;
        case 2:
          SetLabelInputImage(ui->lbInp3Img,io,idx);
          SetLabelInputStatus(ui->lbInp3Sts,io,idx);
          break;
        case 3:
          SetLabelInputImage(ui->lbInp4Img,io,idx);
          SetLabelInputStatus(ui->lbInp4Sts,io,idx);
          break;
      }
    }
  }
}


void SuperVisorFrm::OkClicked()
{
  if (_gbi != 0) {
    if (_gbi->menu != 0) {
      // Disable manual io mode
      _gbi->sysSts[_gbi->ActUnitIndex()].manIO.active = false;
      // close the window
      ((MainMenu*)(_gbi->menu))->DisplayWindow(wiBack);
    }
  }
}


void SuperVisorFrm::Output1Clicked()
{
  if (_gbi != 0) {
    int idx = _gbi->ActUnitIndex();
    ToggleOutput(0,idx);
    SetOutputStatus(0,idx);
  }
}


void SuperVisorFrm::Output2Clicked()
{
  if (_gbi != 0) {
    int idx = _gbi->ActUnitIndex();
    ToggleOutput(1,idx);
    SetOutputStatus(1,idx);
  }
}


void SuperVisorFrm::Output3Clicked()
{
  if (_gbi != 0) {
    int idx = _gbi->ActUnitIndex();
    ToggleOutput(2,idx);
    SetOutputStatus(2,idx);
  }
}


void SuperVisorFrm::Output4Clicked()
{
  if (_gbi != 0) {
    int idx = _gbi->ActUnitIndex();
    ToggleOutput(3,idx);
    SetOutputStatus(3,idx);
  }
}


void SuperVisorFrm::ManModeClicked()
{
  if (_gbi != 0) {
    if (_gbi->sysSts[_gbi->ActUnitIndex()].manIO.active) {
      SetManualMode(false,_gbi->ActUnitIndex());
    }
    else {
      SetManualMode(true,_gbi->ActUnitIndex());
    }
    DisplayManualMode();

    // Update actual status of outputs
    for (int i=0; i<MAX_DIG_OUT; i++) {
      SetOutputStatus(i,_gbi->ActUnitIndex());
    }
  }
}


void SuperVisorFrm::DisplayInputs()
{
  if (_gbi != 0) {
    for (int i=0; i<MAX_DIG_IN; i++) {
      SetInputStatus(i,_gbi->ActUnitIndex());
    }
  }
}


void SuperVisorFrm::ComMsgReceived(MsgDataIn data,int idx)
{
  if (_gbi != 0) {
    //@@ AANPASSEN!!!!!
    // Update loopt nu 3x per seconde voor 4 outputs. (= 12x afbeelding laden uit filesysteem per seconde)
    // Er wordt (nog) niet gekeken naar status wijzigingen, er wordt altijd geupdate.

    // Uitlezen van actuele input status
    _gbi->sysSts[idx].manIO.inputs[0] = ((data.byteBuf[34]) == 0x00 ? false : true);
    _gbi->sysSts[idx].manIO.inputs[1] = ((data.byteBuf[35]) == 0x00 ? false : true);
    _gbi->sysSts[idx].manIO.inputs[2] = ((data.byteBuf[36]) == 0x00 ? false : true);
    _gbi->sysSts[idx].manIO.inputs[3] = ((data.byteBuf[37]) == 0x00 ? false : true);
    // Updaten van de grafische weergave
    DisplayInputs();
  }
}

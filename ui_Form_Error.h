/********************************************************************************
** Form generated from reading UI file 'Form_Error.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_ERROR_H
#define UI_FORM_ERROR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ErrorFrm
{
public:
    QVBoxLayout *verticalLayout_5;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_9;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout;
    QLabel *lbImage;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer;
    QLabel *lbWarn;
    QSpacerItem *verticalSpacer_2;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_4;
    QLabel *lbUnitCap;
    QLabel *lbUnit;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer_5;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_5;
    QHBoxLayout *horizontalLayout_2;
    QLabel *lbDateCap;
    QLabel *lbDate;
    QHBoxLayout *horizontalLayout_3;
    QLabel *lbTimeCap;
    QLabel *lbTime;
    QHBoxLayout *horizontalLayout_6;
    QHBoxLayout *horizontalLayout_4;
    QLabel *lbCodeCap;
    QLabel *lbCode;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer_6;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_3;
    QLabel *lbErrorText;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *btStop;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *btHelp;
    QPushButton *btOk;
    QPushButton *btCancel;
    QPushButton *btHelp2;

    void setupUi(QWidget *ErrorFrm)
    {
        if (ErrorFrm->objectName().isEmpty())
            ErrorFrm->setObjectName(QString::fromUtf8("ErrorFrm"));
        ErrorFrm->setWindowModality(Qt::ApplicationModal);
        ErrorFrm->resize(524, 448);
        ErrorFrm->setCursor(QCursor(Qt::BlankCursor));
        ErrorFrm->setWindowTitle(QString::fromUtf8("Form"));
        ErrorFrm->setStyleSheet(QString::fromUtf8("#groupBox {border: 5px solid; border-radius: 7px; background-color: rgb(255,255,255); border-color: rgb(255, 170, 0);}"));
        verticalLayout_5 = new QVBoxLayout(ErrorFrm);
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        groupBox = new QGroupBox(ErrorFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setTitle(QString::fromUtf8(""));
        horizontalLayout_9 = new QHBoxLayout(groupBox);
        horizontalLayout_9->setSpacing(0);
        horizontalLayout_9->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setContentsMargins(6, 6, 6, 6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(6, -1, 6, -1);
        lbImage = new QLabel(groupBox);
        lbImage->setObjectName(QString::fromUtf8("lbImage"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lbImage->sizePolicy().hasHeightForWidth());
        lbImage->setSizePolicy(sizePolicy);
        lbImage->setMinimumSize(QSize(180, 140));
        lbImage->setMaximumSize(QSize(180, 140));
        lbImage->setText(QString::fromUtf8("!"));
        lbImage->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(lbImage);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        lbWarn = new QLabel(groupBox);
        lbWarn->setObjectName(QString::fromUtf8("lbWarn"));
        QFont font;
        font.setFamily(QString::fromUtf8("Bitstream Charter"));
        font.setPointSize(20);
        font.setBold(true);
        font.setUnderline(true);
        font.setWeight(75);
        font.setStrikeOut(false);
        font.setKerning(true);
        lbWarn->setFont(font);
        lbWarn->setText(QString::fromUtf8("WARNING/ALARM"));

        verticalLayout_2->addWidget(lbWarn);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_4);

        lbUnitCap = new QLabel(groupBox);
        lbUnitCap->setObjectName(QString::fromUtf8("lbUnitCap"));

        verticalLayout->addWidget(lbUnitCap);

        lbUnit = new QLabel(groupBox);
        lbUnit->setObjectName(QString::fromUtf8("lbUnit"));
        lbUnit->setText(QString::fromUtf8("MC_ID"));

        verticalLayout->addWidget(lbUnit);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_3);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_4->addLayout(horizontalLayout);

        verticalSpacer_5 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_4->addItem(verticalSpacer_5);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(6, -1, 6, -1);
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lbDateCap = new QLabel(groupBox);
        lbDateCap->setObjectName(QString::fromUtf8("lbDateCap"));

        horizontalLayout_2->addWidget(lbDateCap);

        lbDate = new QLabel(groupBox);
        lbDate->setObjectName(QString::fromUtf8("lbDate"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lbDate->sizePolicy().hasHeightForWidth());
        lbDate->setSizePolicy(sizePolicy1);
        lbDate->setText(QString::fromUtf8("xx-xx-xxxx"));

        horizontalLayout_2->addWidget(lbDate);


        horizontalLayout_5->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        lbTimeCap = new QLabel(groupBox);
        lbTimeCap->setObjectName(QString::fromUtf8("lbTimeCap"));

        horizontalLayout_3->addWidget(lbTimeCap);

        lbTime = new QLabel(groupBox);
        lbTime->setObjectName(QString::fromUtf8("lbTime"));
        sizePolicy1.setHeightForWidth(lbTime->sizePolicy().hasHeightForWidth());
        lbTime->setSizePolicy(sizePolicy1);
        lbTime->setText(QString::fromUtf8("xx:xx:xx"));

        horizontalLayout_3->addWidget(lbTime);


        horizontalLayout_5->addLayout(horizontalLayout_3);


        verticalLayout_3->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        lbCodeCap = new QLabel(groupBox);
        lbCodeCap->setObjectName(QString::fromUtf8("lbCodeCap"));

        horizontalLayout_4->addWidget(lbCodeCap);

        lbCode = new QLabel(groupBox);
        lbCode->setObjectName(QString::fromUtf8("lbCode"));
        sizePolicy1.setHeightForWidth(lbCode->sizePolicy().hasHeightForWidth());
        lbCode->setSizePolicy(sizePolicy1);
        lbCode->setText(QString::fromUtf8("XX"));

        horizontalLayout_4->addWidget(lbCode);


        horizontalLayout_6->addLayout(horizontalLayout_4);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer);


        verticalLayout_3->addLayout(horizontalLayout_6);


        verticalLayout_4->addLayout(verticalLayout_3);

        verticalSpacer_6 = new QSpacerItem(20, 30, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_4->addItem(verticalSpacer_6);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_3);

        lbErrorText = new QLabel(groupBox);
        lbErrorText->setObjectName(QString::fromUtf8("lbErrorText"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(lbErrorText->sizePolicy().hasHeightForWidth());
        lbErrorText->setSizePolicy(sizePolicy2);
        lbErrorText->setText(QString::fromUtf8("TextLabel"));
        lbErrorText->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        horizontalLayout_8->addWidget(lbErrorText);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_4);


        verticalLayout_4->addLayout(horizontalLayout_8);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(6, -1, 6, 6);
        btStop = new QPushButton(groupBox);
        btStop->setObjectName(QString::fromUtf8("btStop"));
        btStop->setMinimumSize(QSize(90, 70));
        btStop->setMaximumSize(QSize(90, 70));
        btStop->setText(QString::fromUtf8("STOP"));

        horizontalLayout_7->addWidget(btStop);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_2);

        btHelp = new QPushButton(groupBox);
        btHelp->setObjectName(QString::fromUtf8("btHelp"));
        btHelp->setMinimumSize(QSize(90, 70));
        btHelp->setMaximumSize(QSize(90, 70));
        btHelp->setText(QString::fromUtf8("OK ALL"));

        horizontalLayout_7->addWidget(btHelp);

        btOk = new QPushButton(groupBox);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout_7->addWidget(btOk);

        btCancel = new QPushButton(groupBox);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("CANCEL"));

        horizontalLayout_7->addWidget(btCancel);

        btHelp2 = new QPushButton(groupBox);
        btHelp2->setObjectName(QString::fromUtf8("btHelp2"));
        btHelp2->setMinimumSize(QSize(90, 70));
        btHelp2->setMaximumSize(QSize(90, 70));
        btHelp2->setText(QString::fromUtf8("CANCEL ALL"));

        horizontalLayout_7->addWidget(btHelp2);


        verticalLayout_4->addLayout(horizontalLayout_7);


        horizontalLayout_9->addLayout(verticalLayout_4);


        verticalLayout_5->addWidget(groupBox);


        retranslateUi(ErrorFrm);

        QMetaObject::connectSlotsByName(ErrorFrm);
    } // setupUi

    void retranslateUi(QWidget *ErrorFrm)
    {
        lbUnitCap->setText(QApplication::translate("ErrorFrm", "Device ID:", 0, QApplication::UnicodeUTF8));
        lbDateCap->setText(QApplication::translate("ErrorFrm", "Date:", 0, QApplication::UnicodeUTF8));
        lbTimeCap->setText(QApplication::translate("ErrorFrm", "Time:", 0, QApplication::UnicodeUTF8));
        lbCodeCap->setText(QApplication::translate("ErrorFrm", "Code:", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(ErrorFrm);
    } // retranslateUi

};

namespace Ui {
    class ErrorFrm: public Ui_ErrorFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_ERROR_H

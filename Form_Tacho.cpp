#include "Form_Tacho.h"
#include "ui_Form_Tacho.h"
#include "Form_MainMenu.h"
#include "Rout.h"

TachoFrm::TachoFrm(QWidget *parent) :  QBaseWidget(parent), ui(new Ui::TachoFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  editIdx = -1;
  numInput = 0;

  tacho = 0;

  ui->setupUi(this);
  this->setFixedHeight(400);

  ShowSelection(0);

  this->setWindowTitle(tr("Tacho Setup"));

  ui->edManExt->setFocusPolicy(Qt::NoFocus);
  ui->edManTacho->setFocusPolicy(Qt::NoFocus);
  ui->edSyncExt->setFocusPolicy(Qt::NoFocus);

  ui->edManExt->installEventFilter(this);
  ui->edManTacho->installEventFilter(this);
  ui->edSyncExt->installEventFilter(this);

  connect(ui->btCancel,SIGNAL(clicked()),this,SLOT(btCancelClicked()));
  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(btOkClicked()));
  connect(ui->btManual,SIGNAL(clicked()),this,SLOT(btManualClicked()));
  connect(ui->btSynchronise,SIGNAL(clicked()),this,SLOT(btSyncClicked()));

  /*----- Disable windows title bar -----*/
  this->setWindowFlags(Qt::CustomizeWindowHint);
}


TachoFrm::~TachoFrm()
{
  delete ui;
}


void TachoFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      LanguageChange();
      break;
    default:
      break;
  }
}


void TachoFrm::showEvent(QShowEvent *)
{
  ShowSelection(0);

  /*----- Force IDX to unit 0 in TWIN mode -----*/
  if (glob->mstSts.isTwin) {
    glob->mstSts.actUnitIndex = 0;
  }

  if (glob->sysCfg[glob->ActUnitIndex()].GraviRpm == grRpm) {
    ui->lbManExt->setText(glob->rpmUnits.toUpper());
  }
  else {
    ui->lbManExt->setText(tr("Extruder Cap."));
  }
  ui->lbSyncExt->setText(ui->lbManExt->text());

  if ((glob->menu) != 0) {
    ((MainMenu*)(glob->menu))->FoldMenu();
  }
}


bool TachoFrm::eventFilter(QObject *obj, QEvent * ev)
{
  double val;

  if (ev->type() == QEvent::MouseButtonPress) {
    int idx = glob->ActUnitIndex();

    if (obj ==  ui->edManTacho) {
      val = glob->sysCfg[idx].extrusion.tachoMax;
      editIdx = 0;
    }
    else {
      if (obj == ui->edManExt) {
        if (glob->sysCfg[idx].GraviRpm == grGravi) {
          val = glob->sysCfg[idx].extrusion.extCapacity;
        }
        else {
          val = glob->sysCfg[idx].setRpm;
        }
        editIdx = 1;
      }
      else {
        if (obj == ui->edSyncExt) {
          if (glob->sysCfg[idx].GraviRpm == grGravi) {
            val = curExtCap;
          }
          else {
            //                            val = glob->sysCfg[idx].setRpm;
            val = curExtCap;
          }
          editIdx = 2;
        }
        else {
          editIdx = -1;
        }
      }
    }

    if ((editIdx != -1) && (numInput != 0)) {
      if (editIdx != 0) {
        if (glob->sysCfg[idx].GraviRpm == grGravi) {
          numInput->SetDisplay(true,glob->formats.ExtruderFmt.precision,val,0.1,INPUT_MAX_EXTCAP);
        }
        else {
          numInput->SetDisplay(true,glob->formats.RpmFmt.precision,val,0,INPUT_MAX_RPM);
        }
      }
      else {
        numInput->SetDisplay(true,glob->formats.TachoFmt.precision,val,0.1,INPUT_MAX_TACHO);
      }
      KeyBeep();
      numInput->show();
    }
  }
  return QWidget::eventFilter(obj,ev);
}


void TachoFrm::LanguageChange()
{
  if (glob->sysCfg[glob->ActUnitIndex()].GraviRpm == grRpm) {
    ui->lbManExt->setText(glob->rpmUnits.toUpper());
    ui->lbSyncExt->setText(glob->rpmUnits.toUpper());
  }
}


void TachoFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  glob->SetButtonImage(ui->btCancel,itSelection,selCancel);
  glob->SetButtonImage(ui->btOk,itSelection,selOk);

  ui->lbTachoType->setFont(*(glob->baseFont));
  ui->lbManExt->setFont(*(glob->baseFont));
  ui->lbSyncExt->setFont(*(glob->baseFont));
  ui->lbManIs->setFont(*(glob->baseFont));
  ui->lbManTacho->setFont(*(glob->baseFont));
  ui->lbActTacho->setFont(*(glob->baseFont));
  ui->lbActTacho_2->setFont(*(glob->baseFont));
  ui->lbActExtCap->setFont(*(glob->baseFont));
  ui->label_7->setFont(*(glob->baseFont));
  ui->label_8->setFont(*(glob->baseFont));
  ui->label_9->setFont(*(glob->baseFont));
  ui->lbNew->setFont(*(glob->baseFont));
  ui->lbActual->setFont(*(glob->baseFont));

  ui->edManExt->setFont(*(glob->baseFont));
  ui->edManTacho->setFont(*(glob->baseFont));
  ui->edSyncExt->setFont(*(glob->baseFont));
  ui->btManual->setFont(*(glob->baseFont));
  ui->btSynchronise->setFont(*(glob->baseFont));

  connect(glob,SIGNAL(SysSts_TachoChanged(float,int)),this,SLOT(SysSts_TachoChanged(float,int)));
  connect(glob,SIGNAL(SysSts_ExtCapChanged(float,int)),this,SLOT(SysSts_ExtCapChanged(float,int)));
  connect(glob,SIGNAL(SysSts_RpmChanged(float,int)),this,SLOT(SysSts_RpmChanged(float,int)));
}


void TachoFrm::SetNumInput(NumericInputFrm *numInp)
{
  this->numInput = numInp;
  connect(numInput,SIGNAL(InputValue(float)),this,SLOT(ValueReceived(float)));
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
}


void TachoFrm::ShowSelection(int idx)
{
  selIdx = idx;
  int unitIdx = glob->ActUnitIndex();
  QString txt;
  switch(idx) {
    case 1:                                                                     // MANUAL
      // Values for edit fields
      if (glob->sysCfg[unitIdx].GraviRpm == grRpm) {
        txt = QString::number(extCapacity,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision) + QString(" ");
        txt += glob->rpmUnits;
      }
      else {
        txt = QString::number(extCapacity,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision) + QString(" ");
        txt += glob->extrUnits;
      }
      ui->edManExt->setText(txt);
      ui->edManTacho->setText(QString::number(tacho,glob->formats.TachoFmt.format.toAscii(),glob->formats.TachoFmt.precision) + QString(" ")+glob->tachoUnits);

      ui->gbSelection->setVisible(false);
      ui->gbManualTacho->setVisible(true);
      ui->gbSyncTacho->setVisible(false);
      ui->lbTachoType->setText(tr("Manual"));
      break;

    case 2:                                                                     // SYNCHRONISE
      if (glob->sysCfg[unitIdx].GraviRpm == grRpm) {
        curExtCap = glob->sysCfg[unitIdx].setRpm;
        // curExtCap = glob->sysSts[unitIdx].rpm; // 2011-06-30
        //                    if (curExtCap == 0)
        //                    {
        //                        curExtCap = glob->sysCfg[unitIdx].setRpm;
        //                    }
        txt = QString::number(curExtCap,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision) + QString(" ");
        txt += glob->rpmUnits;
      }
      else {
        curExtCap = glob->sysCfg[unitIdx].extrusion.extCapacity;
        // curExtCap = glob->sysSts[unitIdx].extrusion.extCapacityAct; // 2011-06-30
        // Mogelijke uitbreiding voor wanneer unit in OFF status is.
        //                    if (glob->sysSts[unitIdx].MCStatus <= mcsStandby)
        //                    {
        //                        if (curExtCap == 0)
        //                        {
        //                            curExtCap = glob->sysCfg[unitIdx].extrusion.extCapacity;
        //                        }
        //                    }
        txt = QString::number(curExtCap,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision) + QString(" ");
        txt += glob->extrUnits;
      }
      ui->edSyncExt->setText(txt);
      ui->lbActExtCap->setText(txt);

      // Visual update is done automatically, local variable needs to be set once (in case actTacho does not change during input)
      tacho = glob->sysSts[unitIdx].extrusion.tachoAct;
      SysSts_TachoChanged(tacho,unitIdx);

      ui->gbSelection->setVisible(false);
      ui->gbManualTacho->setVisible(false);
      ui->gbSyncTacho->setVisible(true);
      ui->lbTachoType->setText(tr("Synchronise"));
      break;

    default:
      selIdx = 0;
      ui->gbSelection->setVisible(true);
      ui->gbManualTacho->setVisible(false);
      ui->gbSyncTacho->setVisible(false);
      ui->lbTachoType->setText("");

      tacho = glob->sysCfg[unitIdx].extrusion.tachoMax;
      if (glob->sysCfg[unitIdx].GraviRpm == grGravi) {
        extCapacity = glob->sysCfg[unitIdx].extrusion.extCapacity;
      }
      else {
        extCapacity = glob->sysCfg[unitIdx].setRpm;
      }
      break;
  }
}


void TachoFrm::btCancelClicked()
{
  close();
  ShowSelection(0);
}


void TachoFrm::btOkClicked()
{
  int idx = glob->ActUnitIndex();
  if (glob->sysCfg[idx].GraviRpm == grGravi) {
    glob->SysCfg_SetExtCap(extCapacity,idx);
  }
  else {
    glob->SysCfg_SetRpm(extCapacity,idx);
  }
  glob->SysCfg_SetMaxTacho(tacho,idx);
  close();
  ShowSelection(0);
}


void TachoFrm::btManualClicked()
{
  ShowSelection(1);
}


void TachoFrm::btSyncClicked()
{
  ShowSelection(2);
}


void TachoFrm::ValueReceived(float val)
{
  int idx = glob->ActUnitIndex();
  QString txt;
  switch (editIdx) {
    case 0:                                                                     // ManTacho
      tacho = val;
      ui->edManTacho->setText(QString::number(tacho,glob->formats.TachoFmt.format.toAscii(),glob->formats.TachoFmt.precision) + QString(" ")+glob->tachoUnits);
      break;
    case 1:                                                                     // Man Ext
      extCapacity = val;

      if (glob->sysCfg[idx].GraviRpm == grRpm) {
        txt = QString::number(extCapacity,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision) + QString(" ");
        txt += glob->rpmUnits;
      }
      else {
        txt = QString::number(extCapacity,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision) + QString(" ");
        txt += glob->extrUnits;
      }
      ui->edManExt->setText(txt);
      break;
    case 2:                                                                     // Sync Ext
      extCapacity = val;

      if (glob->sysCfg[idx].GraviRpm == grRpm) {
        txt = QString::number(extCapacity,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision) + QString(" ");
        txt += glob->rpmUnits;
      }
      else {
        txt = QString::number(extCapacity,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision) + QString(" ");
        txt += glob->extrUnits;
      }
      ui->edSyncExt->setText(txt);
      break;
  }
}


void TachoFrm::SysSts_TachoChanged(float _tacho, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    ui->lbActTacho->setText("> "+QString::number(_tacho,glob->formats.TachoFmt.format.toAscii(),glob->formats.TachoFmt.precision)+ QString(" ")+glob->tachoUnits + " <");
    ui->lbActTacho_2->setText(ui->lbActTacho->text());
    // Update (max)tacho value with act tacho if mode == SYNCHRONIZE
    if (selIdx == 2) tacho = _tacho;
  }
}


void TachoFrm::SysSts_ExtCapChanged(float _extCap, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    if (glob->sysCfg[idx].GraviRpm == grGravi) {
      QString txt;
      txt = QString::number(_extCap,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision) + " " + glob->extrUnits;
      ui->lbActExtCap->setText(txt);
    }
  }
}


void TachoFrm::SysSts_RpmChanged(float rpm, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    if (glob->sysCfg[idx].GraviRpm == grRpm) {
      QString txt;
      txt = QString::number(rpm,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision)+" "+glob->rpmUnits;
      ui->lbActExtCap->setText(txt);
    }
  }
}

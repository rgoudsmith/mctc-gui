/********************************************************************************
** Form generated from reading UI file 'Form_Chart.ui'
**
** Created: Thu Apr 12 13:50:29 2012
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_CHART_H
#define UI_FORM_CHART_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ChartFrm
{
public:

    void setupUi(QWidget *ChartFrm)
    {
        if (ChartFrm->objectName().isEmpty())
            ChartFrm->setObjectName(QString::fromUtf8("ChartFrm"));
        ChartFrm->resize(494, 386);
        ChartFrm->setCursor(QCursor(Qt::BlankCursor));
        ChartFrm->setWindowTitle(QString::fromUtf8("Form"));

        retranslateUi(ChartFrm);

        QMetaObject::connectSlotsByName(ChartFrm);
    } // setupUi

    void retranslateUi(QWidget *ChartFrm)
    {
        Q_UNUSED(ChartFrm);
    } // retranslateUi

};

namespace Ui {
    class ChartFrm: public Ui_ChartFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_CHART_H

/********************************************************************************
** Form generated from reading UI file 'Form_RegrindConfig.ui'
**
** Created: Wed Feb 6 11:12:26 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_REGRINDCONFIG_H
#define UI_FORM_REGRINDCONFIG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RegrindConfigFrm
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QGroupBox *gbRegLevelSettings;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *horizontalSpacer_3;
    QLabel *lbLevels;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_5;
    QLabel *lbLevel0;
    QSpacerItem *horizontalSpacer_13;
    QLineEdit *edLevel0;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_9;
    QLabel *lbLevel1;
    QSpacerItem *horizontalSpacer_14;
    QLineEdit *edLevel1;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_7;
    QLabel *lbLevel2;
    QSpacerItem *horizontalSpacer_15;
    QLineEdit *edLevel2;
    QSpacerItem *verticalSpacer_4;
    QHBoxLayout *horizontalLayout_8;
    QLabel *lbLevel3;
    QSpacerItem *horizontalSpacer_16;
    QLineEdit *edLevel3;
    QSpacerItem *verticalSpacer_5;
    QHBoxLayout *horizontalLayout_6;
    QLabel *lbLevel4;
    QSpacerItem *horizontalSpacer_17;
    QLineEdit *edLevel4;
    QGroupBox *gbRegSettings;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_5;
    QLabel *lbSettings;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *verticalSpacer_6;
    QHBoxLayout *horizontalLayout_14;
    QLabel *lbRegLevelHigh;
    QSpacerItem *horizontalSpacer_7;
    QLineEdit *edRegHighLevel;
    QSpacerItem *verticalSpacer_7;
    QHBoxLayout *horizontalLayout_15;
    QLabel *lbRegLevel;
    QSpacerItem *horizontalSpacer_8;
    QLineEdit *edRegLevel;
    QSpacerItem *verticalSpacer_8;
    QHBoxLayout *horizontalLayout_13;
    QLabel *lbFillTime;
    QSpacerItem *horizontalSpacer_9;
    QLineEdit *edFillTime;
    QSpacerItem *verticalSpacer_9;
    QHBoxLayout *horizontalLayout_12;
    QLabel *lbFillIntTime;
    QSpacerItem *horizontalSpacer_10;
    QLineEdit *edFillIntTime;
    QSpacerItem *verticalSpacer_10;
    QHBoxLayout *horizontalLayout_11;
    QLabel *lbRegPctExtra;
    QSpacerItem *horizontalSpacer_11;
    QLineEdit *edRegPctExtra;
    QSpacerItem *verticalSpacer_11;
    QHBoxLayout *horizontalLayout_4;
    QLabel *lbRegPctB1;
    QSpacerItem *horizontalSpacer_12;
    QLineEdit *edRegPctB1;
    QPushButton *btOk;

    void setupUi(QWidget *RegrindConfigFrm)
    {
        if (RegrindConfigFrm->objectName().isEmpty())
            RegrindConfigFrm->setObjectName(QString::fromUtf8("RegrindConfigFrm"));
        RegrindConfigFrm->resize(800, 530);
        RegrindConfigFrm->setMaximumSize(QSize(800, 16777215));
        RegrindConfigFrm->setCursor(QCursor(Qt::CrossCursor));
        horizontalLayoutWidget = new QWidget(RegrindConfigFrm);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 0, 771, 80));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        label = new QLabel(horizontalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(35);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        gbRegLevelSettings = new QGroupBox(RegrindConfigFrm);
        gbRegLevelSettings->setObjectName(QString::fromUtf8("gbRegLevelSettings"));
        gbRegLevelSettings->setGeometry(QRect(450, 110, 180, 361));
        verticalLayout = new QVBoxLayout(gbRegLevelSettings);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_3);

        lbLevels = new QLabel(gbRegLevelSettings);
        lbLevels->setObjectName(QString::fromUtf8("lbLevels"));

        horizontalLayout_10->addWidget(lbLevels);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout_10);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        lbLevel0 = new QLabel(gbRegLevelSettings);
        lbLevel0->setObjectName(QString::fromUtf8("lbLevel0"));

        horizontalLayout_5->addWidget(lbLevel0);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_13);

        edLevel0 = new QLineEdit(gbRegLevelSettings);
        edLevel0->setObjectName(QString::fromUtf8("edLevel0"));
        edLevel0->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_5->addWidget(edLevel0);


        verticalLayout->addLayout(horizontalLayout_5);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        lbLevel1 = new QLabel(gbRegLevelSettings);
        lbLevel1->setObjectName(QString::fromUtf8("lbLevel1"));

        horizontalLayout_9->addWidget(lbLevel1);

        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_14);

        edLevel1 = new QLineEdit(gbRegLevelSettings);
        edLevel1->setObjectName(QString::fromUtf8("edLevel1"));
        edLevel1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_9->addWidget(edLevel1);


        verticalLayout->addLayout(horizontalLayout_9);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_3);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        lbLevel2 = new QLabel(gbRegLevelSettings);
        lbLevel2->setObjectName(QString::fromUtf8("lbLevel2"));

        horizontalLayout_7->addWidget(lbLevel2);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_15);

        edLevel2 = new QLineEdit(gbRegLevelSettings);
        edLevel2->setObjectName(QString::fromUtf8("edLevel2"));
        edLevel2->setEnabled(true);
        edLevel2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_7->addWidget(edLevel2);


        verticalLayout->addLayout(horizontalLayout_7);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_4);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        lbLevel3 = new QLabel(gbRegLevelSettings);
        lbLevel3->setObjectName(QString::fromUtf8("lbLevel3"));

        horizontalLayout_8->addWidget(lbLevel3);

        horizontalSpacer_16 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_16);

        edLevel3 = new QLineEdit(gbRegLevelSettings);
        edLevel3->setObjectName(QString::fromUtf8("edLevel3"));
        edLevel3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_8->addWidget(edLevel3);


        verticalLayout->addLayout(horizontalLayout_8);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        lbLevel4 = new QLabel(gbRegLevelSettings);
        lbLevel4->setObjectName(QString::fromUtf8("lbLevel4"));

        horizontalLayout_6->addWidget(lbLevel4);

        horizontalSpacer_17 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_17);

        edLevel4 = new QLineEdit(gbRegLevelSettings);
        edLevel4->setObjectName(QString::fromUtf8("edLevel4"));
        edLevel4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_6->addWidget(edLevel4);


        verticalLayout->addLayout(horizontalLayout_6);

        gbRegSettings = new QGroupBox(RegrindConfigFrm);
        gbRegSettings->setObjectName(QString::fromUtf8("gbRegSettings"));
        gbRegSettings->setGeometry(QRect(20, 110, 321, 371));
        verticalLayout_2 = new QVBoxLayout(gbRegSettings);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);

        lbSettings = new QLabel(gbRegSettings);
        lbSettings->setObjectName(QString::fromUtf8("lbSettings"));

        horizontalLayout_2->addWidget(lbSettings);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);


        verticalLayout_2->addLayout(horizontalLayout_2);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_6);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        lbRegLevelHigh = new QLabel(gbRegSettings);
        lbRegLevelHigh->setObjectName(QString::fromUtf8("lbRegLevelHigh"));

        horizontalLayout_14->addWidget(lbRegLevelHigh);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_7);

        edRegHighLevel = new QLineEdit(gbRegSettings);
        edRegHighLevel->setObjectName(QString::fromUtf8("edRegHighLevel"));
        edRegHighLevel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_14->addWidget(edRegHighLevel);


        verticalLayout_2->addLayout(horizontalLayout_14);

        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_7);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        lbRegLevel = new QLabel(gbRegSettings);
        lbRegLevel->setObjectName(QString::fromUtf8("lbRegLevel"));

        horizontalLayout_15->addWidget(lbRegLevel);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_8);

        edRegLevel = new QLineEdit(gbRegSettings);
        edRegLevel->setObjectName(QString::fromUtf8("edRegLevel"));
        edRegLevel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_15->addWidget(edRegLevel);


        verticalLayout_2->addLayout(horizontalLayout_15);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_8);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        lbFillTime = new QLabel(gbRegSettings);
        lbFillTime->setObjectName(QString::fromUtf8("lbFillTime"));

        horizontalLayout_13->addWidget(lbFillTime);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_9);

        edFillTime = new QLineEdit(gbRegSettings);
        edFillTime->setObjectName(QString::fromUtf8("edFillTime"));
        edFillTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_13->addWidget(edFillTime);


        verticalLayout_2->addLayout(horizontalLayout_13);

        verticalSpacer_9 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_9);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        lbFillIntTime = new QLabel(gbRegSettings);
        lbFillIntTime->setObjectName(QString::fromUtf8("lbFillIntTime"));

        horizontalLayout_12->addWidget(lbFillIntTime);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_10);

        edFillIntTime = new QLineEdit(gbRegSettings);
        edFillIntTime->setObjectName(QString::fromUtf8("edFillIntTime"));
        edFillIntTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_12->addWidget(edFillIntTime);


        verticalLayout_2->addLayout(horizontalLayout_12);

        verticalSpacer_10 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        lbRegPctExtra = new QLabel(gbRegSettings);
        lbRegPctExtra->setObjectName(QString::fromUtf8("lbRegPctExtra"));

        horizontalLayout_11->addWidget(lbRegPctExtra);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_11);

        edRegPctExtra = new QLineEdit(gbRegSettings);
        edRegPctExtra->setObjectName(QString::fromUtf8("edRegPctExtra"));
        edRegPctExtra->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_11->addWidget(edRegPctExtra);


        verticalLayout_2->addLayout(horizontalLayout_11);

        verticalSpacer_11 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_11);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        lbRegPctB1 = new QLabel(gbRegSettings);
        lbRegPctB1->setObjectName(QString::fromUtf8("lbRegPctB1"));

        horizontalLayout_4->addWidget(lbRegPctB1);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_12);

        edRegPctB1 = new QLineEdit(gbRegSettings);
        edRegPctB1->setObjectName(QString::fromUtf8("edRegPctB1"));
        edRegPctB1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(edRegPctB1);


        verticalLayout_2->addLayout(horizontalLayout_4);

        btOk = new QPushButton(RegrindConfigFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setGeometry(QRect(680, 400, 90, 70));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        retranslateUi(RegrindConfigFrm);

        QMetaObject::connectSlotsByName(RegrindConfigFrm);
    } // setupUi

    void retranslateUi(QWidget *RegrindConfigFrm)
    {
        RegrindConfigFrm->setWindowTitle(QApplication::translate("RegrindConfigFrm", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("RegrindConfigFrm", "Regrind Configuration", 0, QApplication::UnicodeUTF8));
        lbLevels->setText(QApplication::translate("RegrindConfigFrm", "Levels", 0, QApplication::UnicodeUTF8));
        lbLevel0->setText(QApplication::translate("RegrindConfigFrm", "Level 0", 0, QApplication::UnicodeUTF8));
        lbLevel1->setText(QApplication::translate("RegrindConfigFrm", "Level 1", 0, QApplication::UnicodeUTF8));
        lbLevel2->setText(QApplication::translate("RegrindConfigFrm", "Level 2", 0, QApplication::UnicodeUTF8));
        lbLevel3->setText(QApplication::translate("RegrindConfigFrm", "Level 3", 0, QApplication::UnicodeUTF8));
        lbLevel4->setText(QApplication::translate("RegrindConfigFrm", "Level 4", 0, QApplication::UnicodeUTF8));
        lbSettings->setText(QApplication::translate("RegrindConfigFrm", "Settings", 0, QApplication::UnicodeUTF8));
        lbRegLevelHigh->setText(QApplication::translate("RegrindConfigFrm", "High level", 0, QApplication::UnicodeUTF8));
        lbRegLevel->setText(QApplication::translate("RegrindConfigFrm", "Regrind level", 0, QApplication::UnicodeUTF8));
        lbFillTime->setText(QApplication::translate("RegrindConfigFrm", "Fill time", 0, QApplication::UnicodeUTF8));
        lbFillIntTime->setText(QApplication::translate("RegrindConfigFrm", "Fill interval time", 0, QApplication::UnicodeUTF8));
        lbRegPctExtra->setText(QApplication::translate("RegrindConfigFrm", "Extra regrind", 0, QApplication::UnicodeUTF8));
        lbRegPctB1->setText(QApplication::translate("RegrindConfigFrm", "Regrind B1", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class RegrindConfigFrm: public Ui_RegrindConfigFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_REGRINDCONFIG_H

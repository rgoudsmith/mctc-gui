/****************************************************************************
** Meta object code from reading C++ file 'Form_SystemCalibration.h'
**
** Created: Wed Feb 6 11:15:15 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_SystemCalibration.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_SystemCalibration.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SystemCalibrationFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      22,   21,   21,   21, 0x05,

 // slots: signature, parameters, type, tag, flags
      50,   21,   21,   21, 0x08,
      64,   21,   21,   21, 0x08,
      91,   21,   21,   21, 0x08,
     107,   21,   21,   21, 0x08,
     123,   21,   21,   21, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_SystemCalibrationFrm[] = {
    "SystemCalibrationFrm\0\0SendCalCommand(eCalCommand)\0"
    "ButtonClick()\0CalStateChanged(eCalState)\0"
    "timerInterval()\0CancelClicked()\0"
    "CalStatusReceived(eCalStatus)\0"
};

const QMetaObject SystemCalibrationFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_SystemCalibrationFrm,
      qt_meta_data_SystemCalibrationFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SystemCalibrationFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SystemCalibrationFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SystemCalibrationFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SystemCalibrationFrm))
        return static_cast<void*>(const_cast< SystemCalibrationFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int SystemCalibrationFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: SendCalCommand((*reinterpret_cast< eCalCommand(*)>(_a[1]))); break;
        case 1: ButtonClick(); break;
        case 2: CalStateChanged((*reinterpret_cast< eCalState(*)>(_a[1]))); break;
        case 3: timerInterval(); break;
        case 4: CancelClicked(); break;
        case 5: CalStatusReceived((*reinterpret_cast< eCalStatus(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void SystemCalibrationFrm::SendCalCommand(eCalCommand _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

/********************************************************************************
** Form generated from reading UI file 'Form_Consumption.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_CONSUMPTION_H
#define UI_FORM_CONSUMPTION_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ConsumptionFrm
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QWidget *horizontalLayoutWidget_6;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_13;
    QLabel *lblDeviceId;
    QSpacerItem *horizontalSpacer_14;
    QLabel *lblUnitName;
    QSpacerItem *horizontalSpacer_15;
    QWidget *horizontalLayoutWidget_4;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_8;
    QLabel *txtTotalConsumption;
    QLabel *lblTotalConsumption;
    QSpacerItem *horizontalSpacer_9;
    QWidget *horizontalLayoutWidget_5;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_10;
    QLabel *lblResetTotal;
    QPushButton *btTotalReset;
    QSpacerItem *horizontalSpacer_12;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_7;
    QLabel *txtBatchConsumption;
    QLabel *lblBatchConsumption;
    QSpacerItem *horizontalSpacer_6;
    QFrame *line;
    QFrame *line_2;
    QFrame *line_3;
    QFrame *line_4;
    QFrame *line_5;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_3;
    QLabel *lblResetBatch;
    QPushButton *btBatchReset;
    QSpacerItem *horizontalSpacer_5;

    void setupUi(QWidget *ConsumptionFrm)
    {
        if (ConsumptionFrm->objectName().isEmpty())
            ConsumptionFrm->setObjectName(QString::fromUtf8("ConsumptionFrm"));
        ConsumptionFrm->resize(800, 413);
        horizontalLayoutWidget = new QWidget(ConsumptionFrm);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 0, 771, 80));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        label = new QLabel(horizontalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(35);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        horizontalLayoutWidget_6 = new QWidget(ConsumptionFrm);
        horizontalLayoutWidget_6->setObjectName(QString::fromUtf8("horizontalLayoutWidget_6"));
        horizontalLayoutWidget_6->setGeometry(QRect(280, 100, 251, 80));
        horizontalLayout_8 = new QHBoxLayout(horizontalLayoutWidget_6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_13);

        lblDeviceId = new QLabel(horizontalLayoutWidget_6);
        lblDeviceId->setObjectName(QString::fromUtf8("lblDeviceId"));
        QFont font1;
        font1.setPointSize(15);
        lblDeviceId->setFont(font1);

        horizontalLayout_8->addWidget(lblDeviceId);

        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_14);

        lblUnitName = new QLabel(horizontalLayoutWidget_6);
        lblUnitName->setObjectName(QString::fromUtf8("lblUnitName"));
        lblUnitName->setFont(font1);
        lblUnitName->setText(QString::fromUtf8("TextLabel"));

        horizontalLayout_8->addWidget(lblUnitName);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_15);

        horizontalLayoutWidget_4 = new QWidget(ConsumptionFrm);
        horizontalLayoutWidget_4->setObjectName(QString::fromUtf8("horizontalLayoutWidget_4"));
        horizontalLayoutWidget_4->setGeometry(QRect(20, 300, 551, 80));
        horizontalLayout_5 = new QHBoxLayout(horizontalLayoutWidget_4);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_8);

        txtTotalConsumption = new QLabel(horizontalLayoutWidget_4);
        txtTotalConsumption->setObjectName(QString::fromUtf8("txtTotalConsumption"));
        txtTotalConsumption->setFont(font1);

        horizontalLayout_5->addWidget(txtTotalConsumption);

        lblTotalConsumption = new QLabel(horizontalLayoutWidget_4);
        lblTotalConsumption->setObjectName(QString::fromUtf8("lblTotalConsumption"));
        lblTotalConsumption->setFont(font1);
        lblTotalConsumption->setText(QString::fromUtf8("TextLabel"));

        horizontalLayout_5->addWidget(lblTotalConsumption);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_9);

        horizontalLayoutWidget_5 = new QWidget(ConsumptionFrm);
        horizontalLayoutWidget_5->setObjectName(QString::fromUtf8("horizontalLayoutWidget_5"));
        horizontalLayoutWidget_5->setGeometry(QRect(580, 300, 191, 80));
        horizontalLayout_6 = new QHBoxLayout(horizontalLayoutWidget_5);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_10);

        lblResetTotal = new QLabel(horizontalLayoutWidget_5);
        lblResetTotal->setObjectName(QString::fromUtf8("lblResetTotal"));
        lblResetTotal->setFont(font1);

        horizontalLayout_6->addWidget(lblResetTotal);

        btTotalReset = new QPushButton(horizontalLayoutWidget_5);
        btTotalReset->setObjectName(QString::fromUtf8("btTotalReset"));
        btTotalReset->setMinimumSize(QSize(90, 70));
        btTotalReset->setMaximumSize(QSize(90, 70));
        btTotalReset->setText(QString::fromUtf8("PushButton"));

        horizontalLayout_6->addWidget(btTotalReset);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_12);

        horizontalLayoutWidget_2 = new QWidget(ConsumptionFrm);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(18, 200, 551, 81));
        horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_7);

        txtBatchConsumption = new QLabel(horizontalLayoutWidget_2);
        txtBatchConsumption->setObjectName(QString::fromUtf8("txtBatchConsumption"));
        txtBatchConsumption->setFont(font1);

        horizontalLayout_3->addWidget(txtBatchConsumption);

        lblBatchConsumption = new QLabel(horizontalLayoutWidget_2);
        lblBatchConsumption->setObjectName(QString::fromUtf8("lblBatchConsumption"));
        lblBatchConsumption->setFont(font1);
        lblBatchConsumption->setText(QString::fromUtf8("TextLabel"));

        horizontalLayout_3->addWidget(lblBatchConsumption);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        line = new QFrame(ConsumptionFrm);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(10, 190, 770, 3));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        line_2 = new QFrame(ConsumptionFrm);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setGeometry(QRect(10, 190, 3, 200));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);
        line_3 = new QFrame(ConsumptionFrm);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setGeometry(QRect(10, 390, 770, 3));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);
        line_4 = new QFrame(ConsumptionFrm);
        line_4->setObjectName(QString::fromUtf8("line_4"));
        line_4->setGeometry(QRect(780, 190, 3, 200));
        line_4->setFrameShape(QFrame::VLine);
        line_4->setFrameShadow(QFrame::Sunken);
        line_5 = new QFrame(ConsumptionFrm);
        line_5->setObjectName(QString::fromUtf8("line_5"));
        line_5->setGeometry(QRect(10, 290, 770, 3));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);
        horizontalLayoutWidget_3 = new QWidget(ConsumptionFrm);
        horizontalLayoutWidget_3->setObjectName(QString::fromUtf8("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(580, 200, 191, 81));
        horizontalLayout_4 = new QHBoxLayout(horizontalLayoutWidget_3);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);

        lblResetBatch = new QLabel(horizontalLayoutWidget_3);
        lblResetBatch->setObjectName(QString::fromUtf8("lblResetBatch"));
        lblResetBatch->setFont(font1);

        horizontalLayout_4->addWidget(lblResetBatch);

        btBatchReset = new QPushButton(horizontalLayoutWidget_3);
        btBatchReset->setObjectName(QString::fromUtf8("btBatchReset"));
        btBatchReset->setMinimumSize(QSize(90, 70));
        btBatchReset->setMaximumSize(QSize(90, 70));
        btBatchReset->setText(QString::fromUtf8("PushButton"));

        horizontalLayout_4->addWidget(btBatchReset);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);


        retranslateUi(ConsumptionFrm);

        QMetaObject::connectSlotsByName(ConsumptionFrm);
    } // setupUi

    void retranslateUi(QWidget *ConsumptionFrm)
    {
        ConsumptionFrm->setWindowTitle(QApplication::translate("ConsumptionFrm", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ConsumptionFrm", "Consumption", 0, QApplication::UnicodeUTF8));
        lblDeviceId->setText(QApplication::translate("ConsumptionFrm", "Unit ID : ", 0, QApplication::UnicodeUTF8));
        txtTotalConsumption->setText(QApplication::translate("ConsumptionFrm", "Total consumption : ", 0, QApplication::UnicodeUTF8));
        lblResetTotal->setText(QApplication::translate("ConsumptionFrm", "Reset", 0, QApplication::UnicodeUTF8));
        txtBatchConsumption->setText(QApplication::translate("ConsumptionFrm", "Day consumption : ", 0, QApplication::UnicodeUTF8));
        lblResetBatch->setText(QApplication::translate("ConsumptionFrm", "Reset", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ConsumptionFrm: public Ui_ConsumptionFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_CONSUMPTION_H

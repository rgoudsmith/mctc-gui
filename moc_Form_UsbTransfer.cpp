/****************************************************************************
** Meta object code from reading C++ file 'Form_UsbTransfer.h'
**
** Created: Wed Feb 6 11:15:18 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_UsbTransfer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_UsbTransfer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_UsbTransferFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      27,   15,   15,   15, 0x08,
      46,   15,   15,   15, 0x0a,
      82,   15,   15,   15, 0x0a,
     101,   15,   15,   15, 0x0a,
     120,   15,   15,   15, 0x0a,
     139,   15,   15,   15, 0x0a,
     159,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_UsbTransferFrm[] = {
    "UsbTransferFrm\0\0CopyDone()\0"
    "MessageResult(int)\0"
    "UsbTransferMode(eTransferDirection)\0"
    "ButtonOneClicked()\0ButtonTwoClicked()\0"
    "ButtonAllClicked()\0ButtonDoneClicked()\0"
    "ButtonOkClicked()\0"
};

const QMetaObject UsbTransferFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_UsbTransferFrm,
      qt_meta_data_UsbTransferFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &UsbTransferFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *UsbTransferFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *UsbTransferFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_UsbTransferFrm))
        return static_cast<void*>(const_cast< UsbTransferFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int UsbTransferFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: CopyDone(); break;
        case 1: MessageResult((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: UsbTransferMode((*reinterpret_cast< eTransferDirection(*)>(_a[1]))); break;
        case 3: ButtonOneClicked(); break;
        case 4: ButtonTwoClicked(); break;
        case 5: ButtonAllClicked(); break;
        case 6: ButtonDoneClicked(); break;
        case 7: ButtonOkClicked(); break;
        default: ;
        }
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void UsbTransferFrm::CopyDone()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE

/********************************************************************************
** Form generated from reading UI file 'Form_UsbTransfer.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_USBTRANSFER_H
#define UI_FORM_USBTRANSFER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStackedWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UsbTransferFrm
{
public:
    QVBoxLayout *verticalLayout_3;
    QStackedWidget *pages;
    QWidget *page;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *lbTitle;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout;
    QPushButton *btOne;
    QPushButton *btTwo;
    QPushButton *btAll;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btOk;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_7;
    QLabel *lbTitle_2;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *verticalSpacer_4;
    QVBoxLayout *verticalLayout_6;
    QVBoxLayout *verticalLayout_4;
    QLabel *lbType;
    QHBoxLayout *horizontalLayout_5;
    QProgressBar *progType;
    QLabel *lbProgType;
    QSpacerItem *verticalSpacer_3;
    QVBoxLayout *verticalLayout_5;
    QLabel *lbItem;
    QHBoxLayout *horizontalLayout_6;
    QProgressBar *progItem;
    QLabel *lbProgItem;
    QSpacerItem *verticalSpacer_5;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_8;
    QSpacerItem *verticalSpacer_6;
    QPushButton *btDone;

    void setupUi(QWidget *UsbTransferFrm)
    {
        if (UsbTransferFrm->objectName().isEmpty())
            UsbTransferFrm->setObjectName(QString::fromUtf8("UsbTransferFrm"));
        UsbTransferFrm->resize(721, 516);
        UsbTransferFrm->setCursor(QCursor(Qt::BlankCursor));
        verticalLayout_3 = new QVBoxLayout(UsbTransferFrm);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        pages = new QStackedWidget(UsbTransferFrm);
        pages->setObjectName(QString::fromUtf8("pages"));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        verticalLayout_2 = new QVBoxLayout(page);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        lbTitle = new QLabel(page);
        lbTitle->setObjectName(QString::fromUtf8("lbTitle"));
        lbTitle->setText(QString::fromUtf8("Transfer"));

        horizontalLayout->addWidget(lbTitle);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        btOne = new QPushButton(page);
        btOne->setObjectName(QString::fromUtf8("btOne"));
        btOne->setMinimumSize(QSize(700, 70));
        btOne->setMaximumSize(QSize(700, 70));
        btOne->setText(QString::fromUtf8("One"));

        verticalLayout->addWidget(btOne);

        btTwo = new QPushButton(page);
        btTwo->setObjectName(QString::fromUtf8("btTwo"));
        btTwo->setMinimumSize(QSize(700, 70));
        btTwo->setMaximumSize(QSize(700, 70));
        btTwo->setText(QString::fromUtf8("Two"));

        verticalLayout->addWidget(btTwo);

        btAll = new QPushButton(page);
        btAll->setObjectName(QString::fromUtf8("btAll"));
        btAll->setMinimumSize(QSize(700, 70));
        btAll->setMaximumSize(QSize(700, 70));
        btAll->setText(QString::fromUtf8("All"));

        verticalLayout->addWidget(btAll);


        horizontalLayout_3->addLayout(verticalLayout);


        verticalLayout_2->addLayout(horizontalLayout_3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        btOk = new QPushButton(page);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("ok"));

        horizontalLayout_2->addWidget(btOk);


        verticalLayout_2->addLayout(horizontalLayout_2);

        pages->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        verticalLayout_7 = new QVBoxLayout(page_2);
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_7);

        lbTitle_2 = new QLabel(page_2);
        lbTitle_2->setObjectName(QString::fromUtf8("lbTitle_2"));

        horizontalLayout_4->addWidget(lbTitle_2);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_6);


        verticalLayout_7->addLayout(horizontalLayout_4);

        verticalSpacer_4 = new QSpacerItem(20, 112, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_4);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        lbType = new QLabel(page_2);
        lbType->setObjectName(QString::fromUtf8("lbType"));
        lbType->setText(QString::fromUtf8("Type: "));

        verticalLayout_4->addWidget(lbType);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        progType = new QProgressBar(page_2);
        progType->setObjectName(QString::fromUtf8("progType"));
        progType->setMinimumSize(QSize(0, 30));
        progType->setValue(24);

        horizontalLayout_5->addWidget(progType);

        lbProgType = new QLabel(page_2);
        lbProgType->setObjectName(QString::fromUtf8("lbProgType"));
        lbProgType->setMinimumSize(QSize(100, 0));
        lbProgType->setText(QString::fromUtf8("-/-"));
        lbProgType->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(lbProgType);


        verticalLayout_4->addLayout(horizontalLayout_5);


        verticalLayout_6->addLayout(verticalLayout_4);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_6->addItem(verticalSpacer_3);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        lbItem = new QLabel(page_2);
        lbItem->setObjectName(QString::fromUtf8("lbItem"));
        lbItem->setText(QString::fromUtf8("Item: "));

        verticalLayout_5->addWidget(lbItem);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        progItem = new QProgressBar(page_2);
        progItem->setObjectName(QString::fromUtf8("progItem"));
        progItem->setMinimumSize(QSize(0, 30));
        progItem->setValue(24);

        horizontalLayout_6->addWidget(progItem);

        lbProgItem = new QLabel(page_2);
        lbProgItem->setObjectName(QString::fromUtf8("lbProgItem"));
        lbProgItem->setMinimumSize(QSize(100, 0));
        lbProgItem->setText(QString::fromUtf8("-/-"));
        lbProgItem->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(lbProgItem);


        verticalLayout_5->addLayout(horizontalLayout_6);


        verticalLayout_6->addLayout(verticalLayout_5);


        verticalLayout_7->addLayout(verticalLayout_6);

        verticalSpacer_5 = new QSpacerItem(20, 112, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_5);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_8);

        verticalSpacer_6 = new QSpacerItem(20, 70, QSizePolicy::Minimum, QSizePolicy::Fixed);

        horizontalLayout_7->addItem(verticalSpacer_6);

        btDone = new QPushButton(page_2);
        btDone->setObjectName(QString::fromUtf8("btDone"));
        btDone->setMinimumSize(QSize(90, 70));
        btDone->setMaximumSize(QSize(90, 70));
        btDone->setText(QString::fromUtf8("done"));

        horizontalLayout_7->addWidget(btDone);


        verticalLayout_7->addLayout(horizontalLayout_7);

        pages->addWidget(page_2);

        verticalLayout_3->addWidget(pages);


        retranslateUi(UsbTransferFrm);

        pages->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(UsbTransferFrm);
    } // setupUi

    void retranslateUi(QWidget *UsbTransferFrm)
    {
        UsbTransferFrm->setWindowTitle(QApplication::translate("UsbTransferFrm", "Form", 0, QApplication::UnicodeUTF8));
        lbTitle_2->setText(QApplication::translate("UsbTransferFrm", "Copying files", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class UsbTransferFrm: public Ui_UsbTransferFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_USBTRANSFER_H

#ifndef SYSTEMCALIBRATIONFRM_H
#define SYSTEMCALIBRATIONFRM_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QTimer>
#include "Form_QbaseWidget.h"
#include "Form_MvcSystem.h"

namespace Ui
{
  class SystemCalibrationFrm;
}


class SystemCalibrationFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    SystemCalibrationFrm(QWidget *parent = 0);
    ~SystemCalibrationFrm();
    void FormInit();
    void DoCalibration(bool);

  protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *e);
    void showEvent(QShowEvent *e);

  private:
    Ui::SystemCalibrationFrm *ui;
    bool calibrate;
    bool showButton;
    QPushButton * btOk;
    QPushButton * btCancel;
    QLabel * lbMsgDisplay;
    QTimer * calTim;
    int calState;
    bool calibDone;
    void SetDisplay();

  private slots:
    void ButtonClick();
    void CalStateChanged(eCalState);
    void timerInterval();
    void CancelClicked();

  public slots:
    void CalStatusReceived(eCalStatus);

    signals:
    void SendCalCommand(eCalCommand);
};
#endif                                                                          // SYSTEMCALIBRATIONFRM_H

/*=================================================================================*/
/* File        : Form_UsbSelect.cpp                                                */
/* Description : Form in which the to be performed USB function can be selected    */
/*=================================================================================*/

#include "Form_UsbSelect.h"
#include "ui_Form_UsbSelect.h"
#include "Form_MainMenu.h"
#include "Rout.h"

USBSelectFrm::USBSelectFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::USBSelectFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);

  /*----- Connects -----*/
  connect(ui->btUSB,SIGNAL(clicked()),this,SLOT(USBDeviceClicked()));
  connect(ui->btCtrl,SIGNAL(clicked()),this,SLOT(DeviceUSBClicked()));
  connect(ui->btRemove,SIGNAL(clicked()),this,SLOT(USBRemoveClicked()));
  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
}


USBSelectFrm::~USBSelectFrm()
{
  delete ui;
}


void USBSelectFrm::changeEvent(QEvent *e)
{
  if (e->type() == QEvent::LanguageChange) {
    ui->retranslateUi(this);
    //    LanguageChanged();
  }
}


void USBSelectFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  ui->lbTitle->setFont(*(glob->captionFont));

  ui->label->setFont(*(glob->baseFont));
  ui->label_2->setFont(*(glob->baseFont));
  ui->label_4->setFont(*(glob->baseFont));

  /*----- Button images -----*/
  glob->SetButtonImage(ui->btUSB,itGeneral,genUSB);
  glob->SetButtonImage(ui->btCtrl,itGeneral,genUSB);
  glob->SetButtonImage(ui->btRemove,itGeneral,genUSB);
  glob->SetButtonImage(ui->btOk,itSelection,selOk);

  //LanguageChanged();
}


void USBSelectFrm::OkClicked()
{
  ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
}


void USBSelectFrm::USBDeviceClicked()
{
  /*===================================*/
  /* Copy files from USB to controller */
  /*===================================*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiUSBTransfer,0);                    /* USB tranfer screen, mode to controller */
}


void USBSelectFrm::DeviceUSBClicked()
{
  /*==================================*/
  /* Copy file from controller to USB */
  /*==================================*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiUSBTransfer,1);                    /* USB tranfer screen, mode to USBr */
}


void USBSelectFrm::USBRemoveClicked()
{
  /*=================================*/
  /* Remove USB stick button pressed */
  /*=================================*/
  glob->usbStick->UsbUnmount();
  /*----- Display message -----*/
  ((MainMenu*)(glob->menu))->DisplayMessage(-1,0,tr("USB Stick can be safely removed !"),msgtConfirm);
  ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
}

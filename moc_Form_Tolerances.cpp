/****************************************************************************
** Meta object code from reading C++ file 'Form_Tolerances.h'
**
** Created: Wed Feb 6 11:15:17 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_Tolerances.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_Tolerances.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TolerancesFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      43,   41,   14,   14, 0x0a,
      68,   41,   14,   14, 0x0a,
      93,   14,   14,   14, 0x0a,
     109,   14,   14,   14, 0x0a,
     125,   14,   14,   14, 0x0a,
     148,   14,   14,   14, 0x0a,
     176,   14,   14,   14, 0x0a,
     188,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_TolerancesFrm[] = {
    "TolerancesFrm\0\0DisplayWindow(eWindowIdx)\0"
    ",\0DevAlmChanged(float,int)\0"
    "CalDevChanged(float,int)\0DevAlmClicked()\0"
    "CalDevClicked()\0UnitValReceived(float)\0"
    "LanguageChanged(eLanguages)\0OkClicked()\0"
    "CancelClicked()\0"
};

const QMetaObject TolerancesFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_TolerancesFrm,
      qt_meta_data_TolerancesFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TolerancesFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TolerancesFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TolerancesFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TolerancesFrm))
        return static_cast<void*>(const_cast< TolerancesFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int TolerancesFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: DisplayWindow((*reinterpret_cast< eWindowIdx(*)>(_a[1]))); break;
        case 1: DevAlmChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: CalDevChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: DevAlmClicked(); break;
        case 4: CalDevClicked(); break;
        case 5: UnitValReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: LanguageChanged((*reinterpret_cast< eLanguages(*)>(_a[1]))); break;
        case 7: OkClicked(); break;
        case 8: CancelClicked(); break;
        default: ;
        }
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void TolerancesFrm::DisplayWindow(eWindowIdx _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

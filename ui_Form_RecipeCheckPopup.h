/********************************************************************************
** Form generated from reading UI file 'Form_RecipeCheckPopup.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_RECIPECHECKPOPUP_H
#define UI_FORM_RECIPECHECKPOPUP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RecipeCheckPopupFrm
{
public:
    QVBoxLayout *verticalLayout_6;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_5;
    QLabel *lbCaption;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_4;
    QLabel *label;
    QLabel *lbCurMode;
    QSpacerItem *horizontalSpacer_2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_5;
    QLabel *lbRcpMode;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *verticalSpacer_4;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label_2;
    QPushButton *btOk;
    QPushButton *btCancel;

    void setupUi(QWidget *RecipeCheckPopupFrm)
    {
        if (RecipeCheckPopupFrm->objectName().isEmpty())
            RecipeCheckPopupFrm->setObjectName(QString::fromUtf8("RecipeCheckPopupFrm"));
        RecipeCheckPopupFrm->resize(677, 483);
        RecipeCheckPopupFrm->setCursor(QCursor(Qt::BlankCursor));
        RecipeCheckPopupFrm->setWindowTitle(QString::fromUtf8("Form"));
        RecipeCheckPopupFrm->setStyleSheet(QString::fromUtf8("#groupBox {border: 5px solid; border-radius: 7px; background-color: rgb(255,255,255); border-color: rgb(0, 0, 125);}"));
        verticalLayout_6 = new QVBoxLayout(RecipeCheckPopupFrm);
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        groupBox = new QGroupBox(RecipeCheckPopupFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setTitle(QString::fromUtf8(""));
        verticalLayout_5 = new QVBoxLayout(groupBox);
        verticalLayout_5->setContentsMargins(6, 6, 6, 6);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        lbCaption = new QLabel(groupBox);
        lbCaption->setObjectName(QString::fromUtf8("lbCaption"));
        lbCaption->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(lbCaption);

        verticalSpacer_3 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_5->addItem(verticalSpacer_3);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));

        horizontalLayout_6->addLayout(verticalLayout_4);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_6->addWidget(label);

        lbCurMode = new QLabel(groupBox);
        lbCurMode->setObjectName(QString::fromUtf8("lbCurMode"));
        lbCurMode->setText(QString::fromUtf8("TextLabel"));
        lbCurMode->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_6->addWidget(lbCurMode);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_2);


        verticalLayout_5->addLayout(horizontalLayout_6);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_4->addWidget(label_5);

        lbRcpMode = new QLabel(groupBox);
        lbRcpMode->setObjectName(QString::fromUtf8("lbRcpMode"));
        lbRcpMode->setText(QString::fromUtf8("TextLabel"));
        lbRcpMode->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(lbRcpMode);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout_5->addLayout(verticalLayout_2);

        verticalSpacer_4 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_5->addItem(verticalSpacer_4);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout->addWidget(label_2);

        btOk = new QPushButton(groupBox);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("ok"));

        horizontalLayout->addWidget(btOk);

        btCancel = new QPushButton(groupBox);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("cancel"));

        horizontalLayout->addWidget(btCancel);


        verticalLayout_5->addLayout(horizontalLayout);


        verticalLayout_6->addWidget(groupBox);


        retranslateUi(RecipeCheckPopupFrm);

        QMetaObject::connectSlotsByName(RecipeCheckPopupFrm);
    } // setupUi

    void retranslateUi(QWidget *RecipeCheckPopupFrm)
    {
        lbCaption->setText(QApplication::translate("RecipeCheckPopupFrm", "Configuration Change !", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("RecipeCheckPopupFrm", "Current mode : ", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("RecipeCheckPopupFrm", " Recipe mode :", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("RecipeCheckPopupFrm", "Change configuration ? ", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(RecipeCheckPopupFrm);
    } // retranslateUi

};

namespace Ui {
    class RecipeCheckPopupFrm: public Ui_RecipeCheckPopupFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_RECIPECHECKPOPUP_H

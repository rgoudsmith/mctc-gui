#include "Form_AlarmConfigItem.h"
#include "ui_Form_AlarmConfigItem.h"
#include "Rout.h"

AlarmConfigItemFrm::AlarmConfigItemFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::AlarmConfigItemFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);

  mode = false;
  alarmNr = 0;

  connect(ui->btType,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
}


AlarmConfigItemFrm::~AlarmConfigItemFrm()
{
  delete ui;
}


void AlarmConfigItemFrm::showEvent(QShowEvent *e)
{
  /*------------*/
  /* Show event */
  /*------------*/
  int idx;

  QBaseWidget::showEvent(e);

  idx = glob->ActUnitIndex();

  /*----- Fetch alarm mode -----*/
  mode = (glob->sysCfg[idx].MvcSettings.alarm.almConfig[alarmNr].actVal == 1);

  if (mode == true) {
    glob->SetButtonImage(ui->btType,itAlarmWarning,awAlarm);
  }
  else {
    glob->SetButtonImage(ui->btType,itAlarmWarning,awWarning);
  }

}


void AlarmConfigItemFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  ui->lbNr->setFont(*(glob->baseFont));
  ui->lbDescript->setFont(*(glob->baseFont));
  ui->btType->setFont(*(glob->baseFont));
}


void AlarmConfigItemFrm::ButtonClicked()
{
  /*-------------------------------------------------------------*/
  /* Mode button clicked : Toggle between alarm and warning mode */
  /*-------------------------------------------------------------*/
  SetAlarmMode(!mode,true);
}


void AlarmConfigItemFrm::SetAlarmNr(int nr)
{
  if (nr != alarmNr) {
    alarmNr = nr;
    ui->lbNr->setText(QString::number(alarmNr));
  }
}


void AlarmConfigItemFrm::SetAlarmDescription(const QString &desc)
{
  if (desc != description) {
    description = desc;
    ui->lbDescript->setText(description);
  }
}


void AlarmConfigItemFrm::SetupUnitAlarm(eMcDeviceType unitType, bool overwrite) {
  /*---------------------------------------------------------*/
  /* Copy the alarm mode to all units of the same given type */
  /*---------------------------------------------------------*/
  int i;
  if (Rout::ActUnitType() == unitType) {
    for (i=0; i< CFG_MAX_UNIT_COUNT; i++) {
      if (glob->sysCfg[i].deviceType == unitType) {
        if (overwrite) {
          glob->SysCfg_AlarmConfig(alarmNr,(mode == true ? 1 : 0),i);
          /*----- If alarm == 8 (Deviation), alarm 7 is also updated (Deviation Quick Response) -----*/
          if (alarmNr == ALM_INDEX_DEV) {
            glob->SysCfg_AlarmConfig(ALM_INDEX_DEV_QR,(mode == true ? 1 : 0),i);
          }
        }
      }
    }
  }
}



void AlarmConfigItemFrm::SetAlarmMode(bool mod, bool overwrite)
{
  if (mod != mode) {
    mode = mod;

    if (mode == true) {
      glob->SetButtonImage(ui->btType,itAlarmWarning,awAlarm);
    }
    else {
      glob->SetButtonImage(ui->btType,itAlarmWarning,awWarning);
    }

    /*----- Copy the alarm mode setting to all units of the same type -----*/
    SetupUnitAlarm(Rout::ActUnitType(),overwrite);
  }
}

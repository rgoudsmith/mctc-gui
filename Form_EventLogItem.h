#ifndef EVENTLOGITEMFRM_H
#define EVENTLOGITEMFRM_H

#include "Form_QbaseWidget.h"

namespace Ui
{
  class EventLogItemFrm;
}


class EventLogItemFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit EventLogItemFrm(QWidget *parent = 0);
    ~EventLogItemFrm();
    void FormInit();

  protected:

  private:
    Ui::EventLogItemFrm *ui;
    void SetFont();
    unsigned long eventID;                                                      // identifier for the event to display
    EventLogItemStruct item;

  private slots:
    void DisplayData();

  public slots:
    void SetData(EventLogItemStruct *);

    signals:
    void SizeChanged();
};
#endif                                                                          // EVENTLOGITEMFRM_H

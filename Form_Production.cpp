#include "Form_Production.h"
#include "ui_Form_Production.h"
#include "Form_MainMenu.h"
#include "Rout.h"

#define PROD_V_OFFSET     15
#define PROD_LEARN_INIT   "---"

ProductionFrm::ProductionFrm(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::ProductionFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  calibForm = 0;
  curState = mvcDSminimum;

  isRpmMode = false;
  showEdit1 = false;
  showEdit2 = false;
  showTachoButton = false;
  // Prod status visible according to MC status
  showProdSts = false;
  editIdx = -1;
  m_ui->setupUi(this);
  m_ui->mvcSystem->SetDispIdx(0);
  SetDisplayState(mvcDSminimum);

  btMidDetail = new QPushButton("(i)",this);
  btRecipe = new QPushButton("RECIPE",this);
  btTachoSet = new QPushButton("TACHO",this);
  btRpmSet1 = new QPushButton("RPM",this);

  edInput2 = new QLineEdit("ShotWeight g",this);
  edInput2->setCursor(Qt::BlankCursor);
  edInput2Label = new QLabel(this);

  edInput1 = new QLineEdit("ShotTime s",this);
  edInput1->setCursor(Qt::BlankCursor);
  edInput1Label = new QLabel(this);

  // Numeric Input Form
  numInput = new NumericInputFrm();
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
  numInput->hide();

  // Character Input Form;
  txtInput = new KeyboardFrm();
  txtInput->move(((width()/2)-(txtInput->width()/2)),((height()/2)-(txtInput->height()/2)));
  txtInput->hide();

  // Tacho input form
  tachoForm = new TachoFrm();
  tachoForm->move(((width()/2)-(tachoForm->width()/2)),((height()/2)-(tachoForm->height()/2)));
  tachoForm->SetNumInput(numInput);

  calibForm = new CalibrationFrm(this);
  calibForm->setFixedWidth(270);
  calibForm->hide();

  // Buttons
  btMidDetail->setFixedSize(90,70);
  btRecipe->setFixedSize(140,70);
  btTachoSet->setFixedSize(90,70);
  btRpmSet1->setFixedSize(90,70);

  btMidDetail->move(((this->width()/2)-30),410);
  btRecipe->move(((this->width()/2)-30),20);
  btRpmSet1->move(120,140);

  // Edits
  edInput2->setFixedSize(140,60);
  edInput2Label->setFixedSize(edInput2->size());
  edInput2Label->setFrameShape(QFrame::Box);

  edInput1->setFixedSize(110,60);
  edInput1Label->setFixedSize(edInput1->size());
  edInput1Label->setFrameShape(QFrame::Box);

  edInput1->move(edInput2->x(),(edInput2->y()+65));
  edInput1->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  edInput1->installEventFilter(this);
  edInput1->setFocusPolicy(Qt::NoFocus);

  edInput2->move((btRecipe->x()-25),(btRecipe->y()+65));
  edInput2->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  edInput2->installEventFilter(this);
  edInput2->setFocusPolicy(Qt::NoFocus);

  edInput1Label->setAlignment(edInput1->alignment());
  edInput2Label->setAlignment(edInput2->alignment());

  // Recipe label
  lbRecipe = new QLabel(this);
  lbRecipe->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  lbRecipe->setFixedSize(btRecipe->size());
  lbRecipe->move(btRecipe->pos());
  lbRecipe->hide();

  // Recipe label name
  lbRecipeName = new QLabel(this);
  lbRecipeName->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  lbRecipeName->setFixedSize(350,25);
  lbRecipeName->move(btRecipe->pos().x()-105,(btRecipe->pos().y()+btRecipe->height()));
  lbRecipeName->setVisible(true);
  lbRecipeName->setText("");

  // Tacho / Dos Time - set (caption)
  lbInput1 = new QLabel(this);
  lbInput1->setFixedSize(140,25);
  lbInput1->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  lbInput1->setText(tr("Tacho set"));

  // Ext Capacity / Shot Weight - set (caption)
  lbInput2 = new QLabel(this);
  lbInput2->setFixedSize(140,25);
  lbInput2->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  lbInput2->setText(tr("Ext Cap set"));

  // Ext Capacity / Shot Weight - act (caption)
  lbValue1Caption = new QLabel(this);
  lbValue1Caption->setFixedSize(140,25);
  lbValue1Caption->setAlignment(lbInput2->alignment());
  lbValue1Caption->setText(tr("Ext Cap act"));

  // Ext Capacity / Shot Weight - act (value)
  lbValue1Value = new QLabel(this);
  lbValue1Value->setFixedSize(lbValue1Caption->size());
  lbValue1Value->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  //  lbValue1Value->setText("0.0 "+QString(UNITS_KG_H));
  lbValue1Value->setText("0.0 "+glob->extrUnits);

  // Tacho / Dos Time - act (caption)
  lbValue2Caption = new QLabel(this);
  lbValue2Caption->setFixedSize(135,25);
  lbValue2Caption->setAlignment(lbInput2->alignment());
  lbValue2Caption->setText(tr("Tacho act"));

  // Tacho / Dos Time - act (value)
  lbValue2Value = new QLabel(this);
  lbValue2Value->setFixedSize(lbValue2Caption->size());
  lbValue2Value->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  lbValue2Value->setText("0 s");

  lbProdStatus1 = new QLabel(this);
  lbProdStatus1->setFixedSize(158,40);
  lbProdStatus1->setFrameShape(QFrame::Box);
  lbProdStatus1->setLineWidth(1);
  lbProdStatus1->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  lbProdStatus1->setText(PROD_LEARN_INIT);

  lbProdStatus2 = new QLabel(this);
  lbProdStatus2->setFixedSize(lbProdStatus1->size());
  lbProdStatus2->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  lbProdStatus2->setText(PROD_LEARN_INIT);
  lbProdStatus2->setVisible(false);
  connect(btMidDetail,SIGNAL(clicked()),this,SLOT(btDisplayClicked()));
  connect(btTachoSet,SIGNAL(clicked()),this,SLOT(btTachoClicked()));
  connect(btRpmSet1,SIGNAL(clicked()),this,SLOT(btTachoClicked()));

  m_ui->mvcSystem->SetMotorStatus(0,0);
  m_ui->mvcSystem->SetMotorStatus(0,1);
}


ProductionFrm::~ProductionFrm()
{
  delete numInput;
  delete txtInput;
  delete tachoForm;
  delete m_ui;
}


void ProductionFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
  case QEvent::LanguageChange:
    m_ui->retranslateUi(this);
    LanguageUpdate();
    break;
  default:
    break;
  }
}


void ProductionFrm::resizeEvent(QResizeEvent * e)
{
  QWidget::resizeEvent(e);
  numInput->move(((width()/2)-(numInput->width()/2)),(((height()/2)-(numInput->height()/2))-10));
  txtInput->move(((width()/2)-(txtInput->width()/2)),((height()/2)-(txtInput->height()/2)));
  tachoForm->move(((width()/2)-(tachoForm->width()/2)),((height()/2)-(tachoForm->height()/2)));
  PositionProdStatus();
  SetInitState();
}


void ProductionFrm::showEvent(QShowEvent *e)
{
  /*------------------------------------------------------------------*/
  /* Show event is called whenever the production screen is displayed */
  /*------------------------------------------------------------------*/

  /*----- Auto start unit -----*/
  Rout::AutoStart();

  QWidget::showEvent(e);
}


void ProductionFrm::hideEvent(QHideEvent *e)
{
  if (calibForm != 0) {
    if (calibForm->isVisible()) {
      calibForm->hide();
    }
  }
  QWidget::hideEvent(e);
}


bool ProductionFrm::eventFilter(QObject *obj, QEvent *ev)
{
  /*--------------*/
  /* Event filter */
  /*--------------*/
  double val=0;

  if (ev->type() == QEvent::MouseButtonPress) {
    /*----------------------------*/
    /* Mouse button pressed event */
    /*----------------------------*/

    /* Check backlit, if active turn it on and ignore event */
    if (Rout::BacklitOn()) {
      return(TRUE);
    }

    /*----- Edit field 2 (Shotweight/Ext.cap) -----*/
    if (obj ==  edInput2) {
      switch (glob->mstCfg.mode) {
      /*----- Detect Injection mode --> Editfield 2 = Shotweight -----*/
      case modInject:
        if(glob->mstSts.isTwin) {
          val = glob->sysCfg[0].injection.shotWeight;
        }
        else {
          val = glob->sysCfg[glob->ActUnitIndex()].injection.shotWeight;
        }
        numInput->SetDisplay(true,glob->formats.ShotWeightFmt.precision,val,0.1,INPUT_MAX_SHOTWEIGHT);
        break;
      case modExtrude:
        /*----- Detect Extrusion mode --> Editfield 2 = Exp.capacity -----*/
        if(glob->mstSts.isTwin) {
          val = glob->sysCfg[0].extrusion.extCapacity;
        }
        else {
          val = glob->sysCfg[glob->ActUnitIndex()].extrusion.extCapacity;
        }

        numInput->SetDisplay(true,glob->formats.ExtruderFmt.precision,val,0.01,INPUT_MAX_EXTCAP);
        break;
      default:
        break;
      }

#warning Jan:kan dit het probleem bij offline cal zijn?
      editIdx = 0;                                                              /* Set shotweight / Ext.cap entry mode */

      KeyBeep();
      numInput->show();
    }
    else {
      /*----- Edit field 1 (Shottime / tacho) -----*/
      if (obj == edInput1) {
        switch(glob->mstCfg.mode) {
        /*----- Detect Injection mode --> Editfield 1 = Shottime -----*/
        case modInject:
          /*---------- Shot time ----------*/
          if(glob->mstSts.isTwin) {
            val = glob->sysCfg[0].injection.shotTime;
          }
          else {
            val = glob->sysCfg[glob->ActUnitIndex()].injection.shotTime;
          }
          numInput->SetDisplay(true,glob->formats.TimeFmt.precision,val,0.1,INPUT_MAX_SHOTTIME);
          break;
          /*----- Detect Extruder mode --> Editfield 1 = Tacho -----*/
        case modExtrude:
          if(glob->mstSts.isTwin) {
            val = glob->sysCfg[0].extrusion.tachoMax;
          }
          else {
            val = glob->sysCfg[glob->ActUnitIndex()].extrusion.tachoMax;
          }
          numInput->SetDisplay(true,glob->formats.TachoFmt.precision,val,0.1,INPUT_MAX_TACHO);
          break;
        default:
          break;
        }
        editIdx = 1;                                                            /* Set shottime / Tacho entry mode */

        KeyBeep();
        numInput->show();
      }
      else {
        editIdx = -1;
      }
    }
  }
  return QWidget::eventFilter(obj,ev);
}


void ProductionFrm::SetRecipeDataChanged(void) {
  /*----------------------------------*/
  /* Set the recipe data changed mode */
  /*----------------------------------*/
  glob->SetButtonImage(btRecipe,itConfig,cfgRcp_Save_Menu);
  recipeChanged = TRUE;
}


void ProductionFrm::ResetRecipeDataChanged(void) {
  /*-------------------------------*/
  /* Reset the recipe changed mode */
  /*-------------------------------*/
  glob->SetButtonImage(btRecipe,itConfig,cfgRcp_Menu);
  recipeChanged = FALSE;
}


void ProductionFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  m_ui->mvcSystem->FormInit();
  numInput->FormInit();
  txtInput->FormInit();
  tachoForm->FormInit();
  calibForm->FormInit();

  // Check for TWIN and set gui accordingly
  glob->SetButtonImage(btMidDetail,itGeneral,genInfo);
  glob->SetButtonImage(btTachoSet,itType,ttTacho);
  ResetRecipeDataChanged();

  if(glob->mstSts.isTwin) {
    m_ui->mvcSystem->SetDisplayType(DISP_TYPE_TWIN);
  }
  else {
    if (glob->mstCfg.slaveCount != 0) {
      m_ui->mvcSystem->SetDisplayType(DISP_TYPE_MULTI_UNIT);
    }
    else {
      m_ui->mvcSystem->SetDisplayType(DISP_TYPE_SINGLE_UNIT);
    }
  }
  btMidDetail->setVisible(true);

  connect(glob,SIGNAL(MstCfg_RcpEnabledChanged(bool)),this,SLOT(RcpEnabledChanged(bool)));
  connect(glob,SIGNAL(MstCfg_ExtUnitsChanged(QString)),this,SLOT(MstCfg_ShotUnitsChanged()));
  connect(glob,SIGNAL(MstCfg_TimeUnitsChanged(QString)),this,SLOT(MstCfg_TimeUnitsChanged()));

  connect(glob,SIGNAL(SysCfg_ShotWeightChanged(float,int)),this,SLOT(SysCfg_ShotWeightChanged(float,int)));
  connect(glob,SIGNAL(SysCfg_ShotTimeChanged(float,int)),this,SLOT(SysCfg_ShotTimeChanged(float,int)));
  connect(glob,SIGNAL(SysSts_ShotTimeChanged(float,int)),this,SLOT(SysSts_ShotTimeChanged(float,int)));

  connect(glob,SIGNAL(SysSts_MeteringActChanged(float,int)),this,SLOT(SysSts_MeteringActChanged(float,int)));
  connect(glob,SIGNAL(SysCfg_MeteringStartChanged(float,int)),this,SLOT(SysCfg_MeteringStartChanged(float,int)));
  connect(glob,SIGNAL(SysSts_MeteringValidChanged(bool,int)),this,SLOT(SysSts_MeteringValidChanged(bool,int)));

  connect(glob,SIGNAL(SysCfg_ExtCapChanged(float,int)),this,SLOT(SysCfg_ExtCapChanged(float,int)));
  connect(glob,SIGNAL(SysSts_ExtCapChanged(float,int)),this,SLOT(SysSts_ExtCapChanged(float,int)));
  //        connect(glob,SIGNAL(SysCfg_TachoMaxChanged(float,int)),this,SLOT(TachoChanged(float,int)));
  connect(glob,SIGNAL(SysSts_TachoChanged(float,int)),this,SLOT(TachoChanged(float,int)));
  connect(glob,SIGNAL(SysSts_LearnStsChanged(bool,int)),this,SLOT(SysSts_LearnStsChanged(bool,int)));
  connect(glob,SIGNAL(MstCfg_RecipeChanged(QString)),this,SLOT(RecipeChanged(QString)));

  connect(calibForm,SIGNAL(SetDisplayState(eMvcDispState)),this,SLOT(SetDisplayState(eMvcDispState)));
  connect(glob,SIGNAL(SysSts_MCStatusChanged(eMCStatus,int)),this,SLOT(MCStatusChanged(eMCStatus,int)));

  // ==== Configuration changes ====
  // User login
  connect(glob,SIGNAL(MstCfg_UserChanged(eUserType)),this,SLOT(MstCfg_UserChanged(eUserType)));
  // Inject / Extrude
  connect(glob,SIGNAL(MstCfg_ProdModeChanged(eMode)),this,SLOT(ModeChanged(eMode)));
  // Gravi / Rpm
  connect(glob,SIGNAL(SysCfg_GraviRpmChanged(eGraviRpm,int)),this,SLOT(GraviRpmChanged(eGraviRpm,int)));
  // Timer/Relay/Tacho/MC_ID
  connect(glob,SIGNAL(MstCfg_InputTypeChanged(eType)),this,SLOT(TypeChanged(eType)));
  // Number of slaves
  connect(glob,SIGNAL(MstCfg_SlaveCountChanged(int)),this,SLOT(SlaveCountChanged(int)));
  connect(glob,SIGNAL(MstSts_ActUnitChanged(int)),this,SLOT(ActUnitIndexChanged(int)));

  connect(m_ui->mvcSystem,SIGNAL(Material1Clicked()),this,SLOT(Material1Clicked()));
  connect(m_ui->mvcSystem,SIGNAL(Material2Clicked()),this,SLOT(Material2Clicked()));
  // connect(this,SIGNAL(RecipeSelected(QString)),_gbi,SLOT())

  connect(btRecipe,SIGNAL(clicked()),this,SLOT(RecipeClicked()));
  connect(txtInput,SIGNAL(inputText(QString)),this,SLOT(TextReceived(QString)));

  // Assign fonts
  btRecipe->setFont(*(glob->baseFont));
  lbRecipe->setFont(*(glob->baseFont));
  lbRecipeName->setFont(*(glob->baseFont));

  btTachoSet->setFont(*(glob->baseFont));
  btRpmSet1->setFont(*(glob->baseFont));

  edInput1->setFont(*(glob->baseFont));
  edInput1Label->setFont(*(glob->baseFont));
  lbInput1->setFont(*(glob->baseFont));

  edInput2->setFont(*(glob->baseFont));
  edInput2Label->setFont(*(glob->baseFont));
  lbInput2->setFont(*(glob->baseFont));

  lbValue1Caption->setFont(*(glob->baseFont));
  lbValue1Value->setFont(*(glob->baseFont));

  lbValue2Caption->setFont(*(glob->baseFont));
  lbValue2Value->setFont(*(glob->baseFont));

  lbProdStatus1->setFont(*(glob->baseFont));
  lbProdStatus2->setFont(*(glob->baseFont));

  SetInitState();
  connect(numInput,SIGNAL(InputValue(float)),this,SLOT(ValueReceived(float)));
  SetupDisplay();
}


void ProductionFrm::LanguageUpdate()
{
  // Calling this function will update all display texts.
  SetupDisplay();
  int i = glob->ActUnitIndex();
  SysSts_LearnStsChanged(glob->sysSts[i].balanceCalibrated,i);
  btRpmSet1->setText(glob->rpmUnits.toUpper());
}


void ProductionFrm::SetDisplayState(eMvcDispState _state)
{
  MvcSystem * mvc;

  if ((_state != curState) || (_state > mvcDSmaximum)) {
    mvc = m_ui->mvcSystem;
    curState = _state;

    /*----- Setup display image -----*/
    if (glob->mstSts.isTwin) {                                                  /* TWIN mode ? */
      if (curState == mvcDSpreCal || curState == mvcDSpreCal2) {
        mvc->SetDisplayType(DISP_TYPE_MULTI_UNIT);                              /* During precal, single unit image */
      }
      else {
        mvc->SetDisplayType(DISP_TYPE_TWIN);                                    /* Dual unit image */
      }
    }
    else {
      if (glob->mstCfg.slaveCount >= 1) {                                       /* Mutiple components ? */
        mvc->SetDisplayType(DISP_TYPE_MULTI_UNIT);                              /* Multi unit display */

      }
      else {
        mvc->SetDisplayType(DISP_TYPE_SINGLE_UNIT);                             /* Single unit display */
      }
    }

    switch (curState) {
    /*------------------------*/
    /* Minimum data view mode */
    /*------------------------*/
    case mvcDSminimum:
      mvc->SetMCIDVisible(true);
      mvc->SetMCStatusVisible(true);
      mvc->SetColorPctVisible(true);
      if (((glob->sysCfg[glob->ActUnitIndex()].GraviRpm == grRpm) && (glob->mstCfg.mode == modExtrude)) && (glob->mstCfg.inpType == ttTacho)) {
        mvc->SetEditColorPct(false);
      }
      else {
        mvc->SetEditColorPct(true);
      }
      mvc->SetDosActVisible(false);
      mvc->SetDosSetVisible(false);
      mvc->SetToGoVisible(true);

      mvc->SetMaterialVisible(false);
      mvc->SetMaterialEditable(false);

      mvc->SetRPMVisible(false);
      mvc->SetMotorVisible(true);
      mvc->SetWeightVisible(false);

      if (calibForm != 0) {
        calibForm->hide();
      }

      /*----- Enable products to go refresh in home screen -----*/
      Rout::homeScreenActive = TRUE;

      break;

      /*----------------------------------*/
      /* Medium data view mode (Not used) */
      /*----------------------------------*/
    case mvcDSmedium:                                                         // Medium settings (not used at this moment)
      mvc->SetMCIDVisible(true);
      mvc->SetMCStatusVisible(true);
      mvc->SetColorPctVisible(true);
      mvc->SetEditColorPct(true);
      mvc->SetDosActVisible(false);
      mvc->SetDosSetVisible(false);
      mvc->SetToGoVisible(false);
      mvc->SetMaterialVisible(true);
      mvc->SetMaterialEditable(false);
      mvc->SetRPMVisible(false);
      mvc->SetMotorVisible(true);
      mvc->SetWeightVisible(false);

      if (calibForm != 0) {
        calibForm->hide();
      }

      break;

      /*-------------------l---------------------------------------------*/
      /* Maximum data view mode (Single unit with all settings visible) */
      /*-----------------------------------------------------------------*/

    case mvcDSmaximum:                                                        // SINGLE unit with all settings visible
      mvc->SetMCIDVisible(true);
      mvc->SetMCStatusVisible(true);
      if (isRpmMode) {
        mvc->SetColorPctVisible(true);
        mvc->SetEditColorPct(false);
        mvc->SetDosActVisible(false);
        mvc->SetDosSetVisible(false);
      }
      else {
        mvc->SetColorPctVisible(false);
        mvc->SetEditColorPct(false);
        mvc->SetDosActVisible(true);
        mvc->SetDosSetVisible(true);
      }
      mvc->SetMaterialVisible(true);
      mvc->SetMaterialEditable(false);
      mvc->SetRPMVisible(true);
      mvc->SetMotorVisible(true);
      mvc->SetWeightVisible(true);
      mvc->SetToGoVisible(false);

      if (calibForm != 0) {
        calibForm->hide();
      }

      break;

    case mvcDSpreCal:
      /*-------------------------------------------------*/
      /* Precalibration                                  */
      /* Display all re quired items for pre-calibration */
      /* Including the pre-cal overlay display           */
      /*-------------------------------------------------*/

      mvc->SetMCIDVisible(true);
      mvc->SetMCStatusVisible(false);
      mvc->SetColorPctVisible(true);
      mvc->SetEditColorPct(true);
      mvc->SetDosActVisible(false);
      mvc->SetDosSetVisible(false);
      mvc->SetMaterialVisible(true);
      mvc->SetMaterialEditable(true);
      mvc->SetRPMVisible(false);
      mvc->SetMotorVisible(false);
      mvc->SetWeightVisible(false);
      mvc->SetToGoVisible(false);

      if (calibForm != 0) {
        calibForm->show();
        calibForm->move((this->width()-(calibForm->width())),0);
      }

      /*----- Reset shottime act to zero on video -----*/
      if (glob->mstCfg.mode == modInject) {
        edInput1->setText(QString::number(0,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) +QString(" ") + glob->timerUnits);
      }

      break;

    case mvcDSpreCal2:
      /*-------------------------------------------------*/
      /* Precalibration                                  */
      /* Display all required items for pre-calibration  */
      /* Including the pre-cal overlay display           */
      /*-------------------------------------------------*/
      mvc->SetMCIDVisible(true);
      mvc->SetMCStatusVisible(false);
      mvc->SetColorPctVisible(true);
      mvc->SetEditColorPct(false);
      mvc->SetDosActVisible(false);
      mvc->SetDosSetVisible(false);
      mvc->SetMaterialVisible(true);
      mvc->SetMaterialEditable(false);
      mvc->SetRPMVisible(true);
      mvc->SetMotorVisible(true);
      mvc->SetWeightVisible(true);
      mvc->SetToGoVisible(false);

      if (calibForm != 0) {
        calibForm->show();
        calibForm->move((this->width()-(calibForm->width())),0);
      }
      break;

    default:
      SetDisplayState(mvcDSminimum);

      if (calibForm != 0) {
        calibForm->hide();
      }
      break;

    }
    SetupDisplay();
  }
}


bool ProductionFrm::calibVisible()
{
  if (calibForm != 0) {
    return calibForm->isVisible();
  }
  else return false;
}


void ProductionFrm::SetEditField(eFieldType field, bool isVisible, bool isEdit)
/*-------------------------*/
/* Setup the an edit field */
/*-------------------------*/
{
  switch (field) {

  case ftColPct:
    m_ui->mvcSystem->SetColorPctVisible(isVisible);
    m_ui->mvcSystem->SetEditColorPct(isEdit && !Rout::keyLock);
    break;

  case ftShotWeight:
    lbInput2->setVisible(isVisible);
    if (isVisible) {
      if (isEdit && !Rout::keyLock) {
        edInput2->setVisible(true);
        edInput2Label->setVisible(false);
      }
      else {
        edInput2->setVisible(false);
        edInput2Label->setVisible(true);
      }
    }
    else {
      edInput2->setVisible(showEdit1);
      edInput2Label->setVisible(showEdit1);
    }
    break;

  case ftShotTime:
    lbInput1->setVisible(isVisible);
    if (isVisible) {
      if (isEdit && !Rout::keyLock) {
        edInput1->setVisible(true);
        edInput1Label->setVisible(false);
      }
      else {
        edInput1->setVisible(false);
        edInput1Label->setVisible(true);
      }
    }
    else {
      edInput1->setVisible(false);
      edInput1Label->setVisible(false);
    }
    break;

  case ftExtCap:
    /*----- Disable extruder capacity field when McWeight is enabled and not learn off line active -----*/
#warning SORRY ROB
    if (Rout::McWeightPresent() && !glob->mstSts.learnOffLineActive) {
      isVisible = false;
    }

    glob->mstSts.learnOffLineActive = false;

    lbInput2->setVisible(isVisible);
    if (isVisible) {
      if(isEdit && !Rout::keyLock) {
        edInput2->setVisible(true);
        edInput2Label->setVisible(false);
      }
      else {
        edInput2->setVisible(false);
        edInput2Label->setVisible(true);
      }
    }
    else {
      edInput2->setVisible(false);
      edInput2Label->setVisible(false);
    }
    break;

  case ftTimeAct:
    lbValue2Caption->setVisible(isVisible);
    lbValue2Value->setVisible(isVisible);
    break;

  case ftExtCapAct:
    lbValue1Caption->setVisible(isVisible);
    lbValue1Value->setVisible(isVisible);
    break;

  case ftTachoAct:
    lbValue2Caption->setVisible(isVisible);
    lbValue2Value->setVisible(isVisible);
    break;

  case ftAll:
    lbInput1->setVisible(isVisible);
    edInput1->setVisible(isVisible);
    edInput1Label->setVisible(isVisible);
    lbInput2->setVisible(isVisible);
    edInput2->setVisible(isVisible);
    edInput2Label->setVisible(isVisible);
    lbValue1Value->setVisible(isVisible);
    lbValue1Caption->setVisible(isVisible);
    lbValue2Value->setVisible(isVisible);
    lbValue2Caption->setVisible(isVisible);
    break;
  default:
    break;

  }
}


void ProductionFrm::DisplayEditFields()
/*-----------------------------------------------------*/
/* Display all the edit field in the production screen */
/*-----------------------------------------------------*/
{
  bool edit;

  /*----- Fields are editable only in minimum mode -----*/
  edit = (curState == mvcDSminimum);

  SetEditField(ftAll,false,false);
  btTachoSet->setVisible(false);
  btRpmSet1->setVisible(false);
  switch(glob->mstCfg.mode) {

  case modInject:
    /* Pre-Cal is identical for ttTimer and ttRelay */
    // Timer act is always visible
    SetEditField(ftColPct,edit,edit);

    //@@ TWIN heeft nog implementatie nodig
    //@@@ MULTI UNIT
#warning RHRHRH TEST TEST TEST
    //        if (!(glob->mstSts.isTwin)) {
    switch(glob->sysCfg[glob->ActUnitIndex()].GraviRpm) {

    case grGravi:
      isRpmMode = false;
      if (curState >= mvcDSpreCal) {
        if (curState == mvcDSpreCal) {
          SetEditField(ftColPct,true,true);
          SetEditField(ftShotTime,true,true);
          SetEditField(ftShotWeight,true,true);
        }
        if (curState == mvcDSpreCal2) {
          SetEditField(ftColPct,true,false);
          SetEditField(ftShotTime,true,false);
          SetEditField(ftShotWeight,true,false);
        }
      }
      else {
        /*----- No act values in minimum mode -----*/
        if (curState == mvcDSminimum) {
          SetEditField(ftShotWeight,true,edit);
          SetEditField(ftShotTime,(glob->mstCfg.inpType == ttTimer),edit);
        }
        else {
          SetEditField(ftTimeAct,true,false);
          switch (glob->mstCfg.inpType) {
          case ttTimer:
            SetEditField(ftShotWeight,true,edit);
            SetEditField(ftShotTime,true,edit);
            break;

          case ttRelay:
            SetEditField(ftShotWeight,true,edit);
            SetEditField(ftShotTime,true,false);
            break;
          default:
            break;
          }
        }
      }
      break;

    case grRpm:
      isRpmMode = true;
      if (curState >= mvcDSpreCal) {
        if (curState == mvcDSpreCal) {
          SetEditField(ftColPct,true,true);
          SetEditField(ftShotTime,true,true);
        }
        if (curState == mvcDSpreCal2) {
          SetEditField(ftColPct,true,false);
          SetEditField(ftShotTime,true,false);
        }
      }
      else {
        SetEditField(ftTimeAct,true,false);
        switch(glob->mstCfg.inpType) {
        /* @@@NEW (22/04/2011) Relay display equals timer display */
        case ttTimer:
        case ttRelay:
          SetEditField(ftShotTime,true,edit);
          break;
        default:
          break;
        }
      }
      break;
    default :
      break;
    }
    //        }
    break;

  case modExtrude:
    switch(glob->sysCfg[glob->ActUnitIndex()].GraviRpm) {
    case grGravi:
      isRpmMode = false;

      if (curState >= mvcDSpreCal) {
        if (curState == mvcDSpreCal) {
          SetEditField(ftColPct,true,true);
          SetEditField(ftExtCap,true,true);
        }
        if (curState == mvcDSpreCal2) {
          SetEditField(ftColPct,true,false);
          SetEditField(ftExtCap,true,false);
        }
      }
      else {
        SetEditField(ftColPct,edit,edit);
        switch(glob->mstCfg.inpType) {
        case ttRelay:
          SetEditField(ftExtCap,true,edit);
          break;

        case ttTacho:
          /*----- No act values in minimum mode -----*/
          if (curState == mvcDSminimum) {
            SetEditField(ftExtCapAct,true,false);
            SetEditField(ftTachoAct,false,false);
          }
          else {
            SetEditField(ftExtCapAct,true,false);
            SetEditField(ftTachoAct,(!edit),false);
          }

          if (glob->GetUser() >= utTooling) {
            btTachoSet->setVisible((curState == mvcDSminimum) ? true : false);
          }
          else btTachoSet->setVisible(false);
          break;

        default:
          break;
        }
      }
      break;

    case grRpm:
      isRpmMode = true;
      switch(glob->mstCfg.inpType) {
      case ttRelay:
        SetEditField(ftColPct,edit,edit);
        break;
      case ttTacho:
        SetEditField(ftColPct,false,false);

        /*----- No act values in minimum mode -----*/
        if (curState == mvcDSminimum) {
          SetEditField(ftTachoAct,false,false);
        }
        else {
          SetEditField(ftTachoAct,(!edit),false);
        }

        btRpmSet1->setVisible(edit);
        if (glob->GetUser() >= utTooling) {
          btTachoSet->setVisible((curState == mvcDSminimum) ? true : false);
        }
        else btTachoSet->setVisible(false);
        break;

      default :
        break;
      }
      break;

    default:
      break;

    }
    break;
  default:
    break;

  }
}


void ProductionFrm::btDisplayClicked()
/*-------------------------------*/
/* Button detail display pressed */
/*-------------------------------*/
{
  /* Check backlit, if active turn it on and ignore event */
  if (Rout::BacklitOn()) {
    return;
  }

  switch(curState) {
  case mvcDSminimum:                                                          // Least detail
    if (glob->mstSts.isTwin) {
      m_ui->mvcSystem->SetDisplayType(DISP_TYPE_TWIN);
    }
    else {
      if (glob->mstCfg.slaveCount != 0) {
        m_ui->mvcSystem->SetDisplayType(DISP_TYPE_MULTI_UNIT);
      }
      else {
        m_ui->mvcSystem->SetDisplayType(DISP_TYPE_SINGLE_UNIT);
      }
    }
    btMidDetail->setVisible(true);
    SetDisplayState(mvcDSmaximum);                                            // Medium detail is skipped
    break;

  case mvcDSmedium:                                                           // Medium detail
    if (glob->mstSts.isTwin) {
      m_ui->mvcSystem->SetDisplayType(DISP_TYPE_TWIN);
    }
    else {
      if (glob->mstCfg.slaveCount != 0) {
        m_ui->mvcSystem->SetDisplayType(DISP_TYPE_MULTI_UNIT);
      }
      else {
        m_ui->mvcSystem->SetDisplayType(DISP_TYPE_SINGLE_UNIT);
      }
    }
    btMidDetail->setVisible(true);
    SetDisplayState(mvcDSminimum);
    break;

  case mvcDSmaximum:                                                          // Max detail
    if (glob->mstSts.isTwin) {
      m_ui->mvcSystem->SetDisplayType(DISP_TYPE_TWIN);
    }
    else {
      if (glob->mstCfg.slaveCount != 0) {
        m_ui->mvcSystem->SetDisplayType(DISP_TYPE_MULTI_UNIT);
      }
      else {
        m_ui->mvcSystem->SetDisplayType(DISP_TYPE_SINGLE_UNIT);
      }
    }
    btMidDetail->setVisible(true);
    SetDisplayState(mvcDSminimum);
    break;

  case mvcDSpreCal:
    if (glob->mstSts.isTwin) {
      m_ui->mvcSystem->SetDisplayType(DISP_TYPE_MULTI_UNIT);
    }
    else {
      m_ui->mvcSystem->SetDisplayType(DISP_TYPE_SINGLE_UNIT);
      btMidDetail->setVisible(false);
    }

    SetDisplayState(curState);
    break;

  default:
    SetDisplayState(mvcDSminimum);
    btMidDetail->setVisible(true);
    break;
  }

  SetRecipeMaterialButtons();
  DisplayEditFields();
}


void ProductionFrm::SetRecipeMaterialButtons()
{
  // For all cases counts that btRecipe and btMaterial are not visible when curState != 0
  if (curState >= mvcDSpreCal) {
    btRecipe->setVisible(false);
    lbRecipe->setVisible(false);
    lbRecipeName->setVisible(false);
  }
  else {
    if (glob->mstCfg.rcpEnable) {
      // Show recipe button/label (depending on curState) and material label
      if (lbRecipe->text() != btRecipe->text()) {
        lbRecipe->setText(btRecipe->text());
      }
      btRecipe->setVisible((curState == mvcDSminimum ? true:false) && (!Rout::keyLock));
      lbRecipeName->setVisible(true);
      // Material = read only
      if (glob->sysCfg[glob->ActUnitIndex()].GraviRpm == grGravi) {
        m_ui->mvcSystem->SetMaterialVisible((curState == mvcDSminimum ? false : true));
        m_ui->mvcSystem->SetMaterialEditable(false);
      }
      else {
        m_ui->mvcSystem->SetMaterialVisible(false);
      }
    }
    else {
      btRecipe->setVisible(false);
      lbRecipe->setVisible(false);
      lbRecipeName->setVisible(false);

      if (glob->sysCfg[glob->ActUnitIndex()].GraviRpm == grGravi) {

        /*----- Material = editable (button) -----*/
        m_ui->mvcSystem->SetMaterialVisible(true);
        m_ui->mvcSystem->SetMaterialEditable((curState == mvcDSminimum) && !Rout::keyLock);
      }
      else {
        m_ui->mvcSystem->SetMaterialVisible(false);
      }
    }
  }
}


void ProductionFrm::btTachoClicked()
{
  if (tachoForm != 0) {
    tachoForm->show();
  }
}


void ProductionFrm::Material1Clicked()
{
  /*===========================*/
  /* Material 1 button clicked */
  /*===========================*/
  if (glob->mstSts.isTwin) {

    //---------------------------------------------------------------------
    // IF preCal is active, and material == "...", start keypad input.
    if (curState >= mvcDSpreCal) {
      // Set name for new material for pre-calibration.
      int idx = glob->ActUnitIndex();

      txtInput->SetMaxLength(MAX_LENGTH_FILENAME_INPUT);

      if (glob->sysCfg[idx].material.name == MATERIAL_EMPTY_NAME) {
        txtInput->SetText("");
      }
      else {
        QString mat;
        glob->GetDispMaterial(&mat,idx);
        txtInput->SetText(mat);
      }
      // Display keyboard input form
      txtInput->show();
      // Load default material in the sysCfg struct (prepare for preCal).
    }
    //----------------------------------------------------------------------
    else {
      glob->MstSts_SetActUnitIndex(0);
      if (glob->sysSts[0].MCStatus >= mcsStandby) {
        ((MainMenu*)(glob->menu))->DisplayWindow(wiMaterialCalibOnline);
      }
      else {
        emit MaterialSelected(glob->sysCfg[0].material.name);
        ((MainMenu*)(glob->menu))->DisplayWindow(wiMaterials,wiHome);
      }
    }
  }
  else {
    // IF preCal is active, and material == "...", start keypad input.
    if (curState >= mvcDSpreCal) {
      // Set name for new material for pre-calibration.
      int idx = glob->ActUnitIndex();

      txtInput->SetMaxLength(MAX_LENGTH_FILENAME_INPUT);

      if (glob->sysCfg[idx].material.name == MATERIAL_EMPTY_NAME) {
        txtInput->SetText("");
      }
      else {
        QString mat;
        glob->GetDispMaterial(&mat,idx);
        txtInput->SetText(mat);
      }
      // Display keyboard input form
      txtInput->show();
      // Load default material in the sysCfg struct (prepare for preCal).
    }
    else {
      // If the current unit is active, open learnOnline form
      if (glob->sysSts[glob->ActUnitIndex()].MCStatus >= mcsStandby) {
        ((MainMenu*)(glob->menu))->DisplayWindow(wiMaterialCalibOnline);
      }
      else {                                                                    // Else (unit is off), open material selection form
        emit MaterialSelected(glob->sysCfg[glob->ActUnitIndex()].material.name);
        ((MainMenu*)(glob->menu))->DisplayWindow(wiMaterials,wiHome);
      }
    }
  }
}


void ProductionFrm::Material2Clicked()
{
  /*=================================================*/
  /* Material 2 button clicked (Active in TWIN mode) */
  /*=================================================*/
  glob->MstSts_SetActUnitIndex(1);
  if (glob->sysSts[1].MCStatus >= mcsStandby) {
    ((MainMenu*)(glob->menu))->DisplayWindow(wiMaterialCalibOnline);
  }
  else {
    emit MaterialSelected(glob->sysCfg[1].material.name);
    ((MainMenu*)(glob->menu))->DisplayWindow(wiMaterials,wiHome);
  }
}


void ProductionFrm::RecipeClicked()
/*-----------------------*/
/* Recipe button clicked */
/*-----------------------*/
{
  RecipeStruct rcp;

  /* Check backlit, if active turn it on and ignore event */
  if (Rout::BacklitOn()) {
    return;
  }

  if (recipeChanged) {
    ((MainMenu*)(glob->menu))->recipeConfigForm->SaveRecipe(&rcp,false,true);
    ResetRecipeDataChanged();
  }
  else {
    /*----- Unit on : Switch to CalibOnLine screen -----*/
    if (glob->sysSts[glob->ActUnitIndex()].MCStatus >= mcsStandby) {
      ((MainMenu*)(glob->menu))->DisplayWindow(wiMaterialCalibOnline);
    }
    else {
      /*----- Unit off :  Open RecipeSelect form with current recipe selected -----*/
      emit RecipeSelected(glob->mstCfg.recipe);
    }
  }

}


void ProductionFrm::PositionProdStatus()
{
  int y = 30;
  int x = 50;
  lbProdStatus1->move(x,y);

  x = 575;
  lbProdStatus2->move(x,lbProdStatus1->pos().y());
}


void ProductionFrm::SetInitState()
{
  PositionProdStatus();
  int idx = glob->ActUnitIndex();
  m_ui->mvcSystem->SetDispIdx(idx);
  MstCfg_ShotUnitsChanged();
  MstCfg_TimeUnitsChanged();

  RecipeChanged(glob->mstCfg.recipe);

  // Learn busy/ready indication
  if (glob->mstSts.isTwin) {
    SysSts_LearnStsChanged(glob->sysSts[0].balanceCalibrated,0);
    SysSts_LearnStsChanged(glob->sysSts[1].balanceCalibrated,1);
  }
  else {
    SysSts_LearnStsChanged(glob->sysSts[idx].balanceCalibrated,idx);
  }

  if (curState != mvcDSpreCal) {
    curState = mvcDSmaximum;
    RcpEnabledChanged(false);
    SetBalanceCalibratedVisible(true);
  }
  else {
    RcpEnabledChanged(glob->mstCfg.rcpEnable);
    SetBalanceCalibratedVisible(false);
  }
  btDisplayClicked();
}


void ProductionFrm::SetBalanceCalibratedVisible(bool visible)
{
  // Wel of niet weergeven van de Calibrating/Calibrated status.
  showProdSts = visible;

  if (glob->mstSts.isTwin) {
    MCStatusChanged(glob->sysSts[0].MCStatus,0);
    MCStatusChanged(glob->sysSts[1].MCStatus,1);
  }
  else {
    int idx = glob->ActUnitIndex();
    MCStatusChanged(glob->sysSts[idx].MCStatus,idx);
  }
}


void ProductionFrm::MCStatusChanged(eMCStatus _sts, int idx)
{
  /*----------------------------------*/
  /* Unit status changed : LEARN / OK */
  /*----------------------------------*/
  /*----- Update status unit 1 -----*/
  if (idx == 0) {
    if (((showProdSts) && (_sts > mcsOff)) && (glob->sysCfg[idx].GraviRpm == grGravi)) {
      lbProdStatus1->setVisible(true);
    }
    else lbProdStatus1->setVisible(false);
  }

  /*----- Update status unit 2 -----*/
  if (glob->mstSts.isTwin) {
    if (idx == 1) {
      if (((showProdSts) && (_sts > mcsOff)) && (glob->sysCfg[idx].GraviRpm == grGravi)) {
        lbProdStatus2->setVisible(true);
      }
      else lbProdStatus2->setVisible(false);
    }
  }
  else {
    lbProdStatus2->setVisible(false);
  }
}


void ProductionFrm::MstCfg_UserChanged(eUserType ut)
{
  if (ut >= utTooling) {
    if (glob->mstCfg.inpType == ttTacho) {
      btTachoSet->setVisible((curState == mvcDSminimum) ? true : false);
    }
  }
  else {
    btTachoSet->setVisible(false);
  }
}


void ProductionFrm::RcpEnabledChanged(bool)
{
  if (curState != mvcDSpreCal) {
    SetRecipeMaterialButtons();
  }
}


void ProductionFrm::MstCfg_ShotUnitsChanged()
{
  // call: ShotWeight changed
  //@@ Aanpassen naar juiste production modus
  SysCfg_ShotWeightChanged(glob->sysCfg[glob->ActUnitIndex()].injection.shotWeight,glob->ActUnitIndex());
}


void ProductionFrm::MstCfg_TimeUnitsChanged()
{
  // call: ShotTime changed
  //@@ Aanpassen naar juiste production modus
  SysCfg_ShotTimeChanged(glob->sysCfg[glob->ActUnitIndex()].injection.shotTime,glob->ActUnitIndex());
}


// TODO: mogelijk nog een eigen event implementeren voor extruder capacity
void ProductionFrm::SysCfg_ShotWeightChanged(float _wght, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    //edInput2->setText(QString::number(glob->sysSts[idx].shotWeight,'f',1)+QString(" ")+glob->extrUnits);
    if (glob->mstCfg.mode == modInject) {
      edInput2->setText(QString::number(_wght,glob->formats.ShotWeightFmt.format.toAscii(),glob->formats.ShotWeightFmt.precision) + QString(" ")+glob->weightUnits);
      edInput2Label->setText(edInput2->text());
    }
    if (glob->mstCfg.mode == modExtrude) {
      edInput2->setText(QString::number(_wght,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision) + QString(" ")+glob->extrUnits);
      edInput2Label->setText(edInput2->text());
    }
  }
}


// TODO: mogelijk nog een eigen event implementeren voor maxTacho
void ProductionFrm::SysCfg_ShotTimeChanged(float _time, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    if (glob->mstCfg.mode == modInject) {
      if (curState >= mvcDSpreCal) {
        if (curState == mvcDSpreCal) {
          edInput1->setText(QString::number(_time,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) +QString(" ") + glob->timerUnits);
          edInput1Label->setText(edInput1->text());
        }
      }
      else {
        if ((glob->mstCfg.inpType != ttRelay) || (glob->sysCfg[idx].GraviRpm == grRpm)) {
          edInput1->setText(QString::number(_time,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) +QString(" ") + glob->timerUnits);
          edInput1Label->setText(edInput1->text());
        }
      }
    }
  }
}


void ProductionFrm::SysSts_ShotTimeChanged(float _time, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    if (glob->mstCfg.mode == modInject) {
      lbValue2Value->setText(QString::number(_time,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) + QString(" ") + glob->timerUnits);
    }
  }
}


void ProductionFrm::SysSts_MeteringActChanged(float time, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    if (glob->mstCfg.mode == modInject) {
      if ((glob->mstCfg.inpType == ttRelay) && (glob->sysCfg[idx].GraviRpm == grGravi)) {
        if (curState < mvcDSpreCal) {
          edInput1->setText(QString::number(time,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) +QString(" ") + glob->timerUnits);
          edInput1Label->setText(edInput1->text());
          lbValue1Value->setText(QString::number(time,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) +QString(" ") + glob->timerUnits);
        }
      }
    }
  }
}


void ProductionFrm::SysCfg_MeteringStartChanged(float time, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    if (glob->mstCfg.mode == modInject) {
      if (glob->mstCfg.inpType == ttRelay) {
        if (curState < mvcDSpreCal) {
          edInput1->setText(QString::number(time,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) + QString(" ") + glob->timerUnits);
          lbInput1->setText(edInput1->text());
        }
      }
    }
  }
}


void ProductionFrm::SysSts_MeteringValidChanged(bool, int) {}

void ProductionFrm::SysCfg_ExtCapChanged(float _cap, int idx)
{

  if (idx == glob->ActUnitIndex()) {
    if (glob->mstCfg.mode == modExtrude) {
      edInput2->setText(QString::number(_cap,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision) + QString(" ")+glob->extrUnits);
      edInput2Label->setText(edInput2->text());
    }
  }
}


void ProductionFrm::SysSts_ExtCapChanged(float cap, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    if (glob->mstCfg.mode == modExtrude) {
      lbValue1Value->setText(QString::number(cap,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision)+QString(" ")+glob->extrUnits);
    }
  }
}


void ProductionFrm::TachoChanged(float _tacho, int idx)
{
  /*---------------------*/
  /* Tacho value changed */
  /*---------------------*/
  int precision;
  if (idx == glob->ActUnitIndex()) {
    if (glob->mstCfg.mode == modExtrude) {

      /*----- Setup tacho field precision depending on actual voltage -----*/
      if (_tacho < 10.0) {
        precision = 2;                                                          /* 2 digits after the comma when < 10.0V */
      }
      else {
        precision = 1;                                                          /* 1 digit after the comma when >= 10.0V */
      }
      lbValue2Value->setText(QString::number(_tacho,glob->formats.TachoFmt.format.toAscii(),precision)+QString(" ")+glob->tachoUnits);
    }
  }
}


void ProductionFrm::SysSts_LearnStsChanged(bool calibrated, int idx)
{
  QString txt;
  if (glob->mstSts.isTwin) {
    if (idx == 0) {
      if (calibrated == false) {
        ((MainMenu*)(glob->menu))->alarmHndlr->GetMessageText(TXT_INDEX_LEARN_BUSY,0,0,&txt);
        lbProdStatus1->setText(txt);
        lbProdStatus1->setAutoFillBackground(true);
        lbProdStatus1->setStyleSheet("background-color: #FFFF55; border-radius: 5px; border: 1px solid #FFFF55");
      }
      else {
        ((MainMenu*)(glob->menu))->alarmHndlr->GetMessageText(TXT_INDEX_LEARN_READY,0,0,&txt);
        lbProdStatus1->setText(txt);
        lbProdStatus1->setAutoFillBackground(true);
        lbProdStatus1->setStyleSheet("background-color: #55FF55; border-radius: 5px; border: 1px solid #55FF55");
      }
    }
    if (idx == 1) {
      if (calibrated == false) {
        ((MainMenu*)(glob->menu))->alarmHndlr->GetMessageText(TXT_INDEX_LEARN_BUSY,0,0,&txt);
        lbProdStatus2->setText(txt);
        lbProdStatus2->setAutoFillBackground(true);
        lbProdStatus2->setStyleSheet("background-color: #FFFF55; border-radius: 5px; border: 1px solid #FFFF55");
      }
      else {
        ((MainMenu*)(glob->menu))->alarmHndlr->GetMessageText(TXT_INDEX_LEARN_READY,0,0,&txt);
        lbProdStatus2->setText(txt);
        lbProdStatus2->setAutoFillBackground(true);
        lbProdStatus2->setStyleSheet("background-color: #55FF55; border-radius: 5px; border: 1px solid #55FF55");
      }
    }
  }
  else {
    if (idx == glob->ActUnitIndex()) {
      if (calibrated == false) {
        ((MainMenu*)(glob->menu))->alarmHndlr->GetMessageText(TXT_INDEX_LEARN_BUSY,0,0,&txt);
        lbProdStatus1->setText(txt);
        lbProdStatus1->setAutoFillBackground(true);
        lbProdStatus1->setStyleSheet("background-color: #FFFF55; border-radius: 5px; border: 1px solid #FFFF55");
      }
      else {
        ((MainMenu*)(glob->menu))->alarmHndlr->GetMessageText(TXT_INDEX_LEARN_READY,0,0,&txt);
        lbProdStatus1->setText(txt);
        lbProdStatus1->setAutoFillBackground(true);
        lbProdStatus1->setStyleSheet("background-color: #55FF55; border-radius: 5px; border: 1px solid #55FF55");
      }
    }
  }
}


void ProductionFrm::ValueReceived(float val)
{
  /*-------------------------------------------------------------------------*/
  /* Value received                                                          */
  /* When data is entered from the numeric keyboard this funtion is signaled */
  /* In TWIN mode : Unit 0 and unit 1 have the same global production data   */
  /*-------------------------------------------------------------------------*/
  if (editIdx != -1) {
    switch(editIdx) {
    /*----- Shotweight / Ext.cap entry mode -----*/
    case 0:
      switch (glob->mstCfg.mode) {
      case modInject:
        if(glob->mstSts.isTwin) {
          glob->SysCfg_SetShotWeight(val,0);
          glob->SysCfg_SetShotWeight(val,1);
        }
        else {
          glob->SysCfg_SetShotWeight(val,glob->ActUnitIndex());
        }
        break;
      case modExtrude:
        if(glob->mstSts.isTwin) {
          glob->SysCfg_SetExtCap(val,0);
          glob->SysCfg_SetExtCap(val,1);
        }
        else {
          glob->SysCfg_SetExtCap(val,glob->ActUnitIndex());
        }
        break;
      default:
        break;
      }
      break;
      /*----- Shottime / Tacho entry mode -----*/
    case 1:
      switch (glob->mstCfg.mode) {
      case modInject:
        if(glob->mstSts.isTwin) {
          glob->SysCfg_SetShotTime(val,0);
          glob->SysCfg_SetShotTime(val,1);
        }
        else {
          glob->SysCfg_SetShotTime(val,glob->ActUnitIndex());
        }
        break;
      case modExtrude:
        if(glob->mstSts.isTwin) {
          glob->SysCfg_SetMaxTacho(val,0);
          glob->SysCfg_SetMaxTacho(val,1);
        }
        else {
          glob->SysCfg_SetMaxTacho(val,glob->ActUnitIndex());
        }
        break;
      default:
        break;
      }
      break;
    }
    /*----- When data is entered, change the recipe icon into the "Save recipe" icon
              if the unit is active -----*/
    SetRecipeDataChanged();
    editIdx = -1;
  }
}


void ProductionFrm::TextReceived(const QString &txt)
{
  int idx;
  /*---- Input from the keyboard received, Set the text on the material button -----*/
  idx = glob->ActUnitIndex();
  glob->SysCfg_SetMaterial((txt+MATERIAL_EXTENSION),false,idx,false);
}


void ProductionFrm::ModeChanged(eMode)
{
  SetupDisplay();
}


void ProductionFrm::GraviRpmChanged(eGraviRpm,int)
{
  SetupDisplay();
}


void ProductionFrm::TypeChanged(eType)
{
  SetupDisplay();
}


void ProductionFrm::RecipeChanged(const QString &rcp)
{
  QString ext = QString(RECIPE_EXTENSION);
  QString tmpRcp;
  tmpRcp = rcp.left(rcp.length()-ext.length());
  lbRecipeName->setText(tmpRcp);

  /* Reset the recipe button image to the normal recipe button image */
  ResetRecipeDataChanged();
}


void ProductionFrm::SetupDisplay()
{
  /*---------------------------------------------------------------------------------------------*/
  /* SetupDisplay set all correct texts for all fields for the selected mode                     */
  /* and positions them at the desired place.                                                    */
  /*                                                                                             */
  /* It also calls the correct procedures to update all values in case of a mode switch          */
  /*                                                                                             */
  /* DisplayEditFields() handles the visibility of all the fields and buttons on the screen      */
  /*---------------------------------------------------------------------------------------------*/

  double y = 0;
  double xMid = 0;
  double x = 0;

  int row = 0;

  y = 390;
  xMid = ((double)(this->width()/2));

  x = (xMid-(btMidDetail->width()/2));
  btMidDetail->move((int)x,(int)y);

  y = (5);
  x = (xMid - (btTachoSet->width()/2));
  btTachoSet->move((int)x,(btMidDetail->pos().y()-(btTachoSet->height()+6)));

  eMode mod = glob->mstCfg.mode;                                                // Inject/Extrude

  x = (xMid - (btRecipe->width()/2));
  btRecipe->move((int)x,(int)y);
  lbRecipe->move(btRecipe->pos());
  lbRecipeName->move(btRecipe->pos().x()-105,(btRecipe->pos().y()+btRecipe->height()));

  {

    /*
      if (glob->isTwin)
      {
          //@@ TODO: Dual unit implementation required
      }
      else
      */
    eGraviRpm graviRpm = glob->sysCfg[glob->ActUnitIndex()].GraviRpm;           // Gravi / RPM
    eType type = glob->mstCfg.inpType;                                          // Timer / Relay / Tacho / MC_ID

    switch(mod) {
    case modExtrude:                                                          // Extrusion mode (default display mode)
      this->lbInput1->setText(tr("Tacho set"));
      this->lbValue2Caption->setText(tr("Tacho act"));

      switch(graviRpm) {
      case grGravi:
        // settings that are identical for ttRelay and ttTacho. (Might change when MCID is added)
        if (glob->mstSts.isTwin) {
          SysCfg_ExtCapChanged(glob->sysCfg[0].extrusion.extCapacity,0);
          SysCfg_ExtCapChanged(glob->sysCfg[1].extrusion.extCapacity,1);

          TachoChanged(glob->sysSts[1].extrusion.tachoAct,0);
          TachoChanged(glob->sysSts[0].extrusion.tachoAct,1);
        }
        else {
          SysCfg_ExtCapChanged(glob->sysCfg[glob->ActUnitIndex()].extrusion.extCapacity,glob->ActUnitIndex());
          TachoChanged(glob->sysSts[glob->ActUnitIndex()].extrusion.tachoAct,glob->ActUnitIndex());
        }
        this->lbInput2->setText(tr("Ext Cap set"));
        this->lbValue1Caption->setText(tr("Extruder act"));

        switch (type) {
        case ttTimer:                                                     // GRAVI / EXT / TIMER (Does not exist)
          row = 0;
          break;
        case ttRelay:                                                     // GRAVI / EXT / RELAY
          row |= 0x02;
          break;
        case ttTacho:                                                     // GRAVI / EXT / TACHO
          if (curState >= mvcDSpreCal) {
            if (curState == mvcDSpreCal) {
              row |= 0x02;
            }
          }
          else {
            row |= 0x04;
            row |= 0x08;
          }
          break;

        default:
          // EMPTY STATEMENT
          break;
        }
        break;

      case grRpm:                                                           // EXT / RPM is identical to Ext/Gravi mode (same case statement)
        // Settings for both ttRelay and ttTacho, might change when MCID is added!
        this->lbInput2->setText(tr("rpm set"));
        showEdit2 = true;
        showTachoButton = false;

        switch (type) {
        case ttTimer:                                                     // RPM / EXT / TIMER
          // DOES NOT EXIST
          break;
        case ttRelay:                                                     // RPM / EXT / RELAY
          // Set RPM
          showEdit1 = false;
          row |= 0x02;
          break;

        case ttTacho:                                                     // RPM / EXT / TACHO
          // SetRPM + Tacho
          showEdit1 = false;
          row |= 0x04;
          row |= 0x08;
          break;

        default:
          // EMPTY STATEMENT
          break;
        }
        break;

      default:
        // EMPTY STATEMENT
        break;
      }
      break;

    case modInject:                                                           // Injection mode
      lbInput1->setText(tr("set time","label"));
      lbValue2Caption->setText(tr("act time","label"));

      switch(graviRpm) {
      case grGravi:
        this->lbInput2->setText(tr("shot weight"));

        if (glob->mstSts.isTwin) {
          SysCfg_ShotWeightChanged(glob->sysCfg[0].injection.shotWeight,0);
          SysCfg_ShotWeightChanged(glob->sysCfg[1].injection.shotWeight,1);

          SysSts_ShotTimeChanged(glob->sysSts[0].injection.shotTimeAct,0);
          SysSts_ShotTimeChanged(glob->sysSts[1].injection.shotTimeAct,1);
        }
        else {
          SysCfg_ShotWeightChanged(glob->sysCfg[glob->ActUnitIndex()].injection.shotWeight,glob->ActUnitIndex());
          SysSts_ShotTimeChanged(glob->sysSts[glob->ActUnitIndex()].injection.shotTimeAct,glob->ActUnitIndex());
        }

        switch (type) {
        case ttTimer:                                                     // GRAVI/INJ/TIMER
          // Shotweight + Dos.Time + Color %
          if (glob->mstSts.isTwin) {
            SysCfg_ShotTimeChanged(glob->sysCfg[0].injection.shotTime,0);
            SysCfg_ShotTimeChanged(glob->sysCfg[1].injection.shotTime,1);
          }
          else {
            int idx = glob->ActUnitIndex();
            SysCfg_ShotTimeChanged(glob->sysCfg[idx].injection.shotTime,idx);
          }

          lbInput1->setText(tr("set time","label"));

          if (curState >= mvcDSpreCal) {
            row |= 0x03;
          }
          else {
            row |= 0x0B;
          }
          break;

        case ttRelay:                                                     // GRAVI / INJ / RELAY
          // Shotweight + Dos.Time + Color %
          // Dos.Time is read-only want die wordt door het proces bepaald.

          if (glob->mstSts.isTwin) {
            SysSts_MeteringActChanged(glob->sysSts[0].injection.meteringAct,0);
            SysSts_MeteringActChanged(glob->sysSts[1].injection.meteringAct,1);
          }
          else {
            SysSts_MeteringActChanged(glob->sysSts[glob->ActUnitIndex()].injection.meteringAct,glob->ActUnitIndex());
          }

          // relay time (dos time set)
          lbInput1->setText(tr("relay time","label"));

          if (curState >= mvcDSpreCal) {
            row |= 0x03;
          }
          else {
            row |= 0x0B;
          }
          break;
        case ttTacho:                                                     // GRAVI / INJ / TACHO does not exist
          // ONGELDIGE KEUZE
          break;
        default:
          break;
        }
        break;

      case grRpm:                                                           // RPM / INJ / TIMER
        // RPM + dos.time
        switch(type) {
        case ttTimer:
        case ttRelay:
          if (glob->mstSts.isTwin) {
            SysCfg_ShotTimeChanged(glob->sysCfg[0].injection.shotTime,0);
            SysCfg_ShotTimeChanged(glob->sysCfg[1].injection.shotTime,1);

            SysSts_ShotTimeChanged(glob->sysSts[0].injection.shotTimeAct,0);
            SysSts_ShotTimeChanged(glob->sysSts[1].injection.shotTimeAct,1);

            m_ui->mvcSystem->SetRPM(glob->sysCfg[0].setRpm,0);
            m_ui->mvcSystem->SetRPM(glob->sysCfg[1].setRpm,1);
          }
          else {
            int idx = glob->ActUnitIndex();

            SysCfg_ShotTimeChanged(glob->sysCfg[idx].injection.shotTime,idx);
            SysSts_ShotTimeChanged(glob->sysSts[idx].injection.shotTimeAct,idx);

            m_ui->mvcSystem->SetRPM(glob->sysCfg[idx].setRpm,idx);
          }

          if (curState >= mvcDSpreCal) {
            row |= 0x03;
          }
          else {
            row |= 0x0B;
          }
          break;
        default:
          break;

        }
        break;
      default:
        break;
      }
      break;
    default:
      break;
    }
  }

  DisplayEditFields();

  if (curState >= mvcDSpreCal) {
    btMidDetail->setVisible(false);
  }
  else {
    btMidDetail->setVisible(true);
  }

  y += 110;

  if ((row & 0x02) != 0x00) {
    x = xMid - (lbInput2->width()/2);
    lbInput2->move((int)x,(int)y);
    y += lbInput2->height();

    x = xMid - (edInput2->width()/2);
    edInput2->move((int)x,(int)y);
    edInput2Label->move(edInput2->pos());
    y += (edInput2->height() + PROD_V_OFFSET);
  }

  if ((row & 0x01) != 0x00) {
    x = (xMid - (lbInput1->width()/2));
    lbInput1->move((int)x,(int)y);
    y += lbInput1->height();

    x = (xMid - (edInput1->width()/2));
    edInput1->move((int)x,(int)y);
    edInput1Label->move(edInput1->pos());
    y += (edInput1->height() + PROD_V_OFFSET);
  }

  x = xMid - (lbValue1Caption->width()/2);
  if ((row & 0x04) != 0x00) {
    if ((mod == modExtrude) && (glob->mstCfg.inpType == ttTacho)) {
      y += 50;
    }
    lbValue1Caption->move((int)x,(int)y);
    y += lbValue1Caption->height();
    lbValue1Value->move((int)x,(int)y);
    y += (lbValue1Value->height() + PROD_V_OFFSET);
  }

  x = xMid - (lbValue2Caption->width()/2);
  if ((row & 0x08) != 0) {
    lbValue2Caption->move((int)x,(int)y);
    y += lbValue2Caption->height();
    lbValue2Value->move((int)x,(int)y);
    y += (lbValue2Value->height() + PROD_V_OFFSET);
  }
  SetRecipeMaterialButtons();
}


void ProductionFrm::SlaveCountChanged(int cnt)
{
  if(cnt == 1) {
    m_ui->mvcSystem->SetDisplayType(DISP_TYPE_TWIN);
  }
  else {
    if (glob->mstCfg.slaveCount != 0) {
      m_ui->mvcSystem->SetDisplayType(DISP_TYPE_MULTI_UNIT);
    }
    else {
      m_ui->mvcSystem->SetDisplayType(DISP_TYPE_SINGLE_UNIT);
    }
  }
  SetInitState();
}


void ProductionFrm::ResetCalibValues()
{
  if (calibForm != 0) {
    calibForm->SetCurrentProdSettings(false);
  }
}


void ProductionFrm::ActUnitIndexChanged(int)
{
  SetInitState();
}

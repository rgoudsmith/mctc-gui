/********************************************************************************
** Form generated from reading UI file 'Form_UsbSelect.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_USBSELECT_H
#define UI_FORM_USBSELECT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_USBSelectFrm
{
public:
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer;
    QLabel *lbTitle;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_4;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QPushButton *btUSB;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QPushButton *btCtrl;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_4;
    QPushButton *btRemove;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btOk;

    void setupUi(QWidget *USBSelectFrm)
    {
        if (USBSelectFrm->objectName().isEmpty())
            USBSelectFrm->setObjectName(QString::fromUtf8("USBSelectFrm"));
        USBSelectFrm->resize(625, 480);
        USBSelectFrm->setCursor(QCursor(Qt::BlankCursor));
        verticalLayout_2 = new QVBoxLayout(USBSelectFrm);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        lbTitle = new QLabel(USBSelectFrm);
        lbTitle->setObjectName(QString::fromUtf8("lbTitle"));

        horizontalLayout_4->addWidget(lbTitle);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(USBSelectFrm);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        btUSB = new QPushButton(USBSelectFrm);
        btUSB->setObjectName(QString::fromUtf8("btUSB"));
        btUSB->setMinimumSize(QSize(90, 70));
        btUSB->setMaximumSize(QSize(90, 70));
        btUSB->setText(QString::fromUtf8(""));

        horizontalLayout->addWidget(btUSB);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(USBSelectFrm);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        btCtrl = new QPushButton(USBSelectFrm);
        btCtrl->setObjectName(QString::fromUtf8("btCtrl"));
        btCtrl->setMinimumSize(QSize(90, 70));
        btCtrl->setMaximumSize(QSize(90, 70));
        btCtrl->setText(QString::fromUtf8(""));

        horizontalLayout_2->addWidget(btCtrl);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));

        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_4 = new QLabel(USBSelectFrm);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_9->addWidget(label_4);

        btRemove = new QPushButton(USBSelectFrm);
        btRemove->setObjectName(QString::fromUtf8("btRemove"));
        btRemove->setMinimumSize(QSize(90, 70));
        btRemove->setMaximumSize(QSize(90, 70));

        horizontalLayout_9->addWidget(btRemove);


        verticalLayout->addLayout(horizontalLayout_9);


        horizontalLayout_6->addLayout(verticalLayout);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);


        verticalLayout_2->addLayout(horizontalLayout_6);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);

        btOk = new QPushButton(USBSelectFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("ok"));

        horizontalLayout_5->addWidget(btOk);


        verticalLayout_2->addLayout(horizontalLayout_5);


        retranslateUi(USBSelectFrm);

        QMetaObject::connectSlotsByName(USBSelectFrm);
    } // setupUi

    void retranslateUi(QWidget *USBSelectFrm)
    {
        USBSelectFrm->setWindowTitle(QApplication::translate("USBSelectFrm", "Form", 0, QApplication::UnicodeUTF8));
        lbTitle->setText(QApplication::translate("USBSelectFrm", "USB Options", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("USBSelectFrm", "Copy files from USB to Controller", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("USBSelectFrm", "Copy files from Controller to USB", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("USBSelectFrm", "Remove USB stick", 0, QApplication::UnicodeUTF8));
        btRemove->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class USBSelectFrm: public Ui_USBSelectFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_USBSELECT_H

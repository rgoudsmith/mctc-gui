#include "Form_WeightCheck.h"
#include "ui_Form_WeightCheck.h"
#include "Form_MainMenu.h"
#include "Rout.h"

WeightCheckFrm::WeightCheckFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::WeightCheckFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);

  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
}


WeightCheckFrm::~WeightCheckFrm()
{
  delete ui;
}


void WeightCheckFrm::changeEvent(QEvent *e)
{
  QBaseWidget::changeEvent(e);
  if (e->type() == QEvent::LanguageChange) {
    ui->retranslateUi(this);
  }
}


void WeightCheckFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;
  glob->SetButtonImage(ui->btOk,itSelection,selOk);

  ui->lbCaption->setFont(*(glob->captionFont));
  ui->lbValVal->setFont(*(glob->baseFont));
  ui->lbValCap->setFont(*(glob->baseFont));
  ui->lbIDCap->setFont(*(glob->baseFont));
  ui->lbIDVal->setFont(*(glob->baseFont));

  connect(glob,SIGNAL(SysSts_FastMassChanged(float,int)),this,SLOT(SysSts_FastMassChanged(float,int)));
  connect(glob,SIGNAL(SysCfg_IDChanged(QString,int)),this,SLOT(McIdChanged(QString,int)));
  connect(glob,SIGNAL(MstSts_ActUnitChanged(int)),this,SLOT(ActUnitIdxChanged(int)));
  int idx = glob->ActUnitIndex();
  SysSts_FastMassChanged(glob->sysSts[idx].fastMass,idx);
  McIdChanged(glob->sysCfg[idx].MC_ID,idx);
  ActUnitIdxChanged(idx);
}


void WeightCheckFrm::SysSts_FastMassChanged(float _mass, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    ui->lbValVal->setText(QString::number(_mass,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision) + QString(" ")+ glob->weightUnits);
  }
}


void WeightCheckFrm::OkClicked()
{
  /*----------------------------------------------------*/
  /* OK button clicked : Switch back to the HOME screen */
  /*----------------------------------------------------*/
  disconnect(glob,SIGNAL(SysCfg_IDChanged(QString,int)),this,SLOT(McIdChanged(QString,int)));
  disconnect(glob,SIGNAL(MstSts_ActUnitChanged(int)),this,SLOT(ActUnitIdxChanged(int)));

  /*----- Switch to Home screen -----*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
}


void WeightCheckFrm::McIdChanged(const QString &id, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    ui->lbIDVal->setText(id);
  }
}


void WeightCheckFrm::ActUnitIdxChanged(int idx)
{
  McIdChanged(glob->sysCfg[idx].MC_ID,idx);
  SysSts_FastMassChanged(glob->sysSts[idx].fastMass,idx);
}

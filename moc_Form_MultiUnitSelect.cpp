/****************************************************************************
** Meta object code from reading C++ file 'Form_MultiUnitSelect.h'
**
** Created: Wed Feb 6 11:15:07 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_MultiUnitSelect.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_MultiUnitSelect.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MultiUnitSelectFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   19,   19,   19, 0x05,

 // slots: signature, parameters, type, tag, flags
      44,   19,   19,   19, 0x0a,
      69,   67,   19,   19, 0x0a,
     100,   96,   19,   19, 0x0a,
     142,   19,   19,   19, 0x0a,
     167,   19,   19,   19, 0x0a,
     188,   67,   19,   19, 0x0a,
     224,   19,   19,   19, 0x0a,
     244,  240,   19,   19, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MultiUnitSelectFrm[] = {
    "MultiUnitSelectFrm\0\0RecipeSelected(QString)\0"
    "SlaveCountChanged(int)\0,\0"
    "SysActiveChanged(bool,int)\0,,,\0"
    "McWeightCapChanged(float,float,float,int)\0"
    "ColPctClickReceived(int)\0ValueReceived(float)\0"
    "ComMsgReceived(tMsgControlUnit,int)\0"
    "RecipeClicked()\0rcp\0RecipeChanged(QString)\0"
};

const QMetaObject MultiUnitSelectFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_MultiUnitSelectFrm,
      qt_meta_data_MultiUnitSelectFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MultiUnitSelectFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MultiUnitSelectFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MultiUnitSelectFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MultiUnitSelectFrm))
        return static_cast<void*>(const_cast< MultiUnitSelectFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int MultiUnitSelectFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: RecipeSelected((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: SlaveCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: SysActiveChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: McWeightCapChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 4: ColPctClickReceived((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: ValueReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: ComMsgReceived((*reinterpret_cast< tMsgControlUnit(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: RecipeClicked(); break;
        case 8: RecipeChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void MultiUnitSelectFrm::RecipeSelected(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

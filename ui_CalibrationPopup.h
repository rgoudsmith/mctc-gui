/********************************************************************************
** Form generated from reading UI file 'CalibrationPopup.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CALIBRATIONPOPUP_H
#define UI_CALIBRATIONPOPUP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStackedWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CalibrationPopup
{
public:
    QVBoxLayout *verticalLayout_3;
    QStackedWidget *pageControl;
    QWidget *page;
    QVBoxLayout *verticalLayout;
    QLabel *lbTitle;
    QLabel *lbMessage;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *lbPrime;
    QPushButton *btOk;
    QPushButton *btCancel;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *lbTitle_2;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *btSave;
    QPushButton *btAbort;
    QPushButton *btContinue;
    QSpacerItem *horizontalSpacer_3;

    void setupUi(QWidget *CalibrationPopup)
    {
        if (CalibrationPopup->objectName().isEmpty())
            CalibrationPopup->setObjectName(QString::fromUtf8("CalibrationPopup"));
        CalibrationPopup->setWindowModality(Qt::ApplicationModal);
        CalibrationPopup->resize(509, 261);
        CalibrationPopup->setCursor(QCursor(Qt::SplitVCursor));
        CalibrationPopup->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout_3 = new QVBoxLayout(CalibrationPopup);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        pageControl = new QStackedWidget(CalibrationPopup);
        pageControl->setObjectName(QString::fromUtf8("pageControl"));
        pageControl->setCursor(QCursor(Qt::BlankCursor));
        pageControl->setStyleSheet(QString::fromUtf8("#pageControl{border: 5px solid black; border-radius:3px; }"));
        pageControl->setFrameShape(QFrame::NoFrame);
        pageControl->setLineWidth(0);
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        page->setCursor(QCursor(Qt::BlankCursor));
        page->setStyleSheet(QString::fromUtf8("background: white;"));
        verticalLayout = new QVBoxLayout(page);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lbTitle = new QLabel(page);
        lbTitle->setObjectName(QString::fromUtf8("lbTitle"));
        lbTitle->setMinimumSize(QSize(0, 40));
        lbTitle->setMaximumSize(QSize(16777215, 40));
        lbTitle->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lbTitle);

        lbMessage = new QLabel(page);
        lbMessage->setObjectName(QString::fromUtf8("lbMessage"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lbMessage->sizePolicy().hasHeightForWidth());
        lbMessage->setSizePolicy(sizePolicy);
        lbMessage->setMaximumSize(QSize(16777215, 16777215));
        lbMessage->setText(QString::fromUtf8("TextLabel"));
        lbMessage->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lbMessage);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        lbPrime = new QLabel(page);
        lbPrime->setObjectName(QString::fromUtf8("lbPrime"));

        horizontalLayout->addWidget(lbPrime);

        btOk = new QPushButton(page);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btOk->sizePolicy().hasHeightForWidth());
        btOk->setSizePolicy(sizePolicy1);
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout->addWidget(btOk);

        btCancel = new QPushButton(page);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("CANCEL"));

        horizontalLayout->addWidget(btCancel);


        verticalLayout->addLayout(horizontalLayout);

        pageControl->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        page_2->setCursor(QCursor(Qt::BlankCursor));
        page_2->setStyleSheet(QString::fromUtf8("background: white;"));
        verticalLayout_2 = new QVBoxLayout(page_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lbTitle_2 = new QLabel(page_2);
        lbTitle_2->setObjectName(QString::fromUtf8("lbTitle_2"));
        lbTitle_2->setMinimumSize(QSize(0, 40));
        lbTitle_2->setMaximumSize(QSize(16777215, 40));
        lbTitle_2->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(lbTitle_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        btSave = new QPushButton(page_2);
        btSave->setObjectName(QString::fromUtf8("btSave"));
        btSave->setMinimumSize(QSize(90, 70));
        btSave->setMaximumSize(QSize(90, 70));
        btSave->setText(QString::fromUtf8("SAVE"));

        horizontalLayout_2->addWidget(btSave);

        btAbort = new QPushButton(page_2);
        btAbort->setObjectName(QString::fromUtf8("btAbort"));
        btAbort->setMinimumSize(QSize(90, 70));
        btAbort->setMaximumSize(QSize(90, 70));
        btAbort->setText(QString::fromUtf8("ABORT"));

        horizontalLayout_2->addWidget(btAbort);

        btContinue = new QPushButton(page_2);
        btContinue->setObjectName(QString::fromUtf8("btContinue"));
        btContinue->setMinimumSize(QSize(90, 70));
        btContinue->setMaximumSize(QSize(90, 70));
        btContinue->setText(QString::fromUtf8("CONTINUE"));

        horizontalLayout_2->addWidget(btContinue);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);


        verticalLayout_2->addLayout(horizontalLayout_2);

        pageControl->addWidget(page_2);

        verticalLayout_3->addWidget(pageControl);


        retranslateUi(CalibrationPopup);

        pageControl->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(CalibrationPopup);
    } // setupUi

    void retranslateUi(QWidget *CalibrationPopup)
    {
        lbTitle->setText(QApplication::translate("CalibrationPopup", "Learn", 0, QApplication::UnicodeUTF8));
        lbPrime->setText(QApplication::translate("CalibrationPopup", "Prime unit?", 0, QApplication::UnicodeUTF8));
        lbTitle_2->setText(QApplication::translate("CalibrationPopup", "Learn interrupted", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(CalibrationPopup);
    } // retranslateUi

};

namespace Ui {
    class CalibrationPopup: public Ui_CalibrationPopup {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CALIBRATIONPOPUP_H

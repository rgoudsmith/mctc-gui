/*---------------------------------------------------------------------------------------------------*/
/* File : FileSystemControl.cpp                                                                      */
/*                                                                                                   */
/* Description :                                                                                     */
/*                                                                                                   */
/* Een object wat lezen en schrijven van de systeem instellingen en configuratie regelt.             */
/* Het lezen en schrijven van recept files loopt via dit object.                                     */
/* Berekenen en controleren van checksums behoort ook tot de functionaliteit.                        */
/* MasterSettings en SystemSettings zitten in een soort gestapelde file, zodat er meerdere units     */
/* in één file opgeslagen kunnen worden.                                                             */
/* MasterSettings komen maar één keer voor,SystemSettings bevat het aantal units dat beschikbaar is.  */
/* Indien er meer opgeslagen moet worden dan tot dan toe geconfigureerd was, dan wordt de filegrootte */
/* aangepast.                                                                                         */
/* Deze data is opgeslagen in de fileheader, deze wordt altijd eerst gelezen en gecontroleerd middels */
/* een checksum.                                                                                      */
/* Met de info uit de header wordt dan verder bepaald hoe de file gelezen moet worden.                */
/*----------------------------------------------------------------------------------------------------*/

#include "FileSystemControl.h"
#include "Rout.h"

/*----- File and path settings -----*/
#define FILE_SETTINGS_FOLDER       "/settings"
#define FILE_SETTINGS_MASTER_FILE  "master.cfg"
#define FILE_SETTINGS_SYSTEM_FILE  "system.cfg"
#define FILE_DATA_FOLDER           "/data"

/*----- File Constants -----*/
#define FILE_HEADER_VERSION 1
#define FILE_SYSTEM_VERSION 1
#define FILE_MASTER_VERSION 1

enum fileTypeID
{
  ftNone,
  ftMasterSettings,
  ftSystemSettings,
  ftMixed,
  ftCount
};

enum itemTypeID
{
  itdNone,
  itdMasterSetting,
  itdSystemSetting,
};

struct FileHeader
{
  int headerVersion;
  fileTypeID typeID;
  int itemVersion;
  int itemCount;
  int headerChecksum;
};

struct ItemHeader
{
  itemTypeID itemID;
  int ItemIndex;
};

struct MasterRecord
{
  ItemHeader head;
  MasterConfigFile masterData;
  int Checksum;
};

struct SystemRecord
{
  ItemHeader head;
  SysConfigFile systemData;
  int Checksum;
};

FileSystemControl::FileSystemControl(QObject *parent) :
QObject(parent)
{
  memset(&masterBuf,0x00,sizeof(MasterConfigFile));
  baseDir = "/";
}


void FileSystemControl::PrintMasterStruct(MasterConfig * const)
{
}


void FileSystemControl::PrintSystemStruct(SysConfig *const)
{
}


int FileSystemControl::SetBaseDir(const QString &dir)
{
  if (directory.exists(dir)) {
    baseDir = dir;
    return 0;
  }
  else return -1;
}


int FileSystemControl::CalculateChecksum(char *data, int size)
{
  /*=============================================*/
  /* Calculate the checksum over a block of data */
  /*=============================================*/
  int res = 0;
  for (int i=0; i<size; i++) {
    res += *data;
    data++;
  }
  return res;
}


bool FileSystemControl::CheckChecksum(char *data,int size,int origChksum)
{
  int chk = CalculateChecksum(data,size);
  return (chk == origChksum);
}


void FileSystemControl::LoadRecipe(RecipeStruct * const rcp, const QString &name)
{
  QFile file(name);
  if (file.exists()) {
    if (file.open(QIODevice::ReadOnly)) {
      file.reset();
      file.read((char*)rcp,sizeof(RecipeStruct));
      file.close();
    }
  }
}


void FileSystemControl::SaveRecipe(RecipeStruct * const rcp, const QString &name)
{
  QFile file(name);
  if (file.exists()) {
    QFile::remove(name);
  }

  if (file.open(QIODevice::ReadWrite)) {
    file.reset();
    file.write((char*)rcp,sizeof(RecipeStruct));
    file.close();
    Rout::SyncFile();
  }
}


/*============================*/
/* Master structure functions */
/*============================*/

void FileSystemControl::CopyMasterData(MasterConfig * const cfg, int direction)
{
  /*-------------------------------------------------------*/
  /* Copy masterbuffer from or to the master configuration */
  /*-------------------------------------------------------*/
  QString pass;

  switch (direction) {

    /*----- Copy master configuration to master buffer -----*/
    case 0:
      memset(&masterBuf,0x00,sizeof(MasterConfigFile));
      masterBuf.autoStart = cfg->autoStart;
      masterBuf.calState = (int)(cfg->calState);
      masterBuf.defLogin = (int)(cfg->defLogin);
      masterBuf.inpType = (int)(cfg->inpType);
      for (int i=0; i<4; i++) {
        masterBuf.IPAddress[i] = cfg->IPAddress[i];
        masterBuf.Netmask[i] = cfg->Netmask[i];
        masterBuf.Gateway[i] = cfg->Gateway[i];
      }
      masterBuf.keyBeep = cfg->keyBeep;
      masterBuf.language = (int)(cfg->language);
      masterBuf.modbusAddr = cfg->modbusAddr;
      masterBuf.mode = (int)(cfg->mode);
      masterBuf.MvcSettings.agent.screenTime.actVal = cfg->MvcSettings.agent.screenTime.actVal;
      masterBuf.rcpEnable = cfg->rcpEnable;
      masterBuf.twinMode = cfg->twinMode;
      masterBuf.slaveCount = cfg->slaveCount;
      masterBuf.wghtUnits = cfg->wghtUnits;
      masterBuf.motorAlarmEnb = cfg->motorAlarmEnb;
      masterBuf.profibusEnb = cfg->profibusEnb;
      masterBuf.imperialMode = cfg->imperialMode;
      masterBuf.units_kgh = cfg->units_kgh;
      masterBuf.shortMeasTime = cfg->shortMeasTime;
      masterBuf.McWeightType = cfg->mcWeightType;
      masterBuf.hysterese = cfg->hysterese;
      masterBuf.tachoRatio = cfg->tachoRatio;
      masterBuf.minExtCap = cfg->minExtCap;
      masterBuf.canWdDis = cfg->canWdDis;

      if (cfg->SuperPasswd.length() <= MAX_LENGTH_PASSWD) {
        memcpy(&(masterBuf.SuperPasswd),cfg->SuperPasswd.toAscii().data(),cfg->SuperPasswd.length());
      }
      /*----- Supervisor password is too long and will be clipped -----*/
      else {
        memcpy(&(masterBuf.SuperPasswd),cfg->SuperPasswd.toAscii().data(),MAX_LENGTH_PASSWD);
      }

      if (cfg->ToolPasswd.length() <= MAX_LENGTH_PASSWD) {
        memcpy(&(masterBuf.ToolPasswd),cfg->ToolPasswd.toAscii().data(),cfg->ToolPasswd.length());
      }
      /*----- Tooling password is to long and will be clipped -----*/
      else {
        memcpy(&(masterBuf.ToolPasswd),cfg->ToolPasswd.toAscii().data(),MAX_LENGTH_PASSWD);
      }

      if (cfg->recipe.length() <= MAX_LENGTH_FILENAME) {
        memcpy(&(masterBuf.RecipeName),cfg->recipe.toAscii().data(),cfg->recipe.length());
      }
      /*----- Recipe filename is too long and will be clipped -----*/
      else {
        memcpy(&(masterBuf.RecipeName),cfg->recipe.toAscii().data(),MAX_LENGTH_FILENAME);
      }
      break;

      /*----- Copy master buffer to master configuration -----*/
    case 1:
      cfg->autoStart = masterBuf.autoStart;
      cfg->calState = (eCalState)(masterBuf.calState);
      cfg->inpType = (eType)(masterBuf.inpType);
      cfg->defLogin = (eUserType)(masterBuf.defLogin);
      for (int i=0; i<4; i++) {
        cfg->IPAddress[i] = masterBuf.IPAddress[i];
        cfg->Netmask[i] = masterBuf.Netmask[i];
        cfg->Gateway[i] = masterBuf.Gateway[i];
      }
      cfg->keyBeep = masterBuf.keyBeep;
      cfg->language = (eLanguages)(masterBuf.language);
      cfg->modbusAddr = masterBuf.modbusAddr;
      cfg->mode = (eMode)(masterBuf.mode);
      cfg->MvcSettings.agent.screenTime.actVal = masterBuf.MvcSettings.agent.screenTime.actVal;
      cfg->rcpEnable = masterBuf.rcpEnable;
      cfg->twinMode = masterBuf.twinMode;
      cfg->slaveCount = masterBuf.slaveCount;
      cfg->wghtUnits = masterBuf.wghtUnits;
      cfg->motorAlarmEnb = masterBuf.motorAlarmEnb;
      cfg->profibusEnb = masterBuf.profibusEnb;
      cfg->imperialMode = masterBuf.imperialMode;
      cfg->mcWeightType = masterBuf.McWeightType;
      cfg->units_kgh = masterBuf.units_kgh;
      cfg->shortMeasTime = masterBuf.shortMeasTime;
      cfg->hysterese = masterBuf.hysterese;
      cfg->tachoRatio = masterBuf.tachoRatio;
      cfg->minExtCap = masterBuf.minExtCap;
      cfg->canWdDis = masterBuf.canWdDis;

      pass = QString("");
      for (int i=0; i<=MAX_LENGTH_PASSWD; i++) {
        if(masterBuf.SuperPasswd[i] != 0x00) {
          pass = pass + QString::fromAscii(&(masterBuf.SuperPasswd[i]),1);
        }
      }
      cfg->SuperPasswd = pass;

      pass = QString("");
      for (int i=0; i<=MAX_LENGTH_PASSWD; i++) {
        if (masterBuf.ToolPasswd[i] != 0x00) {
          pass = pass + QString::fromAscii(&(masterBuf.ToolPasswd[i]),1);
        }
      }
      cfg->ToolPasswd = pass;

      pass = QString("");
      for (int i=0; i<=MAX_LENGTH_FILENAME; i++) {
        if(masterBuf.RecipeName[i] != 0x00) {
          pass = pass + QString::fromAscii(&(masterBuf.RecipeName[i]),1);
        }
      }
      cfg->recipe = pass;
      break;
  }
}


void FileSystemControl::StoreMasterFactDefaults()
{
  /*---------------------------------------*/
  /* Setup master setting factory defaults */
  /*---------------------------------------*/
  int res = 0;
  MasterConfig factDef;

  /*----- Fill the struct with fact defaults: -----*/
  factDef.autoStart = false;
  factDef.defLogin = utOperator;
  factDef.language = lanUK;
  factDef.mode = modInject;
  factDef.inpType = ttTimer;
  factDef.rcpEnable = false;
  factDef.SuperPasswd = "2222";
  factDef.ToolPasswd = "1111";
  factDef.recipe = "Recipe";
  factDef.sysTime = QDateTime::currentDateTime();
  factDef.calState = csNone;
  factDef.modbusAddr = 1;
  factDef.slaveCount = 0;
  factDef.wghtUnits = 0;
  factDef.motorAlarmEnb = true;
  factDef.profibusEnb = false;
  factDef.imperialMode = false;
  factDef.mcWeightType = mcw100;
  factDef.units_kgh = true;
  factDef.shortMeasTime = 30;
  factDef.hysterese = 4.0;
  factDef.tachoRatio = 1.0;
  factDef.minExtCap = 1.8;
  factDef.canWdDis = false;

  /*----- MVC and Agent settings -----*/
  factDef.MvcSettings.agent.screenTime.actVal = 180;

  factDef.keyBeep = true;

  /*----- Master IP -----*/
  for (int i=0; i<4; i++) {
    factDef.IPAddress[i] = 0;
    factDef.Netmask[i]=0;
    factDef.Gateway[i]=0;
  }
  /*----- Force the struct to be saved into the filesystem -----*/
  StoreMasterSettings(&factDef,1,res);
}


void FileSystemControl::StoreMasterSettings(MasterConfig * const mstCfg, int forceCreate, int &res)
{
  /*----------------------------------*/
  /* Save the master settings to file */
  /*----------------------------------*/
  res = -1;

  /*----- Generate operating directory/file -----*/
  QString dir = baseDir + FILE_SETTINGS_FOLDER;
  directory.setPath(dir);

  if ((!(directory.exists())) && (forceCreate == 1)) {
    /*----- Directory did not exist. Creation was forced -----*/
    directory.mkpath(dir);
  }

  if (directory.exists()) {
    dir = dir + QDir::separator() + FILE_SETTINGS_MASTER_FILE;

    /*----- Fill file header with data -----*/
    FileHeader head;
    head.typeID = ftMasterSettings;
    head.headerVersion = FILE_HEADER_VERSION;
    head.itemVersion = FILE_MASTER_VERSION;
    head.itemCount = 1;
    head.headerChecksum = CalculateChecksum((char*)&head,(sizeof(FileHeader)-sizeof(int)));

    /*----- Initialize structure -----*/
    MasterRecord mstData;
    memset(&mstData,0x00,sizeof(MasterRecord));

    /*----- Fill item header with data -----*/
    mstData.head.itemID = itdMasterSetting;
    mstData.head.ItemIndex = 0;
    CopyMasterData(mstCfg,0);
    memcpy(&(mstData.masterData),&masterBuf,sizeof(MasterConfigFile));

    /*----- Calculate checksum -----*/
    mstData.Checksum = CalculateChecksum((char*)&mstData,(sizeof(MasterRecord)-sizeof(int)));

    /*----- Write settings to file -----*/
    file.setFileName(dir);
    if (file.open(QIODevice::ReadWrite)) {
      file.reset();

      file.write((char*)&head,sizeof(FileHeader));
      file.write((char*)&mstData,sizeof(MasterRecord));

      file.close();
      Rout::SyncFile();
      res = 0;
      emit MasterSettingsSaved();
    }
    else {
      /*----- Failed to open file for writing -----*/
      res = 2;
    }
  }
  /*----- Directory does not exists -----*/
  else {
    res = 1;
  }
}


void FileSystemControl::LoadMasterSettings(MasterConfig * const cfg, int &res)
{
  /*------------------------------------*/
  /* Load the master settings from file */
  /*------------------------------------*/
  QString dir;
  FileHeader head;
  MasterRecord mstData;

  res = -1;

  /*----- Generate operating directory/file -----*/
  dir = baseDir + FILE_SETTINGS_FOLDER;
  directory.setPath(dir);

  if (directory.exists()) {
    dir = dir + QDir::separator() + FILE_SETTINGS_MASTER_FILE;
    file.setFileName(dir);

    /*----- Open file -----*/
    if (file.open(QIODevice::ReadOnly)) {
      file.reset();

      /*----- Read header -----*/
      file.read((char*)&head,sizeof(FileHeader));

      /*----- Check header version, item version, item count -----*/
      if (CheckChecksum((char*)&head,(sizeof(FileHeader)-sizeof(int)),head.headerChecksum)) {

        /*----- Loacd item(s) -----*/
        if (head.itemCount == 1) {

          /*----- Read actual data from file -----*/
          file.read((char*)&mstData,sizeof(MasterRecord));
          file.close();

          /*----- Checking the checksum -----*/
          if (CheckChecksum((char*)&mstData,(sizeof(MasterRecord)-sizeof(int)),mstData.Checksum)) {
            memset(&masterBuf,0x00,sizeof(MasterConfigFile));
            memcpy(&masterBuf,&(mstData.masterData),sizeof(MasterConfigFile));

            /*----- Copy values from buffer to destination struct -----*/
            CopyMasterData(cfg,1);

            res = 0;
            emit MasterSettingsLoaded();
          }
          /*----- Checksum error -----*/
          else {
            file.close();
            res = 5;
          }
        }
        /*----- Ivalid itemCount detected -----*/
        else {
          file.close();
          res = 4;
        }
      }
      /*----- FileHeader checksum error -----*/
      else {
        file.close();
        res = 3;
      }
    }
    /*----
    - Error opening file -----*/
    else {
      res = 2;
    }
  }
  /*----- Directory does not exist -----*/
  else {
    res = 1;
  }
}


/*============================*/
/* System structure functions */
/*============================*/

void FileSystemControl::CopySystemData(SysConfig *const sys, int direction)
{
  /*--------------------------------------------------------*/
  /* Copy system buffer from or to the system configuration */
  /*--------------------------------------------------------*/
  QString txt;

  switch (direction) {
    /*----- Copy system configuration to system buffer -----*/
    case 0:
      /*----- Clear buffer -----*/
      memset(&systemBuf,0x00,sizeof(SysConfigFile));

      if (sys->MC_ID.length() <= MAX_LENGTH_MC_ID) {
        memcpy(&(systemBuf.MC_ID),sys->MC_ID.toAscii().data(),sys->MC_ID.length());
      }
      /*----- MC_ID is too long and will be clipped -----*/
      else {
        memcpy(&(systemBuf.MC_ID),sys->MC_ID.toAscii().data(),MAX_LENGTH_MC_ID);
      }

      systemBuf.deviceType = sys->deviceType;
      systemBuf.dosTool = sys->dosTool;
      systemBuf.motor = sys->motor;
      systemBuf.granulate = sys->granulate;
      systemBuf.sysEnabled = sys->sysEnabled;
      systemBuf.colPct = sys->colPct;
      systemBuf.setRpm = sys->setRpm;
      systemBuf.GraviRpm = sys->GraviRpm;
      systemBuf.calibDev = sys->calibDev;

      /*----- Fill System Variables -----*/
      systemBuf.fillSystem = sys->fillSystem;

      /*----- Hopper levels -----*/
      systemBuf.fillLevels = sys->fillLevels;

      /*----- Injection settings -----*/
      systemBuf.injection = sys->injection;

      /*----- Extrusion settings -----*/
      systemBuf.extrusion = sys->extrusion;

      /*----- Movacolor settings -----*/
      systemBuf.MvcSettings = sys->MvcSettings;

      /*----- Regrind parameters -----*/
      systemBuf.regrindParm = sys->regrindParm;

      /* systemBuf.curve does not need to be saved, use material name to restore the curve */

      if (sys->material.name.length() <= MAX_LENGTH_FILENAME) {
        memcpy(&(systemBuf.material.name),sys->material.name.toAscii().data(),sys->material.name.length());
      }
      /*----- Material is too long and will be clipped -----*/
      else {
        memcpy(&(systemBuf.material.name),sys->material.name.toAscii().data(),MAX_LENGTH_FILENAME);
      }

      systemBuf.material.isDefault = sys->material.isDefault;
      systemBuf.curve = sys->curve;
      break;

      /*----- Copy system buffer to system configuration -----*/
    case 1:
      /*----- Convert unit name to QString type -----*/
      txt = QString("");
      for (int i=0; i<=MAX_LENGTH_MC_ID; i++) {
        /*----- 0x00 = End of string code -----*/
        if(systemBuf.MC_ID[i] != 0x00) {
          txt = txt + QString::fromAscii(&(systemBuf.MC_ID[i]),1);
        }
        /*----- 0x00 = End of string -----*/
        else {
          break;
        }
      }
      sys->MC_ID = txt;
      sys->deviceType = systemBuf.deviceType;
      sys->dosTool = systemBuf.dosTool;
      sys->motor = systemBuf.motor;
      sys->granulate = systemBuf.granulate;

      /*-----  Fill System -----*/
      sys->fillSystem = systemBuf.fillSystem;

      /*----- Hopper levels -----*/
      sys->fillLevels = systemBuf.fillLevels;

      sys->sysEnabled = systemBuf.sysEnabled;
      sys->colPct = systemBuf.colPct;
      sys->setRpm = systemBuf.setRpm;
      sys->GraviRpm = systemBuf.GraviRpm;
      sys->calibDev = systemBuf.calibDev;

      /*----- Injection settings -----*/
      sys->injection = systemBuf.injection;

      /*----- Extrusion settings -----*/
      sys->extrusion = systemBuf.extrusion;

      /*----- Curve is not backed-up -----*/
      txt = QString("");
      for (int i=0; i<=MAX_LENGTH_FILENAME; i++) {
        if(systemBuf.material.name[i] != 0x00) {
          txt = txt + QString::fromAscii(&(systemBuf.material.name[i]),1);
        }
        /*----- 0x00 = End of string -----*/
        else {
          break;
        }
      }
      sys->material.name = txt;
      sys->material.isDefault = systemBuf.material.isDefault;
      sys->curve = systemBuf.curve;

      /*----- Movacolor settings -----*/
      sys->MvcSettings = systemBuf.MvcSettings;

      /*----- Regrind parameters -----*/
      sys->regrindParm = systemBuf.regrindParm;

      break;
  }
}


void FileSystemControl::StoreSystemFactDefaults(global * const glob, int idx)
{
  /*---------------------------------------*/
  /* Setup system setting factory defaults */
  /*---------------------------------------*/

  /*----- Store systemsettings of unit with sysCfg[index] == idx) -----*/

  if (idx < CFG_MAX_UNIT_COUNT ) {
    glob->sysCfg[idx].MC_ID = QString("Unit ")+QString::number(idx+1);
    glob->sysCfg[idx].deviceType = mcdtBalance;
    glob->sysCfg[idx].dosTool = dtGLX;
    glob->sysCfg[idx].motor = mtLT;
    glob->sysCfg[idx].granulate = gtNormal;

    /*-----  Fill System Variables -----*/
    glob->sysCfg[idx].fillSystem.fillOnOff = false;
    glob->sysCfg[idx].fillSystem.fillSysType = fsNone;
    glob->sysCfg[idx].fillSystem.alarmTime = 180;
    glob->sysCfg[idx].fillSystem.fillTimeME = 30;
    glob->sysCfg[idx].fillSystem.fillTimeMV = 20;
    glob->sysCfg[idx].fillSystem.emptyTime = 5;
    glob->sysCfg[idx].fillSystem.alarmMode = false;
    glob->sysCfg[idx].fillSystem.fillCycles = 3;
    glob->sysCfg[idx].fillSystem.fillAlarmCycles = 10;
    glob->sysCfg[idx].fillSystem.MVDelay = 15;
    glob->sysCfg[idx].fillSystem.manualFill = false;

    /*----- Hopper levels -----*/
    glob->sysCfg[idx].fillLevels.LoLo = 700;
    glob->sysCfg[idx].fillLevels.Lo = 800;
    glob->sysCfg[idx].fillLevels.Hi = 2500;
    glob->sysCfg[idx].fillLevels.HiHi = 3500;

    glob->sysCfg[idx].sysEnabled = true;
    glob->sysCfg[idx].colPct = 0;
    glob->sysCfg[idx].setRpm = 0;
    glob->sysCfg[idx].GraviRpm = grGravi;
    glob->sysCfg[idx].calibDev = 5;

    /*----- Prime -----*/
    glob->sysCfg[idx].prime.time = 30;
    glob->sysCfg[idx].prime.rpm = 50;

    /*----- Test -----*/
    glob->sysCfg[idx].test.active = false;
    glob->sysCfg[idx].test.cmd = calNone;

    /*-----  Injection variables -----*/
    glob->sysCfg[idx].injection.shotWeight = 0;
    glob->sysCfg[idx].injection.shotTime = 0;
    glob->sysCfg[idx].injection.meteringStart = 0;

    /*-----  Extrusion variables -----*/
    glob->sysCfg[idx].extrusion.extCapacity = 100;
    glob->sysCfg[idx].extrusion.tachoMax = 24;
    glob->sysCfg[idx].material.name = QString("");
    glob->sysCfg[idx].material.isDefault = false;

    /*----- Regrind parameters -----*/
    glob->sysCfg[idx].regrindParm.levelPct[0] = 0;                                              /* Regrind levels, default B0=0, B1=25, B2=50, B3=75 and B4=100 % */
    glob->sysCfg[idx].regrindParm.levelPct[1] = 25;                                             /* Regrind levels, default B0=0, B1=25, B2=50, B3=75 and B4=100 % */
    glob->sysCfg[idx].regrindParm.levelPct[2] = 50;                                             /* Regrind levels, default B0=0, B1=25, B2=50, B3=75 and B4=100 % */
    glob->sysCfg[idx].regrindParm.levelPct[3] = 75;                                             /* Regrind levels, default B0=0, B1=25, B2=50, B3=75 and B4=100 % */
    glob->sysCfg[idx].regrindParm.levelPct[4] = 100;                                            /* Regrind levels, default B0=0, B1=25, B2=50, B3=75 and B4=100 % */
    glob->sysCfg[idx].regrindParm.levelHigh = 4000;                                             /* High level, default 4000 g */
    glob->sysCfg[idx].regrindParm.levelRegrind = 3500;                                          /* Regrind high level, default 3500 g */
    glob->sysCfg[idx].regrindParm.regrindPctExtra = 5;                                          /* Extra regrind percentage (level 4), default 5 % */
    glob->sysCfg[idx].regrindParm.regPctB1 = 80;                                                /* Regrind Band 1 percentage, default 80 % */
    glob->sysCfg[idx].regrindParm.fillTimeMax = 20;                                             /* Max. fill time, default 20 s */
    glob->sysCfg[idx].regrindParm.fillInterval = 300;                                           /* Fill interval time, default 300 s */

    /*----- Force to load default material for this configuration. -----*/
    glob->SysCfg_SetMaterial("dGlxNg.mat",true,idx,false);

    glob->SetMinMaxDefaults(idx);
    glob->SysCfg_MvcSettingsFactoryDefaults(idx);

    int res;
    StoreSystemSettings(idx,glob,1,res);
  }
}


void FileSystemControl::StoreSystemSettings(int idx, global * const glob, int forceCreate, int &res)
{
  /*------------------------------*/
  /* Save system settings to file */
  /*------------------------------*/
  QString dir = baseDir + FILE_SETTINGS_FOLDER;
  directory.setPath(dir);

  if ((!(directory.exists())) && (forceCreate == 1)) {
    directory.mkpath(dir);
  }

  if (directory.exists()) {
    dir = dir + QDir::separator() + FILE_SETTINGS_SYSTEM_FILE;

    FileHeader head;
    head.typeID = ftSystemSettings;
    head.headerVersion = FILE_HEADER_VERSION;
    head.itemVersion = FILE_SYSTEM_VERSION;
    head.itemCount = (glob->mstCfg.slaveCount + 1);
    head.headerChecksum = CalculateChecksum((char*)&head,(sizeof(FileHeader)-sizeof(int)));

    SystemRecord sysData;
    memset(&sysData,0x00,sizeof(SystemRecord));

    /*----- Fill item header with data -----*/
    sysData.head.itemID = itdSystemSetting;
    sysData.head.ItemIndex = idx;

    /*----- Prepare sysCfg data -----*/
    CopySystemData(&(glob->sysCfg[idx]),0);
    memcpy(&(sysData.systemData),&systemBuf,sizeof(SysConfigFile));

    /*----- Calculate checksum -----*/
    sysData.Checksum = CalculateChecksum((char*)&sysData,(sizeof(SystemRecord)-sizeof(int)));

    /*----- Open filce -----*/
    file.setFileName(dir);
    if (file.open(QIODevice::ReadWrite)) {
      /*----- Check filesize and increase size if needed. -----*/
      if (file.size() < ((sizeof(FileHeader))+(sizeof(SystemRecord)*head.itemCount))) {
        file.resize(((sizeof(FileHeader))+(sizeof(SystemRecord)*head.itemCount)));
      }
      file.reset();

      /*----- write header -----*/
      file.write((char*)&head,sizeof(FileHeader));

      /*----- Set filepointer at correct position -----*/
      file.seek((sizeof(FileHeader))+(sizeof(SystemRecord)*idx));

      /*----- write system record into file -----*/
      file.write((char*)&sysData,sizeof(SystemRecord));

      file.close();
      Rout::SyncFile();
      res = 0;
      emit SystemSettingsSaved(idx);
    }
    /*----- Failed to open file for writing -----*/
    else {
      res = 2;
    }
  }
  /*----- Directory does not exists -----*/
  else {
    res = 1;
  }
}


void FileSystemControl::LoadSystemSettings(int idx, global * const glob, int &res)
{
  /*--------------------------------------------------*/
  /* Load system settings from file for the given idx */
  /*--------------------------------------------------*/
  QString dir;
  FileHeader head;
  SystemRecord sysData;

  res = -1;
  dir = baseDir + FILE_SETTINGS_FOLDER;

  directory.setPath(dir);

  if (directory.exists()) {
    dir = dir + QDir::separator() + FILE_SETTINGS_SYSTEM_FILE;
    file.setFileName(dir);
    /*----- Open file -----*/
    if (file.open(QIODevice::ReadOnly)) {
      file.reset();
      /*----- Read header -----*/
      file.read((char*)&head,sizeof(FileHeader));
      /*----- Check header version, item version, item count -----*/
      if (CheckChecksum((char*)&head,(sizeof(FileHeader)-sizeof(int)),head.headerChecksum)) {
        /*----- Load item(s) -----*/
        if (head.typeID == ftSystemSettings) {
          /*----- Desired ID -----*/
          if (head.itemCount > idx) {
            /*----- Setup correct position of the data for the given idx -----*/
            file.reset();
            file.seek(((sizeof(FileHeader))+(sizeof(SystemRecord)*idx)));

            /*----- Read actual data from file -----*/
            file.read((char*)&sysData,sizeof(SystemRecord));
            file.close();

            /*----- Check the checksum -----*/
            if (CheckChecksum((char*)&sysData,(sizeof(SystemRecord)-sizeof(int)),sysData.Checksum)) {
              memset(&systemBuf,0x00,sizeof(SysConfigFile));
              memcpy(&systemBuf,&(sysData.systemData),sizeof(SysConfigFile));

              /*----- Copy values from buffer to destination struct -----*/
              CopySystemData(&(glob->sysCfg[idx]),1);

              /* Restore min/max/default values to prevent any garbage or old values from file */
              glob->SetMinMaxDefaults(idx);

              /*----- Check if recipe function is enabled. -----*/
              /*----- Else this forced loading of materials is already taken care of. -----*/

              if (glob->mstCfg.rcpEnable == false) {

                /*----- Buffer current material settings -----*/
                QString matName = glob->sysCfg[idx].material.name;
                bool matDef = glob->sysCfg[idx].material.isDefault;

                /*----- Force load from file to prevent loading a non-existing material. -----*/
                glob->SysCfg_SetMaterial("",false,idx,false);
                glob->SysCfg_SetMaterial(matName,matDef,idx);
              }

              res = 0;
              emit SystemSettingsLoaded(idx);
            }
            /*----- Checksum error -----*/
            else {
              file.close();
              res = 6;
            }
          }
          /*----- ItemCount is too low -----*/
          else {
            file.close();
            res = 5;
          }
        }
        /*----- Invalid file type -----*/
        else {
          file.close();
          res = 4;
        }
      }
      /*----- FileHeader checksum error -----*/
      else {

        file.close();
        res = 3;
      }
    }
    /*----- Error opening file -----*/
    else {
      res = 2;
    }
  }
  /*----- Directory does not exist -----*/
  else {
    res = 1;
  }
}

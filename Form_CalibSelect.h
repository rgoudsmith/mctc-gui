#ifndef CALIBSELECTFRM_H
#define CALIBSELECTFRM_H

#include <QWidget>
#include "Form_QbaseWidget.h"

namespace Ui
{
  class CalibSelectFrm;
}


class CalibSelectFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    CalibSelectFrm(QWidget *parent = 0);
    ~CalibSelectFrm();
    void FormInit();

  protected:
    void changeEvent(QEvent *e);
    void showEvent(QShowEvent *e);

  private:
    Ui::CalibSelectFrm *ui;
    void LanguageUpdate();

  private slots:
    void OkClicked();

  public slots:
    void LoadcellTypeChanged(int, int);
    void CalibrateClicked();
    void ZeroClicked();
    void WeightCheckClicked();
};
#endif                                                                          // CALIBSELECTFRM_H

#ifndef PROCESSDIAGNOSTICFRM_H
#define PROCESSDIAGNOSTICFRM_H

#include "Form_QbaseWidget.h"

namespace Ui
{
  class ProcessDiagnosticFrm;
}


class ProcessDiagnosticFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit ProcessDiagnosticFrm(QWidget *parent = 0);
    ~ProcessDiagnosticFrm();
    void FormInit();

  private:
    Ui::ProcessDiagnosticFrm *ui;
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

  public slots:
    void DiagDataReceived(tMsgControlUnit,int);
    void SlaveCountChanged(int);
    void OkClicked();

    //    void MCIDChanged(const QString &,int);
    //    void McStatusChanged(eMCStatus,int);
    //    void HopperWeightChanged(float,int);
    //    void ColorPctSetChanged(float,int);
    //    void ColorPctActChanged(float,int);
    //    void CalibratedChanged(bool,int);
    //    void SysCfg_RpmChanged(float,int);
};
#endif                                                                          // PROCESSDIAGNOSTICFRM_H

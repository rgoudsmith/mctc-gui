/****************************************************************************
** Meta object code from reading C++ file 'Form_RecipeCheckPopup.h'
**
** Created: Wed Feb 6 11:15:13 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_RecipeCheckPopup.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_RecipeCheckPopup.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RecipeCheckPopupFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x05,

 // slots: signature, parameters, type, tag, flags
      32,   20,   20,   20, 0x08,
      44,   20,   20,   20, 0x08,
      60,   20,   20,   20, 0x0a,
      87,   20,   20,   20, 0x0a,
     112,   20,   20,   20, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RecipeCheckPopupFrm[] = {
    "RecipeCheckPopupFrm\0\0RecipeOK()\0"
    "OkClicked()\0CancelClicked()\0"
    "CheckRecipe(RecipeStruct*)\0"
    "SetRecipe(RecipeStruct*)\0ApplyRecipe()\0"
};

const QMetaObject RecipeCheckPopupFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_RecipeCheckPopupFrm,
      qt_meta_data_RecipeCheckPopupFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RecipeCheckPopupFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RecipeCheckPopupFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RecipeCheckPopupFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RecipeCheckPopupFrm))
        return static_cast<void*>(const_cast< RecipeCheckPopupFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int RecipeCheckPopupFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: RecipeOK(); break;
        case 1: OkClicked(); break;
        case 2: CancelClicked(); break;
        case 3: CheckRecipe((*reinterpret_cast< RecipeStruct*(*)>(_a[1]))); break;
        case 4: SetRecipe((*reinterpret_cast< RecipeStruct*(*)>(_a[1]))); break;
        case 5: ApplyRecipe(); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void RecipeCheckPopupFrm::RecipeOK()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE

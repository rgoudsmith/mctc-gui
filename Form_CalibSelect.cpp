#include "Form_CalibSelect.h"
#include "ui_Form_CalibSelect.h"
#include "Form_MainMenu.h"
#include "Rout.h"

CalibSelectFrm::CalibSelectFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::CalibSelectFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);

  ////@@ Zero functionality enabled
  ui->btZero->setVisible(false);
  connect(ui->btCalib,SIGNAL(clicked()),this,SLOT(CalibrateClicked()));
  connect(ui->btZero,SIGNAL(clicked()),this,SLOT(ZeroClicked()));
  connect(ui->btCheck,SIGNAL(clicked()),this,SLOT(WeightCheckClicked()));
  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
}


CalibSelectFrm::~CalibSelectFrm()
{
  delete ui;
}


void CalibSelectFrm::changeEvent(QEvent *e)
{
  QBaseWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      LanguageUpdate();
      break;
    default:
      break;
  }
}


void CalibSelectFrm::showEvent(QShowEvent *e)
{
  /*------------*/
  /* Show event */
  /*------------*/
  QBaseWidget::showEvent(e);

  /*----- Refresh the fullscale label -----*/
  LoadcellTypeChanged(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.fullScale.actVal,glob->ActUnitIndex());
}


void CalibSelectFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  LanguageUpdate();

  connect(glob,SIGNAL(SysCfg_LoadCellChanged(int,int)),this,SLOT(LoadcellTypeChanged(int,int)));
  //  LoadcellTypeChanged(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.fullScale.actVal,glob->ActUnitIndex());
}


void CalibSelectFrm::LanguageUpdate()
{
  glob->SetButtonImage(ui->btOk,itSelection,selOk);
  glob->SetButtonImage(ui->btCalib,itConfig,cfgCal_Calib);
  glob->SetButtonImage(ui->btZero,itConfig,cfgCal_Zero);
  glob->SetButtonImage(ui->btCheck,itConfig,cfgCal_Check);

  ui->lbLc->setFont(*(glob->baseFont));
  ui->lbLoadcell->setFont(*(glob->baseFont));
}


// Public slots:
void CalibSelectFrm::LoadcellTypeChanged(int lc, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    if (lc == 0) {
      ui->lbLoadcell->setText(tr("invalid configuration","text for label \"Loadcell configuration\""));
    }
    else {
      int lcKg = lc / 1000;
      ui->lbLoadcell->setText(QString::number(lcKg)+"kg");
    }
  }
}


void CalibSelectFrm::CalibrateClicked()
{
  /*---------------------------------------------------*/
  /* PROCEDURE    : CalibrateClicked.                  */
  /*                                                   */
  /* DESCRIPTION  : Loadcell calibrate button pressed. */
  /*---------------------------------------------------*/
  /*----- Check if unit not in production -----*/
  if (!glob->sysSts[glob->ActUnitIndex()].sysActive.active) {
    glob->MstSts_SetCalState(csCalibration);
    ((MainMenu*)(glob->menu))->DisplayWindow(wiSysCalib);
  }
}


void CalibSelectFrm::ZeroClicked()
{
  glob->MstSts_SetCalState(csZero);
  ((MainMenu*)(glob->menu))->DisplayWindow(wiSysCalib);
}


void CalibSelectFrm::OkClicked()
{
  ((MainMenu*)(glob->menu))->DisplayWindow(wiBack);
}


void CalibSelectFrm::WeightCheckClicked()
{
  ((MainMenu*)(glob->menu))->DisplayWindow(wiWeightCheck);
}

#ifndef SETTINGSFRM_H
#define SETTINGSFRM_H

#include <QVBoxLayout>
#include "Form_QbaseWidget.h"
#include "Form_NumericInput.h"
#include "QmVcScrollbar.h"

namespace Ui
{
  class SettingsFrm;
}


class SettingsFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit SettingsFrm(QWidget *parent = 0);
    ~SettingsFrm();
    void FormInit();

  protected:
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

  private:
    Ui::SettingsFrm *ui;
    QVBoxLayout *layout;
    NumericInputFrm *numInput;
    QLabel * scrollBox;
    QWidget * contents;
    QObjectList objList;

    int itemHeight;

    bool sysChanges;
    bool mstChanges;

    void FillItems();
    void ClearItems();
    int FillAgentSettings();
    int FillMvcSettings();
    void resizeEvent(QResizeEvent *);

  private slots:
    void OkClicked();

  public slots:
    // Agent settings values
    void UserChanged(eUserType);
    void FullScaleReceived(float);
    void KeyBeepReceived(float);
    void TestTime1Received(float);
    void TestTime2Received(float);
    void CalWeightCylinderReceived(float);
    void CalWeightAugerReceived(float);
    void CalStepsReceived(float);
    void CalTimeoutReceived(float);
    void CalAlarmCyclesReceived(float);
    void MotorAlarmModeReceived(float);
    void FillCylinderTimeReceived(float);
    void FillCylinderSpeedReceived(float);
    void ScreenTimeReceived(float);
    void TachoCorrReceived(float);
    void ExtCapTimeReceived(float);
    void ExtCapDevReceived(float);
    void AfterFillDelayReceived(float);
    void InputFilterReceived(float);
    void MetTimeDevReceived(float);
    void CurveGainReceived(float);
    void MinGraviCapReceived(float);
    void InpDelayReceived(float);
    void RunContactOnDelayReceived(float);
    void RunContactOffDelayReceived(float);
    void LogIntervalReceived(float);
    void ColPctSetDevReceived(float);
    void DevAlarmQrReceived(float);
    void MotorSixWiresReceived(float);
    // MovaColor settings values
    void MotionBandReceived(float);
    void MotionDelayReceived(float);
    void WeightFilterReceived(float);
    void WeightFilterFastReceived(float);
    void RefWeightReceived(float);
    // Balance settings (Band values)
    void MeasMinTimeReceived(float);
    void MeasMinBand4Received(float);
    void MaxMotorSpeedReceived(float val);
    void MeasMaxTimeReceived(float);
    void MeasTimeCorrReceived(float);
    void DevThreshInjReceived(float);
    void DevThreshExtReceived(float);
    void RpmCorrFac1Received(float);
    void RpmCorrFac2Received(float);
    void DevB4HystReceived(float);
    void MinExtCapReceived(float);
    void CanWdDisReceived(float);
    void BAL_FillStartReceived(float);
    void BAL_FillStartHlReceived(float);
    void BAL_FillReadyReceived(float);
    void BAL_FillTimeReceived(float);
    void BAL_LidOffWeightReceived(float);
    void BAL_LidOffTimeReceived(float);
    void ConMeasTimeReceived(float);
    void DevAlmPctReceived(float);
    void DevAlmRetryReceived(float);
    void DevAlmPctB4Received(float);
    void DevAlmRetryB4Received(float);
    void ActUpdateReceived(float);
    void ShortMeasTimeReceived(float val);
    void HysteresReceived(float val);
    void DevAlarmRetryQrReceived(float);
    //    void DevAlarmPctQrReceived(float);
    void DevAlarmBandNrQrReceived(float);
    void B0_CorDevReceived(float);
    void B0_CorFac1Received(float);
    void B0_CorFac2Received(float);
    void B0_N1Received(float);
    void B0_N2Received(float);
    void B0_CorMaxReceived(float);
    void B1_CorDevReceived(float);
    void B1_CorFac1Received(float);
    void B1_CorFac2Received(float);
    void B1_N1Received(float);
    void B1_N2Received(float);
    void B1_CorMaxReceived(float);
    void B2_CorDevReceived(float);
    void B2_CorFac1Received(float);
    void B2_CorFac2Received(float);
    void B2_N1Received(float);
    void B2_N2Received(float);
    void B2_CorMaxReceived(float);
    void B3_CorDevReceived(float);
    void B3_CorFac1Received(float);
    void B3_CorFac2Received(float);
    void B3_N1Received(float);
    void B3_N2Received(float);
    void B3_CorMaxReceived(float);
    void B4_CorDevReceived(float);
    void B4_CorFac1Received(float);
    void B4_CorFac2Received(float);
    void B4_N1Received(float);
    void B4_N2Received(float);
    void B4_CorMaxReceived(float);

};
#endif                                                                          // SETTINGSFRM_H

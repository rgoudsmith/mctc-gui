#ifndef FORM_REGRINDCONFIG_H
#define FORM_REGRINDCONFIG_H

#include <QtGui/QWidget>
#include "Form_QbaseWidget.h"
#include "Form_NumericInput.h"

namespace Ui
{
  class RegrindConfigFrm;
}


class RegrindConfigFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit RegrindConfigFrm(QWidget *parent = 0);
    ~RegrindConfigFrm();
    void FormInit();

  protected:
    void resizeEvent(QResizeEvent *e);
    bool eventFilter(QObject *o, QEvent *e);
    void showEvent(QShowEvent *);

  private:
    Ui::RegrindConfigFrm *ui;
    NumericInputFrm * numInput;
    void DisplayData(void);

  private slots:
    void ValueReturned(float);
    void OkClicked();

};
#endif                                                                          // FORM_REGRINDCONFIG_H

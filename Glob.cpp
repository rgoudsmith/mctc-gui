/*========================================================*/
/* File        : Glob.cpp                                 */
/* Description : Global routines and data sturcture class */
/*               Accessible through the pointer glob.     */
/*                                                        */
/*               Example : glob->ActUnitIndex()           */
/*========================================================*/
#include <QtCore>
#include <QPainter>
#include <QFontMetrics>
#include <QDateTime>
#include <QDir>
#include "Glob.h"
#include "Form_MainMenu.h"
#include "Udp.h"
#include "McServer.h"
#include "LogServer.h"
#include  "Rout.h"

#define DATE_UPDATE_VAL 10
#define DEFAULT_SUBTEXT_OFFSET 32
#define DEFAULT_SUBTEXT_FONTSIZE 20

/*----- Global accessible structures -----*/
SysStatus global::sysSts[CFG_MAX_UNIT_COUNT];                                   // System status structure (array)
SysConfig global::sysCfg[CFG_MAX_UNIT_COUNT];                                   // System configuration structures (array)
MasterStatus global::mstSts;                                                    // Master status structure
MasterConfig global::mstCfg;                                                    // Master configuration structure

byte prevProfiResetAlarm;

global::global(QObject * parent)
{
  QObject::setParent(parent);

  sendInterval = 0;
  usbStick = new UsbAccess(this);
  usbStick->FormInit();

  /*----- Setup LCD backlight -----*/
  Rout::LcdBrightNess(100);

  /*----- Timer used for start / stop commands -----*/
  stsTim = new QTimer();
  stsTim->setInterval(500);
  stsTim->stop();
  connect(stsTim,SIGNAL(timeout()),this,SLOT(StsTimElapsed()));

  /*----- Global pallette -----*/
  _palette = new QPalette(QColor::fromRgb(255,255,255,255));
  _appPath = 0;
  translator = 0;

  fileSeparator = QDir::separator();

  SetMinMaxDefaults();

  /*----- Reset the full system to factory defaults. -----*/
  for (int i=0; i<CFG_MAX_UNIT_COUNT; i++) {
    SysCfg_MvcSettingsFactoryDefaults(i);
  }

  /*----- Zero calibration struct -----*/
  calStruct.cmd = calNone;
  calStruct.sts = calsIdle;
  calStruct.command = 0x0000;
  calStruct.status = 0x0000;

  /*----- Level formatting -----*/
  formats.LevelFmt.fieldWidth = 0;
  formats.LevelFmt.format = 'f';
  formats.LevelFmt.precision = 0;

  /*----- Percentage formatting -----*/
  formats.PercentageFmt.fieldWidth = 0;
  formats.PercentageFmt.format = 'f';
  formats.PercentageFmt.precision = 3;

  /*----- Weight formatting -----*/
  formats.WeightFmt.fieldWidth = 0;
  formats.WeightFmt.format = 'f';
  formats.WeightFmt.precision = 0;

  /*----- Shot weight formatting -----*/
  formats.ShotWeightFmt.fieldWidth = 0;
  formats.ShotWeightFmt.format = 'f';
  formats.ShotWeightFmt.precision = 1;

  /*----- Time formatting -----*/
  formats.TimeFmt.fieldWidth = 0;
  formats.TimeFmt.format = 'f';
  formats.TimeFmt.precision = 1;

  /*----- Rpm formatting -----*/
  formats.RpmFmt.fieldWidth = 0;
  formats.RpmFmt.format = 'f';
  formats.RpmFmt.precision = 1;

  /*----- Dosage formatting (dos act/dos set) -----*/
  formats.DosageFmt.fieldWidth = 0;
  formats.DosageFmt.format = 'f';
  formats.DosageFmt.precision = 3;

  /*----- Dosage formatting kg/h (dos act/dos set) -----*/
  formats.DosageKgHFmt.fieldWidth = 0;
  formats.DosageKgHFmt.format = 'f';
  formats.DosageKgHFmt.precision = 1;


  /*----- Extruder formatting -----*/
  formats.ExtruderFmt.fieldWidth = 0;
  formats.ExtruderFmt.format = 'f';
  formats.ExtruderFmt.precision = 1;

  /*----- Tacho formatting -----*/
  formats.TachoFmt.fieldWidth = 0;
  formats.TachoFmt.format = 'f';
  formats.TachoFmt.precision = 2;

  /*----- CalDev formatting -----*/
  formats.CalDevFmt.fieldWidth = 0;
  formats.CalDevFmt.format = 'f';
  formats.CalDevFmt.precision = 0;

  /*----- AlarmPct formatting -----*/
  formats.AlarmPctFmt.fieldWidth = 0;
  formats.AlarmPctFmt.format = 'd';
  formats.AlarmPctFmt.precision = 0;

  mstSts.isTwin = false;
  mstSts.actUnitIndex = -1;
  mstSts.sysStart = false;
  mstSts.popupActive = false;
  mstSts.inputSts = 0;

#ifdef UIT
  /*----- Setup metric or imperial unit text -----*/
  if (!Rout::ImperialMode()) {
    shotUnits = UNITS_MET_G;
    extrUnits = UNITS_MET_KG_H;
    weightUnits = UNITS_MET_G;
  }
  else {
    shotUnits = UNITS_IMP_G;
    extrUnits = UNITS_IMP_KG_H;
    weightUnits = UNITS_IMP_G;
  }

  timerUnits = UNITS_SEC;
  tachoUnits = UNITS_VOLT;
  rpmUnits = "rpm";                                                             // Translated via LanguageUpdate()
#endif

  /*----- CAPTION FONT -----*/
  captionFont = new QFont();
  captionFont->setFamily("Waree");
  captionFont->setPointSize(35);
  captionFont->setBold(true);

  /*----- BASE FONT -----*/
  baseFont = new QFont();
  //  baseFont->setFamily("DejaVuSans.ttf");
  //  baseFont->setFamily("DejaVu Sans");
  baseFont->setFamily("Waree");
  baseFont->setPointSize(20);
  baseFont->setBold(false);

  /*----- CONFIG FONT -----*/
  configFont = new QFont();
  //configFont->setFamily("DejaVuSans-Bold.ttf");
  //  configFont->setFamily("DejaVu Sans");
  configFont->setFamily("Waree");
  configFont->setPointSize(17);
  configFont->setBold(true);
  currentUser = utOperator;

  /*----- Initialize sysSts,sysCfg structures -----*/
  for(int i=0; i<CFG_MAX_UNIT_COUNT; i++) {
    sysSts[i].MCStatus = mcsNone;
    sysSts[i].fillSts = 0;
    sysSts[i].motorSts = 0;
    sysSts[i].mass = 0;
    sysSts[i].fastMass = 0;
    sysSts[i].rpm = 0;
    sysSts[i].actProd = 0;
    sysSts[i].setProd = 0;
    sysSts[i].curveChanged = false;

    sysSts[i].prime.activate = false;
    sysSts[i].prime.activeSts = false;
    sysSts[i].prime.preCal = false;
    sysSts[i].prime.timVal = 0;
    sysSts[i].prime.maxVal = 0;

    sysSts[i].injection.meteringAct = 0;
    sysSts[i].injection.meteringValid = false;
    sysSts[i].injection.shotTimeAct = 0;
    sysSts[i].extrusion.extCapacityAct = 0;
    sysSts[i].extrusion.tachoAct = 0;

    sysSts[i].preCalibration.calib.sts = calsIdle;
    sysSts[i].preCalibration.calib.cmd = calNone;
    sysSts[i].preCalibration.calib.command = 0;
    sysSts[i].preCalibration.calib.status = 0;

    sysSts[i].preCalibration.preCalDosAct = 0;
    sysSts[i].preCalibration.rpmCorrFacValid = false;

    sysSts[i].alarmSts.active = false;
    sysSts[i].alarmSts.mode = 0;
    sysSts[i].alarmSts.nr = 0;
    sysSts[i].alarmSts.reset = false;

    sysSts[i].sysActive.active = false;
    sysSts[i].sysActive.execCmd = false;
    sysSts[i].sysActive.start = false;
    sysSts[i].sysActive.cmd = 0;
    sysSts[i].sysActive.ack = 0;
    sysSts[i].sysActive.sysActSts = sysIdle;

    sysSts[i].uBoardVersion.HardwareVersion = 0x00;
    sysSts[i].uBoardVersion.SoftwareVersion = 0x00;

    sysSts[i].manIO.active = false;
    for(int j=0; j<MAX_DIG_OUT; j++) {
      sysSts[i].manIO.outputs[j] = false;
    }
    sysSts[i].manIO.motorSpeed = 0;
  }

  for(int i=0; i<CFG_MAX_UNIT_COUNT; i++) {
    sysCfg[i].deviceType = mcdtNone,
        sysCfg[i].sysEnabled = true;
    sysCfg[i].dosTool = dtGX;
    sysCfg[i].fillSystem.fillSysType = fsNone;
    sysCfg[i].granulate = gtNormal;
    sysCfg[i].motor = mtLT;
    sysCfg[i].MvcSettings.agent.fullScale.actVal = 0;
    sysCfg[i].GraviRpm = grGravi;

    sysCfg[i].calibDev = 5;
    sysCfg[i].MvcSettings.balance.devAlmPctB4.actVal = 25;

    /*----- Prime values -----*/
    sysCfg[i].prime.rpm = 50;
    sysCfg[i].prime.time = 30;

    sysCfg[i].test.active = false;
    sysCfg[i].test.cmd = calNone;

    sysCfg[i].colPct = 0;

    sysCfg[i].setRpm = 0;

    sysCfg[i].injection.shotWeight = 0;
    sysCfg[i].injection.shotTime = 0;
    sysCfg[i].injection.meteringStart = 0;

    sysCfg[i].extrusion.extCapacity = 0;
    sysCfg[i].extrusion.tachoMax = 0;

    sysCfg[i].MC_ID = "";
    sysCfg[i].material.name = "";
    sysCfg[i].material.isDefault = false;

    /*----- init curve struct -----*/
    sysCfg[i].curve.swVersion = 0;
    sysCfg[i].curve.cylSel = 0;
    sysCfg[i].curve.matType = 0;
    sysCfg[i].curve.calMode = 0;
    sysCfg[i].curve.corFac = 0;

    for(int j=1; j<MAX_CURVE_LINES; j++) {
      sysCfg[i].curve.curveLines[j].capacity = 4.979;
      sysCfg[i].curve.curveLines[j].motorSpeed = 200;
    }
  }

  /*----- Initialize mstCfg structure -----*/
  mstCfg.autoStart = false;
  mstCfg.defLogin = utOperator;
  mstCfg.language = lanUK;
  mstCfg.mode = modInject;
  mstCfg.inpType = ttTimer;
  mstCfg.rcpEnable = false;
  mstCfg.SuperPasswd = "2222";
  mstCfg.ToolPasswd = "1111";
  mstCfg.recipe = "Recipe";
  mstCfg.sysTime = QDateTime::currentDateTime();
  mstCfg.calState = csNone;
  mstCfg.modbusAddr = 1;
  mstCfg.slaveCount = 0;

  mstCfg.keyBeep = true;

  mstCfg.dateSet = false;

  /*----- Master Ethernet settings -----*/
  for (int i=0; i<4; i++) {
    mstCfg.IPAddress[i] = 0;
    mstCfg.Netmask[i] = 0;
    mstCfg.Gateway[i] = 0;
  }

  mstSts.actUnitIndex = 0;
  mstSts.popupActive = false;
  mstSts.primeAvailable = true;
  mstSts.sysActive = false;
  mstSts.sysStart = false;
  mstSts.isTwin = Rout::TwinMode();
  mstSts.inputSts = 0;
  mstSts.ledStatus.alarm = lstsOff;
  mstSts.ledStatus.input = lstsOff;
  mstSts.ledStatus.power = lstsOff;

  mstSts.buzzerCtrl = buzNone;

  /*----- Setup timer for time refresh in main menu -----*/
  dateTim = new QTimer(this);
  dateTim->setInterval(DATE_UPDATE_VAL*1000);                                   // Update interval 10S for date/time
  dateTim->setSingleShot(false);
  connect(dateTim,SIGNAL(timeout()),this,SLOT(GBI_UpdateDateTime()));
  dateTim->start();
  /*---------- Setup communication between GUI and Control task -----*/
  com = new Com(this);
  connect(com,SIGNAL(ComReceived(tMsgControl)),this,SLOT(ComMsgReceived(tMsgControl)));

  /*----- Com timer -----*/
  comTim = new QTimer(this);
  comTim->setInterval(COM_TIMER_INTERVAL);
  comTim->setSingleShot(false);
  connect(comTim,SIGNAL(timeout()),this,SLOT(ComSendData()));
  comTim->start();                                                              /* Start Com */

  connect(this,SIGNAL(SysSts_SysActiveChanged(bool,int)),this,SLOT(MstSts_CheckSysActive(bool,int)));

  SetCorrectUnits();
}


global::~global()
{
  dateTim->stop();
  ClearImages();
  delete baseFont;
  delete configFont;
  delete dateTim;
  comTim->stop();
  //  com->Terminate();
  delete comTim;
}


QPalette* global::Palette()
{
  return _palette;
}


void global::SetMinMaxDefaults()
{
  /*----- Global (system wide) config settings -----*/

  /*----- Scr time -----*/
  mstCfg.MvcSettings.agent.screenTime.defVal = 180;
  mstCfg.MvcSettings.agent.screenTime.minVal = 0;
  mstCfg.MvcSettings.agent.screenTime.maxVal = 600;

  /*----- Unit config settings -----*/
  for (int i=0; i<CFG_MAX_UNIT_COUNT; i++) {
    SetMinMaxDefaults(i);
  }
}


void global::SetMinMaxDefaults(int idx)
{
  int i = idx;
  /*----- Agent Settings -----*/

  /*----- Fullscale -----*/
  sysCfg[i].MvcSettings.agent.fullScale.defVal = 20000;
  sysCfg[i].MvcSettings.agent.fullScale.minVal = 0;
  sysCfg[i].MvcSettings.agent.fullScale.maxVal = 100000;
  /*----- TEST <0.5g/s -----*/
  sysCfg[i].MvcSettings.agent.calib.testGt05g.defVal = 30;
  sysCfg[i].MvcSettings.agent.calib.testGt05g.minVal = 0;
  sysCfg[i].MvcSettings.agent.calib.testGt05g.maxVal = 99;
  /*----- TEST >0.5g/s -----*/
  sysCfg[i].MvcSettings.agent.calib.testLt05g.defVal = 60;
  sysCfg[i].MvcSettings.agent.calib.testLt05g.minVal = 0;
  sysCfg[i].MvcSettings.agent.calib.testLt05g.maxVal = 255;
  /*----- CAL weight cyl -----*/
  sysCfg[i].MvcSettings.agent.calib.weightCylinder.defVal = 5;
  sysCfg[i].MvcSettings.agent.calib.weightCylinder.minVal = 0;
  sysCfg[i].MvcSettings.agent.calib.weightCylinder.maxVal = 99;
  /*----- CAL weight aug -----*/
  sysCfg[i].MvcSettings.agent.calib.weightAuger.defVal = 20;
  sysCfg[i].MvcSettings.agent.calib.weightAuger.minVal = 0;
  sysCfg[i].MvcSettings.agent.calib.weightAuger.maxVal = 99;
  /*----- CAL steps -----*/
  sysCfg[i].MvcSettings.agent.calib.steps.defVal = 1;
  sysCfg[i].MvcSettings.agent.calib.steps.minVal = 0;
  sysCfg[i].MvcSettings.agent.calib.steps.maxVal = 2;
  /*----- CAL time out -----*/
  sysCfg[i].MvcSettings.agent.calib.timeout.defVal = 300;
  sysCfg[i].MvcSettings.agent.calib.timeout.minVal = 0;
  sysCfg[i].MvcSettings.agent.calib.timeout.maxVal = 999;
  /*----- CAL alarm cyc -----*/
  sysCfg[i].MvcSettings.agent.calib.alarmCycles.defVal = 20;
  sysCfg[i].MvcSettings.agent.calib.alarmCycles.minVal = 1;
  sysCfg[i].MvcSettings.agent.calib.alarmCycles.maxVal = 99;
  /*----- Fill time -----*/
  sysCfg[i].MvcSettings.agent.fillTime.defVal = 15;
  sysCfg[i].MvcSettings.agent.fillTime.minVal = 0;
  sysCfg[i].MvcSettings.agent.fillTime.maxVal = 999;
  /*----- Fill speed -----*/
  sysCfg[i].MvcSettings.agent.fillSpeed.defVal = 50;
  sysCfg[i].MvcSettings.agent.fillSpeed.minVal = 0;
  sysCfg[i].MvcSettings.agent.fillSpeed.maxVal = 200;
  /*----- Tacho mode (VERVALT) -----*/
  /*----- Tacho corr. -----*/
  sysCfg[i].MvcSettings.agent.tachoCorrection.defVal = 1;
  sysCfg[i].MvcSettings.agent.tachoCorrection.minVal = 0;
  sysCfg[i].MvcSettings.agent.tachoCorrection.maxVal = 10;
  /*----- Max motor speed corr. -----*/
  sysCfg[i].MvcSettings.agent.maxMotorSpeed.defVal = 200;
  sysCfg[i].MvcSettings.agent.maxMotorSpeed.minVal = 10;
  sysCfg[i].MvcSettings.agent.maxMotorSpeed.maxVal = 400;
  /*----- Ext Cap Tim -----*/
  sysCfg[i].MvcSettings.agent.extCapTime.defVal = 1;
  sysCfg[i].MvcSettings.agent.extCapTime.minVal = 0;
  sysCfg[i].MvcSettings.agent.extCapTime.maxVal = 99;
  /*----- Ext Cap Dev -----*/
  sysCfg[i].MvcSettings.agent.extCapDev.defVal = 1;
  sysCfg[i].MvcSettings.agent.extCapDev.minVal = 0;
  sysCfg[i].MvcSettings.agent.extCapDev.maxVal = 99;
  /*----- MV* Delay (*renamed to afterFill Delay) -----*/
  sysCfg[i].MvcSettings.agent.afterFillDelay.defVal = 15;
  sysCfg[i].MvcSettings.agent.afterFillDelay.minVal = 0;
  sysCfg[i].MvcSettings.agent.afterFillDelay.maxVal = 999;
  /*----- Input Filter -----*/
  sysCfg[i].MvcSettings.agent.inputFilter.defVal = 3;
  sysCfg[i].MvcSettings.agent.inputFilter.minVal = 1;
  sysCfg[i].MvcSettings.agent.inputFilter.maxVal = 32;
  /*----- Met Time Dev -----*/
  sysCfg[i].MvcSettings.agent.MetTimeDev.defVal = 10;
  sysCfg[i].MvcSettings.agent.MetTimeDev.minVal = 0;
  sysCfg[i].MvcSettings.agent.MetTimeDev.maxVal = 99;
  /*----- Curve gain -----*/
  sysCfg[i].MvcSettings.agent.curveGain.defVal = 1;
  sysCfg[i].MvcSettings.agent.curveGain.minVal = 0.01;
  sysCfg[i].MvcSettings.agent.curveGain.maxVal = 99;
  /*----- min grav cap -----*/
  sysCfg[i].MvcSettings.agent.minGraviCap.defVal = 0;
  sysCfg[i].MvcSettings.agent.minGraviCap.minVal = 0;
  sysCfg[i].MvcSettings.agent.minGraviCap.maxVal = 99;
  /*----- inpdelay x10ms -----*/
  sysCfg[i].MvcSettings.agent.inputDelay.defVal = 25;
  sysCfg[i].MvcSettings.agent.inputDelay.minVal = 0;
  sysCfg[i].MvcSettings.agent.inputDelay.maxVal = 255;
  /*----- runC on delay -----*/
  sysCfg[i].MvcSettings.agent.runContactOnDelay.defVal = 0;
  sysCfg[i].MvcSettings.agent.runContactOnDelay.minVal = 0;
  sysCfg[i].MvcSettings.agent.runContactOnDelay.maxVal = 99;
  /*----- runC off delay -----*/
  sysCfg[i].MvcSettings.agent.runContactOffDelay.defVal = 0;
  sysCfg[i].MvcSettings.agent.runContactOffDelay.minVal = 0;
  sysCfg[i].MvcSettings.agent.runContactOffDelay.maxVal = 99;
  /*----- log interval -----*/
  sysCfg[i].MvcSettings.agent.logInterval.defVal = 30;
  sysCfg[i].MvcSettings.agent.logInterval.minVal = 1;
  sysCfg[i].MvcSettings.agent.logInterval.maxVal = 99;
  /*----- col % set dev -----*/
  sysCfg[i].MvcSettings.agent.colPctSetDev.defVal = 10;
  sysCfg[i].MvcSettings.agent.colPctSetDev.minVal = 0;
  sysCfg[i].MvcSettings.agent.colPctSetDev.maxVal = 99;
  /*----- dev alarm pct QR -----*/
  sysCfg[i].MvcSettings.agent.devAlarmQr.defVal = 90;
  sysCfg[i].MvcSettings.agent.devAlarmQr.minVal = 1;
  sysCfg[i].MvcSettings.agent.devAlarmQr.maxVal = 99;
  /*----- motor 6 wires -----*/
  sysCfg[i].MvcSettings.agent.motorSixWires.defVal = 0;
  sysCfg[i].MvcSettings.agent.motorSixWires.minVal = 0;
  sysCfg[i].MvcSettings.agent.motorSixWires.maxVal = 1;

  /*----- MovaColor Settings -----*/

  /*----- Motion Band -----*/
  sysCfg[i].MvcSettings.weight.motionBand.defVal = 2.5;
  sysCfg[i].MvcSettings.weight.motionBand.minVal = 0;
  sysCfg[i].MvcSettings.weight.motionBand.maxVal = 5;
  /*----- Motion Delay -----*/
  sysCfg[i].MvcSettings.weight.motionDelay.defVal = 1;
  sysCfg[i].MvcSettings.weight.motionDelay.minVal = 0;
  sysCfg[i].MvcSettings.weight.motionDelay.maxVal = 9;
  /*----- Weight Filter -----*/
  sysCfg[i].MvcSettings.weight.weightFilter.defVal = 255;
  sysCfg[i].MvcSettings.weight.weightFilter.minVal = 0;
  sysCfg[i].MvcSettings.weight.weightFilter.maxVal = 255;
  /*----- Weight Filter Fast -----*/
  sysCfg[i].MvcSettings.weight.weightFilterFast.defVal = 32;
  sysCfg[i].MvcSettings.weight.weightFilterFast.minVal = 0;
  sysCfg[i].MvcSettings.weight.weightFilterFast.maxVal = 255;
  /*----- Reference Weight -----*/
  sysCfg[i].MvcSettings.weight.refWeight.defVal = 500;
  sysCfg[i].MvcSettings.weight.refWeight.minVal = 0;
  sysCfg[i].MvcSettings.weight.refWeight.maxVal = 9999;
  /*----- Min Measure Time -----*/
  sysCfg[i].MvcSettings.balance.measureMinTime.defVal = 15;
  sysCfg[i].MvcSettings.balance.measureMinTime.minVal = 1;
  sysCfg[i].MvcSettings.balance.measureMinTime.maxVal = 99;
  /*----- Min Measure Time Band 4 -----*/
  sysCfg[i].MvcSettings.balance.measureBand4MinTime.defVal = 90;
  sysCfg[i].MvcSettings.balance.measureBand4MinTime.minVal = 1;
  sysCfg[i].MvcSettings.balance.measureBand4MinTime.maxVal = 999;
  /*----- Max Measure Time -----*/
  sysCfg[i].MvcSettings.balance.measureMaxTime.defVal = 1800;
  sysCfg[i].MvcSettings.balance.measureMaxTime.minVal = 1;
  sysCfg[i].MvcSettings.balance.measureMaxTime.maxVal = 9999;
  /*----- Measure Time Correction Factor -----*/
  sysCfg[i].MvcSettings.balance.measureTimeCorr.defVal = 10;
  sysCfg[i].MvcSettings.balance.measureTimeCorr.minVal = 1;
  sysCfg[i].MvcSettings.balance.measureTimeCorr.maxVal = 99;
  /*----- Dev Threshold Injection -----*/
  sysCfg[i].MvcSettings.balance.devThreshInj.defVal = 15;
  sysCfg[i].MvcSettings.balance.devThreshInj.minVal = 1;
  sysCfg[i].MvcSettings.balance.devThreshInj.maxVal = 99;
  /*----- Dev Threshold Extrusion -----*/
  sysCfg[i].MvcSettings.balance.devThreshExt.defVal = 5;
  sysCfg[i].MvcSettings.balance.devThreshExt.minVal = 1;
  sysCfg[i].MvcSettings.balance.devThreshExt.maxVal = 99;
  /*----- Rpm Correction Factor 1 -----*/
  sysCfg[i].MvcSettings.balance.rpmCorrFac1.defVal = 85;
  sysCfg[i].MvcSettings.balance.rpmCorrFac1.minVal = 1;
  sysCfg[i].MvcSettings.balance.rpmCorrFac1.maxVal = 999;
  /*----- Rpm Correction Factor 2 -----*/
  sysCfg[i].MvcSettings.balance.rpmCorrFac2.defVal = 60;
  sysCfg[i].MvcSettings.balance.rpmCorrFac2.minVal = 1;
  sysCfg[i].MvcSettings.balance.rpmCorrFac2.maxVal = 999;
  /*----- Dev Band 4 Hysteresis -----*/
  sysCfg[i].MvcSettings.balance.devB4Hyst.defVal = 0.5;
  sysCfg[i].MvcSettings.balance.devB4Hyst.minVal = 0;
  sysCfg[i].MvcSettings.balance.devB4Hyst.maxVal = 10;
  /*----- Balance - Fill Start -----*/
  sysCfg[i].MvcSettings.balance.fillStart.defVal = 50;
  sysCfg[i].MvcSettings.balance.fillStart.minVal = 1;
  sysCfg[i].MvcSettings.balance.fillStart.maxVal = 99;
  /*----- Balance - Fill Start HL -----*/
  sysCfg[i].MvcSettings.balance.fillStartHL.defVal = 100;
  sysCfg[i].MvcSettings.balance.fillStartHL.minVal = 1;
  sysCfg[i].MvcSettings.balance.fillStartHL.maxVal = 100;
  /*----- Balance - Fill Ready -----*/
  sysCfg[i].MvcSettings.balance.fillReady.defVal = 50;
  sysCfg[i].MvcSettings.balance.fillReady.minVal = 1;
  sysCfg[i].MvcSettings.balance.fillReady.maxVal = 99;
  /*----- Balance - Fill Time -----*/
  sysCfg[i].MvcSettings.balance.fillTime.defVal = 6;
  sysCfg[i].MvcSettings.balance.fillTime.minVal = 1;
  sysCfg[i].MvcSettings.balance.fillTime.maxVal = 99;
  /*----- Balance - Lid Off Weight -----*/
  sysCfg[i].MvcSettings.balance.lidOffWeight.defVal = 300;
  sysCfg[i].MvcSettings.balance.lidOffWeight.minVal = 1;
  sysCfg[i].MvcSettings.balance.lidOffWeight.maxVal = 2000;
  /*----- Balance - Lid Off Time -----*/
  sysCfg[i].MvcSettings.balance.lidOffTime.defVal = 4;
  sysCfg[i].MvcSettings.balance.lidOffTime.minVal = 1;
  sysCfg[i].MvcSettings.balance.lidOffTime.maxVal = 99;
  /*----- Cons Measure Time -----*/
  sysCfg[i].MvcSettings.balance.measureConsTime.defVal = 10;
  sysCfg[i].MvcSettings.balance.measureConsTime.minVal = 1;
  sysCfg[i].MvcSettings.balance.measureConsTime.maxVal = 99;
  /*----- Dev Alarm Percentage -----*/
  sysCfg[i].MvcSettings.balance.devAlmPct.defVal = 85;
  sysCfg[i].MvcSettings.balance.devAlmPct.minVal = 1;
  sysCfg[i].MvcSettings.balance.devAlmPct.maxVal = 99;
  /*----- Dev Alarm Retry -----*/
  sysCfg[i].MvcSettings.balance.devAlmRetry.defVal = 5;
  sysCfg[i].MvcSettings.balance.devAlmRetry.minVal = 1;
  sysCfg[i].MvcSettings.balance.devAlmRetry.maxVal = 99;
  /*----- Dev Alarm Percantage Band 4 -----*/
  sysCfg[i].MvcSettings.balance.devAlmPctB4.defVal = 25;
  sysCfg[i].MvcSettings.balance.devAlmPctB4.minVal = 1;
  sysCfg[i].MvcSettings.balance.devAlmPctB4.maxVal = 99;
  /*----- Dev Alarm Retry Band 4 -----*/
  sysCfg[i].MvcSettings.balance.devAlmRetryB4.defVal = 3;
  sysCfg[i].MvcSettings.balance.devAlmRetryB4.minVal = 1;
  sysCfg[i].MvcSettings.balance.devAlmRetryB4.maxVal = 99;
  /*----- Dev Alarm Retry QR -----*/
  sysCfg[i].MvcSettings.balance.devAlmRetryQR.defVal = 10;
  sysCfg[i].MvcSettings.balance.devAlmRetryQR.minVal = 1;
  sysCfg[i].MvcSettings.balance.devAlmRetryQR.maxVal = 99;
  /*----- Dev Alarm Percentage QR -----*/
  sysCfg[i].MvcSettings.balance.devAlmPctQR.defVal = 90;
  sysCfg[i].MvcSettings.balance.devAlmPctQR.minVal = 1;
  sysCfg[i].MvcSettings.balance.devAlmPctQR.maxVal = 99;
  /*----- Dev Alarm Band Nr QR -----*/
  sysCfg[i].MvcSettings.balance.devAlmBandNrQR.defVal = 2;
  sysCfg[i].MvcSettings.balance.devAlmBandNrQR.minVal = 0;
  sysCfg[i].MvcSettings.balance.devAlmBandNrQR.maxVal = 4;
  /*----- Act Update -----*/
  sysCfg[i].MvcSettings.balance.actUpdate.defVal = 10;
  sysCfg[i].MvcSettings.balance.actUpdate.minVal = 0;
  sysCfg[i].MvcSettings.balance.actUpdate.maxVal = 99;

  /* Band 0 defaults */
  sysCfg[i].MvcSettings.balance.Bands[0].corDev.defVal = 15;
  sysCfg[i].MvcSettings.balance.Bands[0].corDev.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[0].corDev.maxVal = 99;

  sysCfg[i].MvcSettings.balance.Bands[0].corFac1.defVal = 1;
  sysCfg[i].MvcSettings.balance.Bands[0].corFac1.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[0].corFac1.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[0].corFac2.defVal = 0.7;
  sysCfg[i].MvcSettings.balance.Bands[0].corFac2.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[0].corFac2.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[0].corN1.defVal = 2;
  sysCfg[i].MvcSettings.balance.Bands[0].corN1.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[0].corN1.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[0].corN2.defVal = 5;
  sysCfg[i].MvcSettings.balance.Bands[0].corN2.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[0].corN2.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[0].corMax.defVal = 7;
  sysCfg[i].MvcSettings.balance.Bands[0].corMax.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[0].corMax.maxVal = 9;

  /* Band 1 defaults */
  sysCfg[i].MvcSettings.balance.Bands[1].corDev.defVal = 10;
  sysCfg[i].MvcSettings.balance.Bands[1].corDev.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[1].corDev.maxVal = 99;

  sysCfg[i].MvcSettings.balance.Bands[1].corFac1.defVal = 1;
  sysCfg[i].MvcSettings.balance.Bands[1].corFac1.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[1].corFac1.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[1].corFac2.defVal = 0.8;
  sysCfg[i].MvcSettings.balance.Bands[1].corFac2.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[1].corFac2.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[1].corN1.defVal = 2;
  sysCfg[i].MvcSettings.balance.Bands[1].corN1.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[1].corN1.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[1].corN2.defVal = 4;
  sysCfg[i].MvcSettings.balance.Bands[1].corN2.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[1].corN2.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[1].corMax.defVal = 6;
  sysCfg[i].MvcSettings.balance.Bands[1].corMax.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[1].corMax.maxVal = 9;

  /* Band 2 defaults */
  sysCfg[i].MvcSettings.balance.Bands[2].corDev.defVal = 8;
  sysCfg[i].MvcSettings.balance.Bands[2].corDev.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[2].corDev.maxVal = 99;

  sysCfg[i].MvcSettings.balance.Bands[2].corFac1.defVal = 1;
  sysCfg[i].MvcSettings.balance.Bands[2].corFac1.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[2].corFac1.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[2].corFac2.defVal = 0.8;
  sysCfg[i].MvcSettings.balance.Bands[2].corFac2.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[2].corFac2.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[2].corN1.defVal = 2;
  sysCfg[i].MvcSettings.balance.Bands[2].corN1.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[2].corN1.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[2].corN2.defVal = 4;
  sysCfg[i].MvcSettings.balance.Bands[2].corN2.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[2].corN2.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[2].corMax.defVal = 6;
  sysCfg[i].MvcSettings.balance.Bands[2].corMax.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[2].corMax.maxVal = 9;

  /* Band 3 defaults */
  sysCfg[i].MvcSettings.balance.Bands[3].corDev.defVal = 5;
  sysCfg[i].MvcSettings.balance.Bands[3].corDev.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[3].corDev.maxVal = 99;

  sysCfg[i].MvcSettings.balance.Bands[3].corFac1.defVal = 1;
  sysCfg[i].MvcSettings.balance.Bands[3].corFac1.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[3].corFac1.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[3].corFac2.defVal = 0.8;
  sysCfg[i].MvcSettings.balance.Bands[3].corFac2.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[3].corFac2.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[3].corN1.defVal = 2;
  sysCfg[i].MvcSettings.balance.Bands[3].corN1.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[3].corN1.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[3].corN2.defVal = 4;
  sysCfg[i].MvcSettings.balance.Bands[3].corN2.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[3].corN2.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[3].corMax.defVal = 6;
  sysCfg[i].MvcSettings.balance.Bands[3].corMax.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[3].corMax.maxVal = 9;

  /* Band 4 defaults */
  sysCfg[i].MvcSettings.balance.Bands[4].corDev.defVal = 3;
  sysCfg[i].MvcSettings.balance.Bands[4].corDev.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[4].corDev.maxVal = 99;

  sysCfg[i].MvcSettings.balance.Bands[4].corFac1.defVal = 1;
  sysCfg[i].MvcSettings.balance.Bands[4].corFac1.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[4].corFac1.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[4].corFac2.defVal = 0.9;
  sysCfg[i].MvcSettings.balance.Bands[4].corFac2.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[4].corFac2.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[4].corN1.defVal = 2;
  sysCfg[i].MvcSettings.balance.Bands[4].corN1.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[4].corN1.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[4].corN2.defVal = 5;
  sysCfg[i].MvcSettings.balance.Bands[4].corN2.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[4].corN2.maxVal = 9;

  sysCfg[i].MvcSettings.balance.Bands[4].corMax.defVal = 7;
  sysCfg[i].MvcSettings.balance.Bands[4].corMax.minVal = 0;
  sysCfg[i].MvcSettings.balance.Bands[4].corMax.maxVal = 9;

  //@@@ NEW
  for(int j=0; j<MAX_USER_ALM; j++) {
    sysCfg[i].MvcSettings.alarm.almConfig[j].defVal = 0;
    sysCfg[i].MvcSettings.alarm.almConfig[j].minVal = 0;
    sysCfg[i].MvcSettings.alarm.almConfig[j].maxVal = 0;
  }
}


void global::SysCfg_MvcSettingsFactoryDefaults(int idx)
{
  /*---------------------------------------------------*/
  /* Reset system data MvcSettings to factory defaults */
  /*---------------------------------------------------*/

  /*----- Agent Settings -----*/
  sysCfg[idx].MvcSettings.agent.fullScale.actVal              = sysCfg[idx].MvcSettings.agent.fullScale.defVal;
  sysCfg[idx].MvcSettings.agent.calib.testGt05g.actVal        = sysCfg[idx].MvcSettings.agent.calib.testGt05g.defVal;
  sysCfg[idx].MvcSettings.agent.calib.testLt05g.actVal        = sysCfg[idx].MvcSettings.agent.calib.testLt05g.defVal;
  sysCfg[idx].MvcSettings.agent.calib.weightCylinder.actVal   = sysCfg[idx].MvcSettings.agent.calib.weightCylinder.defVal;
  sysCfg[idx].MvcSettings.agent.calib.weightAuger.actVal      = sysCfg[idx].MvcSettings.agent.calib.weightAuger.defVal;
  sysCfg[idx].MvcSettings.agent.calib.steps.actVal            = sysCfg[idx].MvcSettings.agent.calib.steps.defVal;
  sysCfg[idx].MvcSettings.agent.calib.timeout.actVal          = sysCfg[idx].MvcSettings.agent.calib.timeout.defVal;
  sysCfg[idx].MvcSettings.agent.calib.alarmCycles.actVal      = sysCfg[idx].MvcSettings.agent.calib.alarmCycles.defVal;
  sysCfg[idx].MvcSettings.agent.fillTime.actVal               = sysCfg[idx].MvcSettings.agent.fillTime.defVal;
  sysCfg[idx].MvcSettings.agent.fillSpeed.actVal              = sysCfg[idx].MvcSettings.agent.fillSpeed.defVal;
  sysCfg[idx].MvcSettings.agent.tachoCorrection.actVal        = sysCfg[idx].MvcSettings.agent.tachoCorrection.defVal;
  sysCfg[idx].MvcSettings.agent.maxMotorSpeed.actVal          = sysCfg[idx].MvcSettings.agent.maxMotorSpeed.defVal;
  sysCfg[idx].MvcSettings.agent.extCapTime.actVal             = sysCfg[idx].MvcSettings.agent.extCapTime.defVal;
  sysCfg[idx].MvcSettings.agent.extCapDev.actVal              = sysCfg[idx].MvcSettings.agent.extCapDev.defVal;
  sysCfg[idx].MvcSettings.agent.afterFillDelay.actVal         = sysCfg[idx].MvcSettings.agent.afterFillDelay.defVal;
  sysCfg[idx].MvcSettings.agent.inputFilter.actVal            = sysCfg[idx].MvcSettings.agent.inputFilter.defVal;
  sysCfg[idx].MvcSettings.agent.MetTimeDev.actVal             = sysCfg[idx].MvcSettings.agent.MetTimeDev.defVal;
  sysCfg[idx].MvcSettings.agent.curveGain.actVal              = sysCfg[idx].MvcSettings.agent.curveGain.defVal;
  sysCfg[idx].MvcSettings.agent.minGraviCap.actVal            = sysCfg[idx].MvcSettings.agent.minGraviCap.defVal;
  sysCfg[idx].MvcSettings.agent.inputDelay.actVal             = sysCfg[idx].MvcSettings.agent.inputDelay.defVal;
  sysCfg[idx].MvcSettings.agent.runContactOnDelay.actVal      = sysCfg[idx].MvcSettings.agent.runContactOnDelay.defVal;
  sysCfg[idx].MvcSettings.agent.runContactOffDelay.actVal     = sysCfg[idx].MvcSettings.agent.runContactOffDelay.defVal;
  sysCfg[idx].MvcSettings.agent.logInterval.actVal            = sysCfg[idx].MvcSettings.agent.logInterval.defVal;
  sysCfg[idx].MvcSettings.agent.colPctSetDev.actVal           = sysCfg[idx].MvcSettings.agent.colPctSetDev.defVal;
  sysCfg[idx].MvcSettings.agent.devAlarmQr.actVal             = sysCfg[idx].MvcSettings.agent.devAlarmQr.defVal;
  sysCfg[idx].MvcSettings.agent.motorSixWires.actVal          = sysCfg[idx].MvcSettings.agent.motorSixWires.defVal;

  /*----- MovaColor Settings -----*/
  sysCfg[idx].MvcSettings.weight.motionBand.actVal            = sysCfg[idx].MvcSettings.weight.motionBand.defVal;
  sysCfg[idx].MvcSettings.weight.motionDelay.actVal           = sysCfg[idx].MvcSettings.weight.motionDelay.defVal;
  sysCfg[idx].MvcSettings.weight.weightFilter.actVal          = sysCfg[idx].MvcSettings.weight.weightFilter.defVal;
  sysCfg[idx].MvcSettings.weight.weightFilterFast.actVal      = sysCfg[idx].MvcSettings.weight.weightFilterFast.defVal;
  sysCfg[idx].MvcSettings.weight.refWeight.actVal             = sysCfg[idx].MvcSettings.weight.refWeight.defVal;
  sysCfg[idx].MvcSettings.balance.measureMinTime.actVal       = sysCfg[idx].MvcSettings.balance.measureMinTime.defVal;
  sysCfg[idx].MvcSettings.balance.measureBand4MinTime.actVal  = sysCfg[idx].MvcSettings.balance.measureBand4MinTime.defVal;
  sysCfg[idx].MvcSettings.balance.measureMaxTime.actVal       = sysCfg[idx].MvcSettings.balance.measureMaxTime.defVal;
  sysCfg[idx].MvcSettings.balance.measureTimeCorr.actVal      = sysCfg[idx].MvcSettings.balance.measureTimeCorr.defVal;
  sysCfg[idx].MvcSettings.balance.devThreshInj.actVal         = sysCfg[idx].MvcSettings.balance.devThreshInj.defVal;
  sysCfg[idx].MvcSettings.balance.devThreshExt.actVal         = sysCfg[idx].MvcSettings.balance.devThreshExt.defVal;
  sysCfg[idx].MvcSettings.balance.rpmCorrFac1.actVal          = sysCfg[idx].MvcSettings.balance.rpmCorrFac1.defVal;
  sysCfg[idx].MvcSettings.balance.rpmCorrFac2.actVal          = sysCfg[idx].MvcSettings.balance.rpmCorrFac2.defVal;
  sysCfg[idx].MvcSettings.balance.devB4Hyst.actVal            = sysCfg[idx].MvcSettings.balance.devB4Hyst.defVal;
  sysCfg[idx].MvcSettings.balance.fillStart.actVal            = sysCfg[idx].MvcSettings.balance.fillStart.defVal;
  sysCfg[idx].MvcSettings.balance.fillStartHL.actVal          = sysCfg[idx].MvcSettings.balance.fillStartHL.defVal;
  sysCfg[idx].MvcSettings.balance.fillReady.actVal            = sysCfg[idx].MvcSettings.balance.fillReady.defVal;
  sysCfg[idx].MvcSettings.balance.fillTime.actVal             = sysCfg[idx].MvcSettings.balance.fillTime.defVal;
  sysCfg[idx].MvcSettings.balance.lidOffWeight.actVal         = sysCfg[idx].MvcSettings.balance.lidOffWeight.defVal;
  sysCfg[idx].MvcSettings.balance.lidOffTime.actVal           = sysCfg[idx].MvcSettings.balance.lidOffTime.defVal;
  sysCfg[idx].MvcSettings.balance.measureConsTime.actVal      = sysCfg[idx].MvcSettings.balance.measureConsTime.defVal;
  sysCfg[idx].MvcSettings.balance.devAlmPct.actVal            = sysCfg[idx].MvcSettings.balance.devAlmPct.defVal;
  sysCfg[idx].MvcSettings.balance.devAlmRetry.actVal          = sysCfg[idx].MvcSettings.balance.devAlmRetry.defVal;
  sysCfg[idx].MvcSettings.balance.devAlmPctB4.actVal          = sysCfg[idx].MvcSettings.balance.devAlmPctB4.defVal;
  sysCfg[idx].MvcSettings.balance.devAlmRetryB4.actVal        = sysCfg[idx].MvcSettings.balance.devAlmRetryB4.defVal;
  sysCfg[idx].MvcSettings.balance.devAlmPctQR.actVal          = sysCfg[idx].MvcSettings.balance.devAlmPctQR.defVal;
  sysCfg[idx].MvcSettings.balance.devAlmRetryQR.actVal        = sysCfg[idx].MvcSettings.balance.devAlmRetryQR.defVal;
  sysCfg[idx].MvcSettings.balance.devAlmBandNrQR.actVal       = sysCfg[idx].MvcSettings.balance.devAlmBandNrQR.defVal;
  sysCfg[idx].MvcSettings.balance.actUpdate.actVal            = sysCfg[idx].MvcSettings.balance.actUpdate.defVal;

  /*----- Balance seting -----*/
  for (int i=0; i<5; i++) {
    sysCfg[idx].MvcSettings.balance.Bands[i].corDev.actVal  = sysCfg[idx].MvcSettings.balance.Bands[i].corDev.defVal;
    sysCfg[idx].MvcSettings.balance.Bands[i].corFac1.actVal = sysCfg[idx].MvcSettings.balance.Bands[i].corFac1.defVal;
    sysCfg[idx].MvcSettings.balance.Bands[i].corFac2.actVal = sysCfg[idx].MvcSettings.balance.Bands[i].corFac2.defVal;
    sysCfg[idx].MvcSettings.balance.Bands[i].corN1.actVal   = sysCfg[idx].MvcSettings.balance.Bands[i].corN1.defVal;
    sysCfg[idx].MvcSettings.balance.Bands[i].corN2.actVal   = sysCfg[idx].MvcSettings.balance.Bands[i].corN2.defVal;
    sysCfg[idx].MvcSettings.balance.Bands[i].corMax.actVal  = sysCfg[idx].MvcSettings.balance.Bands[i].corMax.defVal;
  }
}


void global::LoadImages()
{
  /*-------------------------*/
  /* Load the various images */
  /*-------------------------*/
  ImageStruct *img;
  QString imgPath;
  QString filePath;
  QString fullPath;
  QString imgText;
  int yOffset;
  int i,j;

  /*----- get the application path. -----*/
  filePath = _appPath->toAscii().data();

  for (j=0; j<itCount; j++) {
    switch(j) {
    case itDosTool:
      /*----- Load all Dosing Tool image paths into the list. -----*/
      for (i=0; i<dtCount; i++) {
        imgPath = "";
        GetEnumeratedText(etDosTool,i,&imgText);
        switch(i) {
        case dtGX:
          imgPath = DT_GX_IMG;
          break;
        case dtGLX:
          imgPath = DT_GLX_IMG;
          break;
        case dtHX:
          imgPath = DT_HX_IMG;
          break;
        case dtA50:
          imgPath = DT_A30_IMG;
          break;
        case dtA30:
          imgPath = DT_A30_IMG;
          break;
        case dtA20:
          imgPath = DT_A20_IMG;
          break;
        case dtA15:
          imgPath = DT_A15_IMG;
          break;

        }
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = imgText;
        img->fontSize = DEFAULT_SUBTEXT_FONTSIZE;
        img->xOffset = 0;
        img->yOffset = DEFAULT_SUBTEXT_OFFSET;
        _list.append(img);
      }
      break;

    case itMotor:
      /*----- Load all Motor types image paths into the list -----*/
      for (i=0; i<mtCount; i++) {
        imgText = "";
        imgPath = "";
        GetEnumeratedText(etMotor,i,&imgText);
        switch(i) {
        case mtLT:
          imgPath = MT_LT_IMG;
          break;
        case mtHT:
          imgPath = MT_HT_IMG;
          break;
        }
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = imgText;
        img->fontSize = DEFAULT_SUBTEXT_FONTSIZE;
        img->xOffset = 0;
        img->yOffset = DEFAULT_SUBTEXT_OFFSET;
        _list.append(img);
      }
      break;

    case itGranulate:
      for (i=0; i<gtCount; i++) {
        imgText = "";
        imgPath = "";
        GetEnumeratedText(etGranulate,i,&imgText);
        switch(i) {
        case gtNormal:
          imgPath = GT_NG_IMG;
          break;
        case gtMicro:
          imgPath = GT_MG_IMG;
          break;
        }
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = imgText;
        img->fontSize = DEFAULT_SUBTEXT_FONTSIZE;
        img->xOffset = 0;
        img->yOffset = 0;
        _list.append(img);
      }
      break;

    case itMcwType:
      for (i=0; i<mcwCount; i++) {
        imgText = "";
        imgPath = "";
        GetEnumeratedText(etMcwType,i,&imgText);
        imgPath = GENERAL_NO_IMG;
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = imgText;
        img->fontSize = DEFAULT_SUBTEXT_FONTSIZE;
        img->xOffset = 0;
        img->yOffset = 0;
        _list.append(img);
      }
      break;

    case itBalHOType:
      for (i=0; i<balHOCount; i++) {
        imgText = "";
        imgPath = "";
        GetEnumeratedText(etBalHOType,i,&imgText);
        imgPath = GENERAL_NO_IMG;
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = imgText;
        img->fontSize = DEFAULT_SUBTEXT_FONTSIZE;
        img->xOffset = 0;
        img->yOffset = 0;
        _list.append(img);
      }
      break;

    case itFillSys:
      /*----- Load all Fill System image paths into the list -----*/
      for (i=0; i<fsCount; i++) {
        imgText = "";
        imgPath = "";
        yOffset = DEFAULT_SUBTEXT_OFFSET;
        switch(i) {
        case fsNone:
          imgPath = FS_NONE_IMG;
          imgText = tr("LOADER","fillSystem type subtext (NONE) 1/2")+"\n"+tr("OFF","fillSystem type subtext (NONE) 2/2");
          yOffset = 0;
          break;
        case fsME:
          imgPath = FS_ME_IMG;
          GetEnumeratedText(etFillSystem,i,&imgText);
          break;
        case fsMV:
          imgPath = FS_MV_IMG;
          GetEnumeratedText(etFillSystem,i,&imgText);
          break;
        case fsEX:
          imgPath = FS_EX_IMG;
          GetEnumeratedText(etFillSystem,i,&imgText);
          break;
#warning Vervang dit voor een RG button image !!
        case fsRG:
          imgPath = FS_EX_IMG;
          GetEnumeratedText(etFillSystem,i,&imgText);
          break;

        }
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = imgText;
        img->fontSize = DEFAULT_SUBTEXT_FONTSIZE;
        img->xOffset = 0;
        img->yOffset = yOffset;
        _list.append(img);
      }
      break;

    case itType:
      /*----- Load all type image paths into the list -----*/
      for (i=0; i<ttCount; i++) {
        imgText = "";
        imgPath = "";
        GetEnumeratedText(etInpType,i,&imgText);
        switch(i) {
        case ttRelay:
          imgPath = TT_RELAY_IMG;
          break;
        case ttTimer:
          imgPath = TT_TIMER_IMG;
          break;
        case ttTacho:
          imgPath = TT_TACHO_IMG;
          break;
        }
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = imgText;
        img->fontSize = DEFAULT_SUBTEXT_FONTSIZE;
        img->xOffset = 0;
        img->yOffset = DEFAULT_SUBTEXT_OFFSET;
        _list.append(img);
      }
      break;

    case itMode:
      /*----- Load all mode image paths into the list -----*/
      for (i=0; i<modCount; i++) {
        imgText = "";
        imgPath = "";
        GetEnumeratedText(etProdType,i,&imgText);
        switch(i) {
        case modInject:
          imgPath = MOD_INJ_IMG;
          break;
        case modExtrude:
          imgPath = MOD_EXT_IMG;
          break;
        }
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = imgText;
        img->fontSize = DEFAULT_SUBTEXT_FONTSIZE;
        img->xOffset = 0;
        img->yOffset = DEFAULT_SUBTEXT_OFFSET;
        _list.append(img);
      }
      break;

    case itGraviRpm:
      for (i=0; i<grCount; i++) {
        imgText = "";
        imgPath = "";
        GetEnumeratedText(etGraviRpm,i,&imgText);
        switch(i) {
        case grGravi:
          imgPath = GRAVI_RPM_GRAVI_IMG;
          break;
        case grRpm:
          imgPath = GRAVI_RPM_RPM_IMG;
          break;
        }
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = imgText;
        img->fontSize = DEFAULT_SUBTEXT_FONTSIZE;
        img->xOffset = 0;
        img->yOffset = DEFAULT_SUBTEXT_OFFSET;
        _list.append(img);
      }
      break;

    case itUserSelect:
      for (i=0; i<usCount; i++) {
        imgText = "";
        imgPath = "";
        GetEnumeratedText(etUserSelect,i,&imgText);
        switch(i) {
        case usOperator:
          imgPath = US_OPERATOR_IMG;
          break;
        case usTooling:
          imgPath = US_TOOLING_IMG;
          break;
        case usSupervisor:
          imgPath = US_SUPERVISOR_IMG;
          break;
        }
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = imgText;
        img->fontSize = 11;
        img->xOffset = 0;
        img->yOffset = DEFAULT_SUBTEXT_OFFSET;
        _list.append(img);
      }
      break;

    case itGeneral:
      int fontSize;
      for (i=0; i<genCount; i++) {
        imgText = "";
        imgPath = "";
        fontSize = 0;
        switch(i) {
        case genNoImg:
          imgPath = GENERAL_NO_IMG;
          imgText = "X\n"+tr("NO_IMG");
          fontSize = DEFAULT_SUBTEXT_FONTSIZE;
          break;
        case genInfo:
          imgPath = GENERAL_INFO;
          break;
        case genHopper:
          imgPath = GENERAL_HOPPER;
          break;
        case genKeyboard:
          imgPath = GENERAL_KEYBOARD;
          break;
        case genUSB:
          imgPath = GENERAL_USB;
          break;
        case genKeyNum1:
          imgPath = GENERAL_KEY_NUM1;
          break;
        case genKeyNum2:
          imgPath = GENERAL_KEY_NUM2;
          break;
        case genArrowLeft:
          imgPath = GENERAL_ARROW_LEFT;
          break;
        case genArrowRight:
          imgPath = GENERAL_ARROW_RIGHT;
          break;
        case genArrowInc:
          imgPath = GENERAL_ARROW_INC;
          break;
        case genArrowDec:
          imgPath = GENERAL_ARROW_DEC;
          break;
        case genArrowKeyUp:
          imgPath = GENERAL_ARROW_KEY_UP;
          break;
        case genArrowKeyDown:
          imgPath = GENERAL_ARROW_KEY_DOWN;
          break;
        case genArrowKeyLeft:
          imgPath = GENERAL_ARROW_KEY_LEFT;
          break;
        case genArrowKeyRight:
          imgPath = GENERAL_ARROW_KEY_RIGHT;
          break;
        case genFillManual:
          imgPath = GENERAL_FILL_MANUAL;
          break;
        case genUnitOn:
          imgPath = GENERAL_UNIT_ON;
          break;
        case genUnitOff:
          imgPath = GENERAL_UNIT_OFF;
          break;
        case genLedGreen:
          imgPath = GENERAL_LED_GREEN;
          break;
        case genLedRed:
          imgPath = GENERAL_LED_RED;
          break;
        case genInpBackspace:
          imgPath = GENERAL_BUTTON_BACKSPACE;
          break;
        case genInpBackspace2:
          imgPath = GENERAL_BUTTON_BACKSPACE2;
          break;
        case genInpClear:
          imgPath = GENERAL_BUTTON_CLEAR;
          break;
        case genInpClear2:
          imgPath = GENERAL_BUTTON_CLEAR2;
          break;
        case genLearnSave:
          imgPath = GENERAL_LEARN_SAVE;
          break;
        case genLearnSaved:
          imgPath = GENERAL_LEARN_SAVED;
          break;
        }
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = imgText;
        img->fontSize = fontSize;
        img->xOffset = 0;
        img->yOffset = 0;
        _list.append(img);
      }
      break;

    case itMenu:
      for (i=0; i<mnCount; i++) {
        imgPath = "";
        switch(i) {
        case mnButtonOn:
          imgPath = MENU_BUTTON_ON;
          break;
        case mnButtonOff:
          imgPath = MENU_BUTTON_OFF;
          break;
        case mnPrime:
          imgPath = MENU_PRIME;
          break;
        case mnPrimeOff:
          imgPath = MENU_PRIME_OFF;
          break;
        case mnBack:
          imgPath = MENU_BACK;
          break;
        case mnHome:
          imgPath = MENU_HOME;
          break;
        case mnHomeOff:
          imgPath = MENU_HOME_OFF;
          break;
        case mnHome2:
          imgPath = MENU_HOME2;
          break;
        case mnHome2Off:
          imgPath = MENU_HOME2_OFF;
          break;
        case mnAlarmOn:
          imgPath = MENU_ALM_ON;
          break;
        case mnAlarmOff:
          imgPath = MENU_ALM_OFF;
          break;
        case mnMenu:
          imgPath = MENU_MENU;
          break;
        case mnLock:
          imgPath = MENU_LOCK;
          break;
        case mnUnlock:
          imgPath = MENU_UNLOCK;
          break;
        case mnLogin:
          imgPath = MENU_LOGIN;
          break;
        case mnTrend:
          imgPath = MENU_TREND;
          break;
        case mnLearn:
          imgPath = MENU_LEARN;
          break;
        case mnEventLog:
          imgPath = MENU_EVENT_LOG;
          break;
        case mnAlarmCfg:
          imgPath = MENU_ALARM_CFG;
          break;
        case mnUnitSelect:
          imgPath = CFG_NETWORK_SETUP_IMG;
          break;
        case mnLockIndicator:
          imgPath = MENU_LOCK_INDIC;
          break;
        case mnConsumption:
          imgPath = MENU_CONSUMPTION;
          break;
        }

        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = "";
        img->fontSize = 0;
        img->xOffset = 0;
        img->yOffset = 0;
        _list.append(img);
      }
      break;

    case itSelection:
      for (i=0; i<selCount; i++) {
        imgPath = "";
        switch (i) {
        case selOk:
          imgPath = SEL_OK;
          break;
        case selCancel:
          imgPath = SEL_CANCEL;
          break;
        }
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = "";
        img->fontSize = 0;
        img->xOffset = 0;
        img->yOffset = 0;
        _list.append(img);
      }
      break;

    case itKnifeGate:
      for (i=0; i<knifeGateCount; i++) {
        imgPath = "";
        imgText = "";
        switch (i) {
        case knifeGateOn:
          imgPath = GENERAL_KNIFEGATE_ON;
          break;
        case knifeGateOff:
          imgPath = GENERAL_KNIFEGATE_OFF;
          break;
        }
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = "";
        img->fontSize = 0;
        img->xOffset = 0;
        img->yOffset = 0;
        _list.append(img);
      }
      break;

    case itActive:
      for (i=0; i<actCount; i++) {
        imgPath = "";
        imgText = "";
        switch (i) {
        case actYes:
          imgPath = ACT_YES;
          GetEnumeratedText(etOnOff,1,&imgText);
          break;
        case actNo:
          imgPath = ACT_NO;
          GetEnumeratedText(etOnOff,0,&imgText);
          break;
        }
        fullPath = filePath + imgPath;

        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = imgText;
        img->fontSize = DEFAULT_SUBTEXT_FONTSIZE;
        img->xOffset = 0;
        img->yOffset = 0;
        _list.append(img);
      }
      break;

    case itLanguages:
      for (i=0; i<lanCount; i++) {
        imgPath = "";
        switch(i) {
        case lanUK:
          imgPath = FLAGS_UK_IMG;
          break;

        case lang1:
          imgPath = FLAGS_LANG_1_IMG;
          break;

        case lang2:
          imgPath = FLAGS_LANG_2_IMG;
          break;

        case lang3:
          imgPath = FLAGS_LANG_3_IMG;
          break;

        case lang4:
          imgPath = FLAGS_LANG_4_IMG;
          break;

        case lang5:
          imgPath = FLAGS_LANG_5_IMG;
          break;

        case lang6:
          imgPath = FLAGS_LANG_6_IMG;
          break;

        case lang7:
          imgPath = FLAGS_LANG_7_IMG;
          break;

        case lang8:
          imgPath = FLAGS_LANG_8_IMG;
          break;

        case lang9:
          imgPath = FLAGS_LANG_9_IMG;
          break;

        case lang10:
          imgPath = FLAGS_LANG_10_IMG;
          break;

        case lang11:
          imgPath = FLAGS_LANG_11_IMG;
          break;

        case lang12:
          imgPath = FLAGS_LANG_12_IMG;
          break;

        case lang13:
          imgPath = FLAGS_LANG_13_IMG;
          break;

        case lang14:
          imgPath = FLAGS_LANG_14_IMG;
          break;

        case lang15:
          imgPath = FLAGS_LANG_15_IMG;
          break;

        case lang16:
          imgPath = FLAGS_LANG_16_IMG;
          break;

        case lang17:
          imgPath = FLAGS_LANG_17_IMG;
          break;
        }
        fullPath = filePath + imgPath;
        img = new ImageStruct;
        img->imgPath = fullPath;
        img->imgText = "";
        img->fontSize = 0;
        img->xOffset = 0;
        img->yOffset = 0;
        _list.append(img);
      }
      break;

    case itConfig:
      for (i=0; i<cfgCount; i++) {
        imgPath = "";
        imgText = "";
        yOffset = 0;
        switch(i) {
        case cfgConfig:
          imgPath = CFG_CONFIG_IMG;
          break;
        case cfgAdvConfig:
          imgPath = CFG_ADV_CONFIG_IMG;
          break;
        case cfgMaster:
          imgPath = CFG_MASTER_SETUP_IMG;
          break;
        case cfgNetworkSetup:
          imgPath = CFG_NETWORK_SETUP_IMG;
          break;
        case cfgFillSystem:
          imgPath = CFG_FILL_SYSTEM_IMG;
          break;
        case cfgLevels:
          imgPath = CFG_LEVELS_IMG;
          break;
        case cfgCal_Menu:
          imgPath = CFG_CAL_MENU;
          imgText = tr("loadcell");
          break;
        case cfgCal_Calib:
          imgPath = CFG_CAL_CALIB;
          imgText = tr("calibrate");
          break;
        case cfgCal_Zero:
          imgPath = CFG_CAL_ZERO;
          imgText = tr("zero");
          break;
        case cfgCal_Check:
          imgPath = CFG_CAL_CHECK;
          imgText = tr("check");
          break;
        case cfgTol_Menu:
          imgPath = CFG_TOL_MENU;
          break;
        case cfgRcp_Menu:
          imgPath = CFG_RCP_MENU;
          break;
        case cfgRcp_Save_Menu:
          imgPath = CFG_RCP_SAVE_MENU;
          break;
        case cfgMultiUnit:
          imgPath = CFG_MULTI_UNIT;
          break;
        }
        fullPath = filePath + imgPath;
        img = new ImageStruct;
        img->imgText = imgText;
        img->fontSize = DEFAULT_SUBTEXT_FONTSIZE;
        img->xOffset = 0;
        img->yOffset = DEFAULT_SUBTEXT_OFFSET;
        img->imgPath = fullPath;
        _list.append(img);
      }
      break;

    case itFileHandle:
      for (i=0; i<fhCount; i++) {
        imgPath = "";
        switch(i) {
        case fhNew:
          imgPath = FH_NEW;
          break;
        case fhRename:
          imgPath = FH_RENAME;
          break;
        case fhSave:
          imgPath = FH_SAVE;
          break;
        case fhDelete:
          imgPath = FH_DELETE;
          break;
        case fhDeleteAll:
          imgPath = FH_DELETE_ALL;
          break;
        case fhSearch:
          imgPath = FH_SEARCH;
          break;
        }

        fullPath = filePath + imgPath;
        img = new ImageStruct;
        img->imgText = "";
        img->fontSize = 0;
        img->xOffset = 0;
        img->yOffset = 0;
        img->imgPath = fullPath;
        _list.append(img);
      }
      break;

    case itDeviceType:
      for (i=0; i<mcdtCount; i++) {
        imgPath = "";
        switch (i) {
        case mcdtNone:
          imgPath = "";
          break;
        case mcdtWeight:
          imgPath = DEVICE_UNIT_WEIGHT;
          break;
        case mcdtBalance:
          imgPath = DEVICE_UNIT_BALANCE;
          break;
        case mcdtBalanceHO:
          imgPath = DEVICE_UNIT_HO;
          break;
        case mcdtBalanceRegrind:
          imgPath = DEVICE_UNIT_BALANCE_REGRIND;
          break;
        case mcdtBalanceHORegrind:
          imgPath = DEVICE_UNIT_HO_REGRIND;
          break;

        }
        fullPath = filePath + imgPath;
        img = new ImageStruct;
        img->imgText = "";
        img->fontSize = 0;
        img->xOffset = 0;
        img->yOffset = 0;
        img->imgPath = fullPath;
        _list.append(img);
      }
      break;

    case itAlarmWarning:
      for (i=0; i<awCount; i++) {
        imgPath = "";
        switch (i) {
        case awAlarm:
          imgPath = AW_ALARM;
          break;
        case awAlarmBig:
          imgPath = AW_ALARM_BIG;
          break;
        case awWarning:
          imgPath = AW_WARNING;
          break;
        case awWarningBig:
          imgPath = AW_WARNING_BIG;
          break;
        }
        fullPath = filePath + imgPath;
        img = new ImageStruct;
        img->imgText = "";
        img->fontSize = 0;
        img->xOffset = 0;
        img->yOffset = 0;
        img->imgPath = fullPath;
        _list.append(img);
      }
      break;
    }
  }
}


void global::SetAppPath(QString * _path)
{
  ClearImages();
  _appPath = &Rout::appPath;
  LoadImages();
}


void global::GetDefaultMaterial(QString * matName,int idx)
{
  /* Returns the default material FILE NAME of unit with given index */

  /* All default materials start with lowercase character "d".
   Do not use localized text in this function. A filename is generated, this should not be traslated or localized. */
  *matName="d";

  /*----- Add the dosing tool, which starts with a capital character -----*/
  switch(sysCfg[idx].dosTool) {
  case dtGX:
    *matName += "Gx";
    break;
  case dtGLX:
    *matName += "Glx";
    break;
  case dtHX:
    *matName += "Hx";
    break;
  case dtA50:
    *matName += "A50";
    break;
  case dtA30:
    *matName += "A30";
    break;
  case dtA20:
    *matName += "A20";
    break;
  case dtA15:
    *matName += "A15";
    break;
  default:
    *matName += "Glx";
    break;
  }

  /*----- Add granulate type, which also starts with a capitol character -----*/
  switch(sysCfg[idx].granulate) {
  case gtNormal:
    *matName += "Ng";
    break;
  case gtMicro:
    *matName += "Mg";
    break;
  default:
    *matName += "Ng";
    break;
  }
}


void global::GetDefaultMaterialName(QString *matName,int idx,bool extract)
{
  /* Returns the display name of a default material for unit with given index */

  /*----- Initialize default material name. -----*/
  if (!(extract)) {
    *matName="";

    QString tmp;
    GetEnumeratedText(etDosTool,sysCfg[idx].dosTool,&tmp);
    *matName += tmp;

    /*----- Add a dash -----*/
    *matName += " - ";

    GetEnumeratedText(etGranulate,sysCfg[idx].granulate,&tmp);
    *matName += tmp;
  }
  else {
    /* Extract the material name for display from the current matName variable */
    QString txt = *matName;
    int split = 0;
    txt = txt.right((txt.length()-1));
    /*----- search capital letter in material name. -----*/
    for (int i=1; i<txt.length(); i++) {
      if ((txt[i] >= 'A') && (txt[i] <= 'Z')) {
        split = i;
      }
    }

    if (split != 0) {
      QString cyl = txt.left(split);
      QString gra = txt.right((txt.length() - split));
      cyl = cyl.toUpper();
      gra = gra.toUpper();
      *matName = cyl + " - " + gra;
    }
  }
}


QString * global::AppPath()
{
  return &Rout::appPath;
}


void global::GetDispMaterial(QString *mat, int idx)
{
  QString ext = MATERIAL_EXTENSION;
  if (!((sysCfg[idx].material.name.isEmpty()) || (sysCfg[idx].material.name.isNull()))) {
    if (sysCfg[idx].material.isDefault) {
      GetDefaultMaterialName(mat,idx);
    }
    else {
      *mat = sysCfg[idx].material.name;
      *mat = mat->left(mat->length()-ext.length());
    }
  }
  else {
    *mat = "";
  }
}


void global::GetDispMaterial(QString *mat)
{
  QString ext = MATERIAL_EXTENSION;
  if (!((mat->isEmpty()) || (mat->isNull()))) {
    *mat = mat->left(mat->length()-ext.length());
  }
  else {
    *mat = "";
  }
}


void global::GetAlarmText(int index, float value, QString *txt, bool fullTxt)
{
  if (menu != 0) {
    ((MainMenu*)(menu))->alarmHndlr->GetAlarmText(index,value,txt,fullTxt);
  }
  else {
    *txt = "";
  }
}


void global::GetEnumeratedText(eEnumType eType,int index, QString *txt)
{
  if (menu != 0) {
    ((MainMenu*)(menu))->alarmHndlr->GetEnumeratedText(eType,index,txt);
  }
  else {
    *txt = "";
  }
}


int global::GetImageCount(int _imgType, int _imgIdx)
{
  int i;
  int cnt;

  cnt = 0;
  for (i=0; i<_imgType; i++) {
    switch(i) {
    /*----- DosTools -----*/
    case itDosTool:
      cnt += dtCount;
      break;

    case itMotor:
      cnt += mtCount;
      break;

    case itGranulate:
      cnt += gtCount;
      break;

    case itFillSys:
      cnt += fsCount;
      break;

    case itType:
      cnt += ttCount;
      break;

    case itMode:
      cnt += modCount;
      break;

    case itGraviRpm:
      cnt += grCount;
      break;

    case itUserSelect:
      cnt += usCount;
      break;

    case itGeneral:
      cnt += genCount;
      break;

    case itKnifeGate:
      cnt += knifeGateCount;
      break;

    case itMcwType:
      cnt += mcwCount;
      break;

    case itBalHOType:
      cnt += balHOCount;
      break;

    case itMenu:
      cnt += mnCount;
      break;

    case itSelection:
      cnt += selCount;
      break;

    case itActive:
      cnt += actCount;
      break;

    case itLanguages:
      cnt += lanCount;
      break;

    case itConfig:
      cnt += cfgCount;
      break;

    case itFileHandle:
      cnt += fhCount;
      break;

    case itDeviceType:
      cnt += mcdtCount;
      break;
    }
  }

  cnt += _imgIdx;
  return cnt;
}


QString * global::ImagePaths(int _index)
{
  if(_index < _list.size()) {
    return &(_list.at(_index))->imgPath;
  }
  else {
    return 0;
  }
}


QString * global::GetImagePath(int _imgType, int _imgIdx)
{
  int cnt = GetImageCount(_imgType,_imgIdx);
  return ImagePaths(cnt);
}


void global::SetButtonImage(QPushButton *_btn, int imgType, int imgIdx, int borderPx)
{
  QString imgPath;
  QString dbg;
  QString * buf;

  imgPath = "";

  buf = GetImagePath(imgType,imgIdx);
  if (buf != 0) {
    imgPath = *buf;
  }

  if ((imgPath.isNull()) || (imgPath.isEmpty())) {
    /*----- If no image is found, load "NO IMG" -----*/
    imgType = itGeneral;
    imgIdx = genNoImg;
    buf = GetImagePath(imgType,imgIdx);
    if (buf != 0) {
      imgPath = *(buf);
    }
  }

  if ((!imgPath.isNull()) && (!imgPath.isEmpty())) {
    QImage img(imgPath);
    /*----- Load "NO IMG" if desired image is not found -----*/
    if (img.isNull()) {
      imgType = itGeneral;
      imgIdx = genNoImg;
      buf = GetImagePath(imgType,imgIdx);
      if (buf != 0) {
        imgPath = *(buf);
        img.load(imgPath);
      }
    }
    if (_list.size() > 0) {
      ImageStruct *str;
      QPainter painter(&img);
      int idx = GetImageCount(imgType,imgIdx);
      str = _list.at(idx);

      QFont font;
      if (str->fontSize != 0) {
        font.setPointSize(str->fontSize);
        font.setBold(true);
        painter.setFont(font);
        QRect rect(str->xOffset,str->yOffset,img.width(),img.height());
        painter.drawText(rect,Qt::AlignCenter,str->imgText);
      }
      else {
        font.setBold(true);
        painter.setFont(font);
        QRect rect(str->xOffset,str->yOffset,img.width(),img.height());
        painter.drawText(rect,Qt::AlignCenter,str->imgText);
      }
    }

    /*----- Load the image into the icon space of the button. -----*/
    if (!img.isNull()) {
      _btn->setText("");
      _btn->setIcon(QPixmap::fromImage(img));
      _btn->setIconSize(QSize((_btn->width()-(borderPx*2)),(_btn->height()-(borderPx*2))));
    }

    /*Alternative way of loading image as background for the button. (button does not display a clear "clicked" state). ===
      imgPath = "background-image: url("+fileName+"); background-repeat:no-repeat; background-position: center center;";
      m_ui->bt12->setStyleSheet(imgPath); */
  }
  else {
    if(_btn != 0) {
      _btn->setText("");
      _btn->setIcon(QIcon());
    }
  }
}


void global::SetLabelImage(QLabel* _lbl,int imgType,int imgIdx)
{
  QString imgPath;
  QString dbg;
  QString * buf;

  imgPath = "";
  buf = GetImagePath(imgType,imgIdx);
  if (buf != 0) {
    imgPath = *buf;
  }

  if ((imgPath.isNull()) || (imgPath.isEmpty())) {
    /*----- If no image is found, load "NO IMG" -----*/
    imgType = itGeneral;
    imgIdx = genNoImg;
    buf = GetImagePath(imgType,imgIdx);
    if (buf != 0) {
      imgPath = *(buf);
    }
  }

  if ((!imgPath.isNull()) && (!imgPath.isEmpty())) {
    QImage img(imgPath);
    /*----- Load "NO IMG" if desired image is not found -----*/
    if (img.isNull()) {
      imgType = itGeneral;
      imgIdx = genNoImg;
      buf = GetImagePath(imgType,imgIdx);
      if (buf != 0) {
        imgPath = *(buf);
        img.load(imgPath);
      }
    }
    if (_list.size() > 0) {
      ImageStruct *str;
      QPainter painter(&img);
      int idx = GetImageCount(imgType,imgIdx);
      str = _list.at(idx);

      QFont font;
      if (str->fontSize != 0) {
        font.setPointSize(str->fontSize);
        font.setBold(true);
        painter.setFont(font);
        QRect rect(str->xOffset,str->yOffset,img.width(),img.height());
        painter.drawText(rect,Qt::AlignCenter,str->imgText);
      }
      else {
        font.setBold(true);
        painter.setFont(font);
        QRect rect(str->xOffset,str->yOffset,img.width(),img.height());
        painter.drawText(rect,Qt::AlignCenter,str->imgText);
      }
    }

    /*----- Load the image into the icon space of the button. -----*/
    if (!img.isNull()) {
      _lbl->clear();
      _lbl->setPixmap(QPixmap::fromImage(img));
      _lbl->setPixmap(_lbl->pixmap()->scaledToHeight((_lbl->height())));
    }

    /* === Alternative way of loading image as background for the button. (button does not display a clear "clicked" state). ===
      imgPath = "background-image: url("+fileName+"); background-repeat:no-repeat; background-position: center center;";
      m_ui->bt12->setStyleSheet(imgPath); */
  }
  else {

    if(_lbl != 0) {
      _lbl->setText("");
    }
  }
}


void global::LanguageUpdate()
{
  MstCfg_SetRpmUnits(tr("rpm"));
}


void global::SetTranslator(QTranslator *trans)
{
  this->translator = trans;
}


void global::ClearImages()
{
  ImageStruct *img;
  while(!_list.empty()) {
    img = _list.at(0);
    _list.pop_front();
    delete img;
  }
}


void global::SetUser(eUserType ut,bool startup)
{

  if (ut < utCount) {
    if (ut != currentUser) {
      float old = currentUser;
      currentUser = ut;
      if (!startup) {
        AddEventItem(-1,2,TXT_INDEX_USER_CHANGED,ut,old);
      }
      emit MstCfg_UserChanged(currentUser);
    }
  }
  else {
    currentUser = utNone;
  }
}


eUserType global::GetUser()
{
  return currentUser;
}


/*----- PUBLIC SLOTS: -----*/
void global::SysSts_SetRpm(float _rpm, int idx)
{
  /* Set current motor RPM for display */
  /*----- update value -----*/
  if(_rpm != sysSts[idx].rpm) {
    /*----- Schrijf in struct -----*/
    sysSts[idx].rpm = _rpm;
    /*----- send signal with new value -----*/
    emit SysSts_RpmChanged(_rpm,idx);
  }
}


void global::SysCfg_SetRpm(float _rpm,int idx)
{
  /* Set RPM for grRPM production mode */
  if (_rpm != sysCfg[idx].setRpm) {
    float old = sysCfg[idx].setRpm;
    sysCfg[idx].setRpm = _rpm;
    AddEventItem(idx,2,TXT_INDEX_RPM_CHANGED,_rpm,old);
    emit SysCfg_RpmChanged(_rpm,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::SysSts_SetMass(float _mass, int idx)
{
  /*----- update value -----*/
  if (_mass != sysSts[idx].mass) {
    sysSts[idx].mass = _mass;
    /*----- send signal with new value -----*/
    emit SysSts_MassChanged(sysSts[idx].mass,idx);
  }
}


void global::SysSts_SetFastMass(float _mass,int idx)
{

  if (_mass != sysSts[idx].fastMass) {
    sysSts[idx].fastMass = _mass;
    emit SysSts_FastMassChanged(sysSts[idx].fastMass,idx);
  }
}


void global::SysSts_SetFillStatus(int _sts, int idx)
{
  if (_sts != sysSts[idx].fillSts) {
    sysSts[idx].fillSts = _sts;
    emit SysSts_FillChanged(sysSts[idx].fillSts,idx);
  }
}


void global::SysSts_SetMotorStatus(int _sts, int idx)
{
  if (_sts != sysSts[idx].motorSts) {
    sysSts[idx].motorSts = _sts;
    emit SysSts_MotorStsChanged(sysSts[idx].motorSts,idx);
  }
}


void global::SysCfg_SetID(const QString &_id, int idx)
{
  if (_id != sysCfg[idx].MC_ID) {
    sysCfg[idx].MC_ID = _id;
    //@@        AddEventItem(idx,2,); AddEventItem accepteert geen QString als parameter
    emit SysCfg_IDChanged(sysCfg[idx].MC_ID,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::SysSts_SetMCStatus(eMCStatus _sts, byte actIsSet, int idx)
{
  /*-----------------*/
  /* Set unit status */
  /*-----------------*/
  bool actIsSetStatus;

  /*----- Set actIsSet boolean status ----*/
  actIsSetStatus = (actIsSet != 0);

  /*----- Detect change in unit status or in actIsSet status -----*/
  if ((_sts != sysSts[idx].MCStatus) || (sysSts[idx].actIsSet != actIsSetStatus)) {
    sysSts[idx].MCStatus = _sts;
    sysSts[idx].actIsSet = actIsSetStatus;
    emit SysSts_MCStatusChanged(sysSts[idx].MCStatus,idx);
  }
}


void global::SysSts_SetDosAct(float _dos, int idx)
{
  if (_dos != sysSts[idx].actProd) {
    sysSts[idx].actProd = _dos;
    emit SysSts_DosActChanged(sysSts[idx].actProd,idx);
  }
}


void global::SysCfg_SetDosSet(float _dos, int idx)
{
  if (_dos != sysSts[idx].setProd) {
    sysSts[idx].setProd = _dos;
    emit SysCfg_DosSetChanged(sysSts[idx].setProd, idx);
  }
}


void global::SysCfg_SetColPct(float _col, int idx)
{
  if (_col != sysCfg[idx].colPct) {
    float oldVal = sysCfg[idx].colPct;
    sysCfg[idx].colPct = _col;
    AddEventItem(idx,2,TXT_INDEX_COLOR_PCT_CHANGED,_col,oldVal);
    emit SysCfg_ColorPctChanged(sysCfg[idx].colPct,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::SysCfg_SetShotWeight(float _wght, int idx)
{
  /*----------------*/
  /* Set shotweight */
  /*----------------*/
  //  if (_wght != sysCfg[idx].injection.shotWeight) {
  float oldVal = sysCfg[idx].injection.shotWeight;

  /*----- The entered Shot weight is copied to all units -----*/
  for (int i = 0; i<CFG_MAX_UNIT_COUNT; i++) {
    sysCfg[i].injection.shotWeight = _wght;
  }

  AddEventItem(idx,2,TXT_INDEX_SHOTWEIGHT_CHANGED,_wght,oldVal);
  emit SysCfg_ShotWeightChanged(sysCfg[idx].injection.shotWeight,idx);
  emit SysCfg_SaveConfigToFile(idx);
  //  }
}


/*----- ShotTime configuration -----*/
void global::SysCfg_SetShotTime(float _time, int idx)
{
  if (_time != sysCfg[idx].injection.shotTime) {
    float old = sysCfg[idx].injection.shotTime;

    /*----- The entered Shot time is copied to all units -----*/
    for (int i = 0; i<CFG_MAX_UNIT_COUNT; i++) {
      sysCfg[i].injection.shotTime = _time;
    }

    AddEventItem(idx,2,TXT_INDEX_DOSTIME_CHANGED,_time,old);
    emit SysCfg_ShotTimeChanged(sysCfg[idx].injection.shotTime,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


/*----- Actual shottime during production (count-down) -----*/
void global::SysSts_SetShotTime(float _time, int idx)
{
  if (_time != sysSts[idx].injection.shotTimeAct) {
    sysSts[idx].injection.shotTimeAct = _time;
    emit SysSts_ShotTimeChanged(sysSts[idx].injection.shotTimeAct,idx);
  }
}


void global::SysSts_SetMeteringAct(float time, int idx)
{
  if (time != sysSts[idx].injection.meteringAct) {
    sysSts[idx].injection.meteringAct = time;
    emit SysSts_MeteringActChanged(sysSts[idx].injection.meteringAct,idx);
  }
}


void global::SysCfg_SetMeteringStart(float time, int idx)
{
  if (time != sysCfg[idx].injection.meteringStart) {
    sysCfg[idx].injection.meteringStart = time;
    emit SysCfg_MeteringStartChanged(sysCfg[idx].injection.meteringStart,idx);
  }
}

void global::SysSts_SetRegrindStatus(float regPctAct,byte regBandAct,float fillStartLevel,int idx){
  /*------------------------------*/
  /* Update the Regrind status    */
  /*------------------------------*/
  /*
    x++;//regrind unit???
    if ((regPctAct != sysSts[idx].regPctAct) ||
            (regBandAct != sysSts[idx].regBandAct) ||
            (fillStartLevel != sysSts[idx].fillStartLevel)) {
        sysSts[idx].regPctAct = regPctAct;
        sysSts[idx].regBandAct = regBandAct;
        sysSts[idx].fillStartLevel = fillStartLevel;

    }


emit SysSts_
*/
#warning todooooooooo
}

void global::SysSts_SetMcWeightCap(float cap, float capTotal, float colCapPctTotal, int idx)
{
  /*------------------------------*/
  /* Update the McWeight capacity */
  /*------------------------------*/
  if (Rout::McWeightPresent()) {                                               /* McWeight enabled in configuration ? */
    if ((cap != sysSts[idx].mcWeightSts.extCap) ||
        (capTotal != sysSts[idx].mcWeightSts.extCapTotal) ||
        (colCapPctTotal != (100-sysSts[idx].mcWeightSts.mcWeightPct))) {
      sysSts[idx].mcWeightSts.extCap = cap;
      sysSts[idx].mcWeightSts.extCapTotal = capTotal;
      /*----- Calculate the percentage of the McWeight main material -----*/
      float mcWeightPct = 100-colCapPctTotal;
      sysSts[idx].mcWeightSts.mcWeightPct = mcWeightPct;
      emit SysSts_McWeightCapChanged(cap,capTotal,mcWeightPct,idx);
    }
  }
}


void global::SysSts_SetMeteringValid(bool valid, int idx)
{
  if (valid != sysSts[idx].injection.meteringValid) {
    sysSts[idx].injection.meteringValid = valid;
    emit SysSts_MeteringValidChanged(sysSts[idx].injection.meteringValid,idx);
  }
}


void global::SysCfg_SetExtCap(float _cap, int idx)
{

  if (_cap != sysCfg[idx].extrusion.extCapacity) {
    float old = sysCfg[idx].extrusion.extCapacity;
    /*----- The entered extruder capacity is copied to all units -----*/
    for (int i = 0; i<CFG_MAX_UNIT_COUNT; i++) {
      sysCfg[i].extrusion.extCapacity = _cap;
    }
    AddEventItem(idx,2,TXT_INDEX_EXTCAP_CHANGED,_cap,old);
    emit SysCfg_ExtCapChanged(sysCfg[idx].extrusion.extCapacity,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::SysSts_SetExtCap(float cap, int idx)
{
  if (cap != sysSts[idx].extrusion.extCapacityAct) {
    sysSts[idx].extrusion.extCapacityAct = cap;
    emit SysSts_ExtCapChanged(sysSts[idx].extrusion.extCapacityAct,idx);
  }
}


void global::SysCfg_SetMaxTacho(float _tacho, int idx)
{
  if (_tacho != sysCfg[idx].extrusion.tachoMax) {
    float old = sysCfg[idx].extrusion.tachoMax;

    /*----- The entered tacho max is copied to all units -----*/
    for (int i = 0; i<CFG_MAX_UNIT_COUNT; i++) {
      sysCfg[i].extrusion.tachoMax = _tacho;
    }

    AddEventItem(idx,2,TXT_INDEX_TACHOMAX_CHANGED,_tacho,old);
    emit SysCfg_TachoMaxChanged(sysCfg[idx].extrusion.tachoMax,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::SysSts_SetTacho(float _tacho, int idx)
{
  if (_tacho != sysSts[idx].extrusion.tachoAct) {
    sysSts[idx].extrusion.tachoAct = _tacho;
    emit SysSts_TachoChanged(sysSts[idx].extrusion.tachoAct,idx);
  }
}


void global::SysSts_SetProdToGo(float _prod,int idx)
{
  int i;

  /*----- Enable prodToGo refresh when switched to home screen -----*/
  if (Rout::homeScreenActive) {
    Rout::homeScreenActive = false;
    for (i=0; i <= glob->mstCfg.slaveCount; i++) {
      sysSts[i].productsToGo = 0;
    }
  }

  if (_prod != sysSts[idx].productsToGo) {
    sysSts[idx].productsToGo = _prod;
    emit SysSts_ProdToGoChanged(_prod,idx);
  }
}


void global::SysSts_SetLearnSts(bool calibrated, int idx)
{
  if (sysSts[idx].balanceCalibrated != calibrated) {
    sysSts[idx].balanceCalibrated = calibrated;
    emit SysSts_LearnStsChanged(calibrated,idx);
  }
}


void global::SysCfg_SetMaterial(const QString &_mat, bool isDefault, int idx, bool loadFromFile)
{
  /* LoadFromFile is default TRUE. Mag alleen FALSE zijn bij onderdrukking tijdens Pre-calibratie (Learn online/offline)*/
  if ((_mat != sysCfg[idx].material.name) || (isDefault != sysCfg[idx].material.isDefault)) {
    if (loadFromFile) {
      /*----- Store values in desired sysCfg struct -----*/
      sysCfg[idx].material.name = _mat;
      sysCfg[idx].material.isDefault = isDefault;
      /*----- Trigger load (which uses the sysCfg[idx].material struct to get it's data) -----*/
      GBI_LoadMaterialFromFile(idx);
      /*----- Emit signals to notify others that Material has changed. -----*/
      emit SysCfg_MaterialChanged(sysCfg[idx].material.name,idx);
      emit SysCfg_SaveConfigToFile(idx);
    }

    /*---- Set material name without loading from file ----*/
    else {
      /*----- Suppress changes to filesystem, only change display text. -----*/
      sysCfg[idx].material.name = _mat;

      sysCfg[idx].material.isDefault = isDefault;
      emit SysCfg_MaterialChanged(sysCfg[idx].material.name,idx);
    }
  }
}


void global::MstCfg_SetExtUnits(const QString &_units)
{
  if (_units != extrUnits) {
    extrUnits = _units;
    emit MstCfg_ExtUnitsChanged(extrUnits);
  }
}


void global::MstCfg_SetShotUnits(const QString &_units)
{
  if (_units != shotUnits) {
    shotUnits = _units;
    emit MstCfg_ShotUnitsChanged(shotUnits);
  }
}


void global::MstCfg_SetTimeUnits(const QString &_units)
{
  if (_units != timerUnits) {
    timerUnits = _units;
    emit MstCfg_TimeUnitsChanged(timerUnits);
  }
}


void global::MstCfg_SetWeightUnits(const QString &_units)
{
  if (_units != weightUnits) {
    weightUnits = _units;
    emit MstCfg_WeightUnitsChanged(weightUnits);
  }
}


void global::MstCfg_SetRpmUnits(const QString &_units)
{
  if (_units != rpmUnits) {
    rpmUnits = _units;
    emit MstCfg_RpmUnitsChanged(rpmUnits);
  }
}


void global::MstCfg_SetTachoUnits(const QString &_units)
{
  if (_units != tachoUnits) {
    tachoUnits = _units;
    emit MstCfg_TachoUnitsChanged(tachoUnits);
  }
}


void global::MstSts_SetCalState(eCalState cal)
{
  if (mstCfg.calState != cal) {
    mstCfg.calState = cal;
    emit CalState(cal);
  }
}


void global::MstSts_SetPrimeAvailable(bool _active)
{
  if (menu != 0) {
    ((MainMenu*)(menu))->DisplayPrime(_active);
  }
}


eCalState global::GetCalState()
{
  return mstCfg.calState;
}


/*----- Current Configuration slots -----*/
void global::SysCfg_SetDosTool(eDosTool dt,int idx)
{
  if (sysCfg[idx].dosTool != dt) {
    int old = sysCfg[idx].dosTool;
    sysCfg[idx].dosTool = dt;

    /*----- Load default material -----*/
    QString txt;
    GetDefaultMaterial(&txt,idx);
    txt = txt + MATERIAL_EXTENSION;
    SysCfg_SetMaterial(txt,true,idx);
    /*----- Signal changes -----*/
    AddEventItem(idx,2,TXT_INDEX_DOSTOOL_CHANGED,(float)dt,float(old));
    emit SysCfg_DosToolChanged(sysCfg[idx].dosTool,idx);
    /*----- Save config to file is already done by SetMaterial. -----*/
  }
}


void global::SysCfg_SetKnifeGate(eKnifeGate kn,int idx)
{
  if (sysCfg[idx].fillSystem.knifeGateValve != kn) {
    sysCfg[idx].fillSystem.knifeGateValve = kn;
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::MstCfg_SetMcwType(eMcwtype mcwType,int idx)
/*------------------------------------------*/
/* Setup McWeight type and factory defaults */
/*------------------------------------------*/
{
  if (mstCfg.mcWeightType != mcwType) {
    mstCfg.mcWeightType = mcwType;
    SetMcwDefaults(mcwType,idx);              /* Setup Mcw defaults and save to file */
  }
}


void global::MstCfg_SetBalHOType(eBalHOtype balHOType,int idx)
/*--------------------------------------------*/
/* Setup Balance HO type and factory defaults */
/*--------------------------------------------*/
{
  if (mstCfg.mcWeightType != balHOType) {
    mstCfg.mcWeightType = balHOType;       /* BalHOtype is stored in mcWeightType !! */
    SetBalHODefaults(balHOType,idx);       /* Setup BalHO defaults and save to file */
  }
}


void global::SysCfg_SetMotorType(eMotor mt,int idx)
{
  if (sysCfg[idx].motor != mt) {
    int old = sysCfg[idx].motor;
    sysCfg[idx].motor = mt;
    emit SysCfg_MotorChanged(mt,idx);
    emit SysCfg_SaveConfigToFile(idx);
    AddEventItem(idx,2,TXT_INDEX_MOTOR_CHANGED,(float)mt,(float)old);
    switch(mt) {
    case mtLT:
      if (sysCfg[idx].dosTool == dtA30) {
        SysCfg_SetDosTool(dtGX,idx);
      }
      break;

    case mtHT:
      if ((sysCfg[idx].dosTool != dtA30) && (sysCfg[idx].dosTool != dtA30)) {
        SysCfg_SetDosTool(dtA20,idx);
      }
      break;

    default:
      break;
    }
  }
}


void global::SysCfg_SetGranulate(eGranulate gt,int idx)
{
  if (sysCfg[idx].granulate != gt) {
    int old = sysCfg[idx].granulate;
    sysCfg[idx].granulate = gt;
    AddEventItem(idx,2,TXT_INDEX_GRANULATE_CHANGED,(float)gt,float(old));
    emit SysCfg_GranulateChanged(gt,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::MstSts_SetNextUnitIndex()
/*-------------------------- -*/
/* Unit index changed         */
/* Refresh unit name and data */
/*-------------------------- -*/
{
  if (mstCfg.slaveCount != 0) {
    mstSts.actUnitIndex++;
    if (mstSts.actUnitIndex > mstCfg.slaveCount) {
      mstSts.actUnitIndex = 0;
    }
    emit MstSts_ActUnitChanged(mstSts.actUnitIndex);
    /*----- Enable unit name refresh -----*/
    emit SysCfg_IDChanged(sysCfg[mstSts.actUnitIndex].MC_ID,mstSts.actUnitIndex);
  }
}


void global::MstSts_SetPrevUnitIndex()
{
  if (mstCfg.slaveCount != 0) {
    mstSts.actUnitIndex--;
    if (mstSts.actUnitIndex < 0) {
      mstSts.actUnitIndex = mstCfg.slaveCount;

    }
    emit MstSts_ActUnitChanged(mstSts.actUnitIndex);
    /*----- Enable unit name refresh -----*/
    emit SysCfg_IDChanged(sysCfg[mstSts.actUnitIndex].MC_ID,mstSts.actUnitIndex);

  }
}


void global::MstSts_SetActUnitIndex(int idx)
{
  if (idx != mstSts.actUnitIndex) {
    if ((idx >= 0) && (idx <= mstCfg.slaveCount)) {
      mstSts.actUnitIndex = idx;
      emit MstSts_ActUnitChanged(mstSts.actUnitIndex);
      emit SysCfg_IDChanged(sysCfg[idx].MC_ID,idx);                             /* Enable unit name refresh in various screens */
    }
  }
}


void global::MstCfg_SetRecipe(const QString &rcp)
{
  mstCfg.recipe = rcp;
  emit MstCfg_RecipeChanged(mstCfg.recipe);
  emit MstCfg_SaveConfigToFile();
}




void global::SysSts_SetAlarmNr(int nr,int idx)
{
  //  if (idx == ActUnitIndex()) {
#warning Multi unit test
  if (idx <= mstCfg.slaveCount) {
    if (nr != sysSts[idx].alarmSts.nr) {
      sysSts[idx].alarmSts.nr = nr;

      /*----- Check if an alarm is already active, else signal a change -----*/
      //      if ((sysSts[idx].alarmSts.active) && (nr != ALARM_NO_ALARM)) {
      if ((sysSts[idx].alarmSts.active) || (nr == ALARM_NO_ALARM)) {
        emit SysSts_AlarmStatusChanged(sysSts[idx].alarmSts.active,idx);
      }
    }
  }
}


void global::SysSts_SetAlarmActive(bool act,int idx)
{
  //  if (idx == ActUnitIndex()) {
#warning Multi unit test
  if (idx <= mstCfg.slaveCount) {
    if (act != sysSts[idx].alarmSts.active) {
      sysSts[idx].alarmSts.active = act;

      if ((!act) && (sysSts[idx].sysActive.sysActSts == sysAborted)) {
        /*----- Sys = aborted and alarm is no longer active, return to idle -----*/
        sysSts[idx].sysActive.cmd = 0;
        sysSts[idx].sysActive.execCmd = false;
        /*----- Abort full system -----*/
        MstSts_SetSysStart(false);
      }
      emit SysSts_AlarmStatusChanged(sysSts[idx].alarmSts.active,idx);
    }
    else {
      /*----- Status has not changed, but no pop-up is active -----*/
      if ((act && ((!this->mstSts.popupActive)) && (sysSts[idx].sysActive.execCmd))) {
        sysSts[idx].sysActive.cmd = 0;
        sysSts[idx].sysActive.execCmd = false;
        MstSts_SetSysStart(false);
        emit SysSts_AlarmStatusChanged(sysSts[idx].alarmSts.active,idx);
      }
    }
  }
}


void global::SysSts_SetAlarmMode(int mod,int idx)
{
  if (idx <= mstCfg.slaveCount) {
    if (mod != sysSts[idx].alarmSts.mode) {
      sysSts[idx].alarmSts.mode = mod;
      emit SysSts_AlarmModeChanged(sysSts[idx].alarmSts.mode,idx);
    }
  }
}


void global::MstCfg_SetSlaveCount(int cnt)
{
  if (cnt != mstCfg.slaveCount) {
    if ((cnt >= 0) && (cnt <= 15)) {
      mstCfg.slaveCount = cnt;
      mstSts.isTwin = Rout::TwinMode();
      emit MstCfg_SlaveCountChanged(mstCfg.slaveCount);
      emit MstCfg_SaveConfigToFile();

      /*----- Check if current actUnitIndex is still within slaveCount -----*/
      if (ActUnitIndex() >= mstCfg.slaveCount) {
        /*----- If unitIndex out of range, reset to 0: which should always be ok. -----*/
        MstSts_SetActUnitIndex(0);
      }
    }
  }
}


/*----- MovaColor / Agent settings -----*/
void global::MvcCfg_SetLogInterval(int val,int idx)
{
  int oldVal = sysCfg[idx].MvcSettings.agent.logInterval.actVal;
  if (sysCfg[idx].MvcSettings.agent.logInterval.actVal != val) {
    sysCfg[idx].MvcSettings.agent.logInterval.actVal = val;
    emit MvcCfg_LogIntervalChanged(val,idx);

    //@@ MOET NOG GETEST WORDEN
    /*----- Check if log interval is not too fast. -----*/
    if ((val < LOG_INTERNAL_LOWEST_VALUE) && (oldVal >= LOG_INTERNAL_LOWEST_VALUE)) {
      QString txt;
      /*----- Display warning that log interval is too fast for internal memory and system will only log to USB -----*/
      ((MainMenu*)(menu))->alarmHndlr->GetMessageText(TXT_INDEX_INTERNAL_LOG_NOT_POSSIBLE,LOG_INTERNAL_LOWEST_VALUE,0,&txt);
      ((MainMenu*)(menu))->DisplayMessage(-1,0,txt,msgtAccept);
    }
  }
}


/*----- Prime Configuration slots -----*/
void global::SysSts_SetPrimeActive(bool _active, int idx, bool preCal)
{
  if (_active != sysSts[idx].prime.activate) {
    sysSts[idx].prime.activate = _active;
    sysSts[idx].prime.preCal = preCal;
    emit SysSts_PrimeActiveChanged(sysSts[idx].prime.activate,idx);
  }
}


void global::SysSts_SetPrimeStatus(bool _active, int idx)
{
  if (_active != sysSts[idx].prime.activeSts) {
    sysSts[idx].prime.activeSts = _active;

    if (sysSts[idx].prime.activate == sysSts[idx].prime.activeSts) {
      sysSts[idx].sysActive.execCmd = false;
      sysSts[idx].sysActive.cmd = 0;
      sysSts[idx].sysActive.ack = 0;
    }

    emit SysSts_PrimeStatusChanged(sysSts[idx].prime.activeSts,idx);
  }
}


void global::SysCfg_SetPrimeRpm(float _rpm, int idx)
{
  if (_rpm != sysCfg[idx].prime.rpm) {
    sysCfg[idx].prime.rpm = _rpm;
    emit SysCfg_PrimeRpmChanged(sysCfg[idx].prime.rpm,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::SysCfg_SetPrimeTime(int _time, int idx)
{
  if (_time != sysCfg[idx].prime.time) {
    sysCfg[idx].prime.time = _time;
    emit SysCfg_PrimeTimeChanged(sysCfg[idx].prime.time,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


/*----- Fill System Configuration slots -----*/
/*----- Fill System slots -----*/
void global::CfgFill_SetOnOff(bool on,int idx)
{
  if (sysCfg[idx].fillSystem.fillOnOff != on) {
    sysCfg[idx].fillSystem.fillOnOff = on;
    emit CfgFill_OnOffChanged(on,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::CfgFill_SetFillSys(eFillSys fs, int idx)
{
  if (sysCfg[idx].fillSystem.fillSysType != fs) {
    sysCfg[idx].fillSystem.fillSysType = fs;
    /*----- Set fillSystem on/off depending on fillSystem type (none == off, else == on) -----*/
    CfgFill_SetOnOff((fs == fsNone ? false : true),idx);

    emit CfgFill_FillSysChanged(fs,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::CfgFill_SetAlarmTime(float time,int idx)
{
  if (sysCfg[idx].fillSystem.alarmTime != time) {
    float old = sysCfg[idx].fillSystem.alarmTime;
    sysCfg[idx].fillSystem.alarmTime = time;

    AddEventItem(idx,2,TXT_INDEX_ME_ALARMTIME_CHANGED,time,old);
    emit CfgFill_AlmTimeChanged(time,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::CfgFill_SetFillTimeMe(float time,int idx)
{
  if (sysCfg[idx].fillSystem.fillTimeME != time) {
    float old = sysCfg[idx].fillSystem.fillTimeME;
    sysCfg[idx].fillSystem.fillTimeME = time;

    AddEventItem(idx,2,TXT_INDEX_ME_FILLTIME_CHANGED,time,old);
    emit CfgFill_TimeMeChanged(time,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::CfgFill_SetFillTimeMv(float time,int idx)
{
  if (sysCfg[idx].fillSystem.fillTimeMV != time) {
    float old = sysCfg[idx].fillSystem.fillTimeMV;
    sysCfg[idx].fillSystem.fillTimeMV = time;

    AddEventItem(idx,2,TXT_INDEX_MV_FILLTIME_CHANGED,time,old);
    emit CfgFill_TimeMvChanged(time,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::CfgFill_SetEmptyTime(float time,int idx)
{
  if (sysCfg[idx].fillSystem.emptyTime != time) {
    float old = sysCfg[idx].fillSystem.emptyTime;
    sysCfg[idx].fillSystem.emptyTime = time;

    AddEventItem(idx,2,TXT_INDEX_MV_EMPTYTIME_CHANGED,time,old);
    emit CfgFill_EmptyTimeChanged(time,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::CfgFill_SetAlarmMode(bool mode,int idx)
{
  if (sysCfg[idx].fillSystem.alarmMode != mode) {
    float old = (sysCfg[idx].fillSystem.alarmMode == true ? 1 : 0);
    float newVal = (mode == true ? 1 : 0);
    sysCfg[idx].fillSystem.alarmMode = mode;

    AddEventItem(idx,2,TXT_INDEX_FILLALM_MODE_CHANGED,newVal,old);
    emit CfgFill_AlarmModeChanged(mode,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::CfgFill_SetFillCycles(int cycles,int idx)
{
  if (sysCfg[idx].fillSystem.fillCycles != cycles) {
    float old = sysCfg[idx].fillSystem.fillCycles;
    sysCfg[idx].fillSystem.fillCycles = cycles;

    AddEventItem(idx,2,TXT_INDEX_FILLCYCLES_CHANGED,cycles,old);
    emit CfgFill_FillCyclesChanged(cycles,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::CfgFill_SetFillAlarmCycles(int cycles,int idx)
{
  if (sysCfg[idx].fillSystem.fillAlarmCycles != cycles) {
    sysCfg[idx].fillSystem.fillAlarmCycles = cycles;
    emit CfgFill_FillAlarmCyclesChanged(cycles,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::CfgFill_SetMVDelay(float time,int idx)
{
  if (sysCfg[idx].fillSystem.MVDelay != time) {
    sysCfg[idx].fillSystem.MVDelay = time;
    emit CfgFill_MVDelayChanged(time,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::CfgFill_SetManualFill(bool fill, int idx)
{
  /*-------------------------------------*/
  /* Open or close the fill valve manual */
  /*-------------------------------------*/
  if (sysCfg[idx].fillSystem.manualFill != fill) {
    sysCfg[idx].fillSystem.manualFill = fill;
    emit CfgFill_ManualFillChanged(fill,idx);
  }
}


/*----- Advanced System Configuration slots -----*/
void global::SysCfg_SetLoadcellType(int lc, int idx)
{
  if (sysCfg[idx].MvcSettings.agent.fullScale.actVal != lc) {
    sysCfg[idx].MvcSettings.agent.fullScale.actVal = lc;
    emit SysCfg_LoadCellChanged(lc,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::SysCfg_SetGraviRpm(eGraviRpm mode, int idx)
{
  if (sysCfg[idx].GraviRpm != mode) {
    float old = sysCfg[idx].GraviRpm;
    sysCfg[idx].GraviRpm = mode;

    AddEventItem(idx,2,TXT_INDEX_GRAVIRPM_CHANGED,mode,old);
    SetCorrectUnits();

    emit SysCfg_GraviRpmChanged(mode,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::SysCfg_SetDevAlarm(float dev, int idx)
{
  if (sysCfg[idx].MvcSettings.balance.devAlmPctB4.actVal != dev) {
    float old = sysCfg[idx].MvcSettings.balance.devAlmPctB4.actVal;
    sysCfg[idx].MvcSettings.balance.devAlmPctB4.actVal = dev;

    AddEventItem(idx,2,TXT_INDEX_DEV_ALM_B4,dev,old);
    emit SysCfg_DevAlarmChanged(dev,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


void global::SysCfg_SetCalDev(float dev, int idx)
{
  if (sysCfg[idx].calibDev != dev) {
    sysCfg[idx].calibDev = dev;
    emit SysCfg_CalDevChanged(dev,idx);
    emit SysCfg_SaveConfigToFile(idx);
  }
}


/*----- Master Configuration slots -----*/
void global::MstCfg_SetLanguage(eLanguages lan)
{
  if (translator != 0) {
    if (mstCfg.language != lan) {
      float old = mstCfg.language;
      mstCfg.language = lan;
      QString filePath;
      filePath = *(AppPath());
      filePath = filePath + fileSeparator;

      /*----- Load the required language file for the translator -----*/
      switch (mstCfg.language) {

      case lang1:
        filePath = filePath + LANGUAGE_FILE_LANG_1;

        break;

      case lang2:
        filePath = filePath + LANGUAGE_FILE_LANG_2;
        break;

      case lang3:
        filePath = filePath + LANGUAGE_FILE_LANG_3;
        break;

      case lang4:
        filePath = filePath + LANGUAGE_FILE_LANG_4;
        break;

      case lang5:
        filePath = filePath + LANGUAGE_FILE_LANG_5;
        break;

      case lang6:
        filePath = filePath + LANGUAGE_FILE_LANG_6;
        break;

      case lang7:
        filePath = filePath + LANGUAGE_FILE_LANG_7;
        break;

      case lang8:
        filePath = filePath + LANGUAGE_FILE_LANG_8;
        break;

      case lang9:
        filePath = filePath + LANGUAGE_FILE_LANG_9;

        break;

      case lang10:
        filePath = filePath + LANGUAGE_FILE_LANG_10;
        break;

      case lang11:
        filePath = filePath + LANGUAGE_FILE_LANG_11;
        break;

      case lang12:
        filePath = filePath + LANGUAGE_FILE_LANG_12;
        break;

      case lang13:
        filePath = filePath + LANGUAGE_FILE_LANG_13;
        break;

      case lang14:
        filePath = filePath + LANGUAGE_FILE_LANG_14;
        break;

      case lang15:
        filePath = filePath + LANGUAGE_FILE_LANG_15;
        break;

      case lang16:
        filePath = filePath + LANGUAGE_FILE_LANG_16;
        break;

      case lang17:
        filePath = filePath + LANGUAGE_FILE_LANG_17;
        break;

      default:
        break;
      }

      if (QFile::exists(filePath)) {
        translator->load(filePath);
        filePath = "Loading translation: "+filePath;
      }
      else {
        filePath = "File not found: "+filePath;
      }

      ClearImages();
      LoadImages();

      AddEventItem(-1,2,TXT_INDEX_LANGUAGE_CHANGED,lan,old);

      emit MstCfg_LanguageChanged(lan);
      emit MstCfg_SaveConfigToFile();
    }
  }
}


void global::MstCfg_SetProdMode(eMode mt)
{
  if (mstCfg.mode != mt) {
    float old = mstCfg.mode;
    mstCfg.mode = mt;

    if (mstCfg.mode == modInject) {
      for (int i=0; i<2; i++) {
        if (mstCfg.inpType == ttTacho)
          MstCfg_SetInpType(ttTimer);
      }
    }

    if (mstCfg.mode == modExtrude) {
      for (int i=0; i<2; i++) {
        if (mstCfg.inpType == ttTimer)
          MstCfg_SetInpType(ttRelay);
      }
    }
    SetCorrectUnits();

    AddEventItem(-1,2,TXT_INDEX_PRODMODE_CHANGED,mt,old);
    emit MstCfg_ProdModeChanged(mt);
    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_SetInpType(eType tt)
{
  if (mstCfg.inpType != tt) {
    float old = mstCfg.inpType;
    mstCfg.inpType = tt;
    SetCorrectUnits();
    AddEventItem(-1,2,TXT_INDEX_INPTYPE_CHANGED,tt,old);
    emit MstCfg_InputTypeChanged(tt);
    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_SetAutoStart(bool as)
{
  if (mstCfg.autoStart != as) {
    float old = mstCfg.autoStart;
    mstCfg.autoStart = as;

    AddEventItem(-1,2,TXT_INDEX_AUTOSTART_CHANGED,as,old);
    emit MstCfg_AutoStartChanged(as);
    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_SetRecipeEnable(bool re)
/*----------------------------*/
/* Set the recipe enable mode */
/*----------------------------*/
{
  if (mstCfg.rcpEnable != re) {
    float old = mstCfg.rcpEnable;
    mstCfg.rcpEnable = re;

    AddEventItem(-1,2,TXT_INDEX_RCPENABLE_CHANGED,re,old);
    emit MstCfg_RcpEnabledChanged(re);
    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_MotorAlarmEnb(bool re)
/*--------------------------*/
/* Set the motor alarm mode */
/*--------------------------*/
{
  if (mstCfg.motorAlarmEnb != re) {
    mstCfg.motorAlarmEnb = re;

    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_ProfibusEnb(bool re)
/*-------------------------*/
/* Set the Profibus enable */
/*-------------------------*/
{
  if (mstCfg.profibusEnb != re) {
    mstCfg.profibusEnb = re;

    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_ImperialMode(bool re)
/*-----------------------*/
/* Set the Imperial mode */
/*-----------------------*/
{
  if (mstCfg.imperialMode != re) {
    mstCfg.imperialMode = re;

    Rout::SetUnits();

    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_KgH_Mode(bool re)
/*------------------------*/
/* Set the Kg/H unit mode */
/*------------------------*/
{
  if (mstCfg.units_kgh != re) {
    mstCfg.units_kgh = re;

    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_SetTwinMode(bool re)
/*--------------------------*/
/* Set the twin enable mode */
/*--------------------------*/
{
  if (mstCfg.twinMode != re) {
#warning wat doet old hier? JB
    float old = mstCfg.twinMode;
    mstCfg.twinMode = re;
    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_SetDefaultLogin(eUserType ut)
{
  if (mstCfg.defLogin != ut) {
    float old = mstCfg.defLogin;
    mstCfg.defLogin = ut;

    AddEventItem(-1,2,TXT_INDEX_STARTUP_USER_CHANGED,ut,old);
    emit MstCfg_DefaultUserChanged(ut);
    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_SetSuperPass(const QString &pass)
{
  if (mstCfg.SuperPasswd != pass) {
    mstCfg.SuperPasswd = pass;

    emit MstCfg_SuperPassChanged(pass);
    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_SetToolPass(const QString &pass)
{
  if (mstCfg.ToolPasswd != pass) {
    mstCfg.ToolPasswd = pass;

    emit MstCfg_ToolPassChanged(pass);
    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_SetModbusAddr(int adr)
{
  int adres = adr;

  /*----- Range clipping 1-231 -----*/
  if (adres > 231) {
    adres = 231;
  }
  else {
    if (adres <= 0) {
      adres = 1;
    }
  }

  if (mstCfg.modbusAddr != adres) {
    mstCfg.modbusAddr = adres;

    emit MstCfg_ModbusAddrChanged(adres);
    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_SetKeyBeep(bool kb)
{
  if (mstCfg.keyBeep != kb) {
    mstCfg.keyBeep = kb;
    emit MstCfg_SaveConfigToFile();
  }
}


void global::MstCfg_SetSystemTime(const QDateTime &time)
{
  if (mstCfg.sysTime != time) {
    mstCfg.sysTime = time;

    emit MstCfg_SystemTimeChanged(time);
  }
}


void global::MstCfg_UpdateSystemTime(const QString &time)
{
  /*---------------------------------------*/
  /* Update the system clock               */
  /*---------------------------------------*/
  /* Triggered from signal DateTimeChanged */
  /*---------------------------------------*/
  QString cmd;
  QByteArray ba;

  cmd = time;

  cmd = "date "+cmd;

  /*----- Execute the actual update command in the Linux kernel -----*/
  //  QProcess * proc = new QProcess(this);
  //  proc->start(cmd);
  //  proc->waitForStarted();
  //  qDebug() << "Time set PID : " << proc->pid();
  //  qDebug() << "Error code   : " << proc->ProcessError;
  //  qDebug() << "State        : " << proc->ProcessState();
  //  proc->waitForFinished();

  ba = cmd.toLocal8Bit();
  qDebug() << "Set time " << ba.data();
  system(ba.data());

  // delete proc;

  /*----- flag for communication to signal MC_TC-arm that the RTC needs to be updated -----*/
  mstCfg.dateSet = true;

  /*-----Add to event log -----*/
  AddEventItem(-1,2,TXT_INDEX_DATETIME_CHANGED,0,0);
}


void global::MstCfg_SetTachoRatio(float val)
{
  /*---------------------*/
  /* Set the tacho ratio */
  /*---------------------*/
  mstCfg.tachoRatio = val;
  emit MstCfg_SaveConfigToFile();
}


void global::MstSts_SetSysStart(bool start)
{
  /*------------------*/
  /* Start the system */
  /*------------------*/
  if (mstSts.sysStart != start) {
    mstSts.sysStart = start;

    /* iterate through all units */
    for(int i=0; i<=mstCfg.slaveCount; i++) {
      /* Set all units to the same start value */
      /*----- Check for enabled is done internally. -----*/
      SysSts_SetSysStart(start,i);
    }
    emit MstSts_SysStartChanged(start);
  }
}


void global::MstSts_SetSysActive(bool act)
{
  /* global status variable to enable/disable all units simultaniously */
  if (mstSts.sysActive != act) {
    mstSts.sysActive = act;

    AddEventItem(-1,2,TXT_INDEX_SYSTEM_START,(act == true ? 1 : 0),0);
    /*----- If system switches to non-active, but sysStart is set. Force sysStart to inactive. -----*/
    /*----- Also implies all individual units to be set to non-active -----*/
    if ((!(mstSts.sysActive)) && (mstSts.sysStart)) {
      MstSts_SetSysStart(false);
    }
    emit MstSts_SysActiveChanged(act);
  }
}


void global::MstSts_CheckSysActive(bool,int)
{
  bool allActive = true;

  for (int i=0; i<=mstCfg.slaveCount; i++) {
    if (sysCfg[i].sysEnabled) {
      if (!(sysSts[i].sysActive.active)) {
        allActive = false;
      }
    }
  }
  MstSts_SetSysActive(allActive);
}


void global::SysSts_SetSysStart(bool act, int idx)
/*--------------------------------*/
/* Set the active state of a unit */
/*--------------------------------*/
{
  if (idx <= mstCfg.slaveCount) {
    if (sysSts[idx].sysActive.start != act) {
      sysSts[idx].sysActive.start = act;
    }
  }
}


void global::SysSts_SetSysStartAck(int ack, int idx)
{
  if (idx <= mstCfg.slaveCount) {
    if (sysSts[idx].sysActive.ack != ack) {
      sysSts[idx].sysActive.ack = ack;

      if (ack != 0) {

        if (sysSts[idx].sysActive.ack == sysSts[idx].sysActive.cmd) {
          /*---- SysAck received -----*/
          sysSts[idx].sysActive.execCmd = true;
          sysSts[idx].sysActive.cmd = 0x00;
          sysSts[idx].sysActive.ack = 0x00;
        }
      }
    }
  }
}


void global::SysSts_SetSysActive(bool act,int idx)
{
  if (idx <= mstCfg.slaveCount) {
    /* Current status of a unit */
    if (sysSts[idx].sysActive.active != act) {
      sysSts[idx].sysActive.active = act;

      /*----- Clear command when sysStatus changes -----*/
      sysSts[idx].sysActive.execCmd = false;
      sysSts[idx].sysActive.cmd = 0x00;
      sysSts[idx].sysActive.ack = 0x00;

      /*----- Transition from active to non-active: Clear start command to prevent restarting. -----*/
      if (!act) SysSts_SetSysStart(false,idx);
      if ((act) && (sysSts[idx].curveChanged)) sysSts[idx].curveChanged = false;

      emit SysSts_SysActiveChanged(act,idx);
    }
  }
}


void global::SysCfg_SetSysEnabled(bool act, int idx)
{
  /*---------------------------------------------------------------------------*/
  /* SysCfg_SetSysEnabled is an indicator if the unit is enabled in the system */
  /* and allowed to start.                                                     */
  /* Via the "multi unit form" units can be enabled/disabled                   */
  /*---------------------------------------------------------------------------*/
  if (idx <= mstCfg.slaveCount) {
    if (act != sysCfg[idx].sysEnabled) {
      sysCfg[idx].sysEnabled = act;

      if (!(act)) {
        /*----- Make sure to clear the start command of the unit if enabled is false -----*/
        SysSts_SetSysStart(false,idx);
      }

      emit SysSts_SysEnabledChanged(act,idx);
    }
  }
}


void global::SysCfg_AlarmConfig(int almNr, int mode, int idx)
{
  if (idx <= mstCfg.slaveCount) {
    if (almNr < MAX_USER_ALM) {
      //      if (mode != sysCfg[idx].MvcSettings.alarm.almConfig[almNr].actVal) {
      if (mode == 0) {
        sysCfg[idx].MvcSettings.alarm.almConfig[almNr].actVal = 0;
      }
      else {
        sysCfg[idx].MvcSettings.alarm.almConfig[almNr].actVal = 1;
      }
      emit SysCfg_SaveConfigToFile(idx);
    }
  }
  //  }
}


void global::SysCfg_SetHopperLevels(float HiHi, float Hi, float Lo, float LoLo, int idx)
{
  bool update = false;
  float old;

  /*
  All hopperlevels are stored through the same function.
  if there are any changes the configuration is writen to file
  */
  if (idx <= mstCfg.slaveCount) {
    if (sysCfg[idx].fillLevels.HiHi != HiHi) {
      sysCfg[idx].fillLevels.HiHi = HiHi;
      update = true;
    }

    if (sysCfg[idx].fillLevels.Hi != Hi) {
      sysCfg[idx].fillLevels.Hi = Hi;
      update = true;
    }

    if (sysCfg[idx].fillLevels.Lo != Lo) {
      old = sysCfg[idx].fillLevels.Lo;
      sysCfg[idx].fillLevels.Lo = Lo;

      AddEventItem(idx,2,TXT_INDEX_HOPPER_LOW_CHANGED,Lo,old);
      update = true;
    }

    if(sysCfg[idx].fillLevels.LoLo != LoLo) {
      old = sysCfg[idx].fillLevels.LoLo;
      sysCfg[idx].fillLevels.LoLo = LoLo;

      AddEventItem(idx,2,TXT_INDEX_HOPPER_LOW_LOW_CHANGED,LoLo,old);
      update = true;
    }

    if (update) {
      emit SysCfg_HopperLevelsChanged(HiHi,Hi,Lo,LoLo,idx);
      emit SysCfg_SaveConfigToFile(idx);
    }
  }
}


void global::StsSetPreCalDosAct(float dos, int idx)
{
  if (idx <= mstCfg.slaveCount) {
    if (sysSts[idx].preCalibration.preCalDosAct != dos) {
      sysSts[idx].preCalibration.preCalDosAct = dos;
      emit SysSts_PreCalDosActChanged(dos,idx);
    }
  }
}


/*----- PRIVATE SLOTS -----*/
void global::GBI_UpdateDateTime()
{
  MstCfg_SetSystemTime(QDateTime::currentDateTime());
}


int global::Proc_LoadMaterial(const QString &matName, bool isDefault, tCurveStruct *curve)
{
  /* Basic function to load a material from file.
     into a CurveStruct.
  */

  /*----- Construct material path -----*/
  QString path = *(AppPath());

  path += QString(MATERIAL_FOLDER);
  if (isDefault) {
    path += QString(MATERIAL_DEFAULT_FOLDER);
  }
  path += fileSeparator + matName;
  if (QFile::exists(path)) {
    /*----- File exists -------*/
    QFile file(path);
    if (file.size() == sizeof(tCurveStruct)) {
      /*--------- Filesize OK ---------*/
      if (file.open(QIODevice::ReadOnly)) {
        /*---- Read curve ----*/
        file.reset();
        file.read((char*)curve,sizeof(tCurveStruct));
        file.close();
        return 0;
      }
      else {
        /*---- Unable to open file ----*/
        return -3;
      }
    }
    else {
      /*---- Filesize mismatch -----*/
      return -2;
    }
  }
  else {
    /*----- File not found ----*/
    return -1;
  }
}


int global::Proc_GetMaterialDosTool(const QString &matName,bool isDefault)
{
  /*----- Read the dosTool from a material file -----*/
  tCurveStruct curve;
  int res=0;

  res = Proc_LoadMaterial(matName,isDefault,&curve);
  if (res == 0) {
    return ((int)(curve.cylSel));
  }
  else {
    return res;
  }
}


void global::GBI_LoadMaterialFromFile(int idx)
{
  /*
     Load material from file for the device with given index.
     Set _gbi.sysCfg[idx].material.name and _gbi.sysCfg[idx].material.isDefault prior to calling this function.
     The function then reads all settings from it's configuration and tries to set the desired material.
  */
  int res = 0;
  if (idx <= mstCfg.slaveCount) {
    QString material = sysCfg[idx].material.name;
    QString txt;
    bool isDef = sysCfg[idx].material.isDefault;

    res = Proc_GetMaterialDosTool(material,isDef);
#warning res < 0 is een foutcode, handling aanpassen <0 geeft index out of range !!!
    if (res == ((int)(sysCfg[idx].dosTool))) {
      tCurveStruct curve;
      Proc_LoadMaterial(material,isDef,&curve);
      /*----- copy material from local struct into sysCfg struct -----*/
      memcpy(&(sysCfg[idx].curve),&curve,sizeof(tCurveStruct));
      sysSts[idx].curveChanged = true;
    }
    else {
      switch(res) {
      case -3:
        /*----- Display a warning that the material file could not be read -----*/
        if (menu != 0) {
          ((MainMenu*)(menu))->alarmHndlr->GetMessageText(TXT_INDEX_MAT_FILE_NO_READ,0,0,&txt);
          ((MainMenu*)(menu))->DisplayMessage(idx,0,txt,msgtNone);
        }
        break;
      case -2:
        /*-----Material file size mismatch. -----*/
        if (menu != 0) {
          ((MainMenu*)(menu))->alarmHndlr->GetMessageText(TXT_INDEX_MAT_FILE_NO_READ,0,0,&txt);
          ((MainMenu*)(menu))->DisplayMessage(idx,1,txt,msgtNone);
        }
        /*----- Display a warning that the material is incompatible -----*/
        break;
      case -1:
        /*-----"Material file does not exist. -----*/
        if (menu != 0) {
          ((MainMenu*)(menu))->alarmHndlr->GetMessageText(TXT_INDEX_MAT_FILE_NOT_FOUND,0,0,&txt);
          ((MainMenu*)(menu))->DisplayMessage(idx,0,txt,msgtNone);
        }
        /*----- Display a warning that the material file was not found -----*/
        break;
      }

      /*----- Load default material for current configuration -----*/
      if (res < 0) {
        QString mat;
        GetDefaultMaterial(&mat,ActUnitIndex());
        SysCfg_SetMaterial(mat,true,ActUnitIndex());
      }
    }
  }
}


void global::SysCfg_SaveMaterial(int idx, const QString &name, bool silent)
{
  bool doCreate = true;
  tCurveStruct curve;

  QString path = *(AppPath());
  path += QString(MATERIAL_FOLDER) + fileSeparator;
  path += name;
  int popupSel = -1;

  if (QFile::exists(path)) {
    if (!(silent)) {

    }

    if ((popupSel == 1) || silent) {
      QFile::remove(path);
      doCreate = true;
    }
    else doCreate = false;
  }

  if (doCreate) {
    /*----- Copy curve from sysCfg to local variable. -----*/

    memcpy(&curve,&(sysCfg[idx].curve),sizeof(tCurveStruct));
    curve.calMode = 0x01;
    /*----- Open/Create file -----*/
    QFile file(path);
    if (file.open(QIODevice::ReadWrite)) {
      file.reset();
      /*----- Store curve into file -----*/
      file.write((char*)&curve,sizeof(tCurveStruct));
      /*----- Close file -----*/
      file.close();
      Rout::SyncFile();

      if (menu != 0) {
        QString txt;
        /*----- Display a "Material Saved" message -----*/
        ((MainMenu*)(menu))->alarmHndlr->GetMessageText(TXT_INDEX_MAT_SAVED,0,0,&txt);
        ((MainMenu*)(menu))->DisplayMessage(idx,0,txt,msgtAccept);
      }
    }
  }
}


void global::MstCfg_SaveSettings()
{
  emit MstCfg_SaveConfigToFile();
}


void global::SysCfg_SaveSettings(int idx)
{
  emit SysCfg_SaveConfigToFile(idx);
}


void global::RebuildProdScreen()
/*-------------------------------*/
/* Rebuild the production screen */
/*-------------------------------*/
{
  /*----- Signal ProdModeChanged with the actual prod mode
          to enable a production screen rebuild -----*/
  emit MstCfg_ProdModeChanged(mstCfg.mode);
}


void global::MstCfg_LoadedFromFile()
{
  /*----- Master settings changed, emit all signals to update display -----*/
  emit MstCfg_RecipeChanged(mstCfg.recipe);
  emit MstCfg_SlaveCountChanged(mstCfg.slaveCount);
  /*----- Advanced Configuration settings -----*/
  emit MstCfg_LanguageChanged(mstCfg.language);
  emit MstCfg_ProdModeChanged(mstCfg.mode);
  emit MstCfg_InputTypeChanged(mstCfg.inpType);
  emit MstCfg_AutoStartChanged(mstCfg.autoStart);
  emit MstCfg_RcpEnabledChanged(mstCfg.rcpEnable);
  emit MstCfg_DefaultUserChanged(mstCfg.defLogin);
  emit MstCfg_SuperPassChanged(mstCfg.SuperPasswd);
  emit MstCfg_ToolPassChanged(mstCfg.ToolPasswd);

  //  Rout::??????; /* Network configuration*/
}


void global::SysCfg_LoadedFromFile(int idx)
{
  if (idx <= mstCfg.slaveCount) {
    /*----- System settings changed, emit all signals to update display -----*/
    emit SysCfg_RpmChanged(sysCfg[idx].setRpm, idx);
    emit SysCfg_ColorPctChanged(sysCfg[idx].colPct, idx);
    /*-----emit SysCfg_DosSetChanged(sysCfg[idx], idx); -----*/
    emit SysCfg_ShotWeightChanged(sysCfg[idx].injection.shotWeight, idx);
    emit SysCfg_ShotTimeChanged(sysCfg[idx].injection.shotTime, idx);
    emit SysCfg_MeteringStartChanged(sysCfg[idx].injection.meteringStart,idx);
    emit SysCfg_ExtCapChanged(sysCfg[idx].extrusion.extCapacity, idx);
    emit SysCfg_TachoMaxChanged(sysCfg[idx].extrusion.tachoMax, idx);
    /*----- Sys Config unit configuration -----*/
    emit SysCfg_IDChanged(sysCfg[idx].MC_ID, idx);
    emit SysCfg_MaterialChanged(sysCfg[idx].material.name, idx);
    emit SysCfg_DosToolChanged(sysCfg[idx].dosTool, idx);
    emit SysCfg_MotorChanged(sysCfg[idx].motor, idx);
    emit SysCfg_GranulateChanged(sysCfg[idx].granulate,idx);
    emit SysCfg_LoadCellChanged(sysCfg[idx].MvcSettings.agent.fullScale.actVal,idx);
    emit SysCfg_GraviRpmChanged(sysCfg[idx].GraviRpm,idx);
    /*----- Prime Configuration signals -----*/
    emit SysCfg_PrimeRpmChanged(sysCfg[idx].prime.rpm,idx);
    emit SysCfg_PrimeTimeChanged(sysCfg[idx].prime.time,idx);
  }
}


void global::FactoryDefaultsExecuted()
{
  if (this->menu != 0) {
    QString txt;
    ((MainMenu*)(this->menu))->alarmHndlr->GetMessageText(TXT_INDEX_FACTORY_DEFAULTS_SUCCESSFUL,0,0,&txt);
    ((MainMenu*)(this->menu))->DisplayMessage(-1,0,txt,msgtAccept);
  }
}


/*----- Internal function to set precal status -----*/
void global::SetPreCalStatus(unsigned char sts, int idx)
{
  if (idx <= mstCfg.slaveCount) {
    if (sysSts[idx].preCalibration.calib.status != sts) {
      QString dbg;

      /*----- preCal raw status -----*/
      sysSts[idx].preCalibration.calib.status = sts;

      /*----- preCal process status -----*/
      sysSts[idx].preCalibration.calib.sts = calsIdle;
      if ((sts & 0x01) != 0) sysSts[idx].preCalibration.calib.sts = calsBusy;
      if ((sts & 0x04) != 0) sysSts[idx].preCalibration.calib.sts = calsPause;
      if ((sts & 0x02) != 0) sysSts[idx].preCalibration.calib.sts = calsReady;

      /*----- preCal corFac valid -----*/
      sysSts[idx].preCalibration.rpmCorrFacValid = (((sts & 0x08) != 0) ? true : false);

      /*----- trigger update -----*/
      emit SysSts_PreCalStatusChanged(sts,idx);
    }
  }
}


void global::AlarmResetRcv(int idx)
{
  emit AlarmReset(idx);
}


void global::ClearEventList()
{
  EventLogItemStruct * s;

  for (int i=0; i<eventList.count(); i++) {
    s = eventList.at(i);
    delete s;
  }
  eventList.clear();
}


void global::AddEventItem(int,int,int,float,float)
{
}


void global::GetEventText(int nr, int mode, float newValue, float oldValue, QString *txt)
{
  /*----- Get the correct text, according to message type and values (new/old) -----*/
  QString tmp;

  if (menu != 0) {
    if ((mode == 0) || (mode == 1)) {
      ((MainMenu*)(menu))->alarmHndlr->GetAlarmText(nr,newValue,&tmp,true);
    }
    else {
      ((MainMenu*)(menu))->alarmHndlr->GetMessageText(nr,newValue,oldValue,&tmp);
    }
  }
  *txt = tmp;
}


int global::GetEventCount()
{
  return eventList.count();
}


EventLogItemStruct* global::GetEventByIdx(int idx)
{
  if ((idx >= 0) && (idx < eventList.count())) {
    return eventList.at(idx);
  }
  return 0;
}


EventLogItemStruct* global::GetEventById(unsigned long id)
{
  for (int i=0; i<eventList.count(); i++) {
    if ((eventList.at(i)->eventID) == id) {
      return eventList.at(i);
    }
  }
  return 0;
}


/*----- Full config update trigger -----*/
void global::TriggerUpdate(int idx)
{
  if (idx <= mstCfg.slaveCount) {
    emit MstCfg_UserChanged(currentUser);
    emit SysCfg_DosToolChanged(sysCfg[idx].dosTool,idx);
    emit CfgFill_FillSysChanged(sysCfg[idx].fillSystem.fillSysType,idx);
    emit SysCfg_GranulateChanged(sysCfg[idx].granulate,idx);
    emit SysCfg_MotorChanged(sysCfg[idx].motor,idx);
    emit SysCfg_GraviRpmChanged(sysCfg[idx].GraviRpm,idx);

    emit SysCfg_HopperLevelsChanged(sysCfg[idx].fillLevels.HiHi,sysCfg[idx].fillLevels.Hi,sysCfg[idx].fillLevels.Lo,sysCfg[idx].fillLevels.LoLo,idx);

    emit MstCfg_InputTypeChanged(mstCfg.inpType);
    emit SysSts_PreCalStatusChanged(sysSts[idx].preCalibration.calib.status,idx);
    emit SysSts_LearnStsChanged(sysSts[idx].balanceCalibrated,idx);

    emit SysCfg_MaterialChanged(sysCfg[idx].material.name,idx);
  }
}


void global::StsTimElapsed()
{
  /*----------------------------*/
  /* Start / Stop timer elapsed */
  /*----------------------------*/
  int idx = ActUnitIndex();
  sysSts[idx].sysActive.execCmd = false;
  sysSts[idx].sysActive.cmd = 0x00;
  sysSts[idx].sysActive.ack = 0x00;
}


byte global::SystemStartHandler(int idx)
{

  /*----- Check if we are executing a command at this moment -----*/
  if (!(sysSts[idx].sysActive.execCmd)) {
    if (((sysSts[idx].prime.activate) && (!(sysSts[idx].prime.activeSts)))) {
      /*----- Enable prime -----*/
      return CMD_PRIME_START;
    }
    else {
      if (((!(sysSts[idx].prime.activate)) && (sysSts[idx].prime.activeSts))) {
        /*----- Disable prime -----*/
        return CMD_PRIME_STOP;
      }
    }

    /*----- If the unit is configured not active, but the status is active. -----*/
    if ((!(mstSts.sysStart)) || (!(sysCfg[idx].sysEnabled))) {
      if ((sysSts[idx].sysActive.active) && (sysSts[idx].sysActive.sysActSts == sysStarted)) {
        /*----- Send disable command -----*/
        return CMD_PROD_STOP;
      }
    }
    else {
      /*----- Unit is allowed to be enabled -----*/
      /*----- check for start command -----*/
      if ((sysSts[idx].sysActive.start) && (sysSts[idx].sysActive.sysActSts == sysIdle)) {
        /*----- Unit is not active -----*/
        if (!(sysSts[idx].sysActive.active)) {
          /*----- Send activate command -----*/
          if (!(stsTim->isActive())) stsTim->start();
          return CMD_PROD_START;
        }
      }
      else {                                                                    /* Unit is still active */
        if ((!(sysSts[idx].sysActive.start)) && (sysSts[idx].sysActive.sysActSts == sysStarted)) {
          /*----- Send disable command -----*/
          if (!(stsTim->isActive())) stsTim->start();
          return CMD_PROD_STOP;
        }
      }
    }
  }
  stsTim->stop();
  return CMD_NONE;
}


void global::SetSystemStatus(int idx)
{
  if (sysSts[idx].prime.activeSts) {
    sysSts[idx].sysActive.sysActSts = sysPrime;
  }
  else {
    /*----- No current command is active, so we are in an end state (idle, started, aborted) -----*/
    if (!(sysSts[idx].sysActive.execCmd)) {
      if (sysSts[idx].sysActive.active) {
        sysSts[idx].sysActive.sysActSts = sysStarted;
      }
      else {
        if (sysSts[idx].alarmSts.active) {
          sysSts[idx].sysActive.sysActSts = sysAborted;
        }
        else {
          sysSts[idx].sysActive.sysActSts = sysIdle;
        }
      }
    }
    else {                                                                      /* Some command is active */
      if (sysSts[idx].sysActive.start) {
        if (sysSts[idx].alarmSts.active) {
          /*-----Aborted but active -----*/
          sysSts[idx].sysActive.sysActSts = sysAborted;
        }
        else {
          sysSts[idx].sysActive.sysActSts = sysStarting;
        }
      }
      else {
        sysSts[idx].sysActive.sysActSts = sysStopping;
      }
    }
  }
}


void global::ComSendData()
{
  /*-------------------------------------------------------------------------------*/
  /* Communicatie routine voor het verzenden van data naar het MC_TC-arm proces.   */
  /* Deze routine kopieert alle benodigde data naar een communicatie thread        */
  /* die het verzenden en ontvangen voor zijn rekening neemt.                      */
  /*-------------------------------------------------------------------------------*/
  tMsgGui msg;
  tMcTcControlParm param;
  int idx;
  int i;
  int unit;
  idx = ActUnitIndex();

  /*------------------------------*/
  /* Loop for all connected units */
  /*------------------------------*/
  for (unit=0; unit <= mstCfg.slaveCount; unit++) {

    /*----- Update current system status -----*/
    SetSystemStatus(unit);

    msg.data[unit].prodSettings.primeRpm = sysCfg[unit].prime.rpm;

    /*----- Start -----*/
    sysSts[unit].sysActive.cmd = SystemStartHandler(unit);
    msg.data[unit].commands.sysStartCmd = sysSts[unit].sysActive.cmd;

    /*----- Remote start -----*/
    msg.data[unit].commands.remoteStart = sysSts[unit].remoteStart;

    /*----- Production settings -----*/
    msg.data[unit].prodSettings.colPct = sysCfg[unit].colPct;
    msg.data[unit].prodSettings.shotWeight = sysCfg[unit].injection.shotWeight;
    msg.data[unit].prodSettings.extCap = sysCfg[unit].extrusion.extCapacity;
    msg.data[unit].prodSettings.dosTime = sysCfg[unit].injection.shotTime;

    /*---------------------------------------------------------------------*/
    /* Construct calib data from this->calStruct                           */
    /* CalStruct.cmd is the command from the GUI                           */
    /* CalStruct.command is the actual command data to send to MC_TC-arm   */
    /*----------------------------------------------------------------------*/
    if (unit == idx) {                                                          /* Check if unit is active (idx) */

      switch(calStruct.cmd) {
      case calNone:
        calStruct.command &= 0x01;
        break;
      case calStart:
        calStruct.command |= 0x01;
        break;
      case calRefPlaced:
        calStruct.command |= 0x02;
        break;
      case calAbort:
        // Geforceerd naar 0x04, anders blijft calStart staan.
        calStruct.command = 0x04;
        break;
      case calDone:
        calStruct.command = 0x00;
        break;
      default:
        calStruct.command = 0x00;
      }

      msg.data[unit].commands.calib = calStruct.command;
    }
    /*----- Unit not active -----*/
    else {
      msg.data[unit].commands.calib = 0;
    }

    /*----- Deviation settings -----*/
    msg.data[unit].prodSettings.tachoMax = sysCfg[unit].extrusion.tachoMax;
    //    msg.data[unit].dosTimeSetMetStart = sysCfg[unit].injection.meteringStart;

    /*----- SetRPM for RPM production mode -----*/
    msg.data[unit].prodSettings.rpmSet = sysCfg[unit].setRpm;

    /*----- Man Fill Command -----*/
    msg.data[unit].commands.fillMan = (sysCfg[unit].fillSystem.manualFill == true ? 0x01 : 0x00);

    /*----- Set material changed flag -----*/
    if (sysSts[unit].curveChanged) {
      msg.data[unit].commands.curveChanged = 0x01;
    }

    else {
      msg.data[unit].commands.curveChanged = 0x00;

    }

    /*----- Pre calibration -----*/
    msg.data[unit].preCalibration.start = 0;
    msg.data[unit].preCalibration.abort = 0;
    msg.data[unit].preCalibration.pause = 0;
    msg.data[unit].preCalibration.readyAck = 0;
    msg.data[unit].preCalibration.cont = 0;

    switch(sysSts[unit].preCalibration.calib.cmd) {
    case calStart:
      msg.data[unit].preCalibration.start = 0x01;
      break;
    case calAbort:
      msg.data[unit].preCalibration.abort = 0x01;
      break;
    case calPause:
      msg.data[unit].preCalibration.pause = 0x01;
      break;
    case calDone:
      msg.data[unit].preCalibration.readyAck = 0x01;
      break;
    case calContinue:
      msg.data[unit].preCalibration.cont = 0x01;
    default:
      break;
    }

    /*----- Consumption (Not yes supported) -----*/
    msg.data[unit].commands.consReset = 0x00;                                   // Consumption reset
    msg.data[unit].commands.consResetDay = 0x00;                                // Consumption day reset

    // Alarm reset
    msg.data[unit].commands.alarmResetCmd = (sysSts[unit].alarmSts.reset == true ? 0x01 : 0x00);

    /*----- Manual I/O  -----*/
    msg.data[unit].manualIO.active = ((sysSts[unit].manIO.active == true) ? 0x01 : 0x00);
    msg.data[unit].manualIO.output[0] = ((sysSts[unit].manIO.outputs[0] == true) ? 0x01 : 0x00);
    msg.data[unit].manualIO.output[1] = ((sysSts[unit].manIO.outputs[1] == true) ? 0x01 : 0x00);
    msg.data[unit].manualIO.output[2] = ((sysSts[unit].manIO.outputs[2] == true) ? 0x01 : 0x00);
    msg.data[unit].manualIO.output[3] = ((sysSts[unit].manIO.outputs[3] == true) ? 0x01 : 0x00);
    msg.data[unit].manualIO.motorSpeed = sysSts[unit].manIO.motorSpeed;

    /*----- Curve data -----*/
    memcpy(&msg.data[unit].curve,&sysCfg[unit].curve,sizeof(tCurveStruct));

    /*----- Production Parameter struct -----*/
    param.sys.unitType = sysCfg[unit].deviceType;
    param.sys.operatingMode = (unsigned char)sysCfg[unit].GraviRpm;
    param.sys.prodType = mstCfg.mode;
    param.sys.motorAlmEnb = mstCfg.motorAlarmEnb;

    switch (mstCfg.mode) {

    case modInject:
      param.sys.extInputMode = 0;
      switch (mstCfg.inpType) {
      case ttTimer:
        param.sys.injInputMode = 0;
        break;
      case ttRelay:
        // If we are running in INJ/RPM mode, force TIMER input
        if (sysCfg[unit].GraviRpm == grRpm) {
          param.sys.injInputMode = 0;
        }
        else {
          param.sys.injInputMode = 1;
        }
        break;
      default:
        param.sys.injInputMode = 0;
        break;
      }
      break;

    case modExtrude:
      param.sys.injInputMode = 0;
      switch (mstCfg.inpType) {
      case ttRelay:
        param.sys.extInputMode = 0;
        break;
      case ttTacho:
        param.sys.extInputMode = 1;
        break;
      default:
        param.sys.extInputMode = 0;
        break;
      }
      break;

    default:
      param.sys.extInputMode = 0;
      param.sys.injInputMode = 0;
      break;
    }

    /*----- Motor current,max rpm and micro step rpm -----*/
    param.sys.maxMotorSpeed = glob->sysCfg[unit].MvcSettings.agent.maxMotorSpeed.actVal;

    switch(sysCfg[unit].motor) {
    case mtLT:
      param.sys.motorCurrent = 80;
      param.sys.motorMicroStepRpm = 50;
      break;

    case mtHT:
      param.sys.motorCurrent = 155;
      param.sys.motorMicroStepRpm = 35;
      break;

    default:
      param.sys.motorCurrent = 80;
      param.sys.motorMicroStepRpm = 50;
      break;
    }

    param.sys.motorConn6Wire = sysCfg[unit].MvcSettings.agent.motorSixWires.actVal;

    /*--- Running contact delay on/off ---*/
    param.sys.runContactOnDly = sysCfg[unit].MvcSettings.agent.runContactOnDelay.actVal;
    param.sys.runContactOffDly = sysCfg[unit].MvcSettings.agent.runContactOffDelay.actVal;
    param.sys.runContactMode = 0;

    param.sys.metDevPct = sysCfg[unit].MvcSettings.agent.MetTimeDev.actVal;
    param.sys.injRelFilt = sysCfg[unit].MvcSettings.agent.inputFilter.actVal;

    /*----- Tacho correction factor -----*/
    param.sys.tachoCorrFac = sysCfg[unit].MvcSettings.agent.tachoCorrection.actVal;

    /*----- Max motor speed -----*/
    param.sys.maxMotorSpeed = sysCfg[unit].MvcSettings.agent.maxMotorSpeed.actVal;

    /*----- Full scale -----*/
    param.weight.fullScale = (float)(sysCfg[unit].MvcSettings.agent.fullScale.actVal);
    param.weight.refWeight = 500;                                               /* Reference weight */

    /*----- Motion band [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5] -----*/
    param.weight.motionBand = ((unsigned char)((sysCfg[unit].MvcSettings.weight.motionBand.actVal+0.05)/0.5));

    /*----- Motion delay 0 - 9.0 Communicated *10 in a byte -----*/
    param.weight.motionDelay = ((unsigned char)((sysCfg[unit].MvcSettings.weight.motionDelay.actVal * 10)+0.5));

    /*----- Weight filter factor -----*/
    param.weight.weightFiltFac = sysCfg[unit].MvcSettings.weight.weightFilter.actVal;

    /*----- Weight fast filter factor -----*/
    param.weight.weightFastFiltFac = sysCfg[unit].MvcSettings.weight.weightFilterFast.actVal;

    /*----- Alarm config table -----*/
    for (i=0; i<MAX_USER_ALM; i++) {
      param.alm.almCfg[i] = sysCfg[unit].MvcSettings.alarm.almConfig[i].actVal;
    }

    /*----- Filling system settings -----*/

    /*----- Filling system ON/OFF -----*/
    param.fillSystem.fillOnOff = (sysCfg[unit].fillSystem.fillOnOff == true ? 0x01 : 0x00);

    /*----- Filling system type -----*/
    switch(sysCfg[unit].fillSystem.fillSysType) {
    case fsNone:
      param.fillSystem.fillSystem = 0;
      break;
    case fsME:
      param.fillSystem.fillSystem = 1;
      break;
    case fsMV:
      param.fillSystem.fillSystem = 2;
      break;
    case fsEX:
      param.fillSystem.fillSystem = 3;
      break;
    case fsRG:
      param.fillSystem.fillSystem = 4;
      break;

    default:
      param.fillSystem.fillSystem = 0;
      break;
    }

    /*----- McWeight parameters -----*/
    param.mcWeightParm.knifeGateValve = (sysCfg[unit].fillSystem.knifeGateValve == true ? 0x01 : 0x00);
    param.mcWeightParm.hysterese = mstCfg.hysterese;                            /* McWeight extruder hysterese */
    param.mcWeightParm.shortMeasTime = mstCfg.shortMeasTime;                    /* McWeight short measurement time */
    param.mcWeightParm.tachoCapRatio = mstCfg.tachoRatio;                       /* McWeight tacho ratio */
    param.mcWeightParm.minExtCap = mstCfg.minExtCap;                            /* McWeight minimum extruder cap */

    /*----- Filling system parameters -----*/
    param.fillSystem.fillAlmTime = sysCfg[unit].fillSystem.alarmTime;           /* Filling alarm time */
    param.fillSystem.fillTimeMV = sysCfg[unit].fillSystem.fillTimeMV;           /* Filling time MV */
    param.fillSystem.fillEmptyTime = sysCfg[unit].fillSystem.emptyTime;         /* Filling emptying time */

    /* Filling alarm mode */
    param.fillSystem.fillAlmMode = (sysCfg[unit].fillSystem.alarmMode == true ? 1 : 0);
    /* Filling alarm cycles */
    //    param.fillSystem.fillAlmCycles = (unsigned char)(sysCfg[unit].fillSystem.fillAlarmCycles);
    param.fillSystem.mvDelay = sysCfg[unit].fillSystem.MVDelay;                 /* MV Delay */

    /*------ Levels -----*/
    param.levels.hopperEmptyWeight = sysCfg[unit].fillLevels.LoLo;              /* Hopper empty weight */
    param.levels.hopperSensorFill = sysCfg[unit].fillLevels.Lo;                 /* Hopper start fill weight */
    param.levels.hopperHiLevel = sysCfg[unit].fillLevels.Hi;                    /* Hopper hi level weight */
    param.levels.hopperHiHiLevel = sysCfg[unit].fillLevels.HiHi;                /* Hopper hi hi level weight */

    /*------ Manual fill detection -----*/

    /* Mass change fill start No fill system */
    param.manFillDet.balFillStart = sysCfg[unit].MvcSettings.balance.fillStart.actVal;

    /* Mass change fill start Hopper loader */
    param.manFillDet.balFillStartHl = sysCfg[unit].MvcSettings.balance.fillStartHL.actVal;

    /* Mass change fill ready */
    param.manFillDet.balFillReady = sysCfg[unit].MvcSettings.balance.fillReady.actVal;

    /* Fill time */
    param.manFillDet.balFillTime = sysCfg[unit].MvcSettings.balance.fillTime.actVal;

    /* Lid off weight 300 gr */
    param.manFillDet.balLidOffWeight = sysCfg[unit].MvcSettings.balance.lidOffWeight.actVal;

    /*------ Consumption -----*/

    /* Consumption update time */
    param.cons.conMeasTime = sysCfg[unit].MvcSettings.balance.measureConsTime.actVal;

    /*----- Pre calibration -----*/
    param.preCal.calDev = sysCfg[unit].calibDev;                                /* Calibration dev. for auto single point +/- 5% */

    /* Calibration alarm cycles 20x */
    param.preCal.calAlmCycle = sysCfg[unit].MvcSettings.agent.calib.alarmCycles.actVal;

    /* Calibration test time for colCap < 0.5 GR/S */
    param.preCal.testTime1 = sysCfg[unit].MvcSettings.agent.calib.testLt05g.actVal;

    /* Calibration test time for colCap >= 0.5 GR/S */
    param.preCal.testTime2 = sysCfg[unit].MvcSettings.agent.calib.testGt05g.actVal;

    /*----- Balance -----*/
    param.bal.actUpdate = sysCfg[unit].MvcSettings.balance.actUpdate.actVal;    /* Video update level -->  Display "Calibrating" or "Calibrated"  */

    /*--- Curve Gain for Balance ---*/
    param.bal.curveGain = sysCfg[unit].MvcSettings.agent.curveGain.actVal;      /* Curve gain default 1.00 */

    /*--- If colPctSet changed more then 10 % returns a total reset cmd ---*/
    param.bal.colPctSetDev = sysCfg[unit].MvcSettings.agent.colPctSetDev.actVal;/* Color % Set Deviation */

    /*--- Extruder Tacho balance hysterese ---*/
    /* Ext cap sample time [sec] */
    param.bal.extCapSampleTime = sysCfg[unit].MvcSettings.agent.extCapTime.actVal;
    param.bal.extCapPctRel = sysCfg[unit].MvcSettings.agent.extCapDev.actVal;   /* Ext cap change det. pct = 1.0% */

    /*--- RPM correction ---*/
    param.bal.devB4Hyst = sysCfg[unit].MvcSettings.balance.devB4Hyst.actVal;    /* Deviation Band 4 control hysterese --> no rpm correction */
    param.bal.rpmCorFac1 = sysCfg[unit].MvcSettings.balance.rpmCorrFac1.actVal; /* Rpm correction factor 1: dev. > deviation threshold  */
    param.bal.rpmCorFac2 = sysCfg[unit].MvcSettings.balance.rpmCorrFac2.actVal; /* Rpm correction factor 2: dev. < deviation threshold */

    /* Deviation threshold Injection 15 % */
    param.bal.devThresholdInj = sysCfg[unit].MvcSettings.balance.devThreshInj.actVal;

    /* Deviation threshold Extrusion 5 % */
    param.bal.devThresholdExt = sysCfg[unit].MvcSettings.balance.devThreshExt.actVal;

    /*--- measurement time ---*/
    param.bal.minTime = sysCfg[unit].MvcSettings.balance.measureMinTime.actVal; /* Minimum measurement time */

    /* Minimum measurement time for band 4 */
    param.bal.minTimeB4 = sysCfg[unit].MvcSettings.balance.measureBand4MinTime.actVal;
    param.bal.maxTime = sysCfg[unit].MvcSettings.balance.measureMaxTime.actVal; /* Maximum measurement time */

    /* Measurement time correction factor */
    param.bal.timeCorFac = sysCfg[unit].MvcSettings.balance.measureTimeCorr.actVal;

    /*--- Deviation alarm ---*/
    param.bal.devAlmPct = sysCfg[unit].MvcSettings.balance.devAlmPct.actVal;    /* Deviation alarm */
    param.bal.devAlmRetry = sysCfg[unit].MvcSettings.balance.devAlmRetry.actVal;/* Deviation retry */
    /* Deviation alarm for band 4 */
    param.bal.devAlmPctB4  = sysCfg[unit].MvcSettings.balance.devAlmPctB4.actVal;

    /* Deviation retry for band 4 */
    param.bal.devAlmRetryB4 = sysCfg[unit].MvcSettings.balance.devAlmRetryB4.actVal;

    /*--- Quick response deviation alarm ---*/
    param.bal.devAlmPctQR = sysCfg[unit].MvcSettings.balance.devAlmPctQR.actVal;/* 90 % */

    /* retry 3x */
    param.bal.devAlmRetryQR = sysCfg[unit].MvcSettings.balance.devAlmRetryQR.actVal;

    /* Band 2 */
    param.bal.devAlmBandNrQR = sysCfg[unit].MvcSettings.balance.devAlmBandNrQR.actVal;

    /*----- Deviation band settings -----*/
    for (i=0; i<=4; i++) {
      param.bal.dev[i].corDev = sysCfg[unit].MvcSettings.balance.Bands[i].corDev.actVal;
      param.bal.dev[i].corFac1 = sysCfg[unit].MvcSettings.balance.Bands[i].corFac1.actVal;
      param.bal.dev[i].corFac2 = sysCfg[unit].MvcSettings.balance.Bands[i].corFac2.actVal;
      param.bal.dev[i].corN1 = sysCfg[unit].MvcSettings.balance.Bands[i].corN1.actVal;
      param.bal.dev[i].corN2 = sysCfg[unit].MvcSettings.balance.Bands[i].corN2.actVal;
      param.bal.dev[i].corMax = sysCfg[unit].MvcSettings.balance.Bands[i].corMax.actVal;
    }

    /*--- Regrind parameters ---*/
    param.regrind.levelHigh = sysCfg[unit].regrindParm.levelHigh;
    param.regrind.levelRegrind = sysCfg[unit].regrindParm.levelRegrind;
    param.regrind.levelPct[0] = sysCfg[unit].regrindParm.levelPct[0];
    param.regrind.levelPct[1] = sysCfg[unit].regrindParm.levelPct[1];
    param.regrind.levelPct[2] = sysCfg[unit].regrindParm.levelPct[2];
    param.regrind.levelPct[3] = sysCfg[unit].regrindParm.levelPct[3];
    param.regrind.levelPct[4] = sysCfg[unit].regrindParm.levelPct[4];
    param.regrind.regrindPctExtra = sysCfg[unit].regrindParm.regrindPctExtra;
    param.regrind.regPctB1 = sysCfg[unit].regrindParm.regPctB1;
    param.regrind.fillTimeMax = sysCfg[unit].regrindParm.fillTimeMax;
    param.regrind.fillInterval = sysCfg[unit].regrindParm.fillInterval;


    /*----- Imperial mode conversion -----*/
    if (Rout::ImperialMode()) {
      msg.data[unit].prodSettings.extCap = Rout::LbsToKg(msg.data[unit].prodSettings.extCap);
      msg.data[unit].prodSettings.shotWeight = Rout::LbsToGr(msg.data[unit].prodSettings.shotWeight);
      param.levels.hopperEmptyWeight = Rout::LbsToGr(param.levels.hopperEmptyWeight);
      param.levels.hopperSensorFill = Rout::LbsToGr(param.levels.hopperSensorFill);
      param.levels.hopperHiLevel = Rout::LbsToGr(param.levels.hopperHiLevel);
      param.levels.hopperHiHiLevel = Rout::LbsToGr(param.levels.hopperHiHiLevel);
      param.regrind.levelHigh = Rout::LbsToGr(param.regrind.levelHigh);
      param.regrind.levelRegrind = Rout::LbsToGr(param.regrind.levelRegrind);
    }

    /*----- Copy param data to message -----*/
    memcpy(&(msg.data[unit].mcTcControlParm),&param,sizeof(tMcTcControlParm));

    /*----- Consumption reset flags -----*/
    msg.data[unit].commands.consReset = Rout::rstCons[unit].total;
    msg.data[unit].commands.consResetDay = Rout::rstCons[unit].batch;

  }

  /*----- Update buzzer -----*/
  msg.common.buzzerCmd = mstSts.buzzerCtrl;

  /*----- Clear buzzerCtrl after every com cycle -----*/
  mstSts.buzzerCtrl = buzNone;

  /*----- RTC update flag -----*/
  msg.common.rtcUpdate = 0x00;
  if (mstCfg.dateSet) {
    mstCfg.dateSet = false;
    msg.common.rtcUpdate = 0x01;
  }

  /*----- Setup number of slaves (Adjust to non-zero) -----*/
  msg.common.slavesConnected = mstCfg.slaveCount+1;

  /*----- CANBUS watchdog disable -----*/
  msg.common.canWdDis = mstCfg.canWdDis;

  /*----- Profibus configuration -----*/
  msg.common.profibusCfg.profibusEnb = mstCfg.profibusEnb;
  msg.common.profibusCfg.deviceAddress = mstCfg.modbusAddr;

#ifdef UIT
  /*----- Setup TWIN common production data -----*/
  //  if (mstSts.isTwin) {
#warning Dit later aanpassen naar multiple units !!!!!!!!!!!!!!!!!!!!!!!!!!

  if (mstCfg.slaveCount > 0) {
    sysCfg[1].injection = sysCfg[0].injection;
#warning probleem bij offline calibratie.... @@@ JB
    if (!Rout::McWeightPresent()){                                            //tijdelijke oplossing
      sysCfg[1].extrusion = sysCfg[0].extrusion;
    }
  }
  else {
    /*----- Single unit mode, reset message data from unit 1 -----*/
    memset(&msg.data[1],0,sizeof(msg.data[1]));
  }
#endif

  //  #warning Verplaatsen naar Common data !!!
  //  /*----- Copy unit 0 alam config to unit 1 -----*/
  //  msg.data[1].mcTcControlParm.alm = msg.data[0].mcTcControlParm.alm;

  /*----- Send message -----*/
  com->SendMsg(msg);
}


void global::ComMsgReceived(tMsgControl msg)
{
  /*------------------------------------------------------------------------------*/
  /* ComMsgReceived is trigger from COM.CPP on reception of an data message from  */
  /* the control task.                                                            */
  /* The data contains information of all the units.                              */
  /*------------------------------------------------------------------------------*/

  eCalStatus old;
  int unit  ;                                                                   /* Unit loop counter */
  int idx;                                                                      /* Actual selected unit */

  /*----- Get active selected unit -----*/
  idx = ActUnitIndex();

  /*----------------------------------*/
  /* Handling for all connected units */
  /*----------------------------------*/

  /*----- Loop for all connected units -----*/
  for (unit=0; unit <= mstCfg.slaveCount; unit++) {

    /*----- Imperial mode conversion -----*/
    if (Rout::ImperialMode()) {
      msg.data[unit].extCapAct = Rout::KgToLbs(msg.data[unit].extCapAct);
      msg.data[unit].colCapIst = Rout::GrSToLbsH(msg.data[unit].colCapIst);
      msg.data[unit].mcWeightData.extCap = Rout::KgToLbs(msg.data[unit].mcWeightData.extCap);
      msg.data[unit].mcWeightData.extCapTotal = Rout::KgToLbs(msg.data[unit].mcWeightData.extCapTotal);
      msg.data[unit].dosSet = Rout::GrToLbs(msg.data[unit].dosSet);
      msg.data[unit].dosAct = Rout::GrToLbs(msg.data[unit].dosAct);
      msg.data[unit].consumption = Rout::KgToLbs(msg.data[unit].consumption);
      msg.data[unit].weight = Rout::GrToLbs(msg.data[unit].weight);
    }

    SysCfg_SetDosSet(msg.data[unit].dosSet,unit);
    SysSts_SetDosAct(msg.data[unit].dosAct,unit);
    SysSts_SetRpm(msg.data[unit].actRpm,unit);
    SysSts_SetMass(msg.data[unit].weight,unit);

    /*----- balance calibrated 0 = false, 1 = true -----*/
    SysSts_SetLearnSts((msg.data[unit].balProdStatus == 0x00 ? false : true),unit);

    /*----- Current system status -----*/
    // Increment by 1, Internally in Qt stsNone = 0 and stsOff = 1. In hardware stsNone does not exist, and stsOff = 0.
    SysSts_SetMCStatus(((eMCStatus)(msg.data[unit].vidProdSts+1)),msg.data[unit].actIsSet,unit);

    /*----- Current precalibration status -----*/
    unsigned char i=0;
    if (msg.data[unit].preCalBusy != 0x00) i += 1;                              // preCal busy
    if (msg.data[unit].preCalReady != 0x00) i += 2;                             // preCal ready
    if (msg.data[unit].preCalPauseBusy != 0x00) i += 4;                         // preCal pauseBusy
    if (msg.data[unit].preCalIstValid != 0x00) i += 8;                          // preCal corFacValid
    SetPreCalStatus(i,unit);

    /*----- RpmCorFac -----*/
    sysSts[unit].preCalibration.rpmCorrFac = msg.data[unit].rpmCorr;

    StsSetPreCalDosAct(msg.data[unit].preCalCalCapIst,unit);
    SysSts_SetProdToGo(msg.data[unit].productsToGo,unit);
    SysSts_SetShotTime(msg.data[unit].timeToGo,unit);
    SysSts_SetFastMass(msg.data[unit].grossWeightFast,unit);
    SysSts_SetTacho(msg.data[unit].tachoAct,unit);
    SysSts_SetExtCap(msg.data[unit].extCapAct,unit);
    SysSts_SetMeteringAct(msg.data[unit].metTimeAct,unit);
    SysSts_SetMcWeightCap(msg.data[unit].mcWeightData.extCap,msg.data[unit].mcWeightData.extCapTotal,msg.data[unit].mcWeightData.colorPctTotal,unit);

    /*----- Alarm data -----*/
    SysSts_SetAlarmMode(msg.data[unit].alarmMode,unit);                         // AlarmMode
    SysSts_SetAlarmNr(msg.data[unit].alarmNr,unit);                             // AlarmNr

    /* FIRST read ALL alarm data, before settings the alarm active y/n */
    SysSts_SetAlarmActive((msg.data[unit].almActive == 1 ? true : false),unit); // almActive
    SysSts_SetMeteringValid((msg.data[unit].metTimeActValid == 1 ? true : false),unit);

    SysSts_SetSysStartAck(msg.data[unit].mcCmd,unit);
    SysSts_SetSysActive((msg.data[unit].mc24On == true ? true : false),unit);   // act. system status (balance) ON/OFF

    if (msg.data[unit].mcStartError != 0x00) {
      /*----- Start condition error (for balance) -----*/
      //@@ Moet nog aangepast worden naar aparte functie of via alarm handler.
      /*----- clear: start/execCmd/cmd/ack -----*/
      MstSts_SetSysStart(false);
      sysSts[unit].sysActive.execCmd = false;
      sysSts[unit].sysActive.cmd = 0x00;
      sysSts[unit].sysActive.ack = 0x00;

      /*----- Display message according to the reason for abort -----*/
      QString txt;

      switch(msg.data[unit].mcStartError) {
      case 0:
        break;
      case 1:
        if (menu != 0) {
          ((MainMenu*)(menu))->alarmHndlr->GetMessageText(TXT_INDEX_PROD_SETTINGS_ERROR,0,0,&txt);
          ((MainMenu*)(menu))->DisplayMessage(unit,0,txt,(int)msgtAccept);
        }
        break;

      case 2:
        if (menu != 0) {
          ((MainMenu*)(menu))->alarmHndlr->GetAlarmText(ALM_INDEX_LOADCELL_CON,0,&txt,true);
          ((MainMenu*)(menu))->DisplayMessage(unit,0,txt,(int)msgtAccept);
        }
        break;

      default:
        ((MainMenu*)(menu))->DisplayMessage(unit,0,"unknown alarm",(int)msgtAccept);
      }
    }

    if (msg.data[unit].alarmResetAck != 0x00) emit AlarmResetAckRcv(unit);
    SysSts_SetPrimeStatus(((msg.data[unit].primeBusy) == 0x00 ? false : true),unit);

    //    /*----- Update actual equals set value mode -----*/
    //    sysSts[unit].actIsSet = (msg.data[unit].actIsSet != 0);

    SetSystemStatus(unit);

    /* Process diagnostics data */
    sysSts[unit].procDiagData.metRstHystCnt = msg.data[unit].metRstHystCnt;
    sysSts[unit].procDiagData.bandNr = msg.data[unit].bandNr;
    sysSts[unit].procDiagData.inBand = msg.data[unit].inBand;
    sysSts[unit].procDiagData.outBand = msg.data[unit].outBand;
    sysSts[unit].procDiagData.bandCnt = msg.data[unit].bandCnt;
    sysSts[unit].procDiagData.corSts = msg.data[unit].corSts;
    sysSts[unit].procDiagData.mcSts = msg.data[unit].mcSts;
    sysSts[unit].procDiagData.putIst = msg.data[unit].putIst;
    sysSts[unit].procDiagData.getIst = msg.data[unit].getIst;
    sysSts[unit].procDiagData.mcStsQR = msg.data[unit].mcStsQR;
    sysSts[unit].procDiagData.putIstQR = msg.data[unit].putIstQR;
    sysSts[unit].procDiagData.getIstQR = msg.data[unit].getIstQR;

    sysSts[unit].uBoardVersion.SoftwareVersion = msg.data[unit].versionSw;
    sysSts[unit].uBoardVersion.HardwareVersion = msg.data[unit].versionHw;

    sysSts[unit].procDiagData.metTimeAct = msg.data[unit].metTimeAct;
    sysSts[unit].procDiagData.metTimeMeas = msg.data[unit].metTimeMeas;
    sysSts[unit].procDiagData.metTimeCalc = msg.data[unit].metTimeCalc;
    sysSts[unit].procDiagData.colCapIst = msg.data[unit].colCapIst;
    sysSts[unit].procDiagData.vidColDevPct = msg.data[unit].vidColDevPct;
    sysSts[unit].procDiagData.colDevPctRel = msg.data[unit].colDevPctRel;
    sysSts[unit].procDiagData.corCycle = msg.data[unit].corCycle;

    sysSts[unit].procDiagData.vidCorrection = msg.data[unit].vidCorrection;
    sysSts[unit].procDiagData.rpmCorr = msg.data[unit].rpmCorr;
    sysSts[unit].procDiagData.corFacAdjust = msg.data[unit].corFacAdjust;
    sysSts[unit].procDiagData.minTimeCorFac = msg.data[unit].minTimeCorFac;
    sysSts[unit].procDiagData.sampleLen = msg.data[unit].sampleLen;
    sysSts[unit].procDiagData.timeLeft = msg.data[unit].timeLeft;
    sysSts[unit].procDiagData.sampleLenQR = msg.data[unit].sampleLenQR;
    sysSts[unit].procDiagData.timeLeftQR = msg.data[unit].timeLeftQR;

    /*----- Read actual I/O status -----*/
    sysSts[unit].actIO.inputs[0] = ((msg.data[unit].ioData.input[0]) == 0x00 ? false : true);
    sysSts[unit].actIO.inputs[1] = ((msg.data[unit].ioData.input[1]) == 0x00 ? false : true);
    sysSts[unit].actIO.inputs[2] = ((msg.data[unit].ioData.input[2]) == 0x00 ? false : true);
    sysSts[unit].actIO.inputs[3] = ((msg.data[unit].ioData.input[3]) == 0x00 ? false : true);

    sysSts[unit].actIO.outputs[0] = ((msg.data[unit].ioData.output[0]) == 0x00 ? false : true);
    sysSts[unit].actIO.outputs[1] = ((msg.data[unit].ioData.output[1]) == 0x00 ? false : true);
    sysSts[unit].actIO.outputs[2] = ((msg.data[unit].ioData.output[2]) == 0x00 ? false : true);
    sysSts[unit].actIO.outputs[3] = ((msg.data[unit].ioData.output[3]) == 0x00 ? false : true);

    /*----- Consumption -----*/
    sysSts[unit].consumption = msg.data[unit].consumption;
    sysSts[unit].consumptionDay = msg.data[unit].consumptionDay;

    /*----- Consumption reset handshake, reset the consumption reset flags on consumption is zero -----*/
    if (msg.data[unit].consumption == 0) {
      Rout::rstCons[unit].total = false;
    }
    if (msg.data[unit].consumptionDay == 0) {
      Rout::rstCons[unit].batch = false;
    }

    /*----- Detect new shot for MC-LAN update, only when in single unit mode -----*/
    if (unit == 0) {
      if (msg.data[unit].newShotDetect) {
        SigSendUdpData();
      }
    }

    if (glob->mstCfg.profibusEnb) {
      /*----- Tacho max -----*/
      glob->sysCfg[idx].extrusion.tachoMax = msg.common.profiBusCommonCmd.tachoSet;
      glob->sysCfg[idx].extrusion.extCapacity = msg.common.profiBusCommonCmd.extCapKgH;
      SysCfg_SetColPct(msg.data[unit].profibusUnitCmd.colPct,unit);
    }
  }

  /*---------------------------------*/
  /* Handling for selected unit only */
  /*---------------------------------*/

  /*----- Loadcell calibration -----*/
  calStruct.status = msg.data[idx].calib;
  old = calStruct.sts;

  /*----- Calibration busy -----*/
  if ((calStruct.status & 0x01) != 0) {
    if (this->calStruct.cmd == calRefPlaced) {
      calStruct.sts = calsBusy2;
    }

    if (calStruct.sts != calsBusy2) {
      calStruct.sts = calsBusy;
    }

    /*----- Wait for reference weight -----*/
    if ((calStruct.status & 0x02) != 0) {
      calStruct.sts = calsWaitForRef;
    }

    if ((calStruct.status & 0x08) != 0) {
      calStruct.sts = calsReady;
    }
  }

  else {
    /*----- Calibration finished -----*/
    if ((calStruct.status & 0x08) != 0) {
      calStruct.sts = calsReady;
    }
    else {
      calStruct.sts = calsIdle;
    }
  }

  if ((calStruct.status & 0x04) != 0) {
    /*----- Calibration error handling implementeren. -----*/
    calStruct.sts = calsErr;
  }

  /*----- Detect change in calibration state -----*/
  if (calStruct.sts != old) {
    emit this->CalStatus(calStruct.sts);
  }

  /*----- Common Profibus data handling-----*/
  if (glob->mstCfg.profibusEnb) {
    /*----- Start / stop -----*/
    glob->MstSts_SetSysStart(msg.common.profiBusCommonCmd.startCmd);

    /*----- Reset alarms -----*/

    // Detect change in alarm reset status
    if (prevProfiResetAlarm != msg.common.profiBusCommonCmd.resetAlarm) {
      prevProfiResetAlarm = msg.common.profiBusCommonCmd.resetAlarm;
      if (msg.common.profiBusCommonCmd.resetAlarm) {
        Rout::ResetAlarms();
      }
    }
  }

  /*----------------------------------------------------------------------------------------------*/
  /* Emit ComReceived tMsgGuiUnit,idx for unit 0 and unit 1 in twin mode else for the active unit */
  /*----------------------------------------------------------------------------------------------*/
  if (mstSts.isTwin == FALSE) {
    emit ComReceived(msg.data[idx],idx);                                        /* Emit for actvie unit only */
  }
  else {
    emit ComReceived(msg.data[0],0);                                            /* Emit for Unit 0 */
    emit ComReceived(msg.data[1],1);                                            /* Emit fot Unit 1 */
  }

}


// Not used anymore. Units are read from the formats structures of global
void global::SetCorrectUnits()
{
  {
    /*
    if (isTwin)
    {

    }
    else
    */
    switch (mstCfg.mode) {
    case modInject:
      switch(sysCfg[ActUnitIndex()].GraviRpm) {
      case
      grGravi:
        switch(mstCfg.inpType) {
        case ttTimer:                                                     // GRAVI / INJ / TIMER
          MstCfg_SetTimeUnits(UNITS_SEC);                                 // Timer
          if (!Rout::ImperialMode()) {
            MstCfg_SetShotUnits(UNITS_MET_G_S);                           // dos/act, dos/set
            MstCfg_SetWeightUnits(UNITS_MET_G);                           // ShotWeight
          }
          else {
            MstCfg_SetShotUnits(UNITS_IMP_G_S);                           // dos/act, dos/set
            MstCfg_SetWeightUnits(UNITS_IMP_G);                           // ShotWeight
          }

          break;
        case ttRelay:                                                     // GRAVI / INJ / RELAY
          MstCfg_SetTimeUnits(UNITS_SEC);                                 // Timer
          if (!Rout::ImperialMode()) {
            MstCfg_SetShotUnits(UNITS_MET_G_S);                           // dos/act, dos/set
            MstCfg_SetWeightUnits(UNITS_MET_G);                           // ShotWeight
          }
          else {
            MstCfg_SetShotUnits(UNITS_IMP_G_S);                           // dos/act, dos/set
            MstCfg_SetWeightUnits(UNITS_IMP_G);                           // ShotWeight

          }
          break;
        default:
          break;
        }
        break;

      case grRpm:                                                           // RPM / INJ
        // Geen keuzes mogelijk
        // Misschien MC_ID in de toekomst?
        break;

      default:
        break;
      }
      break;

    case modExtrude:
      switch(sysCfg[ActUnitIndex()].GraviRpm) {
      case grGravi:
        switch(mstCfg.inpType) {
        case ttRelay:                                                     // GRAVI / EXT / RELAY
          MstCfg_SetTimeUnits(UNITS_SEC);                                 // ???
          if (!Rout::ImperialMode()) {
            MstCfg_SetShotUnits(UNITS_MET_G_S);                           // dos/act, dos/set
            MstCfg_SetExtUnits(UNITS_MET_KG_H);                           // Extr. capacity
          }
          else {
            MstCfg_SetShotUnits(UNITS_IMP_G_S);                           // dos/act, dos/set
            MstCfg_SetExtUnits(UNITS_IMP_KG_H);                           // Extr. capacity
          }
          break;
        case ttTacho:                                                     // GRAVI / EXT / TACHO
          MstCfg_SetTachoUnits(UNITS_VOLT);                               // Tacho
          if (!Rout::ImperialMode()) {
            MstCfg_SetShotUnits(UNITS_MET_G_S);                           // dos/act, dos/set
            MstCfg_SetExtUnits(UNITS_MET_KG_H);                           // Extr. capacity
          }
          else {
            MstCfg_SetShotUnits(UNITS_IMP_G_S);                           // dos/act, dos/set
            MstCfg_SetExtUnits(UNITS_IMP_KG_H);                           // Extr. capacity
          }
          break;
        default:
          break;
        }
        break;

      case grRpm:
        switch(mstCfg.inpType) {
        case ttRelay:                                                     // RPM / EXT / RELAY
          MstCfg_SetTimeUnits(UNITS_SEC);
          if (!Rout::ImperialMode()) {
            MstCfg_SetShotUnits(UNITS_MET_G_S);
          }
          else {
            MstCfg_SetShotUnits(UNITS_IMP_G_S);
          }
          break;
        case ttTacho:                                                     // RPM / EXT / TACHO
          MstCfg_SetTachoUnits(UNITS_VOLT);
          if (!Rout::ImperialMode()) {
            MstCfg_SetShotUnits(UNITS_MET_G_S);
          }
          else {
            MstCfg_SetShotUnits(UNITS_IMP_G_S);
          }
          break;
        default:
          break;
        }
        break;

      default:
        break;
      }
      break;

    default:
      break;
    }
  }
}


void global::CalSendCommand(eCalCommand cmd)
{
  if (calStruct.cmd != cmd) {
    calStruct.cmd = cmd;
  }
}


void global::PreCalSendCommand(eCalCommand cmd, int idx)
{
  if (sysSts[idx].preCalibration.calib.cmd != cmd) {
    sysSts[idx].preCalibration.calib.cmd = cmd;
  }
}


int global::ActUnitIndex()
{
  /*--------------------------------*/
  /* Return the actual display unit */
  /*--------------------------------*/
  return mstSts.actUnitIndex;
}


void global::SigSendUdpData(void)
{
  /*-----------------------------------------------------------*/
  /* Emit signal SigSendUdp                                    */
  /* This signal is triggers SendUdpData to send data to McLan */
  /*-----------------------------------------------------------*/
  emit SigSendUdp();
}


void global::SignalSaveConfigToFile(int idx)
{
  /*-------------------------------------------------------------------------------------*/
  /* Emit the signal SysCfg_SaveConfigToFile                                             */
  /* This signal triggers the function which saves the configuration of unit idx to file */
  /*-------------------------------------------------------------------------------------*/
  emit SysCfg_SaveConfigToFile(idx);
}


void global::SetMcwDefaults(eMcwtype mcwType, int idx)
{
  /*-----------------------------------------------------*/
  /* Setup the defaults values the given McWeight system */
  /*-----------------------------------------------------*/

  /*----- Type specific settings -----*/
  switch(mcwType) {

  case(mcw100) :
    glob->sysCfg[idx].fillLevels.LoLo = 1000;
    glob->sysCfg[idx].fillLevels.Lo   = 2000;
    glob->sysCfg[idx].fillLevels.Hi   = 6000;
    glob->sysCfg[idx].fillLevels.HiHi = 8000;
    glob->sysCfg[idx].MvcSettings.balance.measureMaxTime.actVal =60;
    glob->sysCfg[idx].MvcSettings.balance.devAlmRetryQR.actVal = 10;
    glob->mstCfg.shortMeasTime = 10;
    glob->mstCfg.hysterese = 4;
    break;

  case(mcw500) :
    glob->sysCfg[idx].fillLevels.LoLo = 2000;
    glob->sysCfg[idx].fillLevels.Lo   = 5000;
    glob->sysCfg[idx].fillLevels.Hi   = 15000;
    glob->sysCfg[idx].fillLevels.HiHi = 20000;
    glob->sysCfg[idx].MvcSettings.balance.measureMaxTime.actVal = 45;
    glob->sysCfg[idx].MvcSettings.balance.devAlmRetryQR.actVal = 7;
    glob->mstCfg.shortMeasTime = 7;
    glob->mstCfg.hysterese = 3;
    break;

  case(mcw1000) :
    glob->sysCfg[idx].fillLevels.LoLo = 3000;
    glob->sysCfg[idx].fillLevels.Lo   = 7000;
    glob->sysCfg[idx].fillLevels.Hi   = 23000;
    glob->sysCfg[idx].fillLevels.HiHi = 40000;
    glob->sysCfg[idx].MvcSettings.balance.measureMaxTime.actVal =30;
    glob->sysCfg[idx].MvcSettings.balance.devAlmRetryQR.actVal = 5;
    glob->mstCfg.shortMeasTime = 5;
    glob->mstCfg.hysterese = 2;
    break;
  }

  /*----- Common settings for all McWeight types -----*/
  glob->sysCfg[idx].fillSystem.alarmTime = 180;
  glob->sysCfg[idx].fillSystem.alarmMode = true;
  glob->sysCfg[idx].fillSystem.knifeGateValve = true;
  glob->sysCfg[idx].dosTool = dtA50;
  glob->sysCfg[idx].MvcSettings.agent.fullScale.actVal = 100000;
  glob->sysCfg[idx].MvcSettings.balance.measureBand4MinTime.actVal = 30;
  glob->sysCfg[idx].MvcSettings.balance.measureMinTime.actVal = 15;
  glob->sysCfg[idx].MvcSettings.balance.fillStart.actVal = 10;
  glob->sysCfg[idx].MvcSettings.balance.fillTime.actVal = 1;
  glob->sysCfg[idx].MvcSettings.balance.lidOffWeight.actVal = 2000;
  glob->sysCfg[idx].MvcSettings.balance.lidOffTime.actVal = 1;
  glob->sysCfg[idx].MvcSettings.balance.devAlmPctQR.actVal = 25;
  glob->sysCfg[idx].MvcSettings.balance.devAlmBandNrQR.actVal = 0;
  glob->sysCfg[idx].MvcSettings.weight.motionDelay.actVal = 5;
  glob->sysCfg[idx].MvcSettings.weight.weightFilter.actVal = 128;

  glob->mstCfg.tachoRatio = 0;

  /*----- Save settings to file -----*/
  emit MstCfg_SaveConfigToFile();
  emit SysCfg_SaveConfigToFile(idx);
}


void global::SetBalHODefaults(eBalHOtype balHOtype, int idx)
{
  /*-------------------------------------------------------*/
  /* Setup the defaults values the given Balance HO system */
  /*-------------------------------------------------------*/

  /*----- Type specific settings -----*/
  switch(balHOtype) {

  case(balHO100) :
    glob->sysCfg[idx].fillLevels.LoLo = 1000;
    glob->sysCfg[idx].fillLevels.Lo   = 2000;
    glob->sysCfg[idx].fillLevels.Hi   = 6000;
    glob->sysCfg[idx].fillLevels.HiHi = 8000;
    glob->sysCfg[idx].MvcSettings.balance.devAlmRetryQR.actVal = 10;
    glob->mstCfg.shortMeasTime = 10;
    glob->mstCfg.hysterese = 4;
    break;

  case(balHO500) :
    glob->sysCfg[idx].fillLevels.LoLo = 2000;
    glob->sysCfg[idx].fillLevels.Lo   = 5000;
    glob->sysCfg[idx].fillLevels.Hi   = 15000;
    glob->sysCfg[idx].fillLevels.HiHi = 20000;
    glob->sysCfg[idx].MvcSettings.balance.measureMaxTime.actVal = 45;
    glob->sysCfg[idx].MvcSettings.balance.devAlmRetryQR.actVal = 7;
    glob->mstCfg.shortMeasTime = 7;
    glob->mstCfg.hysterese = 3;
    break;

  case(balHO1000) :
    glob->sysCfg[idx].fillLevels.LoLo = 3000;
    glob->sysCfg[idx].fillLevels.Lo   = 7000;
    glob->sysCfg[idx].fillLevels.Hi   = 23000;
    glob->sysCfg[idx].fillLevels.HiHi = 40000;
    glob->sysCfg[idx].MvcSettings.balance.measureMaxTime.actVal = 30;
    glob->sysCfg[idx].MvcSettings.balance.devAlmRetryQR.actVal = 5;
    glob->mstCfg.shortMeasTime = 5;
    glob->mstCfg.hysterese = 2;
    break;
  }

  /*----- Common settings for all Balance HO types -----*/
  glob->sysCfg[idx].fillSystem.alarmTime = 180;
  glob->sysCfg[idx].fillSystem.alarmMode = true;
  glob->sysCfg[idx].fillSystem.knifeGateValve = true;
  glob->sysCfg[idx].dosTool = dtA50;
  glob->sysCfg[idx].MvcSettings.agent.fullScale.actVal = 100000;
  glob->sysCfg[idx].MvcSettings.agent.maxMotorSpeed.actVal = 350;
  glob->sysCfg[idx].MvcSettings.balance.measureBand4MinTime.actVal = 45;
  glob->sysCfg[idx].MvcSettings.balance.measureMinTime.actVal = 90;
  glob->sysCfg[idx].MvcSettings.balance.fillStart.actVal = 10;
  glob->sysCfg[idx].MvcSettings.balance.fillTime.actVal = 1;
  glob->sysCfg[idx].MvcSettings.balance.lidOffWeight.actVal = 2000;
  glob->sysCfg[idx].MvcSettings.balance.lidOffTime.actVal = 1;
  glob->sysCfg[idx].MvcSettings.balance.devAlmPctQR.actVal = 25;
  glob->sysCfg[idx].MvcSettings.balance.devAlmBandNrQR.actVal = 0;
  glob->sysCfg[idx].MvcSettings.weight.motionDelay.actVal = 5;
  glob->sysCfg[idx].MvcSettings.weight.weightFilter.actVal = 128;
  glob->sysCfg[idx].motor = mtHT;

  glob->mstCfg.tachoRatio = 0;

  /*----- Save settings to file -----*/
  emit MstCfg_SaveConfigToFile();
  emit SysCfg_SaveConfigToFile(idx);
}


#include "Form_FileSelect.h"
#include "ui_Form_FileSelect.h"
#include <QDir>
#include "Form_MainMenu.h"
#include "Rout.h"

#define FILE_DISPLAY_BOX_ITEMS 5


//QString searchString;                                   /* Recipe list search filter */


FileSelectFrm::FileSelectFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::FileSelectFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  doSearch = true;
  doNew = true;
  doDelete = true;
  doRename = true;
  doAccept = true;
  forceAccept = false;

  currentIndex = -1;
  currentItem = "";
  curScroll = -1;
  maxScroll = -1;

  msgIdx = -1;

  keyInput = 0;
  ScrollBox = 0;

  keyInput = new KeyboardFrm();
  keyInput->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
  keyInput->SetMaxLength(MAX_LENGTH_FILENAME_INPUT);
  connect(keyInput,SIGNAL(inputText(QString)),this,SLOT(TextReceived(QString)));
  keyInput->hide();

  ui->setupUi(this);
  fileExt = "*";
  path = "/";

  ScrollBox = new QLabel(this);
  ScrollBox->setAutoFillBackground(true);
  ScrollBox->setStyleSheet("color: rgb(0,0,0); background-color: rgb(150,150,150)");
  ScrollBox->move((ui->lbScrollBar->pos().x()+1),(ui->lbScrollBar->pos().y()+1));
  ScrollBox->setVisible(true);

  fileList.clear();

  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(ui->btDown,SIGNAL(clicked()),this,SLOT(ScrollDown()));
  connect(ui->btUp,SIGNAL(clicked()),this,SLOT(ScrollUp()));

  connect(ui->btNew,SIGNAL(clicked()),this,SLOT(NewClicked()));
  connect(ui->btSearch,SIGNAL(clicked()),this,SLOT(SearchClicked()));
  connect(ui->btDelete,SIGNAL(clicked()),this,SLOT(DeleteClicked()));
  connect(ui->btDeleteAll,SIGNAL(clicked()),this,SLOT(DeleteAllClicked()));
  connect(ui->btFileRename,SIGNAL(clicked()),this,SLOT(RenameClicked()));

  searchString = "";

}


FileSelectFrm::~FileSelectFrm()
{
  delete keyInput;
  delete ui;
}


void FileSelectFrm::resizeEvent(QResizeEvent *e)
{
  QWidget::resizeEvent(e);
  if (ScrollBox != 0) {
    ScrollBox->setFixedSize((ui->lbScrollBar->width()-2),33);
    ScrollBox->move((ui->lbScrollBar->pos().x()+1),(ui->lbScrollBar->pos().y()+1));
    UpdateScrollBox();
  }
}


void FileSelectFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  // Set all button images:
  glob->SetButtonImage(ui->btOk,itSelection,selOk);
  //glob->SetButtonImage(ui->btCancel,itSelection,selCancel);
  glob->SetButtonImage(ui->btUp,itGeneral,genArrowKeyUp);
  glob->SetButtonImage(ui->btDown,itGeneral,genArrowKeyDown);
  glob->SetButtonImage(ui->btFileRename,itFileHandle,fhRename);
  glob->SetButtonImage(ui->btNew,itFileHandle,fhNew);
  glob->SetButtonImage(ui->btDelete,itFileHandle,fhDelete);
  glob->SetButtonImage(ui->btDeleteAll,itFileHandle,fhDeleteAll);
  glob->SetButtonImage(ui->btSearch,itFileHandle,fhSearch);

  ui->boxSelect->setFont(*(glob->baseFont));
  ui->lbIndex->setFont(*(glob->baseFont));

  if (keyInput != 0) {
    keyInput->FormInit();
  }

  connect(glob,SIGNAL(MstCfg_UserChanged(eUserType)),this,SLOT(CfgCurrentUserChanged(eUserType)));
  connect(glob,SIGNAL(MstSts_SysActiveChanged(bool)),this,SLOT(SysActiveChanged(bool)));
}


void FileSelectFrm::SetFilePath(const QString &dir)
{
  if (!(dir.isNull())) {
    QDir someDir;
    someDir.setPath(dir);
    if (someDir.exists()) {
      path = dir;
    }
  }
}


void FileSelectFrm::SetFileExt(const QString &ext)
{
  fileExt = ext;
  UpdateDisplay();
}


void FileSelectFrm::LoadFiles()
{
  QString fileFilter;
  QDir fileDir;

 /*----- Setup file filter -----*/
  if (searchString.isEmpty()) {
    fileFilter = "*" + fileExt;
  }
  else {
    fileFilter = "*" + searchString + "*" + fileExt;
  }

  fileList.clear();

  if (!path.isNull()) {
    if (fileDir.exists(path)) {
      fileDir.setPath(path);
      fileList = fileDir.entryList(QStringList(fileFilter),QDir::Files | QDir::NoSymLinks);
    }

    // Sort the list on alphabetical order (case-sensitive)
    //        fileList.sort();

    // Alternative to order case-insensitively
    QMap<QString,QString> strMap;
    // Add all items to the QMap, using the strings from fileList.
    foreach(QString str, fileList) {
      // Insert the string as lowercase into the QMap and use this lowercase value as index.
      // The 2nd String is the normal string as value.
      strMap.insert(str.toLower(),str);
    }
    // Finally load the sorted original strings into our fileList variable for further use.
    fileList = strMap.values();

    UpdateDisplay();
  }
}


void FileSelectFrm::UpdateScrollBox()
{
  if (ScrollBox != 0) {
    int x = ui->lbScrollBar->pos().x()+1;
    int top = ui->lbScrollBar->pos().y()+1;

    // bereken scrollBox verschuiving per item
    int scrollCount = 0;

    if (ItemCount() > 0) {
      scrollCount = (int)(ui->lbScrollBar->height() / ItemCount());
    }

    curScroll = scrollCount * currentIndex;

    int y = top + curScroll;
    int maxY = ui->lbScrollBar->height() + ui->lbScrollBar->pos().y();
    maxY -= (ScrollBox->height()+1);

    if (y > maxY) y = maxY;
    else {
      if (currentIndex == (ItemCount()-1)) {
        y = maxY;
      }
    }
    ScrollBox->move(x,y);
  }
}


int FileSelectFrm::ItemCount()
{
  return fileList.count();
}


void FileSelectFrm::ClipExtension(QString *file)
{
  if (file->length() > fileExt.length()) {
    *file = (*file).left((*file).length()-fileExt.length());
  }
}


void FileSelectFrm::UpdateCurrent(const QString &file,bool forceUpdate)
{
  if ((file != currentItem) || (forceUpdate)) {
    currentItem = file;
    currentIndex = fileList.indexOf(file);

    // If item is an invalid file
    if ((currentIndex < 0) && (ItemCount() > 0)) {
      // Select first item of the list
      UpdateCurrent(0);
    }
    UpdateDisplay();
  }
}


void FileSelectFrm::UpdateCurrent(int idx, bool forceUpdate)
{
  if ((currentIndex != idx) || (forceUpdate)) {
    if ((idx < ItemCount()) && (idx >= 0)) {
      currentIndex = idx;
      currentItem = (QString)(fileList.at(currentIndex));
    }
    else {
      currentIndex = -1;
      currentItem = QString("");
    }
    UpdateDisplay();
  }
}


void FileSelectFrm::ScrollUp()
{
  if (currentIndex > 0) {
    UpdateCurrent((currentIndex-1));
  }
}


void FileSelectFrm::ScrollDown()
{
  if (currentIndex < (fileList.count()-1)) {
    UpdateCurrent((currentIndex+1));
  }
}


void FileSelectFrm::DisplayUpBoxItems()
{
  QString upItems = "";
  QString item;
  int start;

  // Calculate range to display in the upper items box
  start = currentIndex - FILE_DISPLAY_BOX_ITEMS;
  if (start < 0) start = 0;

  for (int i = start; i<currentIndex;i++) {
    if ((i == start) && (i > 0)) {
      upItems = upItems + "...\n";
    }

    if (ItemCount() > i) {
      item = QString(fileList.at(i));                                           // Get the item as QString
      ClipExtension(&item);
      if (i != (currentIndex -1)) upItems = upItems + item + "\n";              // Add the item to the list with \n if more items follow
      else upItems = upItems + item;                                            // Add the item to the list without \n if it is the last item
    }
  }
  ui->boxUp->setText(upItems);
}


void FileSelectFrm::DisplayDownBoxItems()
{
  QString downItems = "";
  QString item;
  // Calculate range of items to display in the lower items box
  int eind = (currentIndex+1) + FILE_DISPLAY_BOX_ITEMS;
  if (eind > ItemCount()) eind = ItemCount();

  for (int i = (currentIndex+1); i<eind;i++) {
    item = QString(fileList.at(i));                                             // Get the item as QString
    ClipExtension(&item);
    if (i != (eind-1)) downItems = downItems + item + "\n";                     // Add item to the list with \n if more items follow
    else downItems = downItems + item;                                          // Add item to the list without \n if this is the last item

    if (((i == (eind-1)) && (eind != ItemCount()))) downItems = downItems + "\n...";
  }
  ui->boxDown->setText(downItems);
}


void FileSelectFrm::DisplaySelectedItem()
{
  QString item;

  if ((!currentItem.isEmpty()) && (!currentItem.isNull())) {
    item = currentItem;
    ClipExtension(&item);
    ui->lbIndex->setText(QString::number(currentIndex+1)+"/"+QString::number(ItemCount()));
  }
  else {
    item = "";
    ui->lbIndex->setText("-/"+QString::number(ItemCount()));
  }
  ui->boxSelect->setText(item);
}


void FileSelectFrm::UpdateDisplay()
{
  if (ItemCount() >= 0) {
    DisplayUpBoxItems();
    DisplayDownBoxItems();
    DisplaySelectedItem();
    UpdateScrollBox();
  }

  /*----- Display 'Delete all' button only from superVisor user level -----*/
  if (glob->GetUser() >= utSupervisor) {
    ui->btDeleteAll->setVisible(TRUE);
  }
  else {
    ui->btDeleteAll->setVisible(FALSE);
  }

}


void FileSelectFrm::CanSearch(bool _do)
{
  doSearch = _do;
  SetCorrectButtons();
}


void FileSelectFrm::CanNew(bool _do)
{
  doNew = _do;
  SetCorrectButtons();
}


void FileSelectFrm::CanRename(bool _do)
{
  doRename = _do;
  SetCorrectButtons();
}


void FileSelectFrm::CanDelete(bool _do)
{
  doDelete = _do;
  SetCorrectButtons();
}


void FileSelectFrm::CanAccept(bool _do)
{
  doAccept = _do;
  SetCorrectButtons();
}


void FileSelectFrm::ForceAccept(bool _do)
{
  forceAccept = _do;
  SetCorrectButtons();
}


void FileSelectFrm::SearchClicked()
{
  msgIdx = 3;

  /*----- Start text input form -----*/
  keyInput->SetText(searchString);
  keyInput->show();
}


void FileSelectFrm::NewClicked()
{
  emit NewFileClicked();
}


void FileSelectFrm::DeleteClicked()
{
  // Remove file from filesystem
  msgIdx = 1;
  if ((glob->menu) != 0) {
    //            if (isActiveWindow())
    QString item = currentItem;
    ClipExtension(&item);

    connect(((MainMenu*)(glob->menu)),SIGNAL(ErrorResult(int)),this,SLOT(MessageResult(int)));
    ((MainMenu*)(glob->menu))->DisplayMessage(glob->ActUnitIndex(),0,"'"+item+"' "+tr("will be erased.")+"\n"+tr("Do you want to continue?"),msgtConfirm);
  }
}


void FileSelectFrm::DeleteAllClicked()
{
  msgIdx = 4;
  if ((glob->menu) != 0) {

    QString item = currentItem;
    ClipExtension(&item);

    connect(((MainMenu*)(glob->menu)),SIGNAL(ErrorResult(int)),this,SLOT(MessageResult(int)));
    ((MainMenu*)(glob->menu))->DisplayMessage(glob->ActUnitIndex(),0,tr("All recipes as shown in the list, will be erased!")+"\n"+tr("Do you want to continue?"),msgtConfirm);
  }
}


void FileSelectFrm::RemoveCurrentItem()
{
  // Remove file from filesystem
  if (!(currentItem.isEmpty())) {
    QString fileName;
    QString dbg;

    fileName = path + glob->fileSeparator + currentItem;

    if (QFile::exists(fileName)) {
      if (QFile::remove(fileName)) {
        // file removed
        // Remove item from list
        fileList.removeAt(currentIndex);
        while ((currentIndex >= ItemCount()) && (currentIndex > 0)) {
          currentIndex--;
        }
        UpdateCurrent(currentIndex,true);
        // Refresh display
        UpdateDisplay();
      }
    }
  }
}

void FileSelectFrm::RemoveAllItems()
{
  /*-----------------------------------------*/
  /* Remove all recipe files from filesystem */
  /*-----------------------------------------*/
  QString fileName;
  int i;

  /*----- Load actual recipe file list -----*/
  LoadFiles();

  /*----- Setup recipe path -----*/
//  path = glob->AppPath() + QString(RECIPE_DEFAULT_FOLDER);

  /*----- Remove material files -----*/
  for (i=0; i<ItemCount(); i++) {
    fileName = path + glob->fileSeparator + (QString)(fileList.at(i));
    if (QFile::exists(fileName)) {
      QFile::remove(fileName);
    }
  }

  searchString = "";                          /* clear search string */

  /*----- Refresh display -----*/
  LoadFiles();
  UpdateDisplay();
  UpdateCurrent(0,true);
}



int FileSelectFrm::RenameCurrentItem(const QString &newName)
{
  if (!(currentItem.isEmpty())) {
    QString fileName;
    QString newFileName;
    QString newFile;
    QString dbg;

    newFile = newName + fileExt;
    fileName = path + glob->fileSeparator + currentItem;
    newFileName = path + glob->fileSeparator + newFile;
    if (QFile::exists(fileName)) {
      /*----- New filename already in use -----*/
      if (QFile::exists(newFileName)) {
        return 1;
      }
      else {
        if (QFile::rename(fileName,newFileName)) {
          /*----- Search the current file -----*/
          currentItem = newFile;
          fileList.replace(currentIndex,newFile);
          // Since rename might screw-up the alphabetical ordering, refresh the entire list
          LoadFiles();
          UpdateCurrent(currentItem,true);
          return 0;
        }
        else {
          // Unable to rename
          return 2;
        }
      }
    }
    /*----- File not found -----*/
    else {
      return -1;
    }
  }
  return -1;
}


void FileSelectFrm::RenameClicked()
{
  if (keyInput != 0) {
    msgIdx = 2;
    /*----- Display textInput form -----*/
    QString item = currentItem;

    /*----- Remove extension -----*/
    ClipExtension(&item);

    /*----- Shot text input form (the result will be used for rename) -----*/
    keyInput->SetText(item);
    keyInput->show();
  }
}


void FileSelectFrm::OkClicked()
{
  emit SelectClicked(currentItem);
}


void FileSelectFrm::RefreshDisplay()
{
  LoadFiles();
  if (currentIndex == -1) {
    UpdateCurrent(0,true);
  }
  else {
    UpdateDisplay();
  }
}


void FileSelectFrm::SetSelectedFile(const QString &file)
{
  UpdateCurrent(file,true);
}


void FileSelectFrm::SetCorrectButtons()
{
  bool act = glob->mstSts.sysActive;
  eUserType ut = glob->GetUser();

  if (ut < utTooling) {
    //        ui->btOk->setVisible(false);
    ui->btNew->setVisible(false);
    ui->btDelete->setVisible(false);
    ui->btFileRename->setVisible(false);
    ui->btSearch->setVisible(doSearch);
  }
  else {
    //        ui->btOk->setVisible(true);
    ui->btNew->setVisible(((!act && doNew) == true ? true : false));
    ui->btDelete->setVisible(((!act && doDelete) == true ? true : false));
    ui->btFileRename->setVisible(false);                                        //13-10-2011 ui->btRename->setVisible(((!act && doRename) == true ? true : false));
    ui->btSearch->setVisible(doSearch);
  }

  ui->btOk->setVisible((((!act && doAccept) || forceAccept) == true ? true : false));
}


void FileSelectFrm::CfgCurrentUserChanged(eUserType)
{
  SetCorrectButtons();
}


void FileSelectFrm::SysActiveChanged(bool)
{
  SetCorrectButtons();
}


void FileSelectFrm::MessageResult(int res)
{
  QString item = currentItem;
  disconnect(((MainMenu*)(glob->menu)),SIGNAL(ErrorResult(int)),this,SLOT(MessageResult(int)));

  if (res == 1) {                                                               // Ok clicked
    switch (msgIdx) {
    case 1:
      RemoveCurrentItem();
      break;

    case 4:
      RemoveAllItems();
      break;
    }
  }
  msgIdx = -1;
}


void FileSelectFrm::TextReceived(const QString &txt)
/*------------------------------*/
/* Ascii keyboard text received */
/*------------------------------*/
{
  int res = -1;
  QString name = txt + fileExt;

  switch (msgIdx) {
  /*----- Rename -----*/
  case 2:
    res = RenameCurrentItem(txt);
    switch (res) {
    case 0:
      emit ItemRenamed(name);
      break;
    case 1:                                                                 // Filename already in use
      ((MainMenu*)(glob->menu))->DisplayMessage(glob->ActUnitIndex(),0,tr("Filename already exists."),msgtAccept);
      break;
    case 2:                                                                 // Rename error
      ((MainMenu*)(glob->menu))->DisplayMessage(glob->ActUnitIndex(),0,tr("Unable to rename file."),msgtAccept);
      break;
    }
    break;

    /*----- Search -----*/
  case 3:
    searchString = txt;

    LoadFiles();
    UpdateDisplay();
    UpdateCurrent(0,true);

  break;
  }
}

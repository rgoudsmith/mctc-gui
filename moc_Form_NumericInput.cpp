/****************************************************************************
** Meta object code from reading C++ file 'Form_NumericInput.h'
**
** Created: Wed Feb 6 11:15:08 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_NumericInput.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_NumericInput.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_NumericInputFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x05,
      37,   35,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
      59,   16,   16,   16, 0x08,
      71,   16,   16,   16, 0x08,
      83,   16,   16,   16, 0x08,
      98,   16,   16,   16, 0x08,
     110,   16,   16,   16, 0x08,
     123,   16,   16,   16, 0x08,
     144,   16,   16,   16, 0x08,
     159,   16,   16,   16, 0x08,
     175,   16,   16,   16, 0x08,
     188,   16,   16,   16, 0x08,
     207,   16,   16,   16, 0x08,
     223,   16,   16,   16, 0x08,
     235,   16,   16,   16, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_NumericInputFrm[] = {
    "NumericInputFrm\0\0InputValue(float)\0,\0"
    "InputValue(float,int)\0Increment()\0"
    "Decrement()\0ClearDisplay()\0ShiftLeft()\0"
    "ShiftRight()\0ValueReceived(float)\0"
    "ClearClicked()\0ButtonClicked()\0"
    "DotClicked()\0BackspaceClicked()\0"
    "CancelClicked()\0OkClicked()\0SwitchClicked()\0"
};

const QMetaObject NumericInputFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_NumericInputFrm,
      qt_meta_data_NumericInputFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &NumericInputFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *NumericInputFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *NumericInputFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_NumericInputFrm))
        return static_cast<void*>(const_cast< NumericInputFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int NumericInputFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: InputValue((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 1: InputValue((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: Increment(); break;
        case 3: Decrement(); break;
        case 4: ClearDisplay(); break;
        case 5: ShiftLeft(); break;
        case 6: ShiftRight(); break;
        case 7: ValueReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 8: ClearClicked(); break;
        case 9: ButtonClicked(); break;
        case 10: DotClicked(); break;
        case 11: BackspaceClicked(); break;
        case 12: CancelClicked(); break;
        case 13: OkClicked(); break;
        case 14: SwitchClicked(); break;
        default: ;
        }
        _id -= 15;
    }
    return _id;
}

// SIGNAL 0
void NumericInputFrm::InputValue(float _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void NumericInputFrm::InputValue(float _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE

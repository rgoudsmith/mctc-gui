/********************************************************************************
** Form generated from reading UI file 'Form_IpEditor.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_IPEDITOR_H
#define UI_FORM_IPEDITOR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_IpEditorFrm
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_4;
    QLineEdit *edVal1;
    QLabel *label;
    QLineEdit *edVal2;
    QLabel *label_2;
    QLineEdit *edVal3;
    QLabel *label_3;
    QLineEdit *edVal4;
    QSpacerItem *horizontalSpacer_5;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_2;
    QGridLayout *gridLayout;
    QPushButton *bt7;
    QPushButton *bt8;
    QPushButton *bt9;
    QPushButton *bt4;
    QPushButton *bt5;
    QPushButton *bt6;
    QPushButton *bt1;
    QPushButton *bt3;
    QPushButton *bt0;
    QPushButton *btDot;
    QPushButton *btBack;
    QPushButton *bt2;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btOk;
    QPushButton *btCancel;

    void setupUi(QWidget *IpEditorFrm)
    {
        if (IpEditorFrm->objectName().isEmpty())
            IpEditorFrm->setObjectName(QString::fromUtf8("IpEditorFrm"));
        IpEditorFrm->resize(469, 512);
        IpEditorFrm->setCursor(QCursor(Qt::BlankCursor));
        IpEditorFrm->setWindowTitle(QString::fromUtf8("Form"));
        IpEditorFrm->setStyleSheet(QString::fromUtf8("#groupBox {border: 5px solid black; border-radius:7px;}\n"
""));
        verticalLayout_2 = new QVBoxLayout(IpEditorFrm);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox = new QGroupBox(IpEditorFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setTitle(QString::fromUtf8(""));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);

        edVal1 = new QLineEdit(groupBox);
        edVal1->setObjectName(QString::fromUtf8("edVal1"));
        edVal1->setMinimumSize(QSize(90, 70));
        edVal1->setMaximumSize(QSize(90, 70));
        edVal1->setCursor(QCursor(Qt::BlankCursor));
        edVal1->setFocusPolicy(Qt::NoFocus);
        edVal1->setText(QString::fromUtf8(""));
        edVal1->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(edVal1);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(25);
        label->setFont(font);
        label->setText(QString::fromUtf8("."));

        horizontalLayout_2->addWidget(label);

        edVal2 = new QLineEdit(groupBox);
        edVal2->setObjectName(QString::fromUtf8("edVal2"));
        edVal2->setMinimumSize(QSize(90, 70));
        edVal2->setMaximumSize(QSize(90, 70));
        edVal2->setCursor(QCursor(Qt::BlankCursor));
        edVal2->setFocusPolicy(Qt::NoFocus);
        edVal2->setText(QString::fromUtf8(""));
        edVal2->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(edVal2);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);
        label_2->setText(QString::fromUtf8("."));

        horizontalLayout_2->addWidget(label_2);

        edVal3 = new QLineEdit(groupBox);
        edVal3->setObjectName(QString::fromUtf8("edVal3"));
        edVal3->setMinimumSize(QSize(90, 70));
        edVal3->setMaximumSize(QSize(90, 70));
        edVal3->setCursor(QCursor(Qt::BlankCursor));
        edVal3->setFocusPolicy(Qt::NoFocus);
        edVal3->setText(QString::fromUtf8(""));
        edVal3->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(edVal3);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);
        label_3->setText(QString::fromUtf8("."));

        horizontalLayout_2->addWidget(label_3);

        edVal4 = new QLineEdit(groupBox);
        edVal4->setObjectName(QString::fromUtf8("edVal4"));
        edVal4->setMinimumSize(QSize(90, 70));
        edVal4->setMaximumSize(QSize(90, 70));
        edVal4->setCursor(QCursor(Qt::BlankCursor));
        edVal4->setFocusPolicy(Qt::NoFocus);
        edVal4->setText(QString::fromUtf8(""));
        edVal4->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(edVal4);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        bt7 = new QPushButton(groupBox);
        bt7->setObjectName(QString::fromUtf8("bt7"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(90);
        sizePolicy.setVerticalStretch(70);
        sizePolicy.setHeightForWidth(bt7->sizePolicy().hasHeightForWidth());
        bt7->setSizePolicy(sizePolicy);
        bt7->setMinimumSize(QSize(90, 70));
        bt7->setMaximumSize(QSize(90, 70));
        bt7->setText(QString::fromUtf8("7"));

        gridLayout->addWidget(bt7, 0, 0, 1, 1);

        bt8 = new QPushButton(groupBox);
        bt8->setObjectName(QString::fromUtf8("bt8"));
        sizePolicy.setHeightForWidth(bt8->sizePolicy().hasHeightForWidth());
        bt8->setSizePolicy(sizePolicy);
        bt8->setMinimumSize(QSize(90, 70));
        bt8->setMaximumSize(QSize(90, 70));
        bt8->setText(QString::fromUtf8("8"));

        gridLayout->addWidget(bt8, 0, 1, 1, 1);

        bt9 = new QPushButton(groupBox);
        bt9->setObjectName(QString::fromUtf8("bt9"));
        sizePolicy.setHeightForWidth(bt9->sizePolicy().hasHeightForWidth());
        bt9->setSizePolicy(sizePolicy);
        bt9->setMinimumSize(QSize(90, 70));
        bt9->setMaximumSize(QSize(90, 70));
        bt9->setText(QString::fromUtf8("9"));

        gridLayout->addWidget(bt9, 0, 2, 1, 1);

        bt4 = new QPushButton(groupBox);
        bt4->setObjectName(QString::fromUtf8("bt4"));
        sizePolicy.setHeightForWidth(bt4->sizePolicy().hasHeightForWidth());
        bt4->setSizePolicy(sizePolicy);
        bt4->setMinimumSize(QSize(90, 70));
        bt4->setMaximumSize(QSize(90, 70));
        bt4->setText(QString::fromUtf8("4"));

        gridLayout->addWidget(bt4, 1, 0, 1, 1);

        bt5 = new QPushButton(groupBox);
        bt5->setObjectName(QString::fromUtf8("bt5"));
        sizePolicy.setHeightForWidth(bt5->sizePolicy().hasHeightForWidth());
        bt5->setSizePolicy(sizePolicy);
        bt5->setMinimumSize(QSize(90, 70));
        bt5->setMaximumSize(QSize(90, 70));
        bt5->setText(QString::fromUtf8("5"));

        gridLayout->addWidget(bt5, 1, 1, 1, 1);

        bt6 = new QPushButton(groupBox);
        bt6->setObjectName(QString::fromUtf8("bt6"));
        sizePolicy.setHeightForWidth(bt6->sizePolicy().hasHeightForWidth());
        bt6->setSizePolicy(sizePolicy);
        bt6->setMinimumSize(QSize(90, 70));
        bt6->setMaximumSize(QSize(90, 70));
        bt6->setText(QString::fromUtf8("6"));

        gridLayout->addWidget(bt6, 1, 2, 1, 1);

        bt1 = new QPushButton(groupBox);
        bt1->setObjectName(QString::fromUtf8("bt1"));
        sizePolicy.setHeightForWidth(bt1->sizePolicy().hasHeightForWidth());
        bt1->setSizePolicy(sizePolicy);
        bt1->setMinimumSize(QSize(90, 70));
        bt1->setMaximumSize(QSize(90, 70));
        bt1->setText(QString::fromUtf8("1"));

        gridLayout->addWidget(bt1, 2, 0, 1, 1);

        bt3 = new QPushButton(groupBox);
        bt3->setObjectName(QString::fromUtf8("bt3"));
        sizePolicy.setHeightForWidth(bt3->sizePolicy().hasHeightForWidth());
        bt3->setSizePolicy(sizePolicy);
        bt3->setMinimumSize(QSize(90, 70));
        bt3->setMaximumSize(QSize(90, 70));
        bt3->setText(QString::fromUtf8("3"));

        gridLayout->addWidget(bt3, 2, 2, 1, 1);

        bt0 = new QPushButton(groupBox);
        bt0->setObjectName(QString::fromUtf8("bt0"));
        sizePolicy.setHeightForWidth(bt0->sizePolicy().hasHeightForWidth());
        bt0->setSizePolicy(sizePolicy);
        bt0->setMinimumSize(QSize(90, 70));
        bt0->setMaximumSize(QSize(90, 70));
        bt0->setText(QString::fromUtf8("0"));

        gridLayout->addWidget(bt0, 3, 1, 1, 1);

        btDot = new QPushButton(groupBox);
        btDot->setObjectName(QString::fromUtf8("btDot"));
        sizePolicy.setHeightForWidth(btDot->sizePolicy().hasHeightForWidth());
        btDot->setSizePolicy(sizePolicy);
        btDot->setMinimumSize(QSize(90, 70));
        btDot->setMaximumSize(QSize(90, 70));
        btDot->setText(QString::fromUtf8("."));

        gridLayout->addWidget(btDot, 3, 0, 1, 1);

        btBack = new QPushButton(groupBox);
        btBack->setObjectName(QString::fromUtf8("btBack"));
        btBack->setMinimumSize(QSize(90, 70));
        btBack->setMaximumSize(QSize(90, 70));
        btBack->setText(QString::fromUtf8("BACK"));

        gridLayout->addWidget(btBack, 3, 2, 1, 1);

        bt2 = new QPushButton(groupBox);
        bt2->setObjectName(QString::fromUtf8("bt2"));
        sizePolicy.setHeightForWidth(bt2->sizePolicy().hasHeightForWidth());
        bt2->setSizePolicy(sizePolicy);
        bt2->setMinimumSize(QSize(90, 70));
        bt2->setMaximumSize(QSize(90, 70));
        bt2->setText(QString::fromUtf8("2"));

        gridLayout->addWidget(bt2, 2, 1, 1, 1);


        horizontalLayout_3->addLayout(gridLayout);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btOk = new QPushButton(groupBox);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout->addWidget(btOk);

        btCancel = new QPushButton(groupBox);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("CANCEL"));

        horizontalLayout->addWidget(btCancel);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_2->addWidget(groupBox);


        retranslateUi(IpEditorFrm);

        QMetaObject::connectSlotsByName(IpEditorFrm);
    } // setupUi

    void retranslateUi(QWidget *IpEditorFrm)
    {
        Q_UNUSED(IpEditorFrm);
    } // retranslateUi

};

namespace Ui {
    class IpEditorFrm: public Ui_IpEditorFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_IPEDITOR_H

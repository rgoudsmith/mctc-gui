#include "Form_LearnOnlineItem.h"
#include "ui_Form_LearnOnlineItem.h"
#include "Form_MainMenu.h"
#include "Rout.h"

//#define LEARN_ONLINE_NO_TOOL_IMAGE

LearnOnlineItemFrm::LearnOnlineItemFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::LearnOnlineItemFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);
  unitIndex = 0;
  waitForInput = false;
  keyInput = 0;

  ui->edMaterial->installEventFilter(this);
  connect(ui->btSave,SIGNAL(clicked()),this,SLOT(SaveButtonClicked()));
}


LearnOnlineItemFrm::~LearnOnlineItemFrm()
{
  delete ui;
}


void LearnOnlineItemFrm::showEvent(QShowEvent *e)
{
  QBaseWidget::showEvent(e);

  /*----- When twin mode is active then unit number is already set to 0 or 1 with the forminit -----*/
  if (Rout::TwinMode()) {
    unitIndex = formNr;                           /* Twin mode, set unit index to the formNr (= 0 or 1) */
  }
  else {
    unitIndex = glob->ActUnitIndex();             /* Single unit mode, fetch actual unit number */
  }

  /*----- Update fiels -----*/
  SetMaterialName();
  ui->lbUnit->setText(glob->sysCfg[unitIndex].MC_ID);
}


bool LearnOnlineItemFrm::eventFilter(QObject *obj, QEvent *e)
{
  if (e->type() == QEvent::MouseButtonPress) {
    if (obj == ui->edMaterial) {
      if (glob->mstCfg.rcpEnable == false) {
        KeyBeep();
        ShowInput();
      }
    }
  }
  return QBaseWidget::eventFilter(obj,e);
}


void LearnOnlineItemFrm::FormInit(int itemFormNr)
{
  QBaseWidget::ConnectKeyBeep();

  formNr = itemFormNr;

  ui->lbUnit->setFont(*(glob->baseFont));
  ui->edMaterial->setFont(*(glob->baseFont));

  glob->SetButtonImage(ui->btSave,itSelection,selOk);

  connect(glob,SIGNAL(SysCfg_IDChanged(QString,int)),this,SLOT(DeviceIdChanged(QString,int)));
  connect(glob,SIGNAL(SysCfg_MaterialChanged(QString,int)),this,SLOT(MaterialChanged(QString,int)));
}


void LearnOnlineItemFrm::SetKeyboard(KeyboardFrm *keyb)
{
  if (keyInput != keyb) {
    if (keyInput != 0) {
      disconnect(keyInput,SIGNAL(inputText(QString)),this,SLOT(TextReceived(QString)));
    }

    keyInput = keyb;
    keyInput->FormInit();

    if (keyInput != 0) {
      connect(keyInput,SIGNAL(inputText(QString)),this,SLOT(TextReceived(QString)));
    }
  }
}


void LearnOnlineItemFrm::SetMaterialName()
{
  QString txt;
  if (glob->mstCfg.rcpEnable == false) {
    if (glob->sysCfg[unitIndex].material.isDefault) {
      txt = MATERIAL_EMPTY_NAME;
    }
    else {
      glob->GetDispMaterial(&txt,unitIndex);
      if (txt.isEmpty() || txt.isNull()) {
        txt = MATERIAL_EMPTY_NAME;
      }
    }
    ui->edMaterial->setText(txt);
  }
  else {
    if (glob->sysCfg[unitIndex].material.isDefault) {
      glob->GetDefaultMaterialName(&txt,unitIndex);
    }
    else {
      glob->GetDispMaterial(&txt,unitIndex);
      if (txt.isEmpty() || txt.isNull()) {
        txt = MATERIAL_EMPTY_NAME;
      }
    }
    ui->edMaterial->setText(txt);
  }
}


void LearnOnlineItemFrm::ShowInput()
{
  if (keyInput != 0) {
    waitForInput = true;
    if (ui->edMaterial->text() == MATERIAL_EMPTY_NAME) {
      keyInput->SetText("");
    }
    else {
      keyInput->SetText(ui->edMaterial->text());
    }
    keyInput->SetMaxLength(MAX_LENGTH_FILENAME_INPUT);
    keyInput->show();
  }
}


void LearnOnlineItemFrm::SaveButtonClicked()
{
  /*-----------------------*/
  /* BUTTON : save clicked */
  /*-----------------------*/
  bool save;

  save = true;

  if (glob->mstCfg.rcpEnable == false) {
    QString file = ui->edMaterial->text() + QString(MATERIAL_EXTENSION);
    QString path = *(glob->AppPath());
    path += QString(MATERIAL_FOLDER) + glob->fileSeparator;
    path += file;

    if (QFile::exists(path)) {
      /* Check if the desired material name is equal to the currently selected material.
             If it does match, we can silently overwrite the current material. (save = true)
             If it does not match, but the file does exist. Ask the user if the material can be overwriten. (save = false)
           */
      QString txt;
      txt = tr("Material")+" '"+ui->edMaterial->text()+"' "+tr("already exists.")+"\n"+tr("Overwrite existing material?");
      ((MainMenu*)(glob->menu))->DisplayMessage(-1,0,txt,msgtConfirm);
      connect(((MainMenu*)(glob->menu)),SIGNAL(ErrorResult(int)),this,SLOT(ResultReceived(int)));
      save = false;
    }
  }

  if (save) {
    // Material does not exist... save!
    QString txt;
    StoreMaterialData();

    if (glob->mstCfg.rcpEnable == false) {
      txt = glob->sysCfg[unitIndex].material.name;
      glob->SysCfg_SaveMaterial(unitIndex,glob->sysCfg[unitIndex].material.name,true);
      // Force to load material into the system.
      glob->SysCfg_SetMaterial("...",false,unitIndex,false);
      glob->SysCfg_SetMaterial(txt,false,unitIndex);
    }
    else {
      // Recipe function enabled (save material corFac into the recipe)
      StoreRecipeLineCorFac();
    }
    /*----- Message -----*/
    ((MainMenu*)(glob->menu))->DisplayMessage(-1,0,tr("Data saved."),msgtAccept);
  }
}


void LearnOnlineItemFrm::TextReceived(const QString &txt)
{
  if (waitForInput) {
    waitForInput = false;
    ui->edMaterial->setText(txt);
  }
}


void LearnOnlineItemFrm::StoreMaterialData()
{
  glob->sysCfg[unitIndex].curve.corFac = glob->sysSts[unitIndex].preCalibration.rpmCorrFac;
  // Indicate that we are using a user-saved curve (not a default one)
  glob->sysCfg[unitIndex].curve.calMode = (int)cmodAuto;

  if (glob->mstCfg.rcpEnable == false) {
    QString txt;
    txt = ui->edMaterial->text() + MATERIAL_EXTENSION;
    glob->sysCfg[unitIndex].material.name = txt;
    glob->sysCfg[unitIndex].material.isDefault = false;
  }
}


void LearnOnlineItemFrm::StoreRecipeLineCorFac()
{
  /*-------------------------------------------*/
  /* Store the correction factor in the recipe */
  /*-------------------------------------------*/
  /*----- Generate recipe file name -----*/
  QString rcpPath = (*(glob->AppPath())) + RECIPE_DEFAULT_FOLDER + glob->fileSeparator + glob->mstCfg.recipe;

  QFile rcpFile(rcpPath);
  /*----- Check if the file exists -----*/
  if (rcpFile.exists()) {
    /*----- Open the recipe file -----*/
    if (rcpFile.open(QIODevice::ReadWrite)) {
      RecipeStruct rcp;
      rcpFile.reset();
      rcpFile.read((char*)&rcp,sizeof(RecipeStruct));
      /*----- Overwrite the corFac with the new one (_gbi.sysCfg[unitIndex].curve.corFac) -----*/
      rcp.lines[unitIndex].corFac = glob->sysCfg[unitIndex].curve.corFac;
      /*----- Recalculate the checksum -----*/
      rcp.chksum = (((MainMenu*)(glob->menu))->fileSysCtrl.CalculateChecksum((char*)&rcp,((sizeof(RecipeStruct))-sizeof(int))));
      /*----- Overwrite original recipe with new recipe -----*/
      rcpFile.reset();
      rcpFile.write((char*)&rcp,sizeof(RecipeStruct));
      /*----- Close the file -----*/
      rcpFile.close();
      Rout::SyncFile();
    }
  }
}


void LearnOnlineItemFrm::ResultReceived(int res)
{
  disconnect(((MainMenu*)(glob->menu)),SIGNAL(ErrorResult(int)),this,SLOT(ResultReceived(int)));
  QString txt;
  switch(res) {
  /*----- OK -----*/
  case 1:
    StoreMaterialData();
    txt = glob->sysCfg[unitIndex].material.name;
    glob->SysCfg_SaveMaterial(unitIndex,glob->sysCfg[unitIndex].material.name,true);
    // Force to load material into the system.
    glob->SysCfg_SetMaterial("",false,unitIndex,false);
    glob->SysCfg_SetMaterial(txt,false,unitIndex);
    break;
    /*----- Cancel -----*/
  case 3:
    break;
  default:
    break;
  }
}


void LearnOnlineItemFrm::MaterialChanged(const QString &, int idx)
{
  if (idx == unitIndex) {
    SetMaterialName();
  }
}


void LearnOnlineItemFrm::DeviceIdChanged(const QString &id, int idx)
{
  if (idx == unitIndex) {
    ui->lbUnit->setText(id);
  }
}

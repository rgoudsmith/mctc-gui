/*===========================================================================*
 * File        : Form_UsbTransfer.cpp                                        *
 * Project     : MVC0012 - MC-TC                                             *
 * Description : Page 1 : Form for USB transer to and from the MC-TC.        *
 *             : Page 2 : Form 'Copying files'                               *
 *===========================================================================*/

#include "Form_UsbTransfer.h"
#include "ui_Form_UsbTransfer.h"
#include "Form_MainMenu.h"
#include <QMessageBox>
#include "Rout.h"

UsbTransferFrm::UsbTransferFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::UsbTransferFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);

  errorForm = new ErrorDlg();

  ui->btDone->setVisible(false);
  mode = etdCtrlToUsb;
  waitForResponse = false;

  connect(ui->btOne,SIGNAL(clicked()),this,SLOT(ButtonOneClicked()));
  connect(ui->btTwo,SIGNAL(clicked()),this,SLOT(ButtonTwoClicked()));
  //  connect(ui->btThree,SIGNAL(clicked()),this,SLOT(ButtonThreeClicked()));
  connect(ui->btAll,SIGNAL(clicked()),this,SLOT(ButtonAllClicked()));
  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(ButtonOkClicked()));
  connect(ui->btDone,SIGNAL(clicked()),this,SLOT(ButtonDoneClicked()));

  DisplayWidget(0);
}


UsbTransferFrm::~UsbTransferFrm()
{
  delete errorForm;
  delete ui;
}


void UsbTransferFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  errorForm->FormInit();

  ui->lbTitle->setFont(*(glob->captionFont));
  ui->lbTitle_2->setFont(*(glob->captionFont));

  ui->lbType->setFont(*(glob->baseFont));
  ui->lbItem->setFont(*(glob->baseFont));

  ui->lbProgType->setFont(*(glob->baseFont));
  ui->lbProgItem->setFont(*(glob->baseFont));

  ui->btOne->setFont(*(glob->baseFont));
  ui->btTwo->setFont(*(glob->baseFont));
  ui->btAll->setFont(*(glob->baseFont));

  ui->progItem->setFont(*(glob->baseFont));
  ui->progType->setFont(*(glob->baseFont));

  glob->SetButtonImage(ui->btDone,itSelection,selOk);
  glob->SetButtonImage(ui->btOk,itSelection,selOk);

  SetupDisplay();
}


// Private routines
void UsbTransferFrm::DisplayWidget(int type)
{
  switch (type) {
    // Display page 1 or 2
    case 0:                                                                     // Transfer screen
      ui->pages->setCurrentIndex(0);
      break;

    case 1:                                                                     // Copying screen
      ui->btDone->setVisible(false);
      ui->pages->setCurrentIndex(1);
      break;

    default:
      break;
  }
}


void UsbTransferFrm::SetupDisplay()
{
  /*===============================*/
  /* Setup the USB transfer window */
  /*===============================*/

  /*----- Set display layout + button captions -----*/
  switch (mode) {
    /*----- Controller to USB mode -----*/
    case etdCtrlToUsb:
      /*----- Setup screen title and button text -----*/
      ui->lbTitle->setText(tr("Transfer from Controller to USB"));
      ui->btOne->setText(tr("Unit Backup"));
      ui->btTwo->setText(tr("Configuration and Settings"));
      break;

      /*----- USB to Controller mode -----*/
    case etdUsbToCtrl:
      /*----- Setup screen title and button text -----*/

      ui->lbTitle->setText(tr("Transfer from USB to Controller"));

      /*----- Button one -----*/
      if (glob->mstCfg.rcpEnable) {
        ui->btOne->setText(tr("Recipes and Materials"));
      }
      else {
        ui->btOne->setText(tr("Materials"));
      }

      /*----- Button two -----*/
      ui->btTwo->setText(tr("Configuration and Settings"));

      break;
  }
}


int UsbTransferFrm::TransferMaterials(eTransferDirection direction)
{
  /*--------------------------------*/
  /* Transfer materials to/from USB */
  /*--------------------------------*/
  bool hasDir;
  QString backupFolder = QString(USB_BACKUP_FOLDER) + QString(MATERIAL_FOLDER);

  hasDir = false;
  switch(direction) {
    case etdCtrlToUsb:                                                          // Controller -> USB

      // Check if target directory exists.
      if (!(glob->usbStick->DirExists(backupFolder))) {
        // If not, try to create it
        if (glob->usbStick->DirCreate(backupFolder) == 0) {
          // Create was successful
          hasDir = true;
        }
      }
      else {
        hasDir = true;
      }
      // Copy all Material files from controller to USB.
      // Overwrite without prompting?
      sourcePath = (*(glob->AppPath()))+ MATERIAL_FOLDER + glob->fileSeparator;
      destPath = (glob->usbStick->basePath() + backupFolder + glob->fileSeparator);
      break;

    case etdUsbToCtrl:                                                          // USB -> Controller
      if (glob->usbStick->DirExists(backupFolder)) {
        hasDir = true;
      }
      sourcePath = (glob->usbStick->basePath() + backupFolder + glob->fileSeparator);
      destPath = (*(glob->AppPath()))+ MATERIAL_FOLDER + glob->fileSeparator;
      break;

    default:
      break;
  }

  if (hasDir) {
    QString fileFilter = QString("*") + MATERIAL_EXTENSION;
    QDir fileDir;
    fileList.clear();

    fileDir.setPath(sourcePath);
    fileList = fileDir.entryList(QStringList(fileFilter),QDir::Files | QDir::NoSymLinks);
    SetItemProgressLimit(0,fileList.count());
    SetItemProgressPos(0);

    itemIndex = 0;
    CopyNextFile();
    return 0;
  }
  else {
    return -1;
  }
  return -3;
}


int UsbTransferFrm::TransferRecipes(eTransferDirection direction)
{
  /*------------------------------*/
  /* Transfer recipes to/from USB */
  /*------------------------------*/
  bool hasDir;
  QString backupFolder = QString(USB_BACKUP_FOLDER) + QString(RECIPE_DEFAULT_FOLDER);

  hasDir = false;
  switch(direction) {
    case etdCtrlToUsb:                                                          // Controller -> USB
      // Check if target directory exists.
      if (!(glob->usbStick->DirExists(backupFolder))) {
        // If not, try to create it
        if (glob->usbStick->DirCreate(backupFolder) == 0) {
          // Create was successful
          hasDir = true;
        }
      }
      else {
        hasDir = true;
      }

      sourcePath = (*(glob->AppPath()))+ RECIPE_DEFAULT_FOLDER + glob->fileSeparator;
      destPath = (glob->usbStick->basePath() + backupFolder + glob->fileSeparator);
      break;

    case etdUsbToCtrl:                                                          // USB -> Controller
      if (glob->usbStick->DirExists(backupFolder)) {
        hasDir = true;
      }
      sourcePath = (glob->usbStick->basePath() + backupFolder + glob->fileSeparator);
      destPath = (*(glob->AppPath()))+ RECIPE_DEFAULT_FOLDER + glob->fileSeparator;
      break;

    default:
      break;
  }

  if (hasDir) {
    QString fileFilter = QString("*") + RECIPE_EXTENSION;
    QDir fileDir;

    fileList.clear();

    fileDir.setPath(sourcePath);
    fileList = fileDir.entryList(QStringList(fileFilter),QDir::Files | QDir::NoSymLinks);

    if (fileList.count() > 0) {
      SetItemProgressLimit(0,fileList.count());
    }
    else {
      SetItemProgressLimit(0,-1);
    }
    SetItemProgressPos(0);

    itemIndex = 0;
    CopyNextFile();
    return 0;
  }
  else {
    return -1;
  }
  return -3;
}


int UsbTransferFrm::TransferConfiguration(eTransferDirection direction)
{
  /*------------------------------------*/
  /* Transfer configuration to/from USB */
  /*------------------------------------*/
  bool hasDir;
  QString backupFolder = QString(USB_BACKUP_FOLDER) + QString(GEN_SETTINGS_PATH);
  QString deviceName;
  int ipAddress[4] = {0};
  int modAddress = 0;

  switch(direction) {
    case etdCtrlToUsb:                                                          // Controller -> USB
      // Check if target directory exists.
      if (!(glob->usbStick->DirExists(backupFolder))) {
        // If not, try to create it
        if (glob->usbStick->DirCreate(backupFolder) == 0) {
          // Create was successful
          hasDir = true;
        }
      }
      else {
        hasDir = true;
      }
      // Copy all Material files from controller to USB.
      // Overwrite without prompting?
      sourcePath = (*(glob->AppPath()))+ GEN_SETTINGS_PATH + glob->fileSeparator;
      destPath = (glob->usbStick->basePath() + backupFolder + glob->fileSeparator);
      break;

    case etdUsbToCtrl:                                                          // USB -> Controller
      // Backup original device name, ipaddress and modbus address
      for (int i=0;i<3;i++) {
        ipAddress[i] = glob->mstCfg.IPAddress[i];
      }
      modAddress = glob->mstCfg.modbusAddr;
      deviceName = glob->sysCfg[0].MC_ID;

      if (glob->usbStick->DirExists(backupFolder)) {
        hasDir = true;
      }
      sourcePath = (glob->usbStick->basePath() + backupFolder + glob->fileSeparator);
      destPath = (*(glob->AppPath()))+ GEN_SETTINGS_PATH + glob->fileSeparator;
      break;

    default:
      return -4;
      break;
  }

  if (hasDir) {
    QString fileFilter = QString("*") + GEN_SETTINGS_EXTENSION;
    QDir fileDir;

    fileList.clear();

    fileDir.setPath(sourcePath);
    fileList = fileDir.entryList(QStringList(fileFilter),QDir::Files | QDir::NoSymLinks);

    SetItemProgressLimit(0,fileList.count());
    SetItemProgressPos(0);

    itemIndex = 0;
    CopyNextFile();

    if (direction == etdUsbToCtrl) {
      // Reload configuration from file
      int res;
      ((MainMenu*)(glob->menu))->fileSysCtrl.LoadMasterSettings(&(glob->mstCfg),res);
      Rout::ConfigureNetwork();

      #warning ip address
      glob->MstCfg_SetModbusAddr(modAddress);

      //@@ MULTI_UNIT
      //@@@ MULTI UNIT
      // Restore original deviceID / per unit not yet implemented
      for (int i=0;i<=glob->mstCfg.slaveCount;i++) {
        ((MainMenu*)(glob->menu))->fileSysCtrl.LoadSystemSettings(i,glob,res);
        glob->SysCfg_SetID(deviceName,i);
      }
    }
    return 0;
  }
  else {
    return -1;
  }
  return 0;
}


int UsbTransferFrm::TransferEventLog(eTransferDirection direction)
{
  /*-------------------------------*/
  /* Transfer eventlog to/from USB */
  /*-------------------------------*/
  QString backupFolder = QString(USB_BACKUP_FOLDER) + QString(LOG_FOLDER);
  bool hasDir = false;

  switch(direction) {
    case etdCtrlToUsb:                                                          // Controller -> USB
      if (!(glob->usbStick->DirExists(backupFolder))) {
        // If not, try to create it
        if (glob->usbStick->DirCreate(backupFolder) == 0) {
          // Create was successful
          hasDir = true;
        }
      }
      else {
        hasDir = true;
      }

      sourcePath = (*(glob->AppPath()))+ LOG_FOLDER + glob->fileSeparator;
      destPath = (glob->usbStick->basePath() + backupFolder + glob->fileSeparator);

      break;

    case etdUsbToCtrl:                                                          // USB -> Controller
      /*----- Not possible to copy a log from USB to Controller ---*/
      return 0;
      break;

    default:
      return -4;
      break;
  }

  if (hasDir) {
    QString fileFilter = QString(EVENT_LOG_FILENAME);
    QDir fileDir;

    fileList.clear();
    fileDir.setPath(sourcePath);
    fileList = fileDir.entryList(QStringList(fileFilter),QDir::Files | QDir::NoSymLinks);

    SetItemProgressLimit(0,fileList.count());
    SetItemProgressPos(0);

    itemIndex = 0;
    CopyNextFile();
  }
  return 0;
}


int UsbTransferFrm::TransferProcesLog(eTransferDirection direction)
{
  /*---------------------------------*/
  /* Transfer Proces Log to/from USB */
  /*---------------------------------*/
  QString backupFolder = QString(USB_BACKUP_FOLDER) + QString(LOG_FOLDER);
  bool hasDir = false;

  switch(direction) {
    case etdCtrlToUsb:                                                          // Controller -> USB
      if (!(glob->usbStick->DirExists(backupFolder))) {
        // If not, try to create it
        if (glob->usbStick->DirCreate(backupFolder) == 0) {
          // Create was successful
          hasDir = true;
        }
      }
      else {
        hasDir = true;
      }

      sourcePath = (*(glob->AppPath()))+ LOG_FOLDER + glob->fileSeparator;
      destPath = (glob->usbStick->basePath() + backupFolder + glob->fileSeparator);

      break;

    case etdUsbToCtrl:                                                          // USB -> Controller
      /*---- Not possible to copy a log from USB to Controller ---*/
      return 0;
      break;

    default:
      return -4;
      break;
  }

  if (hasDir) {
    QString fileFilter = QString(PROCES_LOG_FILENAME);
    QDir fileDir;

    fileList.clear();
    fileDir.setPath(sourcePath);
    fileList = fileDir.entryList(QStringList(fileFilter),QDir::Files | QDir::NoSymLinks);

    SetItemProgressLimit(0,fileList.count());
    SetItemProgressPos(0);

    itemIndex = 0;
    CopyNextFile();
  }
  return 0;
}


// Item progressbar control
void UsbTransferFrm::SetItemProgressLimit(int min, int max)
{
  ui->progItem->setMinimum(min);
  ui->progItem->setMaximum(max);
}


void UsbTransferFrm::SetItemProgressPos(int pos)
{
  ui->progItem->setValue(pos);
  if (ui->progItem->maximum() >= 0) {
    ui->lbProgItem->setText(QString::number(pos)+"/"+QString::number(ui->progItem->maximum()));
  }
  else {
    ui->lbProgItem->setText("-/-");
  }
}


// Type progressbar control
void UsbTransferFrm::SetTypeProgressLimit(int min, int max)
{
  ui->progType->setMinimum(min);
  ui->progType->setMaximum(max);
}


void UsbTransferFrm::SetTypeProgressPos(int pos)
{
  ui->progType->setValue(pos);
  ui->lbProgType->setText(QString::number(pos)+"/"+QString::number(ui->progType->maximum()));
}


void UsbTransferFrm::CopyNextFile()
{
  QString sourceFile;
  QString destFile;
  QString txt;
  bool doesExist;

  if (itemIndex < fileList.count()) {
    SetItemProgressPos((itemIndex+1));
    sourceFile = sourcePath + fileList.at(itemIndex);
    destFile = destPath + fileList.at(itemIndex);
    ui->lbItem->setText(fileList.at(itemIndex));

    doesExist = QFile::exists(destFile);

    if ((doesExist) && ((!(yesAll)) && (!(noAll)))) {
      waitForResponse = true;

      txt = tr("File")+" '";
      txt += fileList.at(itemIndex);
      txt += "' ";
      txt += tr("already exists")+"\n"+tr("Overwrite existing file?");

      /*---- File exists, ask for confirmation to overwrite the existing file ----*/
      errorForm->ShowMessage(msgtSave,-1,0,txt);

      int res = errorForm->exec();                                              // Show dialog windows

      /*----- Handle message result -----*/
      switch(res) {
        case 1:                                                                 // YES
          CopyFile(sourceFile,destFile);
          break;

        case 2:                                                                 // YES ALL
          yesAll = true;
          CopyFile(sourceFile,destFile);
          break;

        case 3:                                                                 // NO
          break;

        case 4:                                                                 // NO ALL
          noAll = true;
          break;

      }

      res = 0;
      itemIndex++;
      CopyNextFile();
    }
    else {
      if ((yesAll) || (!(doesExist))) {
        CopyFile(sourceFile,destFile);
      }
      else {
      }
      itemIndex++;
      CopyNextFile();
    }
  }
  else {
    emit CopyDone();
  }
}


bool UsbTransferFrm::CopyFile(const QString &source, const QString &dest)
{
  if (QFile::exists(dest)) {
    QFile::remove(dest);
  }

  if (QFile::copy(source,dest) == true) {
    SetItemProgressPos((itemIndex+1));
    return true;
  }
  else {
    return false;
  }
}


// Public slots
void UsbTransferFrm::MessageResult(int)
{
}


void UsbTransferFrm::UsbTransferMode(eTransferDirection mod)
{
  if (mod != mode) {
    mode = mod;
    DisplayWidget(0);
    SetupDisplay();
  }
}


void UsbTransferFrm::ButtonOneClicked()
{
  /*==================================*/
  /* Button recipe / material clicked */
  /*==================================*/
  yesAll = false;
  noAll = false;
  if (glob->usbStick->UsbPresent()) {
    DisplayWidget(1);                                                           /* Display progess indicator */
    bool rcp = glob->mstCfg.rcpEnable;
    int cnt = 0;
    if (rcp) {
      SetTypeProgressLimit(0,3);
    }
    else {
      SetTypeProgressLimit(0,2);
    }
    SetTypeProgressPos(cnt);

    ui->lbType->setText(tr("Type: Materials"));
    if (TransferMaterials(mode) == 0) {
      cnt++;
      SetTypeProgressPos(cnt);

      if (rcp) {
        ui->lbType->setText(tr("Type: Recipes"));
        if (TransferRecipes(mode) == 0) {
          cnt++;
          SetTypeProgressPos(cnt);

          ui->lbType->setText(tr("Type: Configuration"));
          TransferConfiguration(mode);
          cnt++;
          SetTypeProgressPos(cnt);
        }
      }
      else {
        ui->lbType->setText(tr("Type: Configuration"));
        TransferConfiguration(mode);
        cnt++;
        SetTypeProgressPos(cnt);
      }
    }
  }

  ui->btDone->setVisible(true);
}


void UsbTransferFrm::ButtonTwoClicked()
{
  /*===========================================*/
  /* Button Configuration ans settings clicked */
  /*===========================================*/
  yesAll = false;
  noAll = false;
  if (glob->usbStick->UsbPresent()) {
    DisplayWidget(1);
    SetTypeProgressLimit(0,3);
    int cnt = 0;

    SetTypeProgressPos(cnt);
    ui->lbType->setText("tr(Type: Process Log");
    if (TransferProcesLog(mode) == 0) {
      cnt++;
      SetTypeProgressPos(cnt);
    }

    ui->lbType->setText(tr("Type: Event Log"));
    if (TransferEventLog(mode) == 0) {
      cnt++;
      SetTypeProgressPos(cnt);
    }

    ui->lbType->setText(tr("Type: Configuration"));
    if (TransferConfiguration(mode) == 0) {
      cnt++;
      SetTypeProgressPos(cnt);
    }
  }

  ui->btDone->setVisible(true);
}


void UsbTransferFrm::ButtonAllClicked()
{
  /*====================*/
  /* Button All clicked */
  /*====================*/
  yesAll = false;
  noAll = false;
  if (glob->usbStick->UsbPresent()) {
    int cnt = 0;
    DisplayWidget(1);
    SetTypeProgressLimit(0,5);
    SetTypeProgressPos(cnt);

    ui->lbType->setText(tr("Type: Materials"));
    if (TransferMaterials(mode) == 0) {
      cnt++;
      SetTypeProgressPos(cnt);
    }

    ui->lbType->setText(tr("Type: Recipes"));
    if ( TransferRecipes(mode) == 0) {
      cnt++;
      SetTypeProgressPos(cnt);
    }

    ui->lbType->setText(tr("Type: Process Log"));
    if (TransferProcesLog(mode) == 0) {
      cnt++;
      SetTypeProgressPos(cnt);
    }

    ui->lbType->setText(tr("Type: Event Log"));
    if (TransferEventLog(mode) == 0) {
      cnt++;
      SetTypeProgressPos(cnt);
    }

    ui->lbType->setText("tr(Type: Configuration");
    if (TransferConfiguration(mode) == 0) {
      cnt++;
      SetTypeProgressPos(cnt);
    }
  }
  ui->btDone->setVisible(true);
}


void UsbTransferFrm::ButtonDoneClicked()
{
  DisplayWidget(0);
}


void UsbTransferFrm::ButtonOkClicked()
{
  ((MainMenu*)(glob->menu))->DisplayWindow(wiBack);
}

/********************************************************************************
** Form generated from reading UI file 'Form_FillSystem.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_FILLSYSTEM_H
#define UI_FORM_FILLSYSTEM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QScrollArea>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "Form_MvcSystem.h"

QT_BEGIN_NAMESPACE

class Ui_FillSystemFrm
{
public:
    QHBoxLayout *horizontalLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    MvcSystem *mvcSystem;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btOk;
    QPushButton *btCancel;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *FillSystemFrm)
    {
        if (FillSystemFrm->objectName().isEmpty())
            FillSystemFrm->setObjectName(QString::fromUtf8("FillSystemFrm"));
        FillSystemFrm->resize(786, 477);
        FillSystemFrm->setMaximumSize(QSize(16777215, 16777215));
        FillSystemFrm->setCursor(QCursor(Qt::BlankCursor));
        FillSystemFrm->setWindowTitle(QString::fromUtf8("Form"));
        horizontalLayout = new QHBoxLayout(FillSystemFrm);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        scrollArea = new QScrollArea(FillSystemFrm);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setEnabled(true);
        scrollArea->setFrameShape(QFrame::NoFrame);
        scrollArea->setFrameShadow(QFrame::Raised);
        scrollArea->setLineWidth(0);
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 590, 477));
        mvcSystem = new MvcSystem(scrollAreaWidgetContents);
        mvcSystem->setObjectName(QString::fromUtf8("mvcSystem"));
        mvcSystem->setGeometry(QRect(10, 0, 561, 461));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(mvcSystem->sizePolicy().hasHeightForWidth());
        mvcSystem->setSizePolicy(sizePolicy);
        mvcSystem->setMinimumSize(QSize(0, 0));
        scrollArea->setWidget(scrollAreaWidgetContents);

        horizontalLayout->addWidget(scrollArea);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer = new QSpacerItem(20, 80, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        btOk = new QPushButton(FillSystemFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout_2->addWidget(btOk);

        btCancel = new QPushButton(FillSystemFrm);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("CANCEL"));

        horizontalLayout_2->addWidget(btCancel);


        verticalLayout->addLayout(horizontalLayout_2);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Preferred);

        verticalLayout->addItem(verticalSpacer_2);


        horizontalLayout->addLayout(verticalLayout);


        retranslateUi(FillSystemFrm);

        QMetaObject::connectSlotsByName(FillSystemFrm);
    } // setupUi

    void retranslateUi(QWidget *FillSystemFrm)
    {
        Q_UNUSED(FillSystemFrm);
    } // retranslateUi

};

namespace Ui {
    class FillSystemFrm: public Ui_FillSystemFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_FILLSYSTEM_H

#ifndef RECIPECONFIGFRM_H
#define RECIPECONFIGFRM_H

#include <QWidget>
#include "Form_QbaseWidget.h"
#include "Form_Keyboard.h"
#include "Form_NumericInput.h"
#include "Form_RecipeCheckPopup.h"

namespace Ui
{
  class RecipeFrm;
}


struct RecipeLineBufStruct
{
  char matName[(MAX_LENGTH_FILENAME+1)];
  QString sMatName;
  unsigned char matIsDefault;
  float corFac;
  float colPct;
};

struct RecipeBufStruct
{
  unsigned char devCount;
  char name[(MAX_LENGTH_FILENAME+1)];
  QString sName;
  char text[(MAX_LENGTH_RECIPE_DESCRIPTION+1)];
  QString sText;
  unsigned char mode;
  unsigned char input;
  float var1;
  float var2;
  RecipeLineBufStruct lines[CFG_MAX_UNIT_COUNT];
};

class RecipeFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit RecipeFrm(QWidget *parent = 0);
    ~RecipeFrm();
    void FormInit();
    RecipeBufStruct recipeBuf;

  protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);

  private:
    Ui::RecipeFrm *ui;
    KeyboardFrm * keyInp;
    NumericInputFrm * numInp;
    RecipeCheckPopupFrm * rcpCheckPopup;
    QVBoxLayout *layout;

    void resizeEvent(QResizeEvent *);
    bool eventFilter(QObject *, QEvent *);

    void ClearRecipeBuf(RecipeBufStruct*);
    void ClipRecipeExtension(QString *);
    void DisplayRecipe();
    void CheckRecipe(RecipeStruct*);
    void CheckRecipe();
    void BuildRcpItems();

//    RecipeBufStruct recipeBuf;
    int lineIdx;
    int numIdx;
    void DebugDisplayRecipeStruct(RecipeStruct*);
    void DebugDisplayRecipeBufStruct(RecipeBufStruct*);
    void SetCorrectButtons();

  private slots:
    void RcpSelClicked();
    void CancelClicked();
    void RemoveClicked();
    void SaveClicked();
    void TextReceived(const QString &);
    void ValueReceived(float);
    void GetFullRecipePath(QString *);
    void RecipeLoaded();
    void LoadDefaults();
    void SysActiveChanged(bool);

  public slots:
    void RecipeItemMaterialClicked(int);
    void MaterialReceived(const QString &, bool);
    void SetRecipe(const QString &);
    void CfgCurrentUserChanged(eUserType);
    void CopyRecipeToBuf(RecipeStruct*);
    void CopyRecipeFromBuf(RecipeStruct*);
    void SaveRecipe(RecipeStruct*,bool, bool);
    int LoadRecipe(const QString &, RecipeStruct*);
    void MessageResult(int);

    signals:
    void SetMaterial(const QString &);
    void RecipeSaved();
    void UnitSelected();

};
#endif                                                                          // RECIPECONFIGFRM_H

/****************************************************************************
** Meta object code from reading C++ file 'Form_ProcessDiagnostic.h'
**
** Created: Wed Feb 6 11:15:11 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_ProcessDiagnostic.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_ProcessDiagnostic.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ProcessDiagnosticFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      24,   22,   21,   21, 0x0a,
      62,   21,   21,   21, 0x0a,
      85,   21,   21,   21, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ProcessDiagnosticFrm[] = {
    "ProcessDiagnosticFrm\0\0,\0"
    "DiagDataReceived(tMsgControlUnit,int)\0"
    "SlaveCountChanged(int)\0OkClicked()\0"
};

const QMetaObject ProcessDiagnosticFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_ProcessDiagnosticFrm,
      qt_meta_data_ProcessDiagnosticFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ProcessDiagnosticFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ProcessDiagnosticFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ProcessDiagnosticFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ProcessDiagnosticFrm))
        return static_cast<void*>(const_cast< ProcessDiagnosticFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int ProcessDiagnosticFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: DiagDataReceived((*reinterpret_cast< tMsgControlUnit(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: SlaveCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: OkClicked(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

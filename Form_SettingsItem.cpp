#include "Form_SettingsItem.h"
#include "ui_Form_SettingsItem.h"
#include "Rout.h"

#define MAX_TEXT_LEN 19

SettingsItemFrm::SettingsItemFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::SettingsItemFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  numInput = 0;
  itemID = -1;
  fieldNr = -1;
  ui->setupUi(this);

  connect(ui->btSetting1,SIGNAL(clicked()),this,SLOT(Item1Clicked()));
  connect(ui->btSetting2,SIGNAL(clicked()),this,SLOT(Item2Clicked()));
}


SettingsItemFrm::~SettingsItemFrm()
{
  delete ui;
}


void SettingsItemFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  ui->lbItem1->setFont(*(glob->baseFont));
  ui->lbItem2->setFont(*(glob->baseFont));
  ui->btSetting1->setFont(*(glob->baseFont));
  ui->btSetting2->setFont(*(glob->baseFont));
}


void SettingsItemFrm::SetInput(NumericInputFrm *inp)
{
  if (numInput != 0) {
    // Disconnect from previous input
    disconnect(numInput,SIGNAL(InputValue(float,int)),this,SLOT(ValueReceived(float,int)));
  }

  numInput = inp;
  connect(numInput,SIGNAL(InputValue(float,int)),this,SLOT(ValueReceived(float,int)));
}


void SettingsItemFrm::SetUniqueID(int id)
{
  // Unique id to separate each individual SettingsItemFrm item.
  itemID = id;
}


void SettingsItemFrm::Item1Value()
{
  switch(item1.type) {
    case 0:                                                                     // Only whole numbers.
      ui->btSetting1->setText(QString::number(item1.value,'d',0));
      break;
    case 1:                                                                     // Any number with item1.decimals number of decimals behind the separator.
      ui->btSetting1->setText(QString::number(item1.value,'f',item1.decimals));
      break;
    case 2:                                                                     // String.
      ui->btSetting1->setText(item1.text);
      break;
  }
}


void SettingsItemFrm::Item2Value()
{
  switch(item2.type) {
    case 0:
      //(QString::number(glob->sysSts[glob->ActUnitIndex()].setProd,glob->formats.DosageFmt.format.toAscii(),glob->formats.DosageFmt.precision))
      ui->btSetting2->setText(QString::number(item2.value,'d',0));
      break;
    case 1:
      ui->btSetting2->setText(QString::number(item2.value,'f',item2.decimals));
      break;
    case 2:
      ui->btSetting2->setText(item1.text);
      break;
  }
}


void SettingsItemFrm::ShowOneItem(bool showOne)
{
  if (showOne) {
    ui->lbItem2->setVisible(false);
    ui->btSetting2->setVisible(false);
  }
  else {
    ui->lbItem2->setVisible(true);
    ui->btSetting2->setVisible(true);
  }
}


void SettingsItemFrm::Item1Clicked()
{
  if (numInput != 0) {
    fieldNr = 0;
    switch(item1.type) {
      case 0:
        numInput->SetDisplay(false,0,item1.value,item1.min,item1.max,itemID);
        numInput->SetMinIncVal(item1.minInc);
        numInput->show();
        break;

      case 1:
        numInput->SetDisplay(true,item1.decimals,item1.value,item1.min,item1.max,itemID);
        numInput->SetMinIncVal(item1.minInc);
        numInput->show();
        break;

      case 2:
        // Show text field editor.
        break;
    }
  }
}


void SettingsItemFrm::Item2Clicked()
{
  if (numInput != 0) {
    fieldNr = 1;
    switch(item2.type) {
      case 0:
        numInput->SetDisplay(false,0,item2.value,item2.min,item2.max,itemID);
        numInput->SetMinIncVal(item2.minInc);
        numInput->show();
        break;

      case 1:
        numInput->SetDisplay(true,item2.decimals,item2.value,item2.min,item2.max,itemID);
        numInput->SetMinIncVal(item2.minInc);
        numInput->show();
        break;

      case 2:
        // Show text field editor.
        break;
    }
  }
}


void SettingsItemFrm::SetItem1Title(const QString &txt)
{
  if (txt.length() <= MAX_TEXT_LEN) {
    ui->lbItem1->setText(txt);
  }
}


void SettingsItemFrm::SetItem1(int val, int min, int max, int minInc)
{
  item1.type = 0;
  item1.value = val;
  item1.min = min;
  item1.max = max;
  item1.minInc = minInc;
  Item1Value();
}


void SettingsItemFrm::SetItem1(float val, float min, float max, float minInc, int dec)
{
  item1.type = 1;
  item1.value = val;
  item1.min = min;
  item1.max = max;
  item1.minInc = minInc;
  item1.decimals = dec;
  Item1Value();
}


void SettingsItemFrm::SetItem1(const QString &txt)
{
  item1.type = 2;
  item1.text = txt;
  Item1Value();
}


void SettingsItemFrm::SetItem2Title(const QString &txt)
{
  if (txt.length() <= MAX_TEXT_LEN) {
    ui->lbItem2->setText(txt);
  }
}


void SettingsItemFrm::SetItem2(int val, int min, int max, int minInc)
{
  item2.type = 0;
  item2.value = val;
  item2.min = min;
  item2.max = max;
  item2.minInc = minInc;
  Item2Value();
}


void SettingsItemFrm::SetItem2(float val, float min, float max, float minInc, int dec)
{
  item2.type = 1;
  item2.value = val;
  item2.min = min;
  item2.max = max;
  item2.minInc = minInc;
  item2.decimals = dec;
  Item2Value();
}


void SettingsItemFrm::SetItem2(const QString &txt)
{
  item2.type = 2;
  item2.text = txt;
  Item2Value();
}


void SettingsItemFrm::ValueReceived(float val, int id)
{
  if (id == itemID) {
    switch (fieldNr) {
      case 0:
        item1.value = val;
        emit SetValueItem1(val);
        Item1Value();
        break;
      case 1:
        item2.value = val;
        emit SetValueItem2(val);
        Item2Value();
        break;
    }

    fieldNr = -1;
  }
}

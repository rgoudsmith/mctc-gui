#include <QFile>
#include "Form_MainMenu.h"
#include "ui_Form_MainMenu.h"
#include <qdebug.h>
#include "Udp.h"
#include "McServer.h"
#include "LogServer.h"
#include "ModbusServer.h"
#include "Rout.h"

/*----- Global variables -----*/
#define MAX_BACK_LIST   10                                                      /* Number of back list entries */

int windowDepth;                                                                /* Back list window depth */
eWindowIdx windowBackList[MAX_BACK_LIST];                                       /* Back list array */

MainMenu::MainMenu(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::MainMenu)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  glob->menu = this;                                                            /* Setup pointer to MainMenu */

  homeTim = 0;
  popupID = 0;

  m_ui->setupUi(this);
  m_ui->btAlarm->setVisible(false);                                             /* Disable alarm/ eventlog button */

  /*----- Disable windows title bar -----*/
  this->setWindowFlags(Qt::CustomizeWindowHint);

  ConnectKeyBeep();

  connect(&fileSysCtrl,SIGNAL(MasterSettingsLoaded()),glob,SLOT(MstCfg_LoadedFromFile()));
  connect(&fileSysCtrl,SIGNAL(SystemSettingsLoaded(int)),glob,SLOT(SysCfg_LoadedFromFile(int)));
  connect(glob,SIGNAL(MstCfg_UserChanged(eUserType)),this,SLOT(CfgCurrentUserChanged(eUserType)));
  connect(glob,SIGNAL(MstCfg_LanguageChanged(eLanguages)),this,SLOT(LanguageChanged()));

  OverMenu = 0;

  /*----- Setup Home screen timer -----*/
  homeTim = new QTimer(this);
  homeTim->setSingleShot(false);
  homeTim->setInterval(SPLASH_WINDOW_TIME * 100);
  connect(homeTim,SIGNAL(timeout()),SLOT(ReturnHomeScreen()));
  homeTim->start();

  alarmHndlr = new AlarmHandler(this);
  alarmHndlr->FormInit();

  stdConfig = 0;
  prodForm = 0;
  advConfig2Form = 0;
  loginForm = 0;
  fillSysForm = 0;
  advFillSysForm = 0;
  tolerancesForm = 0;
  mcWeightSettingsFrm = 0;
  calibSelectForm = 0;
  sysCalibForm = 0;
  historyForm = 0;
  primeForm = 0;
  unitSelForm = 0;
  materialForm = 0;
  weightCheckForm = 0;
  recipeSelectForm = 0;
  ipEditorForm = 0;
  settingsForm = 0;
  errorForm = 0;
  dateTimeForm = 0;
  balanceDiagForm = 0;
  superVisorForm = 0;
  popupSetConfirmForm = 0;
  balanceDiagForm = 0;
  learnOnlineForm = 0;
  usbSelectForm = 0;
  usbTransForm = 0;
  alarmConfigForm = 0;
  splashForm = 0;
  consumptionForm = 0;
  regrindConfigForm = 0;
  mcWeightForm = 0;

  /*----- Create the User Login window -----*/
  loginForm = new UserLoginFrm(m_ui->extWidget);
  loginForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  loginForm->hide();

  stdConfig = new UnitConfigStdFrm(m_ui->extWidget);
  stdConfig->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  stdConfig->hide();

  prodForm = new ProductionFrm(m_ui->extWidget);
  prodForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  prodForm->hide();

  mcWeightForm = new McWeightFrm(m_ui->extWidget);
  mcWeightForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  mcWeightForm->hide();

  advConfig2Form = new AdvConfig2Frm(m_ui->extWidget);
  advConfig2Form->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  advConfig2Form->hide();

  fillSysForm = new FillSystemFrm(m_ui->extWidget);
  fillSysForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  fillSysForm->hide();

  advFillSysForm = new AdvLoaderSettingsFrm(m_ui->extWidget);
  advFillSysForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  advFillSysForm->hide();

  tolerancesForm = new TolerancesFrm(m_ui->extWidget);
  tolerancesForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  tolerancesForm->hide();

  mcWeightSettingsFrm = new McWeightSettingsFrm(m_ui->extWidget);
  mcWeightSettingsFrm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  mcWeightSettingsFrm->hide();

  calibSelectForm = new CalibSelectFrm(m_ui->extWidget);
  calibSelectForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  calibSelectForm->hide();

  sysCalibForm = new SystemCalibrationFrm(m_ui->extWidget);
  sysCalibForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  sysCalibForm->hide();

  historyForm = new EventLogHistoryFrm(m_ui->extWidget);
  historyForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  historyForm->hide();

  primeForm = new PrimeFrm(m_ui->extWidget);
  primeForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  primeForm->hide();

  unitSelForm = new MultiUnitSelectFrm(m_ui->extWidget);
  unitSelForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  unitSelForm->hide();

  materialForm = new MaterialSelectFrm(m_ui->extWidget);
  materialForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  materialForm->hide();

  weightCheckForm = new WeightCheckFrm(m_ui->extWidget);
  weightCheckForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  weightCheckForm->hide();

  recipeSelectForm = new RecipeSelectFrm(m_ui->extWidget);
  recipeSelectForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  recipeSelectForm->hide();

  recipeConfigForm = new RecipeFrm(m_ui->extWidget);
  recipeConfigForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  recipeConfigForm->hide();

  ipEditorForm = new IpEditorFrm(m_ui->extWidget);
  ipEditorForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  ipEditorForm->hide();

  /*----- Agent/Movacolor settings -----*/
  settingsForm = new SettingsFrm(m_ui->extWidget);
  settingsForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  settingsForm->hide();

  /*----- Old "telnet" form -----*/
  balanceDiagForm = new ProcessDiagnosticFrm(m_ui->extWidget);
  balanceDiagForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  balanceDiagForm->hide();

  /*----- I/O Test Form -----*/
  superVisorForm = new ManualFrm(m_ui->extWidget);
  superVisorForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  superVisorForm->hide();

  learnOnlineForm = new LearnOnlineFrm(m_ui->extWidget);
  learnOnlineForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  learnOnlineForm->hide();

  usbSelectForm = new USBSelectFrm(m_ui->extWidget);
  usbSelectForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  usbSelectForm->hide();

  consumptionForm = new ConsumptionFrm(m_ui->extWidget);
  consumptionForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  consumptionForm->hide();

  regrindConfigForm = new RegrindConfigFrm(m_ui->extWidget);
  regrindConfigForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  regrindConfigForm->hide();

  usbTransForm = new UsbTransferFrm(m_ui->extWidget);
  usbTransForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  usbTransForm->hide();

  alarmConfigForm = new AlarmConfigFrm(m_ui->extWidget);
  alarmConfigForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  alarmConfigForm->hide();

  connect(prodForm,SIGNAL(RecipeSelected(QString)),recipeConfigForm,SLOT(SetRecipe(QString)));
  connect(recipeConfigForm,SIGNAL(RecipeSaved()),recipeSelectForm,SLOT(RefreshFileList()));

  splashForm = new SplashFrm();
  splashForm->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
  splashForm->menu = this;
  splashForm->hide();

  dateTimeForm = new DateTimeEditorFrm();
  dateTimeForm->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
  dateTimeForm->hide();

  errorForm = new ErrorFrm();
  errorForm->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
  errorForm->hide();

  popupSetConfirmForm = new PopupSettingConfirmFrm();
  popupSetConfirmForm->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
  popupSetConfirmForm->hide();

  udp = new Udp();
  udp->FormInit();

  /*----- McLan TCP/IP server -----*/
  mcServer = new McServer();
  mcServer->FormInit();

  /*----- Log TCP/IP server -----*/
  logServer = new LogServer();
  logServer->FormInit();

  /*----- Modbus server -----*/
  modBusServer = new ModbusServer();
  logServer->FormInit();

  connect(popupSetConfirmForm,SIGNAL(MessageResult(int)),this,SLOT(ErrorResultReceived(int)));

  layout = new QVBoxLayout(m_ui->extWidget);

  layout->addWidget(loginForm);
  layout->addWidget(stdConfig);
  layout->addWidget(prodForm);
  layout->addWidget(mcWeightForm);
  layout->addWidget(fillSysForm);
  layout->addWidget(advFillSysForm);
  layout->addWidget(tolerancesForm);
  layout->addWidget(mcWeightSettingsFrm);
  layout->addWidget(calibSelectForm);
  layout->addWidget(advConfig2Form);
  layout->addWidget(sysCalibForm);
  layout->addWidget(historyForm);
  //  layout->addWidget(trendForm);
  layout->addWidget(primeForm);
  layout->addWidget(unitSelForm);
  layout->addWidget(materialForm);
  layout->addWidget(weightCheckForm);
  layout->addWidget(recipeSelectForm);
  layout->addWidget(recipeConfigForm);
  layout->addWidget(ipEditorForm);
  layout->addWidget(settingsForm);
  layout->addWidget(balanceDiagForm);
  layout->addWidget(superVisorForm);
  layout->addWidget(learnOnlineForm);
  layout->addWidget(usbSelectForm);
  layout->addWidget(usbTransForm);
  layout->addWidget(alarmConfigForm);
  layout->addWidget(consumptionForm);
  layout->addWidget(regrindConfigForm);
  layout->addWidget(mcWeightForm);

  m_ui->extWidget->setLayout(layout);
  /*----- Set the extWidget container item to zero as initialization -----*/
  /*----- connect the btMenu click to the publicaly available signal MenuClicked() so external items (the overlay menu in this case) can connect to it. -----*/
  connect(m_ui->btMenu,SIGNAL(clicked()),this,SLOT(btMenuClicked()));
  connect(m_ui->btHome,SIGNAL(clicked()),this,SLOT(HomeClicked()));
  connect(m_ui->btAlarm,SIGNAL(clicked()),this,SLOT(AlarmClicked()));
  connect(m_ui->btBack,SIGNAL(clicked()),this,SLOT(BackClicked()));
  connect(m_ui->btPrime,SIGNAL(clicked()),this,SLOT(PrimeClicked()));
  connect(m_ui->btOnOff,SIGNAL(clicked()),this,SLOT(OnOffClicked()));
  connect(m_ui->btSelect,SIGNAL(clicked()),this,SLOT(SelectClicked()));

  connect(recipeConfigForm,SIGNAL(SetMaterial(QString)),materialForm,SLOT(SetSelectedMaterial(QString)));
  connect(materialForm,SIGNAL(MaterialSelected(QString,bool)),recipeConfigForm,SLOT(MaterialReceived(QString,bool)));
  connect(recipeSelectForm,SIGNAL(SelectedItem(QString)),recipeConfigForm,SLOT(SetRecipe(QString)));

  connect(prodForm,SIGNAL(RecipeSelected(QString)),recipeSelectForm,SLOT(ShowWithRecipe(QString)));
  connect(unitSelForm,SIGNAL(RecipeSelected(QString)),recipeSelectForm,SLOT(ShowWithRecipe(QString)));
  connect(prodForm,SIGNAL(MaterialSelected(QString)),materialForm,SLOT(SetSelectedMaterial(QString)));

  actWindow = 0;
}


MainMenu::~MainMenu()
{
  homeTim->stop();
  delete alarmConfigForm;
  delete usbTransForm;
  delete usbSelectForm;
  delete popupSetConfirmForm;
  delete learnOnlineForm;
  delete superVisorForm;
  delete dateTimeForm;
  delete balanceDiagForm;
  delete settingsForm;
  delete ipEditorForm;
  delete recipeConfigForm;
  delete recipeSelectForm;
  delete loginForm;
  delete stdConfig;
  delete prodForm;
  delete fillSysForm;
  delete advFillSysForm;
  delete tolerancesForm;
  delete mcWeightSettingsFrm;
  delete calibSelectForm;
  delete advConfig2Form;
  delete sysCalibForm;
  delete historyForm;
  //  delete trendForm;
  delete primeForm;
  delete unitSelForm;
  delete weightCheckForm;
  delete splashForm;
  delete errorForm;
  delete consumptionForm;
  delete mcWeightForm;
  delete glob;
  delete m_ui;
}


void MainMenu::CfgCurrentUserChanged(eUserType)
{
  eUserType ut = glob->GetUser();

  /*----- Enable / disable unit select button -----*/
  if (glob->mstCfg.slaveCount >= 1) {                                           /* Multiple unit configured ? */
    m_ui->btSelect->setVisible(true);                                           /* Enabled */
    m_ui->btSelect->setEnabled(true);
  }
  else {
    m_ui->btSelect->setVisible(false);                                          /* Disabled */
    m_ui->btSelect->setEnabled(false);
  }

  if (ut >= utTooling) {                                                        /* Changed on 13-08-2012 */
    m_ui->btAlarm->setEnabled(true);
    m_ui->btAlarm->setVisible(true);
  }
  else {
    m_ui->btAlarm->setEnabled(false);
    m_ui->btAlarm->setVisible(false);
  }
}


void MainMenu::LanguageChanged()
{
  /*---------------------------------------------------------------*/
  /* If there was a language change, re-create the alarmConfigForm */
  /*---------------------------------------------------------------*/

  /*----- Delete actual form -----*/
  delete alarmConfigForm;

  /*----- Create a new one -----*/
  alarmConfigForm = new AlarmConfigFrm(m_ui->extWidget);
  alarmConfigForm->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  alarmConfigForm->hide();

  /*----- Rebuild the alarm text list for the new language -----*/
  alarmConfigForm->FormInit();
}


bool MainMenu::event(QEvent *event)
{
  /*----------------------------------------------------*/
  /* Set backlit to 100% when a mouse event is detected */
  /*----------------------------------------------------*/
  if (event->type() == QEvent::MouseButtonPress) {

    /* Check backlit, if active turn it on and ignore event */
    if (Rout::BacklitOn()) {
      return(TRUE);
    }
  }
  return QWidget::event(event);
}


void MainMenu::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
  case QEvent::LanguageChange:
    m_ui->retranslateUi(this);
    glob->LanguageUpdate();
    break;
  default:
    break;
  }
}


void MainMenu::resizeEvent(QResizeEvent *e)
{
  QWidget::resizeEvent(e);
  if (OverMenu != 0) {
    ((OverlayMenu*)OverMenu)->Reposition(this->width(),this->height());
  }

  int x,y;
  x = (m_ui->extWidget->width()/2) - (errorForm->width()/2);
  y = (m_ui->extWidget->height()/2) - (errorForm->height()/2);
  errorForm->move(x,y);

  x = (m_ui->extWidget->width()/2) - (popupSetConfirmForm->width()/2);
  y = (m_ui->extWidget->height()/2) - (popupSetConfirmForm->height()/2);
  popupSetConfirmForm->move(x,y);

  x = (m_ui->extWidget->width()/2) - (dateTimeForm->width()/2);
  y = (m_ui->extWidget->height()/2) - (dateTimeForm->height()/2);
  dateTimeForm->move(x,y);
}


global* MainMenu::GBI()
{
  return glob;
}


void MainMenu::SetHomeTimerInterval(int tim)
{
  /*------------------------------------------*/
  /* Re-start timer return to the Home screen */
  /*------------------------------------------*/
  int timVal = tim * 1000;

  if (timVal > 0) {                                                             /* Timer value can't be negative ! */
    if (homeTim->interval() != timVal) {
      homeTim->setInterval(timVal);
    }
  }
  homeTim->start();
}


void MainMenu::ReturnHomeScreen()
{
  /*---------------------------*/
  /* Return to the Home screen */
  /*---------------------------*/
  int idx;

  idx = glob->ActUnitIndex();

  /*----- Detect pop-up active  -----*/
  if (glob->mstSts.popupActive == false) {
    /*----- Switch to production screen -----*/
    if (actWindow == prodForm) {                                                /* Production already active */
      if (glob->sysSts[idx].preCalibration.calib.sts == calNone) {              /* No precalibration busy */
        /*----- Backlit dim -----*/
        Rout::LcdBrightNess(10);                                                /* Dim display backlit */

        /*----- Switch to Minimum production mode -----*/
        prodForm->SetDisplayState(mvcDSminimum);
      }
    }
  }
}


/*----- A link to the overlay menu. -----*/
void MainMenu::SetOverlayMenu(QWidget *oMenu)
{
  OverMenu = oMenu;
  initOverlay();
}


void MainMenu::FoldMenu()
{
  if (OverMenu != 0) {
    ((OverlayMenu*)(OverMenu))->FoldMenu();
  }
}


void MainMenu::initOverlay()
{
  if (OverMenu != 0) {
    /*----- Set the images of the overlay (popup) menu. (0 is at the top, 5 is at the bottom) -----*/
    glob->SetButtonImage(((OverlayMenu*)OverMenu)->GetMenuButton(0),itMenu,mnLogin);
    glob->SetButtonImage(((OverlayMenu*)OverMenu)->GetMenuButton(1),itConfig,cfgConfig);
    glob->SetButtonImage(((OverlayMenu*)OverMenu)->GetMenuButton(2),itMenu,mnTrend);
    glob->SetButtonImage(((OverlayMenu*)OverMenu)->GetMenuButton(3),itMenu,mnLearn);
    glob->SetButtonImage(((OverlayMenu*)OverMenu)->GetMenuButton(4),itGeneral,genUSB);
    glob->SetButtonImage(((OverlayMenu*)OverMenu)->GetMenuButton(5),itMenu,mnUnlock);
    glob->SetButtonImage(((OverlayMenu*)OverMenu)->GetMenuButton(6),itMenu,mnConsumption);
    ((OverlayMenu*)(OverMenu))->FormInit();
  }
}


void MainMenu::PositionOverlayMenu()
{
  if (OverMenu != 0) {
    ((OverlayMenu*)(OverMenu))->Reposition(this->width(),this->height());
  }
}


int MainMenu::LastPopupSelection()
{
  switch(popupID) {
  case 1:
    if (errorForm != 0) {
      return errorForm->LastSelection();
    }
    break;
  case 2:
    if (popupSetConfirmForm != 0) {
      return popupSetConfirmForm->LastSelection();
    }
    break;
  }
  return -1;
}


void MainMenu::SetAppPath(QString *_path)
{
  int res;

  if ((!_path->isEmpty()) && (!_path->isNull())) {
    glob->SetAppPath(_path);
    int idx = 0;
    /* Load system master settings from file */

    fileSysCtrl.SetBaseDir(*(_path));

    /*----- Mastersettings are loaded. If recipe is enabled check the recipe _AFTER_ loading all units -----*/
    fileSysCtrl.LoadMasterSettings(&(glob->mstCfg),res);
    if (res != 0) {
      /*----- Settings not loaded, restoring Factory Defaults -----*/
      fileSysCtrl.StoreMasterFactDefaults();
      /*----- Load Factory Defaults from file. -----*/
      fileSysCtrl.LoadMasterSettings(&(glob->mstCfg),res);
    }

    /*----- Setup metric / imperial units -----*/
    Rout::SetUnits();

    /*----- Configure network -----*/
    Rout::ConfigureNetwork();

    /*----- Fetch eth0 IP and MAC address -----*/
    Rout::ReadEth0Data();

    /*----- force actual unit number to 0 -----*/
    glob->MstSts_SetActUnitIndex(idx);

    /*----- Setup TWIN mode */
    glob->mstSts.isTwin = Rout::TwinMode();

    /*----- Setup startup user level -----*/
    glob->SetUser(glob->mstCfg.defLogin);

    /* Load system unit settings from file */
    for (idx=0;idx<=glob->mstCfg.slaveCount;idx++) {
      fileSysCtrl.LoadSystemSettings(idx,glob,res);

      if (res != 0) {
        /*----- Settings not loaded, restoring Factory Defaults -----*/
        fileSysCtrl.StoreSystemFactDefaults(glob,idx);
        /*----- Load Factory Defaults from file. -----*/
        fileSysCtrl.LoadSystemSettings(idx,glob,res);
        /*----- force actual unit number to 0 -----*/
        glob->MstSts_SetActUnitIndex(idx);
      }
    }

    if (glob->mstCfg.rcpEnable) {
      /*
            This recipe check is a compilation of code from the RecipeCheckPopupFrm unit and the RecipeFrm unit.
            It loads a recipe struct from file, checks its integrity and if everything is ok, loads the recipe into the controller.
        */

      /*----- Check recipe when unit starts to make sure everything is ok. -----*/
      RecipeCheckPopupFrm * check = new RecipeCheckPopupFrm(0);
      RecipeStruct rcp;

      check->FormInit();

      /*----- Load recipe into local recipe struct (local variable = rcpFile) -----*/
      memset((&rcp),0x00,sizeof(RecipeStruct));

      /*----- if a recipe filename is present -----*/
      if (!((glob->mstCfg.recipe.isNull()) || (glob->mstCfg.recipe.isEmpty()))) {
        /*----- Get the full path to the file -----*/
        QString fullPath = glob->mstCfg.recipe;
        fullPath = *(glob->AppPath()) + RECIPE_DEFAULT_FOLDER + glob->fileSeparator + fullPath;

        /*----- Open recipe file read only (to prevent data corruption by accident) -----*/
        if (QFile::exists(fullPath)) {
          QFile fileRcp(fullPath);
          if (fileRcp.open(QIODevice::ReadOnly)) {
            fileRcp.reset();
            fileRcp.read(((char*)&rcp),sizeof(RecipeStruct));
            fileRcp.close();
          }
        }

        /*----- Load the recipe into the RecipeCheckFrmPopup -----*/
        check->SetRecipe(&rcp);
        /*----- Check the recipe for errors -----*/
        if (!(check->ProcessCheck())) {
          /*----- Something is wrong... -----*/

          /*----- Clear recipe name -----*/
          glob->MstCfg_SetRecipe("");

          /*----- Set unit(s) to default material(s). -----*/
          for(int i=0;i<=glob->mstCfg.slaveCount;i++) {
            QString mat;
            glob->GetDefaultMaterial(&mat,i);
            glob->SysCfg_SetMaterial(mat,true,i);
          }

          /*----- Recipe is not OK, run entire check again which will set a message to inform the user -----*/
          check->CheckRecipe(&rcp);
        }
        else {
          /*----- everything is ok... apply the recipe. -----*/
          check->ApplyRecipe();
        }
      }
      //@@ Check recipe implementation needed.
    }

    /*----- Connect _gbi functions to fileSysCtrl to make sure any changes are saved to file. -----*/
    connect(glob,SIGNAL(MstCfg_SaveConfigToFile()),this,SLOT(StoreMasterSettings()));
    connect(glob,SIGNAL(SysCfg_SaveConfigToFile(int)),this,SLOT(StoreSystemSettings(int)));

    /*----- Call the init functions for the various form -----*/
    loginForm->FormInit();
    stdConfig->FormInit();
    prodForm->FormInit();
    fillSysForm->FormInit();
    historyForm->FormInit();
    advFillSysForm->FormInit();
    tolerancesForm->FormInit();
    mcWeightSettingsFrm->FormInit();
    calibSelectForm->FormInit();
    advConfig2Form->FormInit();
    sysCalibForm->FormInit();
    primeForm->FormInit();
    unitSelForm->FormInit();
    materialForm->FormInit();
    weightCheckForm->FormInit();
    recipeSelectForm->FormInit();
    recipeConfigForm->FormInit();
    ipEditorForm->FormInit();
    settingsForm->FormInit();
    balanceDiagForm->FormInit();
    superVisorForm->FormInit();
    learnOnlineForm->FormInit();
    usbSelectForm->FormInit();
    usbTransForm->FormInit();
    errorForm->FormInit();
    popupSetConfirmForm->FormInit();
    dateTimeForm->FormInit();
    consumptionForm->FormInit();
    regrindConfigForm->FormInit();
    mcWeightForm->FormInit();

    glob->SetButtonImage(m_ui->btPrime,itMenu,mnPrime);
    glob->SetButtonImage(m_ui->btBack,itMenu,mnBack);
    glob->SetButtonImage(m_ui->btHome,itMenu,mnHome);
    glob->SetButtonImage(m_ui->btAlarm,itMenu,mnEventLog);
    glob->SetButtonImage(m_ui->btMenu,itMenu,mnMenu);
    glob->SetButtonImage(m_ui->btOnOff,itMenu,mnButtonOn);
    glob->SetButtonImage(m_ui->btSelect,itMenu,mnUnitSelect);

    connect(glob,SIGNAL(MstCfg_SystemTimeChanged(QDateTime)),this,SLOT(TimeChanged(QDateTime)));
    connect(glob,SIGNAL(MstSts_SysActiveChanged(bool)),this,SLOT(SysActiveChanged(bool)));
    connect(glob,SIGNAL(SysSts_MCStatusChanged(eMCStatus,int)),this,SLOT(McStatusChanged(eMCStatus,int)));
    connect(dateTimeForm,SIGNAL(DateTimeChanged(QString)),glob,SLOT(MstCfg_UpdateSystemTime(QString)));
    connect(errorForm,SIGNAL(ButtonClicked(int)),this,SLOT(ErrorResultReceived(int)));

    m_ui->lbDateTime->setFont(*(glob->baseFont));

    /*----- DisplayPrime can _ONLY_ be called _AFTER_ a call to glob->SetAppPath(); -----*/
    DisplayPrime(true);

  }
}


QString * MainMenu::GetAppPath()
{
  return glob->AppPath();
}


void MainMenu::StoreMasterSettings()
{
  int res;
  fileSysCtrl.StoreMasterSettings(&(glob->mstCfg),0,res);
  //@@ Hier kan nog een melding gegenereerd worden als opslaan niet goed gaat. (res != 0)
}


void MainMenu::StoreSystemSettings(int idx)
{
  int res;
  fileSysCtrl.StoreSystemSettings(idx,glob,0,res);
  //@@ Hier kan nog een melding gegenereerd worden als opslaan niet goed gaat. (res != 0)
}


void MainMenu::ActiveAlarmHandler()
{
  if (alarmHndlr != 0) {
    alarmHndlr->ActiveAlarmHandler();
  }
}


int MainMenu::DisplayMessage(int idx,int msgNr,const QString &text,int type)
{
  /*==========================================================================*/
  /* Blocking call to a message popup window.                                 */
  /* Returns the result of the popup                                          */
  /*==========================================================================*/
  if (errorForm != 0) {
    popupID = 1;
    if (type >= msgtCount) type = 0;
    if (type == msgtConfig) type = 0;
    errorForm->ShowMessage(msgType(type),idx,msgNr,text);
    return 0;
  }
  return -1;
}


void MainMenu::DisplayMessage(const QString &txt, int imgType, int imgIdx, int idx)
{
  if (popupSetConfirmForm != 0) {
    popupID = 2;
    popupSetConfirmForm->ShowConfigMessage(txt,imgType,imgIdx,idx);
  }
}


int MainMenu::DisplayMessage(int idx, const QString &text, int timeOut)
{
  /* Blocking call to a message popup window, returns the result of the popup */
  popupID = 1;
  if (errorForm != 0) {
    errorForm->ShowMessage(idx,text,timeOut);
    return 0;
  }
  return -1;
}


void MainMenu::DisplayPrime(bool _disp)
{
  if (_disp) {
    glob->SetButtonImage(m_ui->btPrime,itMenu,mnPrime);
  }
  else {
    glob->SetButtonImage(m_ui->btPrime,itMenu,mnPrimeOff);
  }
}


void MainMenu::DisplayWindow(eWindowIdx windowNr, int calWindow)
{
  /*----------------------------------------------------------------------------------*/
  /* Display a window in the extWidget container according to the clicked menu button */
  /*----------------------------------------------------------------------------------*/

  if (windowNr == wiBack) {
    BackClicked();
  }
  else {
    if (glob->calStruct.sts == calsIdle) {

      if (windowNr < wiCount) {
        /*----- Reset back list when at home screen -----*/
        if ((windowNr == wiHome) || (windowNr == wiHomeMultiUnit)) {
          windowDepth = 0;
        }
        else {
          /*----- Save actual window in back list -----*/
          if ((calWindow != (eWindowIdx)wiBack) && (windowNr != wiHome) && (windowNr != actWindowIdx)) {
            /*----- Update window back list -----*/
            windowBackList[windowDepth] = actWindowIdx;
            if (windowDepth < MAX_BACK_LIST-1) {
              windowDepth++;
            }
          }
        }

        actWindowIdx = (eWindowIdx)windowNr;

        int idx = glob->ActUnitIndex();
        /*----- If we switch to a different window than Prime, and prime is still active, disable prime -----*/
        if ((actWindowIdx != wiPrime) && (glob->sysSts[idx].prime.activeSts)) {
          /*----- Disable prime -----*/
          glob->SysSts_SetPrimeActive(false,idx);
        }

        /*----- If new window != supervisorForm and manual IO is active -----*/
        if ((actWindowIdx != wiManualIo) && (glob->sysSts[idx].manIO.active)) {
          /*----- Disable manual IO -----*/
          glob->sysSts[idx].manIO.active = false;
        }

        if (actWindow != 0) {
          actWindow->hide();
        }

        m_ui->btSelect->setEnabled(true);

        /*----- Switch the actWindow variable to the correct variable -----*/
        switch(windowNr) {

        /*----- No window to display -----*/
        case wiNone:
          actWindow = 0;
          break;

          /*----- Login window -----*/
        case wiLogin:
          actWindow = loginForm;
          break;

          /*----- Standard Unit Configuration -----*/
        case wiUnitCfgStd:
          if (glob->mstSts.sysActive) {
            //            if (glob->GetUser() >= utAgent) {                                 // Agent == Service
            if (glob->GetUser() >= utTooling) {                               // Agent == Service
              actWindow = stdConfig;
            }
            else {
              glob->mstSts.buzzerCtrl = buzNoAccess;
              ((MainMenu*)(glob->menu))->DisplayMessage(-1,0,tr("No access, unit is active !"),msgtAccept);
            }
          }
          else {
            actWindow = stdConfig;

          }
          break;

          /*----- History alarm window -----*/
        case wiAlarmHistory:
          Rout::almDispMode = TRUE;
          actWindow = historyForm;
          break;

          /*----- Actual alarm window -----*/
        case wiAlarmActual:
          Rout::almDispMode = FALSE;
          actWindow = historyForm;
          break;

          /*----- Trending / charts window -----*/
        case wiTrending:
          //              actWindow = trendForm;
          break;

          /*-----  Pre calibration window -----*/
        case wiMaterialCalib:
          prodForm->SetDisplayState(mvcDSpreCal);
          actWindow = prodForm;
          break;

          /*-----  Online calibration window -----*/
        case wiMaterialCalibOnline:
          actWindow = learnOnlineForm;
          break;

          /*-----  Home window -----*/
        case wiHome:

          m_ui->btSelect->setEnabled(false);                                  /* Disable select button */

          /*----- Switch to homescreen for the device type -----*/
          switch (glob->sysCfg[idx].deviceType) {

          case mcdtBalance:
          case mcdtBalanceHO:
          case mcdtBalanceRegrind:
          case mcdtBalanceHORegrind:
            prodForm->SetDisplayState(mvcDSminimum);
            actWindow = prodForm;
            break;

          case mcdtWeight:
            Rout::mcWeightEditModeActive = false;
            actWindow = mcWeightForm;
            break;
          }
          break;

        case wiLevels:

          /*----- Switch to level screen for the device type -----*/
          switch (glob->sysCfg[idx].deviceType) {

          case mcdtBalance:
            actWindow = fillSysForm;
            break;

          case mcdtWeight:
          case mcdtBalanceHO:
            Rout::mcWeightEditModeActive = true;                            /* Set level edit mode */
            actWindow = mcWeightForm;
            break;
          }
          break;

        case wiAdvLevels:
          actWindow = advFillSysForm;
          break;

        case wiTolerances:
          actWindow = tolerancesForm;
          break;

        case wiMcWeightSettings:
          actWindow = mcWeightSettingsFrm;
          break;

        case wiCalibSelect:
          actWindow = calibSelectForm;
          break;

        case wiAdvConfig2:
          actWindow = advConfig2Form;
          break;

        case wiSysCalib:
          actWindow = sysCalibForm;
          break;

        case wiPrime:
          primeForm->move(((this->width()/2)-(primeForm->width()/2)),((this->height()/2) - ((primeForm->height()/2)+80)));
          actWindow = primeForm;
          break;

        case wiUnitSelect:
          Rout::homeScreenMultiUnitActive = MULTI_SCR_MODE_SELECT;            /* Setup multi unit screen select mode */
          actWindow = unitSelForm;
          break;

        case wiHomeMultiUnit:
          m_ui->btSelect->setEnabled(false);                                  /* Disable select button */
          Rout::homeScreenMultiUnitActive = MULTI_SCR_MODE_STATUS;             /* Setup multi unit screen status mode */
          actWindow = unitSelForm;
          break;

        case wiUnitModeConfig:
          Rout::homeScreenMultiUnitActive = MULTI_SCR_MODE_CONFIG;            /* Setup multi unit screen config mode */
          actWindow = unitSelForm;
          break;

        case wiSplash:
          break;

        case wiMaterials:
          materialForm->SetCallerWindow((eWindowIdx)calWindow);
          actWindow = materialForm;
          break;

        case wiWeightCheck:
          actWindow = weightCheckForm;
          break;

        case wiRcpSelect:
          actWindow = recipeSelectForm;
          break;

        case wiRcpConfig:
          actWindow = recipeConfigForm;
          /*----- If this window has been called from MaterialSelectFrm, don't initialize the recipe. -----*/
          if (calWindow != wiMaterials) {
            recipeConfigForm->SetRecipe(glob->selectedRecipe);
          }
          break;

        case wiIpEditor:
          actWindow = ipEditorForm;
          break;

        case wiSysSettings:
          actWindow = settingsForm;
          break;

        case wiBalanceDiag:
          actWindow = balanceDiagForm;
          break;

        case wiManualIo:
          actWindow = superVisorForm;
          break;

        case wiUSB:
          if(glob->usbStick->UsbPresent()) {
            actWindow = usbSelectForm;
          }
          else {
            DisplayMessage(-1,0,tr("USB Stick not present !"),msgtConfirm);
          }
          break;

        case wiUSBTransfer:
          /*----- USB Transfer -----*/
          if (calWindow == 0) {
            /*----- Transfer mode to controller -----*/
            usbTransForm->UsbTransferMode(etdUsbToCtrl);
          }
          else {
            /*----- Transfer mode to USB -----*/
            usbTransForm->UsbTransferMode(etdCtrlToUsb);
          }
          actWindow = usbTransForm;
          break;

        case wiAlarmConfig:
          actWindow = alarmConfigForm;
          break;

        case wiConsumption:
          actWindow = consumptionForm;
          break;

        case wiRegrindConfig:
          actWindow = regrindConfigForm;
          break;

        default:
          actWindow = 0;
          break;
        }

        if (actWindow != 0) {
          FoldMenu();                                                           /* Fold back side menu */

          actWindow->show();
          actWindow->raise();                                                   /* Push actWindow to background */

          /*----- Setup status image of the home button -----*/
          if (((glob->mstCfg.slaveCount == 0) && (windowNr == wiHome)) ||
              ((glob->mstCfg.slaveCount >  0) && (windowNr == wiHomeMultiUnit))) {
            /*----- Home window active , disable "home" button -----*/
            m_ui->btHome->setEnabled(false);
          }
          else {
            /*----- Home window not active , enable "home" button -----*/
            m_ui->btHome->setEnabled(true);
          }

          /*----- Disable on/Off button when profibus is enabled -----*/
          m_ui->btOnOff->setEnabled(!glob->mstCfg.profibusEnb);
        }
      }
    }
  }
}


void MainMenu::BackClicked()
{
  /*---------------------*/
  /* Back button pressed */
  /*---------------------*/
  /* Check backlit, if active turn it on and ignore event */
  if (Rout::BacklitOn()) {
    return;
  }

  /*----- Detect material calibration form active -----*/
  if (actWindowIdx == wiMaterialCalib) {
    if (prodForm->curState >= mvcDSpreCal) {
      if (glob->sysSts[glob->ActUnitIndex()].preCalibration.calib.sts == calsIdle) {
        prodForm->ResetCalibValues();
        DisplayWindow(wiHome);
      }
      /*----- No back key action when calibration is active -----*/
      else {
        return;
      }
    }
  }

  /*----- Display previous window from back list -----*/
  if (windowDepth > 0) {
    DisplayWindow(windowBackList[--windowDepth],wiBack);
  }
  else {
    DisplayWindow(wiHome);
  }
}


void MainMenu::btMenuClicked()
{
  /*---------------------*/
  /* Menu button clicked */
  /*---------------------*/
  /* Check backlit, if active turn it on and ignore event */
  if (Rout::BacklitOn()) {
    return;
  }

  if ((glob->calStruct.sts <= calsIdle) && (glob->sysSts[glob->ActUnitIndex()].preCalibration.calib.sts == calsIdle)) {
    emit MenuClicked();
  }
}


void MainMenu::HomeClicked()
{
  /*----------------------------------*/
  /* Home button clicked              */
  /* Display production status window */
  /*----------------------------------*/
  bool singleUnit;

  /* Check backlit, if active turn it on and ignore event */
  if (Rout::BacklitOn()) {
    return;
  }

  /*----- Detect single unit or twin mode mode -----*/
  singleUnit = (Rout::MultiUnitHomeScreen() == false);

  if ((glob->sysSts[glob->ActUnitIndex()].preCalibration.calib.sts == calsIdle) &&
      (glob->calStruct.sts == calsIdle)) {
    if (((actWindow == prodForm) && (!prodForm->calibVisible())) ||
        (actWindow == unitSelForm)) {

      if (singleUnit) {
        DisplayWindow(wiHome);
      }
      else {
        DisplayWindow(wiHomeMultiUnit);
      }
    }
    else {
      if (actWindow != unitSelForm) {
        if (singleUnit) {
          DisplayWindow(wiHome);
        }
        else {
          DisplayWindow(wiHomeMultiUnit);
        }
      }
    }
  }
  FoldMenu();
}


void MainMenu::AlarmClicked()
{
  /*----------------------*/
  /* Alarm button pressed */
  /*----------------------*/
  /* Check backlit, if active turn it on and ignore event */
  if (Rout::BacklitOn()) {
    return;
  }

  if ((glob->sysSts[glob->ActUnitIndex()].preCalibration.calib.sts == calsIdle) &&
      (glob->calStruct.sts == calsIdle)) {
    DisplayWindow(wiAlarmHistory);
  }
  FoldMenu();
}


void MainMenu::PrimeClicked()
{

  /* Check backlit, if active turn it on and ignore event */
  if (Rout::BacklitOn()) {
    return;
  }

  if (glob->mstSts.primeAvailable) {
    if ((glob->sysSts[glob->ActUnitIndex()].preCalibration.calib.sts == calsIdle) &&
        (glob->calStruct.sts == calsIdle)) {
      ((MainMenu*)(glob->menu))->DisplayWindow(wiPrime);
    }
  }
  FoldMenu();
}


void MainMenu::OnOffClicked()
{
  /*-----------------------*/
  /* On/off button clicked */
  /*-----------------------*/

  /*----- Backlit on, but handle on/off command -----*/
  Rout::BacklitOn();

  bool calBusy = false;
  /*----- Check if none of the units is calibrating -----*/
  for (int i=0;i<(glob->mstCfg.slaveCount+1);i++) {
    if (!((glob->sysSts[i].preCalibration.calib.sts == calsIdle) &&
          (glob->calStruct.sts == calsIdle))) {
      calBusy = true;
    }
  }

  /*----- Start or stop the units (Toggle start status) -----*/
  if (!calBusy) {
    glob->MstSts_SetSysStart(!(glob->mstSts.sysStart));

    /*----- Update on/off button -----*/
    if (glob->mstSts.sysStart) {
      /*----- Unit is on, setup unit off button image -----*/
      glob->SetButtonImage(m_ui->btOnOff,itMenu,mnButtonOff);

      /*----- Switch to home screen -----*/
      if (!Rout::MultiUnitHomeScreen()) {
        ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
      }
      else {
        ((MainMenu*)(glob->menu))->DisplayWindow(wiHomeMultiUnit);              /* Multi component system */
      }
    }
    else {
      /*----- Unit is off, setup unit on button image -----*/
      glob->SetButtonImage(m_ui->btOnOff,itMenu,mnButtonOn);
    }

    if (glob->mstCfg.autoStart) {
      /*----- Update on/off status in production data file -----*/
      Rout::prodData.onOff = glob->mstSts.sysStart;
      Rout::WriteProdData();
    }
  }
  FoldMenu();
}


void MainMenu::SelectClicked()
{
  /*---------------------*/
  /* Select unit clicked */
  /*---------------------*/
  if ((glob->sysSts[glob->ActUnitIndex()].preCalibration.calib.sts == calsIdle) &&
      (glob->calStruct.sts == calsIdle)) {

    /*----- Display unit select form -----*/
    ((MainMenu*)(glob->menu))->DisplayWindow(wiUnitSelect);
  }
  /* Check backlit, if active turn it on and ignore event */
  if (Rout::BacklitOn()) {
    return;
  }
}


void MainMenu::SysActiveChanged(bool act)
{
  if (act) {
    glob->SetButtonImage(m_ui->btOnOff,itMenu,mnButtonOff);
  }
  else {
    glob->SetButtonImage(m_ui->btOnOff,itMenu,mnButtonOn);
  }
}


void MainMenu::PrimeActiveChanged(bool sts,int idx)
{
  emit PrimeActiveChange(sts,idx);
}


void MainMenu::TimeChanged(const QDateTime &date)
{
  QString s;
  s = date.time().toString(FORMAT_TIME_SHORT) + "\n" + date.date().toString(FORMAT_DATE_SHORT);
  m_ui->lbDateTime->setText(s);
}


void MainMenu::McStatusChanged(eMCStatus sts, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    if (sts == mcsOff) {
      glob->mstSts.primeAvailable = true;
      DisplayPrime(true);
    }
    else {
      if (glob->mstSts.primeAvailable != false) {
        glob->mstSts.primeAvailable = false;
        DisplayPrime(false);
      }
    }
  }
}


void MainMenu::ErrorResultReceived(int code)
{
  /*-------------------------------------------------------------------------*/
  /* Is connected to  SIGNAL(MessageResult(int)) used in popupSetConfirmForm */
  /* And connected to SIGNAL(ButtonClicked(int)) used in the errorForm       */
  /*-------------------------------------------------------------------------*/
  emit ErrorResult(code);
}

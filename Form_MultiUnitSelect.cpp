/*-----------------------------*/
/* Multi unit selection screen */
/*-----------------------------*/

#include "Form_MultiUnitSelect.h"
#include "ui_Form_MultiUnitSelect.h"
#include "Form_MainMenu.h"
#include "Rout.h"
#include "Com.h"


QObject *editObject;                            /* Active edit object */
float val1,val2,val3;

MultiUnitSelectFrm::MultiUnitSelectFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::MultiUnitSelectFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);

  unitCnt = 0;
  unitIdx = -1;

  /*----- Clear unut array -----*/
  for (int i=0;i<CFG_MAX_UNIT_COUNT;i++) {
    unitList[i] = 0;
  }
}


MultiUnitSelectFrm::~MultiUnitSelectFrm()
{
  /*------------*/
  /* Destructor */
  /*------------*/
  //  ClearItems();
  delete numInput;
  delete ui;
}


eSystemMode MultiUnitSelectFrm::SystemMode(void) {
  /*-----------------------*/
  /* Fetch the system mode */
  /*-----------------------*/

  switch(glob->mstCfg.mode) {

  /*----- Injection moulding -----*/
  case modInject:

    switch(glob->mstCfg.inpType) {

    /*----- Injection, Relay -----*/
    case ttRelay:
      return(sysModeInjectRelay);
      break;

      /*----- Injection, timer -----*/
    case ttTimer:
      return(sysModeInjectTimer);
    }
    break;

    /*----- Extrusion -----*/
  case modExtrude:
    switch(glob->mstCfg.inpType) {

    /*----- Extrusion, relay -----*/
    case ttRelay:
      return(sysModeExtrusionRelay);
      break;

      /*----- Tacho -----*/
    case ttTacho:
      return(sysModeExtrusionTacho);
      break;
    }
  }
}



void MultiUnitSelectFrm::changeEvent(QEvent *e)
{
  /*--------------*/
  /* Change event */
  /*--------------*/
  QWidget::changeEvent(e);
  switch (e->type()) {
  case QEvent::LanguageChange:
    ui->retranslateUi(this);
    break;
  default:
    break;
  }
}


void MultiUnitSelectFrm::resizeEvent(QResizeEvent *e)
{
  /*--------------*/
  /* Resize event */
  /*--------------*/
  QWidget::resizeEvent(e);
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
}


void MultiUnitSelectFrm::SetInfoText(void) {
  /*---------------------------------------*/
  /* Update static text in the info fields */
  /*---------------------------------------*/

  /*----- Enable black text color when info fields are disabled -----*/
  ui->edInfo1->setStyleSheet(QString::fromUtf8("QLineEdit:disabled"
                                               "{ color: black }"));

  ui->edInfo2->setStyleSheet(QString::fromUtf8("QLineEdit:disabled"
                                               "{ color: black }"));

  ui->edInfo3->setStyleSheet(QString::fromUtf8("QLineEdit:disabled"
                                               "{ color: black }"));



  switch(SystemMode()) {


  /*----- Inj. Relay -----*/
  case sysModeInjectRelay :
    ui->edInfo1->setVisible(true);
    ui->edInfo2->setVisible(true);
    ui->edInfo3->setVisible(true);
    ui->line1->setVisible(false);
    ui->line2->setVisible(false);

    ui->lbedInfo1->setText(tr("Shot weight"));
    ui->lbedInfo2->setText(tr("Relay time"));
    ui->lbedInfo3->setText(tr("Act time"));

    ui->edInfo1->setEnabled(true);
    ui->edInfo2->setEnabled(false);
    ui->edInfo3->setEnabled(false);

    val1 = glob->sysCfg[0].injection.shotWeight;
    break;

    /*----- Inj. Timer -----*/
  case sysModeInjectTimer:
    ui->edInfo1->setVisible(true);
    ui->edInfo2->setVisible(true);
    ui->edInfo3->setVisible(true);
    ui->line1->setVisible(false);
    ui->line2->setVisible(false);

    ui->lbedInfo1->setText(tr("Shot weight"));
    ui->lbedInfo2->setText(tr("Set time"));
    ui->lbedInfo3->setText(tr("Act time"));

    ui->edInfo1->setEnabled(true);
    ui->edInfo2->setEnabled(true);
    ui->edInfo3->setEnabled(false);

    val1 = glob->sysCfg[0].injection.shotWeight;
    val2 = glob->sysCfg[0].injection.shotTime;
    break;


    /*----- Ext. Relay -----*/
  case sysModeExtrusionRelay :
    ui->edInfo1->setVisible(false);
    ui->edInfo2->setVisible(false);
    ui->edInfo3->setVisible(true);
    ui->line1->setVisible(Rout::McWeightPresent());
    ui->line2->setVisible(false);

    ui->lbedInfo3->setText(tr("Ext Cap set"));

    val3 = glob->sysCfg[0].extrusion.extCapacity;
    break;

    /*----- Ext. Tacho -----*/
  case sysModeExtrusionTacho :
    ui->edInfo1->setVisible(false);
    ui->edInfo2->setVisible(true);
    ui->edInfo3->setVisible(true);
    ui->line1->setVisible(Rout::McWeightPresent());
    ui->line2->setVisible(false);

    ui->lbedInfo2->setText(tr("Ext Act"));
    ui->lbedInfo3->setText(tr("Tacho Act"));

    val3 = glob->sysCfg[0].extrusion.extCapacity;
    break;
  }

  /*----- Display the label fields depending on the edit fields en-/disabled -----*/
  ui->lbedInfo1->setVisible(ui->edInfo1->isVisible());
  ui->lbedInfo2->setVisible(ui->edInfo2->isVisible());
  ui->lbedInfo3->setVisible(ui->edInfo3->isVisible());
}


void MultiUnitSelectFrm::UpdateProcesData(void) {
  /*-------------------------------------*/
  /* Update dynamic data the info fields */
  /*-------------------------------------*/
  switch(SystemMode()) {

  /*----- Injection, relay -----*/
  case sysModeInjectRelay :
    ui->edInfo1->setText(QString::number(glob->sysCfg[0].injection.shotWeight,glob->formats.ShotWeightFmt.format.toAscii(),glob->formats.ShotWeightFmt.precision) + QString(" ")+ glob->shotUnits);
    ui->edInfo2->setText(QString::number(glob->sysSts[0].injection.meteringAct,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) + QString(" ")+ glob->timerUnits);
    ui->edInfo3->setText(QString::number(glob->sysSts[0].injection.shotTimeAct,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) + QString(" ")+ glob->timerUnits);
    break;

    /*----- Injection, timer -----*/
  case sysModeInjectTimer :
    ui->edInfo1->setText(QString::number(glob->sysCfg[0].injection.shotWeight,glob->formats.ShotWeightFmt.format.toAscii(),glob->formats.ShotWeightFmt.precision) + QString(" ")+ glob->shotUnits);
    ui->edInfo2->setText(QString::number(glob->sysCfg[0].injection.shotTime,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) + QString(" ")+ glob->timerUnits);
    ui->edInfo3->setText(QString::number(glob->sysSts[0].injection.shotTimeAct,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) + QString(" ")+ glob->timerUnits);
    break;

    /*----- Extrusion -----*/
  case sysModeExtrusionRelay :
    ui->edInfo3->setText(QString::number(glob->sysCfg[0].extrusion.extCapacity,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision) + QString(" ")+ glob->extrUnits);
    break;

    /*----- Tacho -----*/
  case sysModeExtrusionTacho :
    ui->edInfo2->setText(QString::number(glob->sysSts[0].extrusion.extCapacityAct,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision) + QString(" ")+ glob->extrUnits);
    ui->edInfo3->setText(QString::number(glob->sysSts[0].extrusion.tachoAct,glob->formats.TachoFmt.format.toAscii(),glob->formats.TachoFmt.precision) + QString(" ")+ glob->tachoUnits);
    break;
  }
}


void MultiUnitSelectFrm::showEvent(QShowEvent *e)
{
  /*------------*/
  /* Show event */
  /*------------*/
  QWidget::showEvent(e);

  /*----- Auto start unit -----*/
  Rout::AutoStart();

  /*----- Connect data refresh to msg received -----*/
  connect(glob,SIGNAL(ComReceived(tMsgControlUnit,int)),this,SLOT(ComMsgReceived(tMsgControlUnit,int)));

  switch (Rout::homeScreenMultiUnitActive) {

  /*----- Multi unit select screen -----*/
  case(MULTI_SCR_MODE_SELECT) :
    ui->lbText->setFont(*(glob->captionFont));
    ui->lbText->setText(tr("Please select a unit"));
    ui->lbText->setAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
    ui->lbText->setVisible(true);
    ui->btRecipe->setVisible(false);
    ui->Settings->setVisible(false);
    break;


    /*----- Multi unit status screen -----*/
  case(MULTI_SCR_MODE_STATUS) :

    if (glob->mstCfg.rcpEnable) {

      /*----- Display recipe name and button -----*/
      RecipeChanged(glob->mstCfg.recipe);
      ui->lbText->setFont(*(glob->baseFont));
      ui->lbText->setVisible(true);
      ui->lbText->setAlignment(Qt::AlignRight|Qt::AlignVCenter);

      ui->btRecipe->setVisible(!Rout::keyLock);
    }
    else {
      ui->lbText->setVisible(false);
      ui->btRecipe->setVisible(false);
    }

    ui->Settings->setVisible(true);

#warning Disabled !!
    ui->Settings->setVisible(false);
    ui->btRecipe->setVisible(false);
    ui->lbText->setVisible(false);

    SetInfoText();
    break;

    break;

    /*----- Multi unit config screen -----*/
  case(MULTI_SCR_MODE_CONFIG) :
    ui->lbText->setFont(*(glob->captionFont));
    ui->lbText->setText(tr("Unit configuration"));
    ui->lbText->setAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
    ui->lbText->setVisible(true);
    ui->btRecipe->setVisible(false);
    ui->Settings->setVisible(false);
    break;
  }
}

void MultiUnitSelectFrm::hideEvent(QHideEvent *e)
/*--------------------*/
/* Hide event handler */
/*--------------------*/
{
  /*----- Dis-connect data refresh to msg received -----*/
  disconnect(glob,SIGNAL(ComReceived(tMsgControlUnit,int)),this,SLOT(ComMsgReceived(tMsgControlUnit,int)));

  QBaseWidget::hideEvent(e);
}




void MultiUnitSelectFrm::FormInit()
{
  /*-----------*/
  /* Form init */
  /*-----------*/

  QBaseWidget::ConnectKeyBeep();

  /*----- Create numeric entry -----*/
  numInput = new NumericInputFrm();

  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
  numInput->hide();
  numInput->FormInit();
  connect(numInput,SIGNAL(InputValue(float)),this,SLOT(ValueReceived(float)));

  InitItems();

  glob->SetButtonImage(ui->btRecipe,itConfig,cfgRcp_Menu);
  connect(ui->btRecipe,SIGNAL(clicked()),this,SLOT(RecipeClicked()));

  connect(glob,SIGNAL(MstCfg_SlaveCountChanged(int)),this,SLOT(SlaveCountChanged(int)));
  connect(glob,SIGNAL(SysSts_SysEnabledChanged(bool,int)),this,SLOT(SysActiveChanged(bool,int)));
  connect(glob,SIGNAL(SysSts_McWeightCapChanged(float,float,float,int)),this,SLOT(McWeightCapChanged(float,float,float,int)));
  connect(glob,SIGNAL(MstCfg_RecipeChanged(QString)),this,SLOT(RecipeChanged(QString)));

  /*----- Remove border around setting groupbox -----*/
  //  ui->Settings->setStyleSheet("border:0;");

  ui->edInfo1->installEventFilter(this);
  ui->edInfo2->installEventFilter(this);
  ui->edInfo3->installEventFilter(this);

  ui->edInfo1->setFont(*(glob->baseFont));
  ui->edInfo1->setStyleSheet("font-weight: bold;");

  ui->edInfo2->setFont(*(glob->baseFont));
  ui->edInfo2->setStyleSheet("font-weight: bold;");

  ui->edInfo3->setFont(*(glob->baseFont));
  ui->edInfo3->setStyleSheet("font-weight: bold;");

  ui->lbedInfo1->setFont(*(glob->baseFont));
  ui->lbedInfo2->setFont(*(glob->baseFont));
  ui->lbedInfo3->setFont(*(glob->baseFont));
}


void MultiUnitSelectFrm::ComMsgReceived(tMsgControlUnit msg,int idx)
{
  /*--------------------------------------------*/
  /* Comm message received, update dynamic data */
  /*--------------------------------------------*/
  UpdateProcesData();
}


void MultiUnitSelectFrm::InitItems()
{
  MultiComponentItemFrm * item;

  /* Get current number of controllers */
  int gbiUnits = glob->mstCfg.slaveCount+1;

  if (unitCnt < gbiUnits) {
    /* Create new items from last known count upto new count */
    for (int i = unitCnt; i<gbiUnits; i++) {
      item = new MultiComponentItemFrm(this);
      unitList[i] = item;
      ui->units->addWidget(item);
      item->FormInit();
      item->SetUnitIndex(i);
      connect(item,SIGNAL(ColPctClicked(int)),this,SLOT(ColPctClickReceived(int)));
    }
    unitCnt = gbiUnits;
  }
  else {
    if (unitCnt > gbiUnits) {
      // delete all items between new count and last count
      for (int i=gbiUnits;i<unitCnt;i++) {
        item = unitList[i];
        unitList[i] = 0;
        ui->units->removeWidget(item);
        delete item;
      }
      unitCnt = gbiUnits;
    }
  }
}


bool MultiUnitSelectFrm::eventFilter(QObject *o, QEvent *e)
{
  /*--------------*/
  /* Event filter */
  /*--------------*/
  float val,minVal,maxVal;

  int actUnit = glob->ActUnitIndex();
  bool num = false;

  if (e->type() == QEvent::MouseButtonPress) {

    /*----- Keylock not active -----*/
    if (!Rout::keyLock) {

      /*----- Edit info 1 -----*/
      if ((o == ui->edInfo1) && (ui->edInfo1->isEnabled())) {
        editObject = ui->edInfo1;
        val = val1;
        minVal = 0;
        maxVal = 100000;
        num = true;
      }

      /*----- Edit info 2 -----*/
      if ((o == ui->edInfo2) && (ui->edInfo2->isEnabled())) {
        editObject = ui->edInfo2;
        val = val2;
        minVal = 0;
        maxVal = 100000;
        num = true;
      }

      /*----- Edit info 3 -----*/
      if ((o == ui->edInfo3) && (ui->edInfo3->isEnabled())) {
        editObject = ui->edInfo3;
        val = val3;
        minVal = 0;
        maxVal = 100000;
        num = true;
      }

      if (num) {
        numInput->SetDisplay(true,1,val,minVal,maxVal);
        KeyBeep();
        numInput->show();
      }
    }
  }
  return QBaseWidget::eventFilter(o,e);
}


void MultiUnitSelectFrm::ClearItems()
{
  MultiComponentItemFrm * item;
  for (int i=0;i<CFG_MAX_UNIT_COUNT;i++) {
    if (unitList[i] != 0) {
      item = unitList[i];
      disconnect(item,SIGNAL(ColPctClicked(int)),this,SLOT(ColPctClickReceived(int)));
      delete unitList[i];
      unitList[i] = 0;
    }
  }
}


void MultiUnitSelectFrm::SlaveCountChanged(int)
{
  InitItems();
}


void MultiUnitSelectFrm::SysActiveChanged(bool,int)
{
  InitItems();
}


void MultiUnitSelectFrm::McWeightCapChanged(float cap, float capTotal, float capPctTotal, int idx)
{
  /*----------------------------------*/
  /* Event : McWeight capacity change */
  /*----------------------------------*/
  if (ui->line1->isVisible()) {
    ui->line1->setText(tr("Total cap: ")+QString::number(capTotal,'f',2) +QString(" ") +glob->extrUnits);
  }
}


void MultiUnitSelectFrm::ColPctClickReceived(int idx)
{
  unitIdx = idx;
  if (unitIdx >= 0) {
    numInput->SetDisplay(true,glob->formats.PercentageFmt.precision,glob->sysCfg[unitIdx].colPct,0,100);
    numInput->show();
  }
}


void MultiUnitSelectFrm::ValueReceived(float val)
{
  /*--------------------------*/
  /* Numerical value received */
  /*--------------------------*/

  switch(SystemMode()) {

  /*----- Injection, Relay -----*/
  case sysModeInjectRelay:
    /*----- Shot weight -----*/
    if (editObject == ui->edInfo1) {
      val1 = val;
      ui->edInfo1->setText(QString::number(val) + " gr");
      glob->SysCfg_SetShotWeight(val,0);
    }
    break;

    /*----- Injection, timer -----*/
  case sysModeInjectTimer:

    /*----- Shot weight -----*/
    if (editObject == ui->edInfo1) {
      val1 = val;
      ui->edInfo1->setText(QString::number(val) + " " + glob->shotUnits);
      glob->SysCfg_SetShotWeight(val,0);
    }

    /*----- Set time -----*/
    if (editObject == ui->edInfo2) {
      val2 = val;
      ui->edInfo2->setText(QString::number(val) + " " + glob->timerUnits);
      glob->SysCfg_SetShotTime(val2,0);
    }
    break;

    /*----- Extrusion -----*/
  case sysModeExtrusionRelay :
    /*----- Extruder capacity set -----*/
    if (editObject == ui->edInfo1) {
      val1 = val;
      ui->edInfo1->setText(QString::number(val,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision) + QString(" ")+glob->extrUnits);
      glob->SysCfg_SetExtCap(val1,0);
    }
    break;

    /*----- Tacho -----*/
  case sysModeExtrusionTacho :
    break;
  }
}


void MultiUnitSelectFrm::RecipeClicked(void) {
  /*-------------------------*/
  /* BUTTON : Recipe clicked */
  /*-------------------------*/
  RecipeStruct rcp;

  /* Check backlit, if active turn it on and ignore event */
  if (Rout::BacklitOn()) {
    return;
  }

  if (recipeChanged) {
    ((MainMenu*)(glob->menu))->recipeConfigForm->SaveRecipe(&rcp,false,true);
    ResetRecipeDataChanged();
  }
  else {
    emit RecipeSelected(glob->mstCfg.recipe);
  }
}


void MultiUnitSelectFrm::RecipeChanged(const QString &rcp)
/*-------------------------------*/
/* SIGNAL : MstCfg_RecipeChanged */
/*-------------------------------*/
{
  QString ext = QString(RECIPE_EXTENSION);
  QString tmpRcp;
  tmpRcp = rcp.left(rcp.length()-ext.length());
  ui->lbText->setText("Recipe : "+ tmpRcp);
}


void MultiUnitSelectFrm::SetRecipeDataChanged(void) {
  /*----------------------------------*/
  /* Set the recipe data changed mode */
  /*----------------------------------*/
  glob->SetButtonImage(ui->btRecipe,itConfig,cfgRcp_Save_Menu);
  recipeChanged = true;
}


void MultiUnitSelectFrm::ResetRecipeDataChanged(void) {
  /*-------------------------------*/
  /* Reset the recipe changed mode */
  /*-------------------------------*/
  glob->SetButtonImage(ui->btRecipe,itConfig,cfgRcp_Menu);
  recipeChanged = false;
}




#include "Form_RecipeCheckItem.h"
#include "ui_Form_RecipeCheckItem.h"
#include "Rout.h"

RecipeCheckItemFrm::RecipeCheckItemFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::RecipeCheckItemFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);
}


RecipeCheckItemFrm::~RecipeCheckItemFrm()
{
  delete ui;
}


void RecipeCheckItemFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  ui->lbDevId->setFont(*(glob->baseFont));
  ui->lbRcpTool->setFont(*(glob->baseFont));
}


void RecipeCheckItemFrm::SetDevID(const QString &id)
{
  ui->lbDevId->setText(id);
}


void RecipeCheckItemFrm::SetCurDosTool(eDosTool tool)
{
  glob->SetLabelImage(ui->lbCurTool,itDosTool,tool);
}


void RecipeCheckItemFrm::SetRcpDosTool(eDosTool tool)
{
  if (tool != dtCount) {
    glob->SetLabelImage(ui->lbRcpTool,itDosTool,tool);
  }
  else {
    /*---- file not found ----*/
    ui->lbRcpTool->clear();
    ui->lbRcpTool->setText(tr("Material file missing!"));
    ui->lbCurTool->setVisible(false);
    ui->lbRcpTool->setFixedWidth(250);
  }
}

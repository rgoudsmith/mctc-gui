/********************************************************************************
** Form generated from reading UI file 'Form_OverlayMenu.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_OVERLAYMENU_H
#define UI_FORM_OVERLAYMENU_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OverlayMenu
{
public:
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QPushButton *btLogin;
    QPushButton *btConfig;
    QPushButton *btAlarm;
    QPushButton *btConsumption;
    QPushButton *btLearn;
    QPushButton *btUsb;
    QPushButton *btKeyLock;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *OverlayMenu)
    {
        if (OverlayMenu->objectName().isEmpty())
            OverlayMenu->setObjectName(QString::fromUtf8("OverlayMenu"));
        OverlayMenu->setEnabled(true);
        OverlayMenu->resize(98, 555);
        OverlayMenu->setCursor(QCursor(Qt::BlankCursor));
        OverlayMenu->setWindowTitle(QString::fromUtf8("OverlayMenu"));
        verticalLayout = new QVBoxLayout(OverlayMenu);
        verticalLayout->setSpacing(5);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(2, 0, 0, 0);
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        groupBox = new QGroupBox(OverlayMenu);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        groupBox->setMinimumSize(QSize(0, 0));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox{border: 1px solid black; border-radius:3px; background:rgb(127,127,130);}"));
        groupBox->setTitle(QString::fromUtf8(""));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(2, 5, 2, 5);
        btLogin = new QPushButton(groupBox);
        btLogin->setObjectName(QString::fromUtf8("btLogin"));
        btLogin->setMinimumSize(QSize(90, 70));
        btLogin->setMaximumSize(QSize(90, 70));
        btLogin->setText(QString::fromUtf8("LOGIN"));

        verticalLayout_2->addWidget(btLogin);

        btConfig = new QPushButton(groupBox);
        btConfig->setObjectName(QString::fromUtf8("btConfig"));
        btConfig->setMinimumSize(QSize(90, 70));
        btConfig->setMaximumSize(QSize(90, 70));
        btConfig->setText(QString::fromUtf8("CONFIG /\n"
"SETTINGS"));

        verticalLayout_2->addWidget(btConfig);

        btAlarm = new QPushButton(groupBox);
        btAlarm->setObjectName(QString::fromUtf8("btAlarm"));
        btAlarm->setMinimumSize(QSize(90, 70));
        btAlarm->setMaximumSize(QSize(90, 70));
        btAlarm->setText(QString::fromUtf8("HISTORY /\n"
"ALARMS"));

        verticalLayout_2->addWidget(btAlarm);

        btConsumption = new QPushButton(groupBox);
        btConsumption->setObjectName(QString::fromUtf8("btConsumption"));
        btConsumption->setMinimumSize(QSize(90, 70));
        btConsumption->setMaximumSize(QSize(90, 70));
        btConsumption->setText(QString::fromUtf8("CONS"));

        verticalLayout_2->addWidget(btConsumption);

        btLearn = new QPushButton(groupBox);
        btLearn->setObjectName(QString::fromUtf8("btLearn"));
        btLearn->setMinimumSize(QSize(90, 70));
        btLearn->setMaximumSize(QSize(90, 70));
        btLearn->setText(QString::fromUtf8("LEARN"));

        verticalLayout_2->addWidget(btLearn);

        btUsb = new QPushButton(groupBox);
        btUsb->setObjectName(QString::fromUtf8("btUsb"));
        btUsb->setMinimumSize(QSize(90, 70));
        btUsb->setMaximumSize(QSize(90, 70));
        btUsb->setText(QString::fromUtf8("USB"));

        verticalLayout_2->addWidget(btUsb);

        btKeyLock = new QPushButton(groupBox);
        btKeyLock->setObjectName(QString::fromUtf8("btKeyLock"));
        btKeyLock->setMinimumSize(QSize(90, 70));
        btKeyLock->setMaximumSize(QSize(90, 70));
        btKeyLock->setText(QString::fromUtf8("KEY-LOCK"));

        verticalLayout_2->addWidget(btKeyLock);

        verticalSpacer = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer);


        verticalLayout->addWidget(groupBox);


        retranslateUi(OverlayMenu);

        QMetaObject::connectSlotsByName(OverlayMenu);
    } // setupUi

    void retranslateUi(QWidget *OverlayMenu)
    {
        Q_UNUSED(OverlayMenu);
    } // retranslateUi

};

namespace Ui {
    class OverlayMenu: public Ui_OverlayMenu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_OVERLAYMENU_H

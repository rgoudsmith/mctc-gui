/********************************************************************************
** Form generated from reading UI file 'Form_MultiUnitSelect.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_MULTIUNITSELECT_H
#define UI_FORM_MULTIUNITSELECT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MultiUnitSelectFrm
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *info;
    QSpacerItem *horizontalSpacer_2;
    QLabel *lbText;
    QSpacerItem *horizontalSpacer;
    QPushButton *btRecipe;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *units;
    QFrame *Settings;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *QVBoxEdInfo1;
    QSpacerItem *verticalSpacer_9;
    QLabel *lbedInfo1;
    QSpacerItem *verticalSpacer_8;
    QLineEdit *edInfo1;
    QSpacerItem *verticalSpacer_7;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *QVBoxEdInfo2;
    QSpacerItem *verticalSpacer_4;
    QLabel *lbedInfo2;
    QSpacerItem *verticalSpacer_5;
    QLineEdit *edInfo2;
    QSpacerItem *verticalSpacer_6;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *QVBoxEdInfo3;
    QSpacerItem *verticalSpacer;
    QLabel *lbedInfo3;
    QSpacerItem *verticalSpacer_2;
    QLineEdit *edInfo3;
    QSpacerItem *verticalSpacer_3;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *QVBoxProduction;
    QSpacerItem *verticalSpacer_10;
    QLabel *line1;
    QSpacerItem *verticalSpacer_11;
    QLabel *line2;
    QSpacerItem *verticalSpacer_12;

    void setupUi(QWidget *MultiUnitSelectFrm)
    {
        if (MultiUnitSelectFrm->objectName().isEmpty())
            MultiUnitSelectFrm->setObjectName(QString::fromUtf8("MultiUnitSelectFrm"));
        MultiUnitSelectFrm->resize(786, 505);
        MultiUnitSelectFrm->setCursor(QCursor(Qt::BlankCursor));
        MultiUnitSelectFrm->setWindowTitle(QString::fromUtf8("Form"));
        horizontalLayoutWidget = new QWidget(MultiUnitSelectFrm);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 0, 771, 72));
        info = new QHBoxLayout(horizontalLayoutWidget);
        info->setObjectName(QString::fromUtf8("info"));
        info->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        info->addItem(horizontalSpacer_2);

        lbText = new QLabel(horizontalLayoutWidget);
        lbText->setObjectName(QString::fromUtf8("lbText"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lbText->sizePolicy().hasHeightForWidth());
        lbText->setSizePolicy(sizePolicy);
        lbText->setSizeIncrement(QSize(0, 0));
        QFont font;
        font.setPointSize(22);
        font.setBold(true);
        font.setWeight(75);
        lbText->setFont(font);
        lbText->setLayoutDirection(Qt::LeftToRight);
        lbText->setText(QString::fromUtf8("TextLabel"));
        lbText->setAlignment(Qt::AlignCenter);

        info->addWidget(lbText);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        info->addItem(horizontalSpacer);

        btRecipe = new QPushButton(horizontalLayoutWidget);
        btRecipe->setObjectName(QString::fromUtf8("btRecipe"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btRecipe->sizePolicy().hasHeightForWidth());
        btRecipe->setSizePolicy(sizePolicy1);
        btRecipe->setMinimumSize(QSize(140, 70));
        btRecipe->setMaximumSize(QSize(140, 70));
        btRecipe->setText(QString::fromUtf8("Recipe"));
        btRecipe->setShortcut(QString::fromUtf8(""));

        info->addWidget(btRecipe);

        horizontalLayoutWidget_2 = new QWidget(MultiUnitSelectFrm);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 80, 771, 281));
        units = new QHBoxLayout(horizontalLayoutWidget_2);
        units->setObjectName(QString::fromUtf8("units"));
        units->setContentsMargins(0, 0, 0, 0);
        Settings = new QFrame(MultiUnitSelectFrm);
        Settings->setObjectName(QString::fromUtf8("Settings"));
        Settings->setGeometry(QRect(10, 380, 771, 111));
        verticalLayoutWidget_3 = new QWidget(Settings);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(270, 0, 158, 101));
        QVBoxEdInfo1 = new QVBoxLayout(verticalLayoutWidget_3);
        QVBoxEdInfo1->setObjectName(QString::fromUtf8("QVBoxEdInfo1"));
        QVBoxEdInfo1->setContentsMargins(0, 0, 0, 0);
        verticalSpacer_9 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        QVBoxEdInfo1->addItem(verticalSpacer_9);

        lbedInfo1 = new QLabel(verticalLayoutWidget_3);
        lbedInfo1->setObjectName(QString::fromUtf8("lbedInfo1"));
        QFont font1;
        font1.setPointSize(16);
        lbedInfo1->setFont(font1);
        lbedInfo1->setText(QString::fromUtf8("TextLabel"));
        lbedInfo1->setAlignment(Qt::AlignCenter);

        QVBoxEdInfo1->addWidget(lbedInfo1);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        QVBoxEdInfo1->addItem(verticalSpacer_8);

        edInfo1 = new QLineEdit(verticalLayoutWidget_3);
        edInfo1->setObjectName(QString::fromUtf8("edInfo1"));
        edInfo1->setMinimumSize(QSize(0, 50));
        QFont font2;
        font2.setPointSize(15);
        font2.setBold(true);
        font2.setWeight(75);
        edInfo1->setFont(font2);
        edInfo1->setFrame(true);
        edInfo1->setAlignment(Qt::AlignCenter);

        QVBoxEdInfo1->addWidget(edInfo1);

        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        QVBoxEdInfo1->addItem(verticalSpacer_7);

        verticalLayoutWidget_2 = new QWidget(Settings);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(440, 0, 158, 101));
        QVBoxEdInfo2 = new QVBoxLayout(verticalLayoutWidget_2);
        QVBoxEdInfo2->setObjectName(QString::fromUtf8("QVBoxEdInfo2"));
        QVBoxEdInfo2->setContentsMargins(0, 0, 0, 0);
        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        QVBoxEdInfo2->addItem(verticalSpacer_4);

        lbedInfo2 = new QLabel(verticalLayoutWidget_2);
        lbedInfo2->setObjectName(QString::fromUtf8("lbedInfo2"));
        lbedInfo2->setFont(font1);
        lbedInfo2->setText(QString::fromUtf8("TextLabel"));
        lbedInfo2->setAlignment(Qt::AlignCenter);

        QVBoxEdInfo2->addWidget(lbedInfo2);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        QVBoxEdInfo2->addItem(verticalSpacer_5);

        edInfo2 = new QLineEdit(verticalLayoutWidget_2);
        edInfo2->setObjectName(QString::fromUtf8("edInfo2"));
        edInfo2->setMinimumSize(QSize(0, 50));
        edInfo2->setFont(font2);
        edInfo2->setAlignment(Qt::AlignCenter);

        QVBoxEdInfo2->addWidget(edInfo2);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        QVBoxEdInfo2->addItem(verticalSpacer_6);

        verticalLayoutWidget = new QWidget(Settings);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(610, 0, 158, 101));
        QVBoxEdInfo3 = new QVBoxLayout(verticalLayoutWidget);
        QVBoxEdInfo3->setObjectName(QString::fromUtf8("QVBoxEdInfo3"));
        QVBoxEdInfo3->setContentsMargins(0, 0, 0, 0);
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        QVBoxEdInfo3->addItem(verticalSpacer);

        lbedInfo3 = new QLabel(verticalLayoutWidget);
        lbedInfo3->setObjectName(QString::fromUtf8("lbedInfo3"));
        lbedInfo3->setFont(font1);
        lbedInfo3->setText(QString::fromUtf8("TextLabel"));
        lbedInfo3->setAlignment(Qt::AlignCenter);

        QVBoxEdInfo3->addWidget(lbedInfo3);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        QVBoxEdInfo3->addItem(verticalSpacer_2);

        edInfo3 = new QLineEdit(verticalLayoutWidget);
        edInfo3->setObjectName(QString::fromUtf8("edInfo3"));
        edInfo3->setMinimumSize(QSize(0, 50));
        edInfo3->setFont(font2);
        edInfo3->setAlignment(Qt::AlignCenter);

        QVBoxEdInfo3->addWidget(edInfo3);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        QVBoxEdInfo3->addItem(verticalSpacer_3);

        verticalLayoutWidget_4 = new QWidget(Settings);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(0, 0, 261, 101));
        QVBoxProduction = new QVBoxLayout(verticalLayoutWidget_4);
        QVBoxProduction->setObjectName(QString::fromUtf8("QVBoxProduction"));
        QVBoxProduction->setContentsMargins(0, 0, 0, 0);
        verticalSpacer_10 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        QVBoxProduction->addItem(verticalSpacer_10);

        line1 = new QLabel(verticalLayoutWidget_4);
        line1->setObjectName(QString::fromUtf8("line1"));
        line1->setFont(font1);

        QVBoxProduction->addWidget(line1);

        verticalSpacer_11 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        QVBoxProduction->addItem(verticalSpacer_11);

        line2 = new QLabel(verticalLayoutWidget_4);
        line2->setObjectName(QString::fromUtf8("line2"));
        line2->setFont(font1);

        QVBoxProduction->addWidget(line2);

        verticalSpacer_12 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        QVBoxProduction->addItem(verticalSpacer_12);


        retranslateUi(MultiUnitSelectFrm);

        QMetaObject::connectSlotsByName(MultiUnitSelectFrm);
    } // setupUi

    void retranslateUi(QWidget *MultiUnitSelectFrm)
    {
        line1->setText(QApplication::translate("MultiUnitSelectFrm", "Total cap :", 0, QApplication::UnicodeUTF8));
        line2->setText(QString());
        Q_UNUSED(MultiUnitSelectFrm);
    } // retranslateUi

};

namespace Ui {
    class MultiUnitSelectFrm: public Ui_MultiUnitSelectFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_MULTIUNITSELECT_H

/********************************************************************************
** Form generated from reading UI file 'Form_McWeight.ui'
**
** Created: Wed Feb 6 11:12:26 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_MCWEIGHT_H
#define UI_FORM_MCWEIGHT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_McWeightFrm
{
public:
    QPushButton *btOk;
    QPushButton *btCancel;
    QPushButton *btManFill;
    QPushButton *btFillSettings;

    void setupUi(QWidget *McWeightFrm)
    {
        if (McWeightFrm->objectName().isEmpty())
            McWeightFrm->setObjectName(QString::fromUtf8("McWeightFrm"));
        McWeightFrm->resize(800, 492);
        btOk = new QPushButton(McWeightFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setGeometry(QRect(580, 410, 90, 70));
        btCancel = new QPushButton(McWeightFrm);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setGeometry(QRect(690, 410, 90, 70));
        btManFill = new QPushButton(McWeightFrm);
        btManFill->setObjectName(QString::fromUtf8("btManFill"));
        btManFill->setGeometry(QRect(690, 10, 90, 70));
        btFillSettings = new QPushButton(McWeightFrm);
        btFillSettings->setObjectName(QString::fromUtf8("btFillSettings"));
        btFillSettings->setGeometry(QRect(690, 90, 90, 70));

        retranslateUi(McWeightFrm);

        QMetaObject::connectSlotsByName(McWeightFrm);
    } // setupUi

    void retranslateUi(QWidget *McWeightFrm)
    {
        McWeightFrm->setWindowTitle(QApplication::translate("McWeightFrm", "Form", 0, QApplication::UnicodeUTF8));
        btOk->setText(QApplication::translate("McWeightFrm", "Ok", 0, QApplication::UnicodeUTF8));
        btCancel->setText(QApplication::translate("McWeightFrm", "Cancel", 0, QApplication::UnicodeUTF8));
        btManFill->setText(QApplication::translate("McWeightFrm", "ManFill", 0, QApplication::UnicodeUTF8));
        btFillSettings->setText(QApplication::translate("McWeightFrm", "Settings", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class McWeightFrm: public Ui_McWeightFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_MCWEIGHT_H

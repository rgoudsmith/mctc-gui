#include "Form_NumericInput.h"
#include "ui_Form_NumericInput.h"
#include "ImageDef.h"
#include <qmath.h>
#include <QPushButton>
#include "Form_MainMenu.h"
#include "Rout.h"

#define charSize 14
#define dotSize 7
#define cursorOffset 8

#define INPUT_ARROW_HEIGHT 368
#define INPUT_NUMERIC_HEIGHT 502

NumericInputFrm::NumericInputFrm(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::NumericInputFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  isFloat = true;
  decimals = 0;
  hasDot = false;
  minIncValue = 0;
  incValue = 0;
  minValue = 0;
  maxValue = 0;
  curPos = 1;
  calID = 0;

  m_ui->setupUi(this);
  m_ui->lbInput->setFixedWidth(charSize-2);

  connect(m_ui->btInc,SIGNAL(clicked()),this,SLOT(Increment()));

  connect(m_ui->btDec,SIGNAL(clicked()),this,SLOT(Decrement()));
  connect(m_ui->btLeft,SIGNAL(clicked()),this,SLOT(ShiftLeft()));
  connect(m_ui->btRight,SIGNAL(clicked()),this,SLOT(ShiftRight()));

  connect(m_ui->bt0,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt1,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt2,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt3,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt4,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt5,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt6,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt7,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt8,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt9,SIGNAL(clicked()),this,SLOT(ButtonClicked()));

  connect(m_ui->btDot,SIGNAL(clicked()),this,SLOT(DotClicked()));
  connect(m_ui->btClear,SIGNAL(clicked()),this,SLOT(ClearClicked()));
  connect(m_ui->btBack,SIGNAL(clicked()),this,SLOT(BackspaceClicked()));
  connect(m_ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(m_ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));
  connect(m_ui->btSwitch,SIGNAL(clicked()),this,SLOT(SwitchClicked()));

  m_ui->btSign->setVisible(false);

  /*----- Disable windows title bar -----*/
  this->setWindowFlags(Qt::CustomizeWindowHint);
}


NumericInputFrm::~NumericInputFrm()
{
  delete m_ui;
}


// Events
void NumericInputFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      m_ui->retranslateUi(this);
      break;
    default:
      break;
  }
}


void NumericInputFrm::showEvent(QShowEvent *e)
{
  QWidget::raise();
  QWidget::showEvent(e);
  glob->mstSts.popupActive = true;
  // use _gbi.mstSts.inputSts to determine which display to activate
  SetInputType(glob->mstSts.inputSts);

  if ((glob->menu) != 0) {
    ((MainMenu*)(glob->menu))->FoldMenu();
  }
}


// Public procedures
void NumericInputFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  glob->SetButtonImage(m_ui->btLeft,itGeneral,genArrowKeyLeft);
  glob->SetButtonImage(m_ui->btRight,itGeneral,genArrowKeyRight);
  glob->SetButtonImage(m_ui->btInc,itGeneral,genArrowInc);
  glob->SetButtonImage(m_ui->btDec,itGeneral,genArrowDec);

  glob->SetButtonImage(m_ui->btOk,itSelection,selOk);
  glob->SetButtonImage(m_ui->btCancel,itSelection,selCancel);

  glob->SetButtonImage(m_ui->btSwitch,itGeneral,genKeyNum2);

  glob->SetButtonImage(m_ui->btClear,itGeneral,genInpClear);
  glob->SetButtonImage(m_ui->btBack,itGeneral,genInpBackspace);

  setPalette(*(glob->Palette()));

  m_ui->pages->setCurrentWidget(m_ui->page);                                    // Set first page (arrow input) as start page
}


void NumericInputFrm::SetDisplay(bool isFloat, int decimals, float value, float minValue, float maxValue, int callerID)
{
  firstInput = true;

  this->calID = callerID;
  this->isFloat = isFloat;
  if (isFloat) {
    this->decimals = decimals;
  }
  else {
    this->decimals = 0;
  }
  this->value = value;
  this->minValue = minValue;
  this->maxValue = maxValue;

  // Calculate incValue according to decimals.
  if (isFloat) {
    float val = 1;
    for (int i=0; i<decimals;i++) {
      val/=10;
    }
    this->incValue = val;
  }
  else {
    this->incValue = 1;
  }

  this->curPos = 1;

  DisplayValue();
  DisplayValue2(false);
  posCursor();
}


void NumericInputFrm::SetMinIncVal(float val)
{
  if (val >= 0) {
    minIncValue = val;
  }
  else {
    minIncValue = -1*val;
  }
}


// Private functions
void NumericInputFrm::ShiftCursor(bool goLeft)
{
  if (goLeft) {
    curPos++;
    posCursor();
  }
  else {
    curPos--;
    posCursor();
  }
}


void NumericInputFrm::posCursor()
{
  int baseX;
  float dotPos;

  int inpValX = m_ui->InputValue->pos().x();
  int inpValWidth = m_ui->InputValue->width();
  int inpCursorWidth = m_ui->lbInput->width();
  baseX = (inpValX + inpValWidth) - (inpCursorWidth + cursorOffset);

  if ((isFloat) && (decimals > 0)) {
    dotPos = ((float)decimals) + 0.5;
  }
  else {
    dotPos = 99999;
  }

  int x = baseX - ((curPos-1)*charSize);

  if (((float)curPos) > dotPos) {
    x -= dotSize;
  }
  m_ui->lbInput->move(x,m_ui->lbInput->pos().y());
}


float NumericInputFrm::RangeCheck(float val)
{
  if (val > maxValue) {
    val = maxValue;
  }
  else {
    if (val < minValue) {
      val = minValue;
    }
    else {
      // Extra check with minIncValue (round up/round down) to make sure that input is ok.
      if (minIncValue != 0) {
        float dispVal;
        float f1;
        long i1;

        f1 = val / minIncValue;
        i1 = (int)f1;

        f1 = fmodf(val,minIncValue);
        if (f1 >= (minIncValue/2)) {
          i1++;
        }
        dispVal = i1 * minIncValue;
        val = dispVal;
      }
    }
  }
  return val;
}


// Private slots
void NumericInputFrm::DisplayValue()
{
  QString str;

  if (isFloat) {
    str = QString("%1").arg(value,0,'f',decimals);
  }
  else {
    str = QString("%1").arg(value,0,'f',0);
  }

  m_ui->InputValue->setText(str);

  if ((isFloat) && (decimals > 0)) {
    this->hasDot = true;
  }
  else {
    this->hasDot = false;
  }
}


void NumericInputFrm::ClearDisplay()
{
  m_ui->InputValue->setText("0");

  value = 0;
  this->hasDot = false;
}


void NumericInputFrm::Increment()
{
  if (incValue >= minIncValue) {
    value += incValue;
  }
  else {
    value += minIncValue;
  }
  value = RangeCheck(value);
  DisplayValue();
}


void NumericInputFrm::Decrement()
{
  if (incValue >= minIncValue) {
    value -= incValue;
  }
  else {
    value -= minIncValue;
  }
  value = RangeCheck(value);
  DisplayValue();
}


void NumericInputFrm::ShiftLeft()
{
  if (this->incValue < this->maxValue) {
    this->incValue *= 10;
    ShiftCursor(true);
    if (this->incValue > maxValue) {
      this->incValue /= 10;
      ShiftCursor(false);
    }
  }
}


void NumericInputFrm::ShiftRight()
{
  if (curPos > 1) {
    this->incValue /= 10;
    ShiftCursor(false);
  }
}


void NumericInputFrm::ValueReceived(float newVal)
{
  this->value = RangeCheck(newVal);
  DisplayValue();
  OkClicked();
}


void NumericInputFrm::CancelClicked()
{
  glob->mstSts.popupActive = false;
  close();
}


void NumericInputFrm::OkClicked()
{
  float old = value;
  value = RangeCheck(value);
  if (value == old) {
    emit InputValue(this->value);
    emit InputValue(this->value,calID);
    CancelClicked();
  }
  else {
    DisplayValue2(false);
    firstInput = true;
  }
}


void NumericInputFrm::SwitchClicked()
{
  switch(m_ui->pages->currentIndex()) {
    case 0:
      SetInputType(1);
      DisplayValue2(false);
      firstInput = true;
      break;

    case 1:
      SetInputType(0);
      value = RangeCheck(value);
      DisplayValue();
      break;

    default:
      glob->SetButtonImage(m_ui->btSwitch,itGeneral,genKeyNum1);
      m_ui->pages->setCurrentIndex(0);
      value = RangeCheck(value);
      DisplayValue();
      setFixedHeight(INPUT_ARROW_HEIGHT);
      break;
  }
}


void NumericInputFrm::SetInputType(int type)
{
  switch(type) {
    case 0:
      glob->SetButtonImage(m_ui->btSwitch,itGeneral,genKeyNum2);
      setFixedHeight(INPUT_ARROW_HEIGHT);
      break;
    case 1:
      glob->SetButtonImage(m_ui->btSwitch,itGeneral,genKeyNum1);
      setFixedHeight(INPUT_NUMERIC_HEIGHT);
      break;
  }
  m_ui->pages->setCurrentIndex(type);
  // Store last version into mstSts struct
  if (glob->mstSts.inputSts != type) {
    glob->mstSts.inputSts = type;
  }
}


// Page 2 functionality
void NumericInputFrm::DisplayValue2(bool preserveDot)
{
  QString str;

  if (preserveDot) {
    QString dot = ".";
    int dotIdx = m_ui->inputValue->text().indexOf(dot);
    int dec;

    if (dotIdx > 0) {
      int strLen = m_ui->inputValue->text().length();
      dec = (strLen-dotIdx)-1;
    }
    else {
      dec = 0;
      hasDot2 = false;
    }
    str = QString("%1").arg( value,0,'f',dec);
  }
  else {
    if (isFloat) {
      str = QString("%1").arg( value,0,'f',decimals);
    }
    else {
      str = QString("%1").arg( value,0,'f',0);
    }

    if ((isFloat) && (decimals > 0)) {
      this->hasDot2 = true;
    }
    else {
      this->hasDot2 = false;
    }
  }

  m_ui->inputValue->setText(str);
}


void NumericInputFrm::ClearClicked()
{
  m_ui->inputValue->setText("0");
  value = 0;
  hasDot2 = false;
  firstInput = true;
}


void NumericInputFrm::ButtonClicked()
{
  if (firstInput) {
    this->ClearClicked();
    firstInput = false;
  }

  QPushButton *button = qobject_cast<QPushButton*>(sender());
  QString butText = button->text();

  if(m_ui->inputValue->text() == "0") {
    if (butText != ".") {
      m_ui->inputValue->setText(butText);
    }
    else {
      hasDot2 = true;
      m_ui->inputValue->setText(m_ui->inputValue->text()+butText);
    }
  }
  else {
    if ((butText == ".") && ((!(hasDot2)) && (isFloat))) {
      hasDot2 = true;
      m_ui->inputValue->setText(m_ui->inputValue->text()+butText);
    }
    else {
      if (!hasDot2) {
        m_ui->inputValue->setText(m_ui->inputValue->text()+butText);
      }
      else {
        QString dot = ".";
        int dotIdx,strLen;
        // Check length of the string after the decimal seperator (dot)
        // If length is equal to numer of decimals allowed, ignore input
        dotIdx = m_ui->inputValue->text().indexOf(dot);
        strLen = m_ui->inputValue->text().length();
        if ((strLen-dotIdx) <= decimals) {
          m_ui->inputValue->setText(m_ui->inputValue->text()+butText);
        }
      }
    }
  }

  value = m_ui->inputValue->text().toDouble();
}


void NumericInputFrm::DotClicked()
{
  if (firstInput == true) {
    m_ui->inputValue->setText("0.");
    hasDot2 = true;
  }
  else {
    if (!(hasDot2)) {
      m_ui->inputValue->setText(m_ui->inputValue->text() + ".");
      hasDot2 = true;
    }
  }
  firstInput = false;
  value = m_ui->inputValue->text().toDouble();
}


void NumericInputFrm::BackspaceClicked()
{
  if ((m_ui->inputValue->text() != "0") && (m_ui->inputValue->text() != "")) {
    if (m_ui->inputValue->text().right(1) == ".") {
      hasDot2 = false;
    }

    m_ui->inputValue->setText(m_ui->inputValue->text().left(m_ui->inputValue->text().length()-1));

    if (m_ui->inputValue->text() == "") {
      m_ui->inputValue->setText("0");
    }
  }
  firstInput = false;
  value = m_ui->inputValue->text().toDouble();
}

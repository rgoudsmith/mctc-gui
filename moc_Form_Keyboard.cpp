/****************************************************************************
** Meta object code from reading C++ file 'Form_Keyboard.h'
**
** Created: Wed Feb 6 11:15:02 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_Keyboard.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_Keyboard.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KeyboardFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      32,   12,   12,   12, 0x08,
      47,   12,   12,   12, 0x08,
      63,   12,   12,   12, 0x08,
      82,   12,   12,   12, 0x08,
      97,   12,   12,   12, 0x08,
     112,   12,   12,   12, 0x08,
     124,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_KeyboardFrm[] = {
    "KeyboardFrm\0\0inputText(QString)\0"
    "shiftClicked()\0buttonClicked()\0"
    "backspaceClicked()\0spaceClicked()\0"
    "clearClicked()\0okClicked()\0cancelClicked()\0"
};

const QMetaObject KeyboardFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_KeyboardFrm,
      qt_meta_data_KeyboardFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KeyboardFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KeyboardFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KeyboardFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KeyboardFrm))
        return static_cast<void*>(const_cast< KeyboardFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int KeyboardFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: inputText((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: shiftClicked(); break;
        case 2: buttonClicked(); break;
        case 3: backspaceClicked(); break;
        case 4: spaceClicked(); break;
        case 5: clearClicked(); break;
        case 6: okClicked(); break;
        case 7: cancelClicked(); break;
        default: ;
        }
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void KeyboardFrm::inputText(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

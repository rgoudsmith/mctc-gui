#ifndef ERRORDLG_H
#define ERRORDLG_H

#include <QDialog>
#include <QTimer>
#include "Form_QbaseWidget.h"

namespace Ui
{
  class ErrorDlg;
}


class ErrorDlg : public QDialog
{
  Q_OBJECT

    public:
    explicit ErrorDlg(QWidget *parent = 0);
    ~ErrorDlg();
    void FormInit();
    void ShowMessage(msgType,int,int,const QString &);
    void ShowMessage(int,const QString &,int);
    int LastSelection();

  protected:
    void changeEvent(QEvent *e);

  private:
    //    global * _gbi;
    Ui::ErrorDlg *ui;
    int unitIndex;
    msgType mType;
    int lastSelect;
    QTimer * closeTim;
    void CancelAlarm();
    void SetButtons(msgType);
    void LanguageUpdate();
    void CancelForm();
    void KeyBeep();

  private slots:
    void OnCloseTim();
    void bt1Clicked();
    void bt2Clicked();
    void bt3Clicked();
    void bt4Clicked();
    void AlarmStatusChanged(bool,int);

    signals:
    void ButtonClicked(int);
};
#endif                                                                          // ERRORDLG_H

#include <QObjectList>
#include "Form_Settings.h"
#include "ui_Form_Settings.h"
#include "Form_SettingsItem.h"
#include "ImageDef.h"
#include "Form_MainMenu.h"
#include "Rout.h"

SettingsFrm::SettingsFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::SettingsFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  itemHeight = 0;

  sysChanges = false;
  mstChanges = false;

  ui->setupUi(this);
  numInput = new NumericInputFrm(0);

  contents = new QWidget(this);

  layout = new QVBoxLayout(contents);
  contents->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
  ui->scrollArea->setWidget(contents);

  //    ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  ui->scrollArea->setWidgetResizable(true);

  QMvcScrollBar * bar = new QMvcScrollBar(ui->scrollArea);
  connect(bar,SIGNAL(MouseButtonPressed()),this,SLOT(KeyBeep()));
  ui->scrollArea->setVerticalScrollBar(bar);

  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));

}


SettingsFrm::~SettingsFrm()
{
  delete numInput;
  delete ui;
}


void SettingsFrm::showEvent(QShowEvent *e)
{
  /*-------------------------------------------*/
  /* Showevent : The settings screen is opened */
  /*-------------------------------------------*/
  QBaseWidget::showEvent(e);

  /* Show a message box */
  Rout::ShowMsg(tr("Working"),tr("Please wait.... "));

  /* Build service sreen */
  ClearItems();
  FillItems();

  /* Close message */
  Rout::CloseMsg();
}


void SettingsFrm::hideEvent(QHideEvent *e)
{
  /*-------------------------------------------*/
  /* Showevent : The settings screen is closed*/
  /*-------------------------------------------*/
  QBaseWidget::hideEvent(e);

#ifdef UIT
  /*----- In TWIN mode the settings for unit 0 and unit 1 are identical -----*/
  if(glob->mstSts.isTwin) {
    /*----- Copy settings from unit 0 to uni 1 -----*/
    glob->sysCfg[1].MvcSettings = glob->sysCfg[0].MvcSettings;
  }
#endif
}


void SettingsFrm::resizeEvent(QResizeEvent *e)
{
  QWidget::resizeEvent(e);

  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
}


void SettingsFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  numInput->FormInit();
#ifdef UIT
  FillItems();
#endif

  glob->SetButtonImage(ui->btOk,itSelection,selOk);
  connect(glob,SIGNAL(MstCfg_UserChanged(eUserType)),this,SLOT(UserChanged(eUserType)));
}


void SettingsFrm::UserChanged(eUserType)
{
  /*--------------------*/
  /* User level changed */
  /*--------------------*/
#ifdef UIT
  /*----- Rebuild settings screen items because they are user level dependant -----*/
  ClearItems();
  FillItems();
#endif
}


void SettingsFrm::FillItems()
{
  /*--------------------------*/
  /* Setup setting form items */
  /*--------------------------*/
  int cnt = 0;
  int h;

  // Determine which settings to show (Dependant on userlevel).
  if (glob->GetUser() >= utMovaColor) {
    cnt = FillMvcSettings();
  }
  else {
    if (glob->GetUser() >= utAgent) {
      cnt = FillAgentSettings();
    }
  }

  h = ((cnt * itemHeight) + (cnt*9) + 9);
  contents->setFixedHeight(h);
}


void SettingsFrm::ClearItems()
{
  /*--------------------------------------------------*/
  /* Remove all setting items from the display widget */
  /* Loop through all widgets of the objList          */
  /*--------------------------------------------------*/
  QWidget * item = 0;

  while (objList.count() > 0) {
    // If they are SettingsItemFrm type, delete them
    item = qobject_cast<SettingsItemFrm*>(objList.at(0));
    // If the typecast fails, it returns 0.
    if (item != 0) {
      // Delete the item and remove it from the list.
      delete item;
      objList.removeFirst();
      item = 0;
    }
  }
}


int SettingsFrm::FillAgentSettings()
{
  /*----------------------------*/
  /* Setup agent level settings */
  /*----------------------------*/
  SettingsItemFrm * item;
  int unID = 0;

  // Each SettingsItemFrms can contain 2 items. One on the left, one on the right.
  item = new SettingsItemFrm(ui->scrollArea);
  // Set the parent of the item so it is freed correctly when the parent form is destroyed
  item->setParent(ui->scrollArea);

  // Link the NumericInputForm to the SettingsItemFrm.
  item->SetInput(numInput);

  item->FormInit();
  // Setup the form and link it to the desired variables.
  item->ShowOneItem(false);
  // Field = full scall.
  item->SetItem1Title("Full Scale");
  // Set actual, minimum and maximum values and number of decimals for the current value.
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.fullScale.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.fullScale.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.fullScale.maxVal,
                 0);
  // Initialize the 2nd value identival to the first.
  // This value is keybeep (boolean)
  item->SetItem2Title("Key Beep");
  // Set actual, minimum, maximum and precision.
  item->SetItem2((glob->mstCfg.keyBeep == true ? 1 : 0),
                 0,
                 1,
                 0);
  // Set a unique id for the value.
  item->SetUniqueID(unID++);
  // Connect the SettingsItemFrm to the correct Slots for input handling.
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(FullScaleReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(KeyBeepReceived(float)));
  // Display the SettingsItemFrm in the correct layout so it is positioned correctly on the screen.
  layout->addWidget(item);
  // Add it to the objList so we can remove it easily when needed.
  objList.append(item);

  // And so on with all settings...
  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Test < 0.5 g/s");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.testLt05g.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.testLt05g.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.testLt05g.maxVal,
                 0,
                 2);
  item->SetItem2Title("Test > 0.5 g/s");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.testGt05g.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.testGt05g.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.testGt05g.maxVal,
                 0,
                 2);
  item->SetUniqueID(unID++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(TestTime1Received(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(TestTime2Received(float)));
  layout->addWidget(item);
  objList.append(item);
  /*
        item = new SettingsItemFrm(ui->scrollArea);
        item->SetInput(numInput);
        item->FormInit();
        item->ShowOneItem(false);
        item->SetItem1Title("CAL Cylinder weight");
        item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.weightCylinder.actVal,
                       glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.weightCylinder.minVal,
                       glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.weightCylinder.maxVal);
        item->SetItem2Title("CAL Auger weight");
        item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.weightAuger.actVal,
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.weightAuger.minVal,
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.weightAuger.maxVal);
  item->SetUniqueID(2);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(CalWeightCylinderReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(CalWeightAugerReceived(float)));
  layout->addWidget(item);
  */

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Cal Alarm Cycle");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.alarmCycles.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.alarmCycles.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.alarmCycles.maxVal,
                 0);
  item->SetItem2Title("Motor alarm");
  item->SetItem2((glob->mstCfg.motorAlarmEnb == true ? 1 : 0),
                 0,
                 1,
                 0);
  item->SetUniqueID(unID++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(CalAlarmCyclesReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(MotorAlarmModeReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Cylinder fill time");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.fillTime.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.fillTime.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.fillTime.maxVal,
                 0);
  item->SetItem2Title("Cylinder fill speed");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.fillSpeed.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.fillSpeed.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.fillSpeed.maxVal,
                 0,
                 1);
  item->SetUniqueID(unID++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(FillCylinderTimeReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(FillCylinderSpeedReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Screen reset time");
  item->SetItem1(glob->mstCfg.MvcSettings.agent.screenTime.actVal,
                 glob->mstCfg.MvcSettings.agent.screenTime.minVal,
                 glob->mstCfg.MvcSettings.agent.screenTime.maxVal,
                 0);
  item->SetItem2Title("Tacho correction");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.tachoCorrection.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.tachoCorrection.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.tachoCorrection.maxVal,
                 0,
                 3);
  item->SetUniqueID(unID++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(ScreenTimeReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(TachoCorrReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Ext Cap Time");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.extCapTime.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.extCapTime.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.extCapTime.maxVal,
                 0,
                 1);
  item->SetItem2Title("Ext Cap Deviation");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.extCapDev.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.extCapDev.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.extCapDev.maxVal,
                 0,
                 1);
  item->SetUniqueID(unID++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(ExtCapTimeReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(ExtCapDevReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("After Fill Delay");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.afterFillDelay.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.afterFillDelay.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.afterFillDelay.maxVal,
                 0);
  item->SetItem2Title("Input Filter");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.inputFilter.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.inputFilter.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.inputFilter.maxVal,
                 0);
  item->SetUniqueID(unID++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(AfterFillDelayReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(InputFilterReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Met. Time Deviation");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.MetTimeDev.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.MetTimeDev.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.MetTimeDev.maxVal,
                 0,
                 1);
  item->SetItem2Title("Curve Gain");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.curveGain.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.curveGain.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.curveGain.maxVal,
                 0,
                 2);
  item->SetUniqueID(unID++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(MetTimeDevReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(CurveGainReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  /* // Is vervallen.
    item->SetItem1Title("Min. Gravi Cap");
    item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.minGraviCap.actVal,
                   glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.minGraviCap.minVal,
                   glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.minGraviCap.maxVal,
                   0,
                   2);
    */
  item->SetItem1Title("Inp. Delay (x10 ms)");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.inputDelay.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.inputDelay.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.inputDelay.maxVal,
                 0);
  item->SetItem2Title("Max Meas Time");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureMaxTime.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureMaxTime.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureMaxTime.maxVal,
                 0);
  item->SetUniqueID(unID++);
  //connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(MinGraviCapReceived(float)));
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(InpDelayReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(MeasMaxTimeReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Run Cont. On Delay");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.runContactOnDelay.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.runContactOnDelay.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.runContactOnDelay.maxVal,
                 0,
                 1);
  item->SetItem2Title("Run Cont. Off Delay");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.runContactOffDelay.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.runContactOffDelay.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.runContactOffDelay.maxVal,
                 0,
                 1);
  item->SetUniqueID(unID++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(RunContactOnDelayReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(RunContactOffDelayReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Log Interval");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.logInterval.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.logInterval.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.logInterval.maxVal,
                 0);
  item->SetItem2Title("Col % Set Deviation");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.colPctSetDev.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.colPctSetDev.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.colPctSetDev.maxVal,
                 0,
                 1);
  item->SetUniqueID(unID++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(LogIntervalReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(ColPctSetDevReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Deviation Alarm %");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPctQR.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPctQR.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPctQR.maxVal,
                 0);
  item->SetItem2Title("Motor 6 Wires");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.motorSixWires.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.motorSixWires.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.motorSixWires.maxVal,
                 0);
  item->SetUniqueID(unID++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(DevAlarmQrReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(MotorSixWiresReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Short meas.time");
  item->SetItem1(glob->mstCfg.shortMeasTime,0,999,0,1);

  item->SetItem2Title("Ext. hysterese");
  item->SetItem2(glob->mstCfg.hysterese,0,100,0,1);

  item->SetUniqueID(unID++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(ShortMeasTimeReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(HysteresReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(true);
  item->SetItem1Title("Max motor speed");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.maxMotorSpeed.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.maxMotorSpeed.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.maxMotorSpeed.maxVal,
                 0);
  item->SetUniqueID(unID++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(MaxMotorSpeedReceived(float)));
  layout->addWidget(item);
  objList.append(item);


  itemHeight = item->height();
  return unID;
}


int SettingsFrm::FillMvcSettings()
{
  /*--------------------------------*/
  /* Setup movacolor level settings */
  /*--------------------------------*/

  /*----- Show agent settings -----*/
  int cnt = FillAgentSettings();

  /*----- Plus some extra (MovaColor settings) -----*/
  SettingsItemFrm * item;

  // For an explanation on how to initialize a SettingsItemFrm see the FillAgentSettings() function.
  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);

  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Motion Band");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.motionBand.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.motionBand.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.motionBand.maxVal,
                 0.5,
                 1);
  item->SetItem2Title("Motion Delay");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.motionDelay.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.motionDelay.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.motionDelay.maxVal,
                 0,
                 1);
  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(MotionBandReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(MotionDelayReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Weight Filter");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.weightFilter.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.weightFilter.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.weightFilter.maxVal,
                 0);
  item->SetItem2Title("Weight Filter Fast");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.weightFilterFast.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.weightFilterFast.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.weightFilterFast.maxVal,
                 0);
  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(WeightFilterReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(WeightFilterFastReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(true);
  item->SetItem1Title("Reference Weight");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.refWeight.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.refWeight.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.refWeight.maxVal,
                 0,
                 1);
  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(RefWeightReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Min Meas Time");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureMinTime.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureMinTime.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureMinTime.maxVal,
                 0);
  item->SetItem2Title("Min Meas Time B4");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureBand4MinTime.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureBand4MinTime.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureBand4MinTime.maxVal,
                 0);
  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(MeasMinTimeReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(MeasMinBand4Received(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(true);
  item->SetItem1Title("Meas Time Cor");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureTimeCorr.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureTimeCorr.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureTimeCorr.maxVal,
                 0,
                 2);
  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(MeasTimeCorrReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Dev Thresh Inj");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devThreshInj.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devThreshInj.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devThreshInj.maxVal,
                 0);
  item->SetItem2Title("Dev Thresh Ext");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devThreshExt.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devThreshExt.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devThreshExt.maxVal,
                 0);
  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(DevThreshInjReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(DevThreshExtReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Rpm Cor Factor 1");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.rpmCorrFac1.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.rpmCorrFac1.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.rpmCorrFac1.maxVal,
                 0);
  item->SetItem2Title("Rpm Cor Factor 2");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.rpmCorrFac2.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.rpmCorrFac2.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.rpmCorrFac2.maxVal,
                 0);
  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(RpmCorrFac1Received(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(RpmCorrFac2Received(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Dev Band 4 Hyst");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devB4Hyst.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devB4Hyst.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devB4Hyst.maxVal,
                 0,
                 1);
  item->SetItem2Title("Min.ext.cap kg/h");
  item->SetItem2(glob->mstCfg.minExtCap,0,1000,0,1);

  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(DevB4HystReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(MinExtCapReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Fill Start");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillStart.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillStart.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillStart.maxVal,
                 0,
                 1);
  item->SetItem2Title("Fill Start HL");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillStartHL.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillStartHL.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillStartHL.maxVal,
                 0,
                 1);
  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(BAL_FillStartReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(BAL_FillStartHlReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Fill Ready");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillReady.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillReady.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillReady.maxVal,
                 0,
                 1);
  item->SetItem2Title("Fill Time");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillTime.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillTime.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillTime.maxVal,
                 0);
  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(BAL_FillReadyReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(BAL_FillTimeReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Lid Off Weight");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.lidOffWeight.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.lidOffWeight.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.lidOffWeight.maxVal,
                 0,
                 0);
  item->SetItem2Title("Lid Off Time");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.lidOffTime.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.lidOffTime.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.lidOffTime.maxVal,
                 0);
  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(BAL_LidOffWeightReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(BAL_LidOffTimeReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Meas Cons Time");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureConsTime.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureConsTime.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureConsTime.maxVal,
                 0,
                 2);
  item->SetItem2Title("Dev Alarm %");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPct.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPct.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPct.maxVal,
                 0);
  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(ConMeasTimeReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(DevAlmPctReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);
  item->SetItem1Title("Dev Alm Retry");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmRetry.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmRetry.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmRetry.maxVal,
                 0);
  item->SetItem2Title("Dev Alm % B4");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPctB4.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPctB4.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPctB4.maxVal,
                 0);
  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(DevAlmRetryReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(DevAlmPctB4Received(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(true);
  item->SetItem1Title("Dev Alm Retry B4");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmRetryB4.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmRetryB4.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmRetryB4.maxVal,
                 0);

  //  item->SetItem2Title("Dev Alm % QR");
  //  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPctQR.actVal,
  //                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPctQR.minVal,
  //                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPctQR.maxVal,
  //                 0);

  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(DevAlmRetryB4Received(float)));
  //  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(DevAlarmPctQrReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Dev Alm Retry QR");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmRetryQR.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmRetryQR.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmRetryQR.maxVal,
                 0);

  item->SetItem2Title("Dev Alm Band Nr QR");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmBandNrQR.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmBandNrQR.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmBandNrQR.maxVal,
                 0);

  item->SetUniqueID(cnt++);
  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(DevAlarmRetryQrReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(DevAlarmBandNrQrReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Act Update");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.actUpdate.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.actUpdate.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.actUpdate.maxVal,
                 0);
  item->SetItem2Title("CAN WD Disabled");
  item->SetItem2((glob->mstCfg.canWdDis == true ? 1 : 0),
                 0,
                 1,
                 0);

  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(ActUpdateReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(CanWdDisReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  /*----- Band 0 settings -----*/

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 0 CorDev");

  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corDev.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corDev.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corDev.maxVal,
                 0);

  item->SetItem2Title("Band 0 CorFac 1");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corFac1.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corFac1.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corFac1.maxVal,
                 0,
                 1);

  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B0_CorDevReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B0_CorFac1Received(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 0 CorFac 2");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corFac2.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corFac2.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corFac2.maxVal,
                 0,
                 1);
  item->SetItem2Title("Band 0 Cor N1");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corN1.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corN1.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corN1.maxVal,
                 0);
  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B0_CorFac2Received(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B0_N1Received(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 0 Cor N2");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corN2.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corN2.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corN2.maxVal,
                 0);
  item->SetItem2Title("Band 0 Cor Max");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corMax.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corMax.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corMax.maxVal,
                 0);

  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B0_N2Received(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B0_CorMaxReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  /*----- Band 1 settings -----*/

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 1 CorDev");

  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corDev.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corDev.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corDev.maxVal,
                 0);

  item->SetItem2Title("Band 1 CorFac 1");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corFac1.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corFac1.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corFac1.maxVal,
                 0,
                 1);

  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B1_CorDevReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B1_CorFac1Received(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 1 CorFac 2");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corFac2.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corFac2.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corFac2.maxVal,
                 0,
                 1);
  item->SetItem2Title("Band 1 Cor N1");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corN1.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corN1.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corN1.maxVal,
                 0);
  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B1_CorFac2Received(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B1_N1Received(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 1 Cor N2");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corN2.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corN2.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corN2.maxVal,
                 0);
  item->SetItem2Title("Band 1 Cor Max");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corMax.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corMax.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corMax.maxVal,
                 0);

  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B1_N2Received(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B1_CorMaxReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  /*----- Band 2 settings -----*/

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 2 CorDev");

  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corDev.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corDev.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corDev.maxVal,
                 0);

  item->SetItem2Title("Band 2 CorFac 1");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corFac1.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corFac1.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corFac1.maxVal,
                 0,
                 1);

  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B2_CorDevReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B2_CorFac1Received(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 2 CorFac 2");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corFac2.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corFac2.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corFac2.maxVal,
                 0,
                 1);
  item->SetItem2Title("Band 2 Cor N1");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corN1.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corN1.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corN1.maxVal,
                 0);
  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B2_CorFac2Received(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B2_N1Received(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 2 Cor N2");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corN2.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corN2.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corN2.maxVal,
                 0);
  item->SetItem2Title("Band 2 Cor Max");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corMax.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corMax.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corMax.maxVal,
                 0);

  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B2_N2Received(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B2_CorMaxReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  /*----- Band 3 settings -----*/

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 3 CorDev");

  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corDev.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corDev.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corDev.maxVal,
                 0);

  item->SetItem2Title("Band 3 CorFac 1");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corFac1.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corFac1.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corFac1.maxVal,
                 0,
                 1);

  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B3_CorDevReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B3_CorFac1Received(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 3 CorFac 2");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corFac2.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corFac2.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corFac2.maxVal,
                 0,
                 1);
  item->SetItem2Title("Band 3 Cor N1");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corN1.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corN1.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corN1.maxVal,
                 0);
  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B3_CorFac2Received(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B3_N1Received(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 3 Cor N2");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corN2.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corN2.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corN2.maxVal,
                 0);
  item->SetItem2Title("Band 3 Cor Max");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corMax.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corMax.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corMax.maxVal,
                 0);

  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B3_N2Received(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B3_CorMaxReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  /*----- Band 4 settings -----*/

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 4 CorDev");

  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corDev.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corDev.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corDev.maxVal,
                 0);

  item->SetItem2Title("Band 4 CorFac 1");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corFac1.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corFac1.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corFac1.maxVal,
                 0,
                 1);

  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B4_CorDevReceived(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B4_CorFac1Received(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 4 CorFac 2");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corFac2.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corFac2.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corFac2.maxVal,
                 0,
                 1);
  item->SetItem2Title("Band 4 Cor N1");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corN1.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corN1.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corN1.maxVal,
                 0);
  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B4_CorFac2Received(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B4_N1Received(float)));
  layout->addWidget(item);
  objList.append(item);

  item = new SettingsItemFrm(ui->scrollArea);
  item->SetInput(numInput);
  item->FormInit();
  item->ShowOneItem(false);

  item->SetItem1Title("Band 4 Cor N2");
  item->SetItem1(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corN2.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corN2.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corN2.maxVal,
                 0);
  item->SetItem2Title("Band 4 Cor Max");
  item->SetItem2(glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corMax.actVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corMax.minVal,
                 glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corMax.maxVal,
                 0);

  item->SetUniqueID(cnt++);

  connect(item,SIGNAL(SetValueItem1(float)),this,SLOT(B4_N2Received(float)));
  connect(item,SIGNAL(SetValueItem2(float)),this,SLOT(B4_CorMaxReceived(float)));
  layout->addWidget(item);
  objList.append(item);

  return cnt;
}


void SettingsFrm::OkClicked()
{
  if (sysChanges) {
    sysChanges = false;
    glob->SysCfg_SaveSettings(glob->ActUnitIndex());
  }

  if (mstChanges) {
    mstChanges = false;
    glob->MstCfg_SaveSettings();
  }

  ((MainMenu*)(glob->menu))->DisplayWindow(wiBack);
}


// Slots for Agent settings

void SettingsFrm::FullScaleReceived(float fs)
{
  glob->SysCfg_SetLoadcellType(int(fs),glob->ActUnitIndex());
}


void SettingsFrm::KeyBeepReceived(float kb)
{
  glob->MstCfg_SetKeyBeep((kb == 0 ? false : true));
}


void SettingsFrm::TestTime1Received(float tim)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.testLt05g.actVal = tim;
  sysChanges = true;
}


void SettingsFrm::TestTime2Received(float tim)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.testGt05g.actVal = tim;
  sysChanges = true;
}


void SettingsFrm::CalWeightCylinderReceived(float wght)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.weightCylinder.actVal = wght;
  sysChanges = true;
}


void SettingsFrm::CalWeightAugerReceived(float wght)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.weightAuger.actVal = wght;
  sysChanges = true;
}


void SettingsFrm::CalStepsReceived(float cnt)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.steps.actVal = cnt;
  sysChanges = true;
}


void SettingsFrm::CalTimeoutReceived(float tim)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.timeout.actVal = tim;
  sysChanges = true;
}


void SettingsFrm::CalAlarmCyclesReceived(float cycles)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.calib.alarmCycles.actVal = cycles;
  sysChanges = true;
}


void SettingsFrm::MotorAlarmModeReceived(float on)
{
  glob->MstCfg_MotorAlarmEnb((on == 0 ? false : true));
  sysChanges = true;
}


void SettingsFrm::FillCylinderTimeReceived(float tim)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.fillTime.actVal = tim;
  sysChanges = true;
}


void SettingsFrm::FillCylinderSpeedReceived(float rpm)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.fillSpeed.actVal = rpm;
  sysChanges = true;
}


void SettingsFrm::ScreenTimeReceived(float tim)
{
  glob->mstCfg.MvcSettings.agent.screenTime.actVal = tim;
  // Update timer interval
#warning Disabled !!
  //    ((MainMenu*)(glob->menu))->SetHomeTimerInterval(glob->mstCfg.MvcSettings.agent.screenTime.actVal);
  mstChanges = true;
}


void SettingsFrm::TachoCorrReceived(float corr)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.tachoCorrection.actVal = corr;
  sysChanges = true;
}


void SettingsFrm::ExtCapTimeReceived(float tim)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.extCapTime.actVal = tim;
  sysChanges = true;
}


void SettingsFrm::ExtCapDevReceived(float dev)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.extCapDev.actVal = dev;
  sysChanges = true;
}


void SettingsFrm::AfterFillDelayReceived(float dly)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.afterFillDelay.actVal = dly;
  sysChanges = true;
}


void SettingsFrm::InputFilterReceived(float cnt)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.inputFilter.actVal = cnt;
  sysChanges = true;
}


void SettingsFrm::MetTimeDevReceived(float dev)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.MetTimeDev.actVal = dev;
  sysChanges = true;
}


void SettingsFrm::CurveGainReceived(float gain)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.curveGain.actVal = gain;
  sysChanges = true;
}


void SettingsFrm::MinGraviCapReceived(float cap)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.minGraviCap.actVal = cap;
  sysChanges = true;
}


void SettingsFrm::InpDelayReceived(float dly)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.inputDelay.actVal = dly;
  sysChanges = true;
}


void SettingsFrm::RunContactOnDelayReceived(float dly)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.runContactOnDelay.actVal = dly;
  sysChanges = true;
}


void SettingsFrm::RunContactOffDelayReceived(float dly)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.runContactOffDelay.actVal = dly;
  sysChanges = true;
}


void SettingsFrm::LogIntervalReceived(float intv)
{
  glob->MvcCfg_SetLogInterval(intv,glob->ActUnitIndex());
  sysChanges = true;
}


void SettingsFrm::ColPctSetDevReceived(float dev)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.colPctSetDev.actVal = dev;
  sysChanges = true;
}


void SettingsFrm::DevAlarmQrReceived(float dev)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPctQR.actVal = dev;
  sysChanges = true;
}


void SettingsFrm::MotorSixWiresReceived(float on)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.motorSixWires.actVal = (on == 0 ? 0x00:0x01);
  sysChanges = true;
}


// Slots for MovaColor settings
void SettingsFrm::MotionBandReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.motionBand.actVal = val;
  sysChanges = true;
}


void SettingsFrm::MotionDelayReceived(float dly)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.motionDelay.actVal = dly;
  sysChanges = true;
}


void SettingsFrm::WeightFilterReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.weightFilter.actVal = (int)val;
  sysChanges = true;
}


void SettingsFrm::WeightFilterFastReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.weightFilterFast.actVal = (int)val;
  sysChanges = true;
}


void SettingsFrm::RefWeightReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.weight.refWeight.actVal = val;
  sysChanges = true;
}


// Balance settings (Band values)
void SettingsFrm::MeasMinTimeReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureMinTime.actVal = (int)val;
  sysChanges = true;
}


void SettingsFrm::MaxMotorSpeedReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.maxMotorSpeed.actVal = val;
  sysChanges = true;
}


void SettingsFrm::MeasMinBand4Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureBand4MinTime.actVal = (int)val;
  sysChanges = true;
}



void SettingsFrm::MeasMaxTimeReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureMaxTime.actVal = (int)val;
  sysChanges = true;
}


void SettingsFrm::MeasTimeCorrReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureTimeCorr.actVal = val;
  sysChanges = true;
}


void SettingsFrm::DevThreshInjReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devThreshInj.actVal = (int)val;
  sysChanges = true;
}


void SettingsFrm::DevThreshExtReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devThreshExt.actVal = (int)val;
  sysChanges = true;
}


void SettingsFrm::RpmCorrFac1Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.rpmCorrFac1.actVal = (int)val;
  sysChanges = true;
}


void SettingsFrm::RpmCorrFac2Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.rpmCorrFac2.actVal = val;
  sysChanges = true;
}


void SettingsFrm::DevB4HystReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devB4Hyst.actVal = val;
  sysChanges = true;
}


void SettingsFrm::MinExtCapReceived(float val)
{
  glob->mstCfg.minExtCap = val;
  mstChanges = true;
}


void SettingsFrm::CanWdDisReceived(float on)
{
  glob->mstCfg.canWdDis = ((on == 0 ? false : true));
  mstChanges = true;
}


void SettingsFrm::BAL_FillStartReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillStart.actVal = val;
  sysChanges = true;
}


void SettingsFrm::BAL_FillStartHlReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillStartHL.actVal = val;
  sysChanges = true;
}


void SettingsFrm::BAL_FillReadyReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillReady.actVal = val;
  sysChanges = true;
}


void SettingsFrm::BAL_FillTimeReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.fillTime.actVal = val;
  sysChanges = true;
}


void SettingsFrm::BAL_LidOffWeightReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.lidOffWeight.actVal = val;
  sysChanges = true;
}


void SettingsFrm::BAL_LidOffTimeReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.lidOffTime.actVal = val;
  sysChanges = true;
}


void SettingsFrm::ConMeasTimeReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.measureConsTime.actVal = val;
  sysChanges = true;
}


void SettingsFrm::DevAlmPctReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPct.actVal = val;
  sysChanges = true;
}


void SettingsFrm::DevAlmRetryReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmRetry.actVal = val;
  sysChanges = true;
}


void SettingsFrm::DevAlmPctB4Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPctB4.actVal = val;
  sysChanges = true;
}


void SettingsFrm::DevAlmRetryB4Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmRetryB4.actVal = val;
  sysChanges = true;
}


void SettingsFrm::ActUpdateReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.actUpdate.actVal = val;
  sysChanges = true;
}


void SettingsFrm::HysteresReceived(float val)
{
  glob->mstCfg.hysterese = val;
  mstChanges = true;
}


void SettingsFrm::ShortMeasTimeReceived(float val)
{
  glob->mstCfg.shortMeasTime = val;
  mstChanges = true;
}


void SettingsFrm::DevAlarmBandNrQrReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmBandNrQR.actVal = val;
  sysChanges = true;
}


void SettingsFrm::DevAlarmRetryQrReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmRetryQR.actVal = val;
  sysChanges = true;
}


#ifdef UIT
void SettingsFrm::DevAlarmPctQrReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPctQR.actVal = val;
  sysChanges = true;
}
#endif

void SettingsFrm::B0_CorDevReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corDev.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B0_CorFac1Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corFac1.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B0_CorFac2Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corFac2.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B0_N1Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corN1.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B0_N2Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corN2.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B0_CorMaxReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[0].corMax.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B1_CorDevReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corDev.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B1_CorFac1Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corFac1.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B1_CorFac2Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corFac2.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B1_N1Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corN1.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B1_N2Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corN2.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B1_CorMaxReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[1].corMax.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B2_CorDevReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corDev.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B2_CorFac1Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corFac1.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B2_CorFac2Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corFac2.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B2_N1Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corN1.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B2_N2Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corN2.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B2_CorMaxReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[2].corMax.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B3_CorDevReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corDev.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B3_CorFac1Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corFac1.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B3_CorFac2Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corFac2.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B3_N1Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corN1.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B3_N2Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corN2.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B3_CorMaxReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[3].corMax.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B4_CorDevReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corDev.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B4_CorFac1Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corFac1.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B4_CorFac2Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corFac2.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B4_N1Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corN1.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B4_N2Received(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corN2.actVal = val;
  sysChanges = true;
}


void SettingsFrm::B4_CorMaxReceived(float val)
{
  glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.Bands[4].corMax.actVal = val;
  sysChanges = true;
}

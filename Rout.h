/*================================*/
/* File        : Rout.h           */
/* Description : Various routines */
/*================================*/

#ifndef ROUT_H
#define ROUT_H

#include <QFile>
#include "Common.h"
#include "ImageDef.h"
#include "Glob.h"

#define MAX_ALARM_LOG   40

/*----- Multi screen modes ----*/
#define MULTI_SCR_MODE_SELECT     0                             /* Mode : Select a unit */
#define MULTI_SCR_MODE_STATUS     1                             /* Mode : Status screen */
#define MULTI_SCR_MODE_CONFIG     2                             /* Mode : System config */


/*----- Alarm log structure -----*/
typedef struct
{
  int idx;
  bool full;
  EventLogItemStruct logLine[MAX_ALARM_LOG];
} tAlmLog;

/*----- Production data structure -----*/
typedef struct
{
  byte onOff;
} tProdData;

/*----- Consumption reset structure -----*/
typedef struct
{
  bool batch;                                                                   /* Reset batch consumption */
  bool total;                                                                   /* Reset total consumption */
} tRstConst;

/*----- Pointer to global class -----*/
#ifdef ROUT
global *glob;
#else
extern global *glob;
#endif

class Rout
{
  public:
    static void LcdBrightNess(int value);
    static bool BacklitOn();
    static void SyncFile(void);
    static void LogGuiMsg(tMsgGui *msg);
    static void LogControlMsg(tMsgControl *msg);
    static byte GetMc30AlarmCode (byte alarmNr);
    static void ReadEth0Data(void);
    static word SwapWord(word value);
    static int actLcdBrightNess;
    static int cpuRun;
    static QString ipAddress;
    static QString macAddress;
    static byte ip[4];
    static word mac[3];
    static void WriteAlarmLogFile(void);
    static void ReadAlarmLogFile(void);
    static void WriteProdData(void);
    static void ReadProdData(void);
    static tAlmLog almLog;
    static tProdData prodData;
    static bool almBufFull;
    static int almBufIdx;
    static bool keyLock;
    static QString usbPath;
    static void ConfigureNetwork();
    static void ShowMsg(QString title, QString msgTxt);
    static void CloseMsg();
    static eMcDeviceType ActUnitType();
    static bool TwinMode();
    static void AutoStart();
    static void ResetAlarms();
    static float KgToLbs(float kg);
    static float LbsToKg(float lbs);
    static float GrToLbs(float gr);
    static float LbsToGr(float lbs);
    static float LbsHToGrS(float lbsH);
    static float GrSToLbsH(float grS);
    static bool McWeightPresent();
    static bool MultiUnitHomeScreen();
    static int alarmNr;
    static tRstConst rstCons[CFG_MAX_UNIT_COUNT];
    static QString appPath;
    static bool homeScreenActive;
    static bool mcWeightEditModeActive;
    static byte homeScreenMultiUnitActive;
    static bool almDispMode;
    static bool ImperialMode();
    static void SetUnits();
  private:

    signals:

  public slots:

};
#endif                                                                          // ROUT_H

/********************************************************************************
** Form generated from reading UI file 'Form_QbaseWidget.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_QBASEWIDGET_H
#define UI_FORM_QBASEWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QBaseWidget
{
public:

    void setupUi(QWidget *QBaseWidget)
    {
        if (QBaseWidget->objectName().isEmpty())
            QBaseWidget->setObjectName(QString::fromUtf8("QBaseWidget"));
        QBaseWidget->resize(400, 300);
        QBaseWidget->setCursor(QCursor(Qt::BlankCursor));
        QBaseWidget->setWindowTitle(QString::fromUtf8("Form"));

        retranslateUi(QBaseWidget);

        QMetaObject::connectSlotsByName(QBaseWidget);
    } // setupUi

    void retranslateUi(QWidget *QBaseWidget)
    {
        Q_UNUSED(QBaseWidget);
    } // retranslateUi

};

namespace Ui {
    class QBaseWidget: public Ui_QBaseWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_QBASEWIDGET_H

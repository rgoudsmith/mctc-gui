/********************************************************************************
** Form generated from reading UI file 'Form_MvcSystem.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_MVCSYSTEM_H
#define UI_FORM_MVCSYSTEM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MvcSystem
{
public:
    QGridLayout *gridLayout;
    QLabel *label;

    void setupUi(QWidget *MvcSystem)
    {
        if (MvcSystem->objectName().isEmpty())
            MvcSystem->setObjectName(QString::fromUtf8("MvcSystem"));
        MvcSystem->resize(687, 494);
        MvcSystem->setCursor(QCursor(Qt::BlankCursor));
        MvcSystem->setWindowTitle(QString::fromUtf8("Form"));
        gridLayout = new QGridLayout(MvcSystem);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(1, 1, 1, 1);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(MvcSystem);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        label->setPixmap(QPixmap(QString::fromUtf8("images/system4.png")));
        label->setScaledContents(true);

        gridLayout->addWidget(label, 0, 0, 1, 1);


        retranslateUi(MvcSystem);

        QMetaObject::connectSlotsByName(MvcSystem);
    } // setupUi

    void retranslateUi(QWidget *MvcSystem)
    {
        label->setText(QString());
        Q_UNUSED(MvcSystem);
    } // retranslateUi

};

namespace Ui {
    class MvcSystem: public Ui_MvcSystem {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_MVCSYSTEM_H

#include "Form_RecipeCheckPopup.h"
#include "ui_Form_RecipeCheckPopup.h"
#include "Form_RecipeCheckItem.h"
#include "Form_MainMenu.h"
#include "Rout.h"

RecipeCheckPopupFrm::RecipeCheckPopupFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::RecipeCheckPopupFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);

  /*----- Disable windows title bar -----*/
  this->setWindowFlags(Qt::CustomizeWindowHint);

  memset(&recipe,0x00,sizeof(RecipeStruct));

  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));
}


RecipeCheckPopupFrm::~RecipeCheckPopupFrm()
{
  delete ui;
}


void RecipeCheckPopupFrm::changeEvent(QEvent *e)
{
  if (e->type() == QEvent::LanguageChange) {
    LanguageUpdate();
  }
}


void RecipeCheckPopupFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  glob->SetButtonImage(ui->btOk,itSelection,selOk);
  glob->SetButtonImage(ui->btCancel,itSelection,selCancel);
  LanguageUpdate();
}


void RecipeCheckPopupFrm::LanguageUpdate()
{
  ui->lbCaption->setFont(*(glob->captionFont));

  ui->lbCurMode->setFont(*(glob->baseFont));
  ui->lbRcpMode->setFont(*(glob->baseFont));

  ui->label->setFont(*(glob->baseFont));
  ui->label_2->setFont(*(glob->baseFont));
  ui->label_5->setFont(*(glob->baseFont));
}


bool RecipeCheckPopupFrm::ProcessCheck()
{
  /*------------------------------------------------------------*/
  /* Check the recipe settings against the actual unit settings */
  /*------------------------------------------------------------*/
  bool allOk;
  bool canContinue;
  QString unit,rcp;

  /*----- Default mode OK -----*/
  allOk = true;
  canContinue = true;

  /*----- Unit count mismatch -----*/
  if (recipe.devCount != (glob->mstCfg.slaveCount+1)) {
    allOk = false;
    rcp = tr("Configured number of units : ") + QString::number(recipe.devCount);
    unit = tr("Configured number of units : ")  + QString::number(glob->mstCfg.slaveCount+1);
  }
  else {
    /*----- Configuration mismatch -----*/
    if (((recipe.input) != ((unsigned char)(glob->mstCfg.inpType))) ||
        (recipe.mode != ((unsigned char)(glob->mstCfg.mode)))) {
      switch(((eMode)(recipe.mode))) {
      case modInject:
        rcp = tr("INJ");
        break;
      case modExtrude:
        rcp = tr("EXT");
        break;
      default:
        rcp = tr("INVALID");
        break;
      }
      rcp += "/";

      switch(glob->mstCfg.mode) {
      case modInject:
        unit = tr("INJ");
        break;
      case modExtrude:
        unit = tr("EXT");
        break;
      default:
        unit = tr("INVALID");
        break;
      }
      unit += "/";

      switch(recipe.input) {
      case ttRelay:
        rcp += tr("RELAY");
        break;
      case ttTimer:
        rcp += tr("TIMER");
        break;
      case ttTacho:
        rcp += tr("TACHO");
        break;
      default:
        rcp += tr("INVALID");
        break;
      }

      switch(glob->mstCfg.inpType) {
      case ttRelay:
        unit += tr("RELAY");
        break;
      case ttTimer:
        unit += tr("TIMER");
        break;
      case ttTacho:
        unit += tr("TACHO");
        break;
      default:
        unit += tr("INVALID");
        break;
      }
      allOk = false;
    }
  }
  ui->lbCurMode->setText(unit);
  ui->lbRcpMode->setText(rcp);

  /* if a material file is missing or cannot be read,
       it is not allowed to select the current recipe */
  ui->btOk->setVisible(canContinue);

  return allOk;
  return false;
}


//private slots:
void RecipeCheckPopupFrm::SetRecipe(RecipeStruct *rcp)
{
  memcpy(&recipe,rcp,sizeof(RecipeStruct));
}


void RecipeCheckPopupFrm::ApplyRecipe()
{
  /*------------------------------------------------------------*/
  /* Load current recipe into the systems production structures */
  /*------------------------------------------------------------*/
  QString matName;
  bool isDef;
  int tool,idx;

  /* Set number of units */
  if (glob->mstCfg.slaveCount != recipe.devCount-1) {
    glob->mstCfg.slaveCount = recipe.devCount-1;
#warning Reboot unit message toevoegen !!
  }

  /* Set master configuration Prod Mode (INJ/EXT) + Inp Type (REL/TIM/TACHO/MC_ID) */
  glob->MstCfg_SetProdMode((eMode)(recipe.mode));
  glob->MstCfg_SetInpType((eType)(recipe.input));

  /* Set recipe name */
  glob->MstCfg_SetRecipe(QString::fromAscii(recipe.name));

  for (int idx=0; idx<recipe.devCount; idx++) {

    /* Set shotweight/shottime/maxTacho/extCap (Depending on recipe config) */
    if (((eMode)(recipe.mode)) == modInject) {
      /* Shot Weight */
      glob->SysCfg_SetShotWeight(recipe.var1,idx);

      /* Shot Time */
      glob->SysCfg_SetShotTime(recipe.var2,idx);
    }
    else {
      /* Ext Capacity */
      glob->SysCfg_SetExtCap(recipe.var1,idx);

      /* MaxTacho */
      glob->SysCfg_SetMaxTacho(recipe.var2,idx);
    }
  }

  /*----- Load recipe materials to all units -----*/
  for (idx=0; idx<recipe.devCount; idx++) {

    matName = QString::fromAscii(recipe.lines[idx].matName);
    isDef = recipe.lines[idx].matIsDefault;
    tool = glob->Proc_GetMaterialDosTool(matName,isDef);

    /* Set DosTool according to material */
    glob->SysCfg_SetDosTool((eDosTool)tool,idx);

    /* Set material */
    glob->SysCfg_SetMaterial(matName,isDef,idx);

    /* Set color pct according to recipe */
    glob->SysCfg_SetColPct(recipe.lines[idx].colPct,idx);
  }
  emit RecipeOK();
}



void RecipeCheckPopupFrm::OkClicked()
{
  /*-------------------*/
  /* Apply all changes */
  /*-------------------*/
  ApplyRecipe();

  /* Close this form */
  CancelClicked();
}


void RecipeCheckPopupFrm::CancelClicked()
{
  glob->mstSts.popupActive = false;
  close();
}


//public slots:
void RecipeCheckPopupFrm::CheckRecipe(RecipeStruct * rcp)
{
  SetRecipe(rcp);
  if (!(ProcessCheck())) {
    if (!(glob->mstSts.popupActive)) {
      glob->mstSts.popupActive = true;
      show();
    }
  }
  else {
    ApplyRecipe();
  }
}

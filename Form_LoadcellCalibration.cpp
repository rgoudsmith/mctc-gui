#include "Form_LoadcellCalibration.h"
#include "ui_Form_SystemCalibration.h"
#include <QMessageBox>
#include "Form_MainMenu.h"

LoadcellCalibrationFrm::LoadcellCalibrationFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::LoadcellCalibrationFrm)
{
  showButton = false;

  calibrate = true;
  calibDone = false;
  calState = 0;

  ui->setupUi(this);
  ui->mvcSystem->SetDispIdx(0);
  ui->mvcSystem->SetDisplayType(0);
  ui->mvcSystem->SetMCStatusVisible(false);
  ui->mvcSystem->SetColorPctVisible(false);
  ui->mvcSystem->SetEditColorPct(false);
  ui->mvcSystem->SetDosActVisible(false);
  ui->mvcSystem->SetDosSetVisible(false);
  ui->mvcSystem->SetMaterialVisible(false);
  ui->mvcSystem->SetRPMVisible(false);
  ui->mvcSystem->SetMotorVisible(false);

  ui->mvcSystem->SetMCIDVisible(true);
  ui->mvcSystem->SetWeightVisible(false);
  ui->pbProgress->setVisible(false);

  btCancel = new QPushButton("CANCEL",this);
  btCancel->setFixedSize(90,70);
  btCancel->move((this->width()-99),(this->height()-btCancel->height()));

  btOk = new QPushButton("OK",this);
  btOk->setFixedSize(90,70);
  btOk->move((btCancel->pos().x()-(btOk->width()+9)),btCancel->pos().y());

  lbMsgDisplay = new QLabel(this);
  lbMsgDisplay->setText("MSG LABEL");
  lbMsgDisplay->setFixedSize(400,400);
  lbMsgDisplay->move(this->width()-lbMsgDisplay->width(),this->height()-(lbMsgDisplay->height()+80));

  ui->mvcSystem->SetMotorStatus(0,0);
  ui->mvcSystem->SetMotorStatus(0,1);

  calTim = new QTimer(this);

  calTim->setSingleShot(false);
  calTim->setInterval(500);
  connect(calTim,SIGNAL(timeout()),this,SLOT(timerInterval()));
  calTim->stop();
  ui->pbProgress->setMaximum(10);
  ui->pbProgress->setValue(0);

  connect(btOk,SIGNAL(clicked()),this,SLOT(ButtonClick()));
  connect(btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));
}


LoadcellCalibrationFrm::~LoadcellCalibrationFrm()
{
  calTim->stop();
  delete ui;
}


void LoadcellCalibrationFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}


void LoadcellCalibrationFrm::resizeEvent(QResizeEvent *e)
{
  QWidget::resizeEvent(e);
  ui->mvcSystem->SetDisplayType(0);
  btCancel->move(this->width()-(btCancel->width()+9),this->height()-btCancel->height());
  btOk->move((btCancel->pos().x()-(btOk->width()+9)),btCancel->pos().y());
  lbMsgDisplay->move(this->width()-lbMsgDisplay->width(),this->height()-(lbMsgDisplay->height()+80));
}


void LoadcellCalibrationFrm::showEvent(QShowEvent *e)
{
  QWidget::showEvent(e);
  SetDisplay();
}


void LoadcellCalibrationFrm::SetGBI(globalItems * gbi)
{
  QBaseWidget::SetGBI(gbi);

  ui->mvcSystem->SetGBI(_gbi);

  if (_gbi != 0) {
    setPalette(*(_gbi->Palette()));

    _gbi->SetButtonImage(btOk,itSelection,selOk);
    _gbi->SetButtonImage(btCancel,itSelection,selCancel);

    connect(_gbi,SIGNAL(CalState(eCalState)),this,SLOT(CalStateChanged(eCalState)));

    // Signals / Slots to communication.
    connect(_gbi,SIGNAL(CalStatus(eCalStatus)),this,SLOT(CalStatusReceived(eCalStatus)));
    connect(this,SIGNAL(SendCalCommand(eCalCommand)),_gbi,SLOT(CalSendCommand(eCalCommand)));
    lbMsgDisplay->setFont(*(_gbi->baseFont));
  }
}


void LoadcellCalibrationFrm::DoCalibration(bool cal)
{
  calibrate = cal;
  ui->mvcSystem->SetWeightVisible(!cal);
  if (calState == 9999) {
    calState = 0;
  }
  SetDisplay();
}


void LoadcellCalibrationFrm::SetDisplay()
{

  /*----- Setup unit name for active unit -----*/
  ui->mvcSystem->SetDispIdx(_gbi->ActUnitIndex());

  if (btCancel->isVisible() == true) {
    btOk->move((btCancel->pos().x()-(btOk->width()+9)),btCancel->pos().y());
  }
  else {
    btOk->move(btCancel->pos());
  }

  switch(calState) {
    case 0:                                                                     // Startup / Idle
      calTim->start();
      ui->pbProgress->setVisible(false);
      if (calibrate) {
        lbMsgDisplay->setText(tr("Be sure that:")+"\n"+tr(" - Loadcell is connected")+"\n"+tr(" - Motor is connected")+"\n"+tr(" - Hopper is empty")+"\n\n"+tr("Start loadcell calibration?"));
      }
      else {
        lbMsgDisplay->setText(tr("Actual weight will be reset to zero.")+"\n\n"+tr("This affects:")+"\n"+tr("Level alarms and filling levels.")+"\n\n"+tr("Do you want to continue?"));
      }
      break;

    case 1:                                                                     // Calibration started / busy
      // Hide ok button
      ui->pbProgress->setValue(0);
      btOk->setVisible(false);
      showButton = true;
      btCancel->setVisible(false);
      if (calibrate) {
        // Activate progressBar
        ui->pbProgress->setVisible(true);
        lbMsgDisplay->setText(tr("Calibration busy")+"\n"+tr("Please wait..."));
      }
      else {
        lbMsgDisplay->setText(tr("Actual weight reset to zero."));
      }
      break;

    case 2:                                                                     // Place reference weight / pause
      // Hide progress bar
      ui->pbProgress->setVisible(false);
      // Show ok button
      if (showButton == true) {
        showButton = false;
        btOk->setVisible(true);
      }
      // Show image of calibration weight
      //        ((MainMenu*)(_gbi->menu))->ActivateAlarmBuzzer();
      lbMsgDisplay->setText(tr("Place weight and")+"\n"+tr("continue."));
      break;

    case 3:                                                                     // Continue calibration / busy
      // Show progress bar
      // Hide ok button
      btOk->setVisible(false);
      showButton = true;

      ui->pbProgress->setValue(0);
      ui->pbProgress->setVisible(true);
      lbMsgDisplay->setText(tr("Calibration busy")+"\n"+tr("Please wait..."));
      break;

    case 4:                                                                     // Calibration finished / ready
      if (!calibDone) {
        calTim->stop();
        ui->pbProgress->setValue(ui->pbProgress->maximum());
        ui->pbProgress->setVisible(true);
        btOk->setVisible(true);
        // ((MainMenu*)(_gbi->menu))->ActivateAlarmBuzzer();
        lbMsgDisplay->setText(tr("Calibration ready,")+"\n"+tr("Press Ok to continue."));
      }
      break;

    case 5:                                                                     // Calibration error
      // Geef een melding weer en zorg ervoor dat de calibratie correct wordt afgebroken.
      //lbMsgDisplay->setText(tr("Calibration error")+"\n"+tr(" - check loadcell connection")+"\n\n"+tr("Press continue"));
      if (_gbi->menu != 0) {
        // Loadcell connection error, abort calibration
        emit SendCalCommand(calAbort);

        // Display error
        QString txt;
        _gbi->GetAlarmText(ALM_INDEX_LOADCELL_CON,0,&txt,true);
        ((MainMenu*)(_gbi->menu))->DisplayMessage(_gbi->ActUnitIndex(),ALM_INDEX_LOADCELL_CON,txt,msgtAlarm);
      }
      ui->pbProgress->setVisible(false);
      btOk->setVisible(true);
      calState = 0;
      SetDisplay();
      break;

    case 9999:                                                                  // Save calibration
      // Calibration completed: save results
      //        QMessageBox::warning(this,"Notification","Save results (not yet implemented)",QMessageBox::Ok);
      // and close the window

      // If the window is freed, re-initialization is not required.
      // Window will be re-initialized on creation.

      if (_gbi != 0) {
        ui->pbProgress->setValue(0);
        ui->pbProgress->setVisible(false);
        btCancel->setVisible(true);
        showButton = true;
        // calState = 0;

        ((MainMenu*)(_gbi->menu))->DisplayWindow(wiHome);
      }
      break;
  }
}


void LoadcellCalibrationFrm::ButtonClick()
{
  if (_gbi != 0) {
    if (calibrate == csCalibration) {
      if (calState == 4) {
        _gbi->calStruct.sts = calsReady;
      }

      switch(_gbi->calStruct.sts) {
        case calsIdle:
          btCancel->setVisible(false);
          btOk->setVisible(false);
          emit SendCalCommand(calStart);
          break;
        case calsWaitForRef:
          btOk->setVisible(false);
          emit SendCalCommand(calRefPlaced);
          break;
        case calsReady:
          emit SendCalCommand(calDone);
          _gbi->AddEventItem(_gbi->ActUnitIndex(),2,TXT_INDEX_LOADCELL_CALIBRATED,0,0);
          btCancel->setVisible(true);
          _gbi->calStruct.sts = calsIdle;
          calibDone = true;
          calState = 9999;
          break;
        case calsErr:
          // Eventueel handling voor afbreken van calibratie.
          break;
        default:
          break;
      }
    }
    else {
      calState++;
    }
  }
  SetDisplay();
}


void LoadcellCalibrationFrm::CancelClicked()
{
  if (_gbi != 0) {
    ((MainMenu*)(_gbi->menu))->DisplayWindow(wiBack);
  }
}


void LoadcellCalibrationFrm::CalStateChanged(eCalState cal)
{
  switch (cal) {
    case csNone:
      if (_gbi != 0) {
        // Abort calibration!!!
        //@@ LoadcellCalibrationFrm - Calibration abort not yet implemented
        QMessageBox::warning(this,"Calibration warning","Calibration abort (not yet implemented)",QMessageBox::Ok);
        // Go back to previous window
        (((MainMenu*)_gbi->menu))->DisplayWindow(wiBack);
      }
      break;

    case csCalibration:
      DoCalibration(true);
      break;

    case csZero:
      DoCalibration(false);
      break;
  }
}


void LoadcellCalibrationFrm::CalStatusReceived(eCalStatus sts)
{
  if (_gbi != 0) {
    if (_gbi->calStruct.cmd != calNone) {
      emit SendCalCommand(calNone);
    }
  }

  // Status tijdens loadcell calibratie
  switch(sts) {
    case calsIdle:
      if (calibDone) {
        calibDone = false;
        calState = 0;
      }

      if (calState < 3) {
        //                if (_gbi != 0)
        //                {
        //                    if (_gbi->calStruct.cmd != calNone)
        //                    {
        //                        emit SendCalCommand(calNone);
        //                    }
        //                }
        calState = 0;
      }
      else {
        if (calState == 3) {
          calState++;
        }
      }
      break;
    case calsBusy:
      // Clear command
      //            if (_gbi != 0)
      //            {
      //                if (_gbi->calStruct.cmd != calNone)
      //                {
      //                    emit SendCalCommand(calNone);
      //                }
      //            }
      calState = 1;
      break;
    case calsWaitForRef:
      calState = 2;
      break;
    case calsBusy2:
      // Clear command
      //            if (_gbi != 0)
      //            {
      //                if (_gbi->calStruct.cmd != calNone)
      //                {
      //                    emit SendCalCommand(calNone);
      //                }
      //            }
      calState = 3;
      break;
    case calsReady:
      calState = 4;
      break;

    case calsErr:
      //            if (_gbi != 0)
      //            {
      //                if (_gbi->calStruct.cmd != calNone)
      //                {
      //                    emit SendCalCommand(calNone);
      //                }
      //            }
      calState = 5;                                                             // Calibration error state.
      break;

    default:
      QMessageBox::warning(this,"Calibration warning","Invalid calibration status received",QMessageBox::Ok);
      break;
  }
  SetDisplay();
}


void LoadcellCalibrationFrm::timerInterval()
{
  if (ui->pbProgress->isVisible()) {
    int max = ui->pbProgress->maximum();
    int pos = ui->pbProgress->value();

    if (ui->pbProgress->layoutDirection() == Qt::LeftToRight) {
      if (pos == max) {
        ui->pbProgress->setLayoutDirection(Qt::RightToLeft);
      }
      ui->pbProgress->setValue(pos+1);
    }
    else {
      if (pos == 0) {
        ui->pbProgress->setLayoutDirection(Qt::LeftToRight);
      }
      else {
        ui->pbProgress->setValue(pos-1);
      }
    }
  }
}

#ifndef QBASEWIDGET_H
#define QBASEWIDGET_H

#include <QWidget>
#include <QTime>
#include "Glob.h"
#include "ImageDef.h"

namespace Ui
{
  class QBaseWidget;
}


class QBaseWidget : public QWidget
{
  Q_OBJECT

    public:
    explicit QBaseWidget(QWidget *parent = 0);
    ~QBaseWidget();

  protected:
    //    global * _gbi;
    virtual void ConnectKeyBeep();
    virtual void ConnectChildren(QObject*);
    virtual void ConnectChildren(QObject* ,const QString &);

  private:
    Ui::QBaseWidget *ui;
    int dbgLvl;

  public slots:
    void KeyBeep();
};
#endif                                                                          // QBASEWIDGET_H

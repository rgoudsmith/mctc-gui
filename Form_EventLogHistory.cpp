#include "Form_EventLogHistory.h"
#include "ui_Form_EventLogHistory.h"
#include <QVarLengthArray>
#include "Form_MainMenu.h"
#include "Form_EventLogItem.h"
#include "QmVcScrollbar.h"
#include "Rout.h"

#define EVENT_LOG_TIMER 600

EventLogHistoryFrm::EventLogHistoryFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::EventLogHistoryFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  blState = 0;

  ui->setupUi(this);
  curScroll = 0;
  maxScroll = 0;

  layout = new QVBoxLayout(ui->scrollAreaWidgetContents);
  layout->setDirection(QBoxLayout::BottomToTop);

  QMvcScrollBar * bar = new QMvcScrollBar(ui->scrollArea);
  connect(bar,SIGNAL(MouseButtonPressed()),this,SLOT(KeyBeep()));
  ui->scrollArea->setVerticalScrollBar(bar);

  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(ui->btReset,SIGNAL(clicked()),this,SLOT(ResetClicked()));
}


EventLogHistoryFrm::~EventLogHistoryFrm()
{
  delete ui;
}


void EventLogHistoryFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}


void EventLogHistoryFrm::showEvent(QShowEvent *e)
{
  eUserType ut;

  QBaseWidget::showEvent(e);

  /*----- Display screen title -----*/
  if (!Rout::almDispMode) {
    ui->lbTitle->setText("Active alarms");
  }
  else {
    ui->lbTitle->setText("Historic alarms");
  }

  /*----- Alarm log button only visible at MovaColor or Agent level -----*/
  ut = glob->GetUser();
  switch (ut) {
    case utMovaColor:
    case utSyrinx:
    case utAgent:
      ui->btReset->setVisible(true);
      break;
    default:
      ui->btReset->setVisible(false);
  }

  /*----- Reset button only active in active alarm display mode -----*/
  if (!Rout::almDispMode) {
    ui->btReset->setVisible(false);
  }

  LoadAllItems();
}


void EventLogHistoryFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;
  ui->lbTitle->setFont(*(glob->captionFont));
  glob->SetButtonImage(ui->btOk,itSelection,selOk);
}


void EventLogHistoryFrm::OkClicked()
{
  ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
}


void EventLogHistoryFrm::ResetClicked()
{
  /*--------------------------------*/
  /* Reset alarm log button pressed */
  /*--------------------------------*/

  /*----- Reset the alarm log -----*/
  Rout::almLog.idx = 0;
  Rout::almLog.full = FALSE;
  Rout::WriteAlarmLogFile();

  LoadAllItems();
}


void EventLogHistoryFrm::ClearItems()
{
  /*---------------------*/
  /* Clear the item list */
  /*---------------------*/
  int cnt,i;
  QWidget* item;

  /*----- Fetch itemlist size -----*/
  cnt = itemList.size();

  /*----- Clear items -----*/
  for (i=0;i<cnt;i++) {
    item = itemList.at(i);
    delete item;
  }

  itemList.clear();
}


void EventLogHistoryFrm::LoadAllItems()
{
  /*-------------------------------------------------------*/
  /* Build the historic alarm item list from the alarm log */
  /*-------------------------------------------------------*/
  int i;
  EventLogItemFrm *item;
  EventLogItemStruct almData;

  /*----- Clear old item list ----*/
  ClearItems();

  /*----- Show a message box -----*/
  Rout::ShowMsg(tr("Working"),tr("Please wait.... "));

  /*----- Check for active or historic alarm display mode -----*/
  if (!Rout::almDispMode) {
    /*----- Display all actual alarm items -----*/
    for (i=0; i<glob->mstCfg.slaveCount+1; i++) {
      if (glob->sysSts[i].alarmSts.nr != 0) {
        item = new EventLogItemFrm(ui->scrollAreaWidgetContents);
        item->FormInit();

        almData.alarmNr = glob->sysSts[i].alarmSts.nr;
        almData.dateTim = QDateTime::currentDateTime().toTime_t();
        almData.eventID = 0;
        almData.msgType = glob->sysSts[i].alarmSts.mode;
        almData.newValue = 0;
        almData.oldValue = 0;
        almData.unitIdx = i;

        /*----- display alarm log line -----*/
        item->SetData(&almData);
        item->setStyleSheet("QGroupBox{border-color: black; border: 3px solid; border-radius:3px;}");
        layout->addWidget(item);
        itemList.append(item);
      }
    }
  }
  /*----- Display all historic alarm log items -----*/
  else {
    for (i=0; i<Rout::almLog.idx; i++) {
      item = new EventLogItemFrm(ui->scrollAreaWidgetContents);
      item->FormInit();

      /*----- display alarm log line -----*/
      item->SetData(&Rout::almLog.logLine[i]);
      item->setStyleSheet("QGroupBox{border-color: black; border: 3px solid; border-radius:3px;}");
      layout->addWidget(item);
      itemList.append(item);
    }
  }

  /*----- Close the message box -----*/
  Rout::CloseMsg();
}

/****************************************************************************
** Meta object code from reading C++ file 'Form_FileSelect.h'
**
** Created: Wed Feb 6 11:14:58 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_FileSelect.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_FileSelect.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FileSelectFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,
      38,   14,   14,   14, 0x05,
      55,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      76,   14,   14,   14, 0x08,
      88,   14,   14,   14, 0x08,
     104,   14,   14,   14, 0x08,
     117,   14,   14,   14, 0x08,
     133,   14,   14,   14, 0x08,
     152,   14,   14,   14, 0x08,
     168,   14,   14,   14, 0x08,
     179,   14,   14,   14, 0x08,
     192,   14,   14,   14, 0x0a,
     208,   14,   14,   14, 0x0a,
     221,   14,   14,   14, 0x0a,
     237,   14,   14,   14, 0x0a,
     253,   14,   14,   14, 0x0a,
     269,   14,   14,   14, 0x0a,
     287,   14,   14,   14, 0x0a,
     304,   14,   14,   14, 0x0a,
     329,   14,   14,   14, 0x0a,
     362,   14,   14,   14, 0x0a,
     385,   14,   14,   14, 0x0a,
     404,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_FileSelectFrm[] = {
    "FileSelectFrm\0\0SelectClicked(QString)\0"
    "NewFileClicked()\0ItemRenamed(QString)\0"
    "OkClicked()\0SearchClicked()\0NewClicked()\0"
    "DeleteClicked()\0DeleteAllClicked()\0"
    "RenameClicked()\0ScrollUp()\0ScrollDown()\0"
    "CanSearch(bool)\0CanNew(bool)\0"
    "CanRename(bool)\0CanDelete(bool)\0"
    "CanAccept(bool)\0ForceAccept(bool)\0"
    "RefreshDisplay()\0SetSelectedFile(QString)\0"
    "CfgCurrentUserChanged(eUserType)\0"
    "SysActiveChanged(bool)\0MessageResult(int)\0"
    "TextReceived(QString)\0"
};

const QMetaObject FileSelectFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_FileSelectFrm,
      qt_meta_data_FileSelectFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FileSelectFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FileSelectFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FileSelectFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FileSelectFrm))
        return static_cast<void*>(const_cast< FileSelectFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int FileSelectFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: SelectClicked((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: NewFileClicked(); break;
        case 2: ItemRenamed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: OkClicked(); break;
        case 4: SearchClicked(); break;
        case 5: NewClicked(); break;
        case 6: DeleteClicked(); break;
        case 7: DeleteAllClicked(); break;
        case 8: RenameClicked(); break;
        case 9: ScrollUp(); break;
        case 10: ScrollDown(); break;
        case 11: CanSearch((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: CanNew((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: CanRename((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: CanDelete((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: CanAccept((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: ForceAccept((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 17: RefreshDisplay(); break;
        case 18: SetSelectedFile((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 19: CfgCurrentUserChanged((*reinterpret_cast< eUserType(*)>(_a[1]))); break;
        case 20: SysActiveChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 21: MessageResult((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: TextReceived((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 23;
    }
    return _id;
}

// SIGNAL 0
void FileSelectFrm::SelectClicked(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void FileSelectFrm::NewFileClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void FileSelectFrm::ItemRenamed(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE

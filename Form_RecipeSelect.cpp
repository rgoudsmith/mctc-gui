#include "Form_RecipeSelect.h"
#include "ui_Form_RecipeSelect.h"
#include "Form_MainMenu.h"
#include "Rout.h"

RecipeSelectFrm::RecipeSelectFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::RecipeSelectFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);

  connect(ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));

  ui->FileSelect->SetFileExt(QString(RECIPE_EXTENSION));
  ui->FileSelect->CanSearch(true);
  ui->FileSelect->CanNew(true);
  ui->FileSelect->CanDelete(true);
  ui->FileSelect->CanRename(true);
  ui->FileSelect->ForceAccept(true);
}


RecipeSelectFrm::~RecipeSelectFrm()
{
  /*------------*/
  /* Destructor */
  /*------------*/
  delete ui;
}


void RecipeSelectFrm::showEvent(QShowEvent *e)
{
  QBaseWidget::showEvent(e);

  ui->FileSelect->searchString = "";      /* Clear recipe search string */
  RefreshFileList();
}


void RecipeSelectFrm::changeEvent(QEvent *e)
{
  QBaseWidget::changeEvent(e);
  if (e->type() == QEvent::LanguageChange) {
    ui->retranslateUi(this);
  }
}


void RecipeSelectFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  ui->FileSelect->FormInit();
  QString path;
  GetDefaultRecipeFolder(&path);
  ui->FileSelect->SetFilePath(path);
  glob->SetButtonImage(ui->btCancel,itSelection,selCancel);
  connect(ui->FileSelect,SIGNAL(SelectClicked(QString)),this,SLOT(SelectClicked(QString)));
  connect(ui->FileSelect,SIGNAL(NewFileClicked()),this,SLOT(NewRecipeClicked()));
  connect(ui->FileSelect,SIGNAL(ItemRenamed(QString)),this,SLOT(RecipeRenamed(QString)));
  ui->FileSelect->RefreshDisplay();

  ui->lbCaption->setFont(*(glob->captionFont));
}


void RecipeSelectFrm::GetDefaultRecipeFolder(QString *path)
{
  *path = (*(glob->AppPath()) + QString(RECIPE_DEFAULT_FOLDER));
}


void RecipeSelectFrm::CancelClicked()
{
  /*-----------------------------------*/
  /* BUTTON : Cancel clicked           */
  /*-----------------------------------*/
  /* Leave the recipe selection screen */
  /*-----------------------------------*/

  if (!Rout::MultiUnitHomeScreen()) {
    ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
  }
  else {
    ((MainMenu*)(glob->menu))->DisplayWindow(wiHomeMultiUnit);
  }

}


void RecipeSelectFrm::SelectClicked(const QString &file)
{
  /*--------------------------------*/
  /* BUTTON : Recipe select clicked */
  /*--------------------------------*/
  /* Load the selected receipe      */
  /*--------------------------------*/
  // emit the select recipe
  glob->selectedRecipe = file;
  //                emit SelectedItem(file);
  // Open the rcpConfig window for further processing
  ((MainMenu*)(glob->menu))->DisplayWindow(wiRcpConfig);
}


void RecipeSelectFrm::NewRecipeClicked()
{
  /*-----------------------------*/
  /* BUTTON : New recipe clicked */
  /*-----------------------------*/
  /* Create a new recipe         */
  /*-----------------------------*/
  SelectClicked("");
}


void RecipeSelectFrm::RecipeRenamed(const QString &txt)
{
  /*----- Read file from filesystem -----*/
  QString name = *(glob->AppPath()) + RECIPE_DEFAULT_FOLDER + glob->fileSeparator + txt;

  RecipeStruct rcp;
  ((MainMenu*)(glob->menu))->fileSysCtrl.LoadRecipe(&rcp,name);
  /*----- Check checksum -----*/
  if (((MainMenu*)(glob->menu))->fileSysCtrl.CheckChecksum((char*)(&rcp),(sizeof(RecipeStruct)-sizeof(int)),rcp.chksum)) {
    /*----- Alter internal filename to new filename -----*/
    memcpy(rcp.name,txt.toAscii(),txt.length());
    /*----- Recalculate checksum -----*/
    rcp.chksum = ((MainMenu*)(glob->menu))->fileSysCtrl.CalculateChecksum((char*)(&rcp),(sizeof(RecipeStruct)-sizeof(int)));
    /*----- Overwrite old recipe file with new one -----*/
    ((MainMenu*)(glob->menu))->fileSysCtrl.SaveRecipe(&rcp,name);
  }
}


/*--- Button recipe from HOME form ---*/
void RecipeSelectFrm::ShowWithRecipe(const QString &file)
{
  /*----- Read recipes from filesystem -----*/
  ui->FileSelect->RefreshDisplay();
  /*----- Select correct recipe -----*/
  ui->FileSelect->SetSelectedFile(file);
  /*----- Show window -----*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiRcpSelect);
}


void RecipeSelectFrm::RefreshFileList()
{
  ui->FileSelect->RefreshDisplay();
}

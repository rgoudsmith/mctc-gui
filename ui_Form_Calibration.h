/********************************************************************************
** Form generated from reading UI file 'Form_Calibration.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_CALIBRATION_H
#define UI_FORM_CALIBRATION_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStackedWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CalibrationFrm
{
public:
    QVBoxLayout *verticalLayout;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QHBoxLayout *horizontalLayout_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLabel *lbVal1;
    QLabel *lbUnits1;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QLabel *lbVal2;
    QLabel *lbUnits2;
    QSpacerItem *verticalSpacer_2;
    QLabel *lbProcess;
    QLabel *lbTest;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *lbMessage;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer;
    QPushButton *btCommand;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QWidget *CalibrationFrm)
    {
        if (CalibrationFrm->objectName().isEmpty())
            CalibrationFrm->setObjectName(QString::fromUtf8("CalibrationFrm"));
        CalibrationFrm->resize(232, 463);
        QFont font;
        font.setUnderline(false);
        CalibrationFrm->setFont(font);
        CalibrationFrm->setCursor(QCursor(Qt::BlankCursor));
        CalibrationFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout = new QVBoxLayout(CalibrationFrm);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        stackedWidget = new QStackedWidget(CalibrationFrm);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setMaximumSize(QSize(16777215, 16777215));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        page->setCursor(QCursor(Qt::BlankCursor));
        horizontalLayout_3 = new QHBoxLayout(page);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        groupBox = new QGroupBox(page);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        groupBox->setMinimumSize(QSize(170, 0));
        groupBox->setMaximumSize(QSize(16777215, 16777215));
        groupBox->setCursor(QCursor(Qt::BlankCursor));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(0, 30));

        horizontalLayout_2->addWidget(label);

        lbVal1 = new QLabel(groupBox);
        lbVal1->setObjectName(QString::fromUtf8("lbVal1"));
        lbVal1->setMinimumSize(QSize(0, 30));
        lbVal1->setText(QString::fromUtf8("lbVal1"));

        horizontalLayout_2->addWidget(lbVal1);

        lbUnits1 = new QLabel(groupBox);
        lbUnits1->setObjectName(QString::fromUtf8("lbUnits1"));
        lbUnits1->setMinimumSize(QSize(0, 30));
        lbUnits1->setText(QString::fromUtf8("g/sec"));

        horizontalLayout_2->addWidget(lbUnits1);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(0, 30));

        horizontalLayout->addWidget(label_2);

        lbVal2 = new QLabel(groupBox);
        lbVal2->setObjectName(QString::fromUtf8("lbVal2"));
        lbVal2->setMinimumSize(QSize(0, 30));
        lbVal2->setText(QString::fromUtf8("lbVal2"));

        horizontalLayout->addWidget(lbVal2);

        lbUnits2 = new QLabel(groupBox);
        lbUnits2->setObjectName(QString::fromUtf8("lbUnits2"));
        lbUnits2->setMinimumSize(QSize(0, 30));
        lbUnits2->setText(QString::fromUtf8("g/sec"));

        horizontalLayout->addWidget(lbUnits2);


        verticalLayout_2->addLayout(horizontalLayout);

        verticalSpacer_2 = new QSpacerItem(20, 90, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_2);

        lbProcess = new QLabel(groupBox);
        lbProcess->setObjectName(QString::fromUtf8("lbProcess"));
        lbProcess->setMinimumSize(QSize(0, 70));
        lbProcess->setMaximumSize(QSize(16777215, 80));
        lbProcess->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(lbProcess);

        lbTest = new QLabel(groupBox);
        lbTest->setObjectName(QString::fromUtf8("lbTest"));
        lbTest->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(lbTest);


        horizontalLayout_3->addWidget(groupBox);

        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        page_2->setCursor(QCursor(Qt::BlankCursor));
        verticalLayout_3 = new QVBoxLayout(page_2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        lbMessage = new QLabel(page_2);
        lbMessage->setObjectName(QString::fromUtf8("lbMessage"));
        lbMessage->setText(QString::fromUtf8("-"));

        verticalLayout_3->addWidget(lbMessage);

        stackedWidget->addWidget(page_2);

        verticalLayout->addWidget(stackedWidget);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(0);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalSpacer = new QSpacerItem(1, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        btCommand = new QPushButton(CalibrationFrm);
        btCommand->setObjectName(QString::fromUtf8("btCommand"));
        btCommand->setMinimumSize(QSize(150, 70));
        btCommand->setMaximumSize(QSize(150, 70));
        btCommand->setBaseSize(QSize(0, 0));
        btCommand->setText(QString::fromUtf8("Stop && Save"));

        horizontalLayout_4->addWidget(btCommand);

        horizontalSpacer_2 = new QSpacerItem(1, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_4);


        retranslateUi(CalibrationFrm);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(CalibrationFrm);
    } // setupUi

    void retranslateUi(QWidget *CalibrationFrm)
    {
        groupBox->setTitle(QString());
        label->setText(QApplication::translate("CalibrationFrm", "set:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("CalibrationFrm", "act:", 0, QApplication::UnicodeUTF8));
        lbProcess->setText(QApplication::translate("CalibrationFrm", "-", 0, QApplication::UnicodeUTF8));
        lbTest->setText(QApplication::translate("CalibrationFrm", "-", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(CalibrationFrm);
    } // retranslateUi

};

namespace Ui {
    class CalibrationFrm: public Ui_CalibrationFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_CALIBRATION_H

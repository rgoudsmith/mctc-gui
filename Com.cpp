#define COM

#include "Com.h"
#include "errno.h"
#include "qdebug.h"
#include "Rout.h"

#define COM_DELAY_TIME 5
#define COM_MSG_SIZE 864
#define SRV_QUEUE_ID 1234
#define CLT_QUEUE_ID 4321

void Com::OpenMsgQueue()
{
  /*------------------------------------------------------------------------*/
  /* Open a message queue to send data between the GUI and the Control task */
  /*------------------------------------------------------------------------*/
  this->srvQueueID = msgget((key_t)SRV_QUEUE_ID, 0666 | IPC_CREAT);
  this->cltQueueID = msgget((key_t)CLT_QUEUE_ID, 0666 | IPC_CREAT);
}


Com::Com(QObject *parent) :QObject(parent)
{
  /*------------*/
  /* Constuctor */
  /*------------*/
  this->end = false;

  /*----- Clear incoming message buffer -----*/
  memset(&msgIn,0,sizeof(msgIn));

  /*----- Setup other variables -----*/
  this->srvQueueID = -1;
  this->cltQueueID = -1;

  /*----- Setup message ID's -----*/
  this->msgReceive.msg_id = 1;
  this->msgSend.msg_id = 1;

  /*----- Open message queue -----*/
  OpenMsgQueue();
}


void Com::SendMsg(tMsgGui msg)
{
  /*--------------------------------------------------*/
  /* Send and receive a message to the MC_TC-arm task */
  /*--------------------------------------------------*/

  bool systemError;

  /*--------------*/
  /* Send message */
  /*--------------*/

  systemError = FALSE;

  /*----- Copy message data into the send buffer -----*/
  memcpy(&msgSend.common,&msg.common,sizeof(msg)-sizeof(msg.msg_id));

  /*----- Send the message -----*/
  if (msgsnd(srvQueueID, (void*)&(this->msgSend),sizeof(msg)-sizeof(msg.msg_id),IPC_NOWAIT) <= -1) {
    /*----- Send message error handling from here -----*/
    systemError = TRUE;
  }
  /*----- Message send ok -----*/
  else {
    Rout::LogGuiMsg(&msg);                                                      /* Log GUI data */
  }

  /*-----------------*/
  /* Receive message */
  /*-----------------*/

  /*----- Receive message buffer, except the first long msgReceive.msg_id -----*/
  if (msgrcv(cltQueueID, (void*) &(this->msgReceive),sizeof(this->msgReceive)-sizeof(this->msgReceive.msg_id),this->msgReceive.msg_id,IPC_NOWAIT) <= -1) {
    /*----- Receive message error from here -----*/
    systemError = TRUE;
  }
  else {
    /*----- Copy RX message data to internal struct, skip first long --> msgReceive.msg_id -----*/
    memcpy(&msgIn,&msgReceive,sizeof(msgIn));
    Rout::LogControlMsg(&msgIn);                                                /* Log Control data */

    /*----- Signal message received -----*/
    emit ComReceived(msgIn);
  }

  /*----- Update system run counter -----*/
  if (!systemError) {
    Rout::cpuRun++;
  }
}

#include "Form_AdvLoaderSettings.h"
#include "ui_Form_AdvLoaderSettings.h"
#include "Form_MainMenu.h"
#include "Rout.h"

AdvLoaderSettingsFrm::AdvLoaderSettingsFrm(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::AdvLoaderSettingsFrm)
{

  /*-------------*/
  /* Constructor */
  /*-------------*/
  selForm = 0;
  numInput = 0;

  // Init variables
  cfgDecimals = 1;
  cfgMinInput = 0;
  cfgMaxInput = 999.9;
  lineIdx = -1;

  // Setup UI
  m_ui->setupUi(this);

  // Numeric input form
  numInput = new NumericInputFrm();
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
  connect(numInput,SIGNAL(InputValue(float)),this,SLOT(ValueReturned(float)));

  // Selection input form
  selForm = new selectionFrm();
  selForm->move(((width()/2)-(selForm->width()/2)),((height()/2)-(selForm->height()/2)));
  selForm->hide();

  // Link event filter to line edits
  m_ui->edFillTime->installEventFilter(this);
  m_ui->edEmptyTime->installEventFilter(this);
  m_ui->edAlarmTime->installEventFilter(this);

  // Cancel button has no function anymore since values are immediately updated on change.
  m_ui->btCancel->setVisible(false);

  connect(m_ui->btAlmMode,SIGNAL(clicked()),this,SLOT(AlarmModeClicked()));
  connect(m_ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(m_ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));
  connect(selForm,SIGNAL(selectionMade(int,int)),this,SLOT(SelectionReceived(int,int)));

  DisplayAlarmMode();
}


AdvLoaderSettingsFrm::~AdvLoaderSettingsFrm()
{
  delete numInput;
  delete m_ui;
}


void AdvLoaderSettingsFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      m_ui->retranslateUi(this);
      LanguageUpdate();
      break;
    default:
      break;
  }
}


bool AdvLoaderSettingsFrm::eventFilter(QObject *obj, QEvent *ev)
{
  /*
      Event filter is used to catch touchscreen presses on textEdits
      so we can display an input form for the pressed control.
  */
  if (ev->type() == QEvent::MouseButtonPress) {
    // Depending on the pressed textEdit,
    // Set the lineIdx to the correct value
    // And load the current value in val.
    lineIdx = -1;
    float val = 0;
    if (obj == m_ui->edFillTime) {
      lineIdx = 0;
      val = m_ui->edFillTime->text().toFloat();
    }

    if (obj == m_ui->edEmptyTime) {
      lineIdx = 1;
      val = m_ui->edEmptyTime->text().toFloat();
    }

    if (obj == m_ui->edAlarmTime) {
      lineIdx = 2;
      val = m_ui->edAlarmTime->text().toFloat();
    }

    // Display the numeric input window with the stored value
    if (lineIdx >= 0) {
      KeyBeep();
      numInput->SetDisplay(true,cfgDecimals,val,cfgMinInput,cfgMaxInput);
      numInput->show();
      return true;
    }
  }
  return QWidget::eventFilter(obj,ev);
}


void AdvLoaderSettingsFrm::resizeEvent(QResizeEvent *e)
{
  QWidget::resizeEvent(e);
  // Center input forms on the screen
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
  selForm->move(((width()/2)-(selForm->width()/2)),((height()/2)-(selForm->height()/2)));
  DisplayValues();
}


void AdvLoaderSettingsFrm::showEvent(QShowEvent *e)
{
  SetFillSystem();
  QWidget::showEvent(e);
}


void AdvLoaderSettingsFrm::LanguageUpdate()
{
  // Set correct font to all labels
  m_ui->lbCaption->setFont(*(glob->captionFont));
  m_ui->edFillTime->setFont(*(glob->baseFont));
  m_ui->edEmptyTime->setFont(*(glob->baseFont));
  m_ui->edAlarmTime->setFont(*(glob->baseFont));
  m_ui->lbUnit1->setFont(*(glob->baseFont));
  m_ui->lbUnit2->setFont(*(glob->baseFont));
  m_ui->lbUnit3->setFont(*(glob->baseFont));
  m_ui->lbFillTime->setFont(*(glob->baseFont));
  m_ui->lbEmptyTime->setFont(*(glob->baseFont));
  m_ui->lbAlarmTime->setFont(*(glob->baseFont));
  m_ui->lbAlarmMode->setFont(*(glob->baseFont));

  // Display correct images on buttons
  glob->SetButtonImage(m_ui->btOk,itSelection,selOk);
  glob->SetButtonImage(m_ui->btCancel,itSelection,selCancel);
}


void AdvLoaderSettingsFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  // Set glob to input forms
  numInput->FormInit();
  selForm->FormInit();

  LanguageUpdate();
  //setPalette(*(glob->Palette()));
  //  SetFillSystem(glob->sysCfg[0].fillSystem.fillSysType);

  // Connect signals and slots
  connect(glob,SIGNAL(CfgFill_AlarmModeChanged(bool,int)),this,SLOT(CfgFill_AlarmModeChanged(bool,int)));
  connect(glob,SIGNAL(CfgFill_AlmTimeChanged(float,int)),this,SLOT(CfgFill_AlarmTimeChanged(float,int)));
  connect(glob,SIGNAL(CfgFill_EmptyTimeChanged(float,int)),this,SLOT(CfgFill_EmptyTimeChanged(float,int)));
  connect(glob,SIGNAL(CfgFill_TimeMeChanged(float,int)),this,SLOT(CfgFill_TimeMeChanged(float,int)));
  connect(glob,SIGNAL(CfgFill_TimeMvChanged(float,int)),this,SLOT(CfgFill_TimeMvChanged(float,int)));

  // Load settings for the 1st time
  //LoadSettings();

  DisplayValues();
}


void AdvLoaderSettingsFrm::ValueReturned(float val)
{
  // Input form returns a value.
  // Update the value corresponding to lineIdx.
  switch (lineIdx) {
    case 0:
      if (glob->sysCfg[glob->ActUnitIndex()].fillSystem.fillSysType == fsME) {
        glob->CfgFill_SetFillTimeMe(val,glob->ActUnitIndex());
      }

      if (glob->sysCfg[glob->ActUnitIndex()].fillSystem.fillSysType == fsMV) {
        glob->CfgFill_SetFillTimeMv(val,glob->ActUnitIndex());
      }
      break;

    case 1:
      glob->CfgFill_SetEmptyTime(val,glob->ActUnitIndex());
      break;

    case 2:
      glob->CfgFill_SetAlarmTime(val,glob->ActUnitIndex());
      break;
  }
  DisplayValues();
}


void AdvLoaderSettingsFrm::SetAlarmMode(bool alarmMode)
{
  glob->sysCfg[glob->ActUnitIndex()].fillSystem.alarmMode = alarmMode;
  DisplayAlarmMode();
}


void AdvLoaderSettingsFrm::AlarmModeClicked()
{
  selForm->SetFormCaption(tr("Select Alarm Mode"));
  selForm->DisplaySelection(itActive,(glob->sysCfg[glob->ActUnitIndex()].fillSystem.alarmMode == true ? actYes : actNo),glob->ActUnitIndex());
  selForm->show();
}


void AdvLoaderSettingsFrm::DisplayAlarmMode()
{
  glob->SetButtonImage(m_ui->btAlmMode,itActive,(glob->sysCfg[glob->ActUnitIndex()].fillSystem.alarmMode == true ? actYes : actNo));
}


void AdvLoaderSettingsFrm::DisplayValues()
{
  // Display all values.
  // fillTime is fillSystem dependant, so for ME and MV we use different values.
  // Other values (emptyTime and alarmTime) are always the same values.
  switch(glob->sysCfg[glob->ActUnitIndex()].fillSystem.fillSysType) {
    case fsME:
      m_ui->edFillTime->setText(QString("%1").arg(glob->sysCfg[glob->ActUnitIndex()].fillSystem.fillTimeME,0,'f',cfgDecimals));
      break;
    case fsMV:
      m_ui->edFillTime->setText(QString("%1").arg(glob->sysCfg[glob->ActUnitIndex()].fillSystem.fillTimeMV,0,'f',cfgDecimals));
      break;
    default:
      break;
  }
  m_ui->edEmptyTime->setText(QString("%1").arg(glob->sysCfg[glob->ActUnitIndex()].fillSystem.emptyTime,0,'f',cfgDecimals));
  m_ui->edAlarmTime->setText(QString("%1").arg(glob->sysCfg[glob->ActUnitIndex()].fillSystem.alarmTime,0,'f',cfgDecimals));
  DisplayAlarmMode();
}


void AdvLoaderSettingsFrm::SetFillSystem()
{
  /*--------------------------------------------------------*/
  /* Update the visual display according to fillSystem type */
  /*--------------------------------------------------------*/
  int idx;
  eFillSys fillSys;

  idx = glob->ActUnitIndex();

  /*----- Force McWeight to EX fillsystem type -----*/
  if (Rout::ActUnitType() == mcdtWeight) {
    fillSys = fsEX;
  }
  else {
    fillSys = glob->sysCfg[idx].fillSystem.fillSysType;
  }

  switch(fillSys) {
    case fsNone:
      break;
    case fsME:
      m_ui->lbFillTime->setVisible(false);
      m_ui->edFillTime->setVisible(false);
      m_ui->lbUnit1->setVisible(false);
      m_ui->lbEmptyTime->setVisible(false);
      m_ui->edEmptyTime->setVisible(false);
      m_ui->lbUnit2->setVisible(false);
      break;
    case fsMV:
      m_ui->lbFillTime->setVisible(true);
      m_ui->edFillTime->setVisible(true);
      m_ui->lbUnit1->setVisible(true);
      m_ui->lbEmptyTime->setVisible(true);
      m_ui->edEmptyTime->setVisible(true);
      m_ui->lbUnit2->setVisible(true);
      break;
    case fsEX:
      m_ui->lbFillTime->setVisible(false);
      m_ui->edFillTime->setVisible(false);
      m_ui->lbUnit1->setVisible(false);
      m_ui->lbEmptyTime->setVisible(false);
      m_ui->edEmptyTime->setVisible(false);
      m_ui->lbUnit2->setVisible(false);
      break;
    default:
      break;
  }
}


void AdvLoaderSettingsFrm::CfgFill_AlarmModeChanged(bool, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    DisplayValues();
  }
}


void AdvLoaderSettingsFrm::CfgFill_TimeMeChanged(float, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    // Update graphics
    if (fillSys.fillSysType == fsME) {
      DisplayValues();
    }
  }
}


void AdvLoaderSettingsFrm::CfgFill_TimeMvChanged(float, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    // Update graphics
    if (fillSys.fillSysType == fsMV) {
      DisplayValues();
    }
  }
}


void AdvLoaderSettingsFrm::CfgFill_AlarmTimeChanged(float, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    DisplayValues();
  }
}


void AdvLoaderSettingsFrm::CfgFill_EmptyTimeChanged(float, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    DisplayValues();
  }
}


void AdvLoaderSettingsFrm::OkClicked()
{
  // Store settings into GBI (not needed anymore, local buffering of values is not active anymore)
  CancelClicked();
}


void AdvLoaderSettingsFrm::CancelClicked()
{
  ((MainMenu*)(glob->menu))->DisplayWindow(wiLevels);
}


void AdvLoaderSettingsFrm::SelectionReceived(int _type, int _idx)
{
  if (_type == itActive) {
    glob->CfgFill_SetAlarmMode((_idx == actYes ? true : false),glob->ActUnitIndex());
  }
}

#ifndef RECIPECHECKITEMFRM_H
#define RECIPECHECKITEMFRM_H

#include "Form_QbaseWidget.h"

namespace Ui
{
  class RecipeCheckItemFrm;
}


class RecipeCheckItemFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit RecipeCheckItemFrm(QWidget *parent = 0);
    ~RecipeCheckItemFrm();
    void FormInit();

  private:
    Ui::RecipeCheckItemFrm *ui;

  public slots:
    void SetDevID(const QString &);
    void SetCurDosTool(eDosTool);
    void SetRcpDosTool(eDosTool);
};
#endif                                                                          // RECIPECHECKITEMFRM_H

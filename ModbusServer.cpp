/*===========================================================================*
 * File        : ModbusServer.cpp                                            *
 * Project     : MVC0012 - MC-TC                                             *
 * Description : Modbus server.                                              *
 *===========================================================================*/

#include <QtNetwork>
#include "Common.h"
#include "ModbusServer.h"
#include "Glob.h"
#include "Rout.h"
#include <qdebug.h>

/*--- Modbus status bits ---*/
typedef struct {
  word
  sysOn:1,                /* Bit 0: System started */
    startInp:1,             /* Bit 1: Start input */
    sensorInp:1,            /* Bit 2: sensor input (=INP-1) */
    alarmActive:1,          /* Bit 3: alarm active */
    valve:1,                /* Bit 4: valve output on */
    calibrated:1,           /* Bit 5: Status calibrating/calibrated */
    bt6_15:10;
} tStatusBit;

/*--- Modbus command bits ---*/
typedef struct {
  word
  start:1,                /* Bit 0: Start */
    stop:1,                 /* Bit 1: Stop */
    almRst:1,               /* Bit 2: Reset alarm */
    startInp:1,             /* Bit 3: Remote start input */
    consBatchRst:1,         /* Bit 4: Reset batch consumption */
    consTotRst:1,           /* Bit 5: Reset total consumption */
    manFill:1,              /* Bit 6: Manual fill */
    bt7_15:9;
} tCmdBit;

/*--- Modbus data structure ---*/
typedef struct {
  tCmdBit cmdBits;
} tModbusDat;

/*--- Modbus data array ---*/
tModbusDat modbusDat[5];

tStatusBit statusBits;
bool cmdFillActive;

/*--- Modbus read only registers ---*/
#define MB_4X1_CPU_RUN             40001
#define MB_4X2_UNIT_TYPE           40002
#define MB_4X3_PROD_STS            40003
#define MB_4X4_STATUS_BITS         40004
#define MB_4X5_ALM_NR              40005
#define MB_4X6_SPARE               40006
#define MB_4X7_COL_CAP_SET         40007
#define MB_4X9_COL_CAP_ACT         40009
#define MB_4X11_CONS_BATCH         40011
#define MB_4X13_CONS_TOTAL         40013
#define MB_4X15_WEIGHT             40015
#define MB_4X17_MOTOR_SPEED        40017
#define MB_4X19_DOS_TIME_ACT       40019
#define MB_4X21_TACHO_ACT          40021
#define MB_4X23_EXT_CAP_ACT        40023
#define MB_4X25_PROD_TO_GO         40025
#define MB_4X27_REGRIND_PCT_ACT    40027
#define MB_4X29_REGRIND_FILL_LVL   40029
#define MB_4X31_EXT_CAP_MAIN       40031
#define MB_4X33_EXT_CAP_TOTAL      40033
#define MB_4X35_COLOR_PCT_TOTAL    40035

/*--- Modbus read/ write registers ---*/
#define MB_WRITE_REG                41001
#define MB_4X1001_CMD_BITS          41001
#define MB_4X1002_SPARE             41002
#define MB_4X1003_COLOR_PCT         41003
#define MB_4X1005_SHOT_WTH          41005
#define MB_4X1007_DOS_TIME_SET      41007
#define MB_4X1009_EXT_CAP_SET       41009
#define MB_4X1011_TACHO_SET         41011
#define MB_4X1013_MOTOR_SPEED_SET   41013


void ModWriteColPct(byte device, float colPct) {
  /*----------------------------*/
  /* Write the color precentage */
  /*----------------------------*/
  glob->SysCfg_SetColPct(colPct, device);
}


void ModWriteCmdBits(byte device, float cmdBits) {
  /*------------------------*/
  /* Write the command bits */
  /*------------------------*/
  word w;
  tCmdBit cmd;

  /*----- Cast bits to word -----*/
  w = cmdBits;
  cmd = *(tCmdBit*)&w;

  /*----- Start -----*/
  if (cmd.start) {
    glob->MstSts_SetSysStart(TRUE);
  }

  /*----- Stop -----*/
  else if (cmd.stop) {
    glob->MstSts_SetSysStart(FALSE);
  }

  /*----- Reset alarms -----*/
  else if (cmd.almRst) {
    Rout::ResetAlarms();
  }

  /*----- Reset batch consumption -----*/
  else if (cmd.consBatchRst) {
    Rout::rstCons[device].batch = true;
  }

  /*----- Reset total consumption -----*/
  else if (cmd.consTotRst) {
      Rout::rstCons[device].total = true;
  }

  /*---------- Manual fill ----------*/
  /* Open */
  else if (cmd.manFill) {
    glob->CfgFill_SetManualFill(true,device);             /* Open fill valve */
    cmdFillActive = TRUE;                                 /* Set remote man fill active */
  }
  /* Close */
  else if (cmdFillActive) {                               /* Remote fill command busy ? */
    if (!cmd.manFill) {
      glob->CfgFill_SetManualFill(false,device);          /* Close fill valve */
      cmdFillActive = FALSE;
    }
  }

  /*----- Remote start input -----*/
  glob->sysSts[device].remoteStart = cmd.startInp;
}


void ModWriteShotWeight(byte device, float shotWth) {
  /*-----------------------*/
  /* Write the shot weight */
  /*-----------------------*/
  glob->SysCfg_SetShotWeight(shotWth,device);
}


void ModWriteShotTime(byte device, float shotTime) {
  /*---------------------*/
  /* Write the shot time */
  /*---------------------*/
  glob->SysCfg_SetShotTime(shotTime,device);
}


void ModWriteExtCap(byte device, float extCap) {
  /*-----------------------------*/
  /* Write the extruder capacity */
  /*-----------------------------*/
  glob->SysCfg_SetExtCap(extCap,device);
}


void ModWriteTachoMaxCap(byte device, float tacho) {
  /*------------------------------*/
  /* Write the max tacho capacity */
  /*------------------------------*/
  glob->SysCfg_SetMaxTacho(tacho,device);
}


void ModWriteRpm(byte device, float rpm) {
  /*------------------------------*/
  /* Write the max tacho capacity */
  /*------------------------------*/
  glob->SysCfg_SetRpm(rpm,device);
}


void ModReadStatusBits(byte device) {
  /*-------------------------------------------*/
  /* Read the status bits for the given device */
  /*-------------------------------------------*/
  statusBits.sysOn= global::sysSts[device].sysActive.active;         /* Bit 0: System started */
  statusBits.startInp= global::sysSts[device].actIO.inputs[0];       /* Bit 1: Start input */
  statusBits.sensorInp= global::sysSts[device].actIO.inputs[1];      /* Bit 2: sensor input (=INP-1) */
  statusBits.alarmActive=global::sysSts[device].alarmSts.active;     /* Bit 3: alarm active */
  statusBits.valve=global::sysSts[device].actIO.outputs[1];          /* Bit 4: valve output on */
  statusBits.calibrated=global::sysSts[device].balanceCalibrated;    /* Bit 5: Status calibrating/calibrated */
  statusBits.bt6_15=0;
}


const tModData ModTab[] =
/*----------------------------------------*/
/* Modbus register <-> data address table */
/*----------------------------------------*/
{
  {MB_4X1_CPU_RUN, BYTE_TYPE,                           /* Modbus address */
        &Rout::cpuRun,                                  /* Data address */
        0,                                              /* Structure length */
        0,                                              /* Read handler */
        0},                                             /* Write handler */

  {MB_4X2_UNIT_TYPE, BYTE_TYPE,
        &global::sysCfg[0].deviceType,
        sizeof(global::sysCfg[0]),
        0,
        0},

  {MB_4X3_PROD_STS,
        BYTE_TYPE,
        &global::sysSts[0].MCStatus,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X4_STATUS_BITS,
        WORD_TYPE,
        &statusBits,
        0,                                        /* Structlen not used ! */
        &ModReadStatusBits,
        0},

  {MB_4X5_ALM_NR,
        BYTE_TYPE,
        &global::sysSts[0].alarmSts.nr,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X6_SPARE,
        BYTE_TYPE,
        0,
        0,
        0,
        0},

  {MB_4X7_COL_CAP_SET,
        FLOAT_TYPE,
        &global::sysSts[0].setProd,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X9_COL_CAP_ACT,
        FLOAT_TYPE,
        &global::sysSts[0].actProd,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X11_CONS_BATCH,
        FLOAT_TYPE,
        &global::sysSts[0].consumptionDay,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X13_CONS_TOTAL,
        FLOAT_TYPE,
        &global::sysSts[0].consumption,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X15_WEIGHT,
        FLOAT_TYPE,
        &global::sysSts[0].mass,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X17_MOTOR_SPEED,
        FLOAT_TYPE,
        &global::sysSts[0].rpm,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X19_DOS_TIME_ACT,
        FLOAT_TYPE,
        &global::sysSts[0].injection.shotTimeAct,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X21_TACHO_ACT,
        FLOAT_TYPE,
        &global::sysSts[0].extrusion.tachoAct,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X23_EXT_CAP_ACT,
        FLOAT_TYPE,
        &global::sysSts[0].extrusion.extCapacityAct,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X25_PROD_TO_GO,
        FLOAT_TYPE,
        &global::sysSts[0].productsToGo,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X27_REGRIND_PCT_ACT,
        FLOAT_TYPE,
        &global::sysSts[0].regrSts.regPctAct,
        sizeof(global::sysSts[0]),0
        ,
        0},

  {MB_4X29_REGRIND_FILL_LVL,
        FLOAT_TYPE,
        &global::sysSts[0].regrSts.fillStartLevel,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X31_EXT_CAP_MAIN,
        FLOAT_TYPE,
        &global::sysSts[0].mcWeightSts.extCap,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X33_EXT_CAP_TOTAL,
        FLOAT_TYPE,
        &global::sysSts[0].mcWeightSts.extCapTotal,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X35_COLOR_PCT_TOTAL,
        FLOAT_TYPE,
        &global::sysSts[0].mcWeightSts.mcWeightPct,
        sizeof(global::sysSts[0]),
        0,
        0},

  {MB_4X1001_CMD_BITS,
        WORD_TYPE,
        &modbusDat[0].cmdBits,
        sizeof(modbusDat[0]),
        0,
        &ModWriteCmdBits},

  {MB_4X1002_SPARE,
        WORD_TYPE,
        0,
        0,
        0,
        0},

    {MB_4X1003_COLOR_PCT,
          FLOAT_TYPE,
          &global::sysCfg[0].colPct,
          sizeof(global::sysCfg[0]),
          0,
          &ModWriteColPct},

    {MB_4X1005_SHOT_WTH,
          FLOAT_TYPE,
          &global::sysCfg[0].injection.shotWeight,
          sizeof(global::sysCfg[0]),
          0,
          &ModWriteShotWeight},

    {MB_4X1007_DOS_TIME_SET,
          FLOAT_TYPE,
          &global::sysCfg[0].injection.shotTime,
          sizeof(global::sysCfg[0]),
          0,
          &ModWriteShotTime},

    {MB_4X1009_EXT_CAP_SET,
          FLOAT_TYPE,
          &global::sysCfg[0].extrusion.extCapacity,
          sizeof(global::sysCfg[0]),
          0,
          &ModWriteExtCap},

    {MB_4X1011_TACHO_SET,
          FLOAT_TYPE,
          &global::sysCfg[0].extrusion.tachoMax,
          sizeof(global::sysCfg[0]),
          0,
          &ModWriteTachoMaxCap},

    {MB_4X1013_MOTOR_SPEED_SET,
          FLOAT_TYPE,
          &global::sysCfg[0].setRpm,
          sizeof(global::sysCfg[0]),
          0,
          &ModWriteRpm},

    {0xFFFF,
      0,
      0,
      0,
      0,
      0}
  };




  ModbusServer::ModbusServer(QObject *parent) : QObject(parent)
  {
    /*-------------*/
    /* Constructor */
    /*-------------*/
    connect(&server, SIGNAL(newConnection()), this, SLOT(NewConnection()));

    /*----- Listen to Modbus port -----*/
    server.listen(QHostAddress::Any, 502);

    cmdFillActive = FALSE;
  }


  void ModbusServer::NewConnection()
  {
    /*---------------------*/
    /* Setup client socket */
    /*---------------------*/
    client = server.nextPendingConnection();

    /*----- Connect readyReady to TCP Read handler -----*/
    connect(client, SIGNAL(readyRead()), this, SLOT(TcpRead()));
    connect(client, SIGNAL(disconnected()), this, SLOT(TcpDisConnected()));
  }


  void ModbusWriteData(byte device, word addr, word nrOfRegs, word *src) {
    /*--------------------------------------------------------------------------*/
    /* Copy the modbus data buffer to MCTC variable                             */
    /*--------------------------------------------------------------------------*/
    word i;
    byte exit;
    word regOffset;
    word regLen;
    const tModData *p;
    byte b;
    word w;
    long l;
    float f;
    float writeHandlerVal;

    i=0;
    exit = false;
    regOffset=0;

    /*----- Search for address -----*/
    while (!exit) {

      p = &ModTab[i++];                                                           /* Setup pointer to modbus address data tabel */

      /*----- End of table or all registers found -----*/
      if ((p->regAddr == 0xFFFF) || (nrOfRegs <= 0)) {
        exit = true;
      }
      /*----- Address found -----*/
      else if ((addr+regOffset) == p->regAddr) {
        regLen = 0;

        /*----- Fill modbus buffer with data -----*/
        switch (p->dataType) {

        case(BYTE_TYPE) :
          if ((addr+regOffset) >= MB_WRITE_REG) {
            memcpy(((byte*)&b),((byte*)src)+1,1);
          }

          writeHandlerVal = b;

          regLen = 1;
          break;

        case(WORD_TYPE) :
          if ((addr+regOffset) >= MB_WRITE_REG) {
            memcpy(((byte*)&w+0),((byte*)src)+1,1);
            memcpy(((byte*)&w+1),((byte*)src)+0,1);
          }

          writeHandlerVal = w;

          regLen = 1;
          break;

        case(LONG_TYPE) :
          if ((addr+regOffset) >= MB_WRITE_REG) {
            memcpy(((byte*)&l+0),((byte*)src)+1,1);
            memcpy(((byte*)&l+1),((byte*)src)+0,1);
            memcpy(((byte*)&l+2),((byte*)src)+3,1);
            memcpy(((byte*)&l+3),((byte*)src)+2,1);
          }

          writeHandlerVal = l;

          regLen = 2;
          break;

        case FLOAT_TYPE :
          if ((addr+regOffset) >= MB_WRITE_REG) {
            memcpy(((byte*)&f+0),((byte*)src+1),1);
            memcpy(((byte*)&f+1),((byte*)src+0),1);
            memcpy(((byte*)&f+2),((byte*)src+3),1);
            memcpy(((byte*)&f+3),((byte*)src+2),1);
          }

          writeHandlerVal = f;

          regLen = 2;
          break;
        }

        /*----- Update write handler, if present -----*/
        if ((addr+regOffset) >= MB_WRITE_REG) {
          if (p->WriteHandler != 0) {
            p->WriteHandler(device-1,writeHandlerVal);
          }
        }

        /*----- Update local vars -----*/
        if (regLen) {
          regOffset+=regLen;                                                      /* Adjust register offset */
          nrOfRegs-=regLen;                                                       /* Update regs to go */
          src+=regLen;                                                           /* Update buffer pointer */
        }
      }
    }
  }


  void ModbusReadData(byte device, word addr, word *dest, word nrOfRegs)
  {
    /*--------------------------------------------------------------------------*/
    /* Fill the modbus data buffer for the given adress and number of registers */
    /*--------------------------------------------------------------------------*/
    word i;
    bool exit;
    word regOffset;
    word regLen;
    const tModData *p;
    bool validAdr;

    /*----- Search for address -----*/
    i=0;
    exit = false;
    regOffset=0;

    while (!exit) {

      p = &ModTab[i++];                                                           /* Setup pointer to modbus address data tabel */

      /*----- End of table or all registers found -----*/
      if ((p->regAddr == 0xFFFF) || (nrOfRegs <= 0)) {
        exit = true;
      }
      /*----- Address found -----*/
      else if ((addr+regOffset) == p->regAddr) {

        /*----- Update read handler, if present -----*/
        if (p->ReadHandler != 0) {
          p->ReadHandler(device-1);
        }

        regLen = 0;

        validAdr = (p->dataPtr != 0);

        /*----- Fill modbus buffer with data -----*/
        switch (p->dataType) {

        case(BYTE_TYPE) :
          if (validAdr) {
            memcpy(((byte*)dest)+1,((byte*)p->dataPtr+p->structLen*(device-1)+0),1);
            memset(((byte*)dest),0,1);
          }
          regLen = 1;
          break;

        case(WORD_TYPE) :
          if (validAdr) {
            memcpy(((byte*)dest)+1,((byte*)p->dataPtr+p->structLen*(device-1)+0),1);
            memcpy(((byte*)dest)+0,((byte*)p->dataPtr+p->structLen*(device-1)+1),1);
          }
          regLen = 1;
          break;

        case(LONG_TYPE) :
          if (validAdr) {
            memcpy(((byte*)dest)+1,((byte*)p->dataPtr+p->structLen*(device-1)+0),1);
            memcpy(((byte*)dest)+0,((byte*)p->dataPtr+p->structLen*(device-1)+1),1);
            memcpy(((byte*)dest)+3,((byte*)p->dataPtr+p->structLen*(device-1)+2),1);
            memcpy(((byte*)dest)+2,((byte*)p->dataPtr+p->structLen*(device-1)+3),1);
          }
          regLen = 2;
          break;

        case FLOAT_TYPE :
          if (validAdr) {
            memcpy(((byte*)dest)+1,((byte*)p->dataPtr+p->structLen*(device-1)+0),1);
            memcpy(((byte*)dest)+0,((byte*)p->dataPtr+p->structLen*(device-1)+1),1);
            memcpy(((byte*)dest)+3,((byte*)p->dataPtr+p->structLen*(device-1)+2),1);
            memcpy(((byte*)dest)+2,((byte*)p->dataPtr+p->structLen*(device-1)+3),1);
          }
          regLen = 2;

          break;
        }

        /*----- Update local vars -----*/
        if (regLen) {
          regOffset+=regLen;                                                      /* Adjust register offset */
          nrOfRegs-=regLen;                                                       /* Update regs to go */
          dest+=regLen;                                                           /* Update buffer pointer */
        }
      }
    }
  }


  void ModbusServer::TcpRead()
  {
    /*-----------------------*/
    /* TCP Read data handler */
    /*-----------------------*/
    word startReg,nrOfRegs;
    word nrOfBytes;
    word i;
    word len;
    byte deviceAddr;

    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    if (socket == 0) return;

    /*----- Read message data ----*/
    len = socket->bytesAvailable();
    if (len <= (int)sizeof(modBusMsg)) {                                          /* Check message length */
      socket->read((char*)&modBusMsg,len);                                        /* Message data to buf */
    }

    /*----- Check Modbus device address -----*/
    deviceAddr = modBusMsg.func3QueryTcp.unit;
    if ((deviceAddr >= glob->mstCfg.modbusAddr) && (deviceAddr <= glob->mstCfg.modbusAddr+glob->mstCfg.slaveCount)) {

      switch (modBusMsg.func3QueryTcp.funcCode) {

      /*----- Function code 3 : Read holding registers, address 4xxxxx -----*/
      case(3) :

        /*----- Fetch register address and lenght -----*/
        startReg = Rout::SwapWord(modBusMsg.func3QueryTcp.startReg);
        nrOfRegs = Rout::SwapWord(modBusMsg.func3QueryTcp.nrOfRegs);

        /*----- Build reply message -----*/
        modBusMsg.func3ResponseTcp.len = Rout::SwapWord(3+2*nrOfRegs);
        modBusMsg.func3ResponseTcp.nrOfBytes = 2*nrOfRegs;

        /*----- Range check -----*/
        if (nrOfRegs > MAX_REG) {
          nrOfRegs = MAX_REG;
        }

        modBusLen = 9+2*nrOfRegs;                                               /* 9 = sizeof tFunc3Response header without data */

        /*----- Fetch modbus data -----*/
        ModbusReadData(deviceAddr,MODBUS_OFFSET+startReg,(word*)&modBusMsg.func3ResponseTcp.reg,nrOfRegs);

        /*----- Send reply -----*/
        socket->write((char*)&modBusMsg,modBusLen);

        break;

        /*----- Function code 6 :Preset single register ,  address 4xxxxx ----*/
      case(6) :

        /*----- Fetch register address and data -----*/
        startReg = Rout::SwapWord(modBusMsg.func6QueryTcp.startReg);
        i = Rout::SwapWord(modBusMsg.func6QueryTcp.data);

        /*----- Build reply -----*/
        modBusMsg.func6ResponseTcp.startReg = Rout::SwapWord(startReg);
        modBusMsg.func6ResponseTcp.data = Rout::SwapWord(i);

        ModbusWriteData(deviceAddr,MODBUS_OFFSET+startReg,1,(word*)&modBusMsg.func6QueryTcp.data);
        modBusLen = sizeof(tFunc6ResponseTcp);

        /*----- Send reply -----*/
        socket->write((char*)&modBusMsg,modBusLen);
        break;

        /*----- Function code 16 :Write multiple registers ,  address 4xxxxx -----*/
      case(16) :

        /*----- Fetch register address and lenght -----*/
        startReg = Rout::SwapWord(modBusMsg.func16QueryTcp.startReg);
        nrOfRegs = Rout::SwapWord(modBusMsg.func16QueryTcp.nrOfRegs);
        nrOfBytes = modBusMsg.func16QueryTcp.nrOfBytes;

        modBusLen = 13+nrOfBytes;                                               /* 13 = sizeof tFunc16Querey header without data */

        ModbusWriteData(deviceAddr,MODBUS_OFFSET+startReg,nrOfRegs,(word*)&modBusMsg.func16QueryTcp.reg);

        /*----- Build reply -----*/
        modBusMsg.func16ResponseTcp.startReg = Rout::SwapWord(startReg);
        modBusMsg.func16ResponseTcp.nrOfRegs = Rout::SwapWord(nrOfRegs);
        modBusMsg.func16ResponseTcp.len = Rout::SwapWord(6);

        /*----- Send reply -----*/
        socket->write((char*)&modBusMsg,sizeof(tFunc16ResponseTcp));
        break;

        /*----- Unsupported function -----*/
      default :
        i = modBusMsg.func3QueryTcp.funcCode;                                   /* Fetch function code */

        /*----- Build reply -----*/
        modBusMsg.exceptionResponseTcp.funcCode = (byte)i + 0x80;               /* Reply the received function code with the MS bit set */
        modBusMsg.exceptionResponseTcp.exceptionCode = EXCEP_ILLEGAL_FUNCTION;
        modBusMsg.exceptionResponseTcp.len = 3;

        /*----- Send reply -----*/
        socket->write((char*)&modBusMsg,sizeof(tExceptionResponseTcp));
        break;
      }
    }
  }


  void ModbusServer::TcpDisConnected()
  {
    /*------------------------*/
    /* TCP Disconnect handler */
    /*------------------------*/
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    if (socket == 0) return;
    socket->deleteLater();
  }

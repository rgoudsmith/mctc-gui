/****************************************************************************
** Meta object code from reading C++ file 'Form_AdvLoaderSettings.h'
**
** Created: Wed Feb 6 11:14:46 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_AdvLoaderSettings.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_AdvLoaderSettings.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AdvLoaderSettingsFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      22,   21,   21,   21, 0x08,
      43,   21,   21,   21, 0x08,
      55,   21,   21,   21, 0x08,
      71,   21,   21,   21, 0x0a,
      92,   90,   21,   21, 0x0a,
     119,   90,   21,   21, 0x0a,
     155,   90,   21,   21, 0x0a,
     188,   90,   21,   21, 0x0a,
     221,   90,   21,   21, 0x0a,
     257,   90,   21,   21, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_AdvLoaderSettingsFrm[] = {
    "AdvLoaderSettingsFrm\0\0ValueReturned(float)\0"
    "OkClicked()\0CancelClicked()\0"
    "AlarmModeClicked()\0,\0SelectionReceived(int,int)\0"
    "CfgFill_AlarmTimeChanged(float,int)\0"
    "CfgFill_TimeMeChanged(float,int)\0"
    "CfgFill_TimeMvChanged(float,int)\0"
    "CfgFill_EmptyTimeChanged(float,int)\0"
    "CfgFill_AlarmModeChanged(bool,int)\0"
};

const QMetaObject AdvLoaderSettingsFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_AdvLoaderSettingsFrm,
      qt_meta_data_AdvLoaderSettingsFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AdvLoaderSettingsFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AdvLoaderSettingsFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AdvLoaderSettingsFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AdvLoaderSettingsFrm))
        return static_cast<void*>(const_cast< AdvLoaderSettingsFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int AdvLoaderSettingsFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: ValueReturned((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 1: OkClicked(); break;
        case 2: CancelClicked(); break;
        case 3: AlarmModeClicked(); break;
        case 4: SelectionReceived((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: CfgFill_AlarmTimeChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: CfgFill_TimeMeChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: CfgFill_TimeMvChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: CfgFill_EmptyTimeChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 9: CfgFill_AlarmModeChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

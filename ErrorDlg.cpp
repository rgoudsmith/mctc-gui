#include "ErrorDlg.h"
#include "ui_Form_ErrorDlg.h"

#include "Form_MainMenu.h"

ErrorDlg::ErrorDlg(QWidget *parent) : QDialog(parent), ui(new Ui::ErrorDlg)
{
  _gbi = 0;
  ui->setupUi(this);

  unitIndex = 0;
  mType = msgtNone;

  closeTim = new QTimer(this);
  connect(closeTim,SIGNAL(timeout()),this,SLOT(OnCloseTim()));
  connect(ui->btOk,SIGNAL(pressed()),this,SLOT(bt1Clicked()));
  connect(ui->btHelp,SIGNAL(pressed()),this,SLOT(bt2Clicked()));
  connect(ui->btCancel,SIGNAL(pressed()),this,SLOT(bt3Clicked()));
  connect(ui->btHelp2,SIGNAL(pressed()),this,SLOT(bt4Clicked()));
}


ErrorDlg::~ErrorDlg()
{
  delete ui;
}


void ErrorDlg::changeEvent(QEvent *e)
{
  QDialog::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      LanguageUpdate();
      break;
    default:
      break;
  }
}


void ErrorDlg::SetGBI(globalItems *gbi)
{
  if (_gbi != 0) {
    disconnect(_gbi,SIGNAL(SysSts_AlarmStatusChanged(bool,int)),this,SLOT(AlarmStatusChanged(bool,int)));
  }

  _gbi = gbi;

  if (_gbi != 0) {
    LanguageUpdate();
    connect(_gbi,SIGNAL(SysSts_AlarmStatusChanged(bool,int)),this,SLOT(AlarmStatusChanged(bool,int)));
  }
}


void ErrorDlg::LanguageUpdate()
{
  if (_gbi != 0) {
    _gbi->SetButtonImage(ui->btOk,itSelection,selOk);
    _gbi->SetButtonImage(ui->btCancel,itSelection,selCancel);

    ui->lbCodeCap->setFont(*(_gbi->baseFont));
    ui->lbCode->setFont(*(_gbi->baseFont));
    ui->lbDateCap->setFont(*(_gbi->baseFont));
    ui->lbDate->setFont(*(_gbi->baseFont));
    ui->lbErrorText->setFont(*(_gbi->baseFont));
    ui->lbTimeCap->setFont(*(_gbi->baseFont));
    ui->lbTime->setFont(*(_gbi->baseFont));
    ui->lbUnitCap->setFont(*(_gbi->baseFont));
    ui->lbUnit->setFont(*(_gbi->baseFont));
  }
}


void ErrorDlg::SetButtons(msgType mType)
{
  switch(mType) {
    case msgtWarning:
    case msgtAlarm:
    case msgtError:
      ui->lbDate->setVisible(true);
      ui->lbTime->setVisible(true);
      ui->lbDateCap->setVisible(true);
      ui->lbTimeCap->setVisible(true);
      ui->btCancel->setVisible(false);
      ui->btOk->setVisible(true);
      ui->btHelp->setVisible(false);
      ui->btHelp2->setVisible(false);
      if (_gbi != 0) {
        _gbi->SetButtonImage(ui->btOk,itSelection,selOk);
      }
      ui->btHelp->setText("?");
      break;

    case msgtConfirm:
      ui->lbDate->setVisible(false);
      ui->lbTime->setVisible(false);
      ui->lbDateCap->setVisible(false);
      ui->lbTimeCap->setVisible(false);
      ui->btCancel->setVisible(true);
      ui->btOk->setVisible(true);
      ui->btHelp->setVisible(false);
      ui->btHelp2->setVisible(false);
      if (_gbi != 0) {
        _gbi->SetButtonImage(ui->btOk,itSelection,selOk);
      }
      ui->btHelp->setText("?");
      break;

    case msgtAccept:
      ui->lbDate->setVisible(false);
      ui->lbTime->setVisible(false);
      ui->lbDateCap->setVisible(false);
      ui->lbTimeCap->setVisible(false);
      ui->btCancel->setVisible(false);
      ui->btOk->setVisible(true);
      ui->btHelp->setVisible(false);
      if (_gbi != 0) {
        _gbi->SetButtonImage(ui->btOk,itSelection,selOk);
      }
      ui->btHelp2->setVisible(false);
      break;

    case msgtSave:
      ui->lbDate->setVisible(false);
      ui->lbTime->setVisible(false);
      ui->lbDateCap->setVisible(false);
      ui->lbTimeCap->setVisible(false);
      ui->btCancel->setVisible(true);
      ui->btOk->setVisible(true);
      ui->btHelp->setVisible(true);
      if (_gbi != 0) {
        _gbi->SetButtonImage(ui->btOk,itSelection,selOk);
      }
      ui->btHelp2->setVisible(true);
      break;

    default:
      ui->btCancel->setVisible(false);
      ui->btOk->setVisible(true);
      ui->btHelp->setVisible(false);
      ui->lbDate->setVisible(false);
      ui->lbTime->setVisible(false);
      ui->lbDateCap->setVisible(false);
      ui->lbTimeCap->setVisible(false);
      ui->btHelp2->setVisible(false);
      if (_gbi != 0) {
        _gbi->SetButtonImage(ui->btOk,itSelection,selOk);
      }
      ui->btHelp->setText("?");
      break;
  }
}


void ErrorDlg::ShowMessage(msgType mType,int unitIdx, int code, const QString &text)
{
  lastSelect = -1;
  this->mType = mType;
  if (_gbi != 0) {
    _gbi->mstSts.popupActive = true;
    if ((_gbi->menu) != 0) {
      ((MainMenu*)(_gbi->menu))->FoldMenu();
    }
  }
  unitIndex = unitIdx;
  SetButtons(mType);
  ui->lbWarn->setVisible(true);
  ui->lbImage->setVisible(true);
  ui->lbCode->setVisible(true);
  ui->lbCodeCap->setVisible(true);
  ui->btOk->setVisible(true);
  QString styleSheet;
  styleSheet = "#groupBox {border: 5px solid; border-radius: 7px; background-color: rgb(255,255,255); ";
  switch(mType) {
    case msgtWarning:
      ui->lbWarn->setText(tr("WARNING"));
      styleSheet += "border-color: rgb(255, 170, 0);";                          // orange
      break;
    case msgtAlarm:
      ui->lbWarn->setText(tr("ALARM"));
      styleSheet += "border-color: rgb(255, 0, 0);";                            // red
      break;
    case msgtError:
      ui->lbWarn->setText(tr("ERROR"));
      styleSheet += "border-color: rgb(255, 0, 0);";                            // red
      break;
    case msgtConfirm:
      ui->lbImage->setVisible(false);
      ui->lbWarn->setVisible(false);
      ui->lbCode->setVisible(false);
      ui->lbCodeCap->setVisible(false);
      ui->lbWarn->setText(tr("Confirm"));
      styleSheet += "border-color: rgb(0, 0, 125);";                            // dark blue
      break;
    case msgtAccept:
      ui->lbImage->setVisible(false);
      ui->lbWarn->setVisible(false);
      ui->lbCode->setVisible(false);
      ui->lbCodeCap->setVisible(false);
      ui->lbWarn->setText(tr("Accept"));
      styleSheet += "border-color: rgb(0, 0, 125);";                            // dark blue
      break;
    case msgtSave:
      ui->lbImage->setVisible(false);
      ui->lbWarn->setVisible(false);
      ui->lbCode->setVisible(false);
      ui->lbCodeCap->setVisible(false);
      ui->lbWarn->setText(tr("Confirm"));
      styleSheet += "border-color: rgb(0, 0, 125);";                            // dark blue
      break;
    default:
      ui->lbImage->setVisible(false);
      ui->lbWarn->setVisible(false);
      ui->lbCode->setVisible(false);
      ui->lbCodeCap->setVisible(false);
      ui->lbWarn->setText("");
      styleSheet += "border-color: rgb(0, 0, 0);";                              // black
      break;
  }
  styleSheet += "}";
  setStyleSheet(styleSheet);
  if (_gbi != 0) {
    if (unitIndex >= 0) {
      ui->lbUnit->setText(_gbi->sysCfg[unitIndex].MC_ID);
    }
    else {
      ui->lbUnit->setText("System");
    }
    ui->lbDate->setText(_gbi->mstCfg.sysTime.date().toString(FORMAT_DATE_LONG));
    ui->lbTime->setText(_gbi->mstCfg.sysTime.time().toString(FORMAT_TIME_LONG));
    // Find errorCode and check if it is warning or error.
  }
  else {
    ui->lbUnit->setText(QString::number(unitIndex));
  }
  ui->lbCode->setText(QString::number(code));
  ui->lbErrorText->setText(text);
  // Display the window (which is application modal)
  setWindowModality(Qt::ApplicationModal);
}


void ErrorDlg::ShowMessage(int unitIdx,const QString &text, int timeOut)
{
  unitIndex = unitIdx;
  lastSelect = -1;
  mType = msgtNone;

  SetButtons(msgtCount);

  ui->lbWarn->setVisible(false);
  ui->lbImage->setVisible(false);
  ui->lbCode->setVisible(false);
  ui->lbCodeCap->setVisible(false);
  ui->btOk->setVisible(false);

  if (_gbi != 0) {
    if (unitIdx >= 0) {
      ui->lbUnit->setText(_gbi->sysCfg[unitIdx].MC_ID);
    }
    else {
      ui->lbUnit->setText("System");
    }
    ui->lbDate->setText(_gbi->mstCfg.sysTime.date().toString(FORMAT_DATE_LONG));
    ui->lbTime->setText(_gbi->mstCfg.sysTime.time().toString(FORMAT_TIME_LONG));

    if ((_gbi->menu) != 0) {
      ((MainMenu*)(_gbi->menu))->FoldMenu();
    }
  }
  else {
    ui->lbUnit->setText(QString::number(unitIdx));
  }
  ui->lbErrorText->setText(text);

  QString styleSheet;
  styleSheet = "#groupBox {border: 5px solid; border-radius: 7px; background-color: rgb(255,255,255); border-color: rgb(0, 0, 0); }";
  setStyleSheet(styleSheet);

  closeTim->setInterval(timeOut);
}


void ErrorDlg::CancelAlarm()
{
  if ((mType == msgtAlarm) || (mType == msgtWarning)) {
    if (_gbi != 0) {
      _gbi->AlarmResetRcv(unitIndex);
    }
  }
}


void ErrorDlg::CancelForm()
{
  KeyBeep();
  closeTim->stop();
  if (_gbi != 0) {
    _gbi->mstSts.popupActive = false;
  }
  unitIndex = -1;
}


void ErrorDlg::OnCloseTim()
{
  CancelForm();
}


int ErrorDlg::LastSelection()
{
  return lastSelect;
}


void ErrorDlg::KeyBeep()
{

}


void ErrorDlg::bt1Clicked()
{
  {
    //    if (validClick())
    // Button OK
    lastSelect = 1;
    emit ButtonClicked(1);

    if ((mType >= msgtWarning) && (mType <= msgtError)) {
      CancelAlarm();
    }
    CancelForm();
    done(1);
  }
}


void ErrorDlg::bt2Clicked()
{
  {
    //if (validClick())
    // Button Help (YES ALL)
    lastSelect = 2;
    emit ButtonClicked(2);
    CancelForm();
    done(2);
  }
}


void ErrorDlg::bt3Clicked()
{
  {
    //if (validClick())
    // Button Cancel
    lastSelect = 3;
    emit ButtonClicked(3);
    CancelForm();
    done(3);
  }
}


void ErrorDlg::bt4Clicked()
{
  {
    //if (validClick())
    // Button Help2 (NO ALL)
    lastSelect = 4;
    emit ButtonClicked(4);
    CancelForm();
    done(4);
  }
}


void ErrorDlg::AlarmStatusChanged(bool sts, int idx)
{
  if (idx == unitIndex) {
    if (sts == false) {
      // If alarm is no longer active, close the message
      CancelForm();
      done(0);
    }
  }
}

#ifndef FILLSYSTEMFRM_H
#define FILLSYSTEMFRM_H

#include <QtGui/QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QPainter>
#include "Form_QbaseWidget.h"
#include "Form_NumericInput.h"

namespace Ui
{
  class FillSystemFrm;
}


class FillSystemFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    FillSystemFrm(QWidget *parent = 0);
    ~FillSystemFrm();
    void FormInit();
    void SetLevelType(eFillSys _type);
    void DrawLine(QPainter *p, int startX, int startY, int stopX, int stopY, int lineOffset);

  protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *e);
    //    void paintEvent(QPaintEvent *e);
    void showEvent(QShowEvent *);
    bool eventFilter(QObject *, QEvent *);

  private:
    Ui::FillSystemFrm *m_ui;
    NumericInputFrm * numInput;

    QPushButton * btManFill;
    QPushButton * btFillSettings;
    QLineEdit * edLoLo;
    QLineEdit * edLo;
    QLineEdit * edHi;
    QLineEdit * edHiHi;

    QLabel * uLoLo;
    QLabel * uLo;
    QLabel * uHi;
    QLabel * uHiHi;

    eFillSys fillSys;
    int lineIdx;

    int mvcXpos;
    int cfgDecimals;
    float cfgMinInput;
    float cfgMaxInput;

    FillLevelsStruct levels;
    void PrintLevelValues();
    void LanguageUpdate();

  private slots:
    void FillSettingsClicked();
    void ManualFillPressed();
    void ManualFillReleased();
    void OkClicked();
    void CancelClicked();

  public slots:
    void UnitsChanged(const QString &str);
    void CfgFillSystemChanged(eFillSys, int idx=0);
    void CfgLevelsChanged(float,float,float,float,int idx = 0);
    void CfgFill_ManFillChanged(bool,int);
    void ValueReturned(float value);
};
#endif                                                                          // FILLSYSTEMFRM_H

#ifndef RECIPECONFIGITEMFRM_H
#define RECIPECONFIGITEMFRM_H

#include "Form_QbaseWidget.h"
#include "ImageDef.h"
#include "Form_Recipe.h"
#include "Form_NumericInput.h"

namespace Ui
{
  class RecipeItemFrm;
}


class RecipeItemFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit RecipeItemFrm(QWidget *parent = 0);
    ~RecipeItemFrm();
    void FormInit();
    void SetUnitIndex(int idx);

  private:
    Ui::RecipeItemFrm *ui;
    int unitIdx;
    RecipeBufStruct * rcpBuf;
    NumericInputFrm * numInput;
    KeyboardFrm * keyInput;

    bool eventFilter(QObject *, QEvent *);

  private slots:
    void SetColPct(float);

  public slots:
    void SetRecipe(RecipeBufStruct*,int);
    void DeviceIdChanged(const QString &,int);
    void MaterialReceived(const QString &,bool,int);
    void ValueReceived(float,int);
    void SetNumInput(NumericInputFrm *);
    void UpdateFields();

    signals:
    void LineChanged(int);
    void MaterialClicked(int);
};
#endif                                                                          // RECIPECONFIGITEMFRM_H

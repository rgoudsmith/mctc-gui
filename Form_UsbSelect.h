#ifndef USBSELECTFRM_H
#define USBSELECTFRM_H

#include "Form_QbaseWidget.h"

namespace Ui
{
  class USBSelectFrm;
}


class USBSelectFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit USBSelectFrm(QWidget *parent = 0);
    ~USBSelectFrm();
    void FormInit();

  protected:
    void changeEvent(QEvent *);

  private:
    Ui::USBSelectFrm *ui;
    void LanguageChanged();

  private slots:
    void OkClicked();
    void USBDeviceClicked();
    void DeviceUSBClicked();
    void USBRemoveClicked();

  public slots:
};
#endif                                                                          // USBSELECTFRM_H

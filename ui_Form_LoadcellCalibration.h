/********************************************************************************
** Form generated from reading UI file 'Form_LoadcellCalibration.ui'
**
** Created: Thu Feb 2 17:00:09 2012
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_LOADCELLCALIBRATION_H
#define UI_FORM_LOADCELLCALIBRATION_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QProgressBar>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "Form_MvcSystem.h"

QT_BEGIN_NAMESPACE

class Ui_SystemCalibrationFrm
{
public:
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    MvcSystem *mvcSystem;
    QHBoxLayout *horizontalLayout_2;
    QProgressBar *pbProgress;
    QSpacerItem *horizontalSpacer_3;

    void setupUi(QWidget *SystemCalibrationFrm)
    {
        if (SystemCalibrationFrm->objectName().isEmpty())
            SystemCalibrationFrm->setObjectName(QString::fromUtf8("SystemCalibrationFrm"));
        SystemCalibrationFrm->resize(400, 300);
        SystemCalibrationFrm->setCursor(QCursor(Qt::BlankCursor));
        SystemCalibrationFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout = new QVBoxLayout(SystemCalibrationFrm);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer = new QSpacerItem(20, 49, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        mvcSystem = new MvcSystem(SystemCalibrationFrm);
        mvcSystem->setObjectName(QString::fromUtf8("mvcSystem"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(mvcSystem->sizePolicy().hasHeightForWidth());
        mvcSystem->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(mvcSystem);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        pbProgress = new QProgressBar(SystemCalibrationFrm);
        pbProgress->setObjectName(QString::fromUtf8("pbProgress"));
        pbProgress->setLayoutDirection(Qt::LeftToRight);
        pbProgress->setValue(79);
        pbProgress->setTextVisible(false);
        pbProgress->setInvertedAppearance(false);

        horizontalLayout_2->addWidget(pbProgress);

        horizontalSpacer_3 = new QSpacerItem(100, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(SystemCalibrationFrm);

        QMetaObject::connectSlotsByName(SystemCalibrationFrm);
    } // setupUi

    void retranslateUi(QWidget *SystemCalibrationFrm)
    {
        Q_UNUSED(SystemCalibrationFrm);
    } // retranslateUi

};

namespace Ui {
    class SystemCalibrationFrm: public Ui_SystemCalibrationFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_LOADCELLCALIBRATION_H

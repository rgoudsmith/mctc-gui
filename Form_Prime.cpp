#include "Form_Prime.h"
#include "ui_Form_Prime.h"
#include "Form_MainMenu.h"
#include "Rout.h"

#define PRIME_TIM_VAL 500
//#define PRIME_TEST

PrimeFrm::PrimeFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::PrimeFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  lineIdx = -1;
  ui->setupUi(this);
  SetPrime = false;

  /*----- Disable windows title bar -----*/
  this->setWindowFlags(Qt::CustomizeWindowHint);

  primeTim = new QTimer(this);
  primeTim->setSingleShot(false);

  numInput = new NumericInputFrm();
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));

  btPrimeAll = new QPushButton(this);
  btPrimeAll->setFixedSize(90,70);
  btPrimeAll->move(((width()/2)-(btPrimeAll->width()/2)),415);
  btPrimeAll->setText(tr("PRIME")+"\n"+tr("ALL"));

  ui->edTime->setFocusPolicy(Qt::NoFocus);
  ui->edRpm->setFocusPolicy(Qt::NoFocus);
  ui->edTime->installEventFilter(this);
  ui->edRpm->installEventFilter(this);

  ui->edTime_2->setFocusPolicy(Qt::NoFocus);
  ui->edRpm_2->setFocusPolicy(Qt::NoFocus);
  ui->edTime_2->installEventFilter(this);
  ui->edRpm_2->installEventFilter(this);

  DisplayProgress(false,0);
  DisplayProgress(false,1);

  connect(ui->btCancel,SIGNAL(clicked()),this,SLOT(btCancelClicked()));
  connect(ui->btPrime,SIGNAL(clicked()),this,SLOT(btPrimeClicked()));
  connect(ui->btPrime_2,SIGNAL(clicked()),this,SLOT(btPrime2Clicked()));
  connect(primeTim,SIGNAL(timeout()),this,SLOT(TimUpdate()));

  ui->lbTestRpmVal1->setVisible(false);
  ui->lbTestTimeVal1->setVisible(false);
  ui->btTest1->setVisible(false);

  ui->lbTestRpmVal2->setVisible(false);
  ui->lbTestTimeVal2->setVisible(false);
  ui->btTest2->setVisible(false);

  btPrimeAll->setVisible(false);
}


PrimeFrm::~PrimeFrm()
{
  delete numInput;
  delete ui;
}


void PrimeFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}


void PrimeFrm::showEvent(QShowEvent *e)
{
  QWidget::showEvent(e);
  SetupDisplay();
}


void PrimeFrm::resizeEvent(QResizeEvent *e)
{
  QWidget::resizeEvent(e);
  btPrimeAll->move(((width()/2)-(btPrimeAll->width()/2)),415);
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
}


bool PrimeFrm::eventFilter(QObject *obj, QEvent *ev)
{
  if (ev->type() == QEvent::MouseButtonPress) {
    lineIdx = -1;
    float val = 0;

    /*----- Time 1 -----*/
    if (obj == ui->edTime) {
      lineIdx = 0;
      if (glob->mstSts.isTwin) {
        val = glob->sysCfg[0].prime.time;
      }
      else {
        val = glob->sysCfg[glob->ActUnitIndex()].prime.time;
      }
    }

    /*----- RPM 1 -----*/
    if (obj == ui->edRpm) {
      lineIdx = 1;
      if (glob->mstSts.isTwin) {
        val = glob->sysCfg[0].prime.rpm;
      }
      else {
        val = glob->sysCfg[glob->ActUnitIndex()].prime.rpm;
      }
    }

    /*----- Time 2 -----*/
    if (obj == ui->edTime_2) {
      lineIdx = 2;
      val = glob->sysCfg[1].prime.time;
    }

    /*----- RPM 2 -----*/
    if (obj == ui->edRpm_2) {
      lineIdx = 3;
      val = glob->sysCfg[1].prime.rpm;
    }

    if (lineIdx >= 0) {
      KeyBeep();

      /*----- Setup numeric keypad -----*/
      switch(lineIdx) {
        case 0:
          numInput->SetDisplay(false,0,val,1,999);
          numInput->show();
          break;
        case 1:
          numInput->SetDisplay(true,glob->formats.RpmFmt.precision,val,0.1,glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.maxMotorSpeed.actVal);
          numInput->show();
          break;
        case 2:
          numInput->SetDisplay(false,0,val,1,999);
          numInput->show();
          break;
        case 3:
          numInput->SetDisplay(true,glob->formats.RpmFmt.precision,val,0.1,glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.maxMotorSpeed.actVal);
          numInput->show();
          break;

        default:
          break;
      }
    }
  }
  return QWidget::eventFilter(obj,ev);
}


void PrimeFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  numInput->FormInit();

  glob->SetButtonImage(ui->btCancel,itSelection,selCancel);
  glob->SetButtonImage(ui->btPrime,itSelection,selOk);
  glob->SetButtonImage(ui->btPrime_2,itSelection,selOk);
  glob->SetButtonImage(ui->btLeft,itGeneral,genArrowKeyLeft);
  glob->SetButtonImage(ui->btRight,itGeneral,genArrowKeyRight);

  connect(glob,SIGNAL(SysCfg_PrimeRpmChanged(float,int)),this,SLOT(PrimeRpmChanged(float,int)));
  connect(glob,SIGNAL(SysCfg_PrimeTimeChanged(int,int)),this,SLOT(PrimeTimeChanged(int,int)));
  connect(glob,SIGNAL(SysCfg_IDChanged(QString,int)),this,SLOT(UnitNameChanged(QString,int)));
  connect(glob,SIGNAL(SysSts_RpmChanged(float,int)),this,SLOT(TestRpmChanged(float,int)));
  connect(glob,SIGNAL(SysSts_ShotTimeChanged(float,int)),this,SLOT(TestTimeChanged(float,int)));

  connect(ui->btRight,SIGNAL(clicked()),glob,SLOT(MstSts_SetNextUnitIndex()));
  connect(ui->btLeft,SIGNAL(clicked()),glob,SLOT(MstSts_SetPrevUnitIndex()));

  connect(glob,SIGNAL(MstSts_ActUnitChanged(int)),this,SLOT(ActUnitIndexChanged(int)));
  connect(glob,SIGNAL(MstCfg_SlaveCountChanged(int)),this,SLOT(SlaveCountChanged(int)));

  connect(numInput,SIGNAL(InputValue(float)),this,SLOT(ValueReceived(float)));
  connect(glob,SIGNAL(SysSts_PrimeStatusChanged(bool,int)),this,SLOT(PrimeStatusChanged(bool,int)));

  setFont(*(glob->baseFont));

  ui->edTime->setFont(*(glob->baseFont));
  ui->edTime_2->setFont(*(glob->baseFont));
  ui->edRpm->setFont(*(glob->baseFont));
  ui->edRpm_2->setFont(*(glob->baseFont));
  ui->pbTimer->setFont(*(glob->baseFont));
  ui->lbUnitName1->setFont(*(glob->baseFont));
  ui->lbUnitName2->setFont(*(glob->baseFont));
  ui->lbPrime->setFont(*(glob->baseFont));
  ui->lbPrime2->setFont(*(glob->baseFont));

  ui->lbTestTimeVal1->setFont(*(glob->baseFont));
  ui->lbTestTimeVal2->setFont(*(glob->baseFont));
  ui->lbTestRpmVal1->setFont(*(glob->baseFont));
  ui->lbTestRpmVal2->setFont(*(glob->baseFont));

  ui->frame_2->setVisible(glob->mstSts.isTwin);

  if (glob->mstSts.isTwin == false) {
    PrimeRpmChanged(glob->sysCfg[glob->ActUnitIndex()].prime.rpm,glob->ActUnitIndex());
    PrimeTimeChanged(glob->sysCfg[glob->ActUnitIndex()].prime.time,glob->ActUnitIndex());
    UnitNameChanged(glob->sysCfg[glob->ActUnitIndex()].MC_ID,glob->ActUnitIndex());
  }
  else {
    PrimeRpmChanged(glob->sysCfg[0].prime.rpm,0);
    PrimeRpmChanged(glob->sysCfg[1].prime.rpm,1);

    PrimeTimeChanged(glob->sysCfg[0].prime.time,0);
    PrimeTimeChanged(glob->sysCfg[1].prime.time,1);

    UnitNameChanged(glob->sysCfg[0].MC_ID,0);
    UnitNameChanged(glob->sysCfg[1].MC_ID,1);
  }

  SetupDisplay();
}


void PrimeFrm::DisplayProgress(bool show, int idx)
{
  if (idx == 0) {
    ui->pbTimer->setVisible(show);
    ui->Progress1Spacer->setVisible(!show);
  }

  if (idx == 1) {
    ui->pbTimer_2->setVisible(show);
    ui->Progress2Spacer->setVisible(!show);
  }
}


void PrimeFrm::SetupDisplay()
{
  //    DisplayProgress(false,0);
  //    DisplayProgress(false,1);

  /*----- TWIN mode -----*/
  if (glob->mstSts.isTwin) {
    ui->btLeft->setVisible(false);
    ui->btRight->setVisible(false);
    ui->frame_2->setVisible(true);
  }
  /*----- Single unit mode -----*/
  else {
    ui->frame_2->setVisible(false);
    ui->btLeft->setVisible(false);
    ui->btRight->setVisible(false);
  }
}


void PrimeFrm::TimUpdate()
{
  /*----------------------------*/
  /* Timer primTim timeout event*/
  /*----------------------------*/
  /*----- Update progressBar -----*/

  /*----- TWIN mode -----*/
  if (glob->mstSts.isTwin) {

    glob->sysSts[0].prime.timVal += PRIME_TIM_VAL;
    glob->sysSts[1].prime.timVal += PRIME_TIM_VAL;

    if (glob->sysSts[0].prime.timVal >= glob->sysSts[0].prime.maxVal) {
      StopPrime(0);
      ui->pbTimer->setValue(0);
    }
    else {
      ui->pbTimer->setValue((glob->sysSts[0].prime.maxVal-glob->sysSts[0].prime.timVal)/1000);
    }

    if (glob->sysSts[1].prime.timVal >= glob->sysSts[1].prime.maxVal) {
      StopPrime(1);
      ui->pbTimer_2->setValue(0);
    }
    else {
      ui->pbTimer_2->setValue((glob->sysSts[1].prime.maxVal-glob->sysSts[1].prime.timVal)/1000);
    }
  }

  /*----- Single unit mode -----*/
  else {
    int idx = glob->ActUnitIndex();
    glob->sysSts[idx].prime.timVal += PRIME_TIM_VAL;

    if (glob->sysSts[idx].prime.timVal >= glob->sysSts[idx].prime.maxVal) {
      ui->pbTimer->setValue(0);

      if (glob->sysSts[idx].prime.activate == true) {
        if (!(glob->sysSts[idx].prime.preCal)) {
          /*---- When prime is completed, return to production (HOME) window.----*/
          ((MainMenu*)glob->menu)->DisplayWindow(wiHome);
        }
        else {
          /*--- preCal active, suppress home window return ---*/
          glob->sysSts[idx].prime.preCal = false;
        }
        // Stop prime AFTER window check, local StopPrime function sets prime.preCal to false.
        StopPrime(idx);
      }
    }
    else {
      ui->pbTimer->setValue((glob->sysSts[glob->ActUnitIndex()].prime.maxVal- glob->sysSts[glob->ActUnitIndex()].prime.timVal)/1000);
    }
  }
}


void PrimeFrm::StartPrime(int idx)
{
  /*-----------------------*/
  /* Start the prime timer */
  /*-----------------------*/

  /*----- Setup max val for progess bar -----*/
  glob->sysSts[idx].prime.timVal = 0;
  glob->sysSts[idx].prime.maxVal = (glob->sysCfg[idx].prime.time * 1000);

  /*----- TWIN mode active -----*/
  if(glob->mstSts.isTwin) {
    /*----- Unit 0 -----*/
    if (idx == 0) {
      /*----- Change start button into cancel button -----*/
      glob->SetButtonImage(ui->btPrime,itSelection,selCancel);

      /*----- Init progress bar -----*/
      ui->pbTimer->setMaximum((glob->sysSts[idx].prime.maxVal/1000));
      ui->pbTimer->setValue(glob->sysSts[0].prime.maxVal/1000);
      DisplayProgress(true,0);
    }

    /*----- Unit 1 -----*/
    if (idx == 1) {
      /*----- Change start button into cancel button -----*/
      glob->SetButtonImage(ui->btPrime_2,itSelection,selCancel);

      /*----- Init progress bar -----*/
      ui->pbTimer_2->setMaximum((glob->sysSts[idx].prime.maxVal/1000));
      ui->pbTimer_2->setValue(glob->sysSts[1].prime.maxVal/1000);
      DisplayProgress(true,1);
    }

    /*----- Start prime timer -----*/
    if (!(primeTim->isActive())) {
      primeTim->setInterval(PRIME_TIM_VAL);
      primeTim->start();
    }
    /*----- Set prime mode active -----*/
    glob->SysSts_SetPrimeActive(true,idx,false);
  }
  /*----- Single unit mode ----*/
  else {
    if (idx == glob->ActUnitIndex()) {
      glob->SysSts_SetPrimeActive(true,idx,false);
    }
  }
}


void PrimeFrm::StopPrime(int idx)
{
  /*----------------------*/
  /* Stop the prime timer */
  /*----------------------*/
  /*----- TWIN mode -----*/
  if (glob->mstSts.isTwin) {
    if (idx == 0) {
      /*----- Change cancel button into start button -----*/
      glob->SetButtonImage(ui->btPrime,itSelection,selOk);
      DisplayProgress(false,0);                                                 /* Disable progress bar */
    }
    if (idx == 1) {
      /*----- Change cancel button into start button -----*/
      glob->SetButtonImage(ui->btPrime_2,itSelection,selOk);
      DisplayProgress(false,1);                                                 /* Disable progress bar */
    }
    glob->SysSts_SetPrimeActive(false,idx);

    /*----- Check if the timer can be deactivated -----*/
    if ((glob->sysSts[0].prime.activeSts == false) && (glob->sysSts[1].prime.activeSts == false)) {
      primeTim->stop();
    }
  }
  /*----- Single unit -----*/
  else {
    glob->SysSts_SetPrimeActive(false,idx);
    DisplayProgress(false,0);                                                   /* Disable progress bar */
  }
}


void PrimeFrm::btPrimeClicked()
{
  /*-----------------------------*/
  /* Prime unit 1 button clicked */
  /*-----------------------------*/

  /*-----------*/
  /* TWIN mode */
  /*-----------*/
  if (glob->mstSts.isTwin) {
    if (glob->sysSts[0].prime.activeSts == true) {
      StopPrime(0);
    }
    else {
      StartPrime(0);
    }
  }
  else {
    /*------------------*/
    /* Single unit mode */
    /*------------------*/
    if (glob->sysSts[glob->ActUnitIndex()].prime.activeSts == true) {           /* Prime busy ? */
      /*---- Abort prime ----*/
      StopPrime(glob->ActUnitIndex());
    }
    else {
      /*----- Start prime -----*/
      StartPrime(glob->ActUnitIndex());
    }
  }
}


void PrimeFrm::btPrime2Clicked()
{
  /*-----------------------------*/
  /* Prime unit 2 button clicked */
  /*-----------------------------*/
  if (glob->mstSts.isTwin) {
    if (glob->sysSts[1].prime.activeSts == true) {
      StopPrime(1);
    }
    else {
      StartPrime(1);
    }
  }
}


void PrimeFrm::btCancelClicked()
{
  /*-----------------------*/
  /* Cancel button pressed */
  /*-----------------------*/
  if (glob->mstSts.isTwin) {
    StopPrime(0);
    StopPrime(1);
  }
  else {
    //@@ PrimeFrm - Extra check for all units... more units might be running in prime.
    StopPrime(glob->ActUnitIndex());
  }
  // Return to production window.

  ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
}


void PrimeFrm::PrimeRpmChanged(float _rpm, int idx)
{
  /*-------------------*/
  /* Prime RPM changed */
  /*-------------------*/
  if (glob->mstSts.isTwin) {
    if (idx == 0) ui->edRpm->setText(QString::number(_rpm,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision) +" "+ glob->rpmUnits);
    if (idx == 1) ui->edRpm_2->setText(QString::number(_rpm,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision) +" "+ glob->rpmUnits);
  }
  else {
    if (idx == glob->ActUnitIndex()) {
      ui->edRpm->setText(QString::number(_rpm,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision) + " "+glob->rpmUnits);
    }
  }
}


void PrimeFrm::PrimeTimeChanged(int _time, int idx)
{
  /*--------------------*/
  /* Prime time changed */
  /*--------------------*/
  if (glob->mstSts.isTwin) {
    if (idx == 0) ui->edTime->setText(QString::number(_time,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) + QString(" ")+ glob->timerUnits);
    if (idx == 1) ui->edTime_2->setText(QString::number(_time,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) + QString(" ")+ glob->timerUnits);
  }
  else {
    if (idx == glob->ActUnitIndex()) {
      ui->edTime->setText(QString::number(_time,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) + QString(" ")+ glob->timerUnits);
    }
  }
}


void PrimeFrm::UnitNameChanged(const QString &name, int idx)
{
  if (glob->mstSts.isTwin) {
    if (idx == 0) ui->lbUnitName1->setText(name);
    if (idx == 1) ui->lbUnitName2->setText(name);
  }
  else {
    if (idx == glob->ActUnitIndex()) {
      ui->lbUnitName1->setText(name);
    }
  }
}


void PrimeFrm::ValueReceived(float val)
{
  /*-----------------------------------------*/
  /* Numeric keyboard value received handler */
  /*-----------------------------------------*/
  switch(lineIdx) {
    case 0:
      if (glob->mstSts.isTwin) {
        glob->SysCfg_SetPrimeTime((int)val,0);
      }
      else {
        glob->SysCfg_SetPrimeTime((int)val,glob->ActUnitIndex());
      }
      break;
    case 1:
      if (glob->mstSts.isTwin) {
        glob->SysCfg_SetPrimeRpm(val,0);
      }
      else {
        glob->SysCfg_SetPrimeRpm(val,glob->ActUnitIndex());
      }
      break;
    case 2:
      glob->SysCfg_SetPrimeTime((int)val,1);
      break;
    case 3:
      glob->SysCfg_SetPrimeRpm(val,1);
      break;
    default:
      break;
  }
  lineIdx = -1;
}


void PrimeFrm::TestTimeChanged(float _time,int idx)
{
  /*-------------------*/
  /* Test time changed */
  /*-------------------*/
  if (!(glob->sysCfg[idx].test.active)) {
    if (glob->mstSts.isTwin) {

      if (idx == 1) ui->lbTestTimeVal1->setText(QString::number(_time,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision)+" "+glob->timerUnits);
      if (idx == 2) ui->lbTestTimeVal2->setText(QString::number(_time,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision)+" "+glob->timerUnits);
    }
    else {
      ui->lbTestTimeVal1->setText(QString::number(_time,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision)+" "+glob->timerUnits);
    }
  }
}


void PrimeFrm::TestRpmChanged(float _rpm, int idx)
{
  /*------------------*/
  /* Test RPM changed */
  /*------------------*/
  if (!(glob->sysCfg[idx].test.active)) {
    if (glob->mstSts.isTwin) {
      if (idx == 1) ui->lbTestRpmVal1->setText(QString::number(_rpm,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision)+" "+glob->rpmUnits);
      if (idx == 2) ui->lbTestRpmVal2->setText(QString::number(_rpm,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision)+" "+glob->rpmUnits);
    }
    else {
      ui->lbTestRpmVal1->setText(QString::number(_rpm,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision)+" "+glob->rpmUnits);
    }
  }
}


void PrimeFrm::ActUnitIndexChanged(int idx)
{
  if (!(glob->mstSts.isTwin)) {
    PrimeRpmChanged(glob->sysCfg[idx].prime.rpm,idx);
    PrimeTimeChanged(glob->sysCfg[idx].prime.time,idx);
    UnitNameChanged(glob->sysCfg[idx].MC_ID,idx);
  }
}


void PrimeFrm::SlaveCountChanged(int)
{
  if (glob->mstSts.isTwin) {
    PrimeRpmChanged(glob->sysCfg[0].prime.rpm,0);
    PrimeTimeChanged(glob->sysCfg[0].prime.time,0);
    UnitNameChanged(glob->sysCfg[0].MC_ID,0);

    PrimeRpmChanged(glob->sysCfg[1].prime.rpm,1);
    PrimeTimeChanged(glob->sysCfg[1].prime.time,1);
    UnitNameChanged(glob->sysCfg[1].MC_ID,1);
  }
  /* Multi or single unit display is coverd by ActUnitIndexChanged */
}


void PrimeFrm::PrimeStatusChanged(bool sts, int idx)
{
  /*--------------------------------*/
  /* Actual status of prime changed */
  /*--------------------------------*/
  if (idx == glob->ActUnitIndex()) {
    if (sts) {
      /*----- Change start button into cancel button ----*/
      glob->SetButtonImage(ui->btPrime,itSelection,selCancel);
      glob->sysSts[idx].prime.timVal = 0;
      glob->sysSts[idx].prime.maxVal = (glob->sysCfg[idx].prime.time * 1000);
      ui->pbTimer->setMaximum(glob->sysSts[idx].prime.maxVal/1000);
      ui->pbTimer->setValue(glob->sysSts[idx].prime.maxVal/1000);
      DisplayProgress(true,0);

      primeTim->setInterval(PRIME_TIM_VAL);

      /*----- Timer started -----*/
      primeTim->start();
    }
    else {
      glob->SetButtonImage(ui->btPrime,itSelection,selOk);
    }
  }

  /*----- Check timer stop condition -----*/
  bool doStop = true;
  for (int i=0;i<=glob->mstCfg.slaveCount;i++) {
    if (glob->sysSts[i].prime.activate == true) {
      doStop = false;
    }
  }

  if (doStop == true) {
    /*----- Timer stopped -----*/
    primeTim->stop();
  }
}

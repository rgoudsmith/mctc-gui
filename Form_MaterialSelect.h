#ifndef MATERIALSELECTFRM_H
#define MATERIALSELECTFRM_H

#include <QWidget>
#include <QStringList>
#include "Form_QbaseWidget.h"
#include "Form_Keyboard.h"

namespace Ui
{
  class MaterialSelectFrm;
}


class MaterialSelectFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit MaterialSelectFrm(QWidget *parent = 0);
    ~MaterialSelectFrm();
    void FormInit();
    void SetCallerWindow(eWindowIdx);
    QString searchString;                          /* Search key for material file list */

  private:
    void showEvent(QShowEvent *);
    void changeEvent(QEvent *);
    Ui::MaterialSelectFrm *ui;
    QStringList matList;
    QString currentItem;
    QString ext;
    int msgIdx;
    int currentIndex;
    bool currentDefault;
    int dosToolChange;
    eWindowIdx calWindow;
    KeyboardFrm * keyInput;

    void UpdateCurrent(const QString &,bool = false);
    void UpdateCurrent(int,bool = false);
    void LoadMaterials();
    void DisplayUpBoxItems();
    void DisplayDownBoxItems();
    void DisplaySelectedItem();
    void DisplayDefaultMaterial();
    void UpdateDisplay();
    int ItemCount();
    void ClipMaterialExtension(QString *);
    void GetDefaultMaterialFolder(QString *);
    void GetMaterialFolder(QString *);
    void GetDefaultMaterial(QString *);
    void GetDefaultMaterialText(QString *);
    void RemoveCurrentItem();
    void RemoveAllItems();
    void CreateNewItem(const QString &, int &);
    void RenameCurrentItem(const QString &);
    void SetCorrectButtons();
    void CloseForm();
    //    void SearchItem(const QString &);

  private slots:
    void DefaultMatsClicked();
    void NewClicked();
    void DeleteClicked();
    void DeleteAllClicked();
    void ScrollUp();
    void ScrollDown();
    void CancelClicked();
    void RenameClicked();
    void OkClicked();
    void SearchClicked();
    void MessageResult(int);
    void TextReceived(const QString &);
    void SysActiveChanged(bool);

  public slots:
    void ActiveIndexChanged(int);
    void SetSelectedMaterial(const QString &);
    void CfgCurrentUserChanged(eUserType);

    signals:
    void MaterialSelected(const QString &, bool);
};
#endif                                                                          // MATERIALSELECTFRM_H

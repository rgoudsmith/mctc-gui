/****************************************************************************
** Meta object code from reading C++ file 'Form_LearnOnline.h'
**
** Created: Wed Feb 6 11:15:03 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_LearnOnline.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_LearnOnline.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LearnOnlineFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      34,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_LearnOnlineFrm[] = {
    "LearnOnlineFrm\0\0OkButtonClicked()\0"
    "NewUnitSelected()\0"
};

const QMetaObject LearnOnlineFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_LearnOnlineFrm,
      qt_meta_data_LearnOnlineFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LearnOnlineFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LearnOnlineFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LearnOnlineFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LearnOnlineFrm))
        return static_cast<void*>(const_cast< LearnOnlineFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int LearnOnlineFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: OkButtonClicked(); break;
        case 1: NewUnitSelected(); break;
        default: ;
        }
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

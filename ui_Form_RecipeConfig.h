/********************************************************************************
** Form generated from reading UI file 'Form_RecipeConfig.ui'
**
** Created: Thu Jan 26 16:22:30 2012
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_RECIPECONFIG_H
#define UI_FORM_RECIPECONFIG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "Form_RecipeItem.h"

QT_BEGIN_NAMESPACE

class Ui_RecipeConfigFrm
{
public:
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QLabel *lbCaption;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_11;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_10;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout;
    QLineEdit *edRcpName;
    QPushButton *btOk;
    QSpacerItem *horizontalSpacer_3;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_9;
    QVBoxLayout *verticalLayout_4;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer_3;
    QPushButton *btUp;
    QPushButton *btDown;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_6;
    QLineEdit *edVar1;
    QLineEdit *edVar2;
    QLineEdit *edRcpText;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *ReplacementSpacer;
    QHBoxLayout *horizontalLayout_12;
    QLabel *lbDevice;
    QLabel *lbMaterial;
    QLabel *label_3;
    RecipeItemFrm *Device1;
    QSpacerItem *verticalSpacer_4;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QPushButton *btSave;
    QPushButton *btRemove;
    QPushButton *btCancel;

    void setupUi(QWidget *RecipeConfigFrm)
    {
        if (RecipeConfigFrm->objectName().isEmpty())
            RecipeConfigFrm->setObjectName(QString::fromUtf8("RecipeConfigFrm"));
        RecipeConfigFrm->resize(800, 526);
        RecipeConfigFrm->setCursor(QCursor(Qt::BlankCursor));
        RecipeConfigFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout_7 = new QVBoxLayout(RecipeConfigFrm);
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        lbCaption = new QLabel(RecipeConfigFrm);
        lbCaption->setObjectName(QString::fromUtf8("lbCaption"));
        QFont font;
        font.setPointSize(35);
        font.setBold(true);
        font.setWeight(75);
        lbCaption->setFont(font);
        lbCaption->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(lbCaption);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_7->addLayout(horizontalLayout_2);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(0);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        groupBox = new QGroupBox(RecipeConfigFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox {border: 2px solid black; border-radius: 3px; }"));
        groupBox->setTitle(QString::fromUtf8(""));
        horizontalLayout = new QHBoxLayout(groupBox);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        edRcpName = new QLineEdit(groupBox);
        edRcpName->setObjectName(QString::fromUtf8("edRcpName"));
        edRcpName->setMinimumSize(QSize(200, 70));
        edRcpName->setMaximumSize(QSize(200, 70));
        edRcpName->setCursor(QCursor(Qt::BlankCursor));
        edRcpName->setFocusPolicy(Qt::NoFocus);
        edRcpName->setText(QString::fromUtf8(""));
        edRcpName->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(edRcpName);

        btOk = new QPushButton(groupBox);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("Ok"));

        horizontalLayout->addWidget(btOk);


        horizontalLayout_10->addWidget(groupBox);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_3);


        verticalLayout_6->addLayout(horizontalLayout_10);

        groupBox_2 = new QGroupBox(RecipeConfigFrm);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setTitle(QString::fromUtf8(""));
        horizontalLayout_9 = new QHBoxLayout(groupBox_2);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalSpacer_4 = new QSpacerItem(90, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        verticalLayout_4->addItem(horizontalSpacer_4);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_3);

        btUp = new QPushButton(groupBox_2);
        btUp->setObjectName(QString::fromUtf8("btUp"));
        btUp->setMinimumSize(QSize(90, 70));
        btUp->setMaximumSize(QSize(90, 70));
        btUp->setText(QString::fromUtf8("Up"));

        verticalLayout_4->addWidget(btUp);

        btDown = new QPushButton(groupBox_2);
        btDown->setObjectName(QString::fromUtf8("btDown"));
        btDown->setMinimumSize(QSize(90, 70));
        btDown->setMaximumSize(QSize(90, 70));
        btDown->setText(QString::fromUtf8("Down"));

        verticalLayout_4->addWidget(btDown);


        horizontalLayout_9->addLayout(verticalLayout_4);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(5, -1, -1, -1);
        edVar1 = new QLineEdit(groupBox_2);
        edVar1->setObjectName(QString::fromUtf8("edVar1"));
        edVar1->setMinimumSize(QSize(130, 70));
        edVar1->setMaximumSize(QSize(130, 70));
        edVar1->setCursor(QCursor(Qt::BlankCursor));
        edVar1->setFocusPolicy(Qt::NoFocus);
        edVar1->setText(QString::fromUtf8(""));
        edVar1->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(edVar1);

        edVar2 = new QLineEdit(groupBox_2);
        edVar2->setObjectName(QString::fromUtf8("edVar2"));
        edVar2->setMinimumSize(QSize(130, 70));
        edVar2->setMaximumSize(QSize(130, 70));
        edVar2->setCursor(QCursor(Qt::BlankCursor));
        edVar2->setFocusPolicy(Qt::NoFocus);
        edVar2->setText(QString::fromUtf8(""));
        edVar2->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(edVar2);

        edRcpText = new QLineEdit(groupBox_2);
        edRcpText->setObjectName(QString::fromUtf8("edRcpText"));
        edRcpText->setMinimumSize(QSize(240, 70));
        edRcpText->setMaximumSize(QSize(240, 70));
        QFont font1;
        font1.setPointSize(18);
        edRcpText->setFont(font1);
        edRcpText->setCursor(QCursor(Qt::BlankCursor));
        edRcpText->setFocusPolicy(Qt::NoFocus);
        edRcpText->setText(QString::fromUtf8(""));
        edRcpText->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(edRcpText);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);

        ReplacementSpacer = new QSpacerItem(0, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(ReplacementSpacer);


        verticalLayout_5->addLayout(horizontalLayout_6);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalLayout_12->setContentsMargins(-1, 12, -1, -1);
        lbDevice = new QLabel(groupBox_2);
        lbDevice->setObjectName(QString::fromUtf8("lbDevice"));
        lbDevice->setMinimumSize(QSize(200, 0));
        lbDevice->setMaximumSize(QSize(200, 16777215));
        QFont font2;
        font2.setPointSize(18);
        font2.setBold(false);
        font2.setWeight(50);
        lbDevice->setFont(font2);
        lbDevice->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        horizontalLayout_12->addWidget(lbDevice);

        lbMaterial = new QLabel(groupBox_2);
        lbMaterial->setObjectName(QString::fromUtf8("lbMaterial"));
        lbMaterial->setMinimumSize(QSize(200, 0));
        lbMaterial->setMaximumSize(QSize(200, 16777215));
        lbMaterial->setFont(font2);
        lbMaterial->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        horizontalLayout_12->addWidget(lbMaterial);

        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(110, 0));
        label_3->setMaximumSize(QSize(110, 16777215));

        horizontalLayout_12->addWidget(label_3);


        verticalLayout_5->addLayout(horizontalLayout_12);

        Device1 = new RecipeItemFrm(groupBox_2);
        Device1->setObjectName(QString::fromUtf8("Device1"));
        Device1->setMinimumSize(QSize(0, 70));
        Device1->setMaximumSize(QSize(16777215, 90));

        verticalLayout_5->addWidget(Device1);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_4);


        horizontalLayout_9->addLayout(verticalLayout_5);


        verticalLayout_6->addWidget(groupBox_2);


        horizontalLayout_11->addLayout(verticalLayout_6);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        btSave = new QPushButton(RecipeConfigFrm);
        btSave->setObjectName(QString::fromUtf8("btSave"));
        btSave->setMinimumSize(QSize(90, 70));
        btSave->setMaximumSize(QSize(90, 70));
        btSave->setText(QString::fromUtf8("Save"));

        verticalLayout->addWidget(btSave);

        btRemove = new QPushButton(RecipeConfigFrm);
        btRemove->setObjectName(QString::fromUtf8("btRemove"));
        btRemove->setMinimumSize(QSize(90, 70));
        btRemove->setMaximumSize(QSize(90, 70));
        btRemove->setText(QString::fromUtf8("Remove"));

        verticalLayout->addWidget(btRemove);

        btCancel = new QPushButton(RecipeConfigFrm);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("Cancel"));

        verticalLayout->addWidget(btCancel);


        horizontalLayout_11->addLayout(verticalLayout);


        verticalLayout_7->addLayout(horizontalLayout_11);


        retranslateUi(RecipeConfigFrm);

        QMetaObject::connectSlotsByName(RecipeConfigFrm);
    } // setupUi

    void retranslateUi(QWidget *RecipeConfigFrm)
    {
        lbCaption->setText(QApplication::translate("RecipeConfigFrm", "Recipe", 0, QApplication::UnicodeUTF8));
        lbDevice->setText(QApplication::translate("RecipeConfigFrm", "Device ID:", 0, QApplication::UnicodeUTF8));
        lbMaterial->setText(QApplication::translate("RecipeConfigFrm", "Material:", 0, QApplication::UnicodeUTF8));
        label_3->setText(QString());
        Q_UNUSED(RecipeConfigFrm);
    } // retranslateUi

};

namespace Ui {
    class RecipeConfigFrm: public Ui_RecipeConfigFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_RECIPECONFIG_H

#include "Form_QbaseWidget.h"
#include "ui_Form_QbaseWidget.h"
#include "Form_MainMenu.h"
#include "Rout.h"

QBaseWidget::QBaseWidget(QWidget *parent) : QWidget(parent), ui(new Ui::QBaseWidget)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  dbgLvl = 0;
  ui->setupUi(this);
}


QBaseWidget::~QBaseWidget()
{
  delete ui;
}


void QBaseWidget::ConnectChildren(QObject *o)
{
  int i,cnt;
  QObject * p;
  QPushButton *bt;

  cnt = o->children().count();
  i = 0;
  while (i<cnt) {
    p = o->children().at(i);

    if (p->isWidgetType()) {
      if (p->inherits("QPushButton")) {
        bt = qobject_cast<QPushButton*>(p);
        if (bt != 0) {
          connect(bt,SIGNAL(clicked()),this,SLOT(KeyBeep()));
        }
        else {
        }
      }
      else {                                                                    // is widget type, but not QPushButton
        if (!(p->children().isEmpty())) {
          if (p->children().count() > 0) {
            ConnectChildren(p);
          }
        }
      }
    }
    i++;
  }
}


void QBaseWidget::ConnectChildren(QObject *o, const QString &)
{
  int i,cnt;
  QObject * p;
  QPushButton *bt;
  QString dbg;
  //  QWidget * w;
  cnt = o->children().count();
  i = 0;
  while (i<cnt) {
    p = o->children().at(i);

    if (p->isWidgetType()) {
      if (p->inherits("QPushButton")) {
        bt = qobject_cast<QPushButton*>(p);
        if (bt != 0) {
          connect(bt,SIGNAL(clicked()),this,SLOT(KeyBeep()));
        }
        else {
        }
      }
      else {                                                                    // is widget type, but not QPushButton
        if (!(p->children().isEmpty())) {
          if (p->children().count() > 0) {
            ConnectChildren(p);
          }
        }
      }
    }
    i++;
  }
}


void QBaseWidget::ConnectKeyBeep()
{
  if (!(this->children().isEmpty())) {
    if (this->children().count() > 0) {
      ConnectChildren(this);
    }
  }
}


void QBaseWidget::KeyBeep()
{
  /*---------------------*/
  /* Generate a key beep */
  /*---------------------*/
  if (glob->mstCfg.keyBeep) {
    if (glob->mstSts.buzzerCtrl == buzNone) {
      glob->mstSts.buzzerCtrl = buzBeep;
      ((MainMenu*)(glob->menu))->SetHomeTimerInterval(glob->mstCfg.MvcSettings.agent.screenTime.actVal);
    }
  }
}

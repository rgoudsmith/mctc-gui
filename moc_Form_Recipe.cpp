/****************************************************************************
** Meta object code from reading C++ file 'Form_Recipe.h'
**
** Created: Wed Feb 6 11:15:47 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_Recipe.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_Recipe.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RecipeFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,
      32,   10,   10,   10, 0x05,
      46,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      61,   10,   10,   10, 0x08,
      77,   10,   10,   10, 0x08,
      93,   10,   10,   10, 0x08,
     109,   10,   10,   10, 0x08,
     123,   10,   10,   10, 0x08,
     145,   10,   10,   10, 0x08,
     166,   10,   10,   10, 0x08,
     194,   10,   10,   10, 0x08,
     209,   10,   10,   10, 0x08,
     224,   10,   10,   10, 0x08,
     247,   10,   10,   10, 0x0a,
     280,  278,   10,   10, 0x0a,
     311,   10,   10,   10, 0x0a,
     330,   10,   10,   10, 0x0a,
     363,   10,   10,   10, 0x0a,
     394,   10,   10,   10, 0x0a,
     430,  427,   10,   10, 0x0a,
     470,  278,  466,   10, 0x0a,
     504,   10,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RecipeFrm[] = {
    "RecipeFrm\0\0SetMaterial(QString)\0"
    "RecipeSaved()\0UnitSelected()\0"
    "RcpSelClicked()\0CancelClicked()\0"
    "RemoveClicked()\0SaveClicked()\0"
    "TextReceived(QString)\0ValueReceived(float)\0"
    "GetFullRecipePath(QString*)\0RecipeLoaded()\0"
    "LoadDefaults()\0SysActiveChanged(bool)\0"
    "RecipeItemMaterialClicked(int)\0,\0"
    "MaterialReceived(QString,bool)\0"
    "SetRecipe(QString)\0CfgCurrentUserChanged(eUserType)\0"
    "CopyRecipeToBuf(RecipeStruct*)\0"
    "CopyRecipeFromBuf(RecipeStruct*)\0,,\0"
    "SaveRecipe(RecipeStruct*,bool,bool)\0"
    "int\0LoadRecipe(QString,RecipeStruct*)\0"
    "MessageResult(int)\0"
};

const QMetaObject RecipeFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_RecipeFrm,
      qt_meta_data_RecipeFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RecipeFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RecipeFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RecipeFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RecipeFrm))
        return static_cast<void*>(const_cast< RecipeFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int RecipeFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: SetMaterial((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: RecipeSaved(); break;
        case 2: UnitSelected(); break;
        case 3: RcpSelClicked(); break;
        case 4: CancelClicked(); break;
        case 5: RemoveClicked(); break;
        case 6: SaveClicked(); break;
        case 7: TextReceived((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: ValueReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 9: GetFullRecipePath((*reinterpret_cast< QString*(*)>(_a[1]))); break;
        case 10: RecipeLoaded(); break;
        case 11: LoadDefaults(); break;
        case 12: SysActiveChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: RecipeItemMaterialClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: MaterialReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 15: SetRecipe((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 16: CfgCurrentUserChanged((*reinterpret_cast< eUserType(*)>(_a[1]))); break;
        case 17: CopyRecipeToBuf((*reinterpret_cast< RecipeStruct*(*)>(_a[1]))); break;
        case 18: CopyRecipeFromBuf((*reinterpret_cast< RecipeStruct*(*)>(_a[1]))); break;
        case 19: SaveRecipe((*reinterpret_cast< RecipeStruct*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 20: { int _r = LoadRecipe((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< RecipeStruct*(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 21: MessageResult((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 22;
    }
    return _id;
}

// SIGNAL 0
void RecipeFrm::SetMaterial(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void RecipeFrm::RecipeSaved()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void RecipeFrm::UnitSelected()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE

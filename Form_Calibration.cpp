#include "Form_Calibration.h"
#include "ui_Form_Calibration.h"
#include "Form_MainMenu.h"
#include "Rout.h"

CalibrationFrm::CalibrationFrm(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::CalibrationFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  mcIdx = 0;
  subState = 0;
  ready = false;

  m_ui->setupUi(this);
  calibPopup = new CalibrationPopup();
  calibPopup->hide();

  m_ui->lbTest->setVisible(true);
  connect(m_ui->btCommand,SIGNAL(clicked()),this,SLOT(CommandClicked()));
  connect(calibPopup,SIGNAL(SelectionMade(int)),this,SLOT(PopupCommandReceived(int)));

  m_ui->lbMessage->setText(tr("Enter material name,")+"\n"+tr("production data")+"\n"+tr("and press \"START\"")+"\n"+tr(""));
}


CalibrationFrm::~CalibrationFrm()
{
  delete calibPopup;
  delete m_ui;
}


void CalibrationFrm::changeEvent(QEvent *e)
{
  QBaseWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      m_ui->retranslateUi(this);
      LanguageUpdate();
      break;
    default:
      break;
  }
}


void CalibrationFrm::resizeEvent(QResizeEvent *e)
{
  QBaseWidget::resizeEvent(e);
}


void CalibrationFrm::showEvent(QShowEvent *e)
{
  QBaseWidget::showEvent(e);
  // Load current production settings into the preCal struct
  ready = false;
  SetCurrentProdSettings(true);

  // Set graphical display
  m_ui->btCommand->setText(tr("START","PreCal command button text"));
}


void CalibrationFrm::hideEvent(QHideEvent *e)
{
  if (!ready) {
    SetCurrentProdSettings(false);
  }

  QBaseWidget::hideEvent(e);
}


void CalibrationFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  calibPopup->FormInit();

  connect(glob,SIGNAL(SysSts_PreCalStatusChanged(unsigned char,int)),this,SLOT(PreCalStatusChanged(unsigned char,int)));
  connect(glob,SIGNAL(SysSts_PreCalDosActChanged(float,int)),this,SLOT(PreCalDosActChanged(float,int)));
  connect(glob,SIGNAL(SysCfg_DosSetChanged(float,int)),this,SLOT(SysCfg_DosSetChanged(float,int)));

  PreCalStatusChanged(glob->sysSts[glob->ActUnitIndex()].preCalibration.calib.status,glob->ActUnitIndex());
  PreCalDosActChanged(glob->sysSts[glob->ActUnitIndex()].preCalibration.preCalDosAct,glob->ActUnitIndex());

  //m_ui->lbStatus->setFont(*(glob->baseFont));
  //m_ui->lbStatus->setStyleSheet("font-weight: bold");
  LanguageUpdate();
}


void CalibrationFrm::LanguageUpdate()
{
  m_ui->lbMessage->setFont(*(glob->baseFont));
  m_ui->label->setFont(*(glob->baseFont));
  m_ui->label_2->setFont(*(glob->baseFont));
  m_ui->lbProcess->setFont(*(glob->baseFont));
  m_ui->lbTest->setFont(*(glob->baseFont));
  m_ui->lbVal1->setFont(*(glob->baseFont));
  m_ui->lbVal2->setFont(*(glob->baseFont));
  m_ui->lbUnits1->setFont(*(glob->baseFont));
  m_ui->lbUnits2->setFont(*(glob->baseFont));
  m_ui->btCommand->setFont(*(glob->baseFont));

  m_ui->lbVal1->setText(QString::number(glob->sysSts[glob->ActUnitIndex()].setProd,glob->formats.DosageFmt.format.toAscii(),glob->formats.DosageFmt.precision));
  m_ui->lbUnits1->setText(glob->shotUnits);
  m_ui->lbUnits2->setText(m_ui->lbUnits1->text());

  m_ui->lbMessage->setText(tr("Enter material name,")+"\n"+tr("production data")+"\n"+tr("and press \"START\""));
}


void CalibrationFrm::SetCurrentProdSettings(bool doLoad, bool finished)
{
  int idx = glob->ActUnitIndex();
  // Load from global status struct to preCal struct
  if (doLoad) {
    if (!finished) {
      /* === CALIB INIT: LOAD DATA AND PREPARE SYSTEM === */
      // Backup original config values into the preCal sts struct
      glob->sysSts[idx].preCalibration.colPct = glob->sysCfg[idx].colPct;
      glob->sysSts[idx].preCalibration.ExtCap = glob->sysCfg[idx].extrusion.extCapacity;
      glob->sysSts[idx].preCalibration.MaxTacho = glob->sysCfg[idx].extrusion.tachoMax;
      glob->sysSts[idx].preCalibration.ShotTime = glob->sysCfg[idx].injection.shotTime;
      glob->sysSts[idx].preCalibration.ShotWeight = glob->sysCfg[idx].injection.shotWeight;
      glob->sysSts[idx].preCalibration.material.name = glob->sysCfg[idx].material.name;
      glob->sysSts[idx].preCalibration.material.isDefault = glob->sysCfg[idx].material.isDefault;

      // Reset all actual values to 0
      glob->SysCfg_SetColPct(0,idx);
      glob->SysCfg_SetExtCap(0,idx);
      glob->SysCfg_SetMaxTacho(0,idx);
      glob->SysCfg_SetShotTime(0,idx);
      glob->SysCfg_SetShotWeight(0,idx);
      glob->SysCfg_SetMaterial(QString(MATERIAL_EMPTY_NAME), false, idx,false);
    }
    else {
      /* === CALIB SUCCESSFUL: SAVE DATA === */
      // Restore all unaltered values (!= 0) to the current values
      // Any altered values will remain in the active system.
      if (glob->sysCfg[idx].colPct == 0) glob->SysCfg_SetColPct(glob->sysSts[idx].preCalibration.colPct,idx);
      if (glob->sysCfg[idx].extrusion.extCapacity == 0) glob->SysCfg_SetExtCap(glob->sysSts[idx].preCalibration.ExtCap,idx);
      if (glob->sysCfg[idx].extrusion.tachoMax == 0) glob->SysCfg_SetMaxTacho(glob->sysSts[idx].preCalibration.MaxTacho,idx);
      if (glob->sysCfg[idx].injection.shotTime == 0) glob->SysCfg_SetShotTime(glob->sysSts[idx].preCalibration.ShotTime,idx);
      if (glob->sysCfg[idx].injection.shotWeight == 0) glob->SysCfg_SetShotWeight(glob->sysSts[idx].preCalibration.ShotWeight,idx);
      // Changing the material will load it into the system.
      if (glob->sysCfg[idx].material.name == MATERIAL_EMPTY_NAME) {
        glob->SysCfg_SetMaterial(glob->sysSts[idx].preCalibration.material.name,glob->sysSts[idx].preCalibration.material.isDefault,idx);
      }
      else {
        QString mat;
        bool isDef;
        mat = glob->sysCfg[idx].material.name;
        isDef = glob->sysCfg[idx].material.isDefault;
        glob->sysCfg[idx].material.name = "";
        glob->SysCfg_SetMaterial(mat,isDef,idx);

      }
    }
  }

  // Restore all values from preCal struct to global status struct
  else {
    /* === CALIB ABORT: RESTORE DATA === */
    // Writing directly into the structures is not allowed, else the display will not be updated accordingly.
    glob->SysCfg_SetColPct(glob->sysSts[idx].preCalibration.colPct,idx);
    glob->SysCfg_SetExtCap(glob->sysSts[idx].preCalibration.ExtCap,idx);
    glob->SysCfg_SetMaxTacho(glob->sysSts[idx].preCalibration.MaxTacho,idx);
    glob->SysCfg_SetShotTime(glob->sysSts[idx].preCalibration.ShotTime,idx);
    glob->SysCfg_SetShotWeight(glob->sysSts[idx].preCalibration.ShotWeight,idx);
    // overwrite the original file with empty to force reload of the curve
    glob->SysCfg_SetMaterial("",false,idx,false);
    // Now set material to the actual filename and trigger a settings storage.
    glob->SysCfg_SetMaterial(glob->sysSts[idx].preCalibration.material.name,glob->sysSts[idx].preCalibration.material.isDefault,idx);
  }
}


void CalibrationFrm::SetMCIdx(int _idx)
{
  mcIdx = _idx;
}


bool CalibrationFrm::CheckInputValues()
{
  bool ok = false;

  if ((glob->sysCfg[glob->ActUnitIndex()].material.name != MATERIAL_EMPTY_NAME) &&
  (glob->sysCfg[glob->ActUnitIndex()].colPct != 0)) {
    if (glob->mstCfg.mode == modExtrude) {
      if (glob->sysCfg[glob->ActUnitIndex()].extrusion.extCapacity != 0) {
        ok = true;
      }
      else {
        // Extruder capacity is empty
        ok = false;
      }
    }
    else {
      if ((glob->sysCfg[glob->ActUnitIndex()].injection.shotTime != 0) &&
      (glob->sysCfg[glob->ActUnitIndex()].injection.shotWeight != 0)) {
        ok = true;
      }
      else {
        // ShotTime is empty
        // ShotWeight is empty
        ok = false;
      }
    }
  }
  else {
    // Material is empty
    // ColPct is empty
    ok = false;
  }
  return ok;
}


void CalibrationFrm::SaveCurveAsMaterial()
{
  int idx = glob->ActUnitIndex();
  glob->sysCfg[idx].curve.corFac = glob->sysSts[idx].preCalibration.rpmCorrFac;
  glob->SysCfg_SaveMaterial(idx,glob->sysCfg[idx].material.name);
}


void CalibrationFrm::CommandClicked()
{
  int idx;
  idx = glob->ActUnitIndex();
  eCalStatus sts = glob->sysSts[idx].preCalibration.calib.sts;
  switch (sts) {
    case calsIdle:
      // Check input values

      switch (subState) {
        case 0:
          /*----- Start pre-calibration ? -----*/
          if (CheckInputValues()) {
            ShowCalibPopup(0);
            subState++;
          }
          else {
            ((MainMenu*)(glob->menu))->DisplayMessage(idx,100,tr("Wrong production settings.\nCannot perform calibration !"),msgtWarning);
          }
          break;

        case 1:
          /*----- Prime ? -----*/
          subState = 0;
          glob->PreCalSendCommand(calStart,glob->ActUnitIndex());
          emit SetDisplayState(mvcDSpreCal2);
          break;
      }
      break;

    case calsBusy:
      subState = 0;
      glob->PreCalSendCommand(calPause,glob->ActUnitIndex());
      ShowCalibPopup(1);
      break;
    case calsReady:
      subState = 0;
      glob->PreCalSendCommand(calDone,glob->ActUnitIndex());
      SaveCurveAsMaterial();
      SetCurrentProdSettings(true,true);
      ready = true;
      ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
      break;
    case calsPause:
      subState = 0;
      glob->PreCalSendCommand(calContinue,glob->ActUnitIndex());
      break;
    default:
      subState = 0;
      // Default do nothing
      break;
  }

}


void CalibrationFrm::PreCalStatusChanged(unsigned char, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    if (glob->sysSts[idx].preCalibration.calib.cmd != calNone) {
      glob->PreCalSendCommand(calNone,idx);
    }

    switch(glob->sysSts[idx].preCalibration.calib.sts) {
      case calsIdle:
        subState = 0;
        m_ui->btCommand->setText(tr("START","PreCal command button text"));
        m_ui->lbProcess->setText(tr("Idle","PreCal status text"));
        m_ui->lbTest->setText("");
        m_ui->stackedWidget->setCurrentIndex(1);
        break;
      case calsBusy:
        subState = 0;
        m_ui->btCommand->setText(tr("STOP && SAVE","PreCal command button text"));
        m_ui->lbProcess->setText(tr("Learning","PreCal status text 1/2")+"\n"+tr("Busy","PreCal status text 2/2"));
        m_ui->lbTest->setText(tr("please wait..."));
        m_ui->stackedWidget->setCurrentIndex(0);
        break;
      case calsReady:
        subState = 0;
        m_ui->btCommand->setText(tr("FINISH","PreCal command button text"));
        m_ui->lbProcess->setText(tr("Learning","PreCal status text 1/2")+"\n"+tr("Ready","PreCal status text 2/2"));
        m_ui->lbTest->setText("");
        m_ui->stackedWidget->setCurrentIndex(0);
      #ifdef UIT
        glob->MstSts_ActivateAlarmBuzzer();
      #endif
        break;
      case calsPause:
        subState = 0;
        m_ui->lbProcess->setText(tr("Learning","PreCal status text 1/2")+"\n"+tr("Interrupted","PreCal status text 2/2"));
        m_ui->lbTest->setText("");
        break;
      default:
        break;
    }
  }
}


void CalibrationFrm::PreCalDosActChanged(float dos, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    if (glob->sysSts[idx].preCalibration.rpmCorrFacValid) {
      m_ui->lbVal2->setText(QString::number(dos,glob->formats.DosageFmt.format.toAscii(),glob->formats.DosageFmt.precision));
    }
    else {
      m_ui->lbVal2->setText(CLIP_FLOAT_STR);
    }
  }
}


void CalibrationFrm::SysCfg_DosSetChanged(float dos, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    m_ui->lbVal1->setText(QString::number(dos,glob->formats.DosageFmt.format.toAscii(),glob->formats.DosageFmt.precision));
  }
}


void CalibrationFrm::ShowCalibPopup(int pageIdx)
{
  calibPopup->SetActivePage(pageIdx);
  calibPopup->show();

  int y = ((600 / 2) - (calibPopup->height() / 2));
  int x = ((800 / 2) - (calibPopup->width() / 2));
  calibPopup->move(x,y);
}


void CalibrationFrm::PopupCommandReceived(int cmd)
{
  switch (cmd) {
    case -1:
      CommandClicked();
      break;
    case 0:                                                                     // Abort + Save
      SaveCurveAsMaterial();
      // Change settings accordingly
      SetCurrentProdSettings(true,true);
      ready = true;
      // Abort calibration
      glob->PreCalSendCommand(calAbort,glob->ActUnitIndex());
      // Go to HOME window
      ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
      break;

    case 1:                                                                     // Abort + Discard
      // Restore old production settings
      SetCurrentProdSettings(false);
      // Abort calibration
      glob->PreCalSendCommand(calAbort,glob->ActUnitIndex());
      // Jump back to "HOME" window
      ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
      break;

    case 2:                                                                     // Continue
      CommandClicked();
      break;
  }
}

#ifndef CALIBRATIONPOPUP_H
#define CALIBRATIONPOPUP_H

#include <QWidget>
#include <QTimer>
#include "Form_QbaseWidget.h"

namespace Ui
{
  class CalibrationPopup;
}


class CalibrationPopup : public QBaseWidget
{
  Q_OBJECT
    public:
    CalibrationPopup(QWidget *parent = 0);
    ~CalibrationPopup();
    void FormInit();

  protected:
    void changeEvent(QEvent *e);
    void showEvent(QShowEvent *);

  private:
    Ui::CalibrationPopup *ui;
    bool SetPrime;
    float origRpm;
    int origTime;
    int unitIdx;
    void LanguageUpdate();

  private slots:
    void OkClicked();
    void CancelClicked();
    void AbortClicked();
    void SaveClicked();
    void ContinueClicked();

  public slots:
    void SetActivePage(int);
    void PrimeActiveChanged(bool,int);

    signals:
    void PrimeSelected(bool);
    void SelectionMade(int);
};
#endif                                                                          // CALIBRATIONPOPUP_H

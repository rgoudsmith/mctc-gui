/********************************************************************************
** Form generated from reading UI file 'Form_UnitConfigStd.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_UNITCONFIGSTD_H
#define UI_FORM_UNITCONFIGSTD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UnitConfigStdFrm
{
public:
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_4;
    QLabel *lbCaption;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_6;
    QVBoxLayout *verticalLayout_2;
    QLabel *lbUnitName;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *edUnitName;
    QHBoxLayout *horizontalLayout_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *btMotorType;
    QSpacerItem *horizontalSpacer_10;
    QPushButton *btFillSystem;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *btKnifeGate;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *btCilinder;
    QSpacerItem *horizontalSpacer_9;
    QPushButton *btLevels;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *btTacho;
    QHBoxLayout *horizontalLayout_8;
    QPushButton *btGranules;
    QSpacerItem *horizontalSpacer_8;
    QPushButton *btSpare1;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *btMcwType;
    QSpacerItem *horizontalSpacer_5;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_5;
    QPushButton *btGraviRpm;
    QPushButton *btLoadCell;
    QPushButton *btTolerances;
    QSpacerItem *verticalSpacer_4;
    QVBoxLayout *verticalLayout_7;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_3;
    QPushButton *btManIO;
    QPushButton *btDiagnose;
    QPushButton *btService;
    QPushButton *btAdvCfg;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btOk;

    void setupUi(QWidget *UnitConfigStdFrm)
    {
        if (UnitConfigStdFrm->objectName().isEmpty())
            UnitConfigStdFrm->setObjectName(QString::fromUtf8("UnitConfigStdFrm"));
        UnitConfigStdFrm->resize(791, 620);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(UnitConfigStdFrm->sizePolicy().hasHeightForWidth());
        UnitConfigStdFrm->setSizePolicy(sizePolicy);
        UnitConfigStdFrm->setSizeIncrement(QSize(0, 0));
        UnitConfigStdFrm->setBaseSize(QSize(0, 0));
        UnitConfigStdFrm->setCursor(QCursor(Qt::BlankCursor));
        UnitConfigStdFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout_3 = new QVBoxLayout(UnitConfigStdFrm);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);

        lbCaption = new QLabel(UnitConfigStdFrm);
        lbCaption->setObjectName(QString::fromUtf8("lbCaption"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lbCaption->sizePolicy().hasHeightForWidth());
        lbCaption->setSizePolicy(sizePolicy1);
        lbCaption->setMinimumSize(QSize(0, 50));
        lbCaption->setMaximumSize(QSize(16777215, 50));
        QFont font;
        font.setPointSize(35);
        font.setBold(true);
        font.setWeight(75);
        lbCaption->setFont(font);
        lbCaption->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(lbCaption);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);


        verticalLayout_3->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(-1, 35, -1, -1);

        horizontalLayout_5->addLayout(verticalLayout_6);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lbUnitName = new QLabel(UnitConfigStdFrm);
        lbUnitName->setObjectName(QString::fromUtf8("lbUnitName"));
        lbUnitName->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(lbUnitName);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(9, -1, 9, -1);
        edUnitName = new QLineEdit(UnitConfigStdFrm);
        edUnitName->setObjectName(QString::fromUtf8("edUnitName"));
        edUnitName->setMinimumSize(QSize(0, 70));
        edUnitName->setMaximumSize(QSize(16777215, 70));
        edUnitName->setCursor(QCursor(Qt::BlankCursor));
        edUnitName->setFocusPolicy(Qt::NoFocus);
        edUnitName->setText(QString::fromUtf8(""));
        edUnitName->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(edUnitName);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(5);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        groupBox = new QGroupBox(UnitConfigStdFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("border-width:2"));
        verticalLayout_4 = new QVBoxLayout(groupBox);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        btMotorType = new QPushButton(groupBox);
        btMotorType->setObjectName(QString::fromUtf8("btMotorType"));
        QSizePolicy sizePolicy2(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(btMotorType->sizePolicy().hasHeightForWidth());
        btMotorType->setSizePolicy(sizePolicy2);
        btMotorType->setMinimumSize(QSize(110, 90));
        btMotorType->setMaximumSize(QSize(110, 90));
        btMotorType->setSizeIncrement(QSize(1, 1));
        btMotorType->setBaseSize(QSize(50, 50));
        btMotorType->setText(QString::fromUtf8("MotorType"));
        btMotorType->setIconSize(QSize(100, 100));
        btMotorType->setFlat(false);

        horizontalLayout_6->addWidget(btMotorType);

        horizontalSpacer_10 = new QSpacerItem(5, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_10);

        btFillSystem = new QPushButton(groupBox);
        btFillSystem->setObjectName(QString::fromUtf8("btFillSystem"));
        btFillSystem->setMinimumSize(QSize(110, 90));
        btFillSystem->setMaximumSize(QSize(110, 90));
        btFillSystem->setSizeIncrement(QSize(1, 1));
        btFillSystem->setBaseSize(QSize(50, 50));
        btFillSystem->setText(QString::fromUtf8("Fillsystem"));

        horizontalLayout_6->addWidget(btFillSystem);

        horizontalSpacer_6 = new QSpacerItem(5, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_6);

        btKnifeGate = new QPushButton(groupBox);
        btKnifeGate->setObjectName(QString::fromUtf8("btKnifeGate"));
        btKnifeGate->setMinimumSize(QSize(110, 90));
        btKnifeGate->setMaximumSize(QSize(110, 90));
        btKnifeGate->setText(QString::fromUtf8("KnifeGate"));

        horizontalLayout_6->addWidget(btKnifeGate);


        verticalLayout_4->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        btCilinder = new QPushButton(groupBox);
        btCilinder->setObjectName(QString::fromUtf8("btCilinder"));
        btCilinder->setMinimumSize(QSize(110, 90));
        btCilinder->setMaximumSize(QSize(110, 90));
        btCilinder->setSizeIncrement(QSize(1, 1));
        btCilinder->setBaseSize(QSize(50, 50));
        btCilinder->setText(QString::fromUtf8("Cilinder"));
        btCilinder->setIconSize(QSize(100, 100));

        horizontalLayout_7->addWidget(btCilinder);

        horizontalSpacer_9 = new QSpacerItem(5, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_9);

        btLevels = new QPushButton(groupBox);
        btLevels->setObjectName(QString::fromUtf8("btLevels"));
        btLevels->setMinimumSize(QSize(110, 90));
        btLevels->setMaximumSize(QSize(110, 90));
        btLevels->setSizeIncrement(QSize(1, 1));
        btLevels->setBaseSize(QSize(50, 50));
        btLevels->setText(QString::fromUtf8("Levels"));

        horizontalLayout_7->addWidget(btLevels);

        horizontalSpacer_7 = new QSpacerItem(5, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_7);

        btTacho = new QPushButton(groupBox);
        btTacho->setObjectName(QString::fromUtf8("btTacho"));
        btTacho->setMinimumSize(QSize(110, 90));
        btTacho->setMaximumSize(QSize(110, 90));
        btTacho->setText(QString::fromUtf8("Tacho"));

        horizontalLayout_7->addWidget(btTacho);


        verticalLayout_4->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        btGranules = new QPushButton(groupBox);
        btGranules->setObjectName(QString::fromUtf8("btGranules"));
        btGranules->setMinimumSize(QSize(110, 90));
        btGranules->setMaximumSize(QSize(110, 90));
        btGranules->setSizeIncrement(QSize(1, 1));
        btGranules->setBaseSize(QSize(50, 50));
        btGranules->setText(QString::fromUtf8("Granules"));
        btGranules->setIconSize(QSize(100, 100));

        horizontalLayout_8->addWidget(btGranules);

        horizontalSpacer_8 = new QSpacerItem(5, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_8);

        btSpare1 = new QPushButton(groupBox);
        btSpare1->setObjectName(QString::fromUtf8("btSpare1"));
        btSpare1->setMinimumSize(QSize(110, 90));
        btSpare1->setMaximumSize(QSize(110, 90));
        btSpare1->setText(QString::fromUtf8("Spare1"));

        horizontalLayout_8->addWidget(btSpare1);

        horizontalSpacer_2 = new QSpacerItem(5, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_2);

        btMcwType = new QPushButton(groupBox);
        btMcwType->setObjectName(QString::fromUtf8("btMcwType"));
        btMcwType->setMinimumSize(QSize(110, 90));
        btMcwType->setMaximumSize(QSize(110, 90));
        btMcwType->setText(QString::fromUtf8("McwType"));

        horizontalLayout_8->addWidget(btMcwType);


        verticalLayout_4->addLayout(horizontalLayout_8);


        horizontalLayout_3->addWidget(groupBox);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        groupBox_2 = new QGroupBox(UnitConfigStdFrm);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setStyleSheet(QString::fromUtf8("border-width:2"));
        groupBox_2->setTitle(QString::fromUtf8(""));
        verticalLayout_5 = new QVBoxLayout(groupBox_2);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        btGraviRpm = new QPushButton(groupBox_2);
        btGraviRpm->setObjectName(QString::fromUtf8("btGraviRpm"));
        btGraviRpm->setMinimumSize(QSize(110, 90));
        btGraviRpm->setMaximumSize(QSize(110, 90));
        btGraviRpm->setText(QString::fromUtf8("GraviRpm"));

        verticalLayout_5->addWidget(btGraviRpm);

        btLoadCell = new QPushButton(groupBox_2);
        btLoadCell->setObjectName(QString::fromUtf8("btLoadCell"));
        btLoadCell->setMinimumSize(QSize(110, 90));
        btLoadCell->setMaximumSize(QSize(110, 90));
        btLoadCell->setText(QString::fromUtf8("Loadcell"));

        verticalLayout_5->addWidget(btLoadCell);

        btTolerances = new QPushButton(groupBox_2);
        btTolerances->setObjectName(QString::fromUtf8("btTolerances"));
        btTolerances->setMinimumSize(QSize(110, 90));
        btTolerances->setMaximumSize(QSize(110, 90));
        btTolerances->setText(QString::fromUtf8("Tolerances"));

        verticalLayout_5->addWidget(btTolerances);


        horizontalLayout_3->addWidget(groupBox_2);


        verticalLayout_2->addLayout(horizontalLayout_3);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_4);


        horizontalLayout_5->addLayout(verticalLayout_2);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(-1, 35, -1, -1);

        horizontalLayout_5->addLayout(verticalLayout_7);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_3);

        btManIO = new QPushButton(UnitConfigStdFrm);
        btManIO->setObjectName(QString::fromUtf8("btManIO"));
        btManIO->setMinimumSize(QSize(110, 90));
        btManIO->setMaximumSize(QSize(110, 90));

        verticalLayout->addWidget(btManIO);

        btDiagnose = new QPushButton(UnitConfigStdFrm);
        btDiagnose->setObjectName(QString::fromUtf8("btDiagnose"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(btDiagnose->sizePolicy().hasHeightForWidth());
        btDiagnose->setSizePolicy(sizePolicy3);
        btDiagnose->setMinimumSize(QSize(110, 90));
        btDiagnose->setMaximumSize(QSize(110, 90));

        verticalLayout->addWidget(btDiagnose);

        btService = new QPushButton(UnitConfigStdFrm);
        btService->setObjectName(QString::fromUtf8("btService"));
        btService->setMinimumSize(QSize(110, 90));
        btService->setMaximumSize(QSize(110, 90));

        verticalLayout->addWidget(btService);

        btAdvCfg = new QPushButton(UnitConfigStdFrm);
        btAdvCfg->setObjectName(QString::fromUtf8("btAdvCfg"));
        btAdvCfg->setMinimumSize(QSize(110, 90));
        btAdvCfg->setMaximumSize(QSize(110, 90));
        btAdvCfg->setText(QString::fromUtf8("AdvCfg"));

        verticalLayout->addWidget(btAdvCfg);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_3 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        btOk = new QPushButton(UnitConfigStdFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout->addWidget(btOk);


        verticalLayout->addLayout(horizontalLayout);


        horizontalLayout_5->addLayout(verticalLayout);


        verticalLayout_3->addLayout(horizontalLayout_5);


        retranslateUi(UnitConfigStdFrm);

        QMetaObject::connectSlotsByName(UnitConfigStdFrm);
    } // setupUi

    void retranslateUi(QWidget *UnitConfigStdFrm)
    {
        lbCaption->setText(QApplication::translate("UnitConfigStdFrm", "Device Configuration", 0, QApplication::UnicodeUTF8));
        lbUnitName->setText(QApplication::translate("UnitConfigStdFrm", "Device ID:", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QString());
        btManIO->setText(QApplication::translate("UnitConfigStdFrm", "Manual I/O", 0, QApplication::UnicodeUTF8));
        btDiagnose->setText(QApplication::translate("UnitConfigStdFrm", "Diagnose", 0, QApplication::UnicodeUTF8));
        btService->setText(QApplication::translate("UnitConfigStdFrm", "Service", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(UnitConfigStdFrm);
    } // retranslateUi

};

namespace Ui {
    class UnitConfigStdFrm: public Ui_UnitConfigStdFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_UNITCONFIGSTD_H

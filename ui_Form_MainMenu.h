/********************************************************************************
** Form generated from reading UI file 'Form_MainMenu.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_MAINMENU_H
#define UI_FORM_MAINMENU_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainMenu
{
public:
    QVBoxLayout *mainLayout;
    QWidget *extWidget;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout;
    QPushButton *btMenu;
    QLabel *lbDateTime;
    QSpacerItem *horizontalSpacer;
    QPushButton *btHome;
    QPushButton *btBack;
    QPushButton *btSelect;
    QPushButton *btAlarm;
    QPushButton *btPrime;
    QPushButton *btOnOff;

    void setupUi(QWidget *MainMenu)
    {
        if (MainMenu->objectName().isEmpty())
            MainMenu->setObjectName(QString::fromUtf8("MainMenu"));
        MainMenu->resize(793, 455);
        MainMenu->setCursor(QCursor(Qt::BlankCursor));
        MainMenu->setWindowTitle(QString::fromUtf8("Form"));
        mainLayout = new QVBoxLayout(MainMenu);
        mainLayout->setSpacing(0);
        mainLayout->setContentsMargins(0, 0, 0, 0);
        mainLayout->setObjectName(QString::fromUtf8("mainLayout"));
        extWidget = new QWidget(MainMenu);
        extWidget->setObjectName(QString::fromUtf8("extWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(extWidget->sizePolicy().hasHeightForWidth());
        extWidget->setSizePolicy(sizePolicy);
        extWidget->setCursor(QCursor(Qt::BlankCursor));

        mainLayout->addWidget(extWidget);

        groupBox = new QGroupBox(MainMenu);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMinimumSize(QSize(0, 80));
        groupBox->setMaximumSize(QSize(16777215, 80));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        QBrush brush1(QColor(127, 127, 130, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        groupBox->setPalette(palette);
        groupBox->setCursor(QCursor(Qt::BlankCursor));
        groupBox->setAutoFillBackground(true);
        horizontalLayout = new QHBoxLayout(groupBox);
        horizontalLayout->setSpacing(2);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(4, 5, 4, -1);
        btMenu = new QPushButton(groupBox);
        btMenu->setObjectName(QString::fromUtf8("btMenu"));
        btMenu->setMinimumSize(QSize(90, 70));
        btMenu->setMaximumSize(QSize(90, 70));
        btMenu->setCursor(QCursor(Qt::BlankCursor));
        btMenu->setText(QString::fromUtf8("MENU"));
        btMenu->setCheckable(false);
        btMenu->setChecked(false);

        horizontalLayout->addWidget(btMenu);

        lbDateTime = new QLabel(groupBox);
        lbDateTime->setObjectName(QString::fromUtf8("lbDateTime"));
        lbDateTime->setMinimumSize(QSize(90, 70));
        lbDateTime->setMaximumSize(QSize(90, 70));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::Base, brush);
        QBrush brush2(QColor(226, 226, 226, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush2);
        lbDateTime->setPalette(palette1);
        lbDateTime->setCursor(QCursor(Qt::BlankCursor));
        lbDateTime->setLayoutDirection(Qt::LeftToRight);
        lbDateTime->setAutoFillBackground(true);
        lbDateTime->setFrameShape(QFrame::Box);
        lbDateTime->setText(QString::fromUtf8("Time"));
        lbDateTime->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(lbDateTime);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btHome = new QPushButton(groupBox);
        btHome->setObjectName(QString::fromUtf8("btHome"));
        btHome->setMinimumSize(QSize(90, 70));
        btHome->setMaximumSize(QSize(90, 70));
        QFont font;
        font.setBold(false);
        font.setWeight(50);
        btHome->setFont(font);
        btHome->setCursor(QCursor(Qt::BlankCursor));
        btHome->setText(QString::fromUtf8("HOME"));

        horizontalLayout->addWidget(btHome);

        btBack = new QPushButton(groupBox);
        btBack->setObjectName(QString::fromUtf8("btBack"));
        btBack->setMinimumSize(QSize(90, 70));
        btBack->setMaximumSize(QSize(90, 70));
        btBack->setCursor(QCursor(Qt::BlankCursor));
        btBack->setText(QString::fromUtf8("BACK"));

        horizontalLayout->addWidget(btBack);

        btSelect = new QPushButton(groupBox);
        btSelect->setObjectName(QString::fromUtf8("btSelect"));
        btSelect->setMinimumSize(QSize(90, 70));
        btSelect->setMaximumSize(QSize(90, 70));
        btSelect->setFont(font);
        btSelect->setCursor(QCursor(Qt::BlankCursor));
        btSelect->setText(QString::fromUtf8("SEL"));

        horizontalLayout->addWidget(btSelect);

        btAlarm = new QPushButton(groupBox);
        btAlarm->setObjectName(QString::fromUtf8("btAlarm"));
        btAlarm->setEnabled(false);
        btAlarm->setMinimumSize(QSize(90, 70));
        btAlarm->setMaximumSize(QSize(90, 70));
        btAlarm->setCursor(QCursor(Qt::BlankCursor));
        btAlarm->setText(QString::fromUtf8("ALARM"));

        horizontalLayout->addWidget(btAlarm);

        btPrime = new QPushButton(groupBox);
        btPrime->setObjectName(QString::fromUtf8("btPrime"));
        btPrime->setMinimumSize(QSize(90, 70));
        btPrime->setMaximumSize(QSize(90, 70));
        btPrime->setCursor(QCursor(Qt::BlankCursor));
        btPrime->setText(QString::fromUtf8("PRIME"));

        horizontalLayout->addWidget(btPrime);

        btOnOff = new QPushButton(groupBox);
        btOnOff->setObjectName(QString::fromUtf8("btOnOff"));
        btOnOff->setMinimumSize(QSize(90, 70));
        btOnOff->setMaximumSize(QSize(90, 70));
        btOnOff->setCursor(QCursor(Qt::BlankCursor));
        btOnOff->setText(QString::fromUtf8(""));
        QIcon icon;
        icon.addFile(QString::fromUtf8("images/butOn.png"), QSize(), QIcon::Normal, QIcon::Off);
        btOnOff->setIcon(icon);
        btOnOff->setIconSize(QSize(77, 66));
        btOnOff->setChecked(false);

        horizontalLayout->addWidget(btOnOff);


        mainLayout->addWidget(groupBox);


        retranslateUi(MainMenu);

        QMetaObject::connectSlotsByName(MainMenu);
    } // setupUi

    void retranslateUi(QWidget *MainMenu)
    {
        groupBox->setTitle(QString());
        Q_UNUSED(MainMenu);
    } // retranslateUi

};

namespace Ui {
    class MainMenu: public Ui_MainMenu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_MAINMENU_H

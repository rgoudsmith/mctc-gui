#ifndef KEYBOARDFRM_H
#define KEYBOARDFRM_H

#include <QtGui/QWidget>
#include "Form_QbaseWidget.h"

namespace Ui
{
  class KeyboardFrm;
}


class KeyboardFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    KeyboardFrm(QWidget *parent = 0);
    ~KeyboardFrm();
    void FormInit();
    void SetText(QString txt);
    void Clear();
    void SetMaxLength(int);

  protected:
    void changeEvent(QEvent *e);
    void showEvent(QShowEvent *);

  private:
    Ui::KeyboardFrm *m_ui;
    QString input;
    int maxLength;
    int capsState;
    bool emitSignal;
    void updateKeys();

  private slots:
    void shiftClicked();
    void buttonClicked();
    void backspaceClicked();
    void spaceClicked();
    void clearClicked();
    void okClicked();
    void cancelClicked();

    signals:
    void inputText(const QString &);
};
#endif                                                                          // KEYBOARDFRM_H

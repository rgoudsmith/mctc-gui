#ifndef SPLASHFRM_H
#define SPLASHFRM_H

#include <QWidget>
#include <QTimer>

namespace Ui
{
  class SplashFrm;
}


class SplashFrm : public QWidget
{
  Q_OBJECT
    public:
    SplashFrm(QWidget *parent = 0);
    ~SplashFrm();
    void ShowScreen(int time);
    void ShowScreen();
    QWidget * menu;

  protected:
    void changeEvent(QEvent *e);

  private:
    Ui::SplashFrm *ui;
    QTimer * closeTim;

  private slots:
    void closeTimEvent();
};
#endif                                                                          // SPLASHFRM_H

/****************************************************************************
** Meta object code from reading C++ file 'Form_AdvConfig2.h'
**
** Created: Wed Feb 6 11:14:45 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_AdvConfig2.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_AdvConfig2.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AdvConfig2Frm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x08,
      37,   32,   14,   14, 0x08,
      69,   14,   14,   14, 0x08,
      87,   14,   14,   14, 0x08,
     106,   14,   14,   14, 0x08,
     123,   14,   14,   14, 0x08,
     139,   14,   14,   14, 0x08,
     156,   14,   14,   14, 0x08,
     173,   14,   14,   14, 0x08,
     191,   14,   14,   14, 0x08,
     215,   14,   14,   14, 0x08,
     240,   14,   14,   14, 0x08,
     263,   14,   14,   14, 0x08,
     289,   14,   14,   14, 0x08,
     312,   14,   14,   14, 0x08,
     332,  330,   14,   14, 0x08,
     359,   14,   14,   14, 0x08,
     380,   14,   14,   14, 0x08,
     399,   14,   14,   14, 0x08,
     424,   14,   14,   14, 0x08,
     442,   14,   14,   14, 0x08,
     458,   14,   14,   14, 0x0a,
     482,  476,   14,   14, 0x0a,
     513,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_AdvConfig2Frm[] = {
    "AdvConfig2Frm\0\0LanguageUpdate()\0,,,,\0"
    "IpReceived(int,int,int,int,int)\0"
    "ToolPassClicked()\0SuperPassClicked()\0"
    "ModAddrClicked()\0IpAddrClicked()\0"
    "NetmaskClicked()\0GatewayClicked()\0"
    "SlaveCntClicked()\0ToolPassReceived(float)\0"
    "SuperPassReceived(float)\0"
    "ModAddrReceived(float)\0SlaveCountReceived(float)\0"
    "ButtonConfirmClicked()\0DateTimeClicked()\0"
    ",\0SelectionReceived(int,int)\0"
    "ValueReceived(float)\0MessageResult(int)\0"
    "FactoryDefaultsClicked()\0AlarmCfgClicked()\0"
    "ButtonClicked()\0LanguageChanged()\0"
    "_type\0MstCfg_InputTypeChanged(eType)\0"
    "MstCfg_DateTimeChanged(QDateTime)\0"
};

const QMetaObject AdvConfig2Frm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_AdvConfig2Frm,
      qt_meta_data_AdvConfig2Frm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AdvConfig2Frm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AdvConfig2Frm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AdvConfig2Frm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AdvConfig2Frm))
        return static_cast<void*>(const_cast< AdvConfig2Frm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int AdvConfig2Frm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: LanguageUpdate(); break;
        case 1: IpReceived((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 2: ToolPassClicked(); break;
        case 3: SuperPassClicked(); break;
        case 4: ModAddrClicked(); break;
        case 5: IpAddrClicked(); break;
        case 6: NetmaskClicked(); break;
        case 7: GatewayClicked(); break;
        case 8: SlaveCntClicked(); break;
        case 9: ToolPassReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 10: SuperPassReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 11: ModAddrReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 12: SlaveCountReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 13: ButtonConfirmClicked(); break;
        case 14: DateTimeClicked(); break;
        case 15: SelectionReceived((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 16: ValueReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 17: MessageResult((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: FactoryDefaultsClicked(); break;
        case 19: AlarmCfgClicked(); break;
        case 20: ButtonClicked(); break;
        case 21: LanguageChanged(); break;
        case 22: MstCfg_InputTypeChanged((*reinterpret_cast< eType(*)>(_a[1]))); break;
        case 23: MstCfg_DateTimeChanged((*reinterpret_cast< QDateTime(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 24;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

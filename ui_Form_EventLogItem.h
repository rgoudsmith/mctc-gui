/********************************************************************************
** Form generated from reading UI file 'Form_EventLogItem.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_EVENTLOGITEM_H
#define UI_FORM_EVENTLOGITEM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EventLogItemFrm
{
public:
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *lbDate;
    QLabel *lbTime;
    QLabel *lbDevice;
    QLabel *lbIcon;
    QLabel *lbCode;
    QSpacerItem *horizontalSpacer;
    QFrame *line;
    QLabel *label;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *EventLogItemFrm)
    {
        if (EventLogItemFrm->objectName().isEmpty())
            EventLogItemFrm->setObjectName(QString::fromUtf8("EventLogItemFrm"));
        EventLogItemFrm->resize(890, 109);
        EventLogItemFrm->setCursor(QCursor(Qt::BlankCursor));
        EventLogItemFrm->setStyleSheet(QString::fromUtf8("#frame {border-color: black; border: 1px solid; border-radius:3px;}"));
        verticalLayout_2 = new QVBoxLayout(EventLogItemFrm);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        frame = new QFrame(EventLogItemFrm);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame);
        verticalLayout->setContentsMargins(5, 5, 5, 5);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lbDate = new QLabel(frame);
        lbDate->setObjectName(QString::fromUtf8("lbDate"));
        lbDate->setMinimumSize(QSize(0, 0));
        lbDate->setMaximumSize(QSize(16777215, 16777215));
        lbDate->setText(QString::fromUtf8("<date>"));

        horizontalLayout->addWidget(lbDate);

        lbTime = new QLabel(frame);
        lbTime->setObjectName(QString::fromUtf8("lbTime"));
        lbTime->setMinimumSize(QSize(0, 0));
        lbTime->setMaximumSize(QSize(16777215, 16777215));
        lbTime->setText(QString::fromUtf8("<time>"));

        horizontalLayout->addWidget(lbTime);

        lbDevice = new QLabel(frame);
        lbDevice->setObjectName(QString::fromUtf8("lbDevice"));
        lbDevice->setMinimumSize(QSize(0, 0));
        lbDevice->setMaximumSize(QSize(16777215, 16777215));
        lbDevice->setText(QString::fromUtf8("<devID>"));

        horizontalLayout->addWidget(lbDevice);

        lbIcon = new QLabel(frame);
        lbIcon->setObjectName(QString::fromUtf8("lbIcon"));
        lbIcon->setMinimumSize(QSize(0, 0));
        lbIcon->setMaximumSize(QSize(16777215, 16777215));
        lbIcon->setText(QString::fromUtf8("<.img>"));

        horizontalLayout->addWidget(lbIcon);

        lbCode = new QLabel(frame);
        lbCode->setObjectName(QString::fromUtf8("lbCode"));
        lbCode->setMinimumSize(QSize(0, 0));
        lbCode->setMaximumSize(QSize(16777215, 16777215));
        lbCode->setText(QString::fromUtf8("<alm nr>"));

        horizontalLayout->addWidget(lbCode);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout);

        line = new QFrame(frame);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShadow(QFrame::Plain);
        line->setFrameShape(QFrame::HLine);

        verticalLayout->addWidget(line);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        label->setText(QString::fromUtf8("TextLabel"));
        label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout->addWidget(label);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        verticalLayout_2->addWidget(frame);


        retranslateUi(EventLogItemFrm);

        QMetaObject::connectSlotsByName(EventLogItemFrm);
    } // setupUi

    void retranslateUi(QWidget *EventLogItemFrm)
    {
        EventLogItemFrm->setWindowTitle(QApplication::translate("EventLogItemFrm", "Form", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class EventLogItemFrm: public Ui_EventLogItemFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_EVENTLOGITEM_H

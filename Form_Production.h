#ifndef PRODUCTIONFRM_H
#define PRODUCTIONFRM_H

#include <QtGui/QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include "Form_QbaseWidget.h"
#include "Form_NumericInput.h"
#include "Form_Tacho.h"
#include "Form_Calibration.h"
#include "Form_Keyboard.h"

namespace Ui
{
  class ProductionFrm;
}


enum eFieldType
{
  ftNone,
  ftColPct,
  ftShotWeight,
  ftShotTime,
  ftExtCap,
  ftTimeAct,
  ftExtCapAct,
  ftTachoAct,
  ftAll,
  ftCount
};

class ProductionFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    ProductionFrm(QWidget *parent = 0);
    ~ProductionFrm();
    eMvcDispState curState;
    void FormInit();
    void SetInitState();
    void SetRecipeDataChanged(void);
    void ResetRecipeDataChanged(void);
    bool recipeChanged;
    Ui::ProductionFrm *m_ui;

    bool calibVisible();
    // Buttons
    QPushButton * btMidDetail;
    QPushButton * btRecipe;
    QPushButton * btTachoSet;
    QPushButton * btRpmSet1;
    // Edits
    QLineEdit * edInput2;
    QLineEdit * edInput1;
    // Labels
    QLabel * lbInput2;
    QLabel * lbInput1;

    // Display labels for values
    QLabel * lbRecipe;
    QLabel * lbRecipeName;
    QLabel * edInput1Label;
    QLabel * edInput2Label;
    QLabel * lbValue1Caption;
    QLabel * lbValue1Value;
    QLabel * lbValue2Caption;
    QLabel * lbValue2Value;
    QLabel * lbProdStatus1;
    QLabel * lbProdStatus2;
    QLabel * timer;

  protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *e);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
    bool eventFilter(QObject *obj, QEvent *ev);

  private:
//    Ui::ProductionFrm *m_ui;
    NumericInputFrm *numInput;
    KeyboardFrm * txtInput;
    TachoFrm *tachoForm;
    CalibrationFrm *calibForm;

    int editIdx;
    bool showEdit1;
    bool showEdit2;
    bool isRpmMode;
    bool showProdSts;
    bool showTachoButton;
    void SetupDisplay();
    void PositionProdStatus();
    void DisplayEditFields();
    void SetEditField(eFieldType, bool, bool);
    void SetRecipeMaterialButtons();
    void LanguageUpdate();

  private slots:
    void btDisplayClicked();
    void ValueReceived(float);
    void TextReceived(const QString &);
    void btTachoClicked();
    void Material1Clicked();
    void Material2Clicked();
    void RecipeClicked();

  public slots:
    void SetDisplayState(eMvcDispState);
    void MstCfg_UserChanged(eUserType);
    void RcpEnabledChanged(bool);
    void MstCfg_ShotUnitsChanged();
    void MstCfg_TimeUnitsChanged();
    void SysCfg_ShotWeightChanged(float,int idx=0);
    void SysCfg_ShotTimeChanged(float, int idx=0);
    void SysSts_ShotTimeChanged(float,int);
    void SysSts_MeteringActChanged(float,int);
    void SysCfg_MeteringStartChanged(float,int);
    void SysSts_MeteringValidChanged(bool,int);
    void SysCfg_ExtCapChanged(float, int idx=0);
    void SysSts_ExtCapChanged(float,int);
    void TachoChanged(float, int);
    void SysSts_LearnStsChanged(bool,int);
    void SetBalanceCalibratedVisible(bool);
    void ModeChanged(eMode);
    void GraviRpmChanged(eGraviRpm,int);
    void TypeChanged(eType);
    void ResetCalibValues();
    void MCStatusChanged(eMCStatus _sts, int idx);
    void SlaveCountChanged(int);
    void ActUnitIndexChanged(int);
    void RecipeChanged(const QString &);

    signals:
    void RecipeSelected(const QString &);
    void MaterialSelected(const QString &);
};
#endif                                                                          // PRODUCTIONFRM_H

/*=============================================================================================*/
/* Project number       : MVC0012                                                              */
/* Project descrtiption : MC-TC QT Application                                                 */
/*---------------------------------------------------------------------------------------------*/
/* Version number       : 1.00                                                                 */
/* Version date         : 01-11-2011                                                           */
/*---------------------------------------------------------------------------------------------*/
/* History              : 1-5-2010   Programming started by Thijs Bijleveld                    */
/*                      : 1-8-2011   Various changes programmed by Jan Bart and Rob Hartman    */
/*=============================================================================================*/

#include <QtGui/QApplication>
#include <QMessageBox>
#include <QMetaType>
#include <QTranslator>
#include <QProcess>
#include <QWSServer>
#include <QSplashScreen>
#include "Form_MainWindow.h"
#include "Form_MainMenu.h"
#include "Form_OverlayMenu.h"
#include "ImageDef.h"
#include "Com.h"
#include "Rout.h"

int main(int argc, char *argv[])
{
  /*---------------------*/
  /* MC-TC Main programm */
  /*---------------------*/
  QApplication app(argc, argv);
  QTranslator translator;
  QFile fp;
  QString fileData;
  MainWindow w;
  QString msg;

  //QTextCodec::setCodecForTr(QTextCodec::codecForName("GB18030-0"));
  QTextCodec::setCodecForTr(QTextCodec::codecForName("GB18030-0"));
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("GB18030-0"));

  qDebug() << "ComShare version " << COMSHARE_VERSION;

  qDebug("Program started");

  /*----- Splash screen -----*/
  QPixmap pixmap("/opt/MovaColor/images/SplashScreen.png");                     /* PixMap file name */
  QSplashScreen splash(pixmap);                                                 /* Create splashscreen */
  QFont font("FreeSans", 24, QFont::Bold);                                      /* Set splah font size */
  splash.setFont(font);
  splash.show();                                                                /* Show it */

  /*----- Build startup line -----*/
  #ifdef QT_NO_DEBUG
  #warning Release version
  msg = "Software version "APP_VERSION"r starting";                             /* Release version */
  #else
  #warning Debug version
  msg = "Software version "APP_VERSION"d starting";                             /* Debug version */
  #endif

  splash.showMessage(msg);

  QWSServer::setCursorVisible(false);                                           /* Cursor invisible throughout the entire application.*/
  //  QWSServer::setCursorVisible(true);                                           /* Cursor visible throughout the entire application.*/

  qDebug("Program initializing");

  /*----- Add progress dot -----*/
  msg += ".";
  splash.showMessage(msg);

  app.setApplicationName(fileInfo.appName);
  app.setApplicationVersion(fileInfo.version);
  app.installTranslator(&translator);

  /*----- Setup global application path -----*/
  Rout::appPath = app.applicationDirPath();
  qRegisterMetaType<tMsgControlUnit>();
  qRegisterMetaType<tMsgGuiUnit>();

  /*----- Create the global class and setup the global pointer to this class -----*/
  glob = new global();

  /*----- Open main menu -----*/
  MainMenu *menu = new MainMenu(&w);
  OverlayMenu *oMenu = new OverlayMenu(&w);

  oMenu->HideMenuOnClick(true);

  qDebug("Set AppPath start");

  /*----- Add progress dot -----*/
  msg += ".";
  splash.showMessage(msg);

  /*----- Reference appPath to mainmenu -----*/
  if ((!Rout::appPath.isEmpty()) && (!Rout::appPath.isNull())) {
    menu->SetAppPath(&Rout::appPath);
  }
  else {
    QMessageBox::information(&w,"App information","AppPath is not set");
  }
  qDebug("Set AppPath ready");

  /*----- Add progress dot -----*/
  msg += ".";
  splash.showMessage(msg);

  menu->GBI()->SetTranslator(&translator);

  /*----- Force loading of language -----*/
  eLanguages i = menu->GBI()->mstCfg.language;
  menu->GBI()->mstCfg.language = lanUK;
  menu->GBI()->MstCfg_SetLanguage(i);

  qDebug("Initializing QT");

  /*----- Add progress dot -----*/
  msg += ".";
  splash.showMessage(msg);

  /*----- Display menu, which is actually the main application screen -----*/
  menu->show();

  /*----- By default, hide the overlay menu -----*/
  oMenu->hide();

  /*----- Position the overlay menu according to the menu -----*/
  oMenu->Reposition(menu->width(),menu->height());

  /*----- Link the overlay menu to the menu -----*/
  menu->SetOverlayMenu(oMenu);
  menu->initOverlay();

  /*----- Link MenuButton_Clicked SIGNAL to DisplayMenu SLOT -----*/
  menu->connect(menu,SIGNAL(MenuClicked()),oMenu,SLOT(DisplayMenu()));
  menu->connect(oMenu,SIGNAL(MenuButtonClicked(eWindowIdx)),menu,SLOT(DisplayWindow(eWindowIdx)));

  /*----- Show first window -----*/
  if (!Rout::MultiUnitHomeScreen()) {
    menu->DisplayWindow(wiHome);                        /* Single or twin unit home screen */
  }
  else {
    menu->DisplayWindow(wiHomeMultiUnit);               /* Multi unit home screen */
  }

  /*---- Show main window -----*/
  //w.show();
  w.showFullScreen();
  w.setCursor(Qt::BlankCursor);

  /*----- Set the menu as central widget of the mainWindow -----*/
  w.setCentralWidget(menu);

  for (int i=0; i<CFG_MAX_UNIT_COUNT; i++) {
    menu->GBI()->TriggerUpdate(i);
  }

  qDebug("QT started");

  /*----- Add progress dot -----*/
  msg += ".";
  splash.showMessage(msg);

  /*----- Force cursor to always active in the application -----*/
  //  app.setOverrideCursor(QCursor(Qt::ArrowCursor));

  return app.exec();
}

/*===========================================================================*
 * File        : Udp.cpp                                                     *
 * Project     : MVC0012 - MC-TC                                             *
 * Description : UDP message server for McLan.                               *
 *===========================================================================*/

#include <QtNetwork>
#include "Common.h"
#include "Udp.h"
#include "Glob.h"
#include "Rout.h"

int dynLogEnb;                                                                  /* Dynamic log enable */

/*----- Balance control process data -----*/
byte actBandNr;                                                                 /* Actual band number */
float sampleLen;                                                                /* Measurement time */
float corFac;                                                                   /* RPM correction factor */
byte standStill;                                                                /* Standstill */

Udp::Udp(QObject *parent) : QObject(parent)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  udpSocket = new QUdpSocket(this);
  udpSocket->bind(30012, QUdpSocket::ShareAddress);
  dynLogEnb = FALSE;
}


void Udp::FormInit()
{
  /*-----------*/
  /* Init form */
  /*-----------*/

  /*----- Connect ComReceived to fetch the Balance control data -----*/
  connect(glob,SIGNAL(ComReceived(tMsgControlUnit,int)),this,SLOT(ComMsgReceived(tMsgControlUnit,int)));

  /*----- Connect UDP message received signal -----*/
  connect(udpSocket, SIGNAL(readyRead()),this, SLOT(UdpReceive()));
}


void Udp::InitUpdWd(void)
{
  /*----------------------------------------------------------------------------*
   * PROCEDURE    : Initialize the UDP watchdog timer.                          *
   *----------------------------------------------------------------------------*/
  udpWdTim = new QTimer(this);
  udpWdTim->setSingleShot(false);
  udpWdTim->setInterval(5 * 1000);
  connect(udpWdTim,SIGNAL(timeout()),SLOT(UdpWdTimeOut()));
  udpWdTim->start();
}


void Udp::UdpReceive(void)
{
  /*------------------------------------*/
  /* UDP message receive funcion        */
  /* Connected to SIGNAL(readyRead())   */
  /*------------------------------------*/
  tUdpBuf udpBuf;
  word nameLen;
  QString ip0,ip1,ip2,ip3;

  #define pcHdr ((tPcHdr*)&udpBuf.data)
  #define mcLan ((tMcLan*)&udpBuf.data)
  #define bootInfo ((tBootInfo*)&udpBuf.data)

  /*----- Loop for all Data -----*/
  while (udpSocket->hasPendingDatagrams()) {
    /*----- Read datagram -----*/
    udpSocket->readDatagram((char*)udpBuf.data,udpSocket->pendingDatagramSize());

    /*----- Handle command -----*/
    if(pcHdr->msgType == 1) {                                                   /* Command message type ? */

      switch (pcHdr->msgCmd) {
        /*----- McLan message -----*/
        case(1) :

          /*----- Setup unit name -----*/
          nameLen =  glob->sysCfg[0].MC_ID.length();                            /* Fetch name lenght */
          if (nameLen > 16) nameLen = 16;                                       /* Clip it to max 16 */

          memset(mcLan->unitName,0x20,16);                                      /* Blank name */

          /* Copy unit name from configuration */
          memcpy(mcLan->unitName,glob->sysCfg[0].MC_ID.toAscii(),nameLen);

          /*----- IP address to mcLan structure -----*/
          //        mcLan->ip0 = glob->mstCfg.IPAddress[3];
          //        mcLan->ip1 = glob->mstCfg.IPAddress[2];
          //        mcLan->ip2 = glob->mstCfg.IPAddress[1];
          //        mcLan->ip3 = glob->mstCfg.IPAddress[0];

          mcLan->ip0 = Rout::ip[0];
          mcLan->ip1 = Rout::ip[1];
          mcLan->ip2 = Rout::ip[2];
          mcLan->ip3 = Rout::ip[3];

          /*----- Return Karo module MAC adress ------*/
          mcLan->mac0 = Rout::SwapWord(Rout::mac[0]);
          mcLan->mac1 = Rout::SwapWord(Rout::mac[1]);
          mcLan->mac2 = Rout::SwapWord(Rout::mac[2]);

          mcLan->msgType = 2;                                                   /* Change message type to reply */

          /*----- Send reply -----*/
          udpSocket->writeDatagram((const char*)udpBuf.data,sizeof(tMcLan),QHostAddress::Broadcast, 30013);

          break;

          /*----- Bootloader info request -----*/
        case(2) :

          /*----- Data to message -----*/
          bootInfo->mac0 = Rout::SwapWord(Rout::mac[0]);
          bootInfo->mac1 = Rout::SwapWord(Rout::mac[1]);
          bootInfo->mac2 = Rout::SwapWord(Rout::mac[2]);

          bootInfo->blVersion = 0x1234;

          /*----- Setup MC30 prog info structure (Fake a MC-Balance) -----*/
          bootInfo->userPrgInfo.userVersion = 0x500;

          strcpy(bootInfo->userPrgInfo.fileName,"McBal-V50-EN-V50");
          strcpy(bootInfo->userPrgInfo.languageId,"ENG");
          strcpy(bootInfo->userPrgInfo.buildTime,"11:13:00");
          strcpy(bootInfo->userPrgInfo.buildDate,"Nov. 15 2011");
          strcpy(bootInfo->userPrgInfo.info,"McBalance version 5.0");

          bootInfo->dipSwitch = 0;

          strcpy(bootInfo->unitName,"MC-TC");

          bootInfo->msgType = 2;                                                /* Change message type to reply */

          /*----- Send reply -----*/
          udpSocket->writeDatagram((const char*)udpBuf.data,sizeof(tBootInfo),QHostAddress::Broadcast, 30013);

          break;

          /*----- Reset CPU command (Not used) -----*/
        case(3) :
          break;

          /*----- Eanble dynamic log -----*/
        case(4) :
          /*----- Dynamic log already busy ? -----*/
          if (dynLogEnb == FALSE) {
            /*----- Connect signal SigSendUdp to SendUdpData function -----*/
            connect(glob,SIGNAL(SigSendUdp()),this,SLOT(SendUdpData()));
            /*----- Init UPD WD timer -----*/
            InitUpdWd();
            dynLogEnb = TRUE;
          }
          else {
            /*----- Re-trigger WD timer -----*/
            udpWdTim->start();                                                  /* Restart wd timer */
          }
          break;
      }
    }
  }
  #undef pcHdr
  #undef mcLan
  #undef bootInfo
}


void Udp::SendUdpData(void)
{
  /*-----------------------------------*/
  /* Send an UDP data message to McLan */
  /* Connected to SIGNAL(readyRead())  */
  /*-----------------------------------*/
  tUdpBuf udpBuf;
  tDynLogData *logData;
  tPcHdr *pcHeader;

  #define pcHdr ((tPcHdr*)&udpBuf.data)
  #define mcLan ((tMcLan*)&udpBuf.data)
  #define bootInfo ((tBootInfo*)&udpBuf.data)

  if (dynLogEnb) {

    logData = (tDynLogData*)&udpBuf.data[3];
    pcHeader = (tPcHdr*)&udpBuf.data[0];

    pcHeader->msgCmd = 4;
    pcHeader->msgType = 2;

    logData->logCode = LOG_FILL_VALVE;
    logData->dataType = UDP_BYTE_TYPE;
    logData->data = 0;                                                          /* Waar staat de status van de fill valve ? */
    logData++;

    logData->logCode = LOG_ALARM_NR_DYN;
    logData->dataType = UDP_BYTE_TYPE;
    /*----- Convert alarm code to MC-30 alarm code -----*/
    logData->data = Rout::GetMc30AlarmCode(glob->sysSts[0].alarmSts.nr);
    logData++;

    logData->logCode = LOG_STATUS;
    logData->dataType = UDP_BYTE_TYPE;
    /*----- Convert status to MC-30 compatible status -----*/
    if (glob->sysSts[0].MCStatus >= 1) {
      logData->data = glob->sysSts[0].MCStatus-1;
    }
    else {
      logData->data = 0;
    }
    logData++;

    logData->logCode = LOG_DOS_TIME_ACT;
    logData->dataType = UDP_WORD_TYPE;
    /*---- Return the Set dos time as act dos time in INJ / TIMER mode -----*/
    if ((glob->mstCfg.mode == modInject) && (glob->mstCfg.inpType == ttTimer)) {
      logData->data = glob->sysCfg[0].injection.shotTime*10;                    /* Set dos time */
    }
    else {
      logData->data = glob->sysSts[0].injection.meteringAct*10;                 /* Act dos time */
    }
    logData++;

    logData->logCode = LOG_TACHO_VOLT;
    logData->dataType = UDP_FLOAT_TYPE;
    logData->data = glob->sysSts[0].extrusion.tachoAct;
    logData++;

    logData->logCode = LOG_RPM_ACT;
    logData->dataType = UDP_FLOAT_TYPE;
    logData->data =glob->sysSts[0].rpm;
    logData++;

    logData->logCode = LOG_WEIGHT;
    logData->dataType = UDP_FLOAT_TYPE;
    logData->data = glob->sysSts[0].mass;
    logData++;

    logData->logCode = LOG_COLCAP_SET;
    logData->dataType = UDP_FLOAT_TYPE;
    logData->data = glob->sysSts[0].setProd;
    logData++;

    logData->logCode = LOG_COLCAP_ACT;
    logData->dataType = UDP_FLOAT_TYPE;
    logData->data = glob->sysSts[0].actProd;
    logData++;

    logData->logCode = LOG_CONSUMPTION;
    logData->dataType = UDP_FLOAT_TYPE;
    logData->data = 0;                                                          /* Not yet known in QT */
    logData++;

    /*----- Movacolor data -----*/
    logData->logCode =  LOG_STAND_STILL;
    logData->dataType = UDP_BYTE_TYPE;
    logData->data = standStill;
    logData++;

    logData->logCode =  LOG_BAND_NR;
    logData->dataType = UDP_BYTE_TYPE;
    logData->data = actBandNr;
    logData++;

    logData->logCode =  LOG_SAMPLE_LEN;
    logData->dataType = UDP_WORD_TYPE;
    logData->data = sampleLen;
    logData++;

    logData->logCode =  LOG_RPM_CORFAC;
    logData->dataType = UDP_FLOAT_TYPE;
    logData->data = corFac;
    logData++;

    /*----- Send UDP data -----*/
    udpSocket->writeDatagram((const char*)udpBuf.data,3+(sizeof(tDynLogData)*14),QHostAddress::Broadcast, 30013);
  }
}


void Udp::UdpWdTimeOut(void)
{
  /*------------------------------------*/
  /* UDP watchdog timerout              */
  /*-------------------------------------*/
  /*----- Disconnect SendUdp signal -----*/
  disconnect(glob,SIGNAL(SigSendUdp()),this,SLOT(SendUdpData()));
  udpWdTim->stop();                                                             /* Stop wd timer */
  dynLogEnb = FALSE;                                                            /* Reset dynamic log enable */
}


void Udp::ComMsgReceived(tMsgControlUnit msgData,int idx)
{
  /*---------------------------------*/
  /* Connected so signal ComReceived */
  /*---------------------------------*/

  if (idx) {                                                                    /* Skip 'Variable not used' compiler warning */
  }

  /*----- Fetch actual balance data from message and store it into local variables -----*/
  actBandNr = msgData.bandNr;                                                   /* Band nr */
  sampleLen = msgData.sampleLen;                                                /* Sample len */
  corFac = msgData.rpmCorr;                                                     /* Correction factor */
  standStill = msgData.standStill;                                              /* Standstill */
}

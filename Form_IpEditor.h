#ifndef IPEDITORFRM_H
#define IPEDITORFRM_H

#include <QWidget>
#include "Form_QbaseWidget.h"

namespace Ui
{
  class IpEditorFrm;
}


class IpEditorFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit IpEditorFrm(QWidget *parent = 0);
    ~IpEditorFrm();
    void FormInit();

  protected:
    bool eventFilter(QObject *, QEvent *);
    void resizeEvent(QResizeEvent *);
    void showEvent(QShowEvent *);

  private:
    Ui::IpEditorFrm *ui;
    int actField;
    int ipType;
    bool firstInp;
    int val1,val2,val3,val4;
    bool CheckInput(int);
    QLabel * selBox;
    void SetSelBox(int);

  private slots:
    void OkClicked();
    void CancelClicked();
    void DotClicked();
    void BackClicked();
    void DisplayValue(int);
    void ButtonClicked();

  public slots:
    void InitIp(int,int,int,int,int);

    signals:
    void SendIp(int,int,int,int,int);
};
#endif                                                                          // IPEDITORFRM_H

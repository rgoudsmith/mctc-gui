/*===========================================================================*
 * File        : UsbAccess.cpp                                               *
 * Project     : MVC0012 - MC-TC                                             *
 * Description : USB memory stick access routines.                           *
 *===========================================================================*/

#include "Form_MainMenu.h"
#include "UsbAccess.h"
#include <QTextStream>
#include <QProcess>
#include <QFile>
#include <QDir>
#include "ImageDef.h"
#include "Glob.h"
#include <QString>
#include <QList>
#include "Rout.h"

UsbAccess::UsbAccess(QObject *parent) : QObject(parent)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  _gbi = glob;                                                                  /* Setup local GBI pointer */
  usbPresent = false;
}


void UsbAccess::FormInit()
{
}


bool UsbAccess::DirExists(const QString &path)
{
  /*------------------------------------------*/
  /* Check for a certain dir on the USB stick */
  /*------------------------------------------*/
  QString dir = Rout::usbPath+path;
  QDir *checkDir = new QDir(dir);
  bool res = checkDir->exists();
  delete checkDir;
  return res;
}


int UsbAccess::DirCreate(const QString &path)
{
  /*-------------------------------------*/
  /* Create a directory on the USB stick */
  /*-------------------------------------*/
  int retVal;
  QString dir = Rout::usbPath + path;
  if (DirExists(path)) {
    retVal = 1;
  }
  else {
    QDir *mkDir = new QDir(dir);
    if (mkDir->mkpath(mkDir->path())) {
      delete mkDir;                                                             // Free the QDir mkDir object
      retVal = 0;
    }
    else {
      delete mkDir;                                                             // Free the QDir mkDir object
      retVal = -1;
    }
  }
  return(retVal);
}


bool UsbAccess::UsbPresent()
{
  /*--------------------------------------------*/
  /* Check if an USB stick is present           */
  /* Search in the /media for any sd* directory */
  /* Save the usb path namein a global string.  */
  /*--------------------------------------------*/
  QDir dir;
  QStringList filters;

  /*----- Setup path and device wildcard -----*/
  filters << "sd*";
  dir.setPath("/media");

  /*----- Setup filters to search for directories -----*/
  dir.setFilter(QDir::Dirs | QDir::Hidden | QDir::NoSymLinks);
  dir.setNameFilters(filters);
  dir.setSorting(QDir::Size | QDir::Reversed);

  /*----- Fetch directory list from /media -----*/
  QFileInfoList list = dir.entryInfoList();

  /*----- Any directory found ? -----*/
  if (list.size()) {
    /*----- Only one USB stick can be present !! -----*/
    Rout::usbPath = "/media/" + list.at(0).fileName();
    qDebug() << "USB stick found ! --> Device name = " << Rout::usbPath;
    usbPresent = true;
  }
  else {
    usbPresent = false;
  }
  return (usbPresent);
}


bool UsbAccess::UsbUnmount()
{
  /*----------------------------------*/
  /* Unmount the USB stick if present */
  /*----------------------------------*/
  QString cmd;
  QProcess *proc;
  bool retVal;

  if (UsbPresent()) {                                                           /* USB present ? */

    /*----- 'umount /media/sdXX' command -----*/
    cmd = "umount ";
    cmd += Rout::usbPath;                                                       /* Setup path name */
    proc = new QProcess(this);                                                  /* Execute command in the Linux kernel */
    proc->start(cmd);

    /*----- 'rmdir -r /media/sdXX*' command -----*/
    cmd = "rmdir -r";
    cmd += Rout::usbPath;                                                       /* Setup path name */
    proc = new QProcess(this);                                                  /* Execute command in the Linux kernel */
    proc->start(cmd);

    retVal = true;
  }
  else {
    retVal = false;
  }
  return(retVal);
}


QString UsbAccess::basePath()
{
  /*-----------------------------------*/
  /* Return the name of the USB device */
  /*-----------------------------------*/
  return Rout::usbPath;
}

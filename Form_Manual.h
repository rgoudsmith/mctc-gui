#ifndef SUPERVISORFRM_H
#define SUPERVISORFRM_H

#include "Form_QbaseWidget.h"
#include "Form_NumericInput.h"

namespace Ui
{
  class ManualFrm;
}


class ManualFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit ManualFrm(QWidget *parent = 0);
    ~ManualFrm();
    void FormInit();

  private:
    Ui::ManualFrm *ui;
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
    bool eventFilter(QObject *, QEvent *);

    NumericInputFrm * numInput;

    void SetManualMode(bool,int);
    void DisplayManualMode();
    void DisableOutputs(int);
    void SetOutput(bool,int,int);
    void ToggleOutput(int,int);
    void SetRPM(float,int);
    void SetRpmText(float,int);
    void SetButtonImage(QPushButton *,int,int);
    void SetLabelOutputImage(QLabel *,int,int);
    void SetLabelOutputStatus(QLabel *,int,int);
    void SetOutputStatus(int,int);
    void SetLabelInputImage(QLabel *,int,int);
    void SetLabelInputStatus(QLabel *,int,int);
    void SetInputStatus(int,int);
    void SetWeight(float,int);
    void SetTacho(float,int);
    void ResetManualIoMode(void);

  public slots:
    void OkClicked();
    void Output1Clicked();
    void Output2Clicked();
    void Output3Clicked();
    void Output4Clicked();
    void ManModeClicked();
    void ValueReceived(float);
    void DisplayIO(int);
    void SetupMirrorInputs();
    void ComMsgReceived(tMsgControlUnit,int);
};
#endif                                                                          // SUPERVISORFRM_H

#include "Form_Selection.h"
#include "ui_Form_Selection.h"
#include "Form_MainMenu.h"
#include "Rout.h"

#define SelLineOffset 6
#define SelLineThickness 4


int nextCnt;                                        /* Next selection counter */
int actSelection;                                   /* Active selection */


selectionFrm::selectionFrm(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::selectionFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ClearItems();

  selectionType = -1;
  unitIdx = 0;

  m_ui->setupUi(this);

  selLabel = new QLabel(this);
  selLabel->setFrameShape(QFrame::Box);
  selLabel->setLineWidth(SelLineThickness);
  selLabel->setGeometry(10,10,100,100);

  /*----- Disable windows title bar -----*/
  this->setWindowFlags(Qt::CustomizeWindowHint);

  connect(m_ui->bt11,SIGNAL(clicked()),this,SLOT(bt11_clicked()));
  connect(m_ui->bt12,SIGNAL(clicked()),this,SLOT(bt12_clicked()));
  connect(m_ui->bt13,SIGNAL(clicked()),this,SLOT(bt13_clicked()));

  connect(m_ui->bt21,SIGNAL(clicked()),this,SLOT(bt21_clicked()));
  connect(m_ui->bt22,SIGNAL(clicked()),this,SLOT(bt22_clicked()));
  connect(m_ui->bt23,SIGNAL(clicked()),this,SLOT(bt23_clicked()));

  connect(m_ui->bt31,SIGNAL(clicked()),this,SLOT(bt31_clicked()));
  connect(m_ui->bt32,SIGNAL(clicked()),this,SLOT(bt32_clicked()));
  connect(m_ui->bt33,SIGNAL(clicked()),this,SLOT(bt33_clicked()));

  connect(m_ui->btOk,SIGNAL(clicked()),this,SLOT(btOk_clicked()));
  connect(m_ui->btCancel,SIGNAL(clicked()),this,SLOT(btCancel_clicked()));
  connect(m_ui->btNext,SIGNAL(clicked()),this,SLOT(btNext_clicked()));

  SetSelectionBorder();
}


selectionFrm::~selectionFrm()
{
  delete m_ui;
}


void selectionFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
  case QEvent::LanguageChange:
    m_ui->retranslateUi(this);
    LanguageUpdate();
    break;
  default:
    break;
  }
}


void selectionFrm::paintEvent(QPaintEvent *e)
{
  QWidget::paintEvent(e);
  SetSelectionBorder();
}


void selectionFrm::showEvent(QShowEvent *e)
{
  QWidget::showEvent(e);
  glob->mstSts.popupActive = true;

  if ((glob->menu) != 0) {
    ((MainMenu*)(glob->menu))->FoldMenu();
  }

  nextCnt = 0;                                    /* Reset next counter */
}


void selectionFrm::LanguageUpdate()
{
  DisplayItems();
}


void selectionFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  setPalette(*(glob->Palette()));
  glob->SetButtonImage(m_ui->btOk,itSelection,selOk);
  glob->SetButtonImage(m_ui->btCancel,itSelection,selCancel);
  glob->SetButtonImage(m_ui->btNext,itGeneral,genArrowKeyLeft);
  LanguageUpdate();
}


void selectionFrm::btOk_clicked()
{
  /*-------------------*/
  /* Ok button clicked */
  /*-------------------*/

  /*----- Display warning message when HT motor is selected -----*/
  if ((selectionType == itMotor) && (curSelection == mtHT)) {
    ((MainMenu*)(glob->menu))->DisplayMessage(unitIdx,100,tr("HT motor selected.\nCheck if the connected motor is a HT type !"),msgtAccept);
  }

  emit selectionMade(selectionType,curSelection);
  btCancel_clicked();
}


void selectionFrm::btCancel_clicked()
{
  glob->mstSts.popupActive = false;
  this->close();
}

void selectionFrm::btNext_clicked()
/*-------------------------------*/
/* Next selection button clicked */
/*-------------------------------*/
{
  /*----- Switch to next language set -----*/
  if (actSelection == itLanguages) {
    if (++nextCnt > 1) {
      nextCnt = 0;
    }

    /*----- Rebuild itemnr for image display */
    for (int i=0;i<9;i++) {
      items[i].itemNr = i+9*nextCnt;
    }

    /*----- Show a message box -----*/
    Rout::ShowMsg(tr("Working"),tr("Please wait.... "));


    DisplayItems();

    /*----- Close the message box -----*/
    Rout::CloseMsg();

  }
}


void selectionFrm::ClearItems()
{
  for (int i=0; i<9; i++) {
    items[i].itemNr = -1;
    items[i].sortIdx = -1;
  }
}


void selectionFrm::SortItems()
{
  bool doSwitch = true;
  TSelectValue buf;

  // First move all available items to the top of the list.
  while (doSwitch) {
    doSwitch = false;

    for (int i=0;i<9;i++) {
      // ItemNr == -1 indicates an empty (or hidden) item
      // All non-empty items should be moved upwards (to the first places of the list)
      if (items[i].itemNr == -1) {
        if (i < 8) {
          if (items[(i+1)].itemNr != -1) {
            // copy next item to current item
            items[i].itemNr = items[(i+1)].itemNr;
            items[i].sortIdx = items[(i+1)].sortIdx;
            // erase next item
            items[(i+1)].itemNr = -1;
            items[(i+1)].sortIdx = -1;

            doSwitch = true;
          }
        }
      }
    }
  }
  // Shifting is done, sort all items

  doSwitch = true;

  while (doSwitch) {
    doSwitch = false;

    for (int i=0;i<8;i++) {
      if (items[i].sortIdx > items[(i+1)].sortIdx) {
        // Copy current item to buf
        buf.itemNr = items[i].itemNr;
        buf.sortIdx = items[i].sortIdx;
        // Overwrite current item with next item
        items[i].itemNr = items[(i+1)].itemNr;
        items[i].sortIdx = items[(i+1)].sortIdx;
        // Overwrite next item with buf
        items[(i+1)].itemNr = buf.itemNr;
        items[(i+1)].sortIdx = buf.sortIdx;
        // Signal that values have switched places
        doSwitch = true;
      }
    }
  }
}


void selectionFrm::DisplayItems()
{
  /*----------------------------------*/
  /* Display the selection form items */
  /*----------------------------------*/
  QPushButton * bt;

  selLabel->setVisible(false);

  for (int i=0;i<9;i++) {
    switch(i) {
    case 0: bt = m_ui->bt11;
      break;
    case 1: bt = m_ui->bt12;
      break;
    case 2: bt = m_ui->bt13;
      break;
    case 3: bt = m_ui->bt21;
      break;
    case 4: bt = m_ui->bt22;
      break;
    case 5: bt = m_ui->bt23;
      break;
    case 6: bt = m_ui->bt31;
      break;
    case 7: bt = m_ui->bt32;
      break;
    case 8: bt = m_ui->bt33;
      break;
    default: bt = 0;
      break;
    }

    if ((items[i].itemNr != -1)  && (bt!=0)) {
      glob->SetButtonImage(bt,selectionType,items[i].itemNr);
      bt->setVisible(true);
    }
    else {
      if (bt!=0) {
        bt->setVisible(false);
      }
    }
  }
  SetSelectionBorder();
}


void selectionFrm::SetCurrentMode(const eMode &mode)
{
  currentMode = mode;
}


void selectionFrm::DisplaySelection(int _selection, int _selectedIdx, int _unitIdx)
{
  /*------------------------------*/
  /* Display the selection screen */
  /*------------------------------*/

  int maxCnt;

  actSelection = _selection;                                     /* Save the active selection */

  unitIdx = _unitIdx;
  selectionType = _selection;                                                   // return value for calling form when a selection is made.
  curSelection = _selectedIdx;

  ClearItems();

  m_ui->btNext->setVisible(false);

  switch (_selection) {
  case itDosTool:                                                             // Dosing Tools: GX/GLX/HX/A30/A20/A15
    maxCnt = dtCount;
    break;

  case itMotor:                                                               // Motor: LT/HT
    maxCnt = mtCount;
    break;

  case itGranulate:                                                           // Granulate: NG/MG
    maxCnt = gtCount;
    break;

  case itFillSys:                                                             // Fill System: NONE/ME/MV/EX
    maxCnt = fsCount;
    break;

  case itType:                                                                // Type: Timer/Relay/Tacho
    maxCnt = ttCount;
    break;

  case itMode:                                                                // Mode: INJ/EXT
    maxCnt = modCount;
    break;

  case itGraviRpm:                                                            // Mode: GRAVI/RPM
    maxCnt = grCount;
    break;

  case itUserSelect:
    maxCnt = usCount;
    break;

  case itActive :                                                             // Yes/No selection
    maxCnt = actCount;
    break;

  case itLanguages:                                                           // Languages: UK/NL
    //      maxCnt = lanCount;
    maxCnt = 9;                                               /* 9 languages per selection */
    m_ui->btNext->setVisible(true);                           /* Enable next selection button */
    break;

  case itKnifeGate:                                                           // Knifegate On/Off
    maxCnt = knifeGateCount;
    break;

  case itMcwType:                                                             // MccWeight type
    maxCnt = mcwCount;
    break;

  case itBalHOType:                                                             // MccWeight type
    maxCnt = balHOCount;
    break;


  case itDeviceType:                                                          // Unit type
    maxCnt = mcdtCount-1;                                                     // Skip mcdtNone
    break;

  default:                                                                    // default
    maxCnt = 0;
    break;
  }

  for (int i=0;i<maxCnt;i++) {

    switch (_selection) {

    case itLanguages :
      items[i].itemNr = i+9*nextCnt;                                 /* Add nextCnt to image number */
      break;

    case itDeviceType:
      items[i].itemNr = i+1;                                         /* Skip mcdtNode image ( = 0 ) */
      break;

    default:
      items[i].itemNr = i;
      break;
    }
  }
  CheckDependancies();
  SortItems();
  DisplayItems();
}


void selectionFrm::SetSelectionBorder()
{
  QPushButton * bt;
  int selItem = -1;

  for (int i=0;i<9;i++) {
    if (items[i].itemNr == curSelection) {
      selItem = i;
    }
  }
  switch(selItem) {
  case 0: bt = m_ui->bt11;
    break;
  case 1: bt = m_ui->bt12;
    break;
  case 2: bt = m_ui->bt13;
    break;
  case 3: bt = m_ui->bt21;
    break;
  case 4: bt = m_ui->bt22;
    break;
  case 5: bt = m_ui->bt23;
    break;
  case 6: bt = m_ui->bt31;
    break;
  case 7: bt = m_ui->bt32;
    break;
  case 8: bt = m_ui->bt33;
    break;
  default: bt = 0;
    break;
  }

  if (bt != 0) {
    selLabel->setVisible(true);
    selLabel->move((bt->pos().x()-SelLineOffset),(bt->pos().y()-SelLineOffset));
    selLabel->setFixedSize((bt->width()+(SelLineOffset*2)),(bt->height()+(SelLineOffset*2)));
  }
}


void selectionFrm::HideItem(int item)
{
  for (int i=0;i<9;i++) {
    if (items[i].itemNr == item) {
      items[i].sortIdx = -1;
      items[i].itemNr = -1;
      break;
    }
  }
}


void selectionFrm::CheckDependancies()
{
  /*----------------------------------------------------------------------*/
  /* Enable/disable some selection buttons depending on the configuration */
  /*----------------------------------------------------------------------*/
  switch(selectionType) {

  /*----- Dosing tool -----*/
  case itDosTool:
    if (glob->sysCfg[glob->ActUnitIndex()].motor == mtLT) {
      HideItem(dtA30);
    }

    if (glob->sysCfg[glob->ActUnitIndex()].motor == mtHT) {
      HideItem(dtGX);
      HideItem(dtGLX);
      HideItem(dtHX);
      HideItem(dtA15);
    }
    break;

    /*----- Input mode -----*/
  case itType:
    if (currentMode == modExtrude) {
      HideItem(ttTimer);
    }

    if (currentMode == modInject) {
      HideItem(ttTacho);
    }
    break;

    /*----- Language -----*/
  case itLanguages:
    //        HideItem(lanNL);
    break;
  }
}


void selectionFrm::intSelectionMade(int _selIdx)
{
  curSelection = items[_selIdx].itemNr;
  SetSelectionBorder();
}


void selectionFrm::SetFormCaption(const QString &text)
{
  this->setWindowTitle(text);
}

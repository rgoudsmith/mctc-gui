/****************************************************************************
** Meta object code from reading C++ file 'Form_Selection.h'
**
** Created: Wed Feb 6 11:15:27 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_Selection.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_Selection.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_selectionFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   14,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      39,   13,   13,   13, 0x08,
      54,   13,   13,   13, 0x08,
      69,   13,   13,   13, 0x08,
      84,   13,   13,   13, 0x08,
      99,   13,   13,   13, 0x08,
     114,   13,   13,   13, 0x08,
     129,   13,   13,   13, 0x08,
     144,   13,   13,   13, 0x08,
     159,   13,   13,   13, 0x08,
     174,   13,   13,   13, 0x08,
     189,   13,   13,   13, 0x08,
     208,   13,   13,   13, 0x08,
     236,  225,   13,   13, 0x0a,
     266,   14,   13,   13, 0x2a,

       0        // eod
};

static const char qt_meta_stringdata_selectionFrm[] = {
    "selectionFrm\0\0,\0selectionMade(int,int)\0"
    "bt11_clicked()\0bt12_clicked()\0"
    "bt13_clicked()\0bt21_clicked()\0"
    "bt22_clicked()\0bt23_clicked()\0"
    "bt31_clicked()\0bt32_clicked()\0"
    "bt33_clicked()\0btOk_clicked()\0"
    "btCancel_clicked()\0btNext_clicked()\0"
    ",,_unitIdx\0DisplaySelection(int,int,int)\0"
    "DisplaySelection(int,int)\0"
};

const QMetaObject selectionFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_selectionFrm,
      qt_meta_data_selectionFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &selectionFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *selectionFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *selectionFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_selectionFrm))
        return static_cast<void*>(const_cast< selectionFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int selectionFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: selectionMade((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: bt11_clicked(); break;
        case 2: bt12_clicked(); break;
        case 3: bt13_clicked(); break;
        case 4: bt21_clicked(); break;
        case 5: bt22_clicked(); break;
        case 6: bt23_clicked(); break;
        case 7: bt31_clicked(); break;
        case 8: bt32_clicked(); break;
        case 9: bt33_clicked(); break;
        case 10: btOk_clicked(); break;
        case 11: btCancel_clicked(); break;
        case 12: btNext_clicked(); break;
        case 13: DisplaySelection((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 14: DisplaySelection((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 15;
    }
    return _id;
}

// SIGNAL 0
void selectionFrm::selectionMade(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

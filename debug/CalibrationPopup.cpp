#include "CalibrationPopup.h"
#include "ui_CalibrationPopup.h"
#include "Form_MainMenu.h"
#include "Rout.h"

CalibrationPopup::CalibrationPopup(QWidget *parent) : QBaseWidget(parent), ui(new Ui::CalibrationPopup)
{
  ui->setupUi(this);
  SetPrime = false;

  /*----- Disable windows title bar -----*/
  this->setWindowFlags(Qt::CustomizeWindowHint);

  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));

  // Page 2 buttons (for now, all just close the window)
  connect(ui->btAbort,SIGNAL(clicked()),this,SLOT(AbortClicked()));
  connect(ui->btSave,SIGNAL(clicked()),this,SLOT(SaveClicked()));
  connect(ui->btContinue,SIGNAL(clicked()),this,SLOT(ContinueClicked()));
  setFixedWidth(500);
  setFixedHeight(350);
}


CalibrationPopup::~CalibrationPopup()
{
  delete ui;
}


void CalibrationPopup::changeEvent(QEvent *e)
{
  QBaseWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      LanguageUpdate();
      break;
    default:
      break;
  }
}


void CalibrationPopup::showEvent(QShowEvent *e)
{
  QBaseWidget::showEvent(e);
  ui->lbMessage->setText(tr("Check if the hopper is filled")+"\n"+tr("and the motor is connected"));
  ui->lbPrime->setText(tr("Prime unit?"));

  if ((glob->menu) != 0) {
    ((MainMenu*)(glob->menu))->FoldMenu();
  }
  glob->mstSts.popupActive = true;
}


void CalibrationPopup::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  LanguageUpdate();
  connect(glob,SIGNAL(SysSts_PrimeStatusChanged(bool,int)),this,SLOT(PrimeActiveChanged(bool,int)));
}


void CalibrationPopup::LanguageUpdate()
{
  glob->SetButtonImage(ui->btOk,itSelection,selOk);
  glob->SetButtonImage(ui->btCancel,itSelection,selCancel);
  glob->SetButtonImage(ui->btSave,itFileHandle,fhSave);
  glob->SetButtonImage(ui->btAbort,itSelection,selCancel);
  glob->SetButtonImage(ui->btContinue,itGeneral,genArrowKeyRight);

  ui->lbTitle->setFont(*(glob->baseFont));
  ui->lbMessage->setFont(*(glob->baseFont));
  ui->lbPrime->setFont(*(glob->baseFont));

  ui->lbTitle_2->setFont(*(glob->baseFont));
}


void CalibrationPopup::SetActivePage(int pageIdx)
{
  if (pageIdx < ui->pageControl->count()) {
    ui->pageControl->setCurrentIndex(pageIdx);
    ui->btOk->setVisible(true);
    ui->btCancel->setVisible(true);
  }
}


void CalibrationPopup::PrimeActiveChanged(bool sts, int idx)
{
  if (SetPrime) {
    if (idx == unitIdx) {
      if (sts == false) {
        SetPrime = false;
        // We need to send a (-1) signal, which is equal to CancelClicked so we abuse this function.
        CancelClicked();
      }
    }
  }
}


void CalibrationPopup::OkClicked()
{
  ui->lbMessage->setText(tr("Prime busy (15sec - 50rpm)")+"\n"+tr("Please wait..."));

  // Store original prime values
  unitIdx = glob->ActUnitIndex();
  origRpm = glob->sysCfg[unitIdx].prime.rpm;
  origTime = glob->sysCfg[unitIdx].prime.time;

  // Write pre-cal prime values (defines in ImageDef.h)
  glob->SysCfg_SetPrimeRpm(PRECAL_PRIME_RPM,unitIdx);
  glob->SysCfg_SetPrimeTime(PRECAL_PRIME_TIME,unitIdx);

  // hide ok / cancel buttons
  ui->btOk->setVisible(false);
  // ui->btCancel->setVisible(false);
  ui->lbPrime->setText("");

  // Execute prime
  SetPrime = true;                                                              // Signal that this form initiated the prime command to prevent illegal screen switches when normal prime is running.
  glob->sysSts[unitIdx].prime.maxVal = (glob->sysCfg[unitIdx].prime.time * 1000);
  glob->sysSts[unitIdx].prime.timVal = 0;
  glob->SysSts_SetPrimeActive(true,unitIdx,true);
}


void CalibrationPopup::CancelClicked()
{
  if (SetPrime) {
    SetPrime = false;
    glob->SysSts_SetPrimeActive(false,unitIdx,true);
  }
  glob->mstSts.popupActive = false;
  emit SelectionMade(-1);
  close();
}


void CalibrationPopup::SaveClicked()
{
  glob->mstSts.popupActive = false;
  emit SelectionMade(0);
  close();
}


void CalibrationPopup::AbortClicked()
{
  glob->mstSts.popupActive = false;
  emit SelectionMade(1);
  close();
}


void CalibrationPopup::ContinueClicked()
{
  glob->mstSts.popupActive = false;
  emit SelectionMade(2);
  close();
}

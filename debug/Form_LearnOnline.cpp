#include "Form_LearnOnline.h"
#include "ui_Form_LearnOnline.h"
#include "Form_MainMenu.h"
#include "Rout.h"

LearnOnlineFrm::LearnOnlineFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::LearnOnlineFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);

  keyInput = new KeyboardFrm();

  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkButtonClicked()));
}


LearnOnlineFrm::~LearnOnlineFrm()
{
  delete keyInput;
  delete ui;
}


void LearnOnlineFrm::changeEvent(QEvent *e)
{
  if (e->type() == QEvent::LanguageChange) {
    ui->retranslateUi(this);
  }
}


void LearnOnlineFrm::DisplayUnitData(void)
{
  /*--------------------------------------*/
  /* Display the on-line calibration data */
  /*--------------------------------------*/

  /*----- Do not display second learn online data when in single unit mode,
          dual unit mode and McWeight enabled,
          or multi unit mode -----*/
  if ((glob->mstCfg.slaveCount == 0)  || ((glob->mstCfg.slaveCount == 1) && (Rout::McWeightPresent()))
      || (glob->mstCfg.slaveCount > 1)) {
    ui->learnData2->setVisible(false);
  }
  else {
    ui->learnData2->setVisible(true);
  }
}


void LearnOnlineFrm::showEvent(QShowEvent *e)
{
  /*---------------------------------*/
  /* Show window event handler       */
  /* The learn online form is active */
  /*---------------------------------*/

  /*----- Connect unit select signal -----*/
  connect(((MainMenu*)(glob->menu)),SIGNAL(UnitSelected()),this,SLOT(NewUnitSelected()));

  /*----- Display unit data -----*/
  DisplayUnitData();

  QBaseWidget::showEvent(e);
}


void LearnOnlineFrm::hideEvent(QHideEvent *e)
{
  /*------------------------------------*/
  /* Hide window event handler          */
  /* The learn online form is in-active */
  /*------------------------------------*/

  /*----- Disconnect unit select signal -----*/
  disconnect(((MainMenu*)(glob->menu)),SIGNAL(UnitSelected()),this,SLOT(NewUnitSelected()));

  QWidget::hideEvent(e);
}


void LearnOnlineFrm::NewUnitSelected(void)
{
  /*--------------------------------------*/
  /* New unit selected, refresh unit data */
  /*--------------------------------------*/
  DisplayUnitData();
}


void LearnOnlineFrm::FormInit()
{
  int unitIndex;

  QBaseWidget::ConnectKeyBeep();;

  ui->lbTitle->setFont(*(glob->captionFont));

  /*----- Check if McWeight is enabled, ifso it is always unit 0, and the other units are 1 and 2 -----*/
  unitIndex = 0;                                                                /* Unit index start = 0 */
//  if (glob->mstCfg.mcWeightEnb) {                                               /* McWeight enabled ? */
//    unitIndex = 1;                                                              /* Unit index start = 1 */
//  }

  ui->learnData1->FormInit();
  ui->learnData1->SetUnitIndex(unitIndex);
  ui->learnData1->SetKeyboard(keyInput);

  ui->learnData2->FormInit();
  ui->learnData2->SetUnitIndex(unitIndex+1);
  ui->learnData2->SetKeyboard(keyInput);

  glob->SetButtonImage(ui->btOk,itSelection,selCancel);
}


void LearnOnlineFrm::OkButtonClicked()
{
  /*-------------------*/
  /* OK button clicked */
  /*-------------------*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
}

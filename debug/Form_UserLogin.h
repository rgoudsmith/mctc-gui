#ifndef USERLOGINFRM_H
#define USERLOGINFRM_H

#include <QtGui/QWidget>
#include "Form_QbaseWidget.h"
#include "Form_NumKeyboard.h"

namespace Ui
{
  class UserLoginFrm;
}


class UserLoginFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    UserLoginFrm(QWidget *parent = 0);
    ~UserLoginFrm();
    void FormInit();

  protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *);
    void showEvent(QShowEvent *);
    void SetButtonsByLevel(eUserType);

  private:
    Ui::UserLoginFrm *m_ui;
    NumKeyboardFrm * keyboard;

  private slots:
    void LoginReceived(const QString &);
    void LoginClicked();
    void okClicked();

  public slots:
    void UserChanged(eUserType);
};
#endif                                                                          // USERLOGINFRM_H

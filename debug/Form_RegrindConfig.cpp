#include "Form_RegrindConfig.h"
#include "ui_Form_RegrindConfig.h"
#include "Form_MainMenu.h"
#include "Glob.h"
#include "Rout.h"

/*----- Local variables -----*/
int lineIdx;                                                                    /* Actual edit line index */

RegrindConfigFrm::RegrindConfigFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::RegrindConfigFrm)
{
  QBaseWidget::ConnectKeyBeep();

  /*----- Create numeric entry -----*/
  numInput = new NumericInputFrm();
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
  numInput->hide();

  numInput->FormInit();

  /*----- Setup fonts -----*/
  ui->setupUi(this);

  ui->lbLevels->setFont(*(glob->captionFont));

  ui->lbLevel0->setFont(*(glob->baseFont));
  ui->edLevel0->setFont(*(glob->baseFont));

  ui->lbLevel1->setFont(*(glob->baseFont));
  ui->edLevel1->setFont(*(glob->baseFont));

  ui->lbLevel2->setFont(*(glob->baseFont));
  ui->edLevel2->setFont(*(glob->baseFont));

  ui->lbLevel3->setFont(*(glob->baseFont));
  ui->edLevel3->setFont(*(glob->baseFont));

  ui->lbLevel4->setFont(*(glob->baseFont));
  ui->edLevel4->setFont(*(glob->baseFont));

  ui->lbSettings->setFont(*(glob->captionFont));

  ui->lbFillIntTime->setFont(*(glob->baseFont));
  ui->edFillIntTime->setFont(*(glob->baseFont));

  ui->lbFillTime->setFont(*(glob->baseFont));
  ui->edFillTime->setFont(*(glob->baseFont));

  ui->lbRegLevel->setFont(*(glob->baseFont));
  ui->edRegLevel->setFont(*(glob->baseFont));

  ui->lbRegPctB1->setFont(*(glob->baseFont));
  ui->edRegPctB1->setFont(*(glob->baseFont));

  ui->lbRegLevelHigh->setFont(*(glob->baseFont));
  ui->edRegHighLevel->setFont(*(glob->baseFont));

  ui->lbRegPctExtra->setFont(*(glob->baseFont));
  ui->edRegPctExtra->setFont(*(glob->baseFont));

  /* Link event filter to the numeric date entry fields */
  ui->edLevel0->installEventFilter(this);
  ui->edLevel1->installEventFilter(this);
  ui->edLevel2->installEventFilter(this);
  ui->edLevel3->installEventFilter(this);
  ui->edLevel4->installEventFilter(this);
  ui->edFillIntTime->installEventFilter(this);
  ui->edFillTime->installEventFilter(this);
  ui->edRegHighLevel->installEventFilter(this);
  ui->edRegPctB1->installEventFilter(this);
  ui->edRegLevel->installEventFilter(this);
  ui->edRegPctExtra->installEventFilter(this);


}


RegrindConfigFrm::~RegrindConfigFrm()
{
  delete numInput;
  delete ui;
}


void RegrindConfigFrm::resizeEvent(QResizeEvent *e)
{
  QWidget::resizeEvent(e);
  numInput->move(((width()/2)-(numInput->width()/2)),(((height()/2)-(numInput->height()/2))+35));
}


void RegrindConfigFrm::DisplayData(void)
{
  /*--------------------------*/
  /* Display the regrind data */
  /*--------------------------*/
  int actUnit = glob->ActUnitIndex();

  ui->edLevel0->setText(QString::number(glob->sysCfg[actUnit].regrindParm.levelPct[0],'f',0) + " %");
  ui->edLevel1->setText(QString::number(glob->sysCfg[actUnit].regrindParm.levelPct[1],'f',0) + " %");
  ui->edLevel2->setText(QString::number(glob->sysCfg[actUnit].regrindParm.levelPct[2],'f',0) + " %");
  ui->edLevel3->setText(QString::number(glob->sysCfg[actUnit].regrindParm.levelPct[3],'f',0) + " %");
  ui->edLevel4->setText(QString::number(glob->sysCfg[actUnit].regrindParm.levelPct[4],'f',0) + " %");
  ui->edRegHighLevel->setText(QString::number(glob->sysCfg[actUnit].regrindParm.levelHigh,'f',0) + " " + glob->weightUnits);
  ui->edRegLevel->setText(QString::number(glob->sysCfg[actUnit].regrindParm.levelRegrind,'f',0) + " " + glob->weightUnits);
  ui->edFillTime->setText(QString::number(glob->sysCfg[actUnit].regrindParm.fillTimeMax,'f',0) + " s");
  ui->edFillIntTime->setText(QString::number(glob->sysCfg[actUnit].regrindParm.fillInterval,'f',0) + " s");
  ui->edRegPctExtra->setText(QString::number(glob->sysCfg[actUnit].regrindParm.regrindPctExtra,'f',3) + " %");
  ui->edRegPctB1->setText(QString::number(glob->sysCfg[actUnit].regrindParm.regPctB1,'f',3) + " %");
}


void RegrindConfigFrm::showEvent(QShowEvent *e)
{
  /*----------------------------------------------------------*/
  /* Show event : The regrind config screen has become active */
  /*----------------------------------------------------------*/
  QBaseWidget::showEvent(e);



  /*----- Refresh the data fields -----*/
  DisplayData();


  /*--- Display regrind levels depending userlevel ---*/
  if (glob->GetUser() >= utAgent) {
      ui->gbRegLevelSettings->setVisible(true);
      ui->lbRegPctB1->setVisible(true);
      ui->edRegPctB1->setVisible(true);
  }
  else {
      ui->gbRegLevelSettings->setVisible(false);
      ui->lbRegPctB1->setVisible(false);
      ui->edRegPctB1->setVisible(false);
  }
}


bool RegrindConfigFrm::eventFilter(QObject *o, QEvent *e)
{
  /*--------------*/
  /* Event filter */
  /*--------------*/
  float val,minVal,maxVal;

  int actUnit = glob->ActUnitIndex();
  bool num = false;

  val = 1;
  minVal=0;
  maxVal=9;

  if (e->type() == QEvent::MouseButtonPress) {

    /*----- Level entry -----*/
    if (o == ui->edLevel0) {
      val = glob->sysCfg[actUnit].regrindParm.levelPct[0];
      minVal = 0;
      maxVal = 100;
      lineIdx = 0;
      num = true;
    }
    else if (o == ui->edLevel1) {
      val = glob->sysCfg[actUnit].regrindParm.levelPct[1];
      minVal = 1;
      maxVal = 100;
      lineIdx = 1;
      num = true;
    }
    else if (o == ui->edLevel2) {
      val = glob->sysCfg[actUnit].regrindParm.levelPct[2];
      minVal = 2;
      maxVal = 100;
      lineIdx = 2;
      num = true;
    }
    else if (o == ui->edLevel3) {
      val = glob->sysCfg[actUnit].regrindParm.levelPct[3];
      minVal = 3;
      maxVal = 100;
      lineIdx = 3;
      num = true;
    }
    else if (o == ui->edLevel4) {
      val = glob->sysCfg[actUnit].regrindParm.levelPct[4];
      minVal = 4;
      maxVal = 100;
      lineIdx = 4;
      num = true;
    }

    else if (o == ui->edRegHighLevel) {
      val = glob->sysCfg[actUnit].regrindParm.levelHigh;
      minVal = 1;
      maxVal = 99999;
      lineIdx = 5;
      num = true;
    }

    else if (o == ui->edRegLevel) {
      val = glob->sysCfg[actUnit].regrindParm.levelRegrind;
      minVal = 1;
      maxVal = 99999;
      lineIdx = 6;
      num = true;
    }

    else if (o == ui->edFillTime) {
      val = glob->sysCfg[actUnit].regrindParm.fillTimeMax;
      minVal = 0;
      maxVal = 9999;
      lineIdx = 7;
      num = true;
    }

    else if (o == ui->edFillIntTime) {
      val = glob->sysCfg[actUnit].regrindParm.fillInterval;
      minVal = 0;
      maxVal = 9999;
      lineIdx = 8;
      num = true;
    }

    else if (o == ui->edRegPctExtra) {
      val = glob->sysCfg[actUnit].regrindParm.regrindPctExtra;
      minVal = 0;
      maxVal = 100;
      lineIdx = 9;
      num = true;
    }

    else if (o == ui->edRegPctB1) {
      val = glob->sysCfg[actUnit].regrindParm.regPctB1;
      minVal = 0;
      maxVal = 100;
      lineIdx = 10;
      num = true;
    }

    /*--- Show numeric keyboard ---*/
    if (num) {
      numInput->SetDisplay(true,1,val,minVal,maxVal);
      KeyBeep();
      numInput->show();
    }
  }
  return QBaseWidget::eventFilter(o,e);
}


void RegrindConfigFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;
  if (numInput != 0) {
    numInput->FormInit();
  }
  connect(numInput,SIGNAL(InputValue(float)),this,SLOT(ValueReturned(float)));
}


void RegrindConfigFrm::ValueReturned(float value)
{
  /*-----------------------------------*/
  /* Numeric data entry value returned */
  /*-----------------------------------*/
  int actUnit = glob->ActUnitIndex();

  switch(lineIdx) {
    case 0:
      glob->sysCfg[actUnit].regrindParm.levelPct[0] = value;
      ui->edLevel0->setText(QString::number(value) + " %");
      break;

    case 1:
      glob->sysCfg[actUnit].regrindParm.levelPct[1] = value;
      ui->edLevel1->setText(QString::number(value) + " %");
      break;

    case 2:
      glob->sysCfg[actUnit].regrindParm.levelPct[2] = value;
      ui->edLevel2->setText(QString::number(value) + " %");
      break;

    case 3:
      glob->sysCfg[actUnit].regrindParm.levelPct[3] = value;
      ui->edLevel3->setText(QString::number(value) + " %");
      break;

    case 4:
      glob->sysCfg[actUnit].regrindParm.levelPct[4] = value;
      ui->edLevel4->setText(QString::number(value) + " %");
      break;

    case 5:
      glob->sysCfg[actUnit].regrindParm.levelHigh = value;
      ui->edRegHighLevel->setText(QString::number(value) + " " + glob->weightUnits);
      break;

    case 6:
      glob->sysCfg[actUnit].regrindParm.levelRegrind = value;
      ui->edRegLevel->setText(QString::number(value) + " " + glob->weightUnits);
      break;

    case 7:
      glob->sysCfg[actUnit].regrindParm.fillTimeMax = value;
      ui->edFillTime->setText(QString::number(value) + " s");
      break;

    case 8:
      glob->sysCfg[actUnit].regrindParm.fillInterval = value;
      ui->edFillIntTime->setText(QString::number(value) + " s");
      break;

    case 9:
      glob->sysCfg[actUnit].regrindParm.regrindPctExtra = value;
      ui->edRegPctExtra->setText(QString::number(value) + " %");
      break;

    case 10:
      glob->sysCfg[actUnit].regrindParm.regPctB1 = value;
      ui->edRegPctB1->setText(QString::number(value) + " %");
      break;
  }

  /*----- Save the changed system config to file -----*/
  glob->SignalSaveConfigToFile(actUnit);
}

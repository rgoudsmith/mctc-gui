/********************************************************************************
** Form generated from reading UI file 'Form_ProcessLog.ui'
**
** Created: Thu Apr 12 12:24:39 2012
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_PROCESSLOG_H
#define UI_FORM_PROCESSLOG_H

#include <ProcessLogWidget.h>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ProcessLogFrm
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *lbTitle;
    QSpacerItem *horizontalSpacer_2;
    ProcessLogWidget *widget;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btDel;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btOk;

    void setupUi(QWidget *ProcessLogFrm)
    {
        if (ProcessLogFrm->objectName().isEmpty())
            ProcessLogFrm->setObjectName(QString::fromUtf8("ProcessLogFrm"));
        ProcessLogFrm->resize(842, 530);
        ProcessLogFrm->setCursor(QCursor(Qt::BlankCursor));
        ProcessLogFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout = new QVBoxLayout(ProcessLogFrm);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        lbTitle = new QLabel(ProcessLogFrm);
        lbTitle->setObjectName(QString::fromUtf8("lbTitle"));

        horizontalLayout->addWidget(lbTitle);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        widget = new ProcessLogWidget(ProcessLogFrm);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(widget);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        btDel = new QPushButton(ProcessLogFrm);
        btDel->setObjectName(QString::fromUtf8("btDel"));
        btDel->setMinimumSize(QSize(90, 70));
        btDel->setMaximumSize(QSize(90, 70));

        horizontalLayout_2->addWidget(btDel);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        btOk = new QPushButton(ProcessLogFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("ok"));

        horizontalLayout_2->addWidget(btOk);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(ProcessLogFrm);

        QMetaObject::connectSlotsByName(ProcessLogFrm);
    } // setupUi

    void retranslateUi(QWidget *ProcessLogFrm)
    {
        lbTitle->setText(QApplication::translate("ProcessLogFrm", "Process Log", 0, QApplication::UnicodeUTF8));
        btDel->setText(QApplication::translate("ProcessLogFrm", "Del", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(ProcessLogFrm);
    } // retranslateUi

};

namespace Ui {
    class ProcessLogFrm: public Ui_ProcessLogFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_PROCESSLOG_H

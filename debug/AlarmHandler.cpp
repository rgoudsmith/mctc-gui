#include "AlarmHandler.h"
#include "Form_MainMenu.h"
#include "ImageDef.h"
#include "Rout.h"

AlarmHandler::AlarmHandler(QObject *parent) :QObject(parent)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  Rout::ReadAlarmLogFile();                                                     /* Read alarm log file into ram */
}


void AlarmHandler::FormInit()
{
  connect(glob,SIGNAL(SysSts_AlarmModeChanged(int,int)),this,SLOT(AlarmModeChanged(int,int)));
  connect(glob,SIGNAL(SysSts_AlarmStatusChanged(bool,int)),this,SLOT(AlarmActiveChanged(bool,int)));
  connect(glob,SIGNAL(AlarmResetAckRcv(int)),this,SLOT(AlarmResetAck(int)));
  connect(glob,SIGNAL(AlarmReset(int)),this,SLOT(AlarmReset(int)));
}


void AlarmHandler::ActiveAlarmHandler(void)
{
  /*----------------------*/
  /* Active alarm handler */
  /*----------------------*/
  Rout::LcdBrightNess(100);                                                     /* Backlit to 100% */
}


void AlarmHandler::AlarmModeChanged(int, int)
{
}


void AlarmHandler::GetAlarmText(int msgNr, float value, QString * txt, bool fullTxt)
{
  QString tmp;
  switch(msgNr) {
  case ALM_INDEX_FILL_SYSTEM:                                                 // 1 Fill system not filling
    *txt = tr("Filling system unable to load material!");
    break;
  case ALM_INDEX_LO_LEVEL:                                                    // 2 Low message
    tmp = tr("Low level!");
    *txt = tmp;
    break;
  case ALM_INDEX_LO_LO_LEVEL:                                                 // 3 Low low message
    tmp = tr("Empty level!");
    *txt = tmp;
    break;
  case ALM_INDEX_HI_HI_LEVEL:                                                 // 4 Hi Hi message
    tmp = tr("High high level!");
    if (fullTxt) {
      tmp = tmp+"\n"+tr("Level is higher than") + " " +QString::number(value)+glob->weightUnits;
    }
    *txt = tmp;
    break;

  case ALM_INDEX_MIN_MOTOR_SPEED:                                             // 5 Minimum motor speed
    *txt = tr("Minimum motor speed! (<0.1 RPM)");
    break;
  case ALM_INDEX_MAX_MOTOR_SPEED:                                             // 6 Max RPM
    tmp = tr("Maximum RPM exceeded");
    if (fullTxt) {
      tmp = tmp+tr(", change dosing tool")+"\n"+tr("for higher capacity.");
    }
    *txt = tmp;
    break;
  case ALM_INDEX_DEV_QR:                                                      // 7 Maximum deviation (QR)
    *txt = tr("Maximum deviation exceeded!");
    break;
  case ALM_INDEX_DEV:                                                         // 8 Maximum deviation
    tmp = tr("Maximum deviation exceeded!");
    *txt = tmp;
    break;
  case ALM_INDEX_MASTER_SLAVE_COM:                                            // 9  Master/Slave connection failure
    *txt = tr("Master/Slave connection failure!");
    break;
  case ALM_INDEX_VALVE_NOT_CLOSED:                                            // 50 Valve not closed
    *txt = tr("Slide not closed!");                                           // text Slide not closed
    break;
  case ALM_INDEX_MOTOR_CON_4WIRE:                                             // 100 Motor connect failure 4-wire
    *txt = tr("Motor connection failure!");                                   // 4-wire
    break;
  case ALM_INDEX_MOTOR_CON_6WIRE:                                             // 101 Motor connect failure 6-wire
    *txt = tr("Motor connection failure!");                                   // 6-wire
    break;
  case ALM_INDEX_LOADCELL_CON:                                                // 102
    *txt = tr("Loadcell connection failure!");
    break;
  default:
    break;
  }
}


void AlarmHandler::GetMessageText(int msgNr, float newValue,float oldValue, QString * msg)
{
  QString txt1 = tr("changed from");
  QString txt2 = tr("to");
  QString txt;
  QString subTxt;
  int val,i;

  switch (msgNr) {

  case TXT_INDEX_LEARN_BUSY:                                                  // Learn Busy
    *msg = tr("LEARN");
    break;

  case TXT_INDEX_LEARN_READY:                                                 // Learn Ready
    *msg = tr("OK");
    break;

    // Production config changes
  case TXT_INDEX_COLOR_PCT_CHANGED:
    txt = tr("Color pct") + " " + txt1 +" '";
    subTxt = QString::number(oldValue,glob->formats.PercentageFmt.format.toAscii(),glob->formats.PercentageFmt.precision);
    txt = txt + subTxt + "' "+txt2+" '";
    subTxt = QString::number(newValue,glob->formats.PercentageFmt.format.toAscii(),glob->formats.PercentageFmt.precision);
    txt = txt + subTxt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_SHOTWEIGHT_CHANGED:
    txt = tr("Shotweight") +" " + txt1 +" '";
    subTxt = QString::number(oldValue,glob->formats.ShotWeightFmt.format.toAscii(),glob->formats.ShotWeightFmt.precision);
    txt = txt + subTxt + "' "+ txt2 +" '";
    subTxt = QString::number(newValue,glob->formats.ShotWeightFmt.format.toAscii(),glob->formats.ShotWeightFmt.precision);
    txt = txt + subTxt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_DOSTIME_CHANGED:
    txt = tr("Shottime") + " " + txt1 +" '";
    subTxt = QString::number(oldValue,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision);
    txt = txt + subTxt + "' "+ txt2 +" '";
    subTxt = QString::number(newValue,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision);
    txt = txt + subTxt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_RPM_CHANGED:
    txt = tr("Rpm") + " " + txt1 +" '";
    subTxt = QString::number(oldValue,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision);
    txt = txt + subTxt + "' "+txt2+" '";
    subTxt = QString::number(newValue,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision);
    txt = txt + subTxt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_EXTCAP_CHANGED:
    txt = tr("Extruder capacity")+ " " + txt1 +" '";
    subTxt = QString::number(oldValue,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision);
    txt = txt = subTxt + "' "+ txt2 +" '";
    subTxt = QString::number(newValue,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision);
    txt = txt + subTxt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_TACHOMAX_CHANGED:
    txt = tr("Max tacho") + " " + txt1 + " '";
    subTxt = QString::number(oldValue,glob->formats.TachoFmt.format.toAscii(),glob->formats.TachoFmt.precision);
    txt = txt + subTxt + "' "+ txt2 +" '";
    subTxt = QString::number(newValue,glob->formats.TachoFmt.format.toAscii(),glob->formats.TachoFmt.precision);
    txt = txt + subTxt + "'";
    *msg = txt;
    break;

    // System config changes
  case TXT_INDEX_USER_CHANGED:
    txt = tr("User") + " " + txt1 + " '";

    for (i=0; i<2; i++) {
      switch(i) {
      case 0:
        val = oldValue;
        break;
      case 1:
        val = newValue;
        break;
      }

      GetEnumeratedText(etUser,val,&subTxt);

      txt = txt + subTxt;
      if (i == 0) {
        txt = txt + "' "+ txt2 +" '";
      }
    }
    txt = txt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_FILLSYS_CHANGED:
    txt = tr("Fillsystem") + " " + txt1 + " '";

    for (i=0; i<2; i++) {
      switch(i) {
      case 0:
        val = oldValue;
        break;
      case 1:
        val = newValue;
        break;
      }
      GetEnumeratedText(etFillSystem,val,&subTxt);
      txt = txt + subTxt;
      if (i==0) {
        txt = txt + "' "+ txt2 +" '";
      }
    }
    txt = txt +"'";
    *msg = txt;
    break;

  case TXT_INDEX_MOTOR_CHANGED:
    txt = tr("Motor type") + " " + txt1 + " '";

    for (i=0; i<2; i++) {
      switch(i) {
      case 0:
        val = oldValue;
        break;
      case 1:
        val = newValue;
        break;
      }
      GetEnumeratedText(etMotor,val,&subTxt);
      txt = txt + subTxt;
      if (i == 0) {
        txt = txt + "' "+ txt2 +" '";
      }
    }
    txt = txt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_DOSTOOL_CHANGED:
    txt = tr("Dosing tool") + " " + txt1 + " '";

    for (i=0; i<2; i++) {
      switch (i) {
      case 0:
        val = oldValue;
        break;
      case 1:
        val = newValue;
        break;
      }
      GetEnumeratedText(etDosTool,val,&subTxt);
      txt = txt + subTxt;
      if (i==0) {
        txt = txt + "' " + txt2 + " '";
      }
    }
    txt = txt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_GRANULATE_CHANGED:
    txt = tr("Granulate type") + " " + txt1 + " '";

    for(i=0; i<2; i++) {
      switch (i) {
      case 0:
        val = oldValue;
        break;
      case 1:
        val = newValue;
        break;
      }

      GetEnumeratedText(etGranulate,val,&subTxt);
      txt = txt + subTxt;
      if (i == 0) {
        txt = txt + "' " + txt2 +" '";
      }
    }
    txt = txt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_GRAVIRPM_CHANGED:
    txt = tr("GraviRpm") + " " + txt1 +" '";
    for (i=0; i<2; i++) {
      switch(i) {
      case 0:
        val = oldValue;
        break;
      case 1:
        val = newValue;
        break;
      }
      GetEnumeratedText(etGraviRpm,val,&subTxt);
      txt = txt + subTxt;
      if (i==0) {
        txt = txt + "' "+ txt2 +" '";
      }
    }
    txt = txt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_SYSTEM_START:                                                // 26
    if (newValue == 0) {
      txt = tr("System stopped");
    }
    else {
      txt = tr("System started");
    }
    *msg = txt;
    break;

  case TXT_INDEX_HOPPER_LOW_CHANGED:
    txt = tr("Low hopper level") + " " + txt1 + " '";
    subTxt = QString::number(oldValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "' "+ txt2 +" '";
    subTxt = QString::number(newValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_HOPPER_LOW_LOW_CHANGED:
    txt = tr("Low Low hopper level") + " " + txt1 + " '";
    subTxt = QString::number(oldValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "' "+ txt2 +" '";
    subTxt = QString::number(newValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_MANFILL_START:
    *msg = tr("Manual fill");
    break;

  case TXT_INDEX_FILL_START:
    *msg = tr("Filling");
    break;

  case TXT_INDEX_FILLALM_MODE_CHANGED:
    txt = tr("Fill alarm mode") + " " + txt1 + " '";
    for (i=0; i<2; i++) {
      switch (i) {
      case 0:
        val = oldValue;
        break;
      case 1:
        val = newValue;
        break;
      }
      GetEnumeratedText(etOnOff,val,&subTxt);
      txt = txt + subTxt;
      if (i == 0) {
        txt = txt + "' "+ txt2 + " '";
      }
    }
    txt = txt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_FILLCYCLES_CHANGED:                                          // (43)
    txt = tr("FillCycles") + " " + txt1 + " '";
    subTxt = QString::number(oldValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "' "+ txt2 +" '";
    subTxt = QString::number(newValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_ME_FILLTIME_CHANGED:                                         // Adv Loader Config (44)
    txt = tr("ME Fill time") + " " + txt1 + " '";
    subTxt = QString::number(oldValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "' "+ txt2 +" '";
    subTxt = QString::number(newValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_ME_ALARMTIME_CHANGED:                                        // Adv Loader Config (45)
    txt = tr("Fill alarm time") + " " + txt1 + " '";
    subTxt = QString::number(oldValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "' "+ txt2 +" '";
    subTxt = QString::number(newValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_MV_FILLTIME_CHANGED:                                         // Adv Loader Config (46)
    txt = tr("MV Fill time") + " " + txt1 + " '";
    subTxt = QString::number(oldValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "' "+ txt2 +" '";
    subTxt = QString::number(newValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_MV_EMPTYTIME_CHANGED:                                        // Adv Loader Config (47)
    txt = tr("MV Empty time") + " " + txt1 + " '";
    subTxt = QString::number(oldValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "' "+ txt2 +" '";
    subTxt = QString::number(newValue,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision);
    txt = txt + subTxt + "'";
    *msg = txt;
    break;

    //@@ Not yet implemented
    /*
                    case TXT_INDEX_CONS1_RESET 50
                    case TXT_INDEX_CONS2_RESET 51

                    case TXT_INDEX_DEV_ALM_B4 60 // Dev alarm % (Tolerance Settings)
            */
  case TXT_INDEX_LOADCELL_CALIBRATED:                                         // 61
    txt = tr("Loadcell calibrated");
    *msg = txt;
    break;

    //        case TXT_INDEX_CALDEV_SP_CHANGED

  case TXT_INDEX_DATETIME_CHANGED:                                            // 70
    *msg = tr("Date time changed");
    break;

  case TXT_INDEX_PRODMODE_CHANGED:                                            // 71
    txt = tr("Production mode") + " " + txt1 + " '";
    for (i=0; i<2; i++) {
      switch (i) {
      case 0:
        val = oldValue;
        break;
      case 1:
        val = newValue;
        break;
      }
      GetEnumeratedText(etProdType,val,&subTxt);
      txt = txt + subTxt;
      if (i == 0) {
        txt = txt + "' "+ txt2 + " '";
      }
    }
    txt = txt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_INPTYPE_CHANGED:                                             // 72
    txt = tr("Input type") + " " + txt1 + " '";
    for (i=0; i<2; i++) {
      switch (i) {
      case 0:
        val = oldValue;
        break;
      case 1:
        val = newValue;
        break;
      }
      GetEnumeratedText(etInpType,val,&subTxt);
      txt = txt + subTxt;
      if (i == 0) {
        txt = txt + "' "+ txt2 + " '";
      }
    }
    txt = txt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_LANGUAGE_CHANGED:                                            // 73
    txt = tr("Language") + " " + txt1 + " '";
    for (i=0; i<2; i++) {
      switch (i) {
      case 0:
        val = oldValue;
        break;
      case 1:
        val = newValue;
        break;
      }

      GetEnumeratedText(etLanguage,val,&subTxt);
      txt = txt + subTxt;
      if (i == 0) {
        txt = txt + "' "+ txt2 + " '";
      }
    }
    txt = txt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_AUTOSTART_CHANGED:                                           // 74
    txt = tr("Auto start") + " " + txt1 + " '";
    for (i=0; i<2; i++) {
      switch (i) {
      case 0:
        val = oldValue;
        break;
      case 1:
        val = newValue;
        break;
      }

      GetEnumeratedText(etOnOff,val,&subTxt);
      txt = txt + subTxt;
      if (i == 0) {
        txt = txt + "' "+ txt2 + " '";
      }
    }
    txt = txt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_RCPENABLE_CHANGED:                                           // 75
    txt = tr("Recipe enable") + " " + txt1 + " '";
    for (i=0; i<2; i++) {
      switch (i) {
      case 0:
        val = oldValue;
        break;
      case 1:
        val = newValue;
        break;
      }

      GetEnumeratedText(etOnOff,val,&subTxt);
      txt = txt + subTxt;
      if (i == 0) {
        txt = txt + "' "+ txt2 + " '";
      }
    }
    txt = txt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_STARTUP_USER_CHANGED:                                        // 76
    txt = tr("Startup user") + " " + txt1 + " '";
    for (i=0; i<2; i++) {
      switch (i) {
      case 0:
        val = oldValue;
        break;
      case 1:
        val = newValue;
        break;
      }
      GetEnumeratedText(etUserSelect,val,&subTxt);
      txt = txt + subTxt;
      if (i == 0) {
        txt = txt + "' "+ txt2 + " '";
      }
    }
    txt = txt + "'";
    *msg = txt;
    break;

  case TXT_INDEX_FACTORY_DEFAULTS:                                            // 77
    *msg = tr("Reset to Factory Defaults?");
    break;
  case TXT_INDEX_FACTORY_DEFAULTS_SUCCESSFUL:                                 // 78
    *msg = tr("Factory Defaults restored.")+"\n"+tr("Please reboot the controller.");
    break;
  case TXT_INDEX_CONFIRM_FACTORY_DEFAULTS:                                    // 79
    *msg = tr("Are you sure you want to reset the system")+"\n"+tr("to factory defaults?");
    break;
    // 100+ are other system messages
  case TXT_INDEX_PROD_SETTINGS_ERROR:
    *msg = tr("Production settings error!");
    break;
  case TXT_INDEX_DEF_MAT_FILE_NOT_FOUND:
    *msg = tr("Default material file not found.");
    break;
  case TXT_INDEX_MAT_FILE_NO_READ:
    *msg = tr("Material file could not be loaded.")+"\n"+tr("Default material for current")+"\n"+tr("configuration will be used.");
    break;
  case TXT_INDEX_MAT_FILE_NOT_FOUND:
    *msg = tr("Material file could not be found.")+"\n"+tr("Default material for current")+"\n"+tr("configuration will be used.");
    break;
  case TXT_INDEX_MAT_SAVED:
    *msg = tr("Material saved.");
    break;

  case TXT_INDEX_INTERNAL_LOG_NOT_POSSIBLE:
    txt = tr("Logging to internal memory not possible.")+"\n"+tr("Log interval < ")+QString::number(newValue)+"s\n"+tr("Please insert USB-stick.");
    *msg = txt;
    break;

  }

}


void AlarmHandler::GetEnumeratedText(eEnumType enumType, int enumIdx, QString * txt)
{
  QString tmp;

  // Initialize to empty so we always know we return something we expect.
  *txt = "";

  switch(enumType) {
  case etUserSelect:
    switch (enumIdx) {
    case 0:
      *txt = tr("Operator");
      break;
    case 1:
      *txt = tr("Tooling");
      break;
    case 2:
      *txt = tr("Supervisor");
      break;
    default:
      break;
    }
    break;

  case etUser:
    switch (enumIdx) {
    case 0:                                                                 // NONE
      *txt = "None";
      break;
    case 1:                                                                 // Operator
      GetEnumeratedText(etUserSelect,0,&tmp);
      *txt = tmp;
      break;
    case 2:                                                                 // Tooling
      GetEnumeratedText(etUserSelect,1,&tmp);
      *txt = tmp;
      break;
    case 3:                                                                 // Supervisor
    case 4:                                                                 // Supervisor Backdoor (= SuperVisor)
      GetEnumeratedText(etUserSelect,2,&tmp);
      *txt = tmp;
      break;
    case 5:                                                                 // Agent / Service
      *txt = tr("Service");
      break;
    case 6:                                                                 // MovaColor
      *txt = "MovaColor";
      break;
    default:
      break;
    }
    break;

  case etDosTool:
    switch(enumIdx) {
    case 0:                                                                 // GX
      *txt = tr("GX");
      break;
    case 1:                                                                 // GLX
      *txt = tr("GLX");
      break;
    case 2:                                                                 // HX
      *txt = tr("HX");
      break;
    case 3:
      *txt = tr("A30");
      break;
    case 4:
      *txt = tr("A20");
      break;
    case 5:
      *txt = tr("A15");
      break;
    case 6:
      *txt = tr("A50");
      break;

    default:
      break;
    }
    break;

  case etMotor:
    switch(enumIdx) {
    case 0:
      *txt = tr("LT");
      break;
    case 1:
      *txt = tr("HT");
      break;
    default:
      break;
    }
    break;

  case etGranulate:
    switch(enumIdx) {
    case 0:                                                                 // Normal
      *txt = tr("NG");
      break;
    case 1:                                                                 // Micro
      *txt = tr("MG");
      break;
    default:
      break;
    }
    break;

  case etProdType:
    switch(enumIdx) {
    case 0:
      *txt = tr("INJ");
      break;
    case 1:
      *txt = tr("EXT");
      break;
    default:
      break;
    }
    break;

  case etInpType:
    switch(enumIdx) {
    //                case 0:
    //                    *txt = tr("MC ID");
    //                    break;
    case 0:
      *txt = tr("RELAY");
      break;
    case 1:
      *txt = tr("TIMER");
      break;
    case 2:
      *txt = tr("TACHO");
      break;
    default:
      break;
    }
    break;

  case etGraviRpm:
    switch(enumIdx) {
    case 0:
      *txt = tr("RPM");
      break;
    case 1:
      *txt = tr("GRAVI");
      break;
    default:
      break;
    }
    break;

  case etFillSystem:
    switch(enumIdx) {
    case 0:
      *txt = tr("NONE");
      break;
    case 1:
      *txt = tr("ME");
      break;
    case 2:
      *txt = tr("MV");
      break;
    case 3:
      *txt = tr("EX");
      break;
    case 4:
      *txt = tr("RG");
      break;
    default:
      break;
    }
    break;

  case etLanguage:
    switch(enumIdx) {
    case 0:
      *txt = tr("English");
      break;
    case 1:
      *txt = tr("Dutch");
      break;
    case 2:
      *txt = tr("German");
      break;
    default:
      break;
    }

    break;

  case etYesNo:
    switch(enumIdx) {
    case 0:
      *txt = tr("YES");
      break;
    case 1:
      *txt = tr("NO");
      break;
    default:
      break;
    }
    break;

  case etOnOff:
    switch(enumIdx) {
    case 0:
      *txt = tr("OFF");
      break;
    case 1:
      *txt = tr("ON");
      break;
    default:
      break;
    }
    break;

  case etMcStatus:
    switch(enumIdx) {
    /*
                    mcsNone,
                    mcsOff,
                    mcsStandby,
                    mcsDosing,
                    mcsFilling,
                    mcsManFill,
                    mcdUnloading
                */
    case 0:
      *txt = tr(" NONE  ");
      break;
    case 1:
      *txt = tr("  OFF  ");
      break;
    case 2:
      *txt = tr("STANDBY");
      break;
    case 3:
      *txt = tr("DOSING ");
      break;
    case 4:
    case 5:
      *txt = tr("FILLING");
      break;
    case 6:
      *txt = tr("UNLOADING");
      break;
    default:
      *txt = tr("*******");
      break;
    }
    break;

  case etMcwType:
    switch(enumIdx) {
    case 0:
      *txt = tr("MW100");                                                   /* MCW 100 */
      break;
    case 1:
      *txt = tr("MW500");                                                   /* MCW 500 */
      break;
    case 2:
      *txt = tr("MW1000");                                                  /* MCW 1000 */
      break;
    default:
      break;
    }
    break;

  case etBalHOType:
    switch(enumIdx) {
    case 0:
      *txt = tr("HO100");                                                   /* MCW 100 */
      break;
    case 1:
      *txt = tr("HO500");                                                   /* MCW 500 */
      break;
    case 2:
      *txt = tr("HO1000");                                                  /* MCW 1000 */
      break;
    default:
      break;
    }
    break;

  default:
    break;
  }
}


void UpdateAlarmLog(int idx, int alarmNr,int msgType)
{
  /*-----------------------------*/
  /* Update the alarm log buffer */
  /*-----------------------------*/
  Rout::almLog.logLine[Rout::almLog.idx].alarmNr = alarmNr;
  Rout::almLog.logLine[Rout::almLog.idx].dateTim = QDateTime::currentDateTime().toTime_t();
  Rout::almLog.logLine[Rout::almLog.idx].eventID = 0;
  Rout::almLog.logLine[Rout::almLog.idx].msgType = msgType;
  Rout::almLog.logLine[Rout::almLog.idx].newValue = 0;
  Rout::almLog.logLine[Rout::almLog.idx].oldValue = 0;
  Rout::almLog.logLine[Rout::almLog.idx].unitIdx = idx;

  /*----- Update alarm log index -----*/
  if (++Rout::almLog.idx >= MAX_ALARM_LOG) {
    Rout::almLog.idx = 0;
    Rout::almLog.full = TRUE;                                                   /* Set buffer full flag */
  }

  Rout::WriteAlarmLogFile();                                                    /* Write alarm log file */
}


void AlarmHandler::AlarmActiveChanged(bool act, int idx)
{
  /*-----------------------------------------*/
  /* Slot : AlarmHandler                     */
  /* There is a new active alarm             */
  /* Connected to SysSts_AlarmStatusChanged  */
  /*-----------------------------------------*/
  if (act) {
    QString txt = "";
    int msgNr = glob->sysSts[idx].alarmSts.nr;
    float value = 0;

    /*----- Detect active alarm -----*/
    if (msgNr != 0) {
      ActiveAlarmHandler();                                                     /* LCD backlit to 100 % */
      switch(msgNr) {
      case ALM_INDEX_FILL_SYSTEM:                                             // Fill system not filling
        break;
      case ALM_INDEX_LO_LEVEL:                                                // Low message
        value = glob->sysCfg[idx].fillLevels.Lo;
        break;
      case ALM_INDEX_LO_LO_LEVEL:                                             // Low low message
        value = glob->sysCfg[idx].fillLevels.LoLo;
        break;
      case ALM_INDEX_HI_HI_LEVEL:                                             // Hi Hi message
        value = glob->sysCfg[idx].fillLevels.HiHi;
        break;
      case ALM_INDEX_MIN_MOTOR_SPEED:                                         // Minimum motor speed
        break;
      case ALM_INDEX_MAX_MOTOR_SPEED:
        break;
      case ALM_INDEX_DEV_QR:                                                  // QR will not show a value
        break;
      case ALM_INDEX_DEV:                                                     // Normal does show the actual value
        value = glob->sysSts[idx].procDiagData.vidColDevPct;
        break;
      case ALM_INDEX_VALVE_NOT_CLOSED:                                        // Valve not closed
        break;
      case ALM_INDEX_MASTER_SLAVE_COM:                                        // Master/Slave connection failure
        break;
      case ALM_INDEX_MOTOR_CON_4WIRE:
        break;
      case ALM_INDEX_MOTOR_CON_6WIRE:
        break;
      case ALM_INDEX_LOADCELL_CON:
        break;
      default:
        break;
      }
      // Get mode (warning/alarm)
      int msgMode = glob->sysSts[idx].alarmSts.mode;

      // Add item to event list (Geen functionalteit) @@RH
      glob->AddEventItem(idx,msgMode,msgNr,value,0);

      /*----- Update log alarm table -----*/
      UpdateAlarmLog(idx,msgNr,msgMode);

      // Get text for display
      GetAlarmText(msgNr,value,&txt,true);
      // Display alarm popup window
      switch(msgMode) {
      case 0:
        ((MainMenu*)(glob->menu))->DisplayMessage(idx,msgNr,txt,msgtWarning);
        break;
      case 1:
        ((MainMenu*)(glob->menu))->DisplayMessage(idx,msgNr,txt,msgtAlarm);
        break;
      default:
        ((MainMenu*)(glob->menu))->DisplayMessage(idx,msgNr,txt);
        break;
      }
    }

  }
}


void AlarmHandler::AlarmReset(int idx)
{
  /*-------------------------*/
  /* Reset the active alarms */
  /*-------------------------*/

  /*----- Reset alarm for all connected units -----*/
  Rout::ResetAlarms();
}


void AlarmHandler::AlarmResetAck(int idx)
{
  glob->sysSts[idx].alarmSts.reset = false;
}

/****************************************************************************
** Meta object code from reading C++ file 'LogServer.h'
**
** Created: Thu Jan 31 11:21:17 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "LogServer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'LogServer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LogServer[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x0a,
      27,   10,   10,   10, 0x0a,
      37,   10,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_LogServer[] = {
    "LogServer\0\0NewConnection()\0TcpRead()\0"
    "TcpDisConnected()\0"
};

const QMetaObject LogServer::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_LogServer,
      qt_meta_data_LogServer, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LogServer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LogServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LogServer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LogServer))
        return static_cast<void*>(const_cast< LogServer*>(this));
    return QObject::qt_metacast(_clname);
}

int LogServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: NewConnection(); break;
        case 1: TcpRead(); break;
        case 2: TcpDisConnected(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

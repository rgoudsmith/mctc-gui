/*########## Message data in ##########*/

byte balProdStatus;                                                             /* B0 : Balance production status  Display "Calibrating" / "Calibrated" */
byte vidProdSts;                                                                /* B1 : Video production status */
byte preCalBusy;                                                                /* B2 : Precalibration busy  */
byte preCalReady;                                                               /* B3 : Precalibration ready  */
byte preCalPauseBusy;                                                           /* B4 : Precalibration pause busy */
byte preCalCalCapIstValid;                                                      /* B5 : PreCalRpmCorrValid*/
byte prodTestBusy;                                                              /* B6 : Production test busy */
byte alarmNr;                                                                   /* B7 : Alarm number */
byte almActive;                                                                 /* B8 : Alarm active */
byte alarmMode;                                                                 /* B9 : alarm mode alarm/warning */
byte metTimeActValid;                                                           /* B10 : Metering time valid */
byte spare0;                                                                    /* B11 : Spare */

/*-----  Telnet screen -----*/
byte metRstHystCnt;                                                             /* B12 : Metering hysterese reset counter for debug */
byte bandNr;                                                                    /* B13 : Band data - nr */
byte inBand;                                                                    /* B14 : Band data - in*/
byte outBand;                                                                   /* B15 : Band data - out*/
byte bandCnt;                                                                   /* B16 : Band data - Max*/
byte corSts;                                                                    /* B17 : Correction state */
byte mcSts;                                                                     /* B18 : Ist measurement state */
byte putIst;                                                                    /* B19 : Put weight counter */
byte getIst;                                                                    /* B20 : Get weight counter */
byte mcStsQR;                                                                   /* B21 : QR - Ist measurement state */
byte putIstQR;                                                                  /* B22 : QR - Put weight counter */
byte getIstQR;                                                                  /* B23 : QR - Get weight counter */
byte devRetryQR;                                                                /* B24 : QR - Deviation retry counter */

/*----- I/O board information -----*/
byte versionSw;                                                                 /* B25 : Software version */
byte versionHw;                                                                 /* B26 : Hardware version */
byte temperature;                                                               /* B27 : Board temperature */

/*----- Various -----*/
byte mc24On;                                                                    /* B28 : MC-TC on status */
byte mcStartError;                                                              /* B29 : Start error */
byte alarmResetAck;                                                             /* B30 : Alarm reset ack */
byte standStill;                                                                /* B31 : Standstill */
byte newShotDetect;                                                             /* B32 : New shot detect */
byte primeBusy;                                                                 /* B33 : Prime busy */
byte input[4];                                                                  /* B34-37 : Digital inputs */
byte mcCmd;                                                                     /* B38 : McCmd */
byte spare1;                                                                    /* B39 : Spare */

float preCalCalCapIst;                                                          /* F0 : Pre calibration cap ist */
float rpmCorr;                                                                  /* F1 : RPM correction */
float productsToGo;                                                             /* F2 : Theoretical products to go */
float consumption;                                                              /* F3 : Consumption */
float consumptionDay;                                                           /* F4 : Consumption */
float timeToGo;                                                                 /* F5 : Inj mode shot time to go */
float grossWeightFast;                                                          /* F6 : Fast weight for test */

float tachoAct;                                                                 /* F7 : Actual tacjo voltage */
float extCapAct;                                                                /* F8 : Actual extruder capacity */
float metTimeAct;                                                               /* F9 : Dosing (Metering) time */

/*----- Telnet screen -----*/
float metTimeMeas;                                                              /* F10 : Meting time measured */
float metTimeCalc;                                                              /* F11 : Metering time avg */
float colCapIst;                                                                /* F12 : Color cap act (not video)*/
float vidColDevPct;                                                             /* F13 : Capacity deviation ABS*/
float colDevPctRel;                                                             /* F14 : Capacity deviation REL*/
float corCycle;                                                                 /* F15 : Correction Cycle*/
float vidCorrection;                                                            /* F16 : Correction */
float rpmCorr;                                                                  /* F17 : RPM Correction*/
float corFacAdjust;                                                             /* F18 : FacAdj*/
float  minTimeCorFac;                                                           /* F19 : Min Time Corr*/

/*----- Telnet screen word's !!! -----*/
float sampleLen;                                                                /* F20 : Measurement length SET*/
float timeLeft;                                                                 /* F21 : Measurement length ACT*/
float sampleLenQR;                                                              /* F22 : QR Measurement length SET */
float timeLeftQR;                                                               /* F23 : QR Measurement length ACT*/
float colDevPctQR;                                                              /* F24 */

}



    //1  /*------ Diverse -----*/
    pDatMsg->fill.fillMan=some_data.byteBuf[5];
    pDatMsg->preCal.preCalStart=some_data.byteBuf[6];
    pDatMsg->preCal.preCalAbort=some_data.byteBuf[7];
    pDatMsg->preCal.preCalPause=some_data.byteBuf[8];
    pDatMsg->preCal.preCalReadyAck=some_data.byteBuf[9];
    pDatMsg->preCal.preCalContinue=some_data.byteBuf[10];
    pDatMsg->cons.consReset=some_data.byteBuf[11];                              /* Consumption reset */
    pDatMsg->cons.consResetDay=some_data.byteBuf[12];                           /* Consumption reset */

    pDatMsg->mc.prodTestStart=some_data.byteBuf[13];                            //not tested
    pDatMsg->mc.prodTestAbort=some_data.byteBuf[14];                            //not tested
    pDatMsg->alm.alarmResetCmd=some_data.byteBuf[15];
    pDatMsg->can.manualIO = some_data.byteBuf[16];
    pDatMsg->can.manOutput[0] = some_data.byteBuf[17];
    pDatMsg->can.manOutput[1] = some_data.byteBuf[18];
    pDatMsg->can.manOutput[2] = some_data.byteBuf[19];
    pDatMsg->can.manOutput[3] = some_data.byteBuf[20];
    pDatMsg->mc.curveChanged = some_data.byteBuf[21];

    rtcUpdate = some_data.byteBuf[22];

    // slordig
    switch (some_data.byteBuf[23]) {
      case 1:
        buzzerCmd = BEEP_KEY;
        break;

      case 2:                                                                   //usb connect detect
        buzzerCmd = BEEP_ERROR;
        break;

      case 4:                                                                   //usb disconnect detect
        buzzerCmd = BEEP_ERROR;
        break;

        // geen toegang tot menu -->motor on
      case 8:
        buzzerCmd = BEEP_ERROR;
        break;
    }

    pDatMsg->mc.tachoMax=some_data.floatBuf[11];
    pDatMsg->mc.dosTimeSetMetStart=some_data.floatBuf[12];
    pDatMsg->mc.rpmSet = some_data.floatBuf[13];
    pDatMsg->can.manMotorSpeed = some_data.floatBuf[14];
    memcpy(&pDatMsg->mc.curve,&some_data.crv,sizeof(tCurve));



/********************************************************************************
** Form generated from reading UI file 'Form_Tacho.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_TACHO_H
#define UI_FORM_TACHO_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TachoFrm
{
public:
    QVBoxLayout *verticalLayout_8;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_7;
    QVBoxLayout *LayoutHeader;
    QLabel *lbTachoType;
    QSpacerItem *verticalSpacer_4;
    QGroupBox *gbSelection;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QPushButton *btManual;
    QPushButton *btSynchronise;
    QSpacerItem *horizontalSpacer_2;
    QGroupBox *gbManualTacho;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_4;
    QVBoxLayout *verticalLayout;
    QLabel *lbManTacho;
    QLineEdit *edManTacho;
    QVBoxLayout *verticalLayout_3;
    QSpacerItem *verticalSpacer_2;
    QLabel *lbManIs;
    QSpacerItem *verticalSpacer_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *lbManExt;
    QLineEdit *edManExt;
    QSpacerItem *horizontalSpacer_5;
    QGroupBox *gbSyncTacho;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_9;
    QLabel *label_7;
    QSpacerItem *horizontalSpacer_7;
    QLabel *lbSyncExt;
    QHBoxLayout *horizontalLayout_4;
    QLabel *lbActual;
    QSpacerItem *horizontalSpacer_8;
    QLabel *lbActTacho_2;
    QLabel *label_9;
    QLabel *lbActExtCap;
    QFrame *line;
    QHBoxLayout *horizontalLayout_2;
    QLabel *lbNew;
    QSpacerItem *horizontalSpacer_6;
    QLabel *lbActTacho;
    QLabel *label_8;
    QLineEdit *edSyncExt;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *LayoutButtons;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btOk;
    QPushButton *btCancel;

    void setupUi(QWidget *TachoFrm)
    {
        if (TachoFrm->objectName().isEmpty())
            TachoFrm->setObjectName(QString::fromUtf8("TachoFrm"));
        TachoFrm->setWindowModality(Qt::ApplicationModal);
        TachoFrm->resize(544, 618);
        TachoFrm->setCursor(QCursor(Qt::BlankCursor));
        TachoFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout_8 = new QVBoxLayout(TachoFrm);
        verticalLayout_8->setContentsMargins(0, 0, 0, 0);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        groupBox = new QGroupBox(TachoFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox{border: 2px solid black; border-radius: 3px; background: white}"));
        groupBox->setTitle(QString::fromUtf8(""));
        verticalLayout_7 = new QVBoxLayout(groupBox);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        LayoutHeader = new QVBoxLayout();
        LayoutHeader->setObjectName(QString::fromUtf8("LayoutHeader"));
        LayoutHeader->setContentsMargins(-1, -1, -1, 20);
        lbTachoType = new QLabel(groupBox);
        lbTachoType->setObjectName(QString::fromUtf8("lbTachoType"));
        QFont font;
        font.setPointSize(20);
        lbTachoType->setFont(font);
        lbTachoType->setAlignment(Qt::AlignCenter);

        LayoutHeader->addWidget(lbTachoType);


        verticalLayout_7->addLayout(LayoutHeader);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_4);

        gbSelection = new QGroupBox(groupBox);
        gbSelection->setObjectName(QString::fromUtf8("gbSelection"));
        gbSelection->setTitle(QString::fromUtf8(""));
        horizontalLayout_3 = new QHBoxLayout(gbSelection);
        horizontalLayout_3->setSpacing(32);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 5, 0, 5);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        btManual = new QPushButton(gbSelection);
        btManual->setObjectName(QString::fromUtf8("btManual"));
        btManual->setMinimumSize(QSize(200, 90));
        btManual->setMaximumSize(QSize(200, 90));
        QFont font1;
        font1.setPointSize(18);
        btManual->setFont(font1);

        horizontalLayout_3->addWidget(btManual);

        btSynchronise = new QPushButton(gbSelection);
        btSynchronise->setObjectName(QString::fromUtf8("btSynchronise"));
        btSynchronise->setMinimumSize(QSize(200, 90));
        btSynchronise->setMaximumSize(QSize(200, 90));
        btSynchronise->setFont(font1);

        horizontalLayout_3->addWidget(btSynchronise);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout_7->addWidget(gbSelection);

        gbManualTacho = new QGroupBox(groupBox);
        gbManualTacho->setObjectName(QString::fromUtf8("gbManualTacho"));
        gbManualTacho->setMinimumSize(QSize(0, 0));
        gbManualTacho->setTitle(QString::fromUtf8(""));
        horizontalLayout = new QHBoxLayout(gbManualTacho);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lbManTacho = new QLabel(gbManualTacho);
        lbManTacho->setObjectName(QString::fromUtf8("lbManTacho"));
        lbManTacho->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lbManTacho);

        edManTacho = new QLineEdit(gbManualTacho);
        edManTacho->setObjectName(QString::fromUtf8("edManTacho"));
        edManTacho->setMinimumSize(QSize(150, 70));
        edManTacho->setMaximumSize(QSize(150, 70));
        edManTacho->setCursor(QCursor(Qt::BlankCursor));
        edManTacho->setText(QString::fromUtf8(""));
        edManTacho->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(edManTacho);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);

        lbManIs = new QLabel(gbManualTacho);
        lbManIs->setObjectName(QString::fromUtf8("lbManIs"));
        lbManIs->setText(QString::fromUtf8("="));
        lbManIs->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(lbManIs);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_3);


        horizontalLayout->addLayout(verticalLayout_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lbManExt = new QLabel(gbManualTacho);
        lbManExt->setObjectName(QString::fromUtf8("lbManExt"));
        lbManExt->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(lbManExt);

        edManExt = new QLineEdit(gbManualTacho);
        edManExt->setObjectName(QString::fromUtf8("edManExt"));
        edManExt->setMinimumSize(QSize(150, 70));
        edManExt->setMaximumSize(QSize(150, 70));
        edManExt->setCursor(QCursor(Qt::BlankCursor));
        edManExt->setText(QString::fromUtf8(""));
        edManExt->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(edManExt);


        horizontalLayout->addLayout(verticalLayout_2);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_5);


        verticalLayout_7->addWidget(gbManualTacho);

        gbSyncTacho = new QGroupBox(groupBox);
        gbSyncTacho->setObjectName(QString::fromUtf8("gbSyncTacho"));
        gbSyncTacho->setMinimumSize(QSize(0, 200));
        QFont font2;
        font2.setKerning(true);
        gbSyncTacho->setFont(font2);
        gbSyncTacho->setTitle(QString::fromUtf8(""));
        verticalLayout_5 = new QVBoxLayout(gbSyncTacho);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_9);

        label_7 = new QLabel(gbSyncTacho);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(150, 30));
        label_7->setMaximumSize(QSize(150, 30));
        label_7->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_7);

        horizontalSpacer_7 = new QSpacerItem(15, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_7);

        lbSyncExt = new QLabel(gbSyncTacho);
        lbSyncExt->setObjectName(QString::fromUtf8("lbSyncExt"));
        lbSyncExt->setMinimumSize(QSize(150, 30));
        lbSyncExt->setMaximumSize(QSize(150, 30));
        lbSyncExt->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(lbSyncExt);


        verticalLayout_4->addLayout(horizontalLayout_5);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        lbActual = new QLabel(gbSyncTacho);
        lbActual->setObjectName(QString::fromUtf8("lbActual"));
        lbActual->setMinimumSize(QSize(50, 0));

        horizontalLayout_4->addWidget(lbActual);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_8);

        lbActTacho_2 = new QLabel(gbSyncTacho);
        lbActTacho_2->setObjectName(QString::fromUtf8("lbActTacho_2"));
        lbActTacho_2->setMinimumSize(QSize(150, 30));
        lbActTacho_2->setMaximumSize(QSize(150, 30));
        lbActTacho_2->setText(QString::fromUtf8("> 10 Vdc <"));
        lbActTacho_2->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(lbActTacho_2);

        label_9 = new QLabel(gbSyncTacho);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(15, 30));
        label_9->setMaximumSize(QSize(15, 30));
        label_9->setText(QString::fromUtf8("="));
        label_9->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(label_9);

        lbActExtCap = new QLabel(gbSyncTacho);
        lbActExtCap->setObjectName(QString::fromUtf8("lbActExtCap"));
        lbActExtCap->setMinimumSize(QSize(150, 30));
        lbActExtCap->setMaximumSize(QSize(150, 30));
        lbActExtCap->setText(QString::fromUtf8("xxx kg/h"));
        lbActExtCap->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(lbActExtCap);


        verticalLayout_4->addLayout(horizontalLayout_4);

        line = new QFrame(gbSyncTacho);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShadow(QFrame::Plain);
        line->setFrameShape(QFrame::HLine);

        verticalLayout_4->addWidget(line);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lbNew = new QLabel(gbSyncTacho);
        lbNew->setObjectName(QString::fromUtf8("lbNew"));
        lbNew->setMinimumSize(QSize(50, 0));

        horizontalLayout_2->addWidget(lbNew);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);

        lbActTacho = new QLabel(gbSyncTacho);
        lbActTacho->setObjectName(QString::fromUtf8("lbActTacho"));
        lbActTacho->setMinimumSize(QSize(150, 70));
        lbActTacho->setMaximumSize(QSize(150, 70));
        lbActTacho->setText(QString::fromUtf8("> 10 Vdc <"));
        lbActTacho->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(lbActTacho);

        label_8 = new QLabel(gbSyncTacho);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMinimumSize(QSize(15, 70));
        label_8->setMaximumSize(QSize(15, 70));
        label_8->setText(QString::fromUtf8("="));
        label_8->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(label_8);

        edSyncExt = new QLineEdit(gbSyncTacho);
        edSyncExt->setObjectName(QString::fromUtf8("edSyncExt"));
        edSyncExt->setMinimumSize(QSize(150, 70));
        edSyncExt->setMaximumSize(QSize(150, 70));
        edSyncExt->setCursor(QCursor(Qt::BlankCursor));
        edSyncExt->setText(QString::fromUtf8(""));
        edSyncExt->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(edSyncExt);


        verticalLayout_4->addLayout(horizontalLayout_2);


        verticalLayout_5->addLayout(verticalLayout_4);


        verticalLayout_7->addWidget(gbSyncTacho);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer);

        LayoutButtons = new QHBoxLayout();
        LayoutButtons->setObjectName(QString::fromUtf8("LayoutButtons"));
        LayoutButtons->setContentsMargins(-1, -1, 0, -1);
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        LayoutButtons->addItem(horizontalSpacer_3);

        btOk = new QPushButton(groupBox);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        LayoutButtons->addWidget(btOk);

        btCancel = new QPushButton(groupBox);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("CANCEL"));

        LayoutButtons->addWidget(btCancel);


        verticalLayout_7->addLayout(LayoutButtons);


        verticalLayout_8->addWidget(groupBox);


        retranslateUi(TachoFrm);

        QMetaObject::connectSlotsByName(TachoFrm);
    } // setupUi

    void retranslateUi(QWidget *TachoFrm)
    {
        lbTachoType->setText(QApplication::translate("TachoFrm", "Manual", 0, QApplication::UnicodeUTF8));
        btManual->setText(QApplication::translate("TachoFrm", "MANUAL", 0, QApplication::UnicodeUTF8));
        btSynchronise->setText(QApplication::translate("TachoFrm", "SYNCHRONIZE", 0, QApplication::UnicodeUTF8));
        lbManTacho->setText(QApplication::translate("TachoFrm", "Set Tacho", 0, QApplication::UnicodeUTF8));
        lbManExt->setText(QApplication::translate("TachoFrm", "Extruder Cap.", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("TachoFrm", "Act Tacho", 0, QApplication::UnicodeUTF8));
        lbSyncExt->setText(QApplication::translate("TachoFrm", "Extruder Cap.", 0, QApplication::UnicodeUTF8));
        lbActual->setText(QApplication::translate("TachoFrm", "Actual:", 0, QApplication::UnicodeUTF8));
        lbNew->setText(QApplication::translate("TachoFrm", "New:", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(TachoFrm);
    } // retranslateUi

};

namespace Ui {
    class TachoFrm: public Ui_TachoFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_TACHO_H

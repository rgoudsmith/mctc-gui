#include "Form_MultiComponentItem.h"
#include "ui_Form_MultiComponentItem.h"
#include "Form_MainMenu.h"
#include "Rout.h"

MultiComponentItemFrm::MultiComponentItemFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::MultiComponentItemFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  unitIdx = -1;
  connected = false;
  ui->setupUi(this);
  setFixedWidth(190);


  connect(ui->btImage,SIGNAL(clicked()),this,SLOT(ImageClicked()));

  ui->colPct->installEventFilter(this);

  /*----- Setup numeric keyboard -----*/
  numInput = new NumericInputFrm();
  numInput->move(200,50);
  connect(numInput,SIGNAL(InputValue(float)),this,SLOT(ValueReturned(float)));
  numInput->FormInit();

  // Selection input form
  selForm = new selectionFrm();
  //  selForm->move(0,0);
  selForm->move(((800/2)-(selForm->width()/2)),((600/2)-(selForm->height()/2)));
  selForm->hide();
  connect(selForm,SIGNAL(selectionMade(int,int)),this,SLOT(SelectionReceived(int,int)));


  /*----- Centre the text in the status button -----*/
  ui->btStatus->setStyleSheet("Text-align:centre");

  ui->btAlarm->setVisible(false);                                               /* Alarm button off */
  connect(ui->btAlarm,SIGNAL(clicked()),this,SLOT(AlarmClicked()));

}


MultiComponentItemFrm::~MultiComponentItemFrm()
{
  delete ui;
}


bool MultiComponentItemFrm::eventFilter(QObject *obj, QEvent *ev)
{
  /*------------------------------*/
  /* Event filter                 */
  /*------------------------------*/
  /* Detect colPct button pressed */
  /*------------------------------*/
  if (unitIdx != -1) {

    /*----- ColPct button pressed is ignored when unit is a McWeight -----*/
    if (glob->sysCfg[unitIdx].deviceType != mcdtWeight) {

      /*----- Detet mouse button pressed in colPct field -----*/
      if (ev->type() == QEvent::MouseButtonPress) {
        if (obj == ui->colPct) {
          KeyBeep();
          numInput->SetDisplay(true,3,glob->sysCfg[unitIdx].colPct,0,100);
          numInput->show();
          return true;
        }
        else {
          return false;
        }
      }
    }
    return QWidget::eventFilter(obj,ev);
  }
}


void MultiComponentItemFrm::resizeEvent(QResizeEvent *e)
{
  /*--------------*/
  /* Resize event */
  /*--------------*/
  QWidget::resizeEvent(e);
  numInput->move(200,50);
  selForm->move(((800/2)-(selForm->width()/2)),((600/2)-(selForm->height()/2)));
}


void MultiComponentItemFrm::showEvent(QShowEvent *e)
{
  /*------------*/
  /* Show event */
  /*------------*/
  int idx;

  QBaseWidget::showEvent(e);

  if (unitIdx != -1) {

    switch (Rout::homeScreenMultiUnitActive) {

    /*----- Multi unit status screen mode -----*/
    case(MULTI_SCR_MODE_STATUS) :

      ui->btConfig->setVisible(false);
      ui->btInputMode->setVisible(false);

      ui->btStatus->setVisible(true);
      ui->colPct->setVisible(true);

      switch (glob->sysCfg[unitIdx].deviceType) {

      case mcdtWeight:
        ui->colPct->setText(QString::number(glob->sysSts[unitIdx].mcWeightSts.mcWeightPct)+"%");
        ui->groupBox->setMaximumHeight(300);
        ui->groupBox->setAlignment(Qt::AlignVCenter);
        break;

      default :
        ui->colPct->setText(QString::number(glob->sysCfg[unitIdx].colPct,glob->formats.PercentageFmt.format.toAscii(),glob->formats.PercentageFmt.precision)+QString(" %"));
        ui->groupBox->setMaximumHeight(300);
        ui->groupBox->setAlignment(Qt::AlignVCenter);
        break;
      }
      break;

      /*----- Multi unit select screen mode -----*/
    case(MULTI_SCR_MODE_SELECT) :
      ui->colPct->setVisible(false);
      ui->btStatus->setVisible(false);
      ui->btAlarm->setVisible(false);
      ui->btConfig->setVisible(false);
      ui->btInputMode->setVisible(false);
      ui->groupBox->setMaximumHeight(175);
      break;

      /*----- Multi unit system config screen mode -----*/
    case(MULTI_SCR_MODE_CONFIG) :
      ui->colPct->setVisible(false);
      ui->btStatus->setVisible(false);
      ui->btAlarm->setVisible(false);
      //      ui->btConfig->setVisible(true);
      //      ui->btInputMode->setVisible(true);
      //      ui->groupBox->setMaximumHeight(325);
#warning Disabled for now
      ui->btConfig->setVisible(false);
      ui->btInputMode->setVisible(false);
      ui->groupBox->setMaximumHeight(175);

      break;

    }
  }
}


void MultiComponentItemFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  ui->lbName->setFont(*(glob->configFont));
  ui->lbName->setStyleSheet("font-weight: bold;");

  ui->colPct->setFont(*(glob->baseFont));
  ui->colPct->setStyleSheet("font-weight: bold;");

  ui->btStatus->setFont(*(glob->configFont));
  ui->btStatus->setStyleSheet("font-weight: bold;");

  selForm->FormInit();
}


void MultiComponentItemFrm::ConnectSignals()
{
  /*------------------------------------------------------*/
  /* Connect various signals depending in the device type */
  /*------------------------------------------------------*/
  if (!connected) {                                                             // One shot
    switch (glob->sysCfg[unitIdx].deviceType) {

    case mcdtWeight:
      connect(glob,SIGNAL(SysSts_McWeightCapChanged(float,float,float,int)),this,SLOT(McWeightCapChanged(float,float,float,int)));
      connect(glob,SIGNAL(SysSts_MCStatusChanged(eMCStatus,int)),this,SLOT(UnitStatusChanged(eMCStatus,int)));
      break;

    case mcdtBalance:
    case mcdtBalanceHO:
      //      connect(glob,SIGNAL(SysSts_DosActChanged(float,int)),this,SLOT(SysSts_DosActChanged(float,int)));
      //      connect(glob,SIGNAL(SysCfg_DosSetChanged(float,int)),this,SLOT(SysCfg_DosSetChanged(float,int)));
      connect(glob,SIGNAL(SysCfg_ColorPctChanged(float,int)),this,SLOT(SysCfg_ColorPctChanged(float,int)));
      connect(glob,SIGNAL(SysSts_MCStatusChanged(eMCStatus,int)),this,SLOT(UnitStatusChanged(eMCStatus,int)));
      break;

    }

    connect(glob,SIGNAL(SysCfg_IDChanged(QString,int)),this,SLOT(UnitNameChanged(QString,int)));
    connect(glob,SIGNAL(SysSts_AlarmStatusChanged(bool,int)),this,SLOT(AlarmStatusChanged(bool,int)));
    //    connect(glob,SIGNAL(SysSts_SysEnabledChanged(bool,int)),this,SLOT(SysActiveChanged(bool,int)));
  }
  connected = true;
}


void MultiComponentItemFrm::SetUnitIndex(int idx)
{
  if (idx != unitIdx) {
    unitIdx = idx;
    ConnectSignals();
    UpdateAllItems();
  }
}


void MultiComponentItemFrm::UpdateUnitActiveSts()
{
  /*------------------------------------*/
  /* Update the active status of a unit */
  /*------------------------------------*/
  if (unitIdx != -1) {
    bool sysAct;
    bool unitAct;
    unitAct = glob->sysCfg[unitIdx].sysEnabled;
    sysAct = glob->mstSts.sysActive;

    // Current unit status in graphics
    QImage img;
    QString imgPath;

    if (unitAct) {
      imgPath = *(glob->GetImagePath(itGeneral,genLedGreen));
    }
    else {
      imgPath = *(glob->GetImagePath(itGeneral,genLedRed));
    }

    img.load(imgPath);
    img = img.scaled(QSize(20,20),Qt::KeepAspectRatio);
    if (img.isNull()) {
      //      ui->lbStsImg->setText(tr("no image"));
    }
    else {
      //      ui->lbStsImg->setPixmap(QPixmap::fromImage(img));
    }
  }
}


void MultiComponentItemFrm::UpdateAllItems()
{
  // Code to update all graphics items
  if (unitIdx != -1) {
    //    UpdateUnitActiveSts();
    SetupDisplay(glob->sysCfg[unitIdx].deviceType);
    UnitNameChanged(glob->sysCfg[unitIdx].MC_ID,unitIdx);
    UnitStatusChanged(glob->sysSts[unitIdx].MCStatus,unitIdx);
    SysCfg_ColorPctChanged(glob->sysCfg[unitIdx].colPct,unitIdx);
    SysSts_DosActChanged(glob->sysSts[unitIdx].actProd,unitIdx);
    SysCfg_DosSetChanged(glob->sysSts[unitIdx].setProd,unitIdx);
  }
}


void MultiComponentItemFrm::SetupDisplay(eMcDeviceType device)
{
  /*---------------------------------------------*/
  /* Setup the display for the correct unit type */
  /*---------------------------------------------*/
  /*----- Load correct image into the button -----*/
  switch(device) {

  case mcdtWeight:
    glob->SetButtonImage(ui->btImage,itDeviceType,mcdtWeight);
    break;

  case mcdtBalance:
    glob->SetButtonImage(ui->btImage,itDeviceType,mcdtBalance);
    break;

  case mcdtBalanceHO:
    glob->SetButtonImage(ui->btImage,itDeviceType,mcdtBalanceHO);
    break;

  case mcdtBalanceRegrind:
    glob->SetButtonImage(ui->btImage,itDeviceType,mcdtBalanceRegrind);
    break;

  case mcdtBalanceHORegrind:
    glob->SetButtonImage(ui->btImage,itDeviceType,mcdtBalanceHORegrind);
    break;

  default:
    break;
  }
}


void MultiComponentItemFrm::AlarmClicked()
{
  /*----------------------*/
  /* Alarm button clicked */
  /*----------------------*/

  ((MainMenu*)(glob->menu))->DisplayWindow(wiAlarmActual);
}


void MultiComponentItemFrm::ImageClicked()
{
  /*-----------------------------*/
  /* System image button clicked */
  /*-----------------------------*/

  switch (Rout::homeScreenMultiUnitActive) {

  /*----- Status or select screen -----*/
  case(MULTI_SCR_MODE_STATUS) :
  case(MULTI_SCR_MODE_SELECT) :

    if (unitIdx >= 0) {
      glob->MstSts_SetActUnitIndex(unitIdx);                    /* Switch to detail screen */
      ((MainMenu*)(glob->menu))->DisplayWindow(wiBack);
      emit UnitSelected();
    }
    break;

    /*----- Config screen -----*/
  case(MULTI_SCR_MODE_CONFIG) :
    selForm->SetFormCaption(tr("Select Unit type"));
    selForm->DisplaySelection(itDeviceType,glob->sysCfg[unitIdx].deviceType);
    selForm->show();
    break;

  }
}


void MultiComponentItemFrm::btOnOffClicked()
{
  /*----------------------------*/
  /* Unit on/off button clicked */
  /*----------------------------*/
  if (unitIdx != -1) {
    glob->SysCfg_SetSysEnabled((!(glob->sysCfg[unitIdx].sysEnabled)),unitIdx);
    UpdateUnitActiveSts();
  }
}


void MultiComponentItemFrm::UnitNameChanged(const QString &name, int idx)
/*--------------------*/
/* Unuit name changed */
/*--------------------*/
{
  if (idx == unitIdx) {
    ui->lbName->setText(name);
  }
}


void MultiComponentItemFrm::AlarmStatusChanged(bool status,int idx)
{
  /*----------------------*/
  /* Alarm status changed */
  /*----------------------*/

  if (idx == unitIdx) {

    /*----- Alarm update onky when in status screen mode -----*/
    if (Rout::homeScreenMultiUnitActive == MULTI_SCR_MODE_STATUS) {

      /*----- Alarm active ----*/
      if (status) {

        ui->btAlarm->setVisible(true);                                            /* Button active */

        /*----- Show warning or alarm image in button -----*/

        /*----- Warning -----*/
        if (glob->sysSts[idx].alarmSts.mode == 0 ) {                              /* Warning */
          glob->SetButtonImage(ui->btAlarm,itAlarmWarning,awWarning);
        }
        /*----- Alarm -----*/
        else {
          glob->SetButtonImage(ui->btAlarm,itAlarmWarning,awAlarm);
        }
      }

      /*----- Alarm not active -----*/
      else {
        if (glob->sysSts[idx].alarmSts.nr == 0 ) {
          ui->btAlarm->setVisible(false);                                         /* Button not active */
        }
      }
    }
  }
}


void MultiComponentItemFrm::UnitStatusChanged(eMCStatus sts, int idx)
{
  /*-----------------------------*/
  /* Event : Unit status changed */
  /* Update the status text      */
  /*-----------------------------*/
  QString txt;

  if (idx == unitIdx) {

    glob->GetEnumeratedText(etMcStatus,sts,&txt);
    ui->btStatus->setStyleSheet("Text-align:centre");
    ui->btStatus->setText(txt);
  }
}


void MultiComponentItemFrm::SysCfg_ColorPctChanged(float pct,int idx)
/*--------------------------*/
/* Color percentage changed */
/*--------------------------*/
{
  if (idx == unitIdx) {
    ui->colPct->setText(QString::number(pct,glob->formats.PercentageFmt.format.toAscii(),glob->formats.PercentageFmt.precision)+QString(" %"));
  }
}


void MultiComponentItemFrm::McWeightCapChanged(float cap, float capTotal, float capPctTotal, int idx)
/*---------------------------*/
/* McWeight capacity changed */
/*---------------------------*/
{
  if (idx == unitIdx) {
    ui->colPct->setText(QString::number(glob->sysSts[unitIdx].mcWeightSts.mcWeightPct)+"%");
  }
}


void MultiComponentItemFrm::SysSts_DosActChanged(float _dos,int idx)
/*-----------------------*/
/* Actual dosage changed */
/*-----------------------*/
{
  QString units;
  if (idx == unitIdx) {
  }
}


void MultiComponentItemFrm::SysCfg_DosSetChanged(float dos,int idx)
/*------------------------*/
/* Dosage setting changed */
/*------------------------*/
{
  QString units;
  if (idx == unitIdx) {
  }
}


void MultiComponentItemFrm::SysActiveChanged(bool,int)
/*------------------------------*/
/* System active status changed */
/*------------------------------*/
{
  UpdateUnitActiveSts();
}


void MultiComponentItemFrm::ValueReturned(float value)
{
  /*---------------------------------*/
  /* Numeric keyboard value returned */
  /*---------------------------------*/
  if (unitIdx != -1) {
    glob->SysCfg_SetColPct(value,unitIdx);
    ui->colPct->setText(QString::number(glob->sysCfg[unitIdx].colPct,glob->formats.PercentageFmt.format.toAscii(),glob->formats.PercentageFmt.precision)+QString(" %"));

    /*----- Set the recipe changed icon -----*/
    ((MainMenu*)(glob->menu))->prodForm->SetRecipeDataChanged();
  }
}


void MultiComponentItemFrm::SelectionReceived(int _type, int _idx) {
  /*--------------------------------*/
  /* Unit config selection received */
  /*--------------------------------*/

  /*----- Setup device type -----*/
  if (_type == itDeviceType) {
    if (glob->sysCfg[unitIdx].deviceType != (eMcDeviceType)_idx) {
      glob->sysCfg[unitIdx].deviceType = (eMcDeviceType)_idx;           /* Update unit type */
      SetupDisplay((eMcDeviceType)_idx);                                /* Update button image */
      ((MainMenu*)(glob->menu))->StoreSystemSettings(unitIdx);          /* Save settings */
    }
  }
}

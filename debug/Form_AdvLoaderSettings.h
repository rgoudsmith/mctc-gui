#ifndef ADVLOADERSETTINGSFRM_H
#define ADVLOADERSETTINGSFRM_H

#include <QtGui/QWidget>
#include "Form_QbaseWidget.h"
#include "Form_NumericInput.h"
#include "Form_Selection.h"
#include "ImageDef.h"

namespace Ui
{
  class AdvLoaderSettingsFrm;
}


class AdvLoaderSettingsFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    AdvLoaderSettingsFrm(QWidget *parent = 0);
    ~AdvLoaderSettingsFrm();
    void FormInit();
    void SetAlarmMode(bool);
    void SetFillSystem();

  protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *e);
    void showEvent(QShowEvent *e);
    bool eventFilter(QObject *,QEvent *);

    NumericInputFrm * numInput;

  private:
    int cfgDecimals;
    float cfgMinInput;
    float cfgMaxInput;
    selectionFrm * selForm;

    Ui::AdvLoaderSettingsFrm *m_ui;
    int lineIdx;
    FillSysStruct fillSys;
    void DisplayAlarmMode();
    void LanguageUpdate();
    //void UpdateValues();
    void DisplayValues();
    //void LoadSettings();

  private slots:
    void ValueReturned(float);
    void OkClicked();
    void CancelClicked();

  public slots:
    void AlarmModeClicked();
    void SelectionReceived(int,int);
    void CfgFill_AlarmTimeChanged(float, int);
    void CfgFill_TimeMeChanged(float, int);
    void CfgFill_TimeMvChanged(float, int);
    void CfgFill_EmptyTimeChanged(float, int);
    void CfgFill_AlarmModeChanged(bool, int);
    /*
    signals:
        void CfgFill_SetAlarmTime(float,int);
        void CfgFill_SetTimeMe(float,int);
        void CfgFill_SetTimeMv(float,int);
        void CfgFill_SetEmptyTime(float,int);
        void CfgFill_SetAlarmMode(bool,int);
    */
};
#endif                                                                          // ADVLOADERSETTINGSFRM_H

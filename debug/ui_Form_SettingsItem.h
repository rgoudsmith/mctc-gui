/********************************************************************************
** Form generated from reading UI file 'Form_SettingsItem.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_SETTINGSITEM_H
#define UI_FORM_SETTINGSITEM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SettingsItemFrm
{
public:
    QHBoxLayout *horizontalLayout;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_2;
    QLabel *lbItem1;
    QPushButton *btSetting1;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_3;
    QLabel *lbItem2;
    QPushButton *btSetting2;

    void setupUi(QWidget *SettingsItemFrm)
    {
        if (SettingsItemFrm->objectName().isEmpty())
            SettingsItemFrm->setObjectName(QString::fromUtf8("SettingsItemFrm"));
        SettingsItemFrm->resize(492, 80);
        SettingsItemFrm->setCursor(QCursor(Qt::BlankCursor));
        SettingsItemFrm->setWindowTitle(QString::fromUtf8("Form"));
        horizontalLayout = new QHBoxLayout(SettingsItemFrm);
        horizontalLayout->setSpacing(2);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(25);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(-1, 5, 5, 5);
        horizontalSpacer_2 = new QSpacerItem(1, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        lbItem1 = new QLabel(SettingsItemFrm);
        lbItem1->setObjectName(QString::fromUtf8("lbItem1"));
        lbItem1->setMinimumSize(QSize(0, 0));
        lbItem1->setMaximumSize(QSize(16777215, 16777215));
        lbItem1->setText(QString::fromUtf8("Item 1"));

        horizontalLayout_3->addWidget(lbItem1);

        btSetting1 = new QPushButton(SettingsItemFrm);
        btSetting1->setObjectName(QString::fromUtf8("btSetting1"));
        btSetting1->setMinimumSize(QSize(90, 70));
        btSetting1->setMaximumSize(QSize(90, 70));
        btSetting1->setText(QString::fromUtf8("Setting1"));

        horizontalLayout_3->addWidget(btSetting1);


        horizontalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(25);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(-1, 5, 5, 5);
        horizontalSpacer_3 = new QSpacerItem(1, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);

        lbItem2 = new QLabel(SettingsItemFrm);
        lbItem2->setObjectName(QString::fromUtf8("lbItem2"));
        lbItem2->setText(QString::fromUtf8("Item 2"));

        horizontalLayout_4->addWidget(lbItem2);

        btSetting2 = new QPushButton(SettingsItemFrm);
        btSetting2->setObjectName(QString::fromUtf8("btSetting2"));
        btSetting2->setMinimumSize(QSize(90, 70));
        btSetting2->setMaximumSize(QSize(90, 70));
        btSetting2->setText(QString::fromUtf8("Setting2"));

        horizontalLayout_4->addWidget(btSetting2);


        horizontalLayout->addLayout(horizontalLayout_4);


        retranslateUi(SettingsItemFrm);

        QMetaObject::connectSlotsByName(SettingsItemFrm);
    } // setupUi

    void retranslateUi(QWidget *SettingsItemFrm)
    {
        Q_UNUSED(SettingsItemFrm);
    } // retranslateUi

};

namespace Ui {
    class SettingsItemFrm: public Ui_SettingsItemFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_SETTINGSITEM_H

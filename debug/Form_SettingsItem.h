#ifndef SETTINGSITEMFRM_H
#define SETTINGSITEMFRM_H

#include "Form_QbaseWidget.h"
#include "Form_NumericInput.h"

struct TItemValue
{
  int type;                                                                     // 0 = int, 1 = float, 2 = string.
  float value;
  float min;
  float max;
  float minInc;
  int decimals;
  QString text;
};

namespace Ui
{
  class SettingsItemFrm;
}


class SettingsItemFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit SettingsItemFrm(QWidget *parent = 0);
    ~SettingsItemFrm();
    void SetUniqueID(int);
    void SetInput(NumericInputFrm *);
    void FormInit();

  protected:
    void Item1Value();
    void Item2Value();

  private:
    Ui::SettingsItemFrm *ui;
    NumericInputFrm * numInput;
    int itemID;
    int fieldNr;
    TItemValue item1;
    TItemValue item2;

  public slots:
    void ShowOneItem(bool);

    void SetItem1Title(const QString &);
    void SetItem1(int, int, int, int);
    void SetItem1(float, float, float, float, int);
    void SetItem1(const QString &);

    void SetItem2Title(const QString &);
    void SetItem2(int, int, int, int);
    void SetItem2(float, float, float, float, int);
    void SetItem2(const QString &);

    void Item1Clicked();
    void Item2Clicked();

    void ValueReceived(float,int);

    signals:
    void SetValueItem1(float);
    void SetValueItem1(const QString &);
    void SetValueItem2(float);
    void SetValueItem2(const QString &);
};
#endif                                                                          // SETTINGSITEMFRM_H

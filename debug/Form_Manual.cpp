/*=========================*/
/* Manual I/O control form */
/*=========================*/

#include "Form_Manual.h"
#include "ui_Form_Manual.h"
#include "Form_MainMenu.h"
#include "Rout.h"

/*----- Global variables -----*/
bool mirrorInput[CFG_MAX_UNIT_COUNT][MAX_DIG_IN];      /* Inpput mirror registers */
bool mirrorOutput[CFG_MAX_UNIT_COUNT][MAX_DIG_OUT];      /* Output mirror registers */

ManualFrm::ManualFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::ManualFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);
  numInput = 0;

  numInput = new NumericInputFrm();
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
  numInput->hide();

  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(ui->btOutput1,SIGNAL(clicked()),this,SLOT(Output1Clicked()));
  connect(ui->btOutput2,SIGNAL(clicked()),this,SLOT(Output2Clicked()));
  connect(ui->btOutput3,SIGNAL(clicked()),this,SLOT(Output3Clicked()));
  connect(ui->btOutput4,SIGNAL(clicked()),this,SLOT(Output4Clicked()));
  connect(ui->btMode,SIGNAL(clicked()),this,SLOT(ManModeClicked()));

  ui->edRpm->installEventFilter(this);
}


ManualFrm::~ManualFrm()
{
  delete numInput;
  delete ui;
}


void ManualFrm::showEvent(QShowEvent *e)
{
  /*------------------------------------------*/
  /* Show window event handler                */
  /* The Manual I/O screen has become active. */
  /*------------------------------------------*/
  float val;
  QString txt;

  QWidget::showEvent(e);

  /*----- Fetch actual manual motorspeed value -----*/
//  val = glob->sysSts[glob->ActUnitIndex()].manIO.motorSpeed;
//  txt = QString::number(val,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision) + QString(" ") + glob->rpmUnits;
//  ui->edRpm->setText(txt);

  /*----- Setup actual status for the outputs -----*/
  for (int i=0; i<MAX_DIG_OUT; i++) {
    SetOutputStatus(i,glob->ActUnitIndex());
  }
  /*----- Setup actual status for the inputs -----*/
  for (int i=0; i<MAX_DIG_IN; i++) {
    SetInputStatus(i,glob->ActUnitIndex());
  }

  /*----- Enable update of inputs on status change -----*/
  SetupMirrorInputs();

  /*----- Set Man IO status -----*/
  DisplayManualMode();

  /*----- Connect I/O update to message received -----*/
  connect(glob,SIGNAL(ComReceived(tMsgControlUnit,int)),this,SLOT(ComMsgReceived(tMsgControlUnit,int)));
}


void ManualFrm::ResetManualIoMode(void)
{
  /*---------------------------*/
  /* Reset the manual I/O mode */
  /*---------------------------*/

  /*----- TWIN mode -----*/
  if (glob->mstSts.isTwin) {
    glob->sysSts[0].manIO.active = false;
    glob->sysSts[1].manIO.active = false;
  }
  else {
    glob->sysSts[glob->ActUnitIndex()].manIO.active = false;
  }
}


void ManualFrm::hideEvent(QHideEvent *e)
{
  /*---------------------------------------------*/
  /* Hide window event handler                   */
  /* The Manual I/O screen has become in-active. */
  /*---------------------------------------------*/
  QWidget::hideEvent(e);

  /*----- Disconnect various I/O events -----*/
  disconnect(glob,SIGNAL(ComReceived(tMsgControlUnit,int)),this,SLOT(ComMsgReceived(tMsgControlUnit,int)));

  ResetManualIoMode();

  // TEST TEST TEST TEST
  //  OkClicked();                              /* Disable manual I/O mode */
}


bool ManualFrm::eventFilter(QObject *o, QEvent *e)
{
  if (e->type() == QEvent::MouseButtonPress) {
    if (o == ui->edRpm) {
      if (numInput != 0) {
        float val = glob->sysSts[glob->ActUnitIndex()].manIO.motorSpeed;
        numInput->SetDisplay(true,1,val,0,glob->sysCfg[glob->ActUnitIndex()].MvcSettings.agent.maxMotorSpeed.actVal);
        KeyBeep();
        numInput->show();
      }
    }
  }
  return QBaseWidget::eventFilter(o,e);
}


void ManualFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;
  if (numInput != 0) {
    numInput->FormInit();
    connect(numInput,SIGNAL(InputValue(float)),this,SLOT(ValueReceived(float)));
  }

  glob->SetButtonImage(ui->btOk,itSelection,selOk);
  glob->SetButtonImage(ui->btOutput1,itGeneral,genUnitOff,0);
  glob->SetButtonImage(ui->btOutput2,itGeneral,genUnitOff,0);
  glob->SetButtonImage(ui->btOutput3,itGeneral,genUnitOff,0);
  glob->SetButtonImage(ui->btOutput4,itGeneral,genUnitOff,0);

  for (int i=0; i<MAX_DIG_OUT; i++) {
    SetOutputStatus(i,glob->ActUnitIndex());
  }
  glob->SetButtonImage(ui->btMode,itGeneral,genUnitOff,0);
  glob->SetLabelImage(ui->lbImg,itGeneral,genLedRed);
  ui->lbSts->setText(tr("OFF"));

  SetManualMode(false,glob->ActUnitIndex());
  SetOutput(false,0,glob->ActUnitIndex());
  SetOutput(false,1,glob->ActUnitIndex());
  SetOutput(false,2,glob->ActUnitIndex());
  SetOutput(false,3,glob->ActUnitIndex());

  ui->lbSts->setFont(*(glob->baseFont));

  ui->lbInp1Img->setFont(*(glob->baseFont));
  ui->lbInp1Sts->setFont(*(glob->baseFont));
  ui->lbInp2Img->setFont(*(glob->baseFont));
  ui->lbInp2Sts->setFont(*(glob->baseFont));
  ui->lbInp3Img->setFont(*(glob->baseFont));
  ui->lbInp3Sts->setFont(*(glob->baseFont));
  ui->lbInp4Img->setFont(*(glob->baseFont));
  ui->lbInp4Sts->setFont(*(glob->baseFont));

  ui->groupBox->setFont(*(glob->baseFont));
  ui->groupBox_2->setFont(*(glob->baseFont));
  ui->groupBox_3->setFont(*(glob->baseFont));
  ui->groupBox_4->setFont(*(glob->baseFont));
  ui->groupBox_5->setFont(*(glob->baseFont));
  ui->groupBox_6->setFont(*(glob->baseFont));
  ui->groupBox_7->setFont(*(glob->baseFont));
  ui->groupBox_8->setFont(*(glob->baseFont));
  ui->groupBox_9->setFont(*(glob->baseFont));
  ui->groupBox_10->setFont(*(glob->baseFont));

  ui->label->setFont(*(glob->baseFont));
  ui->label_2->setFont(*(glob->baseFont));
  ui->label_7->setFont(*(glob->baseFont));
  ui->label_13->setFont(*(glob->baseFont));
  ui->unitName->setFont(*(glob->baseFont));

  ui->lbWeight->setFont(*(glob->baseFont));
  ui->lbTacho->setFont(*(glob->baseFont));

  ui->DeviceId->setFont(*(glob->baseFont));

  ui->edRpm->setFont(*(glob->baseFont));
  SetRpmText(0,glob->ActUnitIndex());
  SetTacho(0,glob->ActUnitIndex());
}


void ManualFrm::SetManualMode(bool active,int idx)
{
  if (active != glob->sysSts[idx].manIO.active) {
    glob->sysSts[idx].manIO.active = active;
  }
}


void ManualFrm::DisplayManualMode()
{
  if (glob->sysSts[glob->ActUnitIndex()].manIO.active) {
    glob->SetButtonImage(ui->btMode,itGeneral,genUnitOn,0);
    glob->SetLabelImage(ui->lbImg,itGeneral,genLedGreen);
    ui->lbSts->setText(tr("ON"));
  }
  else {
    glob->SetButtonImage(ui->btMode,itGeneral,genUnitOff,0);
    glob->SetLabelImage(ui->lbImg,itGeneral,genLedRed);
    ui->lbSts->setText(tr("OFF"));
  }
}


void ManualFrm::DisableOutputs(int idx)
{
  for (int i=0; i<MAX_DIG_OUT; i++) {
    glob->sysSts[idx].manIO.outputs[i] = false;
  }
}


void ManualFrm::SetOutput(bool act,int io,int idx)
{
  if (glob->sysSts[idx].manIO.outputs[io] != act) {
    glob->sysSts[idx].manIO.outputs[io] = act;
  }
}


void ManualFrm::ToggleOutput(int io, int idx)
{
  if (glob->sysSts[idx].manIO.outputs[io] == true) {
    SetOutput(false,io,idx);
  }
  else {
    SetOutput(true,io,idx);
  }
}


void ManualFrm::SetRPM(float rpm,int idx)
{
  if (glob->sysSts[idx].manIO.motorSpeed != rpm) {
    glob->sysSts[idx].manIO.motorSpeed = rpm;
    SetRpmText(rpm,idx);
  }
}


void ManualFrm::SetRpmText(float val, int)
{
  QString txt;
  txt = QString::number(val,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision) + QString(" ") + glob->rpmUnits;
  ui->edRpm->setText(txt);
}


void ManualFrm::SetWeight(float val, int)
{
  QString txt;
  txt = QString::number(val,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision) + QString(" ") + glob->weightUnits;
  ui->lbWeight->setText(txt);
}


void ManualFrm::SetTacho(float val, int)
{
  QString txt;
  txt = QString::number(val,glob->formats.TachoFmt.format.toAscii(),glob->formats.TachoFmt.precision)+QString(" ") + glob->tachoUnits;
  ui->lbTacho->setText(txt);
}


/*========== Slots ==========*/

void ManualFrm::ValueReceived(float val)
{
  SetRPM(val,glob->ActUnitIndex());
}


void ManualFrm::SetButtonImage(QPushButton *btn, int io, int idx)
{
  bool act;
  bool manAct;

  if ((io >= 0) && (io < MAX_DIG_OUT)) {

    manAct = glob->sysSts[idx].manIO.active;

    if (manAct) {
      act = glob->sysSts[idx].manIO.outputs[io];  /* Read manual output status */
    }
    else {
      act = glob->sysSts[idx].actIO.outputs[io];  /* Read normal output status */
    }

    if (act) {
      glob->SetButtonImage(btn,itGeneral,genUnitOn,0);
    }
    else {
      glob->SetButtonImage(btn,itGeneral,genUnitOff,0);
    }
  }
}


void ManualFrm::SetLabelOutputImage(QLabel *lbl, int io, int idx)
{
  bool act;
  bool manAct;

  if ((io >= 0) && (io < MAX_DIG_OUT)) {

    manAct = glob->sysSts[idx].manIO.active;

    if (manAct) {
      act = glob->sysSts[idx].manIO.outputs[io];  /* Read manual output status */
    }
    else {
      act = glob->sysSts[idx].actIO.outputs[io];  /* Read normal output status */
    }

    if (act) {
      glob->SetLabelImage(lbl,itGeneral,genLedGreen);
    }
    else {
      glob->SetLabelImage(lbl,itGeneral,genLedRed);
    }
  }
}


void ManualFrm::SetLabelOutputStatus(QLabel *lbl, int io, int idx)
{
  bool act;
  bool manAct;

  if ((io >= 0) && (io < MAX_DIG_OUT)) {

    manAct = glob->sysSts[idx].manIO.active;

    if (manAct) {
      act = glob->sysSts[idx].manIO.outputs[io];  /* Read manual output status */
    }
    else {
      act = glob->sysSts[idx].actIO.outputs[io];  /* Read normal output status */
    }

    if (act) {
      lbl->setText(tr("ON"));
    }
    else {
      lbl->setText(tr("OFF"));
    }
  }
}


void ManualFrm::SetOutputStatus(int io, int idx)
{
  if (io < MAX_DIG_OUT) {
    switch(io) {
    case 0:
      SetButtonImage(ui->btOutput1,io,idx);
      SetLabelOutputImage(ui->lbOut1Img,io,idx);
      SetLabelOutputStatus(ui->lbOut1Sts,io,idx);
      break;
    case 1:
      SetButtonImage(ui->btOutput2,io,idx);
      SetLabelOutputImage(ui->lbOut2Img,io,idx);
      SetLabelOutputStatus(ui->lbOut2Sts,io,idx);
      break;
    case 2:
      SetButtonImage(ui->btOutput3,io,idx);
      SetLabelOutputImage(ui->lbOut3Img,io,idx);
      SetLabelOutputStatus(ui->lbOut3Sts,io,idx);
      break;
    case 3:
      SetButtonImage(ui->btOutput4,io,idx);
      SetLabelOutputImage(ui->lbOut4Img,io,idx);
      SetLabelOutputStatus(ui->lbOut4Sts,io,idx);
      break;
    }
  }
}


void ManualFrm::SetLabelInputImage(QLabel *lbl, int io, int idx)
{
  bool act;

  if ((io >= 0) && (io < MAX_DIG_IN)) {
    act = glob->sysSts[idx].actIO.inputs[io];

    if (act) {
      glob->SetLabelImage(lbl,itGeneral,genLedGreen);
    }
    else {
      glob->SetLabelImage(lbl,itGeneral,genLedRed);
    }
  }
}


void ManualFrm::SetLabelInputStatus(QLabel *lbl, int io, int idx)
{
  bool act;

  if ((io >= 0) && (io < MAX_DIG_IN)) {
    act = glob->sysSts[idx].actIO.inputs[io];

    if (act) {
      lbl->setText(tr("ON"));
    }
    else {
      lbl->setText(tr("OFF"));
    }
  }
}


void ManualFrm::SetInputStatus(int io, int idx)
{
  if (io < MAX_DIG_OUT) {
    switch(io) {
    case 0:
      SetLabelInputImage(ui->lbInp1Img,io,idx);
      SetLabelInputStatus(ui->lbInp1Sts,io,idx);
      break;
    case 1:
      SetLabelInputImage(ui->lbInp2Img,io,idx);
      SetLabelInputStatus(ui->lbInp2Sts,io,idx);
      break;
    case 2:
      SetLabelInputImage(ui->lbInp3Img,io,idx);
      SetLabelInputStatus(ui->lbInp3Sts,io,idx);
      break;
    case 3:
      SetLabelInputImage(ui->lbInp4Img,io,idx);
      SetLabelInputStatus(ui->lbInp4Sts,io,idx);
      break;
    }
  }
}


void ManualFrm::OkClicked()
{
  /*-------------------------*/
  /* OK button clicked       */
  /* Disable manual IO mode  */
  /*-------------------------*/

  /*----- Reset manual I/O mode -----*/
  ResetManualIoMode();

  /*----- Close the window -----*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiBack);
}


void ManualFrm::Output1Clicked()
{
  int idx = glob->ActUnitIndex();
  ToggleOutput(0,idx);
  SetOutputStatus(0,idx);
}


void ManualFrm::Output2Clicked()
{
  int idx = glob->ActUnitIndex();
  ToggleOutput(1,idx);
  SetOutputStatus(1,idx);
}


void ManualFrm::Output3Clicked()
{
  int idx = glob->ActUnitIndex();
  ToggleOutput(2,idx);
  SetOutputStatus(2,idx);
}


void ManualFrm::Output4Clicked()
{
  int idx = glob->ActUnitIndex();
  ToggleOutput(3,idx);
  SetOutputStatus(3,idx);
}


void ManualFrm::ManModeClicked()
{
  /*----------------------------*/
  /* Manual mode button pressed */
  /*----------------------------*/
  /*----- Toggle manual I/O mode -----*/
  if (glob->sysSts[glob->ActUnitIndex()].manIO.active) {
    SetManualMode(false,glob->ActUnitIndex());                                  /* Off */
  }
  else {
    SetManualMode(true,glob->ActUnitIndex());                                   /* On */
  }

  DisplayManualMode();

  /*----- Update actual status of outputs -----*/
  for (int i=0; i<MAX_DIG_OUT; i++) {
    SetOutputStatus(i,glob->ActUnitIndex());
  }
}


void ManualFrm::DisplayIO(int idx)
{
  /*------------------------------------------------------*/
  /* Update the status of the digital I/O on when changed */
  /*------------------------------------------------------*/
  float val;

  for (int i=0; i<MAX_DIG_IN; i++) {
    /*----- Detect change -----*/
    if(glob->sysSts[idx].actIO.inputs[i] != mirrorInput[idx][i]) {
      mirrorInput[idx][i] = glob->sysSts[idx].actIO.inputs[i];
      SetInputStatus(i,glob->ActUnitIndex());
    }
  }

  for (int i=0; i<MAX_DIG_OUT; i++) {
    /*----- Detect change -----*/
    if(glob->sysSts[idx].actIO.outputs[i] != mirrorOutput[idx][i]) {
      mirrorOutput[idx][i] = glob->sysSts[idx].actIO.outputs[i];
      SetOutputStatus(i,glob->ActUnitIndex());
    }
  }

  /*----- Update motor speed -----*/
  if (glob->sysSts[idx].manIO.active) {
    /*----- Manual mode speed -----*/
    val = glob->sysSts[glob->ActUnitIndex()].manIO.motorSpeed;
  }
  else {
    /*----- Normal mode speed -----*/
    val = glob->sysSts[glob->ActUnitIndex()].rpm;
  }
  ui->edRpm->setText(QString::number(val,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision) + QString(" ") + glob->rpmUnits);
}



void ManualFrm::SetupMirrorInputs()
{
  /*--------------------------------------------------------------------------*/
  /* Setup the mirror input registers reverse to status of the digital inputs */
  /*--------------------------------------------------------------------------*/
  for (int idx=0; idx<CFG_MAX_UNIT_COUNT; idx++) {

    for (int i=0; i<MAX_DIG_IN; i++) {
      /*----- Setup reverse mirror status to enable first update -----*/
      mirrorInput[idx][i] = !glob->sysSts[idx].actIO.inputs[i];
    }

    for (int i=0; i<MAX_DIG_OUT; i++) {
      /*----- Setup reverse mirror status to enable first update -----*/
      mirrorOutput[idx][i] = !glob->sysSts[idx].actIO.outputs[i];
    }

  }
}


void ManualFrm::ComMsgReceived(tMsgControlUnit data,int idx)
{
  /*----------------------------------------------------------*/
  /* Update the status of the Inputs on the manual I/O screen */
  /*----------------------------------------------------------*/
  /* Connected to signal ComReceived                          */
  /*----------------------------------------------------------*/
  if (idx == glob->ActUnitIndex()) {

    /*----- Update I/O pictures -----*/
    DisplayIO(idx);

    /*----- Update weight -----*/
    SetWeight(glob->sysSts[idx].mass,idx);

    /*----- Update tacho -----*/
    //    SetTacho(glob->sysSts[idx].extrusion.tachoAct,idx);
    SetTacho(data.ioData.tacho,idx);

    ui->unitName->setText(glob->sysCfg[idx].MC_ID);
  }
}

/*===========================================================================*
 * File        : McServer.cpp                                                *
 * Project     : MVC0012 - MC-TC                                             *
 * Description : McLan TCP/IP server for McLan.                              *
 *===========================================================================*/

#include "McServer.h"
#include <iostream>
#include "Common.h"
#include "Glob.h"
#include "Rout.h"

McServer::McServer(QObject* parent): QObject(parent)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  connect(&server, SIGNAL(newConnection()), this, SLOT(NewConnection()));

  /*----- Listen to McLan port -----*/
  server.listen(QHostAddress::Any, 30021);
}


McServer::~McServer()
{
  /*------------*/
  /* Destructor */
  /*------------*/
  server.close();
}


void McServer::FormInit()
{
  /*-----------*/
  /* Init form */
  /*-----------*/
}


void McServer::NewConnection()
{
  /*---------------------*/
  /* Setup client socket */
  /*---------------------*/
  client = server.nextPendingConnection();

  /*----- Connect readyReady to TCP Read handler -----*/
  connect(client, SIGNAL(readyRead()), this, SLOT(TcpRead()));
  connect(client, SIGNAL(disconnected()), this, SLOT(TcpDisConnected()));
}


void McServer::TcpRead()
{
  /*-----------------------*/
  /* TCP Read data handler */
  /*-----------------------*/
  char rxBuf[10];
  tTcpIpMsg *p;

  QTcpSocket * socket = qobject_cast<QTcpSocket *>(sender());
  if (socket == 0) return;

  /*----- Read data from client -----*/
  if (socket->bytesAvailable() <= sizeof(rxBuf)) {                              /* Check message length */
    socket->read(rxBuf, socket->bytesAvailable());                              /* Message data to rxBuf */

    /*----- Fetch command data -----*/
    p = (tTcpIpMsg*)&rxBuf;

    switch (p->groupNr) {

      /*========== Group 0 parameters ==========*/
      case(0):
        switch(p->paramNr) {

          /*----- P0.0 = Fill system mode -----*/
          case(0) :
            p->value  = glob->sysCfg[0].fillSystem.fillSysType;
            break;

            /*---- P0.1 = Fill on/off -----*/
          case(1) :
            p->value  = glob->sysCfg[0].fillSystem.fillOnOff;
            break;

            /*------ Parm not implemented -----*/
          default :
            qDebug() << ">>>>>>>>>> Msg Error group 0 <<<<<<<<<< " <<  p->paramNr;
            break;
        }
        break;

        /*========== Group 1 parameters ==========*/
      case(1) :

        switch(p->paramNr) {

          case(0) :
            /*----- P1.0 = Production status -----*/

            /* Convert MC-TC status codes to MC-30 status codes as used by MC-LAN */
            if (glob->sysSts[0].MCStatus >= 1) {
              p->value = glob->sysSts[0].MCStatus-1;
            }
            else {
              p->value = 0;
            }
            break;

            /*----- P1.1 = Alarm nr. -----*/
          case(1) :
            /*----- Convert alarm code to MC-30 alarm code -----*/
            p->value = Rout::GetMc30AlarmCode(glob->sysSts[0].alarmSts.nr);
            break;

            /*----- P1.2 = Tacho act -----*/
          case(2) :
            p->value =glob->sysSts[0].extrusion.tachoAct;
            break;

            /*----- P1.3 = Act motor rpm -----*/
          case(3) :
            p->value  = glob->sysSts[0].rpm;
            break;

          case(4) :
            /*----- P1.4 = Weight -----*/
            p->value = glob->sysSts[0].mass;
            break;

          case(5) :
            /*----- P1.5 = Col.cap. set -----*/
            p->value = glob->sysSts[0].setProd;
            break;

            /*----- P1.6 = Dos time act -----*/
          case(6) :
            /*---- Return the Set dos time as act dos time in INJ / TIMER mode -----*/
            if ((glob->mstCfg.mode == modInject) && (glob->mstCfg.inpType == ttTimer)) {
              p->value = glob->sysCfg[0].injection.shotTime*10;                 /* Set dos time */
            }
            else {
              p->value =glob->sysSts[0].injection.meteringAct*10;               /* Act dos time */
            }
            break;

            /*----- P1.7 = Fill valve open/closed -----*/
          case(7) :
        #warning Uitzoeken
            p->value = 0;
            break;

            /*----- P1.8 = Col.cap. act -----*/
          case(8) :
            p->value = glob->sysSts[0].actProd;
            break;

            /*----- P1.9 = Col.consumption -----*/
          case(9) :
            p->value = 25000;
            break;

            /*-----  P1.11 = Col. day consumption -----*/
          case(11) :
            p->value = 0;
            break;

            /*------ Parm not implemented -----*/
          default :
            qDebug() << ">>>>>>>>>> Msg Error group 1 <<<<<<<<<< " <<  p->paramNr;
            break;
        }
        break;

        /*========== Group 2 parameters ==========*/
      case(2) :
        switch(p->paramNr) {

          /*----- P 2.0 = Consumption reset -----*/
          case(0) :
            p->value = 0;
            break;

            /*----- P 2.1 = Color pct set act -----*/
          case(1) :
            p->value = glob->sysCfg[0].colPct;
            break;

            /*----- P2.2 = Shot weight -----*/
          case(2) :
            p->value = glob->sysCfg[0].injection.shotWeight;
            break;

            /*----- P2.3 = Dos time set -----*/
          case(3) :
            p->value = glob->sysCfg[0].injection.shotTime;
            break;

            /*----- P2.4 = RPM set -----*/
          case(4) :
            p->value = glob->sysCfg[0].setRpm;
            break;

            /*----- P2.5 = Extruder cap. set kg/h  -----*/
          case(5) :
            p->value = glob->sysCfg[0].extrusion.extCapacity;
            break;

            /*----- P2.6 = Tacho max  -----*/
          case(6) :
            p->value = glob->sysCfg[0].extrusion.tachoMax;
            break;

            /*----- P2.8 = Twin - Regrind Pct Set -----*/
          case(8) :
            p->value = 0;
            break;

            /*----- P2.9 = Reset balance regeling -----*/
          case(9) :
            p->value = 0;
            break;

            /*----- P2.11 = Reset alarm -----*/
          case(11) :
            p->value = 0;
            break;

            /*------ Parm not implemented -----*/
          default :
            qDebug() << ">>>>>>>>>> Msg Error group 2 <<<<<<<<<< " <<  p->paramNr;
            break;
        }
        break;

        /*========== Group 3 parameters ==========*/
      case(3) :
        switch(p->paramNr) {

          /*----- P3.16 = Control mode -----*/
          case(16) :
            p->value = glob->sysCfg[0].GraviRpm;
            break;

            /*----- P3.17 = Production type (Inj/Ext) -----*/
          case(17) :
            p->value = glob->mstCfg.mode;
            break;

            /*----- P3.18 = Production extrusion mode (Relay/Tacho) -----*/
          case(18) :
            if (glob->mstCfg.inpType == ttRelay) {
              p->value = 0;                                                     /* MC30 ext.relay mode */
            }
            else {
              p->value = 1;                                                     /* MC30 ext.tacho mode */
            }
            break;

            /*----- Group 3.19 = Production injection moulding mode (Relay/Timer) -----*/
          case(19) :
            if (glob->mstCfg.inpType == ttTimer) {
              p->value = 0;                                                     /* MC30 inj.timer mode */
            }
            else {
              p->value = 1;                                                     /* MC30 inj.relay mode */
            }
            break;

            /*------ Parm not implemented -----*/
          default :
            qDebug() << ">>>>>>>>>> Msg Error group 3 <<<<<<<<<< " <<  p->paramNr;
            break;
        }
        break;
    }

    /*----- Send reply message -----*/
    socket->write(rxBuf,sizeof(tTcpIpMsg));
  }
}


void McServer::TcpDisConnected()
{
  /*------------------------*/
  /* TCP Disconnect handler */
  /*------------------------ p->value = */
  QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());

  if (socket == 0) return;

  //  client->removeOne(socket);
  socket->deleteLater();
}

/********************************************************************************
** Form generated from reading UI file 'Form_Tolerances.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_TOLERANCES_H
#define UI_FORM_TOLERANCES_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TolerancesFrm
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *lbCaption;
    QSpacerItem *verticalSpacer_2;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *edCalDev;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *edDevAlm;
    QLabel *lbCalibDev;
    QLabel *lbDevAlarm;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *horizontalSpacer_7;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *horizontalSpacer_8;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btOk;
    QPushButton *btCancel;

    void setupUi(QWidget *TolerancesFrm)
    {
        if (TolerancesFrm->objectName().isEmpty())
            TolerancesFrm->setObjectName(QString::fromUtf8("TolerancesFrm"));
        TolerancesFrm->resize(640, 503);
        TolerancesFrm->setCursor(QCursor(Qt::BlankCursor));
        TolerancesFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout = new QVBoxLayout(TolerancesFrm);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lbCaption = new QLabel(TolerancesFrm);
        lbCaption->setObjectName(QString::fromUtf8("lbCaption"));
        QFont font;
        font.setPointSize(15);
        lbCaption->setFont(font);
        lbCaption->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lbCaption);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(9);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(-1, -1, -1, 0);
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        edCalDev = new QLineEdit(TolerancesFrm);
        edCalDev->setObjectName(QString::fromUtf8("edCalDev"));
        edCalDev->setMinimumSize(QSize(95, 60));
        edCalDev->setMaximumSize(QSize(95, 60));
        edCalDev->setFont(font);
        edCalDev->setCursor(QCursor(Qt::BlankCursor));
        edCalDev->setFocusPolicy(Qt::NoFocus);
        edCalDev->setText(QString::fromUtf8("0"));
        edCalDev->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(edCalDev);


        gridLayout->addLayout(horizontalLayout_3, 1, 4, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        edDevAlm = new QLineEdit(TolerancesFrm);
        edDevAlm->setObjectName(QString::fromUtf8("edDevAlm"));
        edDevAlm->setMinimumSize(QSize(95, 60));
        edDevAlm->setMaximumSize(QSize(95, 60));
        edDevAlm->setFont(font);
        edDevAlm->setCursor(QCursor(Qt::BlankCursor));
        edDevAlm->setFocusPolicy(Qt::NoFocus);
        edDevAlm->setText(QString::fromUtf8("0"));
        edDevAlm->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(edDevAlm);


        gridLayout->addLayout(horizontalLayout_4, 0, 4, 1, 1);

        lbCalibDev = new QLabel(TolerancesFrm);
        lbCalibDev->setObjectName(QString::fromUtf8("lbCalibDev"));
        lbCalibDev->setFont(font);
        lbCalibDev->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(lbCalibDev, 1, 3, 1, 1);

        lbDevAlarm = new QLabel(TolerancesFrm);
        lbDevAlarm->setObjectName(QString::fromUtf8("lbDevAlarm"));
        lbDevAlarm->setFont(font);
        lbDevAlarm->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(lbDevAlarm, 0, 3, 1, 1);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_5, 1, 2, 1, 1);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_7, 0, 2, 1, 1);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_6, 1, 5, 1, 1);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_8, 0, 5, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btOk = new QPushButton(TolerancesFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout->addWidget(btOk);

        btCancel = new QPushButton(TolerancesFrm);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("CANCEL"));

        horizontalLayout->addWidget(btCancel);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(TolerancesFrm);

        QMetaObject::connectSlotsByName(TolerancesFrm);
    } // setupUi

    void retranslateUi(QWidget *TolerancesFrm)
    {
        lbCaption->setText(QApplication::translate("TolerancesFrm", "Tolerance Settings", "Form Title", QApplication::UnicodeUTF8));
        edCalDev->setInputMask(QString());
        lbCalibDev->setText(QApplication::translate("TolerancesFrm", "Calibration Deviation:", "label (Calibration Deviation)", QApplication::UnicodeUTF8));
        lbDevAlarm->setText(QApplication::translate("TolerancesFrm", "Deviation Alarm:", "label (Deviation Alarm)", QApplication::UnicodeUTF8));
        Q_UNUSED(TolerancesFrm);
    } // retranslateUi

};

namespace Ui {
    class TolerancesFrm: public Ui_TolerancesFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_TOLERANCES_H

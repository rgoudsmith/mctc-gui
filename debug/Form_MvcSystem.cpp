#include "Form_MvcSystem.h"
#include "ui_Form_MvcSystem.h"
#include "Rout.h"
#include "Form_MainMenu.h"

#define X_OFFSET 80

#define HIHI_LEVEL_X1 25
#define HIHI_LEVEL_Y 55

#define HI_LEVEL_X1 50
#define HI_LEVEL_Y 95

#define LO_LEVEL_X1 125
#define LO_LEVEL_Y 170

#define LOLO_LEVEL_X1 170
#define LOLO_LEVEL_Y 210

#define LABEL_WIDTH 120

/*----- Global variables -----*/
bool shotsToGo1Visible;
bool shotsToGo2Visible;

QImage imgBalanceHO;

MvcSystem::MvcSystem(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::MvcSystem)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  levelColors[0] = Qt::red;
  levelColors[1] = Qt::black;
  levelColors[2] = Qt::black;
  levelColors[3] = Qt::red;

  m_ui->setupUi(this);

  // Init variables.
  editColPct = true;
  editMaterial = true;
  editIdx = -1;

  resVars.scale = 1;
  resVars.xPos = 0;
  resVars.oldWidth = 0;
  resVars.oldHeight = 0;
  resVars.origImgSize.setHeight(0);
  resVars.origImgSize.setWidth(0);
  resVars.oldDispType = -1;

  numInput = new NumericInputFrm();
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
  connect(numInput,SIGNAL(InputValue(float)),this,SLOT(ValueReceived(float)));

  actMCID1 = new QLabel("MC_ID 1: ",this,0);
  actMCID1->setFixedSize(180,30);
  actMCID1->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  actMCID1->setStyleSheet("color: rgb(255,255,255); font-weight: bold;");

  actMCID2 = new QLabel("MC_ID 2: ",this,0);
  actMCID2->setFixedSize(actMCID1->size());
  actMCID2->setAlignment(actMCID1->alignment());
  actMCID2->setStyleSheet(actMCID1->styleSheet());

  actStatus1 = new QLabel("[    ]",this,0);
  actStatus1->setFixedSize(180,30);
  actStatus1->setAlignment(actMCID2->alignment());
  actStatus1->setStyleSheet("font-weight: bold;");

  actStatus2 = new QLabel("[    ]",this,0);
  actStatus2->setFixedSize(actStatus1->size());
  actStatus2->setAlignment(actStatus1->alignment());
  actStatus2->setStyleSheet(actStatus1->styleSheet());

  actRPM1 = new QLabel("1",this,0);
  actRPM1->setFixedWidth(LABEL_WIDTH);
  actRPM1->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
  rpmActive1 = new QLabel("",this,0);                                           // Motor status indicator unit 1

  actRPM2 = new QLabel("2", this,0);
  actRPM2->setFixedWidth(LABEL_WIDTH);
  actRPM2->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
  rpmActive2 = new QLabel("",this,0);                                           // Motor status indicator unit 2

  actWght1 = new QLabel("1",this,0);
  actWght1->setFixedWidth(LABEL_WIDTH);
  actWght1->setAlignment(Qt::AlignRight|Qt::AlignVCenter);

  actWght2 = new QLabel("2",this,0);
  actWght2->setFixedWidth(LABEL_WIDTH);
  actWght2->setAlignment(Qt::AlignRight|Qt::AlignVCenter);

  material1 = new QLabel("MATERIAL 1",this,0);
  material1->setFixedSize(158,25);
  material1->setAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
  material1->setFrameShadow(QFrame::Plain);
  material1->setFrameShape(QFrame::Box);

  material2 = new QLabel("MATERIAL 2",this,0);
  material2->setFixedSize(158,25);
  material2->setAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
  material2->setFrameShadow(QFrame::Plain);
  material2->setFrameShape(QFrame::Box);

  btMaterial1 = new QPushButton("MAT 1",this);
  btMaterial1->setFixedSize(160,40);

  btMaterial2 = new QPushButton("MAT 2",this);
  btMaterial2->setFixedSize(160,40);

  colPct1 = new QLabel("COL_PCT 1",this,0);
  colPct1->setFixedSize(120,30);
  colPct1->setStyleSheet("font-weight: bold;");
  colPct1->setAlignment(Qt::AlignRight|Qt::AlignVCenter);

  colPctEdit1 = new QLineEdit("COL_PCT 1",this);
  colPctEdit1->setFixedSize(120,55);
  colPctEdit1->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
  colPctEdit1->setVisible((editColPct && colPctVis));
  colPctEdit1->installEventFilter(this);
  colPctEdit1->setCursor(Qt::BlankCursor);
  colPctEdit1->setFocusPolicy(Qt::NoFocus);

  colPct2 = new QLabel("COL_PCT 2",this,0);
  colPct2->setFixedSize(colPct1->size());
  colPct2->setStyleSheet(colPct1->styleSheet());
  colPct2->setAlignment(Qt::AlignRight|Qt::AlignVCenter);

  colPctEdit2 = new QLineEdit("COL_PCT 2",this);
  colPctEdit2->setFixedSize(colPctEdit1->size());
  colPctEdit2->setAlignment(colPctEdit1->alignment());
  colPctEdit2->setVisible((editColPct && colPctVis));
  colPctEdit2->installEventFilter(this);
  colPctEdit2->setCursor(Qt::BlankCursor);
  colPctEdit2->setFocusPolicy(Qt::NoFocus);

  // Dos Act
  dosAct1 = new QLabel("DOS_ACT 1",this,0);
  dosAct1->setFixedSize(130,55);
  dosAct1->setAlignment(Qt::AlignRight|Qt::AlignVCenter);

  dosAct2 = new QLabel("DOS_ACT 2",this,0);
  dosAct2->setFixedSize(dosAct1->size());
  dosAct2->setAlignment(Qt::AlignRight|Qt::AlignVCenter);

  // Dos Set
  dosSet1 = new QLabel("DOS_SET 1",this,0);
  dosSet1->setFixedSize(dosAct1->size());
  dosSet1->setAlignment(Qt::AlignRight|Qt::AlignVCenter);

  dosSet2 = new QLabel("DOS_SET 2",this,0);
  dosSet2->setFixedSize(dosAct2->size());
  dosSet2->setAlignment(Qt::AlignRight|Qt::AlignVCenter);

  dosSetCap1 = new QLabel(this);
  dosSetCap1->setFixedSize(dosAct1->size());
  dosSetCap1->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  dosSetCap1->setVisible(false);

  dosActCap1 = new QLabel(this);
  dosActCap1->setFixedSize(dosSetCap1->size());
  dosActCap1->setAlignment(dosSetCap1->alignment());
  dosActCap1->setVisible(false);

  dosActLine1 = new QLabel(this);
  dosActLine1->setFrameShape(QFrame::HLine);
  dosActLine1->setLineWidth(1);
  dosActLine1->setFixedSize(40,dosActCap1->size().height());
  dosActLine1->setVisible(false);

  dosSetLine1 = new QLabel(this);
  dosSetLine1->setFrameShape(QFrame::HLine);
  dosSetLine1->setLineWidth(1);
  dosSetLine1->setFixedSize(40,dosSetCap1->size().height());
  dosSetLine1->setVisible(false);

  dosSetCap2 = new QLabel(this);
  dosSetCap2->setFixedSize(dosAct1->size());
  dosSetCap2->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  dosSetCap2->setText(tr("Set"));
  dosSetCap2->setVisible(false);

  dosActCap2 = new QLabel(this);
  dosActCap2->setFixedSize(dosSetCap2->size());
  dosActCap2->setAlignment(dosSetCap2->alignment());
  dosActCap2->setText(tr("Act"));
  dosActCap2->setVisible(false);

  dosActLine2 = new QLabel(this);
  dosActLine2->setFrameShape(QFrame::HLine);
  dosActLine2->setLineWidth(1);
  dosActLine2->setFixedSize(40,dosActCap2->size().height());
  dosActLine2->setVisible(false);

  dosSetLine2 = new QLabel(this);
  dosSetLine2->setFrameShape(QFrame::HLine);
  dosSetLine2->setLineWidth(1);
  dosSetLine2->setFixedSize(40,dosSetCap2->size().height());
  dosSetLine2->setVisible(false);

  /*----- Shots to go 1 -----*/
  shotsToGo1 = new QLabel(this);
  shotsToGo1->setFixedSize(130,55);
  shotsToGo1->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
  shotsToGo1->setVisible(false);

  shotsToGoCap1 = new QLabel(this);
  shotsToGoCap1->setFixedSize(shotsToGo1->size());
  //  shotsToGoCap1->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  shotsToGoCap1->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
  shotsToGoCap1->setVisible(false);

  /*----- Shots to go 2 -----*/
  shotsToGo2 = new QLabel(this);
  shotsToGo2->setFixedSize(130,55);
  shotsToGo2->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
  shotsToGo2->setVisible(false);

  shotsToGoCap2 = new QLabel(this);
  shotsToGoCap2->setFixedSize(shotsToGo2->size());
  shotsToGoCap2->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
  shotsToGoCap2->setVisible(false);

  // Set properties of Motor Active QFrame
  rpmActive1->setFrameShadow(QFrame::Plain);
  rpmActive1->setFrameShape(QFrame::Box);
  rpmActive1->setStyleSheet("color: rgb(0, 255, 0);");
  rpmActive1->setLineWidth(3);

  slideStatus = new QLabel("",this,0);
  slideStatus->setFixedSize(180,30);
  slideStatus->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  slideStatus->setFont(*(glob->baseFont));
  shotsToGoCap2->setVisible(false);

  rpmActive2->setFrameShadow(rpmActive1->frameShadow());
  rpmActive2->setFrameShape(rpmActive1->frameShape());
  rpmActive2->setStyleSheet(rpmActive1->styleSheet());
  rpmActive2->setLineWidth(rpmActive1->lineWidth());

  connect(btMaterial1,SIGNAL(clicked()),this,SLOT(Mat1Clicked()));
  connect(btMaterial2,SIGNAL(clicked()),this,SLOT(Mat2Clicked()));

  //  PositionLabels(0,1);
  m_ui->label->hide();

  actMCID1->show();
  actRPM1->show();

  actWght1->show();
  material1->show();
  colPct1->show();
  dosSet1->show();
  dosAct1->show();

  rpmActive1->hide();
  rpmActive2->hide();

  dispType = DISP_TYPE_SINGLE_UNIT;

  /*----- Load balance HO image -----*/
  imgBalanceHO.load(glob->AppPath()->toAscii().data() + QString(SYSTEM_MC_HO));
  imgBalanceHO.scaled(this->size(),Qt::KeepAspectRatio,Qt::FastTransformation);

  showLvls = false;
  lvlType = 3;
  SetMotorVisible(false);
  SetEditColorPct(false);
  SetMaterialEditable(false);
  LabelCaptions();
}


MvcSystem::~MvcSystem()
{
  delete numInput;
  delete m_ui;
}


void MvcSystem::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
  case QEvent::LanguageChange:
    m_ui->retranslateUi(this);
    LanguageUpdate();
    break;
  default:
    break;
  }
}


void MvcSystem::showEvent(QShowEvent *e)
{
  /*-----------------------------------------------------*/
  /* Show window event handler.                          */
  /*-----------------------------------------------------*/
  /* The MVC system screen has become active.            */
  /*-----------------------------------------------------*/
  int idx;

  QWidget::showEvent(e);

  /*----- Position labels -----*/
  PositionLabels(resVars.xPos,resVars.scale);

  /*----- Enable unit name refresh -----*/
  idx = glob->ActUnitIndex();
  SetMCID(glob->sysCfg[idx].MC_ID,idx);

  /*----- Refresh dos set and dos act values -----*/
  if (!glob->mstSts.isTwin) {
    SetDosSet(glob->sysSts[idx].setProd,idx);
    SetDosAct(glob->sysSts[idx].actProd,idx);
  }
  /*----- TWIN mode, refresh data fot both units -----*/
  else {
    SetDosSet(glob->sysSts[0].setProd,0);
    SetDosAct(glob->sysSts[0].actProd,0);
    SetDosSet(glob->sysSts[1].setProd,1);
    SetDosAct(glob->sysSts[1].actProd,1);
  }
}


void MvcSystem::resizeEvent(QResizeEvent *e)
{
  QWidget::resizeEvent(e);
  numInput->move(((width()/2)-(numInput->width()/2)),(((height()/2)-(numInput->height()/2))+35));
  LoadImage();
  PositionLabels((resVars.xPos),resVars.scale);
}

#define IMG_LT_X      56                                                        /* X position */
#define IMG_LT_Y      20                                                        /* Y position */
#define IMG_WIDTH     300                                                       /* Width */
#define IMG_HEIGHT    400                                                       /* Height */


void MvcSystem::paintEvent(QPaintEvent *e)
{
  QWidget::paintEvent(e);

  QPainter painter(this);

  painter.setRenderHint(QPainter::Antialiasing, true);

  /*----- Draw Image -----*/

  /*----- Define the image rectangle -----*/
  QRectF r1(IMG_LT_X,IMG_LT_Y,IMG_WIDTH,IMG_HEIGHT);

  switch (Rout::ActUnitType()) {

  case mcdtBalanceHO :
  case mcdtBalanceHORegrind:

    /*----- Load balance HO image -----*/
//    imgBalanceHO.load(glob->AppPath()->toAscii().data() + QString(SYSTEM_MC_HO));
//    imgBalanceHO.scaled(this->size(),Qt::KeepAspectRatio,Qt::FastTransformation);

    painter.drawImage(r1,imgBalanceHO);
    break;

  default :
    painter.drawImage(resVars.xPos,0,resVars.bgImage);
    /*----- Show levels (Showed in set filling system) -----*/
    if (this->showLvls) {
      DrawLevels(&painter,resVars.xPos,resVars.scale);
    }
    break;
  }

  // check for repositioning of labels. Updating every paint cycle slows the process too much.
  if ((resVars.oldHeight != this->height()) ||
      (resVars.oldWidth != this->width())) {
    // Draw level lines before scaling.
    painter.setPen(QPen(QBrush(Qt::red,Qt::SolidPattern), 2, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin));
    painter.setBrush(QBrush(Qt::black, Qt::SolidPattern));

    resVars.oldHeight = this->height();
    resVars.oldWidth = this->width();
    PositionLabels(resVars.xPos,resVars.scale);
  }
}


bool MvcSystem::eventFilter(QObject * obj, QEvent * ev)
{
  /*----------------------------------------*/
  /* Event filter to intercept edit actions */
  /* for ColPct en RPM data entry           */
  /*----------------------------------------*/
  double val;
  eGraviRpm mod;

  if (ev->type() == QEvent::MouseButtonPress) {

    /* Check backlit, if active turn it on and ignore event */
    if (Rout::BacklitOn()) {
      return(TRUE);
    }

    /*----- ColPct1 edit -----*/
    if (obj ==  colPctEdit1) {
      if (dispType != DISP_TYPE_MULTI_UNIT) {
        mod = glob->sysCfg[0].GraviRpm;
        if (mod == grGravi) {
          val = glob->sysCfg[0].colPct;
        }
        else {
          val = glob->sysCfg[0].setRpm;
        }
      }
      else {
        mod = glob->sysCfg[glob->ActUnitIndex()].GraviRpm;
        if (mod == grGravi) {
          val = glob->sysCfg[glob->ActUnitIndex()].colPct;
        }
        else {
          val = glob->sysCfg[glob->ActUnitIndex()].setRpm;
        }
      }
      editIdx = 0;
    }
    /*----- ColPct2 edit -----*/
    else {
      if (obj == colPctEdit2) {
        mod = glob->sysCfg[1].GraviRpm;
        if (mod == grGravi) {
          val = glob->sysCfg[1].colPct;
        }
        else {
          val = glob->sysCfg[1].setRpm;
        }
        editIdx = 1;
      }
      else {
        editIdx = -1;
      }
    }

    if (editIdx != -1) {
      KeyBeep();
      if (mod == grGravi) {
        numInput->SetDisplay(true,glob->formats.PercentageFmt.precision,val,0.000,100.000);
      }
      else {
        numInput->SetDisplay(true,glob->formats.RpmFmt.precision,val,0.0,200.0);
      }
      numInput->show();
    }
  }
  return QWidget::eventFilter(obj,ev);
}


void MvcSystem::LoadImage()
{
  QString imgPath;

  if (resVars.oldDispType != dispType) {
    resVars.oldDispType = dispType;
    switch(resVars.oldDispType) {
    case 0:
      imgPath = QString(SYSTEM_SINGLE_IMG);
      break;
    case 1:
      imgPath = QString(SYSTEM_DUO_IMG);
      break;
    case 2:
      imgPath = QString(SYSTEM_SINGLE_IMG);
      break;
    default:
      imgPath = QString(SYSTEM_SINGLE_IMG);
      break;
    }

    resVars.imgPath = glob->AppPath()->toAscii().data() + imgPath;
  }


  resVars.bgImage.load(resVars.imgPath);
  resVars.origImgSize = resVars.bgImage.size();

  // Scale the image
  resVars.bgImage = resVars.bgImage.scaled(this->size(),Qt::KeepAspectRatio,Qt::FastTransformation);
  resVars.scale = (double)resVars.bgImage.size().width()/(double)resVars.origImgSize.width();
  resVars.xPos = this->width() - resVars.bgImage.width();

}


void MvcSystem::DrawLevels(QPainter *p, int xOffset, double scaleFac)
{
  QPen pen(levelColors[3]);
  QBrush brush(levelColors[3]);

  int lineStart,lineEnd,lineY;

  lineStart = xOffset - X_OFFSET;

  p->setPen(pen);
  p->setBrush(brush);

  // Depending on lvlType draw lines or not.

  // draw hihi level
  if (this->lvlType >= 2) {
    lineEnd = (HIHI_LEVEL_X1 * scaleFac) + xOffset;
    lineY = (HIHI_LEVEL_Y * scaleFac);
    p->drawLine(lineStart,lineY,lineEnd,lineY);
    p->drawEllipse(QPoint(lineEnd,lineY),3,3);
  }

  // Draw hi level (at level 1 and 2, hihi level is "abused")
  if (lvlType == 3) {
    pen.setColor(levelColors[2]);
    brush.setColor(levelColors[2]);

    p->setPen(pen);
    p->setBrush(brush);

    lineEnd = (HI_LEVEL_X1*scaleFac) + xOffset;
    lineY = HI_LEVEL_Y * scaleFac;
    p->drawLine(lineStart,lineY,lineEnd,lineY);
    p->drawEllipse(QPoint(lineEnd,lineY),3,3);
  }

  // p->drawLine((xOffset+30),HIHI_LEVEL,(xOffset+250),LOLO_LEVEL);

  if (lvlType >= 1) {
    pen.setColor(levelColors[1]);
    brush.setColor(levelColors[1]);

    p->setPen(pen);
    p->setBrush(brush);

    lineEnd = (LO_LEVEL_X1*scaleFac) + xOffset;
    lineY = LO_LEVEL_Y * scaleFac;
    p->drawLine(lineStart,lineY,lineEnd,lineY);
    p->drawEllipse(QPoint(lineEnd,lineY),3,3);
  }

  // Draw lolo level
  if (lvlType >= 0) {
    pen.setColor(levelColors[0]);
    brush.setColor(levelColors[0]);

    p->setPen(pen);
    p->setBrush(brush);

    lineEnd = (LOLO_LEVEL_X1*scaleFac) + xOffset;
    lineY = LOLO_LEVEL_Y * scaleFac;
    p->drawLine(lineStart,lineY,lineEnd,lineY);
    p->drawEllipse(QPoint(lineEnd,lineY),3,3);
  }
}


void MvcSystem::PositionLabels(int xOffset, double scaleFac)
{
  /*---------------------------------------*/
  /* Position the label for the MVC system */
  /*---------------------------------------*/

  /*----- Label pos for Balance HO -----*/
  if ((Rout::ActUnitType() == mcdtBalanceHO) || (Rout::ActUnitType() == mcdtBalanceHORegrind)) {
    PositionLabelsHO();
  }
  else {
    /*----- Labels pos for Balance unit 1 -----*/
    PositionLabels1(xOffset,scaleFac);
  }

  /*----- Labels pos for Balance unit 2 -----*/
  if (dispType == DISP_TYPE_TWIN) {
    PositionLabels2(xOffset,scaleFac);
  }
}


void MvcSystem::PositionLabelsHO()
{
  /*--------------------------------*/
  /* Position all labels for Unit 1 */
  /*--------------------------------*/
  int wid,high;
  int x,y;
  int scaleFac = 1;
  int xOffset = 0;

  /*----- MC ID Name positioning -----*/
  x = (160*scaleFac);
  y = (280*scaleFac);
  actMCID1->move((x+xOffset),y);
  actMCID1->setStyleSheet("color: black; font-weight: bold;");

  /*----- Status -----*/
  y += 40;
  actStatus1->move((x+xOffset),y);

  /*----- RPM positioning -----*/
//  x = (-30*scaleFac);
//  y = 245*scaleFac;
   x = (-15*scaleFac);
   y = 285*scaleFac;

  actRPM1->move((x+xOffset),y);

  /*----- Weight positioning -----*/
  x = actRPM1->pos().x();
  y= (315*scaleFac);
  actWght1->move((x+xOffset),y);

  /*----- Motor active indicator -----*/
  x = (51*scaleFac) + (10 - (4*scaleFac));
  y = (152*scaleFac);
  wid = (38*scaleFac);
  high = (49*scaleFac);

  rpmActive1->move((x+xOffset),y);
  rpmActive1->setFixedWidth(wid);
  rpmActive1->setFixedHeight(high);

  /*----- Material -----*/
  x = 62*scaleFac;
//y = 4*scaleFac;
  y = 0;
  material1->move((x+xOffset),y);
  btMaterial1->move(((x-1) + xOffset),0);

  /*----- Dose Color percentage -----*/
  x = 89*scaleFac;
//  y = 61*scaleFac;
  y = 70*scaleFac;
  colPctEdit1->move((x+xOffset),y);

  y = 85*scaleFac;
  colPct1->move((x+xOffset),y);

  /*----- DOS Set -----*/
  x = 72*scaleFac;
  y = 65*scaleFac;
  dosSet1->move((x+xOffset),y);

  /*----- DOS Act -----*/
  y += 20*scaleFac;
  dosAct1->move((x+xOffset),y);

  x -= 20;
  y += 50*scaleFac;
  slideStatus->move((x+xOffset),y);

  x -= 100;
  y = dosSet1->pos().y();
  dosSetCap1->move((x+xOffset),y);

  y = dosAct1->pos().y();
  dosActCap1->move((x+xOffset),y);

  x += 88;
  dosActLine1->move((x+xOffset),(dosActCap1->pos().y()+10));
  dosSetLine1->move((x+xOffset),(dosSetCap1->pos().y()+10));

  /*----- Shots to GO -----*/
  x = 20*scaleFac;
  y = 300*scaleFac;
  shotsToGo1->move((x+xOffset),y);
  y += 25;
  shotsToGoCap1->move((x+xOffset),y);
}


void MvcSystem::PositionLabels1(int xOffset, double scaleFac)
{
  /*--------------------------------*/
  /* Position all labels for Unit 1 */
  /*--------------------------------*/
  int wid,high;
  int x,y;

  /*----- MC ID Name positioning -----*/
  x = (128*scaleFac);
  y = (345*scaleFac);
  actMCID1->move((x+xOffset),y);
  actMCID1->setStyleSheet("color: rgb(255,255,255); font-weight: bold;");

  y += 35;
  actStatus1->move((x+xOffset),y);

  /*----- RPM positioning -----*/
  x = (-6*scaleFac);
  y = 245*scaleFac;
  actRPM1->move((x+xOffset),y);

  /*----- Weight positioning -----*/
  x = actRPM1->pos().x();
  y= (310*scaleFac);
  actWght1->move((x+xOffset),y);

  /*----- Motor active indicator -----*/
  x = (51*scaleFac) + (10 - (4*scaleFac));
  y = (152*scaleFac);
  wid = (38*scaleFac);
  high = (49*scaleFac);

  rpmActive1->move((x+xOffset),y);
  rpmActive1->setFixedWidth(wid);
  rpmActive1->setFixedHeight(high);

  /*----- Material -----*/
  x = 42*scaleFac;
  y = 7*scaleFac;
  material1->move((x+xOffset),y);
  btMaterial1->move(((x-1) + xOffset),0);

  /*----- Dose Color percentage -----*/
  x = 89*scaleFac;
  y = 61*scaleFac;
  colPctEdit1->move((x+xOffset),y);

  y = 85*scaleFac;
  colPct1->move((x+xOffset),y);

  /*----- DOS Set -----*/
  x = 89*scaleFac;
  y = 65*scaleFac;
  dosSet1->move((x+xOffset),y);

  /*----- DOS Act -----*/
  y += 20*scaleFac;
  dosAct1->move((x+xOffset),y);

  x -= 120;
  y = dosSet1->pos().y();
  dosSetCap1->move((x+xOffset),y);

  y = dosAct1->pos().y();
  dosActCap1->move((x+xOffset),y);

  x += 88;
  dosActLine1->move((x+xOffset),(dosActCap1->pos().y()+10));
  dosSetLine1->move((x+xOffset),(dosSetCap1->pos().y()+10));

  /*----- Shots to GO -----*/
  x = 20*scaleFac;
  y = 300*scaleFac;
  shotsToGo1->move((x+xOffset),y);
  y += 25;
  shotsToGoCap1->move((x+xOffset),y);
}



void MvcSystem::PositionLabels2(int xOffset, double scaleFac)
{
  /*--------------------------------*/
  /* Position all labels for Unit 2 */
  /*--------------------------------*/
  int wid,high;
  int x,y;

  /*----- MVC ID Name positioning -----*/
  x = (455*scaleFac);
  y = actMCID1->pos().y();
  actMCID2->move((x+xOffset),y);

  y = actStatus1->pos().y();
  actStatus2->move((x+xOffset),y);

  /*----- RPM positioning -----*/
  x = (643*scaleFac);
  y = actRPM1->pos().y();
  actRPM2->move((x+xOffset),y);

  /*----- Weight positioning -----*/
  y= actWght1->pos().y();
  actWght2->move((x+xOffset),y);

  /*----- Motor active indicator -----*/
  x = (344*scaleFac) + (10 - (4*scaleFac));
  y = (142*scaleFac);
  wid = (38*scaleFac);
  high = (49*scaleFac);

  rpmActive2->move((x+xOffset),y);
  rpmActive2->setFixedWidth(wid);
  rpmActive2->setFixedHeight(high);

  /*----- Material -----*/
  x = 566*scaleFac;
  y = material1->pos().y();
  material2->move((x+xOffset),y);
  btMaterial2->move(((x-1) + xOffset),0);

  /*----- Dose Color percentage -----*/
  x = 555*scaleFac;
  y = colPct1->pos().y();
  colPct2->move((x+xOffset),y);

  y = colPctEdit1->pos().y();
  colPctEdit2->move((x+xOffset),y);

  /*----- DOS Set -----*/
  x = 530*scaleFac;
  y = dosSet1->pos().y();
  dosSet2->move((x+xOffset),y);

  /*----- DOS Act -----*/
  y = dosAct1->pos().y();
  dosAct2->move((x+xOffset),y);

  x += 132;
  y = dosSet2->pos().y();
  dosSetCap2->move((x+xOffset),y);

  y = dosAct2->pos().y();
  dosActCap2->move((x+xOffset),y);

  dosActLine2->move((x+xOffset),(dosActCap2->pos().y()+10));
  dosSetLine2->move((x+xOffset),(dosSetCap2->pos().y()+10));

  /*----- Shots to GO -----*/
  x = 680*scaleFac;
  y = 300*scaleFac;
  shotsToGo2->move((x+xOffset),y);
  y += 25;
  shotsToGoCap2->move((x+xOffset),y);
}


// === SET GBI ===

void MvcSystem::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  actRPM1->setFont((*(glob->baseFont)));
  actRPM2->setFont(*(glob->baseFont));
  colPct1->setFont((*(glob->baseFont)));
  colPct2->setFont(*(glob->baseFont));
  colPctEdit1->setFont((*(glob->baseFont)));
  colPctEdit2->setFont(*(glob->baseFont));
  dosAct1->setFont((*(glob->baseFont)));
  dosAct2->setFont(*(glob->baseFont));
  dosSet1->setFont((*(glob->baseFont)));
  dosSet2->setFont(*(glob->baseFont));
  dosSetCap1->setFont(*(glob->baseFont));
  dosActCap1->setFont(*(glob->baseFont));
  dosSetCap2->setFont(*(glob->baseFont));
  dosActCap2->setFont(*(glob->baseFont));
  actWght1->setFont((*(glob->baseFont)));
  actWght2->setFont(*(glob->baseFont));
  actStatus1->setFont((*(glob->baseFont)));
  actStatus2->setFont(*(glob->baseFont));
  material1->setFont(*(glob->baseFont));
  material2->setFont(*(glob->baseFont));
  actMCID1->setFont(*(glob->baseFont));
  actMCID2->setFont(*(glob->baseFont));
  btMaterial1->setFont(*(glob->baseFont));
  btMaterial2->setFont(*(glob->baseFont));
  shotsToGo1->setFont(*(glob->baseFont));
  shotsToGoCap1->setFont(*(glob->baseFont));
  shotsToGo2->setFont(*(glob->baseFont));
  shotsToGoCap2->setFont(*(glob->baseFont));

  connect(glob,SIGNAL(SysSts_RpmChanged(float,int)),this,SLOT(SetActRPM(float,int)));
  connect(glob,SIGNAL(SysCfg_RpmChanged(float,int)),this,SLOT(SetRPM(float,int)));
  //  connect(glob,SIGNAL(SysSts_MassChanged(float,int)),this,SLOT(SetWeight(float,int)));
  connect(glob,SIGNAL(SysSts_MassChanged(float,int)),this,SLOT(SetWeight(float,int)));
  connect(glob,SIGNAL(SysSts_ProdToGoChanged(float,int)),this,SLOT(SetProdToGo(float,int)));
  connect(glob,SIGNAL(SysCfg_DosSetChanged(float,int)),this,SLOT(SetDosSet(float,int)));
  connect(glob,SIGNAL(SysSts_DosActChanged(float,int)),this,SLOT(SetDosAct(float,int)));
  connect(glob,SIGNAL(SysCfg_IDChanged(QString,int)),this,SLOT(SetMCID(QString,int)));
  connect(glob,SIGNAL(SysCfg_ColorPctChanged(float,int)),this,SLOT(SetColorPct(float,int)));
  connect(glob,SIGNAL(SysSts_MCStatusChanged(eMCStatus,int)),this,SLOT(SetMCStatus(eMCStatus,int)));
  connect(glob,SIGNAL(SysCfg_MaterialChanged(QString,int)),this,SLOT(SetMaterial(QString,int)));
  connect(glob,SIGNAL(SysCfg_GraviRpmChanged(eGraviRpm,int)),this,SLOT(GraviRpmChanged(eGraviRpm,int)));
  connect(glob,SIGNAL(MstSts_ActUnitChanged(int)),this,SLOT(UnitIndexChanged(int)));

  /*----- Connect data refresh to msg received -----*/
  connect(glob,SIGNAL(ComReceived(tMsgControlUnit,int)),this,SLOT(ComMsgReceived(tMsgControlUnit,int)));


  numInput->FormInit();
  InitValues();
  this->update();
}


void MvcSystem::LabelCaptions()
{
  dosSetCap1->setText(tr("Set"));
  dosActCap1->setText(tr("Act"));
}


void MvcSystem::LanguageUpdate()
{
  LabelCaptions();
  InitValues();
}


void MvcSystem::InitValues()
{
  for(int i=0;i<CFG_MAX_UNIT_COUNT;i++) {
    SetColorPct(glob->sysCfg[i].colPct,i);
    SetRPM(glob->sysCfg[i].setRpm,i);
    SetMCID(glob->sysCfg[i].MC_ID,i);
    SetMaterial(glob->sysCfg[i].material.name,i);
    SetDosAct(glob->sysSts[i].actProd,i);
    SetDosSet(glob->sysSts[i].setProd,i);
    SetActRPM(glob->sysSts[i].rpm,i);
    SetWeight(glob->sysSts[i].mass,i);
    SetMCStatus(glob->sysSts[i].MCStatus,i);
  }
  SetEditColorPct(editColPct);
}


void MvcSystem::SetDispIdx(int idx)
{
  if (dispIdx != idx) {
    dispIdx = idx;
    // Refresh display
    InitValues();
  }
}


void MvcSystem::SetDisplayType(int type)
{
  /*--------------------------------------------*/
  /* Setup the MC-TC image type                 */
  /* 0 = Single unit                            */
  /* 1 = Dual init (TWIN mode)                  */
  /* 2 = Single unit (Multi unit mode           */
  /*--------------------------------------------*/
  if (type != dispType) {                                                       /* Detect type change */
    dispType = type;
    LoadImage();
    InitValues();
    update();
  }
}


void MvcSystem::ShowLevels(bool _show)
{
  if (showLvls != _show) {
    showLvls = _show;
    lvlType = 0;
    update();
  }
}


void MvcSystem::ShowLevels(bool _show,int _type)
{
  // Show different levels according to type
  if (showLvls != _show) {
    showLvls = _show;
  }

  if (lvlType != _type) {
    lvlType = _type;
    switch(lvlType) {
    case 0:
      break;
    case 1:
    case 2:
      levelColors[3] = Qt::black;
      break;
    case 3:
      levelColors[3] = Qt::red;
      break;
    }
  }
  update();
}


void MvcSystem::SetEditColorPct(bool _doEdit)
{
  editColPct = _doEdit;
  colPctEdit1->setVisible((editColPct && colPctVis));

  if (dispType == DISP_TYPE_TWIN) {
    colPctEdit2->setVisible((editColPct&&colPctVis));
  }
  else {
    colPctEdit2->setVisible(false);
  }
  update();
}




void MvcSystem::SetMaterialEditable(bool _doEdit)
{
  editMaterial = _doEdit;
  btMaterial1->setVisible((editMaterial && matVis));

  if (dispType == DISP_TYPE_TWIN) {
    btMaterial2->setVisible((editMaterial && matVis));
  }
  else {
    btMaterial2->setVisible(false);
  }
  update();
}


// === PRIVATE SLOTS ===

void MvcSystem::ValueReceived(float val)
{
  switch(editIdx) {
  case 0:
    if (this->dispType != DISP_TYPE_MULTI_UNIT) {
      if(glob->sysCfg[0].GraviRpm == grGravi) {
        glob->SysCfg_SetColPct(val,0);
      }
      else {
        glob->SysCfg_SetRpm(val,0);
      }
    }
    else {
      if(glob->sysCfg[glob->ActUnitIndex()].GraviRpm == grGravi) {
        glob->SysCfg_SetColPct(val,glob->ActUnitIndex());
      }
      else {
        glob->SysCfg_SetRpm(val,glob->ActUnitIndex());
      }
    }

    ((MainMenu*)(glob->menu))->prodForm->SetRecipeDataChanged();

    break;

  case 1:
    if (glob->sysCfg[1].GraviRpm == grGravi) {
      glob->SysCfg_SetColPct(val,1);
    }
    else {
      glob->SysCfg_SetRpm(val,1);
    }

    ((MainMenu*)(glob->menu))->prodForm->SetRecipeDataChanged();

    break;
  }
  editIdx = -1;
}


// === PUBLIC SLOTS ===

// Visual representation
void MvcSystem::SetMCIDVisible(bool _active)
{
  actMCID1->setVisible(_active);

  if (dispType == DISP_TYPE_TWIN) {
    actMCID2->setVisible(_active);
  }
  else {
    actMCID2->setVisible(false);
  }
}


void MvcSystem::SetMCStatusVisible(bool _active)
{
  this->actStatus1->setVisible(_active);

  if (dispType == DISP_TYPE_TWIN) {
    actStatus2->setVisible(_active);
  }
  else {
    actStatus2->setVisible(false);
  }
}


void MvcSystem::SetMotorVisible(bool _active)
{
  rpmStsVisible = _active;

  switch(dispType) {
  case DISP_TYPE_SINGLE_UNIT:
    if ((rpmStsVisible) && (glob->sysSts[0].motorSts != 0)) {
      rpmActive1->setVisible(_active);
    }
    else {
      rpmActive1->setVisible(false);
    }

    rpmActive2->setVisible(false);
    break;

  case DISP_TYPE_TWIN:
    if ((rpmStsVisible) && (glob->sysSts[0].motorSts != 0)) {
      rpmActive1->setVisible(_active);
    }
    else {
      rpmActive1->setVisible(false);
    }

    if ((rpmStsVisible) && (glob->sysSts[1].motorSts != 0)) {
      rpmActive2->setVisible(_active);
    }
    else {
      rpmActive2->setVisible(false);
    }
    break;

  case DISP_TYPE_MULTI_UNIT:
    if ((rpmStsVisible) && (glob->sysSts[dispIdx].motorSts != 0)) {
      rpmActive1->setVisible(_active);
    }
    else {
      rpmActive1->setVisible(false);
    }
    rpmActive2->setVisible(false);
    break;
  }
}


void MvcSystem::SetWeightVisible(bool _active)
{
  actWght1->setVisible(_active);

  if (dispType == DISP_TYPE_TWIN) {
    actWght2->setVisible(_active);
  }
  else {
    actWght2->setVisible(false);
  }
}


void MvcSystem::SetRPMVisible(bool _active)
{
  actRPM1->setVisible(_active);

  if (dispType == DISP_TYPE_TWIN) {
    actRPM2->setVisible(_active);
  }
  else {
    actRPM2->setVisible(false);
  }
}


void MvcSystem::SetColorPctVisible(bool _active)
{
  colPctVis = _active;
  SetEditColorPct(editColPct);

  colPct1->setVisible(_active);

  if (dispType == DISP_TYPE_TWIN) {
    colPct2->setVisible(_active);
  }
  else {
    colPct2->setVisible(false);
  }
}


void MvcSystem::SetMaterialVisible(bool _active)
{
  matVis = _active;
  SetMaterialEditable(editMaterial);

  material1->setVisible(_active);

  if (dispType == DISP_TYPE_TWIN) {
    material2->setVisible(_active);
  }
  else {
    material2->setVisible(false);
  }
}


void MvcSystem::SetDosActVisible(bool _active)
{
  dosAct1->setVisible(_active);
  dosActCap1->setVisible(_active);
  dosActLine1->setVisible(_active);

  /*----- Slide visible is linked to dosAct visible -----*/
  if (Rout::ActUnitType()==mcdtBalanceHO) {
    slideStatus->setVisible(_active);
  }
  else {
    slideStatus->setVisible(false);
  }

  if (dispType == DISP_TYPE_TWIN) {
    dosAct2->setVisible(_active);
    dosActCap2->setVisible(_active);
    dosActLine2->setVisible(_active);
  }
  else {
    dosAct2->setVisible(false);
    dosActCap2->setVisible(false);
    dosActLine2->setVisible(false);
  }
}


void MvcSystem::SetDosSetVisible(bool _active)
{
  dosSet1->setVisible(_active);
  dosSetCap1->setVisible(_active);
  dosSetLine1->setVisible(_active);

  if(dispType == DISP_TYPE_TWIN) {
    dosSet2->setVisible(_active);
    dosSetCap2->setVisible(_active);
    dosSetLine2->setVisible(_active);
  }
  else {
    dosSet2->setVisible(false);
    dosSetCap2->setVisible(false);
    dosSetLine2->setVisible(false);
  }
}


void MvcSystem::SetToGoVisible(bool _active)
{
  /*----------------------------------------*/
  /* Set shots/minutes to go fields visible */
  /* Only when there is data                */
  /*----------------------------------------*/

  /*--------*/
  /* Unit 0 */
  /*--------*/

  /*----- Setup caption text depending on inj/extr mode -----*/
  switch(glob->mstCfg.mode) {
  case modInject:
    shotsToGoCap1->setText(tr("shots"));
    break;

  case modExtrude:
    shotsToGoCap1->setText(tr("min"));
    break;

  case modCount:
    break;
  }

  /*----- Enable display of the fields -----*/
  shotsToGo1Visible = _active;

  if (!shotsToGo1Visible) {
    shotsToGo1->setVisible(false);
    shotsToGoCap1->setVisible(false);
  }

  /*----------------------------*/
  /* Unit 1 (Only in TWIN mode) */
  /*----------------------------*/
  if (dispType == DISP_TYPE_TWIN) {

    /*----- Setup caption text depending on inj/extr mode -----*/
    switch(glob->mstCfg.mode) {
    case modInject:
      shotsToGoCap2->setText(tr("shots"));
      break;

    case modExtrude:
      shotsToGoCap2->setText(tr("min"));
      break;

    case modCount:
      break;
    }

    /*----- Enable display of the fields -----*/
    shotsToGo2Visible = _active;

    if (!shotsToGo2Visible) {
      shotsToGo2->setVisible(false);
      shotsToGoCap2->setVisible(false);
    }
  }
}


void MvcSystem::dispMotorStatus(int _status, QLabel *lbl)
{
  if (rpmStsVisible) {
    lbl->setVisible((_status != 0));
    switch (_status) {
    case 1:                                                                   // Active
      lbl->setStyleSheet("color: rgb(0,255,0);");
      break;

    case 2:                                                                   // Error
      lbl->setStyleSheet("color: rgb(255,0,0);");
      break;
    }
  }
}


// Process indication (dependant of dispIdx)
void MvcSystem::SetMotorStatus(int _status, int idx)
{
  switch(dispType) {
  case DISP_TYPE_SINGLE_UNIT:
    if (idx == 0) {
      dispMotorStatus(_status,rpmActive1);
    }
    break;

  case DISP_TYPE_TWIN:
    switch(idx) {
    case 0:
      dispMotorStatus(_status,rpmActive1);
      break;
    case 1:
      dispMotorStatus(_status,rpmActive2);
      break;
    }
    break;

  case DISP_TYPE_MULTI_UNIT:
    if (idx == dispIdx) {
      dispMotorStatus(_status,rpmActive1);
    }
    break;
  }
}


void MvcSystem::SetWeight(float _wght, int idx)
{
  /*-----------------------*/
  /* SLOT : Weight changed */
  /*-----------------------*/
  QString text;

  /*----- Update weight ----*/
  if (_wght != CLIP_FLOAT) {
    text = QString::number(_wght,glob->formats.WeightFmt.format.toAscii(),glob->formats.WeightFmt.precision) + QString(" ")+ glob->weightUnits;
  }
  else {
    text = CLIP_FLOAT_STR;
  }

  switch(dispType) {
  case DISP_TYPE_SINGLE_UNIT:
    if (idx == 0) {
      actWght1->setText(text);
    }
    break;

  case DISP_TYPE_TWIN:
    switch (idx) {
    case 0:
      actWght1->setText(text);
      break;

    case 1:
      actWght2->setText(text);
      break;
    }
    break;

  case DISP_TYPE_MULTI_UNIT:
    if (idx == dispIdx) {
      actWght1->setText(text);
    }
    break;
  }
}


void MvcSystem::SetProdToGo(float _prodToGo, int idx)
{
  /*------------------------------------*/
  /* SLOT : Products/time to go changed */
  /*------------------------------------*/
  QString text;

  /*----- Update shots to go ----*/
  if (_prodToGo != CLIP_FLOAT) {

    switch (glob->mstCfg.mode) {

    case modInject:
      text = QString::number(_prodToGo,'f',0);
      break;

    case modExtrude:
      text = QString::number(_prodToGo,'f',1);
      break;

    case modCount:
      break;
    }
  }
  else {
    text = CLIP_FLOAT_STR;
  }

  /*----- Update data field -----*/
  switch(idx) {
  case 0:
    if ((_prodToGo == 0) || (!shotsToGo1Visible)) {
      shotsToGo1->setVisible(false);
      shotsToGoCap1->setVisible(false);
    }
    else {
      if (shotsToGo1Visible) {
        shotsToGo1->setVisible(true);
        shotsToGoCap1->setVisible(true);
        shotsToGo1->setText(text);
      }
    }
    break;

  case 1:
    if ((_prodToGo == 0) || (!shotsToGo2Visible)) {
      shotsToGo2->setVisible(false);
      shotsToGoCap2->setVisible(false);
    }
    else {
      if (shotsToGo2Visible) {
        shotsToGo2->setVisible(true);
        shotsToGoCap2->setVisible(true);
        shotsToGo2->setText(text);
      }
    }
    break;
  }
}


void MvcSystem::SetRPM(float _rpm, int idx)
{
  /* Used to set production rpm in grRPM mode (colPct edit is used for input)*/
  QString text;

  if (glob->sysCfg[idx].GraviRpm == grRpm) {
    if (_rpm != CLIP_FLOAT) {
      text = QString::number(_rpm,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision) +" "+ glob->rpmUnits;
    }
    else {
      text = "xxx "+glob->rpmUnits;
    }

    switch(dispType) {
    case DISP_TYPE_SINGLE_UNIT:
      if (idx == 0) {
        colPct1->setText(text);
        colPctEdit1->setText(text);
      }
      break;

    case DISP_TYPE_TWIN:
      switch(idx) {
      case 0:
        colPct1->setText(text);
        colPctEdit1->setText(text);
        break;
      case 1:
        colPct2->setText(text);
        colPctEdit2->setText(text);
        break;
      }
      break;

    case DISP_TYPE_MULTI_UNIT:
      if (idx == dispIdx) {
        colPct2->setText(text);
        colPctEdit2->setText(text);
      }
      break;
    }
  }
}


void MvcSystem::SetActRPM(float _rpm, int idx)
{
  QString text;

  if (_rpm != CLIP_FLOAT) {
    text = QString::number(_rpm,glob->formats.RpmFmt.format.toAscii(),glob->formats.RpmFmt.precision) + " " + glob->rpmUnits;
  }
  else {
    text = CLIP_FLOAT_STR;
  }

  switch(dispType) {
  case DISP_TYPE_SINGLE_UNIT:
    if(idx == 0) {
      actRPM1->setText(text);
    }
    break;

  case DISP_TYPE_TWIN:
    switch(idx) {
    case 0:
      actRPM1->setText(text);
      break;
    case 1:
      actRPM2->setText(text);
      break;
    }
    break;

  case DISP_TYPE_MULTI_UNIT:
    if (idx == dispIdx) {
      actRPM1->setText(text);
    }
    break;
  }
}


void MvcSystem::SetMCID(const QString &str, int idx)
{
  switch(dispType) {
  case DISP_TYPE_SINGLE_UNIT:
    //      if(idx==0) {
    actMCID1->setText(str);
    //      }
    break;

  case DISP_TYPE_TWIN:
    switch(idx) {
    case 0:
      actMCID1->setText(str);
      break;
    case 1:
      actMCID2->setText(str);
      break;
    }
    break;

  case DISP_TYPE_MULTI_UNIT:
    if (idx == dispIdx) {
      actMCID1->setText(str);
    }
    break;
  }
}


void MvcSystem::SetMCStatus(eMCStatus _sts, int idx)
{
  QString text;
  QString tmp = "";

  /*----- Build status text string -----*/
  text = "[ ";
  glob->GetEnumeratedText(etMcStatus,_sts,&tmp);
  text += tmp;

  /*----- Indicator act value equals set value -----*/
  if (_sts > mcsOff) {                                                          /* Is unit on ? */
    if (glob->sysSts[idx].actIsSet) {
      text += " *";
    }
  }

  text = text + " ]";

  switch (dispType) {
  case DISP_TYPE_SINGLE_UNIT:
    if (idx == 0) {
      actStatus1->setText(text);
    }
    break;

  case DISP_TYPE_TWIN:
    switch (idx) {
    case 0:
      actStatus1->setText(text);
      break;
    case 1:
      actStatus2->setText(text);
      break;
    }
    break;

  case DISP_TYPE_MULTI_UNIT:
    if (dispIdx == idx) {
      actStatus1->setText(text);
    }
    break;
  }
}


void MvcSystem::SetColorPct(float _col, int idx)
{
  QString text;

  if (glob->sysCfg[idx].GraviRpm == grGravi) {
    if (_col != CLIP_FLOAT) {
      text = QString::number(_col,glob->formats.PercentageFmt.format.toAscii(),glob->formats.PercentageFmt.precision)+QString(" %");
    }
    else {
      text = CLIP_FLOAT_STR;
    }

    switch(dispType) {
    case DISP_TYPE_SINGLE_UNIT:
      if(idx==0) {
        colPct1->setText(text);
        colPctEdit1->setText(text);
      }
      break;

    case DISP_TYPE_TWIN:
      switch(idx) {
      case 0:
        colPct1->setText(text);
        colPctEdit1->setText(text);
        break;
      case 1:
        colPct2->setText(text);
        colPctEdit2->setText(text);
        break;
      }
      break;

    case DISP_TYPE_MULTI_UNIT:
      if (idx == dispIdx) {
        colPct1->setText(text);
        colPctEdit1->setText(text);
      }
      break;
    }
  }
}


void MvcSystem::SetMaterial(const QString &str, int idx)
{
  QString matName = str;
  QString ext = MATERIAL_EXTENSION;
  if ((str != "") && (str != MATERIAL_EMPTY_NAME)) {
    glob->GetDispMaterial(&matName,idx);
  }
  else {
    matName = MATERIAL_EMPTY_NAME;
  }

  switch(dispType) {
  case DISP_TYPE_SINGLE_UNIT:
    if(idx==0) {
      material1->setText(matName);
      btMaterial1->setText(matName);
    }
    break;

  case DISP_TYPE_TWIN:
    switch(idx) {
    case 0:
      material1->setText(matName);
      btMaterial1->setText(matName);
      break;
    case 1:
      material2->setText(matName);
      btMaterial2->setText(matName);
      break;
    }
    break;

  case DISP_TYPE_MULTI_UNIT:
    if (idx == dispIdx) {
      material1->setText(matName);
      btMaterial1->setText(matName);
    }
    break;

  }
}


void MvcSystem::SetDosAct(float _dos, int idx)
{
  /*------------------------------*/
  /* Refresh the Dosage Act value */
  /*------------------------------*/
  QString text;
  QString units;
  ItemFormatStruct dosFmt;

  switch (Rout::ActUnitType()) {

  case mcdtBalanceHO:
    dosFmt = glob->formats.DosageKgHFmt;
    units = glob->extrUnits;
    _dos *= 3.6;
    break;

  default :
    /*----- Convert gr/s to kg/h if enabled and in extrusion mode -----*/
    if ((glob->mstCfg.units_kgh == true) && (glob->mstCfg.mode == modExtrude)) {
      units = glob->extrUnits;
      _dos *= 3.6;
      dosFmt = glob->formats.DosageKgHFmt;
    }
    else {
      dosFmt = glob->formats.DosageFmt;
      units = glob->shotUnits;
    }
    break;
  }

  if(_dos != CLIP_FLOAT) {
    text = QString::number(_dos,dosFmt.format.toAscii(),dosFmt.precision)+QString(" ")+units;
  }
  else {
    text = CLIP_FLOAT_STR;
  }

  switch(dispType) {
  case DISP_TYPE_SINGLE_UNIT:
    if(idx==0) {
      dosAct1->setText(text);
    }
    break;

  case DISP_TYPE_TWIN:
    switch(idx) {
    case 0:
      dosAct1->setText(text);
      break;

    case 1:
      dosAct2->setText(text);
      break;
    }
    break;

  case DISP_TYPE_MULTI_UNIT:
    if (idx == dispIdx) {
      dosAct1->setText(text);
    }
    break;
  }
}


void MvcSystem::SetDosSet(float _dos, int idx)
{
  /*------------------------------*/
  /* Refresh the Dosage Set value */
  /*------------------------------*/
  QString text;
  QString units;
  ItemFormatStruct dosFmt;

  switch (Rout::ActUnitType()) {

  case mcdtBalanceHO:
    dosFmt = glob->formats.DosageKgHFmt;
    units = glob->extrUnits;
    _dos *= 3.6;
    break;

  default :
    /*----- Convert gr/s to kg/h if enabled and in extrusion mode -----*/
    if ((glob->mstCfg.units_kgh == true) && (glob->mstCfg.mode == modExtrude)) {
      units = glob->extrUnits;
      _dos *= 3.6;
      dosFmt = glob->formats.DosageKgHFmt;
    }
    else {
      dosFmt = glob->formats.DosageFmt;
      units = glob->shotUnits;
    }
    break;
  }


  if(_dos != CLIP_FLOAT) {
    text = QString::number(_dos,dosFmt.format.toAscii(),dosFmt.precision)+QString(" ")+units;
  }
  else {
    text = CLIP_FLOAT_STR;
  }

  switch(dispType) {
  case DISP_TYPE_SINGLE_UNIT:
    if (idx == 0) {
      dosSet1->setText(text);
    }
    break;

  case DISP_TYPE_TWIN:
    switch(idx) {
    case 0:
      dosSet1->setText(text);
      break;

    case 1:
      dosSet2->setText(text);
      break;
    }
    break;

  case DISP_TYPE_MULTI_UNIT:
    if (idx == dispIdx) {
      dosSet1->setText(text);
    }
    break;
  }
}


void MvcSystem::GraviRpmChanged(eGraviRpm gr, int idx)
{
  switch(dispType) {
  case DISP_TYPE_SINGLE_UNIT:
    if (idx == 0) {
      if (gr == grGravi) {
        SetColorPct(glob->sysCfg[0].colPct,idx);
      }
      else {
        SetRPM(glob->sysCfg[0].setRpm,idx);
      }
    }
    break;

  case DISP_TYPE_TWIN:
  case DISP_TYPE_MULTI_UNIT:
    if (idx <= glob->mstCfg.slaveCount) {
      if (gr == grGravi) {
        SetColorPct(glob->sysCfg[idx].colPct,idx);
      }
      else {
        SetRPM(glob->sysCfg[idx].setRpm,idx);
      }
    }
    break;
  }
}


void MvcSystem::Mat1Clicked()
{
  /*---------------------------*/
  /* Material 1 button clicked */
  /*---------------------------*/
  if (!Rout::keyLock) {                                                         /* Keyboard lock not active */
    emit Material1Clicked();
  }
}


void MvcSystem::Mat2Clicked()
{
  /*---------------------------*/
  /* Material 2 button clicked */
  /*---------------------------*/
  if (!Rout::keyLock) {                                                         /* Keyboard lock not active */
    emit Material2Clicked();
  }
}


void MvcSystem::UnitIndexChanged(int idx)
{
  dispIdx = idx;
  InitValues();
  update();
}

void MvcSystem::ComMsgReceived(tMsgControlUnit msg,int idx)
{
  /*--------------------------------------------*/
  /* Comm message received, update dynamic data */
  /*--------------------------------------------*/
  QString statusText;

  if (idx == glob->ActUnitIndex()) {

    if (slideStatus->isVisible()) {         /* Slide status visible ? */

      /*----- Slide status -----*/
      if (glob->sysSts[idx].actIO.inputs[1]) {
        slideStatus->setText(tr("Sl.Closed"));
      }
      else {
        slideStatus->setText(tr("Sl.Open"));
      }
    }
  }
}


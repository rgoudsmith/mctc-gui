/********************************************************************************
** Form generated from reading UI file 'Form_AlarmConfigItem.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_ALARMCONFIGITEM_H
#define UI_FORM_ALARMCONFIGITEM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AlarmConfigItemFrm
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QLabel *lbNr;
    QLabel *lbDescript;
    QPushButton *btType;

    void setupUi(QWidget *AlarmConfigItemFrm)
    {
        if (AlarmConfigItemFrm->objectName().isEmpty())
            AlarmConfigItemFrm->setObjectName(QString::fromUtf8("AlarmConfigItemFrm"));
        AlarmConfigItemFrm->resize(595, 90);
        AlarmConfigItemFrm->setMinimumSize(QSize(0, 90));
        AlarmConfigItemFrm->setMaximumSize(QSize(16777215, 90));
        AlarmConfigItemFrm->setCursor(QCursor(Qt::BlankCursor));
        AlarmConfigItemFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout = new QVBoxLayout(AlarmConfigItemFrm);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame = new QFrame(AlarmConfigItemFrm);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(0, 0));
        frame->setMaximumSize(QSize(16777215, 16777215));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lbNr = new QLabel(frame);
        lbNr->setObjectName(QString::fromUtf8("lbNr"));
        lbNr->setMinimumSize(QSize(70, 0));
        lbNr->setMaximumSize(QSize(70, 16777215));
        lbNr->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(lbNr);

        lbDescript = new QLabel(frame);
        lbDescript->setObjectName(QString::fromUtf8("lbDescript"));

        horizontalLayout->addWidget(lbDescript);

        btType = new QPushButton(frame);
        btType->setObjectName(QString::fromUtf8("btType"));
        btType->setMinimumSize(QSize(90, 70));
        btType->setMaximumSize(QSize(90, 70));
        btType->setText(QString::fromUtf8(""));

        horizontalLayout->addWidget(btType);


        verticalLayout->addWidget(frame);


        retranslateUi(AlarmConfigItemFrm);

        QMetaObject::connectSlotsByName(AlarmConfigItemFrm);
    } // setupUi

    void retranslateUi(QWidget *AlarmConfigItemFrm)
    {
        lbNr->setText(QApplication::translate("AlarmConfigItemFrm", "[nr]", 0, QApplication::UnicodeUTF8));
        lbDescript->setText(QApplication::translate("AlarmConfigItemFrm", "TextLabel", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(AlarmConfigItemFrm);
    } // retranslateUi

};

namespace Ui {
    class AlarmConfigItemFrm: public Ui_AlarmConfigItemFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_ALARMCONFIGITEM_H

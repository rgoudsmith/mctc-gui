#ifndef ALARMCONFIGFRM_H
#define ALARMCONFIGFRM_H

#include <QVBoxLayout>
#include "Form_QbaseWidget.h"

namespace Ui
{
  class AlarmConfigFrm;
}


class AlarmConfigFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit AlarmConfigFrm(QWidget *parent = 0);
    ~AlarmConfigFrm();
    void FormInit();

  protected:
    void resizeEvent(QResizeEvent *);
    void showEvent(QShowEvent *);

  private:
    Ui::AlarmConfigFrm *ui;

    QVBoxLayout * layout;

    void AddConfigItems();
    int ItemCount();

  private slots:
    void OkClicked();
};
#endif                                                                          // ALARMCONFIGFRM_H

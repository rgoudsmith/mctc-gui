/****************************************************************************
** Meta object code from reading C++ file 'Form_IpEditor.h'
**
** Created: Wed Jan 4 16:18:27 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_IpEditor.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_IpEditor.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_IpEditorFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   13,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      41,   12,   12,   12, 0x08,
      53,   12,   12,   12, 0x08,
      69,   12,   12,   12, 0x08,
      82,   12,   12,   12, 0x08,
      96,   12,   12,   12, 0x08,
     114,   12,   12,   12, 0x08,
     135,  130,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_IpEditorFrm[] = {
    "IpEditorFrm\0\0,,,\0SendIp(int,int,int,int)\0"
    "OkClicked()\0CancelClicked()\0DotClicked()\0"
    "BackClicked()\0DisplayValue(int)\0"
    "ButtonClicked()\0,,,,\0InitIp(int,int,int,int,int)\0"
};

const QMetaObject IpEditorFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_IpEditorFrm,
      qt_meta_data_IpEditorFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &IpEditorFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *IpEditorFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *IpEditorFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_IpEditorFrm))
        return static_cast<void*>(const_cast< IpEditorFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int IpEditorFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: SendIp((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 1: OkClicked(); break;
        case 2: CancelClicked(); break;
        case 3: DotClicked(); break;
        case 4: BackClicked(); break;
        case 5: DisplayValue((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: ButtonClicked(); break;
        case 7: InitIp((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        default: ;
        }
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void IpEditorFrm::SendIp(int _t1, int _t2, int _t3, int _t4)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

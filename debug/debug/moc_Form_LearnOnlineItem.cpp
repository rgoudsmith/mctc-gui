/****************************************************************************
** Meta object code from reading C++ file 'Form_LearnOnlineItem.h'
**
** Created: Wed Jan 4 16:19:06 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_LearnOnlineItem.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_LearnOnlineItem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LearnOnlineItemFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   19,   19,   19, 0x08,
      36,   19,   19,   19, 0x08,
      56,   19,   19,   19, 0x08,
      80,   19,   19,   19, 0x08,
      99,   19,   19,   19, 0x08,
     111,   19,   19,   19, 0x08,
     129,   19,   19,   19, 0x0a,
     147,   19,   19,   19, 0x0a,
     167,   19,   19,   19, 0x0a,
     191,  189,   19,   19, 0x0a,
     220,  189,   19,   19, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_LearnOnlineItemFrm[] = {
    "LearnOnlineItemFrm\0\0ButtonClicked()\0"
    "StoreMaterialData()\0StoreRecipeLineCorFac()\0"
    "SetImage(eDosTool)\0ShowInput()\0"
    "SetMaterialName()\0SetUnitIndex(int)\0"
    "ResultReceived(int)\0TextReceived(QString)\0"
    ",\0MaterialChanged(QString,int)\0"
    "DeviceIdChanged(QString,int)\0"
};

const QMetaObject LearnOnlineItemFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_LearnOnlineItemFrm,
      qt_meta_data_LearnOnlineItemFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LearnOnlineItemFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LearnOnlineItemFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LearnOnlineItemFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LearnOnlineItemFrm))
        return static_cast<void*>(const_cast< LearnOnlineItemFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int LearnOnlineItemFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: ButtonClicked(); break;
        case 1: StoreMaterialData(); break;
        case 2: StoreRecipeLineCorFac(); break;
        case 3: SetImage((*reinterpret_cast< eDosTool(*)>(_a[1]))); break;
        case 4: ShowInput(); break;
        case 5: SetMaterialName(); break;
        case 6: SetUnitIndex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: ResultReceived((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: TextReceived((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: MaterialChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 10: DeviceIdChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 11;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

/****************************************************************************
** Meta object code from reading C++ file 'Form_Error.h'
**
** Created: Wed Jan 4 16:19:04 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_Error.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_Error.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ErrorFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      29,    9,    9,    9, 0x08,
      49,    9,    9,    9, 0x08,
      62,    9,    9,    9, 0x08,
      75,    9,    9,    9, 0x08,
      88,    9,    9,    9, 0x08,
     101,    9,    9,    9, 0x08,
     116,  114,    9,    9, 0x08,
     145,  114,    9,    9, 0x08,
     171,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ErrorFrm[] = {
    "ErrorFrm\0\0ButtonClicked(int)\0"
    "on_btStop_pressed()\0OnCloseTim()\0"
    "bt1Clicked()\0bt2Clicked()\0bt3Clicked()\0"
    "bt4Clicked()\0,\0AlarmStatusChanged(bool,int)\0"
    "AlarmModeChanged(int,int)\0"
    "SysActiveChanged(bool)\0"
};

const QMetaObject ErrorFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_ErrorFrm,
      qt_meta_data_ErrorFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ErrorFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ErrorFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ErrorFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ErrorFrm))
        return static_cast<void*>(const_cast< ErrorFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int ErrorFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: ButtonClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: on_btStop_pressed(); break;
        case 2: OnCloseTim(); break;
        case 3: bt1Clicked(); break;
        case 4: bt2Clicked(); break;
        case 5: bt3Clicked(); break;
        case 6: bt4Clicked(); break;
        case 7: AlarmStatusChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: AlarmModeChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 9: SysActiveChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void ErrorFrm::ButtonClicked(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

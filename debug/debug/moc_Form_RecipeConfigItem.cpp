/****************************************************************************
** Meta object code from reading C++ file 'Form_RecipeConfigItem.h'
**
** Created: Wed Jan 4 16:18:43 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_RecipeConfigItem.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_RecipeConfigItem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RecipeConfigItemFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x05,
      38,   20,   20,   20, 0x05,

 // slots: signature, parameters, type, tag, flags
      59,   20,   20,   20, 0x08,
      78,   76,   20,   20, 0x0a,
     110,   76,   20,   20, 0x0a,
     142,  139,   20,   20, 0x0a,
     177,   76,   20,   20, 0x0a,
     202,   20,   20,   20, 0x0a,
     232,   20,   20,   20, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RecipeConfigItemFrm[] = {
    "RecipeConfigItemFrm\0\0LineChanged(int)\0"
    "MaterialClicked(int)\0SetColPct(float)\0"
    ",\0SetRecipe(RecipeBufStruct*,int)\0"
    "DeviceIdChanged(QString,int)\0,,\0"
    "MaterialReceived(QString,bool,int)\0"
    "ValueReceived(float,int)\0"
    "SetNumInput(NumericInputFrm*)\0"
    "UpdateFields()\0"
};

const QMetaObject RecipeConfigItemFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_RecipeConfigItemFrm,
      qt_meta_data_RecipeConfigItemFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RecipeConfigItemFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RecipeConfigItemFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RecipeConfigItemFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RecipeConfigItemFrm))
        return static_cast<void*>(const_cast< RecipeConfigItemFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int RecipeConfigItemFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: LineChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: MaterialClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: SetColPct((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 3: SetRecipe((*reinterpret_cast< RecipeBufStruct*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: DeviceIdChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: MaterialReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 6: ValueReceived((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: SetNumInput((*reinterpret_cast< NumericInputFrm*(*)>(_a[1]))); break;
        case 8: UpdateFields(); break;
        default: ;
        }
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void RecipeConfigItemFrm::LineChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void RecipeConfigItemFrm::MaterialClicked(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE

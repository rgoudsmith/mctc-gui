/****************************************************************************
** Meta object code from reading C++ file 'TfileHandler.h'
**
** Created: Wed Jan 4 16:18:49 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../TfileHandler.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TfileHandler.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TFileHandler[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x0a,
      29,   13,   13,   13, 0x0a,
      55,   51,   13,   13, 0x0a,
      92,   90,   85,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_TFileHandler[] = {
    "TFileHandler\0\0GetIpAddress()\0"
    "SetIpAddress(QString)\0,,,\0"
    "SetIpAddress(int,int,int,int)\0bool\0,\0"
    "CheckUpdateArchive(QString,QString*)\0"
};

const QMetaObject TFileHandler::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_TFileHandler,
      qt_meta_data_TFileHandler, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TFileHandler::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TFileHandler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TFileHandler::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TFileHandler))
        return static_cast<void*>(const_cast< TFileHandler*>(this));
    return QObject::qt_metacast(_clname);
}

int TFileHandler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: GetIpAddress(); break;
        case 1: SetIpAddress((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: SetIpAddress((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 3: { bool _r = CheckUpdateArchive((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QString*(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

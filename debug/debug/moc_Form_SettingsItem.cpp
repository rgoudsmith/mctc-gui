/****************************************************************************
** Meta object code from reading C++ file 'Form_SettingsItem.h'
**
** Created: Wed Jan 4 16:19:00 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_SettingsItem.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_SettingsItem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SettingsItemFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x05,
      38,   16,   16,   16, 0x05,
      61,   16,   16,   16, 0x05,
      82,   16,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
     105,   16,   16,   16, 0x0a,
     123,   16,   16,   16, 0x0a,
     150,  146,   16,   16, 0x0a,
     181,  176,   16,   16, 0x0a,
     219,   16,   16,   16, 0x0a,
     237,   16,   16,   16, 0x0a,
     260,  146,   16,   16, 0x0a,
     286,  176,   16,   16, 0x0a,
     324,   16,   16,   16, 0x0a,
     342,   16,   16,   16, 0x0a,
     357,   16,   16,   16, 0x0a,
     374,  372,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_SettingsItemFrm[] = {
    "SettingsItemFrm\0\0SetValueItem1(float)\0"
    "SetValueItem1(QString)\0SetValueItem2(float)\0"
    "SetValueItem2(QString)\0ShowOneItem(bool)\0"
    "SetItem1Title(QString)\0,,,\0"
    "SetItem1(int,int,int,int)\0,,,,\0"
    "SetItem1(float,float,float,float,int)\0"
    "SetItem1(QString)\0SetItem2Title(QString)\0"
    "SetItem2(int,int,int,int)\0"
    "SetItem2(float,float,float,float,int)\0"
    "SetItem2(QString)\0Item1Clicked()\0"
    "Item2Clicked()\0,\0ValueReceived(float,int)\0"
};

const QMetaObject SettingsItemFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_SettingsItemFrm,
      qt_meta_data_SettingsItemFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SettingsItemFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SettingsItemFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SettingsItemFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SettingsItemFrm))
        return static_cast<void*>(const_cast< SettingsItemFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int SettingsItemFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: SetValueItem1((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 1: SetValueItem1((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: SetValueItem2((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 3: SetValueItem2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: ShowOneItem((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: SetItem1Title((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: SetItem1((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 7: SetItem1((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])),(*reinterpret_cast< float(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 8: SetItem1((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: SetItem2Title((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: SetItem2((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 11: SetItem2((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])),(*reinterpret_cast< float(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 12: SetItem2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 13: Item1Clicked(); break;
        case 14: Item2Clicked(); break;
        case 15: ValueReceived((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void SettingsItemFrm::SetValueItem1(float _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void SettingsItemFrm::SetValueItem1(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void SettingsItemFrm::SetValueItem2(float _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void SettingsItemFrm::SetValueItem2(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE

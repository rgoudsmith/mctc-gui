/****************************************************************************
** Meta object code from reading C++ file 'Form_MaterialSelect.h'
**
** Created: Wed Jan 4 16:18:30 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_MaterialSelect.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_MaterialSelect.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MaterialSelectFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   19,   18,   18, 0x05,

 // slots: signature, parameters, type, tag, flags
      52,   18,   18,   18, 0x08,
      73,   18,   18,   18, 0x08,
      86,   18,   18,   18, 0x08,
     102,   18,   18,   18, 0x08,
     113,   18,   18,   18, 0x08,
     126,   18,   18,   18, 0x08,
     142,   18,   18,   18, 0x08,
     158,   18,   18,   18, 0x08,
     170,   18,   18,   18, 0x08,
     189,   18,   18,   18, 0x08,
     211,   18,   18,   18, 0x08,
     234,   18,   18,   18, 0x0a,
     258,   18,   18,   18, 0x0a,
     287,   18,   18,   18, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MaterialSelectFrm[] = {
    "MaterialSelectFrm\0\0,\0"
    "MaterialSelected(QString,bool)\0"
    "DefaultMatsClicked()\0NewClicked()\0"
    "DeleteClicked()\0ScrollUp()\0ScrollDown()\0"
    "CancelClicked()\0RenameClicked()\0"
    "OkClicked()\0MessageResult(int)\0"
    "TextReceived(QString)\0SysActiveChanged(bool)\0"
    "ActiveIndexChanged(int)\0"
    "SetSelectedMaterial(QString)\0"
    "CfgCurrentUserChanged(eUserType)\0"
};

const QMetaObject MaterialSelectFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_MaterialSelectFrm,
      qt_meta_data_MaterialSelectFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MaterialSelectFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MaterialSelectFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MaterialSelectFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MaterialSelectFrm))
        return static_cast<void*>(const_cast< MaterialSelectFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int MaterialSelectFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: MaterialSelected((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 1: DefaultMatsClicked(); break;
        case 2: NewClicked(); break;
        case 3: DeleteClicked(); break;
        case 4: ScrollUp(); break;
        case 5: ScrollDown(); break;
        case 6: CancelClicked(); break;
        case 7: RenameClicked(); break;
        case 8: OkClicked(); break;
        case 9: MessageResult((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: TextReceived((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: SysActiveChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: ActiveIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: SetSelectedMaterial((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 14: CfgCurrentUserChanged((*reinterpret_cast< eUserType(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 15;
    }
    return _id;
}

// SIGNAL 0
void MaterialSelectFrm::MaterialSelected(const QString & _t1, bool _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

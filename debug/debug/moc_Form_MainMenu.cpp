/****************************************************************************
** Meta object code from reading C++ file 'Form_MainMenu.h'
**
** Created: Wed Jan 4 16:19:10 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_MainMenu.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_MainMenu.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainMenu[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,
      24,    9,    9,    9, 0x05,
      43,   41,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      71,    9,    9,    9, 0x08,
      85,    9,    9,    9, 0x08,
     101,   41,    9,    9, 0x08,
     132,    9,    9,    9, 0x08,
     151,    9,    9,    9, 0x08,
     173,    9,    9,    9, 0x08,
     217,  198,    9,    9, 0x0a,
     249,  240,    9,    9, 0x2a,
     268,    9,    9,    9, 0x0a,
     291,    9,    9,    9, 0x0a,
     305,    9,    9,    9, 0x0a,
     320,    9,    9,    9, 0x0a,
     335,    9,    9,    9, 0x0a,
     350,    9,    9,    9, 0x0a,
     373,    9,    9,    9, 0x0a,
     410,  402,  398,    9, 0x0a,
     449,  446,  398,    9, 0x2a,
     481,  446,  398,    9, 0x0a,
     517,  513,    9,    9, 0x0a,
     553,    9,    9,    9, 0x0a,
     574,   41,    9,    9, 0x0a,
     603,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MainMenu[] = {
    "MainMenu\0\0MenuClicked()\0ErrorResult(int)\0"
    ",\0PrimeActiveChange(bool,int)\0"
    "BackClicked()\0btMenuClicked()\0"
    "McStatusChanged(eMCStatus,int)\0"
    "ReturnHomeScreen()\0StoreMasterSettings()\0"
    "StoreSystemSettings(int)\0windowNr,calWindow\0"
    "DisplayWindow(int,int)\0windowNr\0"
    "DisplayWindow(int)\0TimeChanged(QDateTime)\0"
    "HomeClicked()\0AlarmClicked()\0"
    "PrimeClicked()\0OnOffClicked()\0"
    "SysActiveChanged(bool)\0ErrorResultReceived(int)\0"
    "int\0,,,type\0DisplayMessage(int,int,QString,int)\0"
    ",,\0DisplayMessage(int,int,QString)\0"
    "DisplayMessage(int,QString,int)\0,,,\0"
    "DisplayMessage(QString,int,int,int)\0"
    "ActiveAlarmHandler()\0PrimeActiveChanged(bool,int)\0"
    "CfgCurrentUserChanged(eUserType)\0"
};

const QMetaObject MainMenu::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_MainMenu,
      qt_meta_data_MainMenu, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainMenu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainMenu))
        return static_cast<void*>(const_cast< MainMenu*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int MainMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: MenuClicked(); break;
        case 1: ErrorResult((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: PrimeActiveChange((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: BackClicked(); break;
        case 4: btMenuClicked(); break;
        case 5: McStatusChanged((*reinterpret_cast< eMCStatus(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: ReturnHomeScreen(); break;
        case 7: StoreMasterSettings(); break;
        case 8: StoreSystemSettings((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: DisplayWindow((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 10: DisplayWindow((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: TimeChanged((*reinterpret_cast< const QDateTime(*)>(_a[1]))); break;
        case 12: HomeClicked(); break;
        case 13: AlarmClicked(); break;
        case 14: PrimeClicked(); break;
        case 15: OnOffClicked(); break;
        case 16: SysActiveChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 17: ErrorResultReceived((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: { int _r = DisplayMessage((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 19: { int _r = DisplayMessage((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 20: { int _r = DisplayMessage((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 21: DisplayMessage((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 22: ActiveAlarmHandler(); break;
        case 23: PrimeActiveChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 24: CfgCurrentUserChanged((*reinterpret_cast< eUserType(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 25;
    }
    return _id;
}

// SIGNAL 0
void MainMenu::MenuClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void MainMenu::ErrorResult(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainMenu::PrimeActiveChange(bool _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE

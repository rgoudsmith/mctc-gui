/****************************************************************************
** Meta object code from reading C++ file 'UsbAccess.h'
**
** Created: Wed Jan 4 16:19:19 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../UsbAccess.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'UsbAccess.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_UsbAccess[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,
      36,   34,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      70,   10,   10,   10, 0x0a,
      92,   10,   88,   10, 0x0a,
     121,   10,   10,   10, 0x0a,
     146,   10,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_UsbAccess[] = {
    "UsbAccess\0\0UsbFirmwareStartScan()\0,\0"
    "UsbFirmwareFound(QString,QString)\0"
    "ScanForFirmware()\0int\0"
    "StartFirmwareUpdate(QString)\0"
    "SetUsbMountPath(QString)\0SetUsbFolder(QString)\0"
};

const QMetaObject UsbAccess::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_UsbAccess,
      qt_meta_data_UsbAccess, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &UsbAccess::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *UsbAccess::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *UsbAccess::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_UsbAccess))
        return static_cast<void*>(const_cast< UsbAccess*>(this));
    return QObject::qt_metacast(_clname);
}

int UsbAccess::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: UsbFirmwareStartScan(); break;
        case 1: UsbFirmwareFound((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 2: ScanForFirmware(); break;
        case 3: { int _r = StartFirmwareUpdate((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 4: SetUsbMountPath((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: SetUsbFolder((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void UsbAccess::UsbFirmwareStartScan()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void UsbAccess::UsbFirmwareFound(const QString & _t1, const QString & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE

/****************************************************************************
** Meta object code from reading C++ file 'Form_Prime.h'
**
** Created: Wed Jan 4 16:18:36 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_Prime.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_Prime.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PrimeFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x08,
      27,    9,    9,    9, 0x08,
      45,    9,    9,    9, 0x08,
      68,   66,    9,    9, 0x08,
      95,   66,    9,    9, 0x08,
     121,   66,    9,    9, 0x08,
     150,    9,    9,    9, 0x08,
     162,   66,    9,    9, 0x08,
     188,   66,    9,    9, 0x08,
     215,    9,    9,    9, 0x0a,
     240,    9,    9,    9, 0x0a,
     263,   66,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_PrimeFrm[] = {
    "PrimeFrm\0\0btStartClicked()\0btCancelClicked()\0"
    "ValueReceived(float)\0,\0"
    "PrimeRpmChanged(float,int)\0"
    "PrimeTimeChanged(int,int)\0"
    "UnitNameChanged(QString,int)\0TimUpdate()\0"
    "TestRpmChanged(float,int)\0"
    "TestTimeChanged(float,int)\0"
    "ActUnitIndexChanged(int)\0"
    "SlaveCountChanged(int)\0"
    "PrimeStatusChanged(bool,int)\0"
};

const QMetaObject PrimeFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_PrimeFrm,
      qt_meta_data_PrimeFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PrimeFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PrimeFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PrimeFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PrimeFrm))
        return static_cast<void*>(const_cast< PrimeFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int PrimeFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: btStartClicked(); break;
        case 1: btCancelClicked(); break;
        case 2: ValueReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 3: PrimeRpmChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: PrimeTimeChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: UnitNameChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: TimUpdate(); break;
        case 7: TestRpmChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: TestTimeChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 9: ActUnitIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: SlaveCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: PrimeStatusChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 12;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

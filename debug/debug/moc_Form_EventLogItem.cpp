/****************************************************************************
** Meta object code from reading C++ file 'Form_EventLogItem.h'
**
** Created: Wed Jan 4 16:18:20 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_EventLogItem.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_EventLogItem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_EventLogItemFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x05,
      34,   16,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
      48,   16,   16,   16, 0x08,
      62,   16,   16,   16, 0x0a,
      80,   16,   16,   16, 0x0a,
     109,   16,   16,   16, 0x0a,
     130,  128,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_EventLogItemFrm[] = {
    "EventLogItemFrm\0\0ItemPress(ulong)\0"
    "SizeChanged()\0DisplayData()\0"
    "SetEventID(ulong)\0SetData(EventLogItemStruct*)\0"
    "ItemPressed(ulong)\0,\0UnitIDChanged(QString,int)\0"
};

const QMetaObject EventLogItemFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_EventLogItemFrm,
      qt_meta_data_EventLogItemFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &EventLogItemFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *EventLogItemFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *EventLogItemFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EventLogItemFrm))
        return static_cast<void*>(const_cast< EventLogItemFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int EventLogItemFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: ItemPress((*reinterpret_cast< ulong(*)>(_a[1]))); break;
        case 1: SizeChanged(); break;
        case 2: DisplayData(); break;
        case 3: SetEventID((*reinterpret_cast< ulong(*)>(_a[1]))); break;
        case 4: SetData((*reinterpret_cast< EventLogItemStruct*(*)>(_a[1]))); break;
        case 5: ItemPressed((*reinterpret_cast< ulong(*)>(_a[1]))); break;
        case 6: UnitIDChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void EventLogItemFrm::ItemPress(unsigned long _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void EventLogItemFrm::SizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE

/****************************************************************************
** Meta object code from reading C++ file 'Form_ProcessDiagnostic.h'
**
** Created: Wed Jan 4 16:18:37 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_ProcessDiagnostic.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_ProcessDiagnostic.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ProcessDiagnosticFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      24,   22,   21,   21, 0x0a,
      56,   21,   21,   21, 0x0a,
      79,   21,   21,   21, 0x0a,
      91,   22,   21,   21, 0x0a,
     116,   22,   21,   21, 0x0a,
     147,   22,   21,   21, 0x0a,
     178,   22,   21,   21, 0x0a,
     208,   22,   21,   21, 0x0a,
     238,   22,   21,   21, 0x0a,
     266,   22,   21,   21, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ProcessDiagnosticFrm[] = {
    "ProcessDiagnosticFrm\0\0,\0"
    "DiagDataReceived(MsgDataIn,int)\0"
    "SlaveCountChanged(int)\0OkClicked()\0"
    "MCIDChanged(QString,int)\0"
    "McStatusChanged(eMCStatus,int)\0"
    "HopperWeightChanged(float,int)\0"
    "ColorPctSetChanged(float,int)\0"
    "ColorPctActChanged(float,int)\0"
    "CalibratedChanged(bool,int)\0"
    "SysCfg_RpmChanged(float,int)\0"
};

const QMetaObject ProcessDiagnosticFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_ProcessDiagnosticFrm,
      qt_meta_data_ProcessDiagnosticFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ProcessDiagnosticFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ProcessDiagnosticFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ProcessDiagnosticFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ProcessDiagnosticFrm))
        return static_cast<void*>(const_cast< ProcessDiagnosticFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int ProcessDiagnosticFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: DiagDataReceived((*reinterpret_cast< MsgDataIn(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: SlaveCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: OkClicked(); break;
        case 3: MCIDChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: McStatusChanged((*reinterpret_cast< eMCStatus(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: HopperWeightChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: ColorPctSetChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: ColorPctActChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: CalibratedChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 9: SysCfg_RpmChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

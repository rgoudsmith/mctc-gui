/****************************************************************************
** Meta object code from reading C++ file 'Form_EventLogHistory.h'
**
** Created: Wed Jan 4 16:18:19 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_EventLogHistory.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_EventLogHistory.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_EventLogHistoryFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   19,   19,   19, 0x05,

 // slots: signature, parameters, type, tag, flags
      37,   19,   19,   19, 0x08,
      49,   19,   19,   19, 0x08,
      64,   19,   19,   19, 0x08,
      76,   19,   19,   19, 0x08,
      94,   19,   19,   19, 0x08,
     109,   19,   19,   19, 0x08,
     122,   19,   19,   19, 0x0a,
     150,   19,   19,   19, 0x0a,
     169,   19,   19,   19, 0x0a,
     187,   19,   19,   19, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_EventLogHistoryFrm[] = {
    "EventLogHistoryFrm\0\0ItemPress(ulong)\0"
    "OkClicked()\0ResetClicked()\0InitItems()\0"
    "AddNewItem(ulong)\0LoadAllItems()\0"
    "ClearItems()\0EventNewItemReceived(ulong)\0"
    "ItemPressed(ulong)\0ItemSizeChanged()\0"
    "ShowColor()\0"
};

const QMetaObject EventLogHistoryFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_EventLogHistoryFrm,
      qt_meta_data_EventLogHistoryFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &EventLogHistoryFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *EventLogHistoryFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *EventLogHistoryFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EventLogHistoryFrm))
        return static_cast<void*>(const_cast< EventLogHistoryFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int EventLogHistoryFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: ItemPress((*reinterpret_cast< ulong(*)>(_a[1]))); break;
        case 1: OkClicked(); break;
        case 2: ResetClicked(); break;
        case 3: InitItems(); break;
        case 4: AddNewItem((*reinterpret_cast< ulong(*)>(_a[1]))); break;
        case 5: LoadAllItems(); break;
        case 6: ClearItems(); break;
        case 7: EventNewItemReceived((*reinterpret_cast< ulong(*)>(_a[1]))); break;
        case 8: ItemPressed((*reinterpret_cast< ulong(*)>(_a[1]))); break;
        case 9: ItemSizeChanged(); break;
        case 10: ShowColor(); break;
        default: ;
        }
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void EventLogHistoryFrm::ItemPress(unsigned long _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

/****************************************************************************
** Meta object code from reading C++ file 'Form_MultiComponentItem.h'
**
** Created: Wed Jan 4 16:18:31 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_MultiComponentItem.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_MultiComponentItem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MultiComponentItemFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      25,   23,   22,   22, 0x05,
      61,   23,   22,   22, 0x05,
      87,   22,   22,   22, 0x05,

 // slots: signature, parameters, type, tag, flags
     106,   22,   22,   22, 0x08,
     123,   23,   22,   22, 0x0a,
     152,   23,   22,   22, 0x0a,
     185,   23,   22,   22, 0x0a,
     219,   23,   22,   22, 0x0a,
     244,   23,   22,   22, 0x0a,
     276,   23,   22,   22, 0x0a,
     308,   23,   22,   22, 0x0a,
     335,   22,   22,   22, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MultiComponentItemFrm[] = {
    "MultiComponentItemFrm\0\0,\0"
    "CfgUnitStatusChanged(eMCStatus,int)\0"
    "CfgSetColorPct(float,int)\0ColPctClicked(int)\0"
    "btOnOffClicked()\0UnitNameChanged(QString,int)\0"
    "UnitStatusChanged(eMCStatus,int)\0"
    "SysCfg_ColorPctChanged(float,int)\0"
    "ExtCapChanged(float,int)\0"
    "SysSts_DosActChanged(float,int)\0"
    "SysCfg_DosSetChanged(float,int)\0"
    "SysActiveChanged(bool,int)\0ImageClicked()\0"
};

const QMetaObject MultiComponentItemFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_MultiComponentItemFrm,
      qt_meta_data_MultiComponentItemFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MultiComponentItemFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MultiComponentItemFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MultiComponentItemFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MultiComponentItemFrm))
        return static_cast<void*>(const_cast< MultiComponentItemFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int MultiComponentItemFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: CfgUnitStatusChanged((*reinterpret_cast< eMCStatus(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: CfgSetColorPct((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: ColPctClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: btOnOffClicked(); break;
        case 4: UnitNameChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: UnitStatusChanged((*reinterpret_cast< eMCStatus(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: SysCfg_ColorPctChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: ExtCapChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: SysSts_DosActChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 9: SysCfg_DosSetChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 10: SysActiveChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 11: ImageClicked(); break;
        default: ;
        }
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void MultiComponentItemFrm::CfgUnitStatusChanged(eMCStatus _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MultiComponentItemFrm::CfgSetColorPct(float _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MultiComponentItemFrm::ColPctClicked(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE

/****************************************************************************
** Meta object code from reading C++ file 'Form_DateTimeEditor.h'
**
** Created: Wed Jan 4 16:18:16 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_DateTimeEditor.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_DateTimeEditor.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DateTimeEditorFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x05,

 // slots: signature, parameters, type, tag, flags
      44,   18,   18,   18, 0x08,
      56,   18,   18,   18, 0x08,
      72,   18,   18,   18, 0x08,
      88,   18,   18,   18, 0x08,
     106,   18,   18,   18, 0x08,
     124,   18,   18,   18, 0x08,
     144,   18,   18,   18, 0x08,
     159,   18,   18,   18, 0x08,
     176,   18,   18,   18, 0x08,
     193,   18,   18,   18, 0x08,
     212,   18,   18,   18, 0x08,
     228,   18,   18,   18, 0x08,
     246,   18,   18,   18, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DateTimeEditorFrm[] = {
    "DateTimeEditorFrm\0\0DateTimeChanged(QString)\0"
    "OkClicked()\0CancelClicked()\0HourUpClicked()\0"
    "HourDownClicked()\0MinuteUpClicked()\0"
    "MinuteDownClicked()\0DayUpClicked()\0"
    "DayDownClicked()\0MonthUpClicked()\0"
    "MonthDownClicked()\0YearUpClicked()\0"
    "YearDownClicked()\0ShowDialog()\0"
};

const QMetaObject DateTimeEditorFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_DateTimeEditorFrm,
      qt_meta_data_DateTimeEditorFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DateTimeEditorFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DateTimeEditorFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DateTimeEditorFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DateTimeEditorFrm))
        return static_cast<void*>(const_cast< DateTimeEditorFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int DateTimeEditorFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: DateTimeChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: OkClicked(); break;
        case 2: CancelClicked(); break;
        case 3: HourUpClicked(); break;
        case 4: HourDownClicked(); break;
        case 5: MinuteUpClicked(); break;
        case 6: MinuteDownClicked(); break;
        case 7: DayUpClicked(); break;
        case 8: DayDownClicked(); break;
        case 9: MonthUpClicked(); break;
        case 10: MonthDownClicked(); break;
        case 11: YearUpClicked(); break;
        case 12: YearDownClicked(); break;
        case 13: ShowDialog(); break;
        default: ;
        }
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void DateTimeEditorFrm::DateTimeChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

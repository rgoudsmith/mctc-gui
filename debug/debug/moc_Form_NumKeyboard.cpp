/****************************************************************************
** Meta object code from reading C++ file 'Form_NumKeyboard.h'
**
** Created: Wed Jan 4 16:18:35 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_NumKeyboard.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_NumKeyboard.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_NumKeyboardFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x05,
      34,   15,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      53,   15,   15,   15, 0x08,
      69,   15,   15,   15, 0x08,
      82,   15,   15,   15, 0x08,
      97,   15,   15,   15, 0x08,
     116,   15,   15,   15, 0x08,
     130,   15,   15,   15, 0x08,
     142,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_NumKeyboardFrm[] = {
    "NumKeyboardFrm\0\0InputValue(float)\0"
    "PassInput(QString)\0ButtonClicked()\0"
    "DotClicked()\0ClearClicked()\0"
    "BackspaceClicked()\0SignClicked()\0"
    "OkClicked()\0CancelClicked()\0"
};

const QMetaObject NumKeyboardFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_NumKeyboardFrm,
      qt_meta_data_NumKeyboardFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &NumKeyboardFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *NumKeyboardFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *NumKeyboardFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_NumKeyboardFrm))
        return static_cast<void*>(const_cast< NumKeyboardFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int NumKeyboardFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: InputValue((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 1: PassInput((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: ButtonClicked(); break;
        case 3: DotClicked(); break;
        case 4: ClearClicked(); break;
        case 5: BackspaceClicked(); break;
        case 6: SignClicked(); break;
        case 7: OkClicked(); break;
        case 8: CancelClicked(); break;
        default: ;
        }
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void NumKeyboardFrm::InputValue(float _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void NumKeyboardFrm::PassInput(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE

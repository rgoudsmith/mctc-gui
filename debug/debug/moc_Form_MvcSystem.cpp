/****************************************************************************
** Meta object code from reading C++ file 'Form_MvcSystem.h'
**
** Created: Wed Jan 4 16:19:13 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_MvcSystem.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_MvcSystem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MvcSystem[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      28,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,
      30,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      49,   10,   10,   10, 0x08,
      70,   10,   10,   10, 0x08,
      84,   10,   10,   10, 0x08,
      98,   10,   10,   10, 0x0a,
     119,   10,   10,   10, 0x0a,
     144,   10,   10,   10, 0x0a,
     166,   10,   10,   10, 0x0a,
     189,   10,   10,   10, 0x0a,
     209,   10,   10,   10, 0x0a,
     234,   10,   10,   10, 0x0a,
     260,   10,   10,   10, 0x0a,
     285,   10,   10,   10, 0x0a,
     307,   10,   10,   10, 0x0a,
     330,   10,   10,   10, 0x0a,
     355,  353,   10,   10, 0x0a,
     382,  353,   10,   10, 0x0a,
     406,  353,   10,   10, 0x0a,
     427,  353,   10,   10, 0x0a,
     453,  448,   10,   10, 0x0a,
     474,  448,   10,   10, 0x0a,
     499,  353,   10,   10, 0x0a,
     522,  353,   10,   10, 0x0a,
     540,  353,   10,   10, 0x0a,
     561,  353,   10,   10, 0x0a,
     582,  353,   10,   10, 0x0a,
     613,   10,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MvcSystem[] = {
    "MvcSystem\0\0Material1Clicked()\0"
    "Material2Clicked()\0ValueReceived(float)\0"
    "Mat1Clicked()\0Mat2Clicked()\0"
    "SetMCIDVisible(bool)\0SetMCStatusVisible(bool)\0"
    "SetMotorVisible(bool)\0SetWeightVisible(bool)\0"
    "SetRPMVisible(bool)\0SetMaterialVisible(bool)\0"
    "SetMaterialEditable(bool)\0"
    "SetColorPctVisible(bool)\0SetEditColorPct(bool)\0"
    "SetDosActVisible(bool)\0SetDosSetVisible(bool)\0"
    ",\0SetMCStatus(eMCStatus,int)\0"
    "SetMotorStatus(int,int)\0SetWeight(float,int)\0"
    "SetActRPM(float,int)\0str,\0"
    "SetMCID(QString,int)\0SetMaterial(QString,int)\0"
    "SetColorPct(float,int)\0SetRPM(float,int)\0"
    "SetDosAct(float,int)\0SetDosSet(float,int)\0"
    "GraviRpmChanged(eGraviRpm,int)\0"
    "UnitIndexChanged(int)\0"
};

const QMetaObject MvcSystem::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_MvcSystem,
      qt_meta_data_MvcSystem, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MvcSystem::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MvcSystem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MvcSystem::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MvcSystem))
        return static_cast<void*>(const_cast< MvcSystem*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int MvcSystem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: Material1Clicked(); break;
        case 1: Material2Clicked(); break;
        case 2: ValueReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 3: Mat1Clicked(); break;
        case 4: Mat2Clicked(); break;
        case 5: SetMCIDVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: SetMCStatusVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: SetMotorVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: SetWeightVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: SetRPMVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: SetMaterialVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: SetMaterialEditable((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: SetColorPctVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: SetEditColorPct((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: SetDosActVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: SetDosSetVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: SetMCStatus((*reinterpret_cast< eMCStatus(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 17: SetMotorStatus((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 18: SetWeight((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 19: SetActRPM((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 20: SetMCID((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 21: SetMaterial((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 22: SetColorPct((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 23: SetRPM((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 24: SetDosAct((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 25: SetDosSet((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 26: GraviRpmChanged((*reinterpret_cast< eGraviRpm(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 27: UnitIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 28;
    }
    return _id;
}

// SIGNAL 0
void MvcSystem::Material1Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void MvcSystem::Material2Clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE

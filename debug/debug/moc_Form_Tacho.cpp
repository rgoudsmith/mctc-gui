/****************************************************************************
** Meta object code from reading C++ file 'Form_Tacho.h'
**
** Created: Wed Jan 4 16:18:48 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_Tacho.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_Tacho.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TachoFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x08,
      28,    9,    9,    9, 0x08,
      42,    9,    9,    9, 0x08,
      60,    9,    9,    9, 0x08,
      76,    9,    9,    9, 0x0a,
      99,   97,    9,    9, 0x0a,
     130,   97,    9,    9, 0x0a,
     162,   97,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_TachoFrm[] = {
    "TachoFrm\0\0btCancelClicked()\0btOkClicked()\0"
    "btManualClicked()\0btSyncClicked()\0"
    "ValueReceived(float)\0,\0"
    "SysSts_TachoChanged(float,int)\0"
    "SysSts_ExtCapChanged(float,int)\0"
    "SysSts_RpmChanged(float,int)\0"
};

const QMetaObject TachoFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_TachoFrm,
      qt_meta_data_TachoFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TachoFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TachoFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TachoFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TachoFrm))
        return static_cast<void*>(const_cast< TachoFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int TachoFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: btCancelClicked(); break;
        case 1: btOkClicked(); break;
        case 2: btManualClicked(); break;
        case 3: btSyncClicked(); break;
        case 4: ValueReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 5: SysSts_TachoChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: SysSts_ExtCapChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: SysSts_RpmChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

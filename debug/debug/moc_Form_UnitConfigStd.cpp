/****************************************************************************
** Meta object code from reading C++ file 'Form_UnitConfigStd.h'
**
** Created: Wed Jan 4 16:19:16 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_UnitConfigStd.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_UnitConfigStd.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_unitconfigStd[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      34,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   15,   14,   14, 0x05,
      45,   14,   14,   14, 0x05,
      66,   61,   14,   14, 0x05,
     102,   61,   14,   14, 0x05,
     134,   61,   14,   14, 0x05,
     174,   61,   14,   14, 0x05,
     206,   14,   14,   14, 0x05,
     237,   61,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
     275,   14,   14,   14, 0x08,
     290,   14,   14,   14, 0x08,
     305,   14,   14,   14, 0x08,
     320,   14,   14,   14, 0x08,
     335,   14,   14,   14, 0x08,
     350,   14,   14,   14, 0x08,
     365,   14,   14,   14, 0x08,
     382,   14,   14,   14, 0x08,
     399,   14,   14,   14, 0x08,
     416,   14,   14,   14, 0x08,
     428,   14,   14,   14, 0x08,
     451,   14,   14,   14, 0x08,
     471,  469,   14,   14, 0x0a,
     497,   14,   14,   14, 0x0a,
     511,   14,   14,   14, 0x0a,
     527,   14,   14,   14, 0x0a,
     547,  469,   14,   14, 0x0a,
     576,   14,   14,   14, 0x0a,
     604,   14,   14,   14, 0x0a,
     627,  469,   14,   14, 0x0a,
     659,  469,   14,   14, 0x0a,
     691,  469,   14,   14, 0x0a,
     727,  469,   14,   14, 0x0a,
     755,   14,   14,   14, 0x0a,
     780,  469,   14,   14, 0x0a,
     814,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_unitconfigStd[] = {
    "unitconfigStd\0\0,,\0selectionMade(int,int,int)\0"
    "showWindow(int)\0,idx\0"
    "SysCfg_DosToolChanged(eDosTool,int)\0"
    "SysCfg_MotorChanged(eMotor,int)\0"
    "SysCfg_GranulateChanged(eGranulate,int)\0"
    "CfgFillSysChanged(eFillSys,int)\0"
    "MstCfg_InputTypeChanged(eType)\0"
    "SysCfg_GraviRpmChanged(eGraviRpm,int)\0"
    "bt11_clicked()\0bt12_clicked()\0"
    "bt13_clicked()\0bt21_clicked()\0"
    "bt22_clicked()\0bt23_clicked()\0"
    "btSub1_clicked()\0btSub2_clicked()\0"
    "btSub3_clicked()\0OkClicked()\0"
    "inputReceived(QString)\0UnitNameClicked()\0"
    ",\0receiveSelection(int,int)\0showSelForm()\0"
    "showLevelForm()\0showAdvUnitConfig()\0"
    "UnitNameChanged(QString,int)\0"
    "ActiveUnitIndexChanged(int)\0"
    "SlaveCountChanged(int)\0"
    "SysCfg_SetDosTool(eDosTool,int)\0"
    "SysCfg_SetMotorType(eMotor,int)\0"
    "SysCfg_SetGranulate(eGranulate,int)\0"
    "CfgSetFillSys(eFillSys,int)\0"
    "MstCfg_SetInpType(eType)\0"
    "SysCfg_SetGraviRpm(eGraviRpm,int)\0"
    "CfgCurrentUserChanged(eUserType)\0"
};

const QMetaObject unitconfigStd::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_unitconfigStd,
      qt_meta_data_unitconfigStd, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &unitconfigStd::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *unitconfigStd::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *unitconfigStd::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_unitconfigStd))
        return static_cast<void*>(const_cast< unitconfigStd*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int unitconfigStd::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: selectionMade((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 1: showWindow((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: SysCfg_DosToolChanged((*reinterpret_cast< eDosTool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: SysCfg_MotorChanged((*reinterpret_cast< eMotor(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: SysCfg_GranulateChanged((*reinterpret_cast< eGranulate(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: CfgFillSysChanged((*reinterpret_cast< eFillSys(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: MstCfg_InputTypeChanged((*reinterpret_cast< eType(*)>(_a[1]))); break;
        case 7: SysCfg_GraviRpmChanged((*reinterpret_cast< eGraviRpm(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: bt11_clicked(); break;
        case 9: bt12_clicked(); break;
        case 10: bt13_clicked(); break;
        case 11: bt21_clicked(); break;
        case 12: bt22_clicked(); break;
        case 13: bt23_clicked(); break;
        case 14: btSub1_clicked(); break;
        case 15: btSub2_clicked(); break;
        case 16: btSub3_clicked(); break;
        case 17: OkClicked(); break;
        case 18: inputReceived((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 19: UnitNameClicked(); break;
        case 20: receiveSelection((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 21: showSelForm(); break;
        case 22: showLevelForm(); break;
        case 23: showAdvUnitConfig(); break;
        case 24: UnitNameChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 25: ActiveUnitIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 26: SlaveCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 27: SysCfg_SetDosTool((*reinterpret_cast< eDosTool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 28: SysCfg_SetMotorType((*reinterpret_cast< eMotor(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 29: SysCfg_SetGranulate((*reinterpret_cast< eGranulate(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 30: CfgSetFillSys((*reinterpret_cast< eFillSys(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 31: MstCfg_SetInpType((*reinterpret_cast< eType(*)>(_a[1]))); break;
        case 32: SysCfg_SetGraviRpm((*reinterpret_cast< eGraviRpm(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 33: CfgCurrentUserChanged((*reinterpret_cast< eUserType(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 34;
    }
    return _id;
}

// SIGNAL 0
void unitconfigStd::selectionMade(int _t1, int _t2, int _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void unitconfigStd::showWindow(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void unitconfigStd::SysCfg_DosToolChanged(eDosTool _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void unitconfigStd::SysCfg_MotorChanged(eMotor _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void unitconfigStd::SysCfg_GranulateChanged(eGranulate _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void unitconfigStd::CfgFillSysChanged(eFillSys _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void unitconfigStd::MstCfg_InputTypeChanged(eType _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void unitconfigStd::SysCfg_GraviRpmChanged(eGraviRpm _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_END_MOC_NAMESPACE

/****************************************************************************
** Meta object code from reading C++ file 'Form_RecipeConfig.h'
**
** Created: Wed Jan 4 16:19:05 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_RecipeConfig.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_RecipeConfig.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RecipeConfigFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x05,
      38,   16,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
      52,   16,   16,   16, 0x08,
      64,   16,   16,   16, 0x08,
      80,   16,   16,   16, 0x08,
      96,   16,   16,   16, 0x08,
     110,   16,   16,   16, 0x08,
     132,   16,   16,   16, 0x08,
     153,   16,   16,   16, 0x08,
     181,   16,   16,   16, 0x08,
     196,   16,   16,   16, 0x08,
     211,   16,   16,   16, 0x08,
     234,   16,   16,   16, 0x0a,
     267,  265,   16,   16, 0x0a,
     298,   16,   16,   16, 0x0a,
     317,   16,   16,   16, 0x0a,
     350,   16,   16,   16, 0x0a,
     381,   16,   16,   16, 0x0a,
     414,  265,   16,   16, 0x0a,
     449,  265,  445,   16, 0x0a,
     483,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RecipeConfigFrm[] = {
    "RecipeConfigFrm\0\0SetMaterial(QString)\0"
    "RecipeSaved()\0OkClicked()\0CancelClicked()\0"
    "RemoveClicked()\0SaveClicked()\0"
    "TextReceived(QString)\0ValueReceived(float)\0"
    "GetFullRecipePath(QString*)\0RecipeLoaded()\0"
    "LoadDefaults()\0SysActiveChanged(bool)\0"
    "RecipeItemMaterialClicked(int)\0,\0"
    "MaterialReceived(QString,bool)\0"
    "SetRecipe(QString)\0CfgCurrentUserChanged(eUserType)\0"
    "CopyRecipeToBuf(RecipeStruct*)\0"
    "CopyRecipeFromBuf(RecipeStruct*)\0"
    "SaveRecipe(RecipeStruct*,bool)\0int\0"
    "LoadRecipe(QString,RecipeStruct*)\0"
    "MessageResult(int)\0"
};

const QMetaObject RecipeConfigFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_RecipeConfigFrm,
      qt_meta_data_RecipeConfigFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RecipeConfigFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RecipeConfigFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RecipeConfigFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RecipeConfigFrm))
        return static_cast<void*>(const_cast< RecipeConfigFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int RecipeConfigFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: SetMaterial((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: RecipeSaved(); break;
        case 2: OkClicked(); break;
        case 3: CancelClicked(); break;
        case 4: RemoveClicked(); break;
        case 5: SaveClicked(); break;
        case 6: TextReceived((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: ValueReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 8: GetFullRecipePath((*reinterpret_cast< QString*(*)>(_a[1]))); break;
        case 9: RecipeLoaded(); break;
        case 10: LoadDefaults(); break;
        case 11: SysActiveChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: RecipeItemMaterialClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: MaterialReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 14: SetRecipe((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: CfgCurrentUserChanged((*reinterpret_cast< eUserType(*)>(_a[1]))); break;
        case 16: CopyRecipeToBuf((*reinterpret_cast< RecipeStruct*(*)>(_a[1]))); break;
        case 17: CopyRecipeFromBuf((*reinterpret_cast< RecipeStruct*(*)>(_a[1]))); break;
        case 18: SaveRecipe((*reinterpret_cast< RecipeStruct*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 19: { int _r = LoadRecipe((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< RecipeStruct*(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 20: MessageResult((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 21;
    }
    return _id;
}

// SIGNAL 0
void RecipeConfigFrm::SetMaterial(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void RecipeConfigFrm::RecipeSaved()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE

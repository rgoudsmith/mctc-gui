/****************************************************************************
** Meta object code from reading C++ file 'FileSystemControl.h'
**
** Created: Wed Jan 4 16:18:22 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../FileSystemControl.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'FileSystemControl.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FileSystemControl[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x05,
      41,   18,   18,   18, 0x05,
      66,   18,   18,   18, 0x05,
      89,   18,   18,   18, 0x05,

 // slots: signature, parameters, type, tag, flags
     115,   18,   18,   18, 0x0a,
     144,  141,   18,   18, 0x0a,
     195,  193,   18,   18, 0x0a,
     239,  193,   18,   18, 0x0a,
     290,  286,   18,   18, 0x0a,
     342,  141,   18,   18, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_FileSystemControl[] = {
    "FileSystemControl\0\0MasterSettingsSaved()\0"
    "SystemSettingsSaved(int)\0"
    "MasterSettingsLoaded()\0SystemSettingsLoaded(int)\0"
    "StoreMasterFactDefaults()\0,,\0"
    "StoreMasterSettings(MasterConfig*const,int,int&)\0"
    ",\0LoadMasterSettings(MasterConfig*const,int&)\0"
    "StoreSystemFactDefaults(globalItems*const,int)\0"
    ",,,\0StoreSystemSettings(int,globalItems*const,int,int&)\0"
    "LoadSystemSettings(int,globalItems*,int&)\0"
};

const QMetaObject FileSystemControl::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_FileSystemControl,
      qt_meta_data_FileSystemControl, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FileSystemControl::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FileSystemControl::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FileSystemControl::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FileSystemControl))
        return static_cast<void*>(const_cast< FileSystemControl*>(this));
    return QObject::qt_metacast(_clname);
}

int FileSystemControl::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: MasterSettingsSaved(); break;
        case 1: SystemSettingsSaved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: MasterSettingsLoaded(); break;
        case 3: SystemSettingsLoaded((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: StoreMasterFactDefaults(); break;
        case 5: StoreMasterSettings((*reinterpret_cast< MasterConfig*const(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 6: LoadMasterSettings((*reinterpret_cast< MasterConfig*const(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: StoreSystemFactDefaults((*reinterpret_cast< globalItems*const(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: StoreSystemSettings((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< globalItems*const(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 9: LoadSystemSettings((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< globalItems*(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        default: ;
        }
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void FileSystemControl::MasterSettingsSaved()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void FileSystemControl::SystemSettingsSaved(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void FileSystemControl::MasterSettingsLoaded()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void FileSystemControl::SystemSettingsLoaded(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE

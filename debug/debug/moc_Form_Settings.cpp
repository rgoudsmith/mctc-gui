/****************************************************************************
** Meta object code from reading C++ file 'Form_Settings.h'
**
** Created: Wed Jan 4 16:18:58 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_Settings.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_Settings.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SettingsFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      58,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x08,
      25,   12,   12,   12, 0x0a,
      48,   12,   12,   12, 0x0a,
      73,   12,   12,   12, 0x0a,
      96,   12,   12,   12, 0x0a,
     121,   12,   12,   12, 0x0a,
     146,   12,   12,   12, 0x0a,
     179,   12,   12,   12, 0x0a,
     209,   12,   12,   12, 0x0a,
     233,   12,   12,   12, 0x0a,
     259,   12,   12,   12, 0x0a,
     289,   12,   12,   12, 0x0a,
     321,   12,   12,   12, 0x0a,
     354,   12,   12,   12, 0x0a,
     380,   12,   12,   12, 0x0a,
     405,   12,   12,   12, 0x0a,
     431,   12,   12,   12, 0x0a,
     456,   12,   12,   12, 0x0a,
     486,   12,   12,   12, 0x0a,
     513,   12,   12,   12, 0x0a,
     539,   12,   12,   12, 0x0a,
     564,   12,   12,   12, 0x0a,
     591,   12,   12,   12, 0x0a,
     615,   12,   12,   12, 0x0a,
     648,   12,   12,   12, 0x0a,
     682,   12,   12,   12, 0x0a,
     709,   12,   12,   12, 0x0a,
     737,   12,   12,   12, 0x0a,
     763,   12,   12,   12, 0x0a,
     792,   12,   12,   12, 0x0a,
     818,   12,   12,   12, 0x0a,
     845,   12,   12,   12, 0x0a,
     873,   12,   12,   12, 0x0a,
     905,   12,   12,   12, 0x0a,
     930,   12,   12,   12, 0x0a,
     957,   12,   12,   12, 0x0a,
     985,   12,   12,   12, 0x0a,
    1012,   12,   12,   12, 0x0a,
    1040,   12,   12,   12, 0x0a,
    1068,   12,   12,   12, 0x0a,
    1096,   12,   12,   12, 0x0a,
    1123,   12,   12,   12, 0x0a,
    1150,   12,   12,   12, 0x0a,
    1175,   12,   12,   12, 0x0a,
    1204,   12,   12,   12, 0x0a,
    1235,   12,   12,   12, 0x0a,
    1264,   12,   12,   12, 0x0a,
    1292,   12,   12,   12, 0x0a,
    1324,   12,   12,   12, 0x0a,
    1354,   12,   12,   12, 0x0a,
    1381,   12,   12,   12, 0x0a,
    1406,   12,   12,   12, 0x0a,
    1433,   12,   12,   12, 0x0a,
    1460,   12,   12,   12, 0x0a,
    1489,   12,   12,   12, 0x0a,
    1514,   12,   12,   12, 0x0a,
    1545,   12,   12,   12, 0x0a,
    1574,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_SettingsFrm[] = {
    "SettingsFrm\0\0OkClicked()\0"
    "UserChanged(eUserType)\0FullScaleReceived(float)\0"
    "KeyBeepReceived(float)\0TestTime1Received(float)\0"
    "TestTime2Received(float)\0"
    "CalWeightCylinderReceived(float)\0"
    "CalWeightAugerReceived(float)\0"
    "CalStepsReceived(float)\0"
    "CalTimeoutReceived(float)\0"
    "CalAlarmCyclesReceived(float)\0"
    "FillCylinderTimeReceived(float)\0"
    "FillCylinderSpeedReceived(float)\0"
    "ScreenTimeReceived(float)\0"
    "TachoCorrReceived(float)\0"
    "ExtCapTimeReceived(float)\0"
    "ExtCapDevReceived(float)\0"
    "AfterFillDelayReceived(float)\0"
    "InputFilterReceived(float)\0"
    "MetTimeDevReceived(float)\0"
    "CurveGainReceived(float)\0"
    "MinGraviCapReceived(float)\0"
    "InpDelayReceived(float)\0"
    "RunContactOnDelayReceived(float)\0"
    "RunContactOffDelayReceived(float)\0"
    "LogIntervalReceived(float)\0"
    "ColPctSetDevReceived(float)\0"
    "DevAlarmQrReceived(float)\0"
    "MotorSixWiresReceived(float)\0"
    "MotionBandReceived(float)\0"
    "MotionDelayReceived(float)\0"
    "WeightFilterReceived(float)\0"
    "WeightFilterFastReceived(float)\0"
    "RefWeightReceived(float)\0"
    "MeasMinTimeReceived(float)\0"
    "MeasMinBand4Received(float)\0"
    "MeasMaxTimeReceived(float)\0"
    "MeasTimeCorrReceived(float)\0"
    "DevThreshInjReceived(float)\0"
    "DevThreshExtReceived(float)\0"
    "RpmCorrFac1Received(float)\0"
    "RpmCorrFac2Received(float)\0"
    "DevB4HystReceived(float)\0"
    "BAL_FillStartReceived(float)\0"
    "BAL_FillStartHlReceived(float)\0"
    "BAL_FillReadyReceived(float)\0"
    "BAL_FillTimeReceived(float)\0"
    "BAL_LidOffWeightReceived(float)\0"
    "BAL_LidOffTimeReceived(float)\0"
    "ConMeasTimeReceived(float)\0"
    "DevAlmPctReceived(float)\0"
    "DevAlmRetryReceived(float)\0"
    "DevAlmPctB4Received(float)\0"
    "DevAlmRetryB4Received(float)\0"
    "ActUpdateReceived(float)\0"
    "DevAlarmRetryQrReceived(float)\0"
    "DevAlarmPctQrReceived(float)\0"
    "DevAlarmBandNrQrReceived(float)\0"
};

const QMetaObject SettingsFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_SettingsFrm,
      qt_meta_data_SettingsFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SettingsFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SettingsFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SettingsFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SettingsFrm))
        return static_cast<void*>(const_cast< SettingsFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int SettingsFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: OkClicked(); break;
        case 1: UserChanged((*reinterpret_cast< eUserType(*)>(_a[1]))); break;
        case 2: FullScaleReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 3: KeyBeepReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 4: TestTime1Received((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 5: TestTime2Received((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: CalWeightCylinderReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 7: CalWeightAugerReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 8: CalStepsReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 9: CalTimeoutReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 10: CalAlarmCyclesReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 11: FillCylinderTimeReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 12: FillCylinderSpeedReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 13: ScreenTimeReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 14: TachoCorrReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 15: ExtCapTimeReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 16: ExtCapDevReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 17: AfterFillDelayReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 18: InputFilterReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 19: MetTimeDevReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 20: CurveGainReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 21: MinGraviCapReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 22: InpDelayReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 23: RunContactOnDelayReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 24: RunContactOffDelayReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 25: LogIntervalReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 26: ColPctSetDevReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 27: DevAlarmQrReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 28: MotorSixWiresReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 29: MotionBandReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 30: MotionDelayReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 31: WeightFilterReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 32: WeightFilterFastReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 33: RefWeightReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 34: MeasMinTimeReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 35: MeasMinBand4Received((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 36: MeasMaxTimeReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 37: MeasTimeCorrReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 38: DevThreshInjReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 39: DevThreshExtReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 40: RpmCorrFac1Received((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 41: RpmCorrFac2Received((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 42: DevB4HystReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 43: BAL_FillStartReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 44: BAL_FillStartHlReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 45: BAL_FillReadyReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 46: BAL_FillTimeReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 47: BAL_LidOffWeightReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 48: BAL_LidOffTimeReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 49: ConMeasTimeReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 50: DevAlmPctReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 51: DevAlmRetryReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 52: DevAlmPctB4Received((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 53: DevAlmRetryB4Received((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 54: ActUpdateReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 55: DevAlarmRetryQrReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 56: DevAlarmPctQrReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 57: DevAlarmBandNrQrReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 58;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

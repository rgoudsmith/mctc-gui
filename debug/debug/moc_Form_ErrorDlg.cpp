/****************************************************************************
** Meta object code from reading C++ file 'Form_ErrorDlg.h'
**
** Created: Wed Jan 4 16:18:18 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_ErrorDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_ErrorDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ErrorDlg[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      29,    9,    9,    9, 0x08,
      42,    9,    9,    9, 0x08,
      55,    9,    9,    9, 0x08,
      68,    9,    9,    9, 0x08,
      81,    9,    9,    9, 0x08,
      96,   94,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ErrorDlg[] = {
    "ErrorDlg\0\0ButtonClicked(int)\0OnCloseTim()\0"
    "bt1Clicked()\0bt2Clicked()\0bt3Clicked()\0"
    "bt4Clicked()\0,\0AlarmStatusChanged(bool,int)\0"
};

const QMetaObject ErrorDlg::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ErrorDlg,
      qt_meta_data_ErrorDlg, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ErrorDlg::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ErrorDlg::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ErrorDlg::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ErrorDlg))
        return static_cast<void*>(const_cast< ErrorDlg*>(this));
    return QDialog::qt_metacast(_clname);
}

int ErrorDlg::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: ButtonClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: OnCloseTim(); break;
        case 2: bt1Clicked(); break;
        case 3: bt2Clicked(); break;
        case 4: bt3Clicked(); break;
        case 5: bt4Clicked(); break;
        case 6: AlarmStatusChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void ErrorDlg::ButtonClicked(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

/****************************************************************************
** Meta object code from reading C++ file 'Form_Tolerances.h'
**
** Created: Wed Jan 4 16:18:50 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_Tolerances.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_Tolerances.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TolerancesFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      36,   34,   14,   14, 0x0a,
      61,   34,   14,   14, 0x0a,
      86,   14,   14,   14, 0x0a,
     102,   14,   14,   14, 0x0a,
     118,   14,   14,   14, 0x0a,
     141,   14,   14,   14, 0x0a,
     169,   14,   14,   14, 0x0a,
     181,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_TolerancesFrm[] = {
    "TolerancesFrm\0\0DisplayWindow(int)\0,\0"
    "DevAlmChanged(float,int)\0"
    "CalDevChanged(float,int)\0DevAlmClicked()\0"
    "CalDevClicked()\0UnitValReceived(float)\0"
    "LanguageChanged(eLanguages)\0OkClicked()\0"
    "CancelClicked()\0"
};

const QMetaObject TolerancesFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_TolerancesFrm,
      qt_meta_data_TolerancesFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TolerancesFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TolerancesFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TolerancesFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TolerancesFrm))
        return static_cast<void*>(const_cast< TolerancesFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int TolerancesFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: DisplayWindow((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: DevAlmChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: CalDevChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: DevAlmClicked(); break;
        case 4: CalDevClicked(); break;
        case 5: UnitValReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: LanguageChanged((*reinterpret_cast< eLanguages(*)>(_a[1]))); break;
        case 7: OkClicked(); break;
        case 8: CancelClicked(); break;
        default: ;
        }
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void TolerancesFrm::DisplayWindow(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

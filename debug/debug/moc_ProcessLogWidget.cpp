/****************************************************************************
** Meta object code from reading C++ file 'ProcessLogWidget.h'
**
** Created: Wed Jan 4 16:18:40 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../ProcessLogWidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ProcessLogWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ProcessLogWidget[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      27,   18,   17,   17, 0x05,
      60,   17,   17,   17, 0x05,

 // slots: signature, parameters, type, tag, flags
      75,   70,   17,   17, 0x0a,
     100,   17,   17,   17, 0x0a,
     112,   17,   17,   17, 0x0a,
     130,  126,   17,   17, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ProcessLogWidget[] = {
    "ProcessLogWidget\0\0selected\0"
    "selectionChanged(QItemSelection)\0"
    "KeyBeep()\0item\0addEntry(LogLineStruct&)\0"
    "editEntry()\0removeEntry()\0row\0"
    "removeEntry(int)\0"
};

const QMetaObject ProcessLogWidget::staticMetaObject = {
    { &QTabWidget::staticMetaObject, qt_meta_stringdata_ProcessLogWidget,
      qt_meta_data_ProcessLogWidget, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ProcessLogWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ProcessLogWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ProcessLogWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ProcessLogWidget))
        return static_cast<void*>(const_cast< ProcessLogWidget*>(this));
    return QTabWidget::qt_metacast(_clname);
}

int ProcessLogWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTabWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: selectionChanged((*reinterpret_cast< const QItemSelection(*)>(_a[1]))); break;
        case 1: KeyBeep(); break;
        case 2: addEntry((*reinterpret_cast< LogLineStruct(*)>(_a[1]))); break;
        case 3: editEntry(); break;
        case 4: removeEntry(); break;
        case 5: removeEntry((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void ProcessLogWidget::selectionChanged(const QItemSelection & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ProcessLogWidget::KeyBeep()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE

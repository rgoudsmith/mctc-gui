/****************************************************************************
** Meta object code from reading C++ file 'Form_OverlayMenu.h'
**
** Created: Wed Jan 4 16:19:14 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Form_OverlayMenu.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_OverlayMenu.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_OverlayMenu[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,
      34,   12,   12,   12, 0x05,
      56,   12,   12,   12, 0x05,
      77,   12,   12,   12, 0x05,
      98,   12,   12,   12, 0x05,
     118,   12,   12,   12, 0x05,
     141,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
     164,   12,   12,   12, 0x0a,
     178,   12,   12,   12, 0x0a,
     197,  189,   12,   12, 0x0a,
     224,  189,   12,   12, 0x0a,
     252,  189,   12,   12, 0x0a,
     279,  189,   12,   12, 0x0a,
     306,  189,   12,   12, 0x0a,
     332,   12,   12,   12, 0x0a,
     349,   12,   12,   12, 0x0a,
     367,   12,   12,   12, 0x0a,
     384,   12,   12,   12, 0x0a,
     401,   12,   12,   12, 0x0a,
     417,   12,   12,   12, 0x0a,
     436,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_OverlayMenu[] = {
    "OverlayMenu\0\0MenuBtLoginClicked()\0"
    "MenuBtConfigClicked()\0MenuBtAlarmClicked()\0"
    "MenuBtLearnClicked()\0MenuBtSaveClicked()\0"
    "MenuBtKeyLockClicked()\0MenuButtonClicked(int)\0"
    "DisplayMenu()\0FoldMenu()\0caption\0"
    "SetBtLoginCaption(QString)\0"
    "SetBtConfigCaption(QString)\0"
    "SetBtAlarmCaption(QString)\0"
    "SetBtLearnCaption(QString)\0"
    "SetBtSaveCaption(QString)\0BtLoginClicked()\0"
    "BtConfigClicked()\0BtAlarmClicked()\0"
    "BtLearnClicked()\0BtSaveClicked()\0"
    "BtKeyLockClicked()\0UserChanged(eUserType)\0"
};

const QMetaObject OverlayMenu::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_OverlayMenu,
      qt_meta_data_OverlayMenu, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OverlayMenu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OverlayMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OverlayMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OverlayMenu))
        return static_cast<void*>(const_cast< OverlayMenu*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int OverlayMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: MenuBtLoginClicked(); break;
        case 1: MenuBtConfigClicked(); break;
        case 2: MenuBtAlarmClicked(); break;
        case 3: MenuBtLearnClicked(); break;
        case 4: MenuBtSaveClicked(); break;
        case 5: MenuBtKeyLockClicked(); break;
        case 6: MenuButtonClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: DisplayMenu(); break;
        case 8: FoldMenu(); break;
        case 9: SetBtLoginCaption((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: SetBtConfigCaption((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: SetBtAlarmCaption((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: SetBtLearnCaption((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 13: SetBtSaveCaption((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 14: BtLoginClicked(); break;
        case 15: BtConfigClicked(); break;
        case 16: BtAlarmClicked(); break;
        case 17: BtLearnClicked(); break;
        case 18: BtSaveClicked(); break;
        case 19: BtKeyLockClicked(); break;
        case 20: UserChanged((*reinterpret_cast< eUserType(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 21;
    }
    return _id;
}

// SIGNAL 0
void OverlayMenu::MenuBtLoginClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void OverlayMenu::MenuBtConfigClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void OverlayMenu::MenuBtAlarmClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void OverlayMenu::MenuBtLearnClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void OverlayMenu::MenuBtSaveClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void OverlayMenu::MenuBtKeyLockClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}

// SIGNAL 6
void OverlayMenu::MenuButtonClicked(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_END_MOC_NAMESPACE

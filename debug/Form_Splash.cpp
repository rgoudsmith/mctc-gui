#include "Form_Splash.h"
#include "ui_Form_Splash.h"
#include "Form_MainMenu.h"

SplashFrm::SplashFrm(QWidget *parent) : QWidget(parent), ui(new Ui::SplashFrm)
{
  ui->setupUi(this);

  closeTim = new QTimer(this);
  closeTim->setInterval(10000);
  closeTim->stop();
  closeTim->setSingleShot(true);

  connect(closeTim,SIGNAL(timeout()),this,SLOT(closeTimEvent()));
  // ui->label_3->setVisible(false);

  this->setWindowFlags((Qt::SplashScreen));
  //    this->setWindowFlags((this->windowFlags() | Qt::SplashScreen));
}


SplashFrm::~SplashFrm()
{
  closeTim->stop();
  delete ui;
}


void SplashFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}


void SplashFrm::ShowScreen(int time)
{
  ui->lbGuiVersion->setText(fileInfo.version);
  this->show();
  closeTim->setInterval(time);
  closeTim->start();
}


void SplashFrm::closeTimEvent()
{
  closeTim->stop();
  ((MainMenu*)menu)->DisplayWindow(wiHome);
  this->close();
}

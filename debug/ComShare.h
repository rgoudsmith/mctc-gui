/*--------------------------------------------------------------------------------------*/
/* File        : ComShare.h                                                             */
/* Description : Share communication structures between mctc-gui and mctc-control tasks */
/*--------------------------------------------------------------------------------------*/

#ifndef COMSHARE_H
#define COMSHARE_H

#define COMSHARE_VERSION    "1.20"

/*----- Constants -----*/
#define CFG_MAX_UNIT_COUNT 5                                                    /* Max number of units */

#define MAX_CURVE_LINES 51
#define MAX_USER_ALM 10
#define MAX_DIG_OUT 4
#define MAX_DIG_IN 4

/*----- MC commands -----*/
#define CMD_NONE                0                                               /* None */
#define CMD_PROD_START          1                                               /* Start production */
#define CMD_PROD_STOP           2                                               /* Stop production */
#define CMD_PRIME_START         3                                               /* start prime (filling cylinder before production start) */
#define CMD_PRIME_STOP          4                                               /* stop prime (filling cylinder) */

/*----- Unit types -----*/
#define UNIT_TYPE_NONE               0
#define UNIT_TYPE_WEIGHT             1
#define UNIT_TYPE_BALANCE            2
#define UNIT_TYPE_BALANCE_HO         3
#define UNIT_TYPE_BALANCE_REGRIND    4
#define UNIT_TYPE_BALANCE_HO_REGRIND 5


/*--- Profibus status ---*/
#define PROFI_NONE       0                 /* None */
#define PROFI_SPI_ERR    1                 /* SPI error */
#define PROFI_NO_VPC3    2                 /* No VPC3 chip detect */
#define PROFI_VPC3_DET   3                 /* VPC3 chip detect */
#define PROFI_INIT       4                 /* VPC3 Init */
#define PROFI_OFFLINE    5                 /* Offline */
#define PROFI_ONLINE     6                 /* Online */



/*----- Curve line -----*/
typedef struct {
  float motorSpeed;
  float capacity;
} tCurveLineStruct;

/*----- Curve structure -----*/
typedef struct {
  unsigned char swVersion;
  unsigned char cylSel;
  unsigned char matType;
  unsigned char calMode;
  float corFac;
  tCurveLineStruct curveLines[MAX_CURVE_LINES];
} tCurveStruct;

/*----- deviation -----*/
typedef struct {
  byte corDev;                                                                  /* Deviation */
  float corFac1;                                                                /* Correction factor 1 */
  float corFac2;                                                                /* Correction factor 2 */
  byte corN1;                                                                   /* N1 count */
  byte corN2;                                                                   /* N2 count */
  byte corMax;                                                                  /* N max count */
} tBalDev;

/*-----  Configuration ----*/
typedef struct {
  byte unitType;                                                                /* Unit device type */
  byte operatingMode;                                                           /* RPM / CAL Operating mode */
  byte prodType;                                                                /* Extusion / Injection moulding  mode */
  byte injInputMode;                                                            /* Injection moulding pDat->can.input mode */
  byte extInputMode;                                                            /* Extruder pDat->can.input mode */
  byte injRelFilt;                                                              /* Inj relay metering time average filter factor */
  float metDevPct;                                                              /* Metering time hysterese deviation */
  float maxMotorSpeed;                                                          /* Max motor speed */
  byte motorCurrent;                                                            /* Motor current */
  byte motorMicroStepRpm;                                                       /* Motor micro step rpm */
  byte motorConn6Wire;                                                          /* Motor 6 wire alarm */
  byte motorAlmEnb;                                                             /* Motor alarm enable */
  float runContactOnDly;                                                        /* Run relay contact on delay */
  float runContactOffDly;                                                       /* Run relay contact off delay */
  byte runContactMode;                                                          /* Running contact mode */
  float tachoCorrFac;                                                           /* Tacho correction factor */
} tParmSystem;

/*-----  Alarm ----*/
typedef struct {
  byte almCfg[MAX_USER_ALM];                                                    /* Alarm configuration for user configurable alarms */
} tParmAlarm;

/*----- Filling system settings -----*/
typedef struct {
  byte fillOnOff;                                                               /* Filling system ON/OFF */
  byte fillSystem;                                                              /* Filling system */
  float fillAlmTime;                                                            /* Filling alarm time */
  float fillTimeMV;                                                             /* Filling time MV */
  float fillEmptyTime;                                                          /* Filling emptying time */
  byte fillAlmMode;                                                             /* Filling alarm mode */
  word mvDelay;                                                                 /* After fill delay "MV Delay" */
} tParmFillSystem;

/*------ Levels -----*/
typedef struct {
  float hopperEmptyWeight;                                                      /* Hopper empty weight */
  float hopperSensorFill;                                                       /* Hopper start fill weight */
  float hopperHiLevel;                                                          /* Hopper hi level weight */
  float hopperHiHiLevel;                                                        /* Hopper hi hi level weight */
} tParmLevels;

/*------ Manual fill detection -----*/
typedef struct {
  float balFillStart;                                                           /* Mass change fill start No fill system */
  float balFillStartHl;                                                         /* Mass change fill start Hopper Loader */
  float balFillReady;                                                           /* Mass change fill ready */
  byte balFillTime;                                                             /* Fill time */
  byte balLidOffTime;                                                           /* Lid Off time 4 sec. */
  float balLidOffWeight;                                                        /* Lid off weight 300 gr */
} tParmManFillDet;

/*------ Manual fill detection -----*/
typedef struct {
  float conMeasTime;                                                            /* Consumption update time */
} tParmCons;

/*------ Balance -----*/
typedef struct {
  tBalDev dev[5];                                                               /* Balance band deviation band data */
  byte actUpdate;                                                               /* Actual value update deviation percentage */
  float curveGain;                                                              /* Curve gain */
  float colPctSetDev;                                                           /* Color % Set Deviation */
  float extCapPctRel;                                                           /* Ext cap dev pct (tacho mode) */
  float extCapSampleTime;                                                       /* Ext cap sample time (tacho mode) */

  float devB4Hyst;                                                              /* Deviation Band 4 control hysterese */
  word rpmCorFac1;                                                              /* Rpm correction factor > deviation threshold */
  word rpmCorFac2;                                                              /* Rpm correction factor < deviation threshold */

  word devThresholdInj;                                                         /* Deviation threshold [%] */
  word devThresholdExt;                                                         /* Deviation threshold [%] */

  /*--- measurement time ---*/
  byte minTime;                                                                 /* Minimum measurement time band 0,1,2,3 */
  word minTimeB4;                                                               /* Minimum measurement time band 4 */
  word maxTime;                                                                 /* Maximum measurement time band */
  float timeCorFac;                                                             /* Measurement time correction factor */

  /*--- ComMsgReceivedDeviation alarm ---*/
  byte devAlmPct;                                                               /* Deviation alarm */
  byte devAlmRetry;                                                             /* Deviation retry */
  byte devAlmPctB4;                                                             /* Deviation alarm for band 4 */
  byte devAlmRetryB4;                                                           /* Deviation retry for band 4 */

  /*--- Deviation alarm QR ---*/
  byte devAlmPctQR;                                                             /* Deviation alarm QR */
  byte devAlmRetryQR;                                                           /* Deviation retry QR */
  byte devAlmBandNrQR;                                                          /* Deviation QR alarm Band nr. */

} tParmBalance;

/*------ PreCal -----*/
typedef struct {
  float calDev;                                                                 /* Calibration dev. for auto single point +/- 5% */
  byte calAlmCycle;                                                             /* Calibration alarm cycles 20x */
  byte testTime1;                                                               /* Calibration test time for pDat->mc.colCap < 0.5 GR/S */
  byte testTime2;                                                               /* Calibration test time for pDat->mc.colCap >= 0.5 GR/S */
} tParmPreCal;

/*------ Weight -----*/
typedef struct {
  float fullScale;                                                              /* Full scale */
  float refWeight;                                                              /* Reference weight */
  byte motionBand;                                                              /* Motion band [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5] */
  byte motionDelay;                                                             /* Motion delay [100 ms]*/
  byte weightFiltFac;                                                           /* Weight filter factor */
  byte weightFastFiltFac;                                                       /* Weight fast filter factor */
} tParmWeight;

/*------ McWeight parameters -----*/
typedef struct {
  float tachoCapRatio;                                                          /* Tacho cap. ratio factor */
  byte knifeGateValve;                                                          /* Loader enable */
  float shortMeasTime;                                                          /* Short meas. time */
  float hysterese;                                                              /* Ext cap. hysterese */
  float minExtCap;                                                              /* Minimum Extruder cap. kg/h */
} tMcWeightParm;

#define MAX_REGR_LEVELS   5                                                     /* Regrind Levels */

/*------ Regrind parameters -----*/
typedef struct {
  float levelHigh;                                                              /* Hihg level, default 4000 g */
  float levelRegrind;                                                           /* Regrind high level, default 3500 g */
  byte levelPct[MAX_REGR_LEVELS];                                               /* Regrind levels, default B0=0, B1=25, B2=50, B3=75 and B4=100 % */
  float regrindPctExtra;                                                        /* Extra regrind percentage (level 4), default 5 % */
  float regPctB1;                                                               /* Regrind Band 1 percentage, default 80 % */
  word fillTimeMax;                                                             /* Max. fill time, default 20 s */
  word fillInterval;                                                            /* Fill interval time, default 300 s */
} tRegrindParm;

/*----- McTc control task parameters -----*/
typedef struct {
  tParmSystem sys;                                                              /* System configuration */
  tParmWeight weight;                                                           /* Weight settings */
  tParmAlarm alm;                                                               /* Alarm configuration */
  tParmLevels levels;                                                           /* Hopper levels */
  tParmFillSystem fillSystem;                                                   /* Fill system configuration */
  tParmManFillDet manFillDet;                                                   /* Manual fill detectino parameters */
  tParmCons cons;                                                               /* Consumption */
  tParmPreCal preCal;                                                           /* Pre calibration parameters */
  tParmBalance bal;                                                             /* Balance parameters */
  tMcWeightParm mcWeightParm;                                                   /* McWeight parameters */
  tRegrindParm regrind;                                                         /* Regrind parameters */
} tMcTcControlParm;

/*------ Production settings -----*/
typedef struct {
  float colPct;                                                                 /* Color % */
  float extCap;                                                                 /* Ext. cap. set */
  float primeRpm;                                                               /* Prime speed */
  float shotWeight;                                                             /* Shot weight */
  float dosTime;                                                                /* Dosing time */
  float tachoMax;                                                               /* Tacho max. voltage set */
  float rpmSet;                                                                 /* RPM set (RPM mode) */
} tProdSettings;

/*------ Pre-calibration (Material calibration) -----*/
typedef struct {
  byte start;                                                                   /* Start */
  byte abort;                                                                   /* Stop */
  byte pause;                                                                   /* Pause */
  byte readyAck;                                                                /* Ready acknowledge */
  byte cont;                                                                    /* Continue */
} tPreCalibration;

/*------ Manual I/O -----*/
typedef struct {
  byte active;                                                                  /* Manual IO active ! */
  float motorSpeed;                                                             /* Motor speed */
  byte output[4];                                                               /* Outputs */
} tManualIO;

/*------ Various commands -----*/
typedef struct {
  byte sysStartCmd;                                                             /* System commands */
  byte calib;                                                                   /* Loadcell cal. cmd */
  byte fillMan;                                                                 /* Manual fill command */
  byte consReset;                                                               /* Consumption reset */
  byte consResetDay;                                                            /* Consumption reset */
  byte alarmResetCmd;                                                           /* Reset alarm */
  byte curveChanged;                                                            /* Curve changed signal */
  byte remoteStart;                                                             /* Remote start input */
} tCommands;

/*------ Profibus GUI task global config record (Send from GUI to control task) -----*/
typedef struct {
  byte profibusEnb;                                                             /* Profibus enabled */
  byte deviceAddress;                                                           /* Profibus device address */
} tProfiBusGuiCommonCfg;

/*------ Profibus Control task global command record (Send from Control task to GUI task) -----*/
typedef struct {
  byte profibusStatus;                                                          /* Profibus status */
  byte startCmd;                                                                /* Start / stop */
  byte resetAlarm;                                                              /* Reset active alarm */
  float extCapKgH;                                                              /* Extr. cap. set [kg/h] */
  float tachoSet;                                                               /* Tacho max. set [V] */
} tProfiBusControlCommonCmd;

/*------ Profibus Control task command record per unit (Send from Control task to GUI task) -----*/
typedef struct {
  float colPct;                                                                 /* Color % */
} tProfiBusControlUnitCmd;

/*-------------------------------------------------------------------------*/
/* GUI task : Single unit message (Stucture send from Gui to Control task) */
/*-------------------------------------------------------------------------*/
typedef struct {
  tCommands commands;                                                           /* Div. commands */
  tProdSettings prodSettings;                                                   /* Prod. settings */
  tPreCalibration preCalibration;                                               /* Pre cal. commands */
  tMcTcControlParm mcTcControlParm;                                             /* Parameters */
  tManualIO manualIO;                                                           /* Manual IO */
  tCurveStruct curve;                                                           /* Material Curve */
} tMsgGuiUnit;

/*-------------------------------------------------------------------------*/
/* GUI task : Common message (Stucture send from Gui to Control task) */
/*-------------------------------------------------------------------------*/
typedef struct {
  byte buzzerCmd;                                                               /* Buzzer command */
  byte rtcUpdate;                                                               /* RTC update request */
  byte slavesConnected;                                                         /* Number of conencted slave units */
  byte canWdDis;                                                                /* CAN watch dog enable */
  tProfiBusGuiCommonCfg profibusCfg;                                            /* Profibus configuration parameters */
} tMsgGuiCommon;

/*------ McWeight data -----*/
typedef struct {
  float extCap;                                                                 /* McWeight cap kg/h (without add.)*/
  float extCapTotal;                                                            /* Total ext. cap. kg/h */
  float colorPctTotal;                                                          /* Total color % */
} tMcWeightData;

/*------ Regrind data -----*/
typedef struct {
  float regPctAct;                                                              /* Actual regrind % */
  byte regBandAct;                                                              /* Actual regrind band nr */
  float fillStartLevel;                                                         /* Fill start level [g] */
} tRegrindData;

/*------ I/O data -----*/
typedef struct {
  byte input[4];                                                                /* Digital inputs status */
  byte output[4];                                                               /* Digital outputs status */
  float tacho;                                                                  /* Tacho value */
} tIoData;

/*-----------------------------------------------------------------------------*/
/* Control task : Single unit message (Stucture send from Control to Gui task) */
/*-----------------------------------------------------------------------------*/
typedef struct {
  float dosSet;                                                                 /* Setpoint dosage capacity [g/s] */
  float dosAct;                                                                 /* Actual dosage capacity [g/s] */
  float actRpm;                                                                 /* Actual rpm */
  float weight;                                                                 /* Actual weight [g] */
  unsigned char calib;
  float preCalCalCapIst;                                                        /* Pre calibration cap ist */
  float rpmCorr;                                                                /* RPM correction */
  float productsToGo;                                                           /* Theoretical products/time to go */
  float consumption;                                                            /* Consumption (Not used) */
  float consumptionDay;                                                         /* Consumption  (Not used) */
  float timeToGo;                                                               /* Inj mode shot time to go */
  float grossWeightFast;                                                        /* Fast weight for test */

  float tachoAct;                                                               /* Actual tacjo voltage */
  float extCapAct;                                                              /* Actual extruder capacity */
  float metTimeAct;                                                             /* Dosing (Metering) time */
  float metTimeMeas;                                                            /* Meting time measured */
  float metTimeCalc;                                                            /* Metering time avg */
  float colCapIst;                                                              /* Color cap act (not video)*/
  float vidColDevPct;                                                           /* Capacity deviation ABS*/
  float colDevPctRel;                                                           /* Capacity deviation REL*/
  float corCycle;                                                               /* Correction Cycle*/
  float vidCorrection;                                                          /* Correction */
  float corFacAdjust;                                                           /* FacAdj*/
  float minTimeCorFac;                                                          /* Min Time Corr*/
  float sampleLen;                                                              /* Measurement length SET*/
  float timeLeft;                                                               /* Measurement length ACT*/
  float sampleLenQR;                                                            /* QR Measurement length SET */
  float timeLeftQR;                                                             /* QR Measurement length ACT*/
  float colDevPctQR;                                                            /* QR color dev. % */
  float tachoRatioAct;                                                          /* Tacho ratio factor actual */
  byte balProdStatus;                                                           /* Balance production status  Display "Calibrating (Learn)" / "Calibrated (OK)" */
  byte vidProdSts;                                                              /* Video production status */
  byte preCalBusy;                                                              /* Precalibration busy  */
  byte preCalReady;                                                             /* Precalibration ready  */
  byte preCalPauseBusy;                                                         /* Precalibration pause busy */
  byte preCalIstValid;                                                          /* Precalibration Ist value valid */
  byte prodTestBusy;                                                            /* Production test busy (Not used)*/
  byte alarmNr;                                                                 /* Alarm number */
  byte almActive;                                                               /* Alarm active */
  byte alarmMode;                                                               /* Alarm mode alarm/warning */
  byte metTimeActValid;                                                         /* Metering time valid */
  byte metRstHystCnt;                                                           /* Metering hysterese reset counter for diagnose */
  byte bandNr;                                                                  /* Band data - nr */
  byte inBand;                                                                  /* Band data - in*/
  byte outBand;                                                                 /* Band data - out*/
  byte bandCnt;                                                                 /* Band data - Max*/
  byte corSts;                                                                  /* Correction state */
  byte mcSts;                                                                   /* Ist measurement state */
  byte putIst;                                                                  /* Put weight counter */
  byte getIst;                                                                  /* Get weight counter */
  byte mcStsQR;                                                                 /* QR - Ist measurement state */
  byte putIstQR;                                                                /* QR - Put weight counter */
  byte getIstQR;                                                                /* QR - Get weight counter */
  byte devRetryQR;                                                              /* QR - deviation retry counter */
  byte versionSw;                                                               /* Software version */
  byte versionHw;                                                               /* Hardware version */
  byte temperature;                                                             /* Board temperature (Not used) */
  byte mc24On;                                                                  /* MC-TC on status */
  byte mcStartError;                                                            /* Start error */
  byte alarmResetAck;                                                           /* Alarm reset ack */
  byte standStill;                                                              /* Standstill */
  byte newShotDetect;                                                           /* New shot detect */
  byte primeBusy;                                                               /* Prime busy */
  tIoData ioData;                                                               /* IO data */
  byte mcCmd;                                                                   /* McCmd */
  byte actIsSet;                                                                /* Act value = set value mode active */

  tMcWeightData mcWeightData;                                                   /* McWeight */
  tRegrindData regrindData;                                                     /* Regrind */

  tProfiBusControlUnitCmd profibusUnitCmd;                                      /* Profibus data */

} tMsgControlUnit;

/*------------------------------------------------------------------------*/
/* Control task : Common message (Stucture send from Control to Gui task) */
/*------------------------------------------------------------------------*/
typedef struct {
  tProfiBusControlCommonCmd profiBusCommonCmd;
} tMsgControlCommon;

/*----- GUI message (Send from GUI to Control task) ----*/
typedef struct {
  long int msg_id;                                                              /* Message ID */
  tMsgGuiCommon common;                                                         /* GUI : Common data */
  tMsgGuiUnit data[CFG_MAX_UNIT_COUNT];                                         /* GUI : Unit data */
} tMsgGui;

/*----- Control message (Send from Control to GUI task) -----*/
typedef struct {
  long int msg_id;                                                              /* Message ID */
  tMsgControlCommon common;                                                     /* CONTROL : Common data */
  tMsgControlUnit data[CFG_MAX_UNIT_COUNT];                                     /* CONTROL : Unit data */
} tMsgControl;
#endif

/****************************************************************************
** Meta object code from reading C++ file 'Form_Manual.h'
**
** Created: Thu Jan 31 11:21:14 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_Manual.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_Manual.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ManualFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x0a,
      23,   10,   10,   10, 0x0a,
      40,   10,   10,   10, 0x0a,
      57,   10,   10,   10, 0x0a,
      74,   10,   10,   10, 0x0a,
      91,   10,   10,   10, 0x0a,
     108,   10,   10,   10, 0x0a,
     129,   10,   10,   10, 0x0a,
     144,   10,   10,   10, 0x0a,
     166,  164,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ManualFrm[] = {
    "ManualFrm\0\0OkClicked()\0Output1Clicked()\0"
    "Output2Clicked()\0Output3Clicked()\0"
    "Output4Clicked()\0ManModeClicked()\0"
    "ValueReceived(float)\0DisplayIO(int)\0"
    "SetupMirrorInputs()\0,\0"
    "ComMsgReceived(tMsgControlUnit,int)\0"
};

const QMetaObject ManualFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_ManualFrm,
      qt_meta_data_ManualFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ManualFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ManualFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ManualFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ManualFrm))
        return static_cast<void*>(const_cast< ManualFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int ManualFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: OkClicked(); break;
        case 1: Output1Clicked(); break;
        case 2: Output2Clicked(); break;
        case 3: Output3Clicked(); break;
        case 4: Output4Clicked(); break;
        case 5: ManModeClicked(); break;
        case 6: ValueReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 7: DisplayIO((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: SetupMirrorInputs(); break;
        case 9: ComMsgReceived((*reinterpret_cast< tMsgControlUnit(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

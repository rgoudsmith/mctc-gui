/****************************************************************************
** Meta object code from reading C++ file 'Form_RecipeCheckItem.h'
**
** Created: Thu Jan 31 11:20:55 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_RecipeCheckItem.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_RecipeCheckItem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RecipeCheckItemFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   19,   19,   19, 0x0a,
      38,   19,   19,   19, 0x0a,
      62,   19,   19,   19, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RecipeCheckItemFrm[] = {
    "RecipeCheckItemFrm\0\0SetDevID(QString)\0"
    "SetCurDosTool(eDosTool)\0SetRcpDosTool(eDosTool)\0"
};

const QMetaObject RecipeCheckItemFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_RecipeCheckItemFrm,
      qt_meta_data_RecipeCheckItemFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RecipeCheckItemFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RecipeCheckItemFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RecipeCheckItemFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RecipeCheckItemFrm))
        return static_cast<void*>(const_cast< RecipeCheckItemFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int RecipeCheckItemFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: SetDevID((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: SetCurDosTool((*reinterpret_cast< eDosTool(*)>(_a[1]))); break;
        case 2: SetRcpDosTool((*reinterpret_cast< eDosTool(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

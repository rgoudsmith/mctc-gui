#ifndef UNITCONFIGSTD_H
#define UNITCONFIGSTD_H

#include <QtGui/QWidget>
#include <QMessageBox>
#include <QFileDialog>
#include "Form_Selection.h"
#include "Form_Keyboard.h"
#include "Form_QbaseWidget.h"

namespace Ui
{
  class UnitConfigStdFrm;
}


class UnitConfigStdFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    UnitConfigStdFrm(QWidget *parent = 0);
    ~UnitConfigStdFrm();
    void FormInit();

  protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *e);
    bool eventFilter(QObject *, QEvent *);
    void showEvent(QShowEvent *);

  private:
    Ui::UnitConfigStdFrm *m_ui;
    selectionFrm *selFrm;
    KeyboardFrm * charKey;
    int options[itCount];

    void SetupDisplay();
    void LanguageUpdate();
    void SetButtonsByLevel();

    signals:
    void selectionMade(int,int,int);
    void SysCfg_DosToolChanged(eDosTool,int idx);
    void SysCfg_KnifeGate(eKnifeGate,int idx);
    void SysCfg_MotorChanged(eMotor,int idx);
    void SysCfg_GranulateChanged(eGranulate, int idx);
    void CfgFillSysChanged(eFillSys, int idx);
    void MstCfg_InputTypeChanged(eType);
    void SysCfg_GraviRpmChanged(eGraviRpm,int idx);

  private slots:
    void BtMotorTypeClicked();
    void BtCilinderClicked();
    void BtGranulesClicked();
    void BtFillSystemClicked();
    //    void bt31_clicked() { emit selectionMade(6); }
    void btGraviRpm_clicked();
    void btLoadCell_Clicked();
    void btTolerances_clicked();
    //    void btSub4_clicked(); // Handled by other fuction: ShowAdvUnitConfig()
    void OkClicked();
    void inputReceived(const QString &);
    void UnitNameClicked();
    void BtDiagClicked();
    void BtServiceClicked();
    void BtManualIoClicked();

  public slots:
    void ReceiveSelection(int,int);
    void ShowSelForm();
    void BtLevelsCLicked();
    void ShowAdvUnitConfig();
    void UnitNameChanged(const QString &,int);
    void ActiveUnitIndexChanged(int);
    void SlaveCountChanged(int);
    void BtKnifeGateClicked();
    void BtTachoClicked();
    void BtMcwTypeClicked();
    //    void showMasterConfig();

    void SysCfg_SetDosTool(eDosTool,int);
    void SysCfg_SetMotorType(eMotor,int);
    void SysCfg_SetGranulate(eGranulate,int);
    void CfgSetFillSys(eFillSys,int);
    void MstCfg_SetInpType(eType);
    void SysCfg_SetGraviRpm(eGraviRpm,int);
    void CfgCurrentUserChanged(eUserType);
};
#endif                                                                          // UNITCONFIGSTD_H

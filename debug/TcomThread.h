#ifndef TCOMTHREAD_H
#define TCOMTHREAD_H

//#include <QThread>
#include <QObject>
#include <QString>
#include <QMetaType>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "ImageDef.h"

#include "sys/types.h"
#include "sys/ipc.h"
#include "sys/msg.h"

#define MSG_END 1000
#define COM_FLOATBUF_SIZE 30
#define COM_BYTEBUF_SIZE 40

enum eComError
{
  ceNone,
  ceNoSrvQueue,
  ceNoCltQueue,
  ceNoSend,
  ceNoReceive
};

/*
msgIn.dosSet = msgReceive.val1;
msgIn.dosAct = msgReceive.val2;
msgIn.actRpm = msgReceive.val3;
msgIn.weight = msgReceive.val4;
*/

struct MsgStruct
{
  long int msg_id;                                                              // size = 4, but does not add up for COM_MSG_SIZE
  float dosSet;
  float dosAct;
  float actRpm;
  float weight;
  float shotWeightSet;                                                          // Shot Weight set
  float dosTimeSet;                                                             // Dos time set
  unsigned char prodType;
  unsigned char InjInputMode;
  unsigned char ExtInputMode;
  unsigned char OperatingMode;
  unsigned short calib;
  // Size = 6x4 + 4 + 2 = 30

  // New added 19-08-2010
  float floatBuf[COM_FLOATBUF_SIZE];
  unsigned char byteBuf[COM_BYTEBUF_SIZE];
  // Size = 30 + (30x4) + 40 = 190;

  // Curve struct
  CurveStruct curve;
  // Size = 190 + (2*4*51)+4+(4*1) = 606
  AppParamStruct AppParameters;
};

struct MsgDataOut
{
  float colPct;
  float KgH;
  float primeRpm;
  float sysStartCmd;
  float ShotWeight;
  float DosTime;
  unsigned char prodType;
  unsigned char InjInputMode;
  unsigned char ExtInputMode;
  unsigned char OperatingMode;                                                  // (cal/rpm)
  unsigned char calib;
  unsigned char spare;
  // New added 19-08-2010
  float floatBuf[COM_FLOATBUF_SIZE];
  unsigned char byteBuf[COM_BYTEBUF_SIZE];

  CurveStruct curve;

  AppParamStruct AppParameters;
};
Q_DECLARE_METATYPE(MsgDataOut)

struct MsgDataIn
{
  float dosSet;
  float dosAct;
  float actRpm;
  float weight;
  float ShotWeight;
  float DosTime;
  unsigned char ProductionType;
  unsigned char InjInputMode;
  unsigned char ExtInputMode;
  unsigned char OperatingMode;                                                  // (cal/rpm)
  unsigned char calib;
  unsigned char spare;
  // New added 19-08-2010
  float floatBuf[COM_FLOATBUF_SIZE];
  unsigned char byteBuf[COM_BYTEBUF_SIZE];
  CurveStruct curve;

  AppParamStruct AppParameters;
};
Q_DECLARE_METATYPE(MsgDataIn)

class Com : public QObject
{
  Q_OBJECT
    private:
    bool end;
    MsgDataIn msgIn;
    MsgDataOut msgOut;
    int srvQueueID;
    int cltQueueID;
    MsgStruct msgSend;
    MsgStruct msgReceive;
    bool doSend;

  public:
    explicit Com(QObject *parent = 0);
    void run(void);
    void OpenMsgQueue(void);

    signals:
    void ComReceived(MsgDataIn);
    void MsgQueueError(int);

  public slots:
    void Terminate();
    void SendMsg(MsgDataOut);
};
#endif                                                                          // TCOMTHREAD_H

/*===========================================================================*
 * File        : Udp.h                                                       *
 * Project     : MVC0012 - MC-TC                                             *
 * Description : UDP message server defines.                                 *
 *===========================================================================*/

#ifndef UDP_H
#define UDP_H

#include <QObject>
#include <QTimer>
#include <QUdpSocket>
#include "Glob.h"
#include "Common.h"

/*----- Udp data buffer -----*/
struct sUdpBuf
{
  byte data[1500];
} __attribute__ ((packed));
typedef struct sUdpBuf tUdpBuf;

/*----- Dynamic log data point -----*/
struct sDynLogData
{
  byte logCode;                                                                 /* Log file code */
  byte dataType;                                                                /* Data type */
  float data;                                                                   /* Data */
}  __attribute__ ((packed));
typedef struct sDynLogData tDynLogData;

/*----- Dynamic log codes -----*/
#define LOG_STATUS                  64
#define LOG_DOS_TIME_ACT            65
#define LOG_TACHO_VOLT              66
#define LOG_WEIGHT                  67
#define LOG_RPM_ACT                 68
#define LOG_COLCAP_SET              69
#define LOG_FILL_VALVE              70
#define LOG_STAND_STILL             71
#define LOG_BAND_NR                 72
#define LOG_COLCAP_ACT              73
#define LOG_SAMPLE_LEN              74
#define LOG_RPM_CORFAC              75
#define LOG_CONSUMPTION             76
#define LOG_ALARM_NR_DYN            77

#define MAX_DYNAMIC_LOG_POINTS       10                                         /* Max dynamic log points */
#define MAX_DYNAMIC_MVC_LOG_POINTS 4                                            /* Max dynamic MovaColor log points */

/*------------*/
/* Data types */
/*------------*/
#define UDP_BYTE_TYPE         0x01                                              /* Byte data type */
#define UDP_WORD_TYPE         0x02                                              /* Word data type */
#define UDP_INT_TYPE          0x03                                              /* Int data type */
#define UDP_LONG_TYPE         0x04                                              /* Long data type */
#define UDP_FLOAT_TYPE        0x05                                              /* Float data type */
#define UDP_STRING_TYPE       0x07                                              /* String data type */

/*-------------------*/
/* MC-Lan structures */
/*-------------------*/

/*----- MC30 user program info structure -----*/
struct sUserPrgInfo
{
  word checkSum;                                                                /* User program checksum         0x100 */
  word userVersion;                                                             /* Software version user program 0x102 */
  char fileName[32];                                                            /* User program file name        0x104 */
  char languageId[4];                                                           /* Language Id                   0x124 */
  char buildTime[16];                                                           /* Build time                    0x128 */
  char buildDate[16];                                                           /* Build date                    0x138 */
  char info[32];                                                                /* Info                          0x148 */
} __attribute__ ((packed));
typedef struct sUserPrgInfo tUserPrgInfo;

/*----- Bootloader common header -----*/
struct sPcHdr
{
  byte msgType;                                                                 /* Message type */
  word msgCmd;                                                                  /* Message command */
} __attribute__ ((packed));
typedef struct sPcHdr tPcHdr;

/*----- Bootloader info request -----*/
struct sBootInfo
{
  byte msgType;                                                                 /* Message type */
  word msgCmd;                                                                  /* Message command */
  word mac0,mac1,mac2;                                                          /* MAC address */
  word blVersion;                                                               /* Bootloader version */
  tUserPrgInfo userPrgInfo;                                                     /* Application information */
  char unitName[32];                                                            /* Unit name */
  byte dipSwitch;                                                               /* DipSw setting */
} __attribute__ ((packed));
typedef struct sBootInfo tBootInfo;

/*----- McService record -----*/
struct sMcService
{
  byte msgType;                                                                 /* Message type */
  word msgCmd;                                                                  /* Message command */
  char unitName[16];                                                            /* Unit name */
  byte ip0,ip1,ip2,ip3;                                                         /* IP address */
  word mac0,mac1,mac2;                                                          /* MAC address */
} __attribute__ ((packed));
typedef sMcService tMcLan;

class Udp : public QObject
{
  /*-------------*/
  /* Udp:: Class */
  /*-------------*/
  Q_OBJECT
    public:
    explicit Udp(QObject *parent = 0);
    void FormInit();

  private:
    QUdpSocket *udpSocket;
    //    global * _gbi;
    QTimer * udpWdTim;
    void InitUpdWd();

    signals:

  private slots:
    void UdpReceive();
    void SendUdpData();
    void UdpWdTimeOut();
    void ComMsgReceived(tMsgControlUnit,int);

  public slots:

};
#endif                                                                          // USBACCESS_H

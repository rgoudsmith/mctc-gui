#include "Form_Keyboard.h"
#include "ui_Form_Keyboard.h"
#include "ImageDef.h"
#include "Form_MainMenu.h"
#include "Rout.h"

#define MAX_LENGTH 35

KeyboardFrm::KeyboardFrm(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::KeyboardFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  m_ui->setupUi(this);
  maxLength = MAX_LENGTH;
  capsState = 0;
  emitSignal = false;

  /*----- Disable windows title bar -----*/
  this->setWindowFlags(Qt::CustomizeWindowHint);

  // "Special" buttons
  connect(m_ui->btShift,SIGNAL(clicked()),this,SLOT(shiftClicked()));
  connect(m_ui->btBackspace,SIGNAL(clicked()),this,SLOT(backspaceClicked()));
  connect(m_ui->btSpace,SIGNAL(clicked()),this,SLOT(spaceClicked()));
  connect(m_ui->btClear,SIGNAL(clicked()),this,SLOT(clearClicked()));
  connect(m_ui->btOk,SIGNAL(clicked()),this,SLOT(okClicked()));
  connect(m_ui->btCancel,SIGNAL(clicked()),this,SLOT(cancelClicked()));

  // Full alphabet
  connect(m_ui->btA,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btB,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btC,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btD,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btE,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btF,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btG,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btH,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btI,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btJ,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btK,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btL,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btM,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btN,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btO,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btP,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btQ,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btR,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btS,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btT,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btU,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btV,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btW,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btX,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btY,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->btZ,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  // Dash key
  connect(m_ui->btDash,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  // Numerics
  connect(m_ui->bt1,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->bt2,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->bt3,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->bt4,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->bt5,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->bt6,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->bt7,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->bt8,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->bt9,SIGNAL(clicked()),this,SLOT(buttonClicked()));
  connect(m_ui->bt0,SIGNAL(clicked()),this,SLOT(buttonClicked()));
}


KeyboardFrm::~KeyboardFrm()
{
  delete m_ui;
}


void KeyboardFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      m_ui->retranslateUi(this);
      break;
    default:
      break;
  }
}


void KeyboardFrm::showEvent(QShowEvent *e)
{
  QWidget::showEvent(e);
  glob->mstSts.popupActive = true;
  emitSignal = true;

  if ((glob->menu) != 0) {
    ((MainMenu*)(glob->menu))->FoldMenu();
  }
}


void KeyboardFrm::updateKeys()
{
  switch(capsState) {
    case 0:
      m_ui->btA->setText("a");
      m_ui->btB->setText("b");
      m_ui->btC->setText("c");
      m_ui->btD->setText("d");
      m_ui->btE->setText("e");
      m_ui->btF->setText("f");
      m_ui->btG->setText("g");
      m_ui->btH->setText("h");
      m_ui->btI->setText("i");
      m_ui->btJ->setText("j");
      m_ui->btK->setText("k");
      m_ui->btL->setText("l");
      m_ui->btM->setText("m");
      m_ui->btN->setText("n");
      m_ui->btO->setText("o");
      m_ui->btP->setText("p");
      m_ui->btQ->setText("q");
      m_ui->btR->setText("r");
      m_ui->btS->setText("s");
      m_ui->btT->setText("t");
      m_ui->btU->setText("u");
      m_ui->btV->setText("v");
      m_ui->btW->setText("w");
      m_ui->btX->setText("x");
      m_ui->btY->setText("y");
      m_ui->btZ->setText("z");
      m_ui->btDash->setText("-");
      break;
    case 1:
    case 2:
      m_ui->btA->setText("A");
      m_ui->btB->setText("B");
      m_ui->btC->setText("C");
      m_ui->btD->setText("D");
      m_ui->btE->setText("E");
      m_ui->btF->setText("F");
      m_ui->btG->setText("G");
      m_ui->btH->setText("H");
      m_ui->btI->setText("I");
      m_ui->btJ->setText("J");
      m_ui->btK->setText("K");
      m_ui->btL->setText("L");
      m_ui->btM->setText("M");
      m_ui->btN->setText("N");
      m_ui->btO->setText("O");
      m_ui->btP->setText("P");
      m_ui->btQ->setText("Q");
      m_ui->btR->setText("R");
      m_ui->btS->setText("S");
      m_ui->btT->setText("T");
      m_ui->btU->setText("U");
      m_ui->btV->setText("V");
      m_ui->btW->setText("W");
      m_ui->btX->setText("X");
      m_ui->btY->setText("Y");
      m_ui->btZ->setText("Z");
      m_ui->btDash->setText("_");
      break;
  }
}


void KeyboardFrm::shiftClicked()
{
  capsState++;
  if (capsState > 2) {
    capsState = 0;
  }
  updateKeys();
}


void KeyboardFrm::buttonClicked()
{
  if (m_ui->inputField->text().length() < maxLength) {
    m_ui->inputField->setText(m_ui->inputField->text() + (qobject_cast<QPushButton*>(sender()))->text());
  }

  if (capsState == 1) {
    capsState = 0;
    updateKeys();
  }
}


void KeyboardFrm::backspaceClicked()
{
  // Backspace, remove last character from the string.
  m_ui->inputField->setText(m_ui->inputField->text().left(m_ui->inputField->text().length()-1));
}


void KeyboardFrm::spaceClicked()
{
  if (m_ui->inputField->text().length() < maxLength) {
    m_ui->inputField->setText(m_ui->inputField->text() + " ");
  }
}


void KeyboardFrm::clearClicked()
{
  m_ui->inputField->setText("");
}


void KeyboardFrm::cancelClicked()
{
  glob->mstSts.popupActive = false;
  close();
}


void KeyboardFrm::okClicked()
{
  input = m_ui->inputField->text();
  if (emitSignal) {
    emitSignal = false;
    emit inputText(input);
  }
  cancelClicked();
}


void KeyboardFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  glob->SetButtonImage(m_ui->btOk,itSelection,selOk);
  glob->SetButtonImage(m_ui->btCancel,itSelection,selCancel);
  glob->SetButtonImage(m_ui->btBackspace,itGeneral,genInpBackspace2);
  glob->SetButtonImage(m_ui->btClear,itGeneral,genInpClear2);

  m_ui->bt0->setFont(*(glob->baseFont));
  m_ui->bt1->setFont(*(glob->baseFont));
  m_ui->bt2->setFont(*(glob->baseFont));
  m_ui->bt3->setFont(*(glob->baseFont));
  m_ui->bt4->setFont(*(glob->baseFont));
  m_ui->bt5->setFont(*(glob->baseFont));
  m_ui->bt6->setFont(*(glob->baseFont));
  m_ui->bt7->setFont(*(glob->baseFont));
  m_ui->bt8->setFont(*(glob->baseFont));
  m_ui->bt9->setFont(*(glob->baseFont));

  m_ui->btA->setFont(*(glob->baseFont));
  m_ui->btB->setFont(*(glob->baseFont));
  m_ui->btC->setFont(*(glob->baseFont));
  m_ui->btD->setFont(*(glob->baseFont));
  m_ui->btE->setFont(*(glob->baseFont));
  m_ui->btF->setFont(*(glob->baseFont));
  m_ui->btG->setFont(*(glob->baseFont));
  m_ui->btH->setFont(*(glob->baseFont));
  m_ui->btI->setFont(*(glob->baseFont));
  m_ui->btJ->setFont(*(glob->baseFont));
  m_ui->btK->setFont(*(glob->baseFont));
  m_ui->btL->setFont(*(glob->baseFont));
  m_ui->btM->setFont(*(glob->baseFont));
  m_ui->btN->setFont(*(glob->baseFont));
  m_ui->btO->setFont(*(glob->baseFont));
  m_ui->btP->setFont(*(glob->baseFont));
  m_ui->btQ->setFont(*(glob->baseFont));
  m_ui->btR->setFont(*(glob->baseFont));
  m_ui->btS->setFont(*(glob->baseFont));
  m_ui->btT->setFont(*(glob->baseFont));
  m_ui->btU->setFont(*(glob->baseFont));
  m_ui->btV->setFont(*(glob->baseFont));
  m_ui->btW->setFont(*(glob->baseFont));
  m_ui->btX->setFont(*(glob->baseFont));
  m_ui->btY->setFont(*(glob->baseFont));
  m_ui->btZ->setFont(*(glob->baseFont));

  m_ui->btSpace->setFont(*(glob->baseFont));
  m_ui->btShift->setFont(*(glob->baseFont));
  m_ui->btDash->setFont(*(glob->baseFont));
  m_ui->btBackspace->setFont(*(glob->baseFont));
  m_ui->btClear->setFont(*(glob->baseFont));
}


void KeyboardFrm::SetMaxLength(int len)
{
  this->maxLength = len;
  if (maxLength > MAX_LENGTH) {
    maxLength = MAX_LENGTH;
  }
}


void KeyboardFrm::Clear()
{
  this->clearClicked();
}


void KeyboardFrm::SetText(QString txt)
{
  m_ui->inputField->setText(txt);
}

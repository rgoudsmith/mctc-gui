/*===================================================================================================*/
/* File        : Form_OverlayMenu.cpp                                                                */
/* Description : Side bar menu, this menu becomes active when menu button pressed from the main menu */
/*===================================================================================================*/

#include "Form_OverlayMenu.h"
#include "ui_Form_OverlayMenu.h"
#include "Form_MainMenu.h"
#include "Rout.h"

OverlayMenu::OverlayMenu(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::OverlayMenu)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  hideMenuOnClick = false;
  firstShow = true;
  isVisible = false;

  m_ui->setupUi(this);

  // Export all button clicks as public signals of the menu.
  connect(m_ui->btLogin,SIGNAL(clicked()),this,SLOT(BtLoginClicked()));
  connect(m_ui->btConfig,SIGNAL(clicked()),this,SLOT(BtConfigClicked()));
  connect(m_ui->btAlarm,SIGNAL(clicked()),this,SLOT(BtAlarmClicked()));
  connect(m_ui->btLearn,SIGNAL(clicked()),this,SLOT(BtLearnClicked()));
  connect(m_ui->btUsb,SIGNAL(clicked()),this,SLOT(BtUsbClicked()));
  connect(m_ui->btKeyLock,SIGNAL(clicked()),this,SLOT(BtKeyLockClicked()));
  connect(m_ui->btConsumption,SIGNAL(clicked()),this,SLOT(BtConsumptionClicked()));
}


OverlayMenu::~OverlayMenu()
{
  delete m_ui;
}


void OverlayMenu::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      m_ui->retranslateUi(this);
      break;
    default:
      break;
  }
}


void OverlayMenu::showEvent(QShowEvent *e)
{
  QBaseWidget::showEvent(e);

  /*----- Rebuild overlay menu -----*/
  UserChanged(glob->GetUser());
}


void OverlayMenu::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  connect(glob,SIGNAL(MstCfg_UserChanged(eUserType)),this,SLOT(UserChanged(eUserType)));
}


void OverlayMenu::Reposition(int w, int h)
{
  parentW = w;
  parentH = h;
  UserChanged(glob->GetUser());
}


// SLOTS

void OverlayMenu::HideMenuOnClick(bool hide)
{
  this->hideMenuOnClick = hide;
}


void OverlayMenu::DisplayMenu()
{
  if (this->isVisible) {
    this->hide();
    isVisible = false;
  }
  else {
    if (firstShow) {
      firstShow = false;
      UserChanged(glob->GetUser());
    }

    this->show();
    isVisible = true;
  }
}


void OverlayMenu::FoldMenu()
{
  if (hideMenuOnClick) {
    if (this->isVisible) {
      DisplayMenu();
    }
  }
}


QPushButton * OverlayMenu::GetMenuButton(int _idx)
{
  switch(_idx) {
    case 0:
      return m_ui->btLogin;
    case 1:
      return m_ui->btConfig;
    case 2:
      return m_ui->btAlarm;
    case 3:
      return m_ui->btLearn;
    case 4:
      return m_ui->btUsb;
    case 5:
      return m_ui->btKeyLock;
    case 6:
      return m_ui->btConsumption;

    default:
      return 0;
  }
}


void OverlayMenu::SetMenuLength(int actButCnt)
{
  int thisW,thisH,thisX,thisY;

  thisX = this->pos().x();
  thisW = this->width();

  thisH = (actButCnt*70) + ((actButCnt*5) + 15);
  thisY = parentH - (thisH + 80);
  this->setGeometry(thisX,thisY,thisW,thisH);
}


void OverlayMenu::UserChanged(eUserType ut)
{
  int btnCnt;                                                                   /* Number of buttons in overlay menu */
  int idx;

  btnCnt = 1;
  idx = glob->ActUnitIndex();

  /*----- Setup the keylock button image -----*/
  SetKeyLockButtonImage();

  switch (ut) {
    case utNone:
      // Lowest possible user level.
      // TODO: Might be equal to utOperator.
      UserChanged(utOperator);
      break;

    case utOperator:
      // Default (lowest) normal user level.
      m_ui->btConfig->setVisible(false);
      m_ui->btAlarm->setVisible(false);
      m_ui->btLearn->setVisible(false);
      m_ui->btUsb->setVisible(false);
      m_ui->btConsumption->setVisible(true);
      btnCnt+=1;

      /* When keylock is active show the lock indicator button in the menu */
      /* This button is not active but only used as an indicator */
      if (Rout::keyLock) {
        m_ui->btKeyLock->setVisible(true);
        glob->SetButtonImage(GetMenuButton(5),itMenu,mnLockIndicator);
        btnCnt+=1;                                                              /* Adjust button lenght   */
      }
      /*----- Keylock not active, show no button -----*/
      else {
        m_ui->btKeyLock->setVisible(false);
      }

      SetMenuLength(btnCnt);
      break;

    case utTooling:
      // Medium normal user level.
      m_ui->btConfig->setVisible(true);
      m_ui->btAlarm->setVisible(false);
      m_ui->btLearn->setVisible(true);
      m_ui->btUsb->setVisible(false);
      m_ui->btKeyLock->setVisible(true);
      m_ui->btConsumption->setVisible(true);
      btnCnt+=4;
      SetMenuLength(btnCnt);
      break;

    case utSupervisor:
      // Highest normal user level
      m_ui->btConfig->setVisible(true);
      m_ui->btAlarm->setVisible(true);
      m_ui->btLearn->setVisible(true);
      m_ui->btUsb->setVisible(true);
      m_ui->btKeyLock->setVisible(true);
      m_ui->btConsumption->setVisible(true);
      btnCnt += 6;                                                              /* Adjust button lenght   */
      SetMenuLength(btnCnt);
      break;

    case utSupervisorBackdoor:
      // Universal supervisor account (fixed password);
      UserChanged(utSupervisor);
      break;

    case utAgent:                                                               /* Agent user level: menu is identical to MovaColor login. */
    case utMovaColor:                                                           /* Highest clearance level. Everything is available */
    case utSyrinx:
      m_ui->btConfig->setVisible(true);
      m_ui->btAlarm->setVisible(true);
      m_ui->btLearn->setVisible(true);
      m_ui->btUsb->setVisible(true);                                            /* Enable USB button */
      m_ui->btKeyLock->setVisible(true);
      m_ui->btConsumption->setVisible(true);
      btnCnt+=6;                                                                /* Adjust button lenght   */
      SetMenuLength(btnCnt);
      break;

    default:
      UserChanged(utNone);
      break;
  }

  /*----- Alarm button disabled -----*/
  m_ui->btAlarm->setVisible(false);
  btnCnt--;

  /*----- Disable Learn button when McWeight type -----*/
  if (Rout::ActUnitType() == mcdtWeight) {
    if (m_ui->btLearn->isVisible()) {
      m_ui->btLearn->setVisible(false);
      btnCnt--;
    }
  }
}


void OverlayMenu::SetBtLoginCaption(const QString &caption)
{
  m_ui->btLogin->setText(caption);
}


void OverlayMenu::SetBtConfigCaption(const QString &caption)
{
  m_ui->btConfig->setText(caption);
}


void OverlayMenu::SetBtAlarmCaption(const QString &caption)
{
  m_ui->btAlarm->setText(caption);
}


void OverlayMenu::SetBtLearnCaption(const QString &caption)
{
  m_ui->btLearn->setText(caption);
}


void OverlayMenu::SetBtUsbCaption(const QString &caption)
{
  m_ui->btUsb->setText(caption);
}


void OverlayMenu::BtLoginClicked()
{
  if (m_ui->btLogin->isVisible()) {
    emit MenuButtonClicked(wiLogin);
    FoldMenu();
  }
}


void OverlayMenu::BtConfigClicked()
{
  if (m_ui->btConfig->isVisible()) {
    emit MenuButtonClicked(wiUnitCfgStd);
    FoldMenu();
  }
}


void OverlayMenu::BtAlarmClicked()
{
  if (m_ui->btAlarm->isVisible()) {
    emit MenuButtonClicked(wiTrending);
    FoldMenu();
  }
}


void OverlayMenu::BtLearnClicked()
{
  if (m_ui->btLearn->isVisible()) {
    if (glob->mstSts.sysActive) {

      /*----- Online calibration -----*/
      emit MenuButtonClicked(wiMaterialCalibOnline);
      // Display a message window to save/save as/cancel the recipe.
      // ((MainMenu*)(glob->menu))->DisplayMessage(0,0,"Saving the current material will overwrite\nthe original. Choose 'Save As'\nto create a new material.",msgtSave);
    }
    else {
      /*----- Offline calibration -----*/
      #warning SORRY ROB
      glob->mstSts.learnOffLineActive = true;                                   //@@@jan
      emit MenuButtonClicked(wiMaterialCalib);
    }
    FoldMenu();
  }
}


void OverlayMenu::BtUsbClicked()
{
  if (m_ui->btUsb->isVisible()) {
    emit MenuButtonClicked(wiUSB);
    FoldMenu();
  }
}


void OverlayMenu::SetKeyLockButtonImage()
{
  /*-----------------------------------------------------------------*/
  /* Set the keylock button image depending on lock or unlocked mode */
  /*-----------------------------------------------------------------*/
  if (Rout::keyLock) {
    /*----- Locked image -----*/
    glob->SetButtonImage(GetMenuButton(5),itMenu,mnLock);
  }
  else {
    /*----- Unlocked image -----*/
    glob->SetButtonImage(GetMenuButton(5),itMenu,mnUnlock);
  }
}


void OverlayMenu::BtKeyLockClicked()
/*------------------------------*/
/* Lock keyboard button pressed */
/*-----------------------------*/
{
  if (m_ui->btKeyLock->isVisible()) {

    /*----- Keylock mode can only be changed when no operator -----*/
    if (glob->GetUser() > utOperator) {

      /*----- Toggle keylock mode -----*/
      Rout::keyLock = !Rout::keyLock;

      /*----- Update lock button image -----*/
      SetKeyLockButtonImage();

      /*----- Rebuild the production screen to update the read-only flags -----*/
      glob->RebuildProdScreen();
    }
  }
}


void OverlayMenu::BtConsumptionClicked()
/*----------------------------*/
/* Consumption button pressed */
/*---------------------------*/
{
  if (m_ui->btConsumption->isVisible()) {
    emit MenuButtonClicked(wiConsumption);
    FoldMenu();
  }

}

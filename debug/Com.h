#ifndef COM_H
#define COM_H

#include <QObject>
#include <QString>
#include <QMetaType>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "Common.h"
#include "ImageDef.h"
#include "ComShare.h"

#include "sys/types.h"
#include "sys/ipc.h"
#include "sys/msg.h"

Q_DECLARE_METATYPE(tMsgGuiUnit)
Q_DECLARE_METATYPE(tMsgControlUnit)

class Com : public QObject
{
  Q_OBJECT
    private:
    bool end;
    tMsgControl msgIn;
    int srvQueueID;
    int cltQueueID;
    tMsgGui msgSend;
    tMsgControl msgReceive;

  public:
    explicit Com(QObject *parent = 0);
    void run(void);

    void OpenMsgQueue(void);

    signals:
    void ComReceived(tMsgControl);

  public slots:

    void SendMsg(tMsgGui);
};
#endif                                                                          // COM_H

#ifndef PRIMEFRM_H
#define PRIMEFRM_H

#include <QWidget>
#include <QTimer>
#include <QPushButton>
#include "Form_QbaseWidget.h"
#include "Form_NumericInput.h"

namespace Ui
{
  class PrimeFrm;
}


class PrimeFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    PrimeFrm(QWidget *parent = 0);
    ~PrimeFrm();
    void FormInit();

  protected:
    void changeEvent(QEvent *e);
    void showEvent(QShowEvent *);
    void resizeEvent(QResizeEvent *);
    bool eventFilter(QObject *, QEvent *);

  private:
    Ui::PrimeFrm *ui;
    QTimer * primeTim;
    QPushButton * btPrimeAll;
    NumericInputFrm * numInput;
    int lineIdx;
    bool SetPrime;
    void DisplayProgress(bool,int);
    void StartPrime(int);
    void StopPrime(int);
    void SetupDisplay();

  private slots:
    void btPrimeClicked();
    void btPrime2Clicked();
    void btCancelClicked();
    void ValueReceived(float);
    void PrimeRpmChanged(float,int);
    void PrimeTimeChanged(int,int);
    void UnitNameChanged(const QString &,int);
    void TimUpdate();
    void TestRpmChanged(float,int);
    void TestTimeChanged(float,int);

  public slots:
    void ActUnitIndexChanged(int);
    void SlaveCountChanged(int);
    void PrimeStatusChanged(bool,int);
};
#endif                                                                          // PRIMEFRM_H

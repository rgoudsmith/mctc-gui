#ifndef FORM_MCWEIGHTSETTINGS_H
#define FORM_MCWEIGHTSETTINGS_H

#include <QWidget>
#include "Form_QbaseWidget.h"
#include "Form_Selection.h"
#include "Form_Keyboard.h"
#include "Form_NumericInput.h"

namespace Ui
{
  class McWeightSettingsFrm;
}


class McWeightSettingsFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit McWeightSettingsFrm(QWidget *parent = 0);
    ~McWeightSettingsFrm();
    void FormInit();

  protected:
    void showEvent(QShowEvent *);
    bool eventFilter(QObject *, QEvent *);
    void resizeEvent(QResizeEvent *);

  private:
    Ui::McWeightSettingsFrm *m_ui;
    NumericInputFrm * numKeys;
    void SetButtonImages();
    void TachoRatioClicked();

  public slots:
    void UnitValReceived(float);
    void OkClicked();
    void CancelClicked();
};
#endif                                                                          // FORM_MCWEIGHTSETTINGS_H

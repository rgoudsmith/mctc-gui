/********************************************************************************
** Form generated from reading UI file 'Form_LearnOnline.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_LEARNONLINE_H
#define UI_FORM_LEARNONLINE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "Form_LearnOnlineItem.h"

QT_BEGIN_NAMESPACE

class Ui_LearnOnlineFrm
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QLabel *lbTitle;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *verticalSpacer;
    LearnOnlineItemFrm *learnData1;
    LearnOnlineItemFrm *learnData2;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btOk;

    void setupUi(QWidget *LearnOnlineFrm)
    {
        if (LearnOnlineFrm->objectName().isEmpty())
            LearnOnlineFrm->setObjectName(QString::fromUtf8("LearnOnlineFrm"));
        LearnOnlineFrm->resize(707, 566);
        LearnOnlineFrm->setCursor(QCursor(Qt::BlankCursor));
        LearnOnlineFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout = new QVBoxLayout(LearnOnlineFrm);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        lbTitle = new QLabel(LearnOnlineFrm);
        lbTitle->setObjectName(QString::fromUtf8("lbTitle"));

        horizontalLayout_2->addWidget(lbTitle);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        learnData1 = new LearnOnlineItemFrm(LearnOnlineFrm);
        learnData1->setObjectName(QString::fromUtf8("learnData1"));
        learnData1->setMinimumSize(QSize(0, 120));

        verticalLayout->addWidget(learnData1);

        learnData2 = new LearnOnlineItemFrm(LearnOnlineFrm);
        learnData2->setObjectName(QString::fromUtf8("learnData2"));
        learnData2->setMinimumSize(QSize(0, 120));

        verticalLayout->addWidget(learnData2);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btOk = new QPushButton(LearnOnlineFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout->addWidget(btOk);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(LearnOnlineFrm);

        QMetaObject::connectSlotsByName(LearnOnlineFrm);
    } // setupUi

    void retranslateUi(QWidget *LearnOnlineFrm)
    {
        lbTitle->setText(QApplication::translate("LearnOnlineFrm", "Save Material", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(LearnOnlineFrm);
    } // retranslateUi

};

namespace Ui {
    class LearnOnlineFrm: public Ui_LearnOnlineFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_LEARNONLINE_H

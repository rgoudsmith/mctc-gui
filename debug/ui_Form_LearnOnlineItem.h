/********************************************************************************
** Form generated from reading UI file 'Form_LearnOnlineItem.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_LEARNONLINEITEM_H
#define UI_FORM_LEARNONLINEITEM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LearnOnlineItemFrm
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QLabel *lbUnit;
    QLineEdit *edMaterial;
    QSpacerItem *horizontalSpacer;
    QPushButton *btSave;

    void setupUi(QWidget *LearnOnlineItemFrm)
    {
        if (LearnOnlineItemFrm->objectName().isEmpty())
            LearnOnlineItemFrm->setObjectName(QString::fromUtf8("LearnOnlineItemFrm"));
        LearnOnlineItemFrm->resize(678, 130);
        LearnOnlineItemFrm->setCursor(QCursor(Qt::BlankCursor));
        LearnOnlineItemFrm->setWindowTitle(QString::fromUtf8("Form"));
        LearnOnlineItemFrm->setStyleSheet(QString::fromUtf8("#lbUnit{border-color: black; border: 1px solid; border-radius: 3px;}\n"
"\n"
"#lbMaterial{border-color: black; border: 1px solid; border-radius: 3px;}\n"
"\n"
"#lbCylinder{border-color: black; border: 0px solid; border-radius: 3px;}\n"
"\n"
"#groupBox{border-color: black; border: 1px solid; border-radius: 3px;}"));
        verticalLayout = new QVBoxLayout(LearnOnlineItemFrm);
        verticalLayout->setContentsMargins(2, 2, 2, 2);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(LearnOnlineItemFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        horizontalLayout = new QHBoxLayout(groupBox);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        lbUnit = new QLabel(groupBox);
        lbUnit->setObjectName(QString::fromUtf8("lbUnit"));
        lbUnit->setMinimumSize(QSize(150, 70));
        lbUnit->setMaximumSize(QSize(16777215, 70));
        lbUnit->setFrameShape(QFrame::NoFrame);
        lbUnit->setText(QString::fromUtf8(""));
        lbUnit->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(lbUnit);

        edMaterial = new QLineEdit(groupBox);
        edMaterial->setObjectName(QString::fromUtf8("edMaterial"));
        edMaterial->setMinimumSize(QSize(250, 70));
        edMaterial->setMaximumSize(QSize(250, 70));
        edMaterial->setFocusPolicy(Qt::NoFocus);
        edMaterial->setText(QString::fromUtf8(""));
        edMaterial->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(edMaterial);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btSave = new QPushButton(groupBox);
        btSave->setObjectName(QString::fromUtf8("btSave"));
        btSave->setMinimumSize(QSize(90, 70));
        btSave->setMaximumSize(QSize(90, 70));
        btSave->setText(QString::fromUtf8("SAVE"));

        horizontalLayout->addWidget(btSave);


        verticalLayout->addWidget(groupBox);


        retranslateUi(LearnOnlineItemFrm);

        QMetaObject::connectSlotsByName(LearnOnlineItemFrm);
    } // setupUi

    void retranslateUi(QWidget *LearnOnlineItemFrm)
    {
        groupBox->setTitle(QString());
        Q_UNUSED(LearnOnlineItemFrm);
    } // retranslateUi

};

namespace Ui {
    class LearnOnlineItemFrm: public Ui_LearnOnlineItemFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_LEARNONLINEITEM_H

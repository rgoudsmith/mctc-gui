#ifndef UDP_H
#define UDP_H

#include <QObject>
#include <QTimer>

class Udp : public QObject
{
  Q_OBJECT
    public:
    explicit Udp(QObject *parent = 0);
    bool Test();
    void SetGBI(QObject *);

  private:
    QObject * _gbi;

    signals:

  private slots:

  public slots:
};
#endif                                                                          // UDP.H

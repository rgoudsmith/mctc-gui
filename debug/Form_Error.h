#ifndef ERRORFRM_H
#define ERRORFRM_H

#include <QWidget>
#include <QTimer>
#include "Form_QbaseWidget.h"

namespace Ui
{
  class ErrorFrm;
}


class ErrorFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    ErrorFrm(QWidget *parent = 0);
    ~ErrorFrm();
    void FormInit();
    void ShowMessage(msgType,int,int,const QString &);
    void ShowMessage(int,const QString &,int);
    int LastSelection();

  protected:
    void changeEvent(QEvent *e);

  private:
    Ui::ErrorFrm *ui;
    int unitIndex;
    msgType mType;
    int lastSelect;
    QTimer * closeTim;
    void CancelAlarm();
    void SetButtons(msgType);
    void LanguageUpdate();
    void CancelForm();

  private slots:
    void on_btStop_pressed();
    void OnCloseTim();
    void BtOkClicked();
    void BtCancelClicked();
    void BtHelpClicked();
    void BtHelp2Clicked();
    void AlarmStatusChanged(bool,int);
    void AlarmModeChanged(int,int);

  public slots:
    void SysActiveChanged(bool);

    signals:
    void ButtonClicked(int);
};
#endif                                                                          // ERRORFRM_H

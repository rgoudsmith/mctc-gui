#ifndef MAINMENU_H
#define MAINMENU_H

#include <QtGui/QWidget>
#include <QFormLayout>
#include <QTimer>

#include "Form_QbaseWidget.h"
#include "Form_OverlayMenu.h"
#include "Form_UserLogin.h"
#include "Form_UnitConfigStd.h"
#include "Form_MvcSystem.h"
#include "Form_Production.h"
#include "Form_FillSystem.h"
#include "Form_NumericInput.h"
#include "Form_Keyboard.h"
#include "Form_AdvLoaderSettings.h"
#include "Form_Tolerances.h"
#include "Form_CalibSelect.h"
#include "Form_AdvConfig2.h"
#include "Form_SystemCalibration.h"
#include "Form_EventLogHistory.h"
#include "Form_Prime.h"
#include "Form_MultiUnitSelect.h"
#include "Form_Splash.h"
#include "Form_Error.h"
#include "Form_MaterialSelect.h"
#include "Form_WeightCheck.h"
#include "Form_RecipeSelect.h"
#include "Form_Recipe.h"
#include "Form_EventLogHistory.h"
#include "Form_IpEditor.h"
#include "Form_Settings.h"
#include "FileSystemControl.h"
#include "Form_ProcessDiagnostic.h"
#include "AlarmHandler.h"
#include "Form_DateTimeEditor.h"
#include "Form_Manual.h"
#include "Form_LearnOnline.h"
#include "Form_PopupSettingConfirm.h"
#include "Form_UsbSelect.h"
#include "Form_UsbTransfer.h"
#include "Form_AlarmConfig.h"
#include "Form_Consumption.h"
#include "Form_RegrindConfig.h"
#include "Form_McWeight.h"
#include "Form_McWeightSettings.h"
#include "Udp.h"
#include "McServer.h"
#include "LogServer.h"
#include "ModbusServer.h"

namespace Ui
{
  class MainMenu;
}


class MainMenu : public QBaseWidget
{
  Q_OBJECT
    public:
    MainMenu(QWidget *parent = 0);
    ~MainMenu();
    QString * GetAppPath();
    QWidget * actWindow;

    global* GBI();
    FileSystemControl fileSysCtrl;

    void initOverlay();
    void DisplayPrime(bool);
    void SetAppPath(QString *_path);
    void SetOverlayMenu(QWidget *oMenu = 0);
    int LastPopupSelection();
    AlarmHandler * alarmHndlr;
    DateTimeEditorFrm * dateTimeForm;
    ProductionFrm * prodForm;
    RecipeFrm * recipeConfigForm;
    void FoldMenu();
    void SetHomeTimerInterval(int);
    MultiUnitSelectFrm * unitSelForm;

  protected:
    bool event(QEvent *);
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *);

  private:
    int popupID;

    Ui::MainMenu *m_ui;
    QWidget * OverMenu;
    QString * AppPath;
    QTimer * homeTim;
    QVBoxLayout * layout;
    eWindowIdx actWindowIdx;
    UserLoginFrm * loginForm;
    UnitConfigStdFrm * stdConfig;
//  ProductionFrm * prodForm;
    McWeightFrm * mcWeightForm;
    FillSystemFrm * fillSysForm;
    AdvLoaderSettingsFrm * advFillSysForm;
    TolerancesFrm * tolerancesForm;
    CalibSelectFrm * calibSelectForm;
    AdvConfig2Frm * advConfig2Form;
    SystemCalibrationFrm * sysCalibForm;
    EventLogHistoryFrm * historyForm;
    PrimeFrm * primeForm;
    SplashFrm * splashForm;
    ErrorFrm * errorForm;
    MaterialSelectFrm * materialForm;
    WeightCheckFrm * weightCheckForm;
    RecipeSelectFrm * recipeSelectForm;
//    RecipeFrm * recipeConfigForm;
    IpEditorFrm * ipEditorForm;
    SettingsFrm * settingsForm;
    ProcessDiagnosticFrm * balanceDiagForm;
    ManualFrm * superVisorForm;
    LearnOnlineFrm * learnOnlineForm;
    PopupSettingConfirmFrm * popupSetConfirmForm;
    USBSelectFrm * usbSelectForm;
    UsbTransferFrm * usbTransForm;
    AlarmConfigFrm * alarmConfigForm;
    ConsumptionFrm * consumptionForm;
    RegrindConfigFrm * regrindConfigForm;
    McWeightSettingsFrm * mcWeightSettingsFrm;
    void PositionOverlayMenu();
    Udp* udp;
    McServer* mcServer;
    LogServer* logServer;
    ModbusServer* modBusServer;

    signals:
    void MenuClicked();
    void UnitSelected();
    void ErrorResult(int);
    void PrimeActiveChange(bool,int);

  private slots:
    void BackClicked();
    void btMenuClicked();
    void McStatusChanged(eMCStatus, int);
    void ReturnHomeScreen();
    void StoreMasterSettings();

  public slots:
    void DisplayWindow(eWindowIdx windowNr, int calWindow = -1);
    void TimeChanged(const QDateTime &);
    void HomeClicked();
    void AlarmClicked();
    void PrimeClicked();
    void OnOffClicked();
    void SelectClicked();
    void SysActiveChanged(bool);
    void ErrorResultReceived(int);
    int DisplayMessage(int,int,const QString&,int type = 0);
    int DisplayMessage(int, const QString &, int);
    void DisplayMessage(const QString &, int, int, int);
    void ActiveAlarmHandler();
    void PrimeActiveChanged(bool,int);
    //    void SetHomeTimerInterval(int);
    void CfgCurrentUserChanged(eUserType);
    void LanguageChanged();
    void StoreSystemSettings(int);

};
#endif                                                                          // MAINMENU_H

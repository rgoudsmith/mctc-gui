#ifndef SHARED_H
#define SHARED_H

#define MAX_CURVE_LINES 51

struct CurveLineStruct
{
  float motorSpeed;
  float capacity;
};                                                                              // Size = 2 bytes

struct CurveStruct                                                              // Material struct
{
  unsigned char swVersion;
  unsigned char cylSel;
  unsigned char matType;
  unsigned char calMode;
  float corFac;
  CurveLineStruct curveLines[MAX_CURVE_LINES];                                  // length = 51
};                                                                              // Size = 416 bytes

typedef struct
{
  float colPct;
  float KgH;
  float primeRpm;
  float sysStartCmd;
  float ShotWeight;
  float DosTime;
  unsigned char prodType;
  unsigned char InjInputMode;
  unsigned char ExtInputMode;
  unsigned char OperatingMode;                                                  // (cal/rpm)
  unsigned char calib;
  unsigned char spare0;

  /*------ Floatbuf[30] -----*/
  float spare2[11];                                                             /* F0-F10 */
  float tachoMax;                                                               /* F11 */
  float dosTimeSetMetStart;                                                     /* F12 */
  float rpmSet;                                                                 /* F13 */
  float manMotorSpeed;                                                          /* F14 */
  float spare3[15];                                                             /* F15-F29 */

  /*----- Bytebuf[40] -----*/
  byte spare[5];                                                                /* B0-B4 */
  byte fillMan;                                                                 /* B5 */
  byte preCalStart;                                                             /* B6 */
  byte preCalAbort;                                                             /* B7 */
  byte preCalPause;                                                             /* B8 */
  byte preCalReadyAck;                                                          /* B9 */
  byte preCalContinue;                                                          /* B10 */
  byte consReset;                                                               /* B11 : Consumption reset */
  byte consResetDay;
  /* B12 : Consumption reset */
  byte prodTestStart;                                                           /* B13 */
  byte prodTestAbort;                                                           /* B14 */
  byte alarmResetCmd;                                                           /* B15 */
  byte manualIO;                                                                /* B16 */
  byte manOutput[4];                                                            /* B17-B20 */
  byte curveChanged;                                                            /* B21 */
  byte rtcUpdate;                                                               /* B22 */
  byte buzzerCmd;                                                               /* B23 */
  byte spare1[16];                                                              /* B24-B39 */

  CurveStruct curve;

  AppParamStruct AppParameters;
} tMsgDataOut;

#ifdef COM
tMsgDataOut MsgDataOut;
#else
extern tMsgDataOut MsgDataOut;
#endif
Q_DECLARE_METATYPE(tMsgDataOut)

typedef struct
{
  float dosSet;
  float dosAct;
  float actRpm;
  float weight;
  float ShotWeight;
  float DosTime;
  unsigned char ProductionType;
  unsigned char InjInputMode;
  unsigned char ExtInputMode;
  unsigned char OperatingMode;                                                  // (cal/rpm)
  unsigned char calib;
  unsigned char spare0;

  /*----- Floatbuf[30] -----*/
  float preCalCalCapIst;                                                        /* F0 : Pre calibration cap ist */
  float rpmCorr;                                                                /* F1 : RPM correction */
  float productsToGo;                                                           /* F2 : Theoretical products to go */
  float consumption;
  /* F3 : Consumption (Not used) */
  float consumptionDay;                                                         /* F4 : Consumption  (Not used) */
  float timeToGo;                                                               /* F5 : Inj mode shot time to go */
  float grossWeightFast;                                                        /* F6 : Fast weight for test */

  float tachoAct;                                                               /* F7 : Actual tacjo voltage */
  float extCapAct;
  /* F8 : Actual extruder capacity */
  float metTimeAct;                                                             /* F9 : Dosing (Metering) time */

  /*----- Telnet screen -----*/
  float metTimeMeas;                                                            /* F10 : Meting time measured */
  float metTimeCalc;                                                            /* F11 : Metering time avg */
  float colCapIst;                                                              /* F12 : Color cap act (not video)*/
  float vidColDevPct;                                                           /* F13 : Capacity deviation ABS*/
  float colDevPctRel;                                                           /* F14 : Capacity deviation REL*/
  float corCycle;                                                               /* F15 : Correction Cycle*/
  float vidCorrection;                                                          /* F16 : Correction */
  float _rpmCorr;                                                               /* F17 : RPM Correction*/
  float corFacAdjust;                                                           /* F18 : FacAdj*/
  float minTimeCorFac;                                                          /* F19 : Min Time Corr*/

  /*----- Telnet screen word's !!! -----*/
  float sampleLen;                                                              /* F20 : Measurement length SET*/
  float timeLeft;                                                               /* F21 : Measurement length ACT*/
  float sampleLenQR;                                                            /* F22 : QR Measurement length SET */
  float timeLeftQR;                                                             /* F23 : QR Measurement length ACT*/
  float colDevPctQR;                                                            /* F24 */
  float spare4[5];                                                              /* F25-F29 */

  /*----- Bytebuf[30] -----*/
  byte balProdStatus;                                                           /* B0 : Balance production status  Display "Calibrating" / "Calibrated" */
  byte vidProdSts;                                                              /* B1 : Video production status */
  byte preCalBusy;                                                              /* B2 : Precalibration busy  */
  byte preCalReady;                                                             /* B3 : Precalibration ready  */
  byte preCalPauseBusy;                                                         /* B4 : Precalibration pause busy */
  byte preCalCalCapIstValid;                                                    /* B5 : PreCalRpmCorrValid*/
  byte prodTestBusy;                                                            /* B6 : Production test busy (Not used)*/
  byte alarmNr;                                                                 /* B7 : Alarm number */
  byte almActive;                                                               /* B8 : Alarm active */
  byte alarmMode;                                                               /* B9 : alarm mode alarm/warning */
  byte metTimeActValid;                                                         /* B10 : Metering time valid */
  byte _spare0;                                                                 /* B11 : Spare */

  /*-----  Telnet screen -----*/
  byte metRstHystCnt;                                                           /* B12 : Metering hysterese reset counter for debug */
  byte bandNr;                                                                  /* B13 : Band data - nr */
  byte inBand;                                                                  /* B14 : Band data - in*/
  byte outBand;                                                                 /* B15 : Band data - out*/
  byte bandCnt;                                                                 /* B16 : Band data - Max*/
  byte corSts;                                                                  /* B17 : Correction state */
  byte mcSts;                                                                   /* B18 : Ist measurement state */
  byte putIst;                                                                  /* B19 : Put weight counter */
  byte getIst;                                                                  /* B20 : Get weight counter */
  byte mcStsQR;                                                                 /* B21 : QR - Ist measurement state */
  byte putIstQR;                                                                /* B22 : QR - Put weight counter */
  byte getIstQR;                                                                /* B23 : QR - Get weight counter */
  byte devRetryQR;                                                              /* B24 : QR - Deviation retry counter */

  /*----- I/O board information -----*/
  byte versionSw;                                                               /* B25 : Software version */
  byte versionHw;                                                               /* B26 : Hardware version */
  byte temperature;                                                             /* B27 : Board temperature (Not used) */

  /*----- Various -----*/
  byte mc24On;                                                                  /* B28 : MC-TC on status */
  byte mcStartError;                                                            /* B29 : Start error */
  byte alarmResetAck;                                                           /* B30 : Alarm reset ack */
  byte standStill;                                                              /* B31 : Standstill */
  byte newShotDetect;                                                           /* B32 : New shot detect */
  byte primeBusy;                                                               /* B33 : Prime busy */
  byte input[4];                                                                /* B34-37 : Digital inputs */
  byte mcCmd;                                                                   /* B38 : McCmd */
  byte spare1;                                                                  /* B39 : Spare */

  CurveStruct curve;

  AppParamStruct AppParameters;
} tMsgDataIn;
#endif                                                                          // SHARED_H

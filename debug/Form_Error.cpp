/*------------------------------*/
/* File        : Form_Error.cpp */
/* Description : Alarm screen   */
/*------------------------------*/

#include "Form_Error.h"
#include "ui_Form_Error.h"
#include "Form_MainMenu.h"
#include "Rout.h"

ErrorFrm::ErrorFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::ErrorFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);

  unitIndex = 0;
  mType = msgtNone;

  closeTim = new QTimer(this);
  connect(closeTim,SIGNAL(timeout()),this,SLOT(OnCloseTim()));
  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(BtOkClicked()));
  connect(ui->btHelp,SIGNAL(clicked()),this,SLOT(BtHelpClicked()));
  connect(ui->btCancel,SIGNAL(clicked()),this,SLOT(BtCancelClicked()));
  connect(ui->btHelp2,SIGNAL(clicked()),this,SLOT(BtHelp2Clicked()));

  /*----- Disable windows title bar -----*/
  this->setWindowFlags(Qt::CustomizeWindowHint);

}


ErrorFrm::~ErrorFrm()
{
  delete ui;
}


void ErrorFrm::changeEvent(QEvent *e)
{
  /*----------------------*/
  /* Change event handler */
  /*----------------------*/
  QBaseWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      LanguageUpdate();
      break;
    default:
      break;
  }
}


void ErrorFrm::FormInit()
{
  disconnect(glob,SIGNAL(SysSts_AlarmStatusChanged(bool,int)),this,SLOT(AlarmStatusChanged(bool,int)));
  disconnect(glob,SIGNAL(SysSts_AlarmModeChanged(int,int)),this,SLOT(AlarmModeChanged(int,int)));
  disconnect(glob,SIGNAL(MstSts_SysActiveChanged(bool)),this,SLOT(SysActiveChanged(bool)));

  QBaseWidget::ConnectKeyBeep();;
  LanguageUpdate();
  connect(glob,SIGNAL(SysSts_AlarmStatusChanged(bool,int)),this,SLOT(AlarmStatusChanged(bool,int)));
  connect(glob,SIGNAL(SysSts_AlarmModeChanged(int,int)),this,SLOT(AlarmModeChanged(int,int)));
  connect(glob,SIGNAL(MstSts_SysActiveChanged(bool)),this,SLOT(SysActiveChanged(bool)));
}


void ErrorFrm::LanguageUpdate()
{
  glob->SetButtonImage(ui->btOk,itSelection,selOk);
  glob->SetButtonImage(ui->btCancel,itSelection,selCancel);
  glob->SetButtonImage(ui->btStop,itMenu,mnButtonOff);

  ui->lbCodeCap->setFont(*(glob->baseFont));
  ui->lbCode->setFont(*(glob->baseFont));
  ui->lbDateCap->setFont(*(glob->baseFont));
  ui->lbDate->setFont(*(glob->baseFont));
  ui->lbErrorText->setFont(*(glob->baseFont));
  ui->lbTimeCap->setFont(*(glob->baseFont));
  ui->lbTime->setFont(*(glob->baseFont));
  ui->lbUnitCap->setFont(*(glob->baseFont));
  ui->lbUnit->setFont(*(glob->baseFont));
}


void ErrorFrm::SetButtons(msgType mType)
{
  switch(mType) {
    case msgtWarning:
    case msgtAlarm:
    case msgtError:
      ui->lbDate->setVisible(true);
      ui->lbTime->setVisible(true);
      ui->lbDateCap->setVisible(true);
      ui->lbTimeCap->setVisible(true);
      ui->btCancel->setVisible(false);
      ui->btOk->setVisible(true);
      ui->btHelp->setVisible(false);
      ui->btHelp2->setVisible(false);
      glob->SetButtonImage(ui->btOk,itSelection,selOk);
      ui->btHelp->setText("?");
      break;

    case msgtConfirm:
      ui->lbDate->setVisible(false);
      ui->lbTime->setVisible(false);
      ui->lbDateCap->setVisible(false);
      ui->lbTimeCap->setVisible(false);
      ui->btCancel->setVisible(true);
      ui->btOk->setVisible(true);
      ui->btHelp->setVisible(false);
      ui->btHelp2->setVisible(false);
      glob->SetButtonImage(ui->btOk,itSelection,selOk);
      ui->btHelp->setText("?");
      break;

    case msgtAccept:
      ui->lbDate->setVisible(false);
      ui->lbTime->setVisible(false);
      ui->lbDateCap->setVisible(false);
      ui->lbTimeCap->setVisible(false);
      ui->btCancel->setVisible(false);
      ui->btOk->setVisible(true);
      ui->btHelp->setVisible(false);
      glob->SetButtonImage(ui->btOk,itSelection,selOk);
      ui->btHelp2->setVisible(false);
      break;

    case msgtSave:
      ui->lbDate->setVisible(false);
      ui->lbTime->setVisible(false);
      ui->lbDateCap->setVisible(false);
      ui->lbTimeCap->setVisible(false);
      ui->btCancel->setVisible(true);
      ui->btOk->setVisible(true);
      ui->btHelp->setVisible(true);
      glob->SetButtonImage(ui->btOk,itSelection,selOk);
      ui->btHelp2->setVisible(true);
      break;

    default:
      ui->btCancel->setVisible(false);
      ui->btOk->setVisible(true);
      ui->btHelp->setVisible(false);
      ui->lbDate->setVisible(false);
      ui->lbTime->setVisible(false);
      ui->lbDateCap->setVisible(false);
      ui->lbTimeCap->setVisible(false);
      ui->btHelp2->setVisible(false);
      glob->SetButtonImage(ui->btOk,itSelection,selOk);
      ui->btHelp->setText("?");
      break;
  }

  // Stop knop niet zichtbaar, verzoek van Derk S. op 13-08-2012.
  ui->btStop->setVisible(false);                                                //ui->btStop->setVisible((mType == msgtWarning));
}


void ErrorFrm::ShowMessage(msgType mType,int unitIdx, int code, const QString &text)
{
  lastSelect = -1;
  this->mType = mType;

  glob->mstSts.popupActive = true;
  if ((glob->menu) != 0) {
    ((MainMenu*)(glob->menu))->FoldMenu();
  }

  unitIndex = unitIdx;

  SetButtons(mType);

  ui->lbWarn->setVisible(true);
  ui->lbImage->setVisible(true);
  ui->lbCode->setVisible(true);
  ui->lbCodeCap->setVisible(true);
  ui->btOk->setVisible(true);

  QString styleSheet;
  styleSheet = "#groupBox {border: 5px solid; border-radius: 7px; background-color: rgb(255,255,255); ";

  switch(mType) {
    case msgtWarning:
      ui->lbWarn->setText(tr("WARNING"));
      glob->SetLabelImage(ui->lbImage,itAlarmWarning,awWarningBig);
      styleSheet += "border-color: rgb(255, 170, 0);";                          // orange
      break;
    case msgtAlarm:
      ui->lbWarn->setText(tr("ALARM"));
      glob->SetLabelImage(ui->lbImage,itAlarmWarning,awAlarmBig);
      styleSheet += "border-color: rgb(255, 0, 0);";                            // red
      break;
    case msgtError:
      ui->lbWarn->setText(tr("ERROR"));
      styleSheet += "border-color: rgb(255, 0, 0);";                            // red
      break;
    case msgtConfirm:
      ui->lbImage->setVisible(false);
      ui->lbWarn->setVisible(false);
      ui->lbCode->setVisible(false);
      ui->lbCodeCap->setVisible(false);
      ui->lbWarn->setText(tr("Confirm"));
      styleSheet += "border-color: rgb(0, 0, 125);";                            // dark blue
      break;
    case msgtAccept:
      ui->lbImage->setVisible(false);
      ui->lbWarn->setVisible(false);
      ui->lbCode->setVisible(false);
      ui->lbCodeCap->setVisible(false);
      ui->lbWarn->setText(tr("Accept"));
      styleSheet += "border-color: rgb(0, 0, 125);";                            // dark blue
      break;
    default:
      ui->lbImage->setVisible(false);
      ui->lbWarn->setVisible(false);
      ui->lbCode->setVisible(false);
      ui->lbCodeCap->setVisible(false);
      ui->lbWarn->setText("");
      styleSheet += "border-color: rgb(0, 0, 0);";                              // black
      break;
  }
  styleSheet += "}";
  setStyleSheet(styleSheet);
  /*----- Display unit ID -----*/
  if (unitIndex >= 0) {
    ui->lbUnit->setText(glob->sysCfg[unitIndex].MC_ID);
    ui->lbUnitCap->setVisible(true);
    ui->lbUnit->setVisible(true);
  }
  /*----- System message, don't display the ID -----*/
  else {
    ui->lbUnitCap->setVisible(false);
    ui->lbUnit->setVisible(false);
  }
  ui->lbDate->setText(glob->mstCfg.sysTime.date().toString());
  ui->lbTime->setText(glob->mstCfg.sysTime.time().toString());

  // Find errorCode and check if it is warning or error.
  ui->lbCode->setText(QString::number(code));
  ui->lbErrorText->setText(text);
  // Display the window (which is application modal)
  setWindowModality(Qt::ApplicationModal);
  show();
}


void ErrorFrm::ShowMessage(int unitIdx,const QString &text, int timeOut)
{
  unitIndex = unitIdx;
  lastSelect = -1;
  mType = msgtNone;

  SetButtons(msgtCount);

  ui->lbWarn->setVisible(false);
  ui->lbImage->setVisible(false);
  ui->lbCode->setVisible(false);
  ui->lbCodeCap->setVisible(false);
  ui->btOk->setVisible(false);

  if (unitIdx >= 0) {
    ui->lbUnit->setText(glob->sysCfg[unitIdx].MC_ID);
  }
  else {
    ui->lbUnit->setText(tr("System"));
  }
  ui->lbDate->setText(glob->mstCfg.sysTime.date().toString());
  ui->lbTime->setText(glob->mstCfg.sysTime.time().toString());

  if ((glob->menu) != 0) {
    ((MainMenu*)(glob->menu))->FoldMenu();
  }
  ui->lbErrorText->setText(text);

  QString styleSheet;
  styleSheet = "#groupBox {border: 5px solid; border-radius: 7px; background-color: rgb(255,255,255); border-color: rgb(0, 0, 0); }";
  setStyleSheet(styleSheet);

  closeTim->setInterval(timeOut);
  show();
}


void ErrorFrm::CancelAlarm()
{
  glob->AlarmResetRcv(unitIndex);
}


void ErrorFrm::CancelForm()
{
  unitIndex = -1;
  closeTim->stop();

  glob->mstSts.popupActive = false;
  close();
}


void ErrorFrm::OnCloseTim()
{
  CancelForm();
}


int ErrorFrm::LastSelection()
{
  return lastSelect;
}


void ErrorFrm::BtOkClicked()
{
  /*-------------------*/
  /* OK button clicked */
  /*-------------------*/
  if (isActiveWindow()) {
    // Button OK
    //@@ @????????????????????
    //        lastSelect = 1;
    emit ButtonClicked(1);
    //    if ((mType == msgtWarning) || (mType == msgtError)) {
    if ((mType == msgtWarning) || (mType == msgtAlarm)) {
      CancelAlarm();
    }
    CancelForm();
  }
}


void ErrorFrm::BtHelpClicked()
{
  /*---------------------*/
  /* HELP button clicked */
  /*---------------------*/
  if (isActiveWindow()) {
    // Button Help (YES ALL)
    lastSelect = 2;
    emit ButtonClicked(2);
    CancelForm();
  }
}


void ErrorFrm::BtCancelClicked()
{
  /*-----------------------*/
  /* CANCEL button clicked */
  /*-----------------------*/
  if (isActiveWindow()) {
    lastSelect = 3;
    emit ButtonClicked(3);
    CancelForm();
  }
}


void ErrorFrm::BtHelp2Clicked()
{
  /*----------------------*/
  /* HELP2 button clicked */
  /*----------------------*/
  if (isActiveWindow()) {
    // Button Help2 (NO ALL)
    lastSelect = 4;
    emit ButtonClicked(4);
    CancelForm();
  }
}


void ErrorFrm::AlarmStatusChanged(bool sts, int idx)
{
  if (idx == unitIndex) {
    if (sts == false) {
      // If alarm is no longer active, close the message
      CancelForm();
    }
  }
}


void ErrorFrm::AlarmModeChanged(int mode, int idx)
{
  if (idx == unitIndex) {
    if (glob->sysSts[idx].alarmSts.active == true) {
      QString styleSheet;
      styleSheet = "#groupBox {border: 5px solid; border-radius: 7px; background-color: rgb(255,255,255); ";

      switch(mode) {
        case 0:
          ui->lbWarn->setText(tr("WARNING"));
          styleSheet += "border-color: rgb(255, 170, 0);";                      // orange
          break;
        case 1:
          ui->lbWarn->setText(tr("ALARM"));
          styleSheet += "border-color: rgb(255, 0, 0);";                        // red
          break;
      }
      styleSheet += "}";
      setStyleSheet(styleSheet);
    }
  }
}


void ErrorFrm::on_btStop_pressed()
{
  glob->MstSts_SetSysStart(false);
}


void ErrorFrm::SysActiveChanged(bool act)
{
  if (act) {
    glob->SetButtonImage(ui->btStop,itMenu,mnButtonOff);
  }
  else {
    glob->SetButtonImage(ui->btStop,itMenu,mnButtonOn);
  }
}

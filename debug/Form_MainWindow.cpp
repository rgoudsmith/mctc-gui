#include "Form_MainWindow.h"
#include "ui_Form_MainWindow.h"
#include "ImageDef.h"
#include <QFile>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  /*----- Disable windows title bar -----*/
  this->setWindowFlags(Qt::CustomizeWindowHint);
}


MainWindow::~MainWindow()
{
  delete ui;
}

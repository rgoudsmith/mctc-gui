/*-----------------------------*/
/* Multi unit selection screen */
/*-----------------------------*/

#include "Form_MultiUnitSelect.h"
#include "ui_Form_MultiUnitSelect.h"
#include "Form_MainMenu.h"
#include "Rout.h"
#include "Com.h"


QObject *editObject;                            /* Active edit object */
float val1,val2,val3;

MultiUnitSelectFrm::MultiUnitSelectFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::MultiUnitSelectFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);

  unitCnt = 0;
  unitIdx = -1;

  /*----- Clear unut array -----*/
  for (int i=0;i<CFG_MAX_UNIT_COUNT;i++) {
    unitList[i] = 0;
  }
}


MultiUnitSelectFrm::~MultiUnitSelectFrm()
{
  /*------------*/
  /* Destructor */
  /*------------*/
  //  ClearItems();
  delete numInput;
  delete ui;
}


void MultiUnitSelectFrm::changeEvent(QEvent *e)
{
  /*--------------*/
  /* Change event */
  /*--------------*/
  QWidget::changeEvent(e);
  switch (e->type()) {
  case QEvent::LanguageChange:
    ui->retranslateUi(this);
    break;
  default:
    break;
  }
}


void MultiUnitSelectFrm::resizeEvent(QResizeEvent *e)
{
  /*--------------*/
  /* Resize event */
  /*--------------*/
  QWidget::resizeEvent(e);
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
}


void MultiUnitSelectFrm::SetInfoText(void) {
  /*---------------------------------------*/
  /* Update static text in the info fields */
  /*---------------------------------------*/

  /*----- Enable black text color when info fields are disabled -----*/
  ui->edInfo1->setStyleSheet(QString::fromUtf8("QLineEdit:disabled"
                                               "{ color: black }"));

  ui->edInfo2->setStyleSheet(QString::fromUtf8("QLineEdit:disabled"
                                               "{ color: black }"));

  ui->edInfo3->setStyleSheet(QString::fromUtf8("QLineEdit:disabled"
                                               "{ color: black }"));


  switch(glob->mstCfg.mode) {

  /*----- Injection moulding -----*/
  case modInject:

    ui->edInfo1->setVisible(true);
    ui->edInfo2->setVisible(true);
    ui->edInfo3->setVisible(true);

    switch(glob->mstCfg.inpType) {

    /*----- Relay -----*/
    case ttRelay:
      ui->lbedInfo1->setText("Shot weight");
      ui->lbedInfo2->setText("Relay time");
      ui->lbedInfo3->setText("Act time");
      ui->edInfo1->setEnabled(true);
      ui->edInfo2->setEnabled(false);
      ui->edInfo3->setEnabled(false);
      break;

      /*----- Timer -----*/
    case ttTimer:
      ui->lbedInfo1->setText("Shot weight");
      ui->lbedInfo2->setText("Set time");
      ui->lbedInfo3->setText("Act time");
      ui->edInfo1->setEnabled(true);
      ui->edInfo2->setEnabled(true);
      ui->edInfo3->setEnabled(false);
      break;
    }
    break;

    /*----- Extrusion -----*/
  case modExtrude:

    ui->edInfo1->setVisible(true);
    ui->edInfo3->setVisible(false);

    switch(glob->mstCfg.inpType) {

    /*----- Relay -----*/
    case ttRelay:
      ui->lbedInfo1->setText("Ext Cap set");
      ui->edInfo2->setVisible(false);
      break;

      /*----- Tacho -----*/
    case ttTacho:
      ui->lbedInfo1->setText("Ext Act");
      ui->lbedInfo2->setText("Tacho Act");
      ui->edInfo2->setVisible(true);
      break;
    }
    break;
  }
}


void MultiUnitSelectFrm::UpdateProcesData(void) {
  /*-------------------------------------*/
  /* Update dynamic data the info fields */
  /*-------------------------------------*/
  switch(glob->mstCfg.mode) {

  /*----- Injection moulding -----*/
  case modInject:

    switch(glob->mstCfg.inpType) {

    /*----- Injection, relay -----*/
    case ttRelay:
      ui->edInfo2->setText(QString::number(glob->sysSts[0].injection.meteringAct,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) + QString(" ")+ glob->timerUnits);
      ui->edInfo3->setText(QString::number(glob->sysSts[0].injection.shotTimeAct,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) + QString(" ")+ glob->timerUnits);
      break;

      /*----- Injection, timer -----*/
    case ttTimer:
      ui->edInfo3->setText(QString::number(glob->sysSts[0].injection.shotTimeAct,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision) + QString(" ")+ glob->timerUnits);
      break;
    }
    break;

    /*----- Extrusion -----*/
  case modExtrude:
    switch(glob->mstCfg.inpType) {

    /*----- Relay -----*/
    case ttRelay:
      break;

      /*----- Tacho -----*/
    case ttTacho:
      break;
    }
    break;
  }
}

void MultiUnitSelectFrm::showEvent(QShowEvent *e)
{
  /*------------*/
  /* Show event */
  /*------------*/
  QWidget::showEvent(e);

  /*----- Connect data refresh to msg received -----*/
  connect(glob,SIGNAL(ComReceived(tMsgControlUnit,int)),this,SLOT(ComMsgReceived(tMsgControlUnit,int)));

  switch (Rout::homeScreenMultiUnitActive) {

  /*----- Multi unit select screen -----*/
  case(MULTI_SCR_MODE_SELECT) :
    ui->lbText->setText(tr("Please select a unit"));
    ui->lbText->setVisible(true);
    ui->btRecipe->setVisible(false);
    ui->Settings->setVisible(false);
    break;


    /*----- Multi unit status screen -----*/
  case(MULTI_SCR_MODE_STATUS) :
    ui->lbText->setVisible(false);

    /*----- Display recipe button -----*/
//    ui->btRecipe->setVisible(glob->mstCfg.rcpEnable);
//    ui->Settings->setVisible(true);

#warning Disabled !!
        ui->Settings->setVisible(false);
        ui->btRecipe->setVisible(false);

    SetInfoText();
    break;

#ifdef UIT
    if (Rout::McWeightPresent()) {
      ui->lbMcWeightTotCap->setVisible(true);
      ui->lbMcWeightTotCap->setText(tr("Total capacity : ")+QString::number(glob->sysSts[0].mcWeightSts.extCapTotal,'f',2) +QString(" ") +glob->extrUnits);
    }
    else {
      ui->lbMcWeightTotCap->setVisible(false);
    }
#endif
    break;

    /*----- Multi unit config screen -----*/
  case(MULTI_SCR_MODE_CONFIG) :
    ui->lbText->setText(tr("Unit configuration"));
    ui->lbText->setVisible(true);
    ui->btRecipe->setVisible(false);
    ui->Settings->setVisible(false);
    break;
  }
}

void MultiUnitSelectFrm::hideEvent(QHideEvent *e)
/*--------------------*/
/* Hide event handler */
/*--------------------*/
{
  /*----- Dis-connect data refresh to msg received -----*/
  disconnect(glob,SIGNAL(ComReceived(tMsgControlUnit,int)),this,SLOT(ComMsgReceived(tMsgControlUnit,int)));

  QBaseWidget::hideEvent(e);
}




void MultiUnitSelectFrm::FormInit()
{
  /*-----------*/
  /* Form init */
  /*-----------*/

  QBaseWidget::ConnectKeyBeep();

  /*----- Create numeric entry -----*/
  numInput = new NumericInputFrm();

  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
  numInput->hide();
  numInput->FormInit();
  connect(numInput,SIGNAL(InputValue(float)),this,SLOT(ValueReceived(float)));

  InitItems();

  connect(glob,SIGNAL(MstCfg_SlaveCountChanged(int)),this,SLOT(SlaveCountChanged(int)));
  connect(glob,SIGNAL(SysSts_SysEnabledChanged(bool,int)),this,SLOT(SysActiveChanged(bool,int)));
  connect(glob,SIGNAL(SysSts_McWeightCapChanged(float,float,float,int)),this,SLOT(McWeightCapChanged(float,float,float,int)));

  /*----- Remove border around setting groupbox -----*/
  //  ui->Settings->setStyleSheet("border:0;");

  ui->edInfo1->installEventFilter(this);
  ui->edInfo2->installEventFilter(this);
  ui->edInfo3->installEventFilter(this);

  ui->edInfo1->setFont(*(glob->baseFont));
  ui->edInfo1->setStyleSheet("font-weight: bold;");

  ui->edInfo2->setFont(*(glob->baseFont));
  ui->edInfo2->setStyleSheet("font-weight: bold;");

  ui->edInfo3->setFont(*(glob->baseFont));
  ui->edInfo3->setStyleSheet("font-weight: bold;");
}


void MultiUnitSelectFrm::ComMsgReceived(tMsgControlUnit msg,int idx)
{
  /*--------------------------------------------*/
  /* Comm message received, update dynamic data */
  /*--------------------------------------------*/
  UpdateProcesData();
}


void MultiUnitSelectFrm::InitItems()
{
  MultiComponentItemFrm * item;

  /* Get current number of controllers */
  int gbiUnits = glob->mstCfg.slaveCount+1;

  if (unitCnt < gbiUnits) {
    /* Create new items from last known count upto new count */
    for (int i = unitCnt; i<gbiUnits; i++) {
      item = new MultiComponentItemFrm(this);
      unitList[i] = item;
      ui->units->addWidget(item);
      item->FormInit();
      item->SetUnitIndex(i);
      connect(item,SIGNAL(ColPctClicked(int)),this,SLOT(ColPctClickReceived(int)));
    }
    unitCnt = gbiUnits;
  }
  else {
    if (unitCnt > gbiUnits) {
      // delete all items between new count and last count
      for (int i=gbiUnits;i<unitCnt;i++) {
        item = unitList[i];
        unitList[i] = 0;
        ui->units->removeWidget(item);
        delete item;
      }
      unitCnt = gbiUnits;
    }
  }
}


bool MultiUnitSelectFrm::eventFilter(QObject *o, QEvent *e)
{
  /*--------------*/
  /* Event filter */
  /*--------------*/
  float val,minVal,maxVal;

  int actUnit = glob->ActUnitIndex();
  bool num = false;

  if (e->type() == QEvent::MouseButtonPress) {

    /*----- Edit info 1 -----*/
    if ((o == ui->edInfo1) && (ui->edInfo1->isEnabled())) {
      editObject = ui->edInfo1;
      val = val1;
      minVal = 0;
      maxVal = 100000;
      num = true;
    }

    /*----- Edit info 2 -----*/
    if ((o == ui->edInfo2) && (ui->edInfo2->isEnabled())) {
      editObject = ui->edInfo2;
      val = val2;
      minVal = 0;
      maxVal = 100000;
      num = true;
    }

    /*----- Edit info 3 -----*/
    if ((o == ui->edInfo3) && (ui->edInfo3->isEnabled())) {
      editObject = ui->edInfo3;
      val = val3;
      minVal = 0;
      maxVal = 100000;
      num = true;
    }

    if (num) {
      numInput->SetDisplay(true,1,val,minVal,maxVal);
      KeyBeep();
      numInput->show();
    }
  }
  return QBaseWidget::eventFilter(o,e);
}


void MultiUnitSelectFrm::ClearItems()
{
  MultiComponentItemFrm * item;
  for (int i=0;i<CFG_MAX_UNIT_COUNT;i++) {
    if (unitList[i] != 0) {
      item = unitList[i];
      disconnect(item,SIGNAL(ColPctClicked(int)),this,SLOT(ColPctClickReceived(int)));
      delete unitList[i];
      unitList[i] = 0;
    }
  }
}


void MultiUnitSelectFrm::SlaveCountChanged(int)
{
  InitItems();
}


void MultiUnitSelectFrm::SysActiveChanged(bool,int)
{
  InitItems();
}


void MultiUnitSelectFrm::McWeightCapChanged(float cap, float capTotal, float capPctTotal, int idx)
{
  /*----------------------------------*/
  /* Event : McWeight capacity change */
  /*----------------------------------*/
  //    ui->lbMcWeightTotCap->setText(tr("Total capacity : ")+QString::number(capTotal,'f',2) +QString(" ") +glob->extrUnits);
}


void MultiUnitSelectFrm::ColPctClickReceived(int idx)
{
  unitIdx = idx;
  if (unitIdx >= 0) {
    numInput->SetDisplay(true,glob->formats.PercentageFmt.precision,glob->sysCfg[unitIdx].colPct,0,100);
    numInput->show();
  }
}



void MultiUnitSelectFrm::ValueReceived(float val)
{
  /*--------------------------*/
  /* Numerical value received */
  /*--------------------------*/

  switch(glob->mstCfg.mode) {

  /*----- Injection moulding -----*/
  case modInject:

    switch(glob->mstCfg.inpType) {

    /*----- Injection, Relay -----*/
    case ttRelay:
      /*----- Shot weight -----*/
      if (editObject == ui->edInfo1) {
        val1 = val;
        ui->edInfo1->setText(QString::number(val) + " gr");
        glob->SysCfg_SetShotWeight(val,0);
      }
      break;

      /*----- Injection, timer -----*/
    case ttTimer:

      /*----- Shot weight -----*/
      if (editObject == ui->edInfo1) {
        val1 = val;
        ui->edInfo1->setText(QString::number(val) + " gr");
        glob->SysCfg_SetShotWeight(val,0);
      }

      /*----- Set time -----*/
      if (editObject == ui->edInfo2) {
        val2 = val;
        ui->edInfo2->setText(QString::number(val) + " s");
        glob->SysCfg_SetShotTime(val2,0);
      }
      break;
    }
    break;

    /*----- Extrusion -----*/
  case modExtrude:
    switch(glob->mstCfg.inpType) {

    /*----- Extrusion, relay -----*/
    case ttRelay:
      /*----- Extruder capacity set -----*/
      if (editObject == ui->edInfo1) {
        val1 = val;
//        ui->edInfo1->setText(QString::number(val) + " kg/h");
        ui->edInfo1->setText(QString::number(val,glob->formats.ShotWeightFmt.format.toAscii(),glob->formats.ShotWeightFmt.precision) + QString(" ")+glob->weightUnits);
      }
      break;

      /*----- Tacho -----*/
    case ttTacho:
      break;
    }
    break;
  }
}




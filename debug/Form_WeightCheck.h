#ifndef WEIGHTCHECKFRM_H
#define WEIGHTCHECKFRM_H

#include <QWidget>
#include "Form_QbaseWidget.h"

namespace Ui
{
  class WeightCheckFrm;
}


class WeightCheckFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit WeightCheckFrm(QWidget *parent = 0);
    ~WeightCheckFrm();
    void FormInit();

  protected:
    void changeEvent(QEvent *);

  private:
    Ui::WeightCheckFrm *ui;

  private slots:
    void OkClicked();

  public slots:
    void SysSts_FastMassChanged(float,int);
    void McIdChanged(const QString &, int);
    void ActUnitIdxChanged(int);
};
#endif                                                                          // WEIGHTCHECKFRM_H

#ifndef CALIBRATIONFRM_H
#define CALIBRATIONFRM_H

#include <QtGui/QWidget>
#include "Form_QbaseWidget.h"
#include "CalibrationPopup.h"

namespace Ui
{
  class CalibrationFrm;
}


class CalibrationFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    CalibrationFrm(QWidget *parent = 0);
    ~CalibrationFrm();
    void FormInit();
    void SetMCIdx(int);
    void SetCurrentProdSettings(bool doLoad, bool finished=false);

  protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent * e);
    void showEvent(QShowEvent *e);
    void hideEvent(QHideEvent *);

  private:
    Ui::CalibrationFrm *m_ui;
    int mcIdx;
    int subState;
    bool ready;
    CalibrationPopup *calibPopup;
    bool CheckInputValues();
    void SaveCurveAsMaterial();
    void LanguageUpdate();

  private slots:
    void CommandClicked();

  public slots:
    void PreCalStatusChanged(unsigned char,int);
    void PreCalDosActChanged(float,int);
    void SysCfg_DosSetChanged(float,int);
    void ShowCalibPopup(int);
    void PopupCommandReceived(int);

    signals:
    void SetDisplayState(eMvcDispState);
};
#endif                                                                          // CALIBRATIONFRM_H

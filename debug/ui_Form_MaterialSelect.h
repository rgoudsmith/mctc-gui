/********************************************************************************
** Form generated from reading UI file 'Form_MaterialSelect.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_MATERIALSELECT_H
#define UI_FORM_MATERIALSELECT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MaterialSelectFrm
{
public:
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_7;
    QLabel *lbCaption;
    QSpacerItem *horizontalSpacer_8;
    QHBoxLayout *horizontalLayout_6;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_4;
    QVBoxLayout *verticalLayout_7;
    QPushButton *btUp;
    QLabel *label;
    QPushButton *btDown;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout;
    QLabel *boxUp;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_5;
    QLabel *boxSelect;
    QPushButton *btOk;
    QLabel *lbIndex;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_2;
    QLabel *boxDown;
    QVBoxLayout *verticalLayout_3;
    QSpacerItem *verticalSpacer_4;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *btDefault;
    QSpacerItem *verticalSpacer_5;
    QSpacerItem *horizontalSpacer_6;
    QVBoxLayout *verticalLayout;
    QPushButton *btSearch;
    QSpacerItem *verticalSpacer;
    QPushButton *btNew;
    QPushButton *btDeleteAll;
    QPushButton *btDelete;
    QPushButton *btRename;
    QSpacerItem *verticalSpacer_2;
    QPushButton *btCancel;

    void setupUi(QWidget *MaterialSelectFrm)
    {
        if (MaterialSelectFrm->objectName().isEmpty())
            MaterialSelectFrm->setObjectName(QString::fromUtf8("MaterialSelectFrm"));
        MaterialSelectFrm->resize(869, 521);
        MaterialSelectFrm->setCursor(QCursor(Qt::BlankCursor));
        MaterialSelectFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout_5 = new QVBoxLayout(MaterialSelectFrm);
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_7);

        lbCaption = new QLabel(MaterialSelectFrm);
        lbCaption->setObjectName(QString::fromUtf8("lbCaption"));
        lbCaption->setMinimumSize(QSize(0, 45));
        lbCaption->setMaximumSize(QSize(16777215, 45));
        QFont font;
        font.setPointSize(35);
        font.setBold(true);
        font.setWeight(75);
        lbCaption->setFont(font);

        horizontalLayout_7->addWidget(lbCaption);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_8);


        verticalLayout_5->addLayout(horizontalLayout_7);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(0);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(0);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(0, 0, 0, 70);
        btUp = new QPushButton(MaterialSelectFrm);
        btUp->setObjectName(QString::fromUtf8("btUp"));
        btUp->setMinimumSize(QSize(90, 70));
        btUp->setMaximumSize(QSize(90, 70));
        btUp->setText(QString::fromUtf8("Up"));
        btUp->setAutoRepeat(true);
        btUp->setAutoRepeatDelay(300);
        btUp->setAutoRepeatInterval(200);

        verticalLayout_7->addWidget(btUp);

        label = new QLabel(MaterialSelectFrm);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFrameShape(QFrame::Box);

        verticalLayout_7->addWidget(label);

        btDown = new QPushButton(MaterialSelectFrm);
        btDown->setObjectName(QString::fromUtf8("btDown"));
        btDown->setMinimumSize(QSize(90, 70));
        btDown->setMaximumSize(QSize(90, 70));
        btDown->setText(QString::fromUtf8("Down"));
        btDown->setAutoRepeat(true);
        btDown->setAutoRepeatDelay(300);
        btDown->setAutoRepeatInterval(200);

        verticalLayout_7->addWidget(btDown);


        horizontalLayout_4->addLayout(verticalLayout_7);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        boxUp = new QLabel(MaterialSelectFrm);
        boxUp->setObjectName(QString::fromUtf8("boxUp"));
        boxUp->setMinimumSize(QSize(200, 150));
        boxUp->setMaximumSize(QSize(200, 150));
        QFont font1;
        font1.setPointSize(20);
        boxUp->setFont(font1);
        boxUp->setFrameShape(QFrame::Box);
        boxUp->setFrameShadow(QFrame::Plain);
        boxUp->setText(QString::fromUtf8("boxUp"));
        boxUp->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        horizontalLayout->addWidget(boxUp);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_3);


        horizontalLayout->addLayout(verticalLayout_2);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_5);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout_4->addLayout(horizontalLayout);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        boxSelect = new QLabel(MaterialSelectFrm);
        boxSelect->setObjectName(QString::fromUtf8("boxSelect"));
        boxSelect->setMinimumSize(QSize(200, 70));
        boxSelect->setMaximumSize(QSize(200, 70));
        boxSelect->setFrameShape(QFrame::Box);
        boxSelect->setLineWidth(4);
        boxSelect->setText(QString::fromUtf8("boxSelect"));
        boxSelect->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_5->addWidget(boxSelect);

        btOk = new QPushButton(MaterialSelectFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("Ok"));

        horizontalLayout_5->addWidget(btOk);

        lbIndex = new QLabel(MaterialSelectFrm);
        lbIndex->setObjectName(QString::fromUtf8("lbIndex"));
        lbIndex->setMinimumSize(QSize(90, 0));
        lbIndex->setMaximumSize(QSize(90, 16777215));
        lbIndex->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(lbIndex);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);


        verticalLayout_4->addLayout(horizontalLayout_5);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        boxDown = new QLabel(MaterialSelectFrm);
        boxDown->setObjectName(QString::fromUtf8("boxDown"));
        boxDown->setMinimumSize(QSize(200, 150));
        boxDown->setMaximumSize(QSize(200, 150));
        boxDown->setFont(font1);
        boxDown->setFrameShape(QFrame::Box);
        boxDown->setText(QString::fromUtf8("boxDown"));
        boxDown->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        boxDown->setIndent(0);
        boxDown->setTextInteractionFlags(Qt::NoTextInteraction);

        horizontalLayout_2->addWidget(boxDown);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_4);


        horizontalLayout_2->addLayout(verticalLayout_3);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_4->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        btDefault = new QPushButton(MaterialSelectFrm);
        btDefault->setObjectName(QString::fromUtf8("btDefault"));
        btDefault->setMinimumSize(QSize(200, 70));
        btDefault->setMaximumSize(QSize(200, 70));
        QFont font2;
        font2.setPointSize(18);
        btDefault->setFont(font2);
        btDefault->setText(QString::fromUtf8("Default Material"));

        horizontalLayout_3->addWidget(btDefault);

        verticalSpacer_5 = new QSpacerItem(20, 70, QSizePolicy::Minimum, QSizePolicy::Fixed);

        horizontalLayout_3->addItem(verticalSpacer_5);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);


        verticalLayout_4->addLayout(horizontalLayout_3);


        horizontalLayout_4->addLayout(verticalLayout_4);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        btSearch = new QPushButton(MaterialSelectFrm);
        btSearch->setObjectName(QString::fromUtf8("btSearch"));
        btSearch->setMinimumSize(QSize(90, 70));
        btSearch->setMaximumSize(QSize(90, 70));
        btSearch->setText(QString::fromUtf8("Search"));

        verticalLayout->addWidget(btSearch);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        btNew = new QPushButton(MaterialSelectFrm);
        btNew->setObjectName(QString::fromUtf8("btNew"));
        btNew->setMinimumSize(QSize(90, 70));
        btNew->setMaximumSize(QSize(90, 70));
        btNew->setText(QString::fromUtf8("New"));

        verticalLayout->addWidget(btNew);

        btDeleteAll = new QPushButton(MaterialSelectFrm);
        btDeleteAll->setObjectName(QString::fromUtf8("btDeleteAll"));
        btDeleteAll->setMinimumSize(QSize(90, 70));

        verticalLayout->addWidget(btDeleteAll);

        btDelete = new QPushButton(MaterialSelectFrm);
        btDelete->setObjectName(QString::fromUtf8("btDelete"));
        btDelete->setMinimumSize(QSize(90, 70));
        btDelete->setMaximumSize(QSize(90, 70));
        btDelete->setText(QString::fromUtf8("Delete"));

        verticalLayout->addWidget(btDelete);

        btRename = new QPushButton(MaterialSelectFrm);
        btRename->setObjectName(QString::fromUtf8("btRename"));
        btRename->setEnabled(true);
        btRename->setMinimumSize(QSize(90, 70));
        btRename->setMaximumSize(QSize(90, 70));
        btRename->setText(QString::fromUtf8("Rename"));

        verticalLayout->addWidget(btRename);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        btCancel = new QPushButton(MaterialSelectFrm);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("Cancel"));

        verticalLayout->addWidget(btCancel);


        horizontalLayout_4->addLayout(verticalLayout);


        horizontalLayout_6->addLayout(horizontalLayout_4);


        verticalLayout_5->addLayout(horizontalLayout_6);


        retranslateUi(MaterialSelectFrm);

        QMetaObject::connectSlotsByName(MaterialSelectFrm);
    } // setupUi

    void retranslateUi(QWidget *MaterialSelectFrm)
    {
        lbCaption->setText(QApplication::translate("MaterialSelectFrm", "Materials", 0, QApplication::UnicodeUTF8));
        label->setText(QString());
        lbIndex->setText(QApplication::translate("MaterialSelectFrm", "999/999", 0, QApplication::UnicodeUTF8));
        btDeleteAll->setText(QApplication::translate("MaterialSelectFrm", "DeleteAll", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(MaterialSelectFrm);
    } // retranslateUi

};

namespace Ui {
    class MaterialSelectFrm: public Ui_MaterialSelectFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_MATERIALSELECT_H

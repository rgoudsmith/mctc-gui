/********************************************************************************
** Form generated from reading UI file 'Form_Production.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_PRODUCTION_H
#define UI_FORM_PRODUCTION_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "Form_MvcSystem.h"

QT_BEGIN_NAMESPACE

class Ui_ProductionFrm
{
public:
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    MvcSystem *mvcSystem;

    void setupUi(QWidget *ProductionFrm)
    {
        if (ProductionFrm->objectName().isEmpty())
            ProductionFrm->setObjectName(QString::fromUtf8("ProductionFrm"));
        ProductionFrm->resize(557, 383);
        ProductionFrm->setCursor(QCursor(Qt::BlankCursor));
        ProductionFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout = new QVBoxLayout(ProductionFrm);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer = new QSpacerItem(20, 70, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        mvcSystem = new MvcSystem(ProductionFrm);
        mvcSystem->setObjectName(QString::fromUtf8("mvcSystem"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(mvcSystem->sizePolicy().hasHeightForWidth());
        mvcSystem->setSizePolicy(sizePolicy);
        mvcSystem->setCursor(QCursor(Qt::BlankCursor));

        horizontalLayout->addWidget(mvcSystem);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(ProductionFrm);

        QMetaObject::connectSlotsByName(ProductionFrm);
    } // setupUi

    void retranslateUi(QWidget *ProductionFrm)
    {
        Q_UNUSED(ProductionFrm);
    } // retranslateUi

};

namespace Ui {
    class ProductionFrm: public Ui_ProductionFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_PRODUCTION_H

/*===========================================================*/
/* File        : Form_Recipe.cpp                             */
/* Desctiption : Recipe Main screen                          */
/*               Each line is displayed with Form_RecipeItem */
/*===========================================================*/
#include "Form_Recipe.h"
#include "ui_Form_Recipe.h"
#include "Form_MainMenu.h"
#include "QmVcScrollbar.h"
#include "Form_RecipeItem.h"
#include "Rout.h"

/*----- Global variables -----*/
RecipeItemFrm *rcpItem[CFG_MAX_UNIT_COUNT];                                     /* Recipe item forms */

RecipeFrm::RecipeFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::RecipeFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  lineIdx = -1;
  numIdx = -1;

  keyInp = 0;
  numInp = 0;
  rcpCheckPopup = 0;

  ui->setupUi(this);
  ui->edRcpName->installEventFilter(this);
  ui->edRcpText->installEventFilter(this);

  ui->edVar1->installEventFilter(this);
  ui->edVar2->installEventFilter(this);

  connect(ui->btRcpSelect,SIGNAL(clicked()),this,SLOT(RcpSelClicked()));
  connect(ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));
  connect(ui->btSave,SIGNAL(clicked()),this,SLOT(SaveClicked()));

  keyInp = new KeyboardFrm();
  keyInp->move(((width()/2)-(keyInp->width()/2)),((height()/2)-(keyInp->height()/2)));
  keyInp->hide();
  connect(keyInp,SIGNAL(inputText(QString)),this,SLOT(TextReceived(QString)));

  numInp = new NumericInputFrm();
  numInp->move(((width()/2)-(numInp->width()/2)),((height()/2)-(numInp->height()/2)));
  numInp->hide();
  connect(numInp,SIGNAL(InputValue(float)),this,SLOT(ValueReceived(float)));

  rcpCheckPopup = new RecipeCheckPopupFrm();
  rcpCheckPopup->move(((width()/2)-(rcpCheckPopup->width()/2)),((height()/2)-(rcpCheckPopup->height()/2)));
  rcpCheckPopup->hide();

  /*----- Add MVC scrollbar -----*/
  QMvcScrollBar * bar = new QMvcScrollBar(ui->scrollArea);
  connect(bar,SIGNAL(MouseButtonPressed()),this,SLOT(KeyBeep()));
  ui->scrollArea->setVerticalScrollBar(bar);

  layout = new QVBoxLayout(ui->scrollAreaWidgetContents);
}


RecipeFrm::~RecipeFrm()
{
  /*------------*/
  /* Destructor */
  /*------------*/
  delete numInp;
  delete keyInp;
  delete rcpCheckPopup;
  delete ui;
}


void RecipeFrm::BuildRcpItems(void)
{
  /*------------------------*/
  /* Build the recipe items */
  /*------------------------*/
  int i;

  for (i=0; i<glob->mstCfg.slaveCount+1; i++) {
    rcpItem[i] = new RecipeItemFrm(this);
    layout->addWidget(rcpItem[i]);
    rcpItem[i]->SetUnitIndex(i);
    rcpItem[i]->FormInit();
    rcpItem[i]->SetNumInput(this->numInp);
    rcpItem[i]->SetRecipe(&recipeBuf,i);
    connect(rcpItem[i],SIGNAL(MaterialClicked(int)),this,SLOT(RecipeItemMaterialClicked(int)));
  }
}


void RecipeFrm::showEvent(QShowEvent *e)
{
  /*------------*/
  /* Show event */
  /*------------*/

  QBaseWidget::showEvent(e);
}


void RecipeFrm::resizeEvent(QResizeEvent *e)
{
  /*--------------*/
  /* Resize event */
  /*--------------*/

  QWidget::resizeEvent(e);
  if (keyInp != 0) {
    keyInp->move(((width()/2)-(keyInp->width()/2)),((height()/2)-(keyInp->height()/2)));
  }
  if (numInp != 0) {
    numInp->move(((width()/2)-(numInp->width()/2)),((height()/2)-(numInp->height()/2)));
  }
  if (rcpCheckPopup != 0) {
    rcpCheckPopup->move(((width()/2)-(rcpCheckPopup->width()/2)),((height()/2)-(rcpCheckPopup->height()/2)));
  }
}


bool RecipeFrm::eventFilter(QObject *obj, QEvent *e)
{

  switch (e->type()) {
  case QEvent::MouseButtonPress:
    if ((glob->GetUser() >= utTooling) && (glob->mstSts.sysActive == false)) {
      if (obj == ui->edRcpText) {
        lineIdx = 0;
        keyInp->SetText(recipeBuf.sText);
        keyInp->SetMaxLength(MAX_LENGTH_RECIPE_DESCRIPTION);
        KeyBeep();
        keyInp->show();
      }

      if (obj == ui->edRcpName) {
        lineIdx = 1;

        QString name;
        name = recipeBuf.sName;
        ClipRecipeExtension(&name);
        keyInp->SetText(name);

        keyInp->SetMaxLength(MAX_LENGTH_FILENAME_INPUT);
        KeyBeep();
        keyInp->show();
      }

      if (obj == ui->edVar1) {
        /*----- Extra check for production mode -----*/
        numIdx = 0;
        if (glob->mstCfg.mode == modInject) {
          numInp->SetDisplay(true,glob->formats.ShotWeightFmt.precision,recipeBuf.var1,0,INPUT_MAX_SHOTWEIGHT);
        }
        else {
          numInp->SetDisplay(true,glob->formats.ExtruderFmt.precision,recipeBuf.var1,0,INPUT_MAX_EXTCAP);
        }
        KeyBeep();
        numInp->show();
      }

      if (obj == ui->edVar2) {
        numIdx = 1;
        if (glob->mstCfg.mode == modInject) {
          numInp->SetDisplay(true,glob->formats.TimeFmt.precision,recipeBuf.var2,0,INPUT_MAX_SHOTTIME);
        }
        else {
          numInp->SetDisplay(true,glob->formats.TachoFmt.precision,recipeBuf.var2,0,INPUT_MAX_TACHO);
        }
        KeyBeep();
        numInp->show();
      }
    }
    break;

  default:
    break;
  }

  return QWidget::eventFilter(obj,e);
}


void RecipeFrm::changeEvent(QEvent *e)
{
  if (e->type() == QEvent::LanguageChange) {
    ui->retranslateUi(this);
  }
}


void RecipeFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  keyInp->FormInit();
  numInp->FormInit();
  rcpCheckPopup->FormInit();

  glob->SetButtonImage(ui->btCancel,itSelection,selCancel);
  glob->SetButtonImage(ui->btRcpSelect,itSelection,selOk);
  glob->SetButtonImage(ui->btRemove,itFileHandle,fhDelete);
  glob->SetButtonImage(ui->btSave,itFileHandle,fhSave);

  ui->lbCaption->setFont(*(glob->captionFont));

  ui->edRcpName->setFont(*(glob->baseFont));
  ui->edRcpText->setFont(*(glob->baseFont));
  ui->edVar1->setFont(*(glob->baseFont));
  ui->edVar2->setFont(*(glob->baseFont));

  connect(rcpCheckPopup,SIGNAL(RecipeOK()),this,SLOT(RecipeLoaded()));
  connect(glob,SIGNAL(MstCfg_UserChanged(eUserType)),this,SLOT(CfgCurrentUserChanged(eUserType)));
  connect(glob,SIGNAL(MstSts_SysActiveChanged(bool)),this,SLOT(SysActiveChanged(bool)));

  BuildRcpItems();

  SysActiveChanged(glob->mstSts.sysActive);
  SetRecipe(glob->selectedRecipe);
}


void RecipeFrm::ClearRecipeBuf(RecipeBufStruct *buf)
{
  buf->devCount = (glob->mstCfg.slaveCount+1);
  buf->input = (unsigned char)(glob->mstCfg.inpType);
  buf->mode = (unsigned char)(glob->mstCfg.mode);
  memset(buf->name,0x00,(MAX_LENGTH_FILENAME+1));
  memset(buf->text,0x00,(MAX_LENGTH_RECIPE_DESCRIPTION+1));
  buf->sName = "";
  buf->sText = "";
  buf->var1 = 0;
  buf->var2 = 0;
  for (int i=0;i<CFG_MAX_UNIT_COUNT;i++) {
    buf->lines[i].colPct = 0;
    buf->lines[i].corFac = 0;
    buf->lines[i].matIsDefault = true;
    memset((buf->lines[i].matName),0x00,(MAX_LENGTH_FILENAME+1));
    buf->lines[i].sMatName = "";
  }
}


void RecipeFrm::ClipRecipeExtension(QString *str)
{
  if (str->length() > QString(RECIPE_EXTENSION).length()) {
    QString ext = RECIPE_EXTENSION;
    *str = str->left(str->length()-ext.length());
  }
}


void RecipeFrm::CopyRecipeToBuf(RecipeStruct* buf)
{
  /*----- Fill entire structure with zero values to make sure there is no carbadge stored and checksum will be ok. -----*/
  memset(buf,0x00,sizeof(RecipeStruct));
  /*----- Software version of the recipe struct -----*/
  buf->swVersion = 0x00;
  /*----- Device count -----*/
  buf->devCount = recipeBuf.devCount;
  /*----- Actual recipe name and description text -----*/
  memcpy(buf->name,recipeBuf.name,MAX_LENGTH_FILENAME);
  memcpy(buf->text,recipeBuf.text,MAX_LENGTH_FILENAME);

  /*----- Production mode and inputType -----*/
  buf->mode = recipeBuf.mode;
  buf->input = recipeBuf.input;

  /*----- 2 Variables corresponding to production type -----*/
  buf->var1 = recipeBuf.var1;                                                   /* ShotTime or ExtCap: depending on prodMode */
  buf->var2 = recipeBuf.var2;                                                   /* ShotWeight or maxTacho: depending on prodMode */

  /*
     Extra checks to see if the selected material is the same as currently used for production.
     If not, set corFac to 0, so when the recipe is loaded the current corFac from the material will be used.
     If so, use the current actual corFac from production as the corFac for the recipe.
  */

  for (int i=0;i<buf->devCount;i++) {
    buf->lines[i].colPct = recipeBuf.lines[i].colPct;

    /*----- copy material name -----*/
    memcpy(buf->lines[i].matName,recipeBuf.lines[i].matName,MAX_LENGTH_FILENAME);
    /*----- copy isDefault indicator -----*/
    buf->lines[i].matIsDefault = recipeBuf.lines[i].matIsDefault;

    /*----- Check if material in recipeBuf is equal to the currently selected material. -----*/
    /*----- If so, copy the current corFac to the recipe. -----*/
    if ((QString::fromAscii(buf->lines[i].matName) == glob->sysCfg[i].material.name) &&
        (buf->lines[i].matIsDefault == glob->sysCfg[i].material.isDefault)) {
      buf->lines[i].corFac = glob->sysSts[i].procDiagData.rpmCorr;
    }
    else {
      /*
       When loading a recipe, corFac == 0 means to get the current corFac from the material file
       when the recipe is loaded into the system
      */
      buf->lines[i].corFac = 0;
    }
  }
  /*----- Calculate checksum -----*/
  int chkSum = (((MainMenu*)(glob->menu))->fileSysCtrl.CalculateChecksum(((char*)(buf)),(sizeof(RecipeStruct)-sizeof(int))));
  buf->chksum = chkSum;
}


void RecipeFrm::CopyRecipeFromBuf(RecipeStruct* buf)
{
  /*----- Store recipe name -----*/
  recipeBuf.sName = QString::fromAscii(buf->name);
  memcpy(recipeBuf.name,buf->name,MAX_LENGTH_FILENAME);
  /*----- Store recipe description -----*/
  memcpy(recipeBuf.text,buf->text,MAX_LENGTH_RECIPE_DESCRIPTION);

  recipeBuf.mode = buf->mode;
  recipeBuf.input = buf->input;
  recipeBuf.var1 = buf->var1;
  recipeBuf.var2 = buf->var2;

  recipeBuf.devCount = buf->devCount;

  for (int i=0;i<buf->devCount;i++) {
    recipeBuf.lines[i].colPct = buf->lines[i].colPct;
    /*----- Load material into recipe struct -----*/
    memcpy(recipeBuf.lines[i].matName,buf->lines[i].matName,MAX_LENGTH_FILENAME);
    recipeBuf.lines[i].sMatName = QString::fromAscii(buf->lines[i].matName);

    recipeBuf.lines[i].matIsDefault = buf->lines[i].matIsDefault;

    //@@ extra functionality to check for material corFac if recipe specific corFac == 0.
    recipeBuf.lines[i].corFac = buf->lines[i].corFac;
  }
}


void RecipeFrm::SaveRecipe(RecipeStruct* rcp,bool overwrite, bool saveActSettingsMode)
{
  int i;

  recipeBuf.mode = (unsigned char)(glob->mstCfg.mode);
  recipeBuf.input = (unsigned char)(glob->mstCfg.inpType);

  recipeBuf.devCount = (glob->mstCfg.slaveCount + 1);

  /*----- Detect save from actual settings mode -----*/
  if (saveActSettingsMode) {

    /*----- Save the actual injection or extrusion setings -----*/
    if (glob->mstCfg.mode == modInject) {
      recipeBuf.var1 = glob->sysCfg[0].injection.shotWeight;
      recipeBuf.var2 = glob->sysCfg[0].injection.shotTime;
    }
    else {
      recipeBuf.var1 = glob->sysCfg[0].extrusion.extCapacity;
      recipeBuf.var2 = glob->sysCfg[0].extrusion.tachoMax;
    }

    /*----- Save the actual color percentages -----*/
    for (i=0; i<recipeBuf.devCount; i++) {
      recipeBuf.lines[i].colPct = glob->sysCfg[i].colPct;
    }
  }

  QString fullPath = recipeBuf.sName;
  GetFullRecipePath(&fullPath);
  /*----- Check if the filename is already in use before saving. -----*/

  bool save = true;

  if (!(overwrite)) {
    if (QFile::exists(fullPath)) {
      QString name;
      name = recipeBuf.sName;
      ClipRecipeExtension(&name);
      /*----- If so, ask the user if it is ok to overwrite the existing file. -----*/
      connect(((MainMenu*)(glob->menu)),SIGNAL(ErrorResult(int)),this,SLOT(MessageResult(int)));
      ((MainMenu*)(glob->menu))->DisplayMessage(-1,0,tr("Recipe '")+name+tr("' already exists")+"\n"+tr("Overwrite existing recipe?"),msgtConfirm);
      save = false;
    }
  }

  if (save) {
    /*----- If save is allowed, store the recipe into the file-system -----*/
    /*----- copy current recipe into a buffer -----*/
    CopyRecipeToBuf(rcp);
    /*----- (See material functionality) -----*/
    QFile rcpStoreFile(fullPath);
    if (rcpStoreFile.open(QIODevice::ReadWrite)) {
      rcpStoreFile.reset();
      rcpStoreFile.write((char*)rcp,sizeof(RecipeStruct));
      rcpStoreFile.close();
      Rout::SyncFile();

      ((MainMenu*)(glob->menu))->DisplayMessage(-1,0,tr("Recipe saved."),msgtAccept);

      emit RecipeSaved();


      /*----- Show new recipe name in prod screen because it is the actual recipe ----*/
      QString name = QString::fromAscii(rcp->name);
      ClipRecipeExtension(&name);
      ((MainMenu*)(glob->menu))->prodForm->lbRecipeName->setText(name);
    }
  }
}


int RecipeFrm::LoadRecipe(const QString &fileName, RecipeStruct* rcp)
{
  /*-----------------------------------------------------------------*/
  /* Load recipe into local recipe struct (local variable = rcpFile) */
  /*-----------------------------------------------------------------*/
  int i;
  int retVal;

  retVal = -1;                                                                  /* Default returnValu = "File not found" */

  memset(rcp,0x00,sizeof(RecipeStruct));

  QString fullPath = fileName;
  GetFullRecipePath(&fullPath);

  /*----- Open recipe file read only (to prevent mess-ups) -----*/
  if (QFile::exists(fullPath)) {
    QFile fileRcp(fullPath);
    if (fileRcp.open(QIODevice::ReadOnly)) {
      fileRcp.reset();
      fileRcp.read(((char*)rcp),sizeof(RecipeStruct));
      fileRcp.close();

      /*----- Check checksum -----*/
      bool chk = ((MainMenu*)(glob->menu))->fileSysCtrl.CheckChecksum((char*)rcp,(sizeof(RecipeStruct)-sizeof(int)),(rcp->chksum));

      /*----- Checksum ok -----*/
      if (chk) {
        /*----- Use rcp struct to set all corresponding SysCfg settings
           Check every line if material matches with current configuration
           Copy recipe from temp rcp buffer (rcp) to local struct recipeBuf -----*/
        CopyRecipeFromBuf(rcp);
        /*----- if all checks are ok:
          1) Load correct recipe name
          2) Cycle through all units and set correct material name -----*/

        //          for (int i=0;i<recipeBuf.devCount;i++) {
        /*-----@@@ to do //@@ nu slechts 1 item, moet nog meer worden met dynamisch aanmaken van items etc. a.d.h.v. devCount. -----*/
        for (i=0; i<glob->mstCfg.slaveCount+1; i++) {
          rcpItem[i]->SetRecipe(&recipeBuf,i);
        }

        /*----- 3) Write the rpm correction factor from recipe into material if the recipe line contains a value != 0 -----*/
        retVal = 0;
      }
      /*---- Checksum error ---*/
      else {
        retVal = -3;
      }
    }
    /*---- Unable to open recipe ----*/
    else {
      retVal = -2;
    }
  }
  /*---- File not found ----*/
  else {
    retVal = -1;
  }
  return(retVal);
}


void RecipeFrm::CheckRecipe(RecipeStruct *rcp)
{
  /*----- Run recipe check via extra form -----*/
  rcpCheckPopup->CheckRecipe(rcp);
}


void RecipeFrm::CheckRecipe()
{
  RecipeStruct rcp;
  CopyRecipeToBuf(&rcp);
  rcpCheckPopup->CheckRecipe(&rcp);
}


void RecipeFrm::DisplayRecipe()
{
  /*----- Inj: Var1 = shotWeight, Var2 = shotTime -----*/
  /*----- Ext: Var1 = Ext. Cap., Var2 = max tacho -----*/
  eMode mod = (eMode)recipeBuf.mode;
  eType typ = (eType)recipeBuf.input;
  switch(mod) {
  case modInject:
    ui->edVar1->setText(QString::number(recipeBuf.var1,glob->formats.ShotWeightFmt.format.toAscii(),glob->formats.ShotWeightFmt.precision)+QString(" ")+glob->weightUnits);
    ui->edVar1->setVisible(true);
    switch(typ) {
    case ttTimer:
      ui->edVar2->setText(QString::number(recipeBuf.var2,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision)+QString(" ")+glob->timerUnits);
      ui->edVar2->setVisible(true);
      break;

    case ttRelay:
      ui->edVar2->setVisible(false);
      break;

    default:
      break;

    }
    break;

  case modExtrude:
    ui->edVar1->setText(QString::number(recipeBuf.var1,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision)+QString(" ")+glob->extrUnits);
    ui->edVar1->setVisible(true);
    switch(typ) {
    case ttRelay:
      ui->edVar2->setVisible(false);
      break;

    case ttTacho:
      ui->edVar2->setText(QString::number(recipeBuf.var2,glob->formats.TachoFmt.format.toAscii(),glob->formats.TachoFmt.precision)+QString(" ")+glob->tachoUnits);
      ui->edVar2->setVisible(true);
      break;

    default:
      break;

    }
    break;

  default:
    break;

  }

  /*----- Display current recipe name -----*/
  QString tmpRcp;

  /*----- Display recipe name -----*/
  tmpRcp = recipeBuf.sName;
  if ((tmpRcp.isEmpty()) || (tmpRcp.isNull())) {
    tmpRcp = MATERIAL_EMPTY_NAME;
  }
  else {
    ClipRecipeExtension(&tmpRcp);
  }
  ui->edRcpName->setText(tmpRcp);
}


void RecipeFrm::SetCorrectButtons()
{
  bool act = glob->mstSts.sysActive;
  eUserType ut = glob->GetUser();

  if (ut < utTooling) {
    ui->lbRcpSelect->setVisible(false);
    ui->btRcpSelect->setVisible(false);
    ui->btRemove->setVisible(false);
    ui->btSave->setVisible(false);
  }
  else {
    ui->btRemove->setVisible((act == false ? true : false));
    ui->btSave->setVisible((act == false ? true : false));
    ui->btRcpSelect->setVisible((act == false ? true : false));
    ui->lbRcpSelect->setVisible((act == false ? true : false));
  }
}


void RecipeFrm::RcpSelClicked()
{
  /*--------------------------*/
  /* BUTTON : Select clicked  */
  /* Load the selected recipe */
  /*--------------------------*/
  if (!((recipeBuf.sName.isNull()) || (recipeBuf.sName.isEmpty()))) {
    QString fullPath = recipeBuf.sName;
    GetFullRecipePath(&fullPath);

    /*----- Open recipe file read only (to prevent mess-ups) -----*/
    if (QFile::exists(fullPath)) {
      CheckRecipe();
    }
    else {
      /*----- Show some message that the recipe does not exist in the filesystem yet. -----*/
      //@@ Recipe does not exists in the filesystem yet. Save it first.
    }
  }
}


void RecipeFrm::CancelClicked()
{
  /*-------------------------*/
  /* BUTTON : Cancel clicked */
  /*-------------------------*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiRcpSelect);
}


void RecipeFrm::SaveClicked()
{
  /*------------------------------*/
  /* BUTTON : Save recipe clicked */
  /*------------------------------*/
  int errorCode,errorNr,i;
  RecipeStruct rcp;

  /* Check RecipeName input and Material Names inputs.
     RecipeName must be unequal to MATERIAL_EMPTY_NAME */

  errorCode = 0;

  /*----- Check recipe name -----*/
  if (((recipeBuf.sName.isEmpty()) || (recipeBuf.sName.isNull()))) {
    errorCode = 1;
  }
  /*----- Check recipe lines for all units ----*/
  else {
    for (i=0; i<=glob->mstCfg.slaveCount; i++) {
      if (((recipeBuf.lines[i].sMatName.isEmpty()) || (recipeBuf.lines[i].sMatName.isNull()))) {
        errorCode = 2;
        errorNr = i;
      }
    }
  }

  /*----- Handle rcp error -----*/
  switch (errorCode) {

  /*----- No error : Save recipe to file -----*/
  case(0) :
    SaveRecipe(&rcp,false,false);
    break;

    /*----- Recipe name error -----*/
  case(1) :
    ((MainMenu*)(glob->menu))->DisplayMessage(-1,0,tr("Recipe not saved.\n\nRecipe name is not correct !"),msgtConfirm);
    break;

    /*----- Recipe line error -----*/
  case(2) :
    ((MainMenu*)(glob->menu))->DisplayMessage(-1,0,tr("Receipe not saved.\n\nRecipe line not correct for unit : ")+glob->sysCfg[errorNr].MC_ID,msgtConfirm);
    break;
  }
}


void RecipeFrm::RemoveClicked()
{
  //@@ Moet nog geimplementeerd worden!
}


void RecipeFrm::TextReceived(const QString &txt)
{
  QString tmpText;
  RecipeStruct rcp;

  switch (lineIdx) {
  /*----- Recipe description text -----*/
  case 0:
    ui->edRcpText->setText(txt);
    recipeBuf.sText = txt;
    /*----- Convert to ascii for easy storage to file -----*/
    strcpy(recipeBuf.text,txt.toAscii().data());
    recipeBuf.text[MAX_LENGTH_RECIPE_DESCRIPTION] = 0;
    break;

    /*----- Recipe name text -----*/
  case 1:
    tmpText = txt;
    if ((tmpText.isEmpty()) || (tmpText.isNull())) {
      ui->edRcpName->setText(MATERIAL_EMPTY_NAME);
    }
    else {
      ui->edRcpName->setText(tmpText);
      tmpText += RECIPE_EXTENSION;
    }
    /*----- Add extension to tmpText for storage -----*/
    recipeBuf.sName = tmpText;
    /*----- Convert to ascii for easy storage to file -----*/
    strcpy(recipeBuf.name,tmpText.toAscii().data());
    recipeBuf.name[MAX_LENGTH_FILENAME] = 0;
    break;

    /*----- Recipe rename name text -----*/
  case 2:
    tmpText = txt;
    if (!((tmpText.isEmpty()) || (tmpText.isNull()))) {
      /*----- Add extension to tmpText for storage -----*/
      tmpText += RECIPE_EXTENSION;
      recipeBuf.sName = tmpText;
      /*----- Convert to ascii for easy storage to file -----*/
      strcpy(recipeBuf.name,tmpText.toAscii().data());
      recipeBuf.name[MAX_LENGTH_FILENAME] = 0;

      SaveRecipe(&rcp,false,false);
    }
    break;

  }
  lineIdx = -1;
}


void RecipeFrm::ValueReceived(float val)
{
  switch (numIdx) {
  //@@ use different display conversion depending on mode and inpType
  case 0:
    recipeBuf.var1 = val;                                                     /* shotWeight */
    if (recipeBuf.mode == modInject) {
      ui->edVar1->setText(QString::number(val,glob->formats.ShotWeightFmt.format.toAscii(),glob->formats.ShotWeightFmt.precision)+QString(" ")+glob->weightUnits);
    }
    else {
      ui->edVar1->setText(QString::number(val,glob->formats.ExtruderFmt.format.toAscii(),glob->formats.ExtruderFmt.precision)+QString(" ")+glob->extrUnits);
    }
    break;
  case 1:
    recipeBuf.var2 = val;                                                     /* shotTime */
    if (recipeBuf.mode == modInject) {
      ui->edVar2->setText(QString::number(val,glob->formats.TimeFmt.format.toAscii(),glob->formats.TimeFmt.precision)+QString(" ")+glob->timerUnits);
    }
    else {
      ui->edVar2->setText(QString::number(val,glob->formats.TachoFmt.format.toAscii(),glob->formats.TachoFmt.precision)+QString(" ")+glob->tachoUnits);
    }
    break;
  }
  numIdx = -1;
}


void RecipeFrm::RecipeLoaded()
{
  ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
}


void RecipeFrm::RecipeItemMaterialClicked(int idx)
{
  emit SetMaterial(recipeBuf.lines[idx].sMatName);
  lineIdx = 100 + idx;
  ((MainMenu*)(glob->menu))->DisplayWindow(wiMaterials,wiRcpConfig);
}


void RecipeFrm::MaterialReceived(const QString &mat, bool isDefault)
/*--------------------------------------------------*/
/* Material received from material selection screen */
/*--------------------------------------------------*/
{
  int idx;
  if (lineIdx >= 100) {
    idx = lineIdx % 100;
    /*----- Store material in recipe line for the given unit -----*/
    rcpItem[idx]->MaterialReceived(mat,isDefault,idx);
  }
  lineIdx = -1;
}


void RecipeFrm::MessageResult(int res)
/*------------------------------------------------*/
/* Result from "Recipe alreay exsist, overwrite ? */
/*------------------------------------------------*/
{
  RecipeStruct rcp;

  disconnect(((MainMenu*)(glob->menu)),SIGNAL(ErrorResult(int)),this,SLOT(MessageResult(int)));

  /*----- OK Clicked.   -----*/
  if (res == 1) {
    SaveRecipe(&rcp,true,false);
    res = 6;
  }
  /*----- Start keyboard to rename the recipe -----*/
  else if (res == 3) {
    //    recipeBuf.sName = "Test.rcp";
    //    SaveRecipe(&rcp,true,false);
    ////    ((MainMenu*)(glob->menu))->DisplayWindow(wiRcpConfig,wiHome);
    lineIdx = 2;

    QString name;
    name = recipeBuf.sName;
    ClipRecipeExtension(&name);
    keyInp->SetText(name);

    keyInp->SetMaxLength(MAX_LENGTH_FILENAME_INPUT);
    KeyBeep();
    keyInp->show();
  }
}


void RecipeFrm::GetFullRecipePath(QString *rcp)
{
  *rcp = *(glob->AppPath()) + RECIPE_DEFAULT_FOLDER + glob->fileSeparator + *rcp;
}


void RecipeFrm::LoadDefaults()
{
  int i;

  recipeBuf.devCount = glob->mstCfg.slaveCount+1;
  recipeBuf.input = (unsigned char)(glob->mstCfg.inpType);
  recipeBuf.mode = (unsigned char)(glob->mstCfg.mode);

  for (i=0; i<glob->mstCfg.slaveCount+1; i++) {
    rcpItem[i]->SetRecipe(&recipeBuf,i);
  }
}


void RecipeFrm::SysActiveChanged(bool)
{
  SetCorrectButtons();
}


void RecipeFrm::SetRecipe(const QString &rcp)
{
  /*-----------------------*/
  /* Set the actual recipe */
  /*-----------------------*/
  if (!((rcp.isNull()) || (rcp.isEmpty()))) {
    /*----- Load recipe from file and display it -----*/
    RecipeStruct rcpStruct;
    /*----- Load recipe with filename rcp into rcpStruct -----*/
    int res = LoadRecipe(rcp,&rcpStruct);
    /*----- If load was successful, check configuration. -----*/
    if (res == 0) {
      /*----- Recipe is loaded successfully. Display recipe configuration. -----*/
      /*----- Copy recipe from buffer struct to internal data struct. -----*/
      CopyRecipeFromBuf(&rcpStruct);
      /*----- Display recipe on screen -----*/
      DisplayRecipe();
    }
    else {
      DisplayRecipe();
    }
  }
  else {
    ClearRecipeBuf(&recipeBuf);
    LoadDefaults();
    DisplayRecipe();
  }
}


void RecipeFrm::CfgCurrentUserChanged(eUserType)
{
  SetCorrectButtons();
}

#ifndef USBACCESS_H
#define USBACCESS_H

#include <QObject>
#include <QTimer>

class UsbAccess : public QObject
{
  Q_OBJECT
    public:
    explicit UsbAccess(QObject *parent = 0);
    bool UsbPresent();
    bool UsbUnmount();
    QString basePath();
    //    QString SubFolder();
    //    QString FullUsbPath();
    bool DirExists(const QString &);
    int DirCreate(const QString &);
    void FormInit();

  private:
    QObject * _gbi;
    bool usbPresent;

  private slots:

  public slots:
    //void SetUsbMountPath(const QString &);
    //void SetUsbFolder(const QString &);
};
#endif                                                                          // USBACCESS_H

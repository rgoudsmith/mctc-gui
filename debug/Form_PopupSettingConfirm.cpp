#include "Form_PopupSettingConfirm.h"
#include "ui_Form_PopupSettingConfirm.h"
#include "Form_MainMenu.h"
#include "Rout.h"

PopupSettingConfirmFrm::PopupSettingConfirmFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::PopupSettingConfirmFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);
  lastSel = -1;
  unitIdx = -1;

  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));
}


PopupSettingConfirmFrm::~PopupSettingConfirmFrm()
{
  delete ui;
}


void PopupSettingConfirmFrm::changeEvent(QEvent *e)
{
  if (e->type() == QEvent::LanguageChange) {
    LanguageUpdate();
  }
}


void PopupSettingConfirmFrm::showEvent(QShowEvent *)
{
  if (glob->mstSts.popupActive) {
    close();
  }
  else {
    glob->mstSts.popupActive = true;
  }

  if ((glob->menu) != 0) {
    ((MainMenu*)(glob->menu))->FoldMenu();
  }
}


void PopupSettingConfirmFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  glob->SetButtonImage(ui->btOk,itSelection,selOk);
  glob->SetButtonImage(ui->btCancel,itSelection,selCancel);

  ui->label->setFont(*(glob->baseFont));
  ui->lbDeviceId->setFont(*(glob->baseFont));
  ui->lbMessageText->setFont(*(glob->baseFont));
}


void PopupSettingConfirmFrm::LanguageUpdate()
{
  SetUnitIndex(unitIdx);
}


int PopupSettingConfirmFrm::LastSelection()
{
  return lastSel;
}


void PopupSettingConfirmFrm::CloseForm(int res)
{
  lastSel = res;
  glob->mstSts.popupActive = false;
  emit MessageResult(res);
  close();
}


void PopupSettingConfirmFrm::OkClicked()
{
  CloseForm(1);
}


void PopupSettingConfirmFrm::CancelClicked()
{
  CloseForm(0);
}


void PopupSettingConfirmFrm::ShowConfigMessage(const QString &txt, int imgType, int imgIdx, int idx)
{
  SetUnitIndex(idx);
  SetMessageText(txt);
  SetItemImage(imgType,imgIdx);
  show();
}


void PopupSettingConfirmFrm::SetMessageText(const QString &txt)
{
  ui->lbMessageText->setText(txt);
}


void PopupSettingConfirmFrm::SetItemImage(int imgType,int imgIdx)
{
  glob->SetLabelImage(ui->lbItemImage,imgType,imgIdx);
  //        QString * path;
  //        path = glob->GetImagePath(imgType,imgIdx);
  //        QImage img(*path);

  //        ui->lbItemImage->setPixmap(QPixmap::fromImage(img));
  //        ui->lbItemImage->setPixmap(ui->lbItemImage->pixmap()->scaledToHeight(ui->lbItemImage->height()));
}


void PopupSettingConfirmFrm::SetUnitIndex(int idx)
{
  unitIdx = idx;
  if ((idx <= glob->mstCfg.slaveCount) && (idx >=0)) {
    ui->lbDeviceId->setText(glob->sysCfg[unitIdx].MC_ID);
  }
}

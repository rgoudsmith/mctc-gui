/*===========================================================================*
 * File        : LogServer.cpp                                               *
 * Project     : MVC0012 - MC-TC                                             *
 * Description : Log TCP/IP server.                                          *
 *===========================================================================*/

#include "LogServer.h"
#include <iostream>
#include "Common.h"
#include "Rout.h"

LogServer::LogServer(QObject* parent): QObject(parent)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  connect(&server, SIGNAL(newConnection()), this, SLOT(NewConnection()));

  /*----- Listen to Log port -----*/
  server.listen(QHostAddress::Any, 30014);
}


LogServer::~LogServer()
{
  /*------------*/
  /* Destructor */
  /*------------*/
  server.close();
}


void LogServer::FormInit()
{
  /*-----------*/
  /* Init form */
  /*-----------*/
}


void LogServer::NewConnection()
{
  /*---------------------*/
  /* Setup client socket */
  /*---------------------*/
  client = server.nextPendingConnection();

  /*----- Connect readyReady to TCP Read handler -----*/
  connect(client, SIGNAL(readyRead()), this, SLOT(TcpRead()));
  connect(client, SIGNAL(disconnected()), this, SLOT(TcpDisConnected()));
}


void LogServer::TcpRead()
{
  /*-----------------------*/
  /* TCP Read data handler */
  /*-----------------------*/
  char buf[sizeof(tTcpIpLogRpl)];
  tTcpIpLogCmd *cmd;
  tTcpIpLogRpl *rpl;
  int i;
  int command;
  int blockNr;

  QTcpSocket * socket = qobject_cast<QTcpSocket *>(sender());
  if (socket == 0) return;

  /*----- Read data from client -----*/
  if (socket->bytesAvailable() <= sizeof(buf)) {                                /* Check message length */
    socket->read(buf, socket->bytesAvailable());                                /* Message data to buf */

    cmd = (tTcpIpLogCmd*)&buf;                                                  /* Command message */
    rpl = (tTcpIpLogRpl*)&buf;                                                  /* Reply message */

    /*----- Fetch command -----*/
    command = cmd->cmd;                                                         /* Fetch command code */
    blockNr = cmd->blockNr;                                                     /* Block nr requested */

    switch (command) {

      /*----- Read logdata block request -----*/
      case(1):

        /*----- Build reply message -----*/
        rpl->cmd = command;
        rpl->blockNr = blockNr;

        /*----- Copy log data to TcpIp buffer -----*/
        for (i=0; i<LOG_BLOCK_SIZE; i++) {

          /*----- Setup fake data -----*/
          rpl->log[i].logCode = LOG_MOTOR_TYPE;
          rpl->log[i].time0 = 1;
          rpl->log[i].time1 = 2;
          rpl->log[i].time2 = 3;
          rpl->log[i].data = 50.0;
        }
        break;

        /*----- Clear log data request -----*/
      case(2):
        /*----- Build reply message -----*/
        rpl->cmd = command;
        rpl->blockNr = blockNr;
        break;
    }

    /*----- Send reply message -----*/
    socket->write(buf,sizeof(tTcpIpLogRpl));
  }
}


void LogServer::TcpDisConnected()
{
  /*------------------------*/
  /* TCP Disconnect handler */
  /*------------------------*/
  QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());

  if (socket == 0) return;

  //  client->removeOne(socket);
  socket->deleteLater();
}

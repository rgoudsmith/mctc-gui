/********************************************************************************
** Form generated from reading UI file 'Form_AdvLoaderSettings.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_ADVLOADERSETTINGS_H
#define UI_FORM_ADVLOADERSETTINGS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AdvLoaderSettingsFrm
{
public:
    QVBoxLayout *verticalLayout_2;
    QLabel *lbCaption;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_5;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QLabel *lbFillTime;
    QSpacerItem *horizontalSpacer;
    QLineEdit *edFillTime;
    QLabel *lbUnit1;
    QHBoxLayout *horizontalLayout_2;
    QLabel *lbEmptyTime;
    QSpacerItem *horizontalSpacer_2;
    QLineEdit *edEmptyTime;
    QLabel *lbUnit2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *lbAlarmTime;
    QSpacerItem *horizontalSpacer_3;
    QLineEdit *edAlarmTime;
    QLabel *lbUnit3;
    QHBoxLayout *horizontalLayout_4;
    QLabel *lbAlarmMode;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *btAlmMode;
    QLabel *lbUnit3_2;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *btOk;
    QPushButton *btCancel;

    void setupUi(QWidget *AdvLoaderSettingsFrm)
    {
        if (AdvLoaderSettingsFrm->objectName().isEmpty())
            AdvLoaderSettingsFrm->setObjectName(QString::fromUtf8("AdvLoaderSettingsFrm"));
        AdvLoaderSettingsFrm->resize(709, 463);
        AdvLoaderSettingsFrm->setCursor(QCursor(Qt::BlankCursor));
        AdvLoaderSettingsFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout_2 = new QVBoxLayout(AdvLoaderSettingsFrm);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lbCaption = new QLabel(AdvLoaderSettingsFrm);
        lbCaption->setObjectName(QString::fromUtf8("lbCaption"));
        lbCaption->setMaximumSize(QSize(16777215, 50));
        QFont font;
        font.setPointSize(35);
        font.setBold(true);
        font.setWeight(75);
        lbCaption->setFont(font);
        lbCaption->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(lbCaption);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_5);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lbFillTime = new QLabel(AdvLoaderSettingsFrm);
        lbFillTime->setObjectName(QString::fromUtf8("lbFillTime"));
        QFont font1;
        font1.setPointSize(15);
        lbFillTime->setFont(font1);
        lbFillTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(lbFillTime);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        edFillTime = new QLineEdit(AdvLoaderSettingsFrm);
        edFillTime->setObjectName(QString::fromUtf8("edFillTime"));
        edFillTime->setMinimumSize(QSize(100, 70));
        edFillTime->setMaximumSize(QSize(100, 70));
        QFont font2;
        font2.setPointSize(15);
        font2.setBold(false);
        font2.setWeight(50);
        edFillTime->setFont(font2);
        edFillTime->setCursor(QCursor(Qt::BlankCursor));
        edFillTime->setFocusPolicy(Qt::NoFocus);
        edFillTime->setText(QString::fromUtf8("0"));
        edFillTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        edFillTime->setReadOnly(false);

        horizontalLayout->addWidget(edFillTime);

        lbUnit1 = new QLabel(AdvLoaderSettingsFrm);
        lbUnit1->setObjectName(QString::fromUtf8("lbUnit1"));
        lbUnit1->setFont(font1);
        lbUnit1->setText(QString::fromUtf8("s"));

        horizontalLayout->addWidget(lbUnit1);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lbEmptyTime = new QLabel(AdvLoaderSettingsFrm);
        lbEmptyTime->setObjectName(QString::fromUtf8("lbEmptyTime"));
        lbEmptyTime->setFont(font1);
        lbEmptyTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(lbEmptyTime);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        edEmptyTime = new QLineEdit(AdvLoaderSettingsFrm);
        edEmptyTime->setObjectName(QString::fromUtf8("edEmptyTime"));
        edEmptyTime->setMinimumSize(QSize(100, 70));
        edEmptyTime->setMaximumSize(QSize(100, 70));
        edEmptyTime->setFont(font2);
        edEmptyTime->setCursor(QCursor(Qt::BlankCursor));
        edEmptyTime->setFocusPolicy(Qt::NoFocus);
        edEmptyTime->setText(QString::fromUtf8("0"));
        edEmptyTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        edEmptyTime->setReadOnly(false);

        horizontalLayout_2->addWidget(edEmptyTime);

        lbUnit2 = new QLabel(AdvLoaderSettingsFrm);
        lbUnit2->setObjectName(QString::fromUtf8("lbUnit2"));
        lbUnit2->setFont(font1);
        lbUnit2->setText(QString::fromUtf8("s"));

        horizontalLayout_2->addWidget(lbUnit2);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        lbAlarmTime = new QLabel(AdvLoaderSettingsFrm);
        lbAlarmTime->setObjectName(QString::fromUtf8("lbAlarmTime"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lbAlarmTime->sizePolicy().hasHeightForWidth());
        lbAlarmTime->setSizePolicy(sizePolicy);
        lbAlarmTime->setFont(font1);
        lbAlarmTime->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(lbAlarmTime);

        horizontalSpacer_3 = new QSpacerItem(83, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        edAlarmTime = new QLineEdit(AdvLoaderSettingsFrm);
        edAlarmTime->setObjectName(QString::fromUtf8("edAlarmTime"));
        edAlarmTime->setMinimumSize(QSize(100, 70));
        edAlarmTime->setMaximumSize(QSize(100, 70));
        edAlarmTime->setFont(font2);
        edAlarmTime->setCursor(QCursor(Qt::BlankCursor));
        edAlarmTime->setFocusPolicy(Qt::NoFocus);
        edAlarmTime->setText(QString::fromUtf8("0"));
        edAlarmTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        edAlarmTime->setReadOnly(false);

        horizontalLayout_3->addWidget(edAlarmTime);

        lbUnit3 = new QLabel(AdvLoaderSettingsFrm);
        lbUnit3->setObjectName(QString::fromUtf8("lbUnit3"));
        lbUnit3->setFont(font1);
        lbUnit3->setText(QString::fromUtf8("s"));

        horizontalLayout_3->addWidget(lbUnit3);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        lbAlarmMode = new QLabel(AdvLoaderSettingsFrm);
        lbAlarmMode->setObjectName(QString::fromUtf8("lbAlarmMode"));
        lbAlarmMode->setFont(font1);
        lbAlarmMode->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(lbAlarmMode);

        horizontalSpacer_4 = new QSpacerItem(35, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);

        btAlmMode = new QPushButton(AdvLoaderSettingsFrm);
        btAlmMode->setObjectName(QString::fromUtf8("btAlmMode"));
        btAlmMode->setMinimumSize(QSize(90, 70));
        btAlmMode->setMaximumSize(QSize(90, 70));
        btAlmMode->setText(QString::fromUtf8("ON/OFF"));

        horizontalLayout_4->addWidget(btAlmMode);

        lbUnit3_2 = new QLabel(AdvLoaderSettingsFrm);
        lbUnit3_2->setObjectName(QString::fromUtf8("lbUnit3_2"));
        lbUnit3_2->setFont(font1);

        horizontalLayout_4->addWidget(lbUnit3_2);


        verticalLayout->addLayout(horizontalLayout_4);


        horizontalLayout_5->addLayout(verticalLayout);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_6);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_7);

        btOk = new QPushButton(AdvLoaderSettingsFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout_6->addWidget(btOk);

        btCancel = new QPushButton(AdvLoaderSettingsFrm);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("CANCEL"));

        horizontalLayout_6->addWidget(btCancel);


        verticalLayout_2->addLayout(horizontalLayout_6);


        retranslateUi(AdvLoaderSettingsFrm);

        QMetaObject::connectSlotsByName(AdvLoaderSettingsFrm);
    } // setupUi

    void retranslateUi(QWidget *AdvLoaderSettingsFrm)
    {
        lbCaption->setText(QApplication::translate("AdvLoaderSettingsFrm", "Advanced Loader Settings", 0, QApplication::UnicodeUTF8));
        lbFillTime->setText(QApplication::translate("AdvLoaderSettingsFrm", "Fill time:", 0, QApplication::UnicodeUTF8));
        lbEmptyTime->setText(QApplication::translate("AdvLoaderSettingsFrm", "Empty time:", 0, QApplication::UnicodeUTF8));
        lbAlarmTime->setText(QApplication::translate("AdvLoaderSettingsFrm", "Alarm time:", 0, QApplication::UnicodeUTF8));
        lbAlarmMode->setText(QApplication::translate("AdvLoaderSettingsFrm", "Fill alarm mode:", 0, QApplication::UnicodeUTF8));
        lbUnit3_2->setText(QString());
        Q_UNUSED(AdvLoaderSettingsFrm);
    } // retranslateUi

};

namespace Ui {
    class AdvLoaderSettingsFrm: public Ui_AdvLoaderSettingsFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_ADVLOADERSETTINGS_H

/********************************************************************************
** Form generated from reading UI file 'Form_PopupSettingConfirm.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_POPUPSETTINGCONFIRM_H
#define UI_FORM_POPUPSETTINGCONFIRM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PopupSettingConfirmFrm
{
public:
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLabel *lbDeviceId;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *verticalSpacer;
    QLabel *lbMessageText;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *lbItemImage;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *btOk;
    QPushButton *btCancel;

    void setupUi(QWidget *PopupSettingConfirmFrm)
    {
        if (PopupSettingConfirmFrm->objectName().isEmpty())
            PopupSettingConfirmFrm->setObjectName(QString::fromUtf8("PopupSettingConfirmFrm"));
        PopupSettingConfirmFrm->resize(545, 358);
        PopupSettingConfirmFrm->setCursor(QCursor(Qt::BlankCursor));
        PopupSettingConfirmFrm->setWindowTitle(QString::fromUtf8("Form"));
        PopupSettingConfirmFrm->setStyleSheet(QString::fromUtf8("#groupBox {border: 5px solid; border-radius: 7px; background-color: rgb(255,255,255); border-color: rgb(0, 0, 125);}"));
        verticalLayout_3 = new QVBoxLayout(PopupSettingConfirmFrm);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox = new QGroupBox(PopupSettingConfirmFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setTitle(QString::fromUtf8(""));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        lbDeviceId = new QLabel(groupBox);
        lbDeviceId->setObjectName(QString::fromUtf8("lbDeviceId"));
        lbDeviceId->setText(QString::fromUtf8("id"));

        verticalLayout->addWidget(lbDeviceId);


        horizontalLayout_2->addLayout(verticalLayout);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);


        verticalLayout_2->addLayout(horizontalLayout_2);

        verticalSpacer = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer);

        lbMessageText = new QLabel(groupBox);
        lbMessageText->setObjectName(QString::fromUtf8("lbMessageText"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lbMessageText->sizePolicy().hasHeightForWidth());
        lbMessageText->setSizePolicy(sizePolicy);
        lbMessageText->setText(QString::fromUtf8("tekst"));
        lbMessageText->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout_2->addWidget(lbMessageText);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        lbItemImage = new QLabel(groupBox);
        lbItemImage->setObjectName(QString::fromUtf8("lbItemImage"));
        lbItemImage->setMinimumSize(QSize(100, 80));
        lbItemImage->setMaximumSize(QSize(100, 80));
        lbItemImage->setFrameShape(QFrame::Box);
        lbItemImage->setText(QString::fromUtf8(""));
        lbItemImage->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(lbItemImage);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);

        btOk = new QPushButton(groupBox);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout_3->addWidget(btOk);

        btCancel = new QPushButton(groupBox);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("CANCEL"));

        horizontalLayout_3->addWidget(btCancel);


        verticalLayout_2->addLayout(horizontalLayout_3);


        verticalLayout_3->addWidget(groupBox);


        retranslateUi(PopupSettingConfirmFrm);

        QMetaObject::connectSlotsByName(PopupSettingConfirmFrm);
    } // setupUi

    void retranslateUi(QWidget *PopupSettingConfirmFrm)
    {
        label->setText(QApplication::translate("PopupSettingConfirmFrm", "Device ID:", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(PopupSettingConfirmFrm);
    } // retranslateUi

};

namespace Ui {
    class PopupSettingConfirmFrm: public Ui_PopupSettingConfirmFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_POPUPSETTINGCONFIRM_H

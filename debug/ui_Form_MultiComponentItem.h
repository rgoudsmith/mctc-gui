/********************************************************************************
** Form generated from reading UI file 'Form_MultiComponentItem.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_MULTICOMPONENTITEM_H
#define UI_FORM_MULTICOMPONENTITEM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MultiComponentItemFrm
{
public:
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_3;
    QPushButton *btImage;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer_4;
    QLabel *lbName;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer_2;
    QLineEdit *colPct;
    QLabel *btStatus;
    QSpacerItem *verticalSpacer;
    QPushButton *btAlarm;
    QSpacerItem *verticalSpacer_6;
    QPushButton *btConfig;
    QSpacerItem *verticalSpacer_5;
    QPushButton *btInputMode;
    QSpacerItem *verticalSpacer_7;

    void setupUi(QWidget *MultiComponentItemFrm)
    {
        if (MultiComponentItemFrm->objectName().isEmpty())
            MultiComponentItemFrm->setObjectName(QString::fromUtf8("MultiComponentItemFrm"));
        MultiComponentItemFrm->resize(468, 502);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MultiComponentItemFrm->sizePolicy().hasHeightForWidth());
        MultiComponentItemFrm->setSizePolicy(sizePolicy);
        MultiComponentItemFrm->setCursor(QCursor(Qt::BlankCursor));
        MultiComponentItemFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout_4 = new QVBoxLayout(MultiComponentItemFrm);
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        groupBox = new QGroupBox(MultiComponentItemFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        groupBox->setMaximumSize(QSize(130, 700));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        groupBox->setPalette(palette);
        groupBox->setAutoFillBackground(true);
        groupBox->setFlat(false);
        verticalLayout_3 = new QVBoxLayout(groupBox);
        verticalLayout_3->setSpacing(3);
        verticalLayout_3->setContentsMargins(4, 4, 4, 4);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        btImage = new QPushButton(groupBox);
        btImage->setObjectName(QString::fromUtf8("btImage"));
        btImage->setMinimumSize(QSize(0, 100));

        verticalLayout_3->addWidget(btImage);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_4);

        lbName = new QLabel(groupBox);
        lbName->setObjectName(QString::fromUtf8("lbName"));
        lbName->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(lbName);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_3);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        colPct = new QLineEdit(groupBox);
        colPct->setObjectName(QString::fromUtf8("colPct"));
        colPct->setMinimumSize(QSize(0, 40));
        colPct->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(colPct);

        btStatus = new QLabel(groupBox);
        btStatus->setObjectName(QString::fromUtf8("btStatus"));
        btStatus->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(btStatus);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        btAlarm = new QPushButton(groupBox);
        btAlarm->setObjectName(QString::fromUtf8("btAlarm"));
        btAlarm->setMinimumSize(QSize(75, 75));

        verticalLayout_2->addWidget(btAlarm);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_6);

        btConfig = new QPushButton(groupBox);
        btConfig->setObjectName(QString::fromUtf8("btConfig"));
        btConfig->setMinimumSize(QSize(75, 75));

        verticalLayout_2->addWidget(btConfig);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_5);

        btInputMode = new QPushButton(groupBox);
        btInputMode->setObjectName(QString::fromUtf8("btInputMode"));
        btInputMode->setMinimumSize(QSize(75, 75));
        btInputMode->setSizeIncrement(QSize(0, 0));

        verticalLayout_2->addWidget(btInputMode);

        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_7);


        verticalLayout_3->addLayout(verticalLayout_2);


        verticalLayout_4->addWidget(groupBox);


        retranslateUi(MultiComponentItemFrm);

        QMetaObject::connectSlotsByName(MultiComponentItemFrm);
    } // setupUi

    void retranslateUi(QWidget *MultiComponentItemFrm)
    {
        groupBox->setTitle(QString());
        btImage->setText(QApplication::translate("MultiComponentItemFrm", "Image", 0, QApplication::UnicodeUTF8));
        lbName->setText(QApplication::translate("MultiComponentItemFrm", "UnitName", 0, QApplication::UnicodeUTF8));
        colPct->setText(QApplication::translate("MultiComponentItemFrm", "ColPct", 0, QApplication::UnicodeUTF8));
        btStatus->setText(QApplication::translate("MultiComponentItemFrm", "TextLabel", 0, QApplication::UnicodeUTF8));
        btAlarm->setText(QApplication::translate("MultiComponentItemFrm", "PushButton", 0, QApplication::UnicodeUTF8));
        btConfig->setText(QApplication::translate("MultiComponentItemFrm", "PushButton", 0, QApplication::UnicodeUTF8));
        btInputMode->setText(QApplication::translate("MultiComponentItemFrm", "PushButton", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(MultiComponentItemFrm);
    } // retranslateUi

};

namespace Ui {
    class MultiComponentItemFrm: public Ui_MultiComponentItemFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_MULTICOMPONENTITEM_H

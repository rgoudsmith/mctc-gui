/****************************************************************************
** Meta object code from reading C++ file 'Form_McWeightSettings.h'
**
** Created: Thu Jan 31 11:21:28 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_McWeightSettings.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_McWeightSettings.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_McWeightSettingsFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x0a,
      44,   20,   20,   20, 0x0a,
      56,   20,   20,   20, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_McWeightSettingsFrm[] = {
    "McWeightSettingsFrm\0\0UnitValReceived(float)\0"
    "OkClicked()\0CancelClicked()\0"
};

const QMetaObject McWeightSettingsFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_McWeightSettingsFrm,
      qt_meta_data_McWeightSettingsFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &McWeightSettingsFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *McWeightSettingsFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *McWeightSettingsFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_McWeightSettingsFrm))
        return static_cast<void*>(const_cast< McWeightSettingsFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int McWeightSettingsFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: UnitValReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 1: OkClicked(); break;
        case 2: CancelClicked(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

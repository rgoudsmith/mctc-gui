#include "Form_UnitConfigStd.h"
#include "ui_Form_UnitConfigStd.h"
#include "ImageDef.h"
#include "Form_MainMenu.h"
#include "Rout.h"

#define C_iconOffset 10

UnitConfigStdFrm::UnitConfigStdFrm(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::UnitConfigStdFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  m_ui->setupUi(this);
  selFrm = new selectionFrm();
  charKey = new KeyboardFrm();
  // Position input forms
  selFrm->move(((width()/2)-(selFrm->width()/2)),((height()/2)-(selFrm->height()/2)));
  charKey->move(((width()/2)-(charKey->width()/2)),((height()/2)-(charKey->height()/2)));

  connect(this,SIGNAL(selectionMade(int,int,int)),selFrm,SLOT(DisplaySelection(int,int,int)));
  connect(selFrm,SIGNAL(selectionMade(int,int)),this,SLOT(ReceiveSelection(int,int)));

  connect(charKey,SIGNAL(inputText(QString)),this,SLOT(inputReceived(QString)));

  connect(m_ui->btMotorType,SIGNAL(clicked()),this,SLOT(BtMotorTypeClicked()));
  connect(m_ui->btCilinder,SIGNAL(clicked()),this,SLOT(BtCilinderClicked()));
  connect(m_ui->btGranules,SIGNAL(clicked()),this,SLOT(BtGranulesClicked()));
  connect(m_ui->btFillSystem,SIGNAL(clicked()),this,SLOT(BtFillSystemClicked()));
  connect(m_ui->btLevels,SIGNAL(clicked()),this,SLOT(BtLevelsCLicked()));
  connect(m_ui->btKnifeGate,SIGNAL(clicked()),this,SLOT(BtKnifeGateClicked()));
  connect(m_ui->btTacho,SIGNAL(clicked()),this,SLOT(BtTachoClicked()));
  connect(m_ui->btMcwType,SIGNAL(clicked()),this,SLOT(BtMcwTypeClicked()));

  // Right-side "menu" buttons
  connect(m_ui->btGraviRpm,SIGNAL(clicked()),this,SLOT(btGraviRpm_clicked()));
  connect(m_ui->btLoadCell,SIGNAL(clicked()),this,SLOT(btLoadCell_Clicked()));
  connect(m_ui->btTolerances,SIGNAL(clicked()),this,SLOT(btTolerances_clicked()));
  connect(m_ui->btAdvCfg,SIGNAL(clicked()),this,SLOT(ShowAdvUnitConfig()));
  connect(m_ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(m_ui->btDiagnose,SIGNAL(clicked()),this,SLOT(BtDiagClicked()));
  connect(m_ui->btService,SIGNAL(clicked()),this,SLOT(BtServiceClicked()));
  connect(m_ui->btManIO,SIGNAL(clicked()),this,SLOT(BtManualIoClicked()));

  /*----- Hide spare buttons -----*/
  m_ui->btSpare1->hide();

  m_ui->edUnitName->installEventFilter(this);
}


UnitConfigStdFrm::~UnitConfigStdFrm()
{
  /*------------*/
  /* Destructor */
  /*------------*/
  disconnect(charKey,SIGNAL(inputText(QString)),this,SLOT(inputReceived(QString)));
  disconnect(this,SIGNAL(selectionMade(int,int,int)),selFrm,SLOT(DisplaySelection(int,int,int)));
  disconnect(selFrm,SIGNAL(selectionMade(int,int)),this,SLOT(ReceiveSelection(int,int)));

  delete selFrm;
  delete charKey;
  delete m_ui;
}


void UnitConfigStdFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
  case QEvent::LanguageChange:
    m_ui->retranslateUi(this);
    LanguageUpdate();
    break;
  default:
    break;
  }

  m_ui->btMotorType->setIconSize(QSize((m_ui->btCilinder->width()-C_iconOffset),(m_ui->btCilinder->height()-C_iconOffset)));
}


void UnitConfigStdFrm::resizeEvent(QResizeEvent *e)
{
  QWidget::resizeEvent(e);

  glob->SetButtonImage(m_ui->btLevels,itGeneral,genHopper);
  selFrm->move(((width()/2)-(selFrm->width()/2)),((height()/2)-(selFrm->height()/2)));
  charKey->move(((width()/2)-(charKey->width()/2)),((height()/2)-(charKey->height()/2)));
}


bool UnitConfigStdFrm::eventFilter(QObject *obj, QEvent *e)
{
  if (e->type() == QEvent::MouseButtonPress) {
    if (obj == m_ui->edUnitName) {
      KeyBeep();
      UnitNameClicked();
    }
  }
  return QBaseWidget::eventFilter(obj,e);
}


void UnitConfigStdFrm::ShowSelForm()
{
  selFrm->show();
}


void UnitConfigStdFrm::BtDiagClicked()
{
  /*---------------------------*/
  /* BUTTON : Diagnose clicked */
  /*---------------------------*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiBalanceDiag);
}


void UnitConfigStdFrm::BtServiceClicked()
{
  /*--------------------------*/
  /* BUTTON : Service clicked */
  /*--------------------------*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiSysSettings);
}


void UnitConfigStdFrm::BtManualIoClicked()
{
  /*-----------------------------*/
  /* BUTTON : Manual I/O clicked */
  /*-----------------------------*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiManualIo);
}


void UnitConfigStdFrm::BtLevelsCLicked()
{
  /*-------------------------*/
  /* BUTTON : Levels clicked */
  /*-------------------------*/

  /*----- Display regrind config window -----*/
  if ((Rout::ActUnitType() == mcdtBalance) &&
      (glob->sysCfg[glob->ActUnitIndex()].fillSystem.fillSysType == fsRG)) {
    ((MainMenu*)(glob->menu))->DisplayWindow(wiRegrindConfig);
  }
  /*----- Display balance level window -----*/
  else {
    ((MainMenu*)(glob->menu))->DisplayWindow(wiLevels);
  }
}


void UnitConfigStdFrm::BtKnifeGateClicked()
{
  /*----------------------------*/
  /* BUTTON : KnifeGate clicked */
  /*----------------------------*/
  emit selectionMade(itKnifeGate,options[itKnifeGate],glob->ActUnitIndex());
  ShowSelForm();
}


void UnitConfigStdFrm::BtTachoClicked()
{
  /*------------------------*/
  /* BUTTON : Tacho clicked */
  /*------------------------*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiMcWeightSettings);
}


void UnitConfigStdFrm::ShowAdvUnitConfig()
{
  /*----------------------------------*/
  /* BUTTON : Advanced config clicked */
  /*----------------------------------*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiAdvConfig2);
}


void UnitConfigStdFrm::FormInit()
{
  /*-----------*/
  /* Form init */
  /*-----------*/
  MainMenu * m;
  QBaseWidget::ConnectKeyBeep();;

  selFrm->FormInit();
  charKey->FormInit();

  m = ((MainMenu*)(glob->menu));
  glob->SetButtonImage(m_ui->btLevels,itGeneral,genHopper);
  glob->SetButtonImage(m_ui->btTacho,itType,ttTacho);

  LanguageUpdate();

  m_ui->edUnitName->setText(glob->sysCfg[glob->ActUnitIndex()].MC_ID);

  connect(glob,SIGNAL(SysCfg_IDChanged(QString,int)),this,SLOT(UnitNameChanged(QString,int)));

  // connect outgoing signals to report changes
  connect(this,SIGNAL(SysCfg_DosToolChanged(eDosTool,int)),glob,SLOT(SysCfg_SetDosTool(eDosTool,int)));
  connect(this,SIGNAL(SysCfg_MotorChanged(eMotor,int)),glob,SLOT(SysCfg_SetMotorType(eMotor,int)));
  connect(this,SIGNAL(SysCfg_GranulateChanged(eGranulate,int)),glob,SLOT(SysCfg_SetGranulate(eGranulate,int)));
  connect(this,SIGNAL(CfgFillSysChanged(eFillSys,int)),glob,SLOT(CfgFill_SetFillSys(eFillSys,int)));
  connect(this,SIGNAL(SysCfg_GraviRpmChanged(eGraviRpm,int)),glob,SLOT(SysCfg_SetGraviRpm(eGraviRpm,int)));

  // connect incomming signals to receive changes
  connect(glob,SIGNAL(SysCfg_DosToolChanged(eDosTool,int)),this,SLOT(SysCfg_SetDosTool(eDosTool,int)));
  connect(glob,SIGNAL(SysCfg_MotorChanged(eMotor,int)),this,SLOT(SysCfg_SetMotorType(eMotor,int)));
  connect(glob,SIGNAL(SysCfg_GranulateChanged(eGranulate,int)),this,SLOT(SysCfg_SetGranulate(eGranulate,int)));
  connect(glob,SIGNAL(CfgFill_FillSysChanged(eFillSys,int)),this,SLOT(CfgSetFillSys(eFillSys,int)));
  connect(glob,SIGNAL(SysCfg_GraviRpmChanged(eGraviRpm,int)),this,SLOT(SysCfg_SetGraviRpm(eGraviRpm,int)));
  connect(glob,SIGNAL(MstCfg_SlaveCountChanged(int)),this,SLOT(SlaveCountChanged(int)));
  connect(glob,SIGNAL(MstCfg_UserChanged(eUserType)),this,SLOT(CfgCurrentUserChanged(eUserType)));

  // Confirm button (must be done as last, to make sure KeyBeep is called before the form is closed.

  connect(glob,SIGNAL(MstSts_ActUnitChanged(int)),this,SLOT(ActiveUnitIndexChanged(int)));

  m_ui->btService->setFont(*(glob->baseFont));
  m_ui->btDiagnose->setFont(*(glob->baseFont));
  m_ui->btManIO->setFont(*(glob->baseFont));
}


void UnitConfigStdFrm::SetButtonsByLevel()
{
  /*---------------------------------------------------*/
  /* Display Diagnose / Service and manual I/O buttons */
  /*---------------------------------------------------*/
  switch (glob->GetUser()) {
  case utOperator:
  case utTooling:
  case utSupervisor:
  case utSupervisorBackdoor:
    m_ui->btDiagnose->setVisible(false);
    m_ui->btManIO->setVisible(false);
    m_ui->btService->setVisible(false);

    /*----- Disable some buttons when unit is on -----*/
    if (Rout::prodData.onOff) {
      m_ui->btMotorType->setEnabled(false);
      m_ui->btCilinder->setEnabled(false);
      m_ui->btGraviRpm->setEnabled(false);
      m_ui->btService->setEnabled(false);
    }
    else {
      m_ui->btMotorType->setEnabled(true);
      m_ui->btCilinder->setEnabled(true);
      m_ui->btGraviRpm->setEnabled(true);
      m_ui->btService->setEnabled(true);
    }
    break;

  case utAgent:
    m_ui->btDiagnose->setVisible(false);
    m_ui->btManIO->setVisible(true);
    m_ui->btService->setVisible(true);

    m_ui->btMotorType->setEnabled(true);
    m_ui->btCilinder->setEnabled(true);
    m_ui->btGraviRpm->setEnabled(true);
    m_ui->btService->setEnabled(true);

    break;

  case utMovaColor:
  case utSyrinx:
    m_ui->btDiagnose->setVisible(true);
    m_ui->btManIO->setVisible(true);
    m_ui->btService->setVisible(true);

    m_ui->btMotorType->setEnabled(true);
    m_ui->btCilinder->setEnabled(true);
    m_ui->btGraviRpm->setEnabled(true);
    m_ui->btService->setEnabled(true);
    break;

  default:
    break;
  }
}


void UnitConfigStdFrm::showEvent(QShowEvent *e)
{
  /*-----------------*/
  /* Show form event */
  /*-----------------*/
  int idx;

  QBaseWidget::showEvent(e);

  /*----- Display service/diagnose/IO buttons -----*/
  SetButtonsByLevel();

  /*----- Setup config buttons depending on the unit type -----*/
  switch(Rout::ActUnitType()) {

  case mcdtWeight :
    m_ui->btMotorType->hide();
    m_ui->btCilinder->hide();
    m_ui->btGranules->hide();
    m_ui->btTolerances->hide();
    m_ui->btGraviRpm->hide();
    m_ui->btFillSystem->hide();
    m_ui->btTacho->show();
    m_ui->btKnifeGate->show();
    m_ui->btMcwType->show();
    break;

  case mcdtBalanceHO :
    m_ui->btMotorType->show();
    m_ui->btCilinder->show();
    m_ui->btGranules->show();
    m_ui->btTolerances->hide();
    m_ui->btGraviRpm->hide();
    m_ui->btFillSystem->hide();
    m_ui->btTacho->hide();
    m_ui->btKnifeGate->show();
    m_ui->btMcwType->show();
    break;

  default :
    m_ui->btMotorType->show();
    m_ui->btCilinder->show();
    m_ui->btGranules->show();
    m_ui->btTolerances->show();
    m_ui->btGraviRpm->show();
    m_ui->btFillSystem->show();
    m_ui->btTacho->hide();
    m_ui->btKnifeGate->hide();
    m_ui->btMcwType->hide();
    break;
  }

  /*----- Update knife gate button image -----*/
  idx = glob->ActUnitIndex();

  if (glob->sysCfg[idx].fillSystem.knifeGateValve == true) {
    glob->SetButtonImage(m_ui->btKnifeGate,itKnifeGate,knifeGateOn);
  }
  else {
    glob->SetButtonImage(m_ui->btKnifeGate,itKnifeGate,knifeGateOff);
  }

  /*----- Update McWeight type button,
          is also used to define the BalanceHO type  -----*/
  SysCfg_SetDosTool(glob->sysCfg[idx].dosTool,idx);



  /*---- McWeight unit type -----*/
  if (Rout::ActUnitType() == mcdtWeight) {
    glob->SetButtonImage(m_ui->btMcwType,itMcwType,glob->sysCfg[idx].mcWeightType);
                           }
  /*----- Balance HO unit type -----*/
  else {
    glob->SetButtonImage(m_ui->btMcwType,itBalHOType,glob->sysCfg[idx].mcWeightType);
  }
}


void UnitConfigStdFrm::LanguageUpdate()
{
  glob->SetButtonImage(m_ui->btLoadCell,itConfig,cfgCal_Menu);
  glob->SetButtonImage(m_ui->btTolerances,itConfig,cfgTol_Menu);
  glob->SetButtonImage(m_ui->btAdvCfg,itConfig,cfgAdvConfig);

  glob->SetButtonImage(m_ui->btOk,itSelection,selOk);

  m_ui->lbCaption->setFont(*(glob->captionFont));
  m_ui->lbUnitName->setFont(*(glob->baseFont));
  m_ui->edUnitName->setFont(*(glob->baseFont));
  SetupDisplay();
}


void UnitConfigStdFrm::SetupDisplay()
{
  // Load all settings
  int idx = glob->ActUnitIndex();
  SysCfg_SetDosTool(glob->sysCfg[idx].dosTool,idx);
  SysCfg_SetMotorType(glob->sysCfg[idx].motor,idx);
  SysCfg_SetGranulate(glob->sysCfg[idx].granulate,idx);
  CfgSetFillSys(glob->sysCfg[idx].fillSystem.fillSysType,idx);
  SysCfg_SetGraviRpm(glob->sysCfg[idx].GraviRpm,idx);
}


void UnitConfigStdFrm::ReceiveSelection(int _selType, int _selection)
{
  QString val;
  QString imgPath;
  QPushButton * btn;
  QImage img;

  btn = 0;
  options[_selType] = _selection;

  // Get the corresponding button by _selType
  // and update the config structure in GlobalItems
  switch(_selType) {

  case(itMcwType):
    btn = m_ui->btMcwType;
    glob->MstCfg_SetMcwType((eMcwtype)_selection,glob->ActUnitIndex());
    break;

  case(itBalHOType):
    btn = m_ui->btMcwType;
    glob->MstCfg_SetBalHOType((eBalHOtype)_selection,glob->ActUnitIndex());
    break;

  case itKnifeGate:
    btn = m_ui->btKnifeGate;
    glob->SysCfg_SetKnifeGate((eKnifeGate)_selection,glob->ActUnitIndex());
    break;

  case itDosTool:
    emit SysCfg_DosToolChanged((eDosTool)_selection,glob->ActUnitIndex());
    btn = m_ui->btCilinder;
    break;

  case itMotor:
    btn = m_ui->btMotorType;
    emit SysCfg_MotorChanged((eMotor)_selection,glob->ActUnitIndex());
    break;

  case itGranulate:
    btn = m_ui->btGranules;
    emit SysCfg_GranulateChanged((eGranulate)_selection,glob->ActUnitIndex());
    break;

  case itFillSys:
    btn = m_ui->btFillSystem;
    emit CfgFillSysChanged((eFillSys)_selection,glob->ActUnitIndex());
    break;

  case itType:
    emit MstCfg_InputTypeChanged((eType)_selection);
    break;

  case 5:
    btn = m_ui->btLevels;
    break;

  case itGraviRpm:
    btn = m_ui->btGraviRpm;
    emit SysCfg_GraviRpmChanged((eGraviRpm)_selection,glob->ActUnitIndex());
    break;

  case 7:
    //    btn = m_ui->bt23;
    break;

  case 11:
    btn = m_ui->btLoadCell;
    break;

  case 12:
    btn = m_ui->btTolerances;
    break;
  }

  // Try to load the desired image.
  if (btn != 0) {
    glob->SetButtonImage(btn,_selType,_selection);
  }
}


void UnitConfigStdFrm::SysCfg_SetDosTool(eDosTool dt,int idx)
{
  if ((int)dt < dtCount) {
    if (glob->ActUnitIndex() == idx) {
      ReceiveSelection(itDosTool,dt);
    }
  }
  else {
    qDebug() << "Warning Dostool error !!!";
  }
}


void UnitConfigStdFrm::SysCfg_SetMotorType(eMotor mt, int idx)
{
  if (glob->ActUnitIndex() == idx) {
    ReceiveSelection(itMotor,mt);
  }
}


void UnitConfigStdFrm::SysCfg_SetGranulate(eGranulate gt,int idx)
{
  if (glob->ActUnitIndex() == idx) {
    ReceiveSelection(itGranulate,gt);
  }
}


void UnitConfigStdFrm::CfgSetFillSys(eFillSys fs,int idx)
{
  if (idx == glob->ActUnitIndex()) {
    ReceiveSelection(itFillSys,fs);
  }
}


void UnitConfigStdFrm::MstCfg_SetInpType(eType tt)
{
  ReceiveSelection(itType,tt);
}


void UnitConfigStdFrm::SysCfg_SetGraviRpm(eGraviRpm gr, int idx)
{
  if (glob->ActUnitIndex() == idx) {
    ReceiveSelection(itGraviRpm,gr);
  }
}


void UnitConfigStdFrm::CfgCurrentUserChanged(eUserType ut)
{
  if (ut < utSupervisor) {
    m_ui->btAdvCfg->setVisible(false);
  }
  else {
    m_ui->btAdvCfg->setVisible(true);
  }
}


void UnitConfigStdFrm::BtMotorTypeClicked()
{
  /*-----------------------------*/
  /* BUTTON : Motor type clicked */
  /*-----------------------------*/
  emit selectionMade(itMotor,options[itMotor],glob->ActUnitIndex());
  ShowSelForm();
}


void UnitConfigStdFrm::BtCilinderClicked()
{
  /*---------------------------*/
  /* BUTTON : Cilinder clicked */
  /*---------------------------*/
  emit selectionMade(itDosTool,options[itDosTool],glob->ActUnitIndex());
  ShowSelForm();
}


void UnitConfigStdFrm::BtGranulesClicked()
{
  /*---------------------------*/
  /* BUTTON : Granules clicked */
  /*---------------------------*/
  emit selectionMade(itGranulate,options[itGranulate],glob->ActUnitIndex());
  ShowSelForm();
}


void UnitConfigStdFrm::BtMcwTypeClicked()
{
  /*--------------------------------*/
  /* BUTTON : McWeight type clicked */
  /*--------------------------------*/

  /*----- Unit type McWeight -----*/
  if (Rout::ActUnitType() == mcdtWeight) {
    emit selectionMade(itMcwType,options[itMcwType],glob->ActUnitIndex());
  }
  /*----- Unit type Balance HO -----*/
  else {
    emit selectionMade(itBalHOType,options[itBalHOType],glob->ActUnitIndex());
  }
  ShowSelForm();
}


void UnitConfigStdFrm::BtFillSystemClicked()
{
  /*-----------------------------*/
  /* BUTTON : FillSystem clicked */
  /*-----------------------------*/
  emit selectionMade(itFillSys,options[itFillSys],glob->ActUnitIndex());
  ShowSelForm();
}


void UnitConfigStdFrm::btGraviRpm_clicked()
{
  /*-----------------------*/
  /* Gravi / RPM selection */
  /*-----------------------*/
  emit selectionMade(itGraviRpm,options[itGraviRpm],glob->ActUnitIndex());
  ShowSelForm();
}


void UnitConfigStdFrm::btLoadCell_Clicked()
{
  /*----------------------*/
  /* Loadcell calibration */
  /*----------------------*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiCalibSelect);
}


void UnitConfigStdFrm::btTolerances_clicked()
{
  /*---------------------*/
  /* Tolerance settings */
  /*---------------------*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiTolerances);
}


void UnitConfigStdFrm::OkClicked()
{
  ((MainMenu*)glob->menu)->DisplayWindow(wiHome);
}


void UnitConfigStdFrm::UnitNameClicked()
{
  charKey->SetText(m_ui->edUnitName->text());
  charKey->SetMaxLength(10);
  charKey->show();
}


void UnitConfigStdFrm::inputReceived(const QString &ID)
{
  glob->SysCfg_SetID(ID,glob->ActUnitIndex());
}


void UnitConfigStdFrm::UnitNameChanged(const QString &ID, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    m_ui->edUnitName->setText(ID);
  }
}


void UnitConfigStdFrm::ActiveUnitIndexChanged(int idx)
{
  SysCfg_SetDosTool(glob->sysCfg[idx].dosTool,idx);
  SysCfg_SetMotorType(glob->sysCfg[idx].motor,idx);
  SysCfg_SetGranulate(glob->sysCfg[idx].granulate,idx);
  CfgSetFillSys(glob->sysCfg[idx].fillSystem.fillSysType,idx);
  SysCfg_SetGraviRpm(glob->sysCfg[idx].GraviRpm,idx);
  UnitNameChanged(glob->sysCfg[idx].MC_ID,idx);
}


void UnitConfigStdFrm::SlaveCountChanged(int)
{
  SetupDisplay();
}

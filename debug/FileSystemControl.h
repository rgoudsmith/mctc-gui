#ifndef FILESYSTEMCONTROL_H
#define FILESYSTEMCONTROL_H

#include <QDir>
#include <QFile>
#include <QObject>
#include "ImageDef.h"
#include "Glob.h"

/*  --- Controller class for file operation ---

 - Storage of system configuration / settings
   o master Config (mstCfg)
   o system Config (sysCfg) per unit (some smart system to make sure we don't save too many data to disk

 - Storage of production data / event log / alarm log / etc
*/

struct MasterConfigFile
{
  /*------------------------------------------------------------------------*/
  /* Master configuration structure as read/written to/from the file system */
  /*------------------------------------------------------------------------*/
  int language;                                                                 /* Language */
  int mode;                                                                     /* Mode : Injection moulding / Extrusion */
  int inpType;                                                                  /* Input type : Relay / Timer / Tacho */
  bool autoStart;                                                               /* Auto start enable */
  bool rcpEnable;                                                               /* Recipe enable */
  bool twinMode;                                                                /* Twin mode */
  bool keyBeep;                                                                 /* Key beep enable */
  int wghtUnits;                                                                /* Weight units */
  int slaveCount;                                                               /* Number of connected slaves */
  int defLogin;                                                                 /* Default login level */
  char SuperPasswd[(MAX_LENGTH_PASSWD+1)];                                      /* Supervisor password */
  char ToolPasswd[(MAX_LENGTH_PASSWD+1)];                                       /* Tooling password */
  char RecipeName[(MAX_LENGTH_FILENAME+1)];                                     /* Recipe name */
  int IPAddress[4];                                                             /* IP address */
  int Netmask[4];                                                               /* Netmask */
  int Gateway[4];                                                               /* Gateway */
  int modbusAddr;                                                               /* Modbus address */
  int calState;                                                                 /* ????? */
  SettingMvcMstStruct MvcSettings;                                              /* ????? */
  bool motorAlarmEnb;                                                           /* Motor alarm enbable */
  bool profibusEnb;
  bool imperialMode;
//  int McWeightType;                                                             /* McWeight type */
  bool units_kgh;                                                               /* Units Kg/H mode */
  float shortMeasTime;                                                          /* McWeight short measurement time */
  float hysterese;                                                              /* McWeight hysterese */
  float tachoRatio;                                                             /* McWeight tacho ratio */
  float minExtCap;                                                              /* Minimum extruder capacity */
  bool canWdDis;                                                                /* Canbus WD disable */
};

struct MaterialBuf
{
  char name[(MAX_LENGTH_FILENAME+1)];
  bool isDefault;
};

struct SysConfigFile
{
  /*------------------------------------------------------------------------*/
  /* System configuration structure as read/written to/from the file system */
  /*------------------------------------------------------------------------*/

  /*----- Standard system config -----*/
  char MC_ID[(MAX_LENGTH_MC_ID+1)];                                             /* Unit description */
  eMcDeviceType deviceType;                                                     /* Device type : Balance, Balance HO, McWeight etc. */
  eDosTool dosTool;                                                             /* Dosing tool */
  eMotor motor;                                                                 /* Motor type */
  eGranulate granulate;                                                         /* Granulate type : NG. MG etc. */
  FillSysStruct fillSystem;                                                     /* Fillsystem : ME, MV etc. */
  FillLevelsStruct fillLevels;                                                  /* Filling levels */
  RegrindParmStruct regrindParm;                                                /* Regrind parameters */
  bool sysEnabled;                                                              /* Unit enable/disable */
  float colPct;                                                                 /* Color percentage set */
  float setRpm;                                                                 /* RPM set */

  /*----- Advanced system config -----*/
  eGraviRpm GraviRpm;                                                           /* 0 = RPM mode, 1 = Gravi metric mode */
  float calibDev;                                                               /* ???????????? */
  int McWeightType;                                                             /* McWeight type */
  PrimeCfg prime;                                                               /* Prime configuration */
  TestVal test;                                                                 /* Test function variables */
  SysInjectCfgStruct injection;                                                 /* Injection moulding mode settings */
  SysExtCfgStruct extrusion;                                                    /* Extrusion mode settings */
  tCurveStruct curve;                                                           /* Curve data */
  MaterialBuf material;                                                         /* ????? wordt nergens gebruikt !! */
  SettingMvcStruct MvcSettings;                                                 /* Various system settings */
};

class FileSystemControl : public QObject
{
  Q_OBJECT
    public:
    explicit FileSystemControl(QObject *parent = 0);
    int SetBaseDir(const QString &);
    void PrintMasterStruct(MasterConfig * const);
    void PrintSystemStruct(SysConfig * const);

    int CalculateChecksum(char *, int);
    bool CheckChecksum(char *,int,int);

    void LoadRecipe(RecipeStruct * const, const QString &);
    void SaveRecipe(RecipeStruct * const, const QString &);

  private:
    QDir directory;
    QFile file;
    QString baseDir;
    MasterConfigFile masterBuf;
    SysConfigFile systemBuf;

    void CopyMasterData(MasterConfig * const,int);
    void CopySystemData(SysConfig * const, int);

    signals:
    void MasterSettingsSaved();
    void SystemSettingsSaved(int);
    void MasterSettingsLoaded();
    void SystemSettingsLoaded(int);

  public slots:
    void StoreMasterFactDefaults();
    void StoreMasterSettings(MasterConfig * const, int, int &);
    void LoadMasterSettings(MasterConfig * const,int &);
    void StoreSystemFactDefaults(global * const, int);
    void StoreSystemSettings(int, global * const, int, int &);
    void LoadSystemSettings(int, global *,int &);
};
#endif                                                                          // FILESYSTEMCONTROL_H

#ifndef TUSBACCESS_H
#define TUSBACCESS_H

#include <QObject>
#include <QTimer>

class TUsbAccess : public QObject
{
  Q_OBJECT
    public:
    explicit TUsbAccess(QObject *parent = 0);
    bool UsbPresent();
    QString basePath();
    QString SubFolder();
    QString FullUsbPath();
    bool DirExists(const QString &);
    int DirCreate(const QString &);
    void SetGBI(QObject *);

  private:
    QObject * _gbi;
    bool usbPresent;
    QString mountPath;
    QString devicePath;
    QString subFolder;
    QTimer usbTim;
    QTimer removeTim;
    bool ScanFileForFirmware(const QString &, QString *);
    bool isRemoving;
    int removeState;

    void SyncUSB();

    signals:
    void UsbDetected();
    void UsbRemoved();
    void UsbFirmwareStartScan();
    void UsbFirmwareFound(const QString &, const QString &);
    void RemovingUSB();
    void ReadyForRemove();

  private slots:
    void timUpdate();
    void timRemove();

  public slots:
    void ScanForFirmware();
    int StartFirmwareUpdate(const QString &);
    void SetUsbMountPath(const QString &);
    void SetUsbFolder(const QString &);
    void PrepareForRemove();
};
#endif                                                                          // TUSBACCESS_H

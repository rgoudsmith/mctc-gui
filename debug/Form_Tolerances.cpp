#include "Form_Tolerances.h"
#include "ui_Form_Tolerances.h"
#include "ImageDef.h"
#include "Form_MainMenu.h"
#include "Rout.h"

TolerancesFrm::TolerancesFrm(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::TolerancesFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  lineNr = -1;
  devAlarm = 0;
  calibDev = 0;

  m_ui->setupUi(this);

  numKeys = new NumericInputFrm();
  numKeys->move(((width()/2)-(numKeys->width()/2)),((height()/2)-(numKeys->height()/2)));
  numKeys->hide();

  connect(m_ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));
  connect(m_ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));

  m_ui->edDevAlm->installEventFilter(this);
  m_ui->edCalDev->installEventFilter(this);
}


TolerancesFrm::~TolerancesFrm()
{
  delete numKeys;
  delete m_ui;
}


void TolerancesFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      m_ui->retranslateUi(this);
      break;
    default:
      break;
  }
}


void TolerancesFrm::resizeEvent(QResizeEvent *e)
{
  QWidget::resizeEvent(e);
  numKeys->move(((width()/2)-(numKeys->width()/2)),((height()/2)-(numKeys->height()/2)));
}


void TolerancesFrm::showEvent(QShowEvent *e)
{
  QWidget::showEvent(e);
  SetButtonImages();
}


bool TolerancesFrm::eventFilter(QObject *obj, QEvent *ev)
{
  if (ev->type() == QEvent::MouseButtonPress) {
    if (obj == m_ui->edDevAlm) {
      KeyBeep();
      DevAlmClicked();
    }

    if (obj == m_ui->edCalDev) {
      KeyBeep();
      CalDevClicked();
    }
  }
  return QWidget::eventFilter(obj,ev);
}


void TolerancesFrm::SetButtonImages()
{
  calibDev = glob->sysCfg[glob->ActUnitIndex()].calibDev;
  devAlarm = glob->sysCfg[glob->ActUnitIndex()].MvcSettings.balance.devAlmPctB4.actVal;

  CalDevChanged(calibDev,glob->ActUnitIndex());
  DevAlmChanged(devAlarm,glob->ActUnitIndex());

  glob->SetButtonImage(m_ui->btCancel,itSelection,selCancel);
  glob->SetButtonImage(m_ui->btOk,itSelection,selOk);
}


void TolerancesFrm::FormInit()
{
  MainMenu* m;
  QBaseWidget::ConnectKeyBeep();;

  m = ((MainMenu*)(glob->menu));
  connect(this,SIGNAL(DisplayWindow(eWindowIdx)),m,SLOT(DisplayWindow(eWindowIdx)));

  numKeys->FormInit();
  connect(numKeys,SIGNAL(InputValue(float)),this,SLOT(UnitValReceived(float)));

  connect(glob,SIGNAL(SysCfg_DevAlarmChanged(float,int)),this,SLOT(DevAlmChanged(float,int)));
  connect(glob,SIGNAL(SysCfg_CalDevChanged(float,int)),this,SLOT(CalDevChanged(float,int)));

  connect(glob,SIGNAL(MstCfg_LanguageChanged(eLanguages)),this,SLOT(LanguageChanged(eLanguages)));

  m_ui->lbCaption->setFont(*(glob->captionFont));

  m_ui->lbCalibDev->setFont(*(glob->baseFont));
  m_ui->lbDevAlarm->setFont(*(glob->baseFont));

  m_ui->edCalDev->setFont(*(glob->baseFont));
  m_ui->edDevAlm->setFont(*(glob->baseFont));

  SetButtonImages();
}


void TolerancesFrm::DevAlmChanged(float devAlm, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    devAlarm = devAlm;
    m_ui->edDevAlm->setText(QString("%1").arg(devAlm,glob->formats.AlarmPctFmt.fieldWidth,glob->formats.AlarmPctFmt.format.toAscii(),glob->formats.AlarmPctFmt.precision)+" %");
  }
}


void TolerancesFrm::CalDevChanged(float calDev,int idx)
{
  if (idx == glob->ActUnitIndex()) {
    calibDev = calDev;
    m_ui->edCalDev->setText(QString("%1").arg(calDev,glob->formats.CalDevFmt.fieldWidth,glob->formats.CalDevFmt.format.toAscii(),glob->formats.CalDevFmt.precision)+" %");
  }
}


void TolerancesFrm::DevAlmClicked()
{
  lineNr = 0;
  numKeys->SetDisplay(true,glob->formats.AlarmPctFmt.precision,devAlarm,0,100);
  numKeys->show();
}


void TolerancesFrm::CalDevClicked()
{
  lineNr = 1;
  numKeys->SetDisplay(true,glob->formats.CalDevFmt.precision,calibDev,0,100);
  numKeys->show();
}


void TolerancesFrm::UnitValReceived(float val)
{
  switch(lineNr) {
    case 0:
      DevAlmChanged(val,glob->ActUnitIndex());
      break;
    case 1:
      CalDevChanged(val,glob->ActUnitIndex());
      break;
  }
  lineNr = -1;
}


void TolerancesFrm::CancelClicked()
{
  ((MainMenu*)(glob->menu))->DisplayWindow(wiUnitCfgStd);
}


void TolerancesFrm::OkClicked()
{
  glob->SysCfg_SetDevAlarm(devAlarm,glob->ActUnitIndex());
  glob->SysCfg_SetCalDev(calibDev,glob->ActUnitIndex());
  CancelClicked();
}


void TolerancesFrm::LanguageChanged(eLanguages)
{
  SetButtonImages();
}

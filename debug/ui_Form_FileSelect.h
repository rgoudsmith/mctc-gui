/********************************************************************************
** Form generated from reading UI file 'Form_FileSelect.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_FILESELECT_H
#define UI_FORM_FILESELECT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FileSelectFrm
{
public:
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_4;
    QVBoxLayout *verticalLayout_7;
    QPushButton *btUp;
    QLabel *lbScrollBar;
    QPushButton *btDown;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout;
    QLabel *boxUp;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_5;
    QLabel *boxSelect;
    QPushButton *btOk;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_2;
    QLabel *boxDown;
    QSpacerItem *verticalSpacer_4;
    QLabel *lbIndex;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label;
    QSpacerItem *horizontalSpacer_5;
    QVBoxLayout *verticalLayout;
    QPushButton *btSearch;
    QSpacerItem *verticalSpacer;
    QPushButton *btNew;
    QPushButton *btDeleteAll;
    QPushButton *btDelete;
    QPushButton *btFileRename;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *FileSelectFrm)
    {
        if (FileSelectFrm->objectName().isEmpty())
            FileSelectFrm->setObjectName(QString::fromUtf8("FileSelectFrm"));
        FileSelectFrm->resize(774, 376);
        FileSelectFrm->setMaximumSize(QSize(16777215, 376));
        FileSelectFrm->setCursor(QCursor(Qt::BlankCursor));
        FileSelectFrm->setWindowTitle(QString::fromUtf8("Form"));
        horizontalLayout_4 = new QHBoxLayout(FileSelectFrm);
        horizontalLayout_4->setSpacing(0);
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_4 = new QSpacerItem(80, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(0);
#ifndef Q_OS_MAC
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
#endif
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        btUp = new QPushButton(FileSelectFrm);
        btUp->setObjectName(QString::fromUtf8("btUp"));
        btUp->setMinimumSize(QSize(90, 70));
        btUp->setMaximumSize(QSize(90, 70));
        btUp->setText(QString::fromUtf8("Up"));
        btUp->setAutoRepeat(true);
        btUp->setAutoRepeatDelay(300);
        btUp->setAutoRepeatInterval(200);

        verticalLayout_7->addWidget(btUp);

        lbScrollBar = new QLabel(FileSelectFrm);
        lbScrollBar->setObjectName(QString::fromUtf8("lbScrollBar"));
        lbScrollBar->setFrameShape(QFrame::Box);
        lbScrollBar->setText(QString::fromUtf8(""));

        verticalLayout_7->addWidget(lbScrollBar);

        btDown = new QPushButton(FileSelectFrm);
        btDown->setObjectName(QString::fromUtf8("btDown"));
        btDown->setMinimumSize(QSize(90, 70));
        btDown->setMaximumSize(QSize(90, 70));
        btDown->setText(QString::fromUtf8("Down"));
        btDown->setAutoRepeat(true);
        btDown->setAutoRepeatDelay(300);
        btDown->setAutoRepeatInterval(200);

        verticalLayout_7->addWidget(btDown);


        horizontalLayout_4->addLayout(verticalLayout_7);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        boxUp = new QLabel(FileSelectFrm);
        boxUp->setObjectName(QString::fromUtf8("boxUp"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(boxUp->sizePolicy().hasHeightForWidth());
        boxUp->setSizePolicy(sizePolicy);
        boxUp->setMinimumSize(QSize(450, 153));
        boxUp->setMaximumSize(QSize(450, 153));
        QFont font;
        font.setPointSize(20);
        boxUp->setFont(font);
        boxUp->setFrameShape(QFrame::Box);
        boxUp->setFrameShadow(QFrame::Plain);
        boxUp->setText(QString::fromUtf8(""));
        boxUp->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        horizontalLayout->addWidget(boxUp);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        horizontalLayout->addItem(verticalSpacer_3);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout_4->addLayout(horizontalLayout);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(0);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        boxSelect = new QLabel(FileSelectFrm);
        boxSelect->setObjectName(QString::fromUtf8("boxSelect"));
        boxSelect->setMinimumSize(QSize(450, 70));
        boxSelect->setMaximumSize(QSize(450, 70));
        boxSelect->setFrameShape(QFrame::Box);
        boxSelect->setLineWidth(4);
        boxSelect->setText(QString::fromUtf8(""));
        boxSelect->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_5->addWidget(boxSelect);

        btOk = new QPushButton(FileSelectFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("Ok"));

        horizontalLayout_5->addWidget(btOk);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);


        verticalLayout_4->addLayout(horizontalLayout_5);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        boxDown = new QLabel(FileSelectFrm);
        boxDown->setObjectName(QString::fromUtf8("boxDown"));
        sizePolicy.setHeightForWidth(boxDown->sizePolicy().hasHeightForWidth());
        boxDown->setSizePolicy(sizePolicy);
        boxDown->setMinimumSize(QSize(450, 153));
        boxDown->setMaximumSize(QSize(450, 153));
        boxDown->setFont(font);
        boxDown->setFrameShape(QFrame::Box);
        boxDown->setText(QString::fromUtf8(""));
        boxDown->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        boxDown->setIndent(0);
        boxDown->setTextInteractionFlags(Qt::NoTextInteraction);

        horizontalLayout_2->addWidget(boxDown);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        horizontalLayout_2->addItem(verticalSpacer_4);

        lbIndex = new QLabel(FileSelectFrm);
        lbIndex->setObjectName(QString::fromUtf8("lbIndex"));
        lbIndex->setMinimumSize(QSize(90, 0));
        lbIndex->setMaximumSize(QSize(90, 16777215));
        lbIndex->setText(QString::fromUtf8("999/999"));
        lbIndex->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(lbIndex);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_4->addLayout(horizontalLayout_2);


        horizontalLayout_4->addLayout(verticalLayout_4);

        label = new QLabel(FileSelectFrm);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_4->addWidget(label);

        horizontalSpacer_5 = new QSpacerItem(80, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        btSearch = new QPushButton(FileSelectFrm);
        btSearch->setObjectName(QString::fromUtf8("btSearch"));
        btSearch->setMinimumSize(QSize(90, 70));
        btSearch->setMaximumSize(QSize(90, 70));
        btSearch->setText(QString::fromUtf8("Search"));

        verticalLayout->addWidget(btSearch);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        btNew = new QPushButton(FileSelectFrm);
        btNew->setObjectName(QString::fromUtf8("btNew"));
        btNew->setMinimumSize(QSize(90, 70));
        btNew->setMaximumSize(QSize(90, 70));
        btNew->setText(QString::fromUtf8("New"));

        verticalLayout->addWidget(btNew);

        btDeleteAll = new QPushButton(FileSelectFrm);
        btDeleteAll->setObjectName(QString::fromUtf8("btDeleteAll"));
        btDeleteAll->setMinimumSize(QSize(90, 70));

        verticalLayout->addWidget(btDeleteAll);

        btDelete = new QPushButton(FileSelectFrm);
        btDelete->setObjectName(QString::fromUtf8("btDelete"));
        btDelete->setMinimumSize(QSize(90, 70));
        btDelete->setMaximumSize(QSize(90, 70));
        btDelete->setText(QString::fromUtf8("Delete"));

        verticalLayout->addWidget(btDelete);

        btFileRename = new QPushButton(FileSelectFrm);
        btFileRename->setObjectName(QString::fromUtf8("btFileRename"));
        btFileRename->setEnabled(true);
        btFileRename->setMinimumSize(QSize(90, 70));
        btFileRename->setMaximumSize(QSize(90, 70));
        btFileRename->setText(QString::fromUtf8("Rename"));

        verticalLayout->addWidget(btFileRename);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);


        horizontalLayout_4->addLayout(verticalLayout);


        retranslateUi(FileSelectFrm);

        QMetaObject::connectSlotsByName(FileSelectFrm);
    } // setupUi

    void retranslateUi(QWidget *FileSelectFrm)
    {
        label->setText(QString());
        btDeleteAll->setText(QApplication::translate("FileSelectFrm", "DeleteAll", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(FileSelectFrm);
    } // retranslateUi

};

namespace Ui {
    class FileSelectFrm: public Ui_FileSelectFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_FILESELECT_H

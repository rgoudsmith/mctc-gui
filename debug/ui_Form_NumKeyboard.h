/********************************************************************************
** Form generated from reading UI file 'Form_NumKeyboard.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_NUMKEYBOARD_H
#define UI_FORM_NUMKEYBOARD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NumKeyboardFrm
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *inputValue;
    QPushButton *bt7;
    QPushButton *bt8;
    QPushButton *bt9;
    QPushButton *btBack;
    QPushButton *bt4;
    QPushButton *bt5;
    QPushButton *bt6;
    QPushButton *btClear;
    QPushButton *bt1;
    QPushButton *bt2;
    QPushButton *bt3;
    QPushButton *btSign;
    QPushButton *bt0;
    QPushButton *btDot;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btOk;
    QPushButton *btCancel;

    void setupUi(QWidget *NumKeyboardFrm)
    {
        if (NumKeyboardFrm->objectName().isEmpty())
            NumKeyboardFrm->setObjectName(QString::fromUtf8("NumKeyboardFrm"));
        NumKeyboardFrm->setWindowModality(Qt::ApplicationModal);
        NumKeyboardFrm->resize(346, 450);
        NumKeyboardFrm->setCursor(QCursor(Qt::BlankCursor));
        NumKeyboardFrm->setWindowTitle(QString::fromUtf8("Numeric Keyboard"));
        verticalLayout_2 = new QVBoxLayout(NumKeyboardFrm);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox = new QGroupBox(NumKeyboardFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox{border: 5px solid black; border-radius: 7px;}"));
        verticalLayout = new QVBoxLayout(groupBox);
#ifndef Q_OS_MAC
        verticalLayout->setContentsMargins(9, 9, 9, 9);
#endif
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        inputValue = new QLabel(groupBox);
        inputValue->setObjectName(QString::fromUtf8("inputValue"));
        inputValue->setMinimumSize(QSize(0, 30));
        inputValue->setMaximumSize(QSize(16777215, 40));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        inputValue->setPalette(palette);
        QFont font;
        font.setPointSize(20);
        font.setBold(true);
        font.setWeight(75);
        inputValue->setFont(font);
        inputValue->setAutoFillBackground(false);
        inputValue->setStyleSheet(QString::fromUtf8("border: 1px solid black; border-radius: 3px; background:white"));
        inputValue->setFrameShape(QFrame::Box);
        inputValue->setText(QString::fromUtf8("0"));
        inputValue->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(inputValue, 0, 0, 1, 4);

        bt7 = new QPushButton(groupBox);
        bt7->setObjectName(QString::fromUtf8("bt7"));
        bt7->setMinimumSize(QSize(70, 70));
        bt7->setMaximumSize(QSize(70, 70));
        QFont font1;
        font1.setPointSize(30);
        font1.setBold(true);
        font1.setWeight(75);
        bt7->setFont(font1);
        bt7->setText(QString::fromUtf8("7"));

        gridLayout->addWidget(bt7, 1, 0, 1, 1);

        bt8 = new QPushButton(groupBox);
        bt8->setObjectName(QString::fromUtf8("bt8"));
        bt8->setMinimumSize(QSize(70, 70));
        bt8->setMaximumSize(QSize(70, 70));
        bt8->setFont(font1);
        bt8->setText(QString::fromUtf8("8"));

        gridLayout->addWidget(bt8, 1, 1, 1, 1);

        bt9 = new QPushButton(groupBox);
        bt9->setObjectName(QString::fromUtf8("bt9"));
        bt9->setMinimumSize(QSize(70, 70));
        bt9->setMaximumSize(QSize(70, 70));
        bt9->setFont(font1);
        bt9->setText(QString::fromUtf8("9"));

        gridLayout->addWidget(bt9, 1, 2, 1, 1);

        btBack = new QPushButton(groupBox);
        btBack->setObjectName(QString::fromUtf8("btBack"));
        btBack->setMinimumSize(QSize(70, 70));
        btBack->setMaximumSize(QSize(70, 70));
        btBack->setText(QString::fromUtf8("<---"));

        gridLayout->addWidget(btBack, 1, 3, 1, 1);

        bt4 = new QPushButton(groupBox);
        bt4->setObjectName(QString::fromUtf8("bt4"));
        bt4->setMinimumSize(QSize(70, 70));
        bt4->setMaximumSize(QSize(70, 70));
        bt4->setFont(font1);
        bt4->setText(QString::fromUtf8("4"));

        gridLayout->addWidget(bt4, 2, 0, 1, 1);

        bt5 = new QPushButton(groupBox);
        bt5->setObjectName(QString::fromUtf8("bt5"));
        bt5->setMinimumSize(QSize(70, 70));
        bt5->setMaximumSize(QSize(70, 70));
        bt5->setFont(font1);
        bt5->setText(QString::fromUtf8("5"));

        gridLayout->addWidget(bt5, 2, 1, 1, 1);

        bt6 = new QPushButton(groupBox);
        bt6->setObjectName(QString::fromUtf8("bt6"));
        bt6->setMinimumSize(QSize(70, 70));
        bt6->setMaximumSize(QSize(70, 70));
        bt6->setFont(font1);
        bt6->setText(QString::fromUtf8("6"));

        gridLayout->addWidget(bt6, 2, 2, 1, 1);

        btClear = new QPushButton(groupBox);
        btClear->setObjectName(QString::fromUtf8("btClear"));
        btClear->setMinimumSize(QSize(70, 70));
        btClear->setMaximumSize(QSize(70, 70));
        btClear->setText(QString::fromUtf8("Clear"));

        gridLayout->addWidget(btClear, 2, 3, 1, 1);

        bt1 = new QPushButton(groupBox);
        bt1->setObjectName(QString::fromUtf8("bt1"));
        bt1->setMinimumSize(QSize(70, 70));
        bt1->setMaximumSize(QSize(70, 70));
        bt1->setFont(font1);
        bt1->setText(QString::fromUtf8("1"));

        gridLayout->addWidget(bt1, 3, 0, 1, 1);

        bt2 = new QPushButton(groupBox);
        bt2->setObjectName(QString::fromUtf8("bt2"));
        bt2->setMinimumSize(QSize(70, 70));
        bt2->setMaximumSize(QSize(70, 70));
        bt2->setFont(font1);
        bt2->setText(QString::fromUtf8("2"));

        gridLayout->addWidget(bt2, 3, 1, 1, 1);

        bt3 = new QPushButton(groupBox);
        bt3->setObjectName(QString::fromUtf8("bt3"));
        bt3->setMinimumSize(QSize(70, 70));
        bt3->setMaximumSize(QSize(70, 70));
        bt3->setFont(font1);
        bt3->setText(QString::fromUtf8("3"));

        gridLayout->addWidget(bt3, 3, 2, 1, 1);

        btSign = new QPushButton(groupBox);
        btSign->setObjectName(QString::fromUtf8("btSign"));
        btSign->setMinimumSize(QSize(70, 70));
        btSign->setMaximumSize(QSize(70, 70));
        btSign->setText(QString::fromUtf8("+/-"));

        gridLayout->addWidget(btSign, 3, 3, 1, 1);

        bt0 = new QPushButton(groupBox);
        bt0->setObjectName(QString::fromUtf8("bt0"));
        bt0->setMinimumSize(QSize(70, 70));
        bt0->setMaximumSize(QSize(70, 70));
        bt0->setFont(font1);
        bt0->setText(QString::fromUtf8("0"));

        gridLayout->addWidget(bt0, 4, 1, 1, 1);

        btDot = new QPushButton(groupBox);
        btDot->setObjectName(QString::fromUtf8("btDot"));
        btDot->setMinimumSize(QSize(70, 70));
        btDot->setMaximumSize(QSize(70, 70));
        btDot->setFont(font1);
        btDot->setText(QString::fromUtf8("."));

        gridLayout->addWidget(btDot, 4, 2, 1, 1);


        verticalLayout->addLayout(gridLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btOk = new QPushButton(groupBox);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(70, 70));
        btOk->setText(QString::fromUtf8("Ok"));

        horizontalLayout->addWidget(btOk);

        btCancel = new QPushButton(groupBox);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("Cancel"));

        horizontalLayout->addWidget(btCancel);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_2->addWidget(groupBox);


        retranslateUi(NumKeyboardFrm);

        QMetaObject::connectSlotsByName(NumKeyboardFrm);
    } // setupUi

    void retranslateUi(QWidget *NumKeyboardFrm)
    {
        groupBox->setTitle(QString());
        Q_UNUSED(NumKeyboardFrm);
    } // retranslateUi

};

namespace Ui {
    class NumKeyboardFrm: public Ui_NumKeyboardFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_NUMKEYBOARD_H

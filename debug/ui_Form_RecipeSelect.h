/********************************************************************************
** Form generated from reading UI file 'Form_RecipeSelect.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_RECIPESELECT_H
#define UI_FORM_RECIPESELECT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "Form_FileSelect.h"

QT_BEGIN_NAMESPACE

class Ui_RecipeSelectFrm
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_7;
    QLabel *lbCaption;
    QSpacerItem *horizontalSpacer_8;
    FileSelectFrm *FileSelect;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btCancel;

    void setupUi(QWidget *RecipeSelectFrm)
    {
        if (RecipeSelectFrm->objectName().isEmpty())
            RecipeSelectFrm->setObjectName(QString::fromUtf8("RecipeSelectFrm"));
        RecipeSelectFrm->resize(849, 515);
        RecipeSelectFrm->setCursor(QCursor(Qt::BlankCursor));
        RecipeSelectFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout = new QVBoxLayout(RecipeSelectFrm);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_7);

        lbCaption = new QLabel(RecipeSelectFrm);
        lbCaption->setObjectName(QString::fromUtf8("lbCaption"));
        lbCaption->setMinimumSize(QSize(0, 45));
        lbCaption->setMaximumSize(QSize(16777215, 45));
        QFont font;
        font.setPointSize(35);
        font.setBold(true);
        font.setWeight(75);
        lbCaption->setFont(font);

        horizontalLayout_7->addWidget(lbCaption);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_8);


        verticalLayout->addLayout(horizontalLayout_7);

        FileSelect = new FileSelectFrm(RecipeSelectFrm);
        FileSelect->setObjectName(QString::fromUtf8("FileSelect"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(FileSelect->sizePolicy().hasHeightForWidth());
        FileSelect->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(FileSelect);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btCancel = new QPushButton(RecipeSelectFrm);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("Cancel"));

        horizontalLayout->addWidget(btCancel);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(RecipeSelectFrm);

        QMetaObject::connectSlotsByName(RecipeSelectFrm);
    } // setupUi

    void retranslateUi(QWidget *RecipeSelectFrm)
    {
        lbCaption->setText(QApplication::translate("RecipeSelectFrm", "Recipes", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(RecipeSelectFrm);
    } // retranslateUi

};

namespace Ui {
    class RecipeSelectFrm: public Ui_RecipeSelectFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_RECIPESELECT_H

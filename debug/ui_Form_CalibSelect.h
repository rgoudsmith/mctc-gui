/********************************************************************************
** Form generated from reading UI file 'Form_CalibSelect.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_CALIBSELECT_H
#define UI_FORM_CALIBSELECT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CalibSelectFrm
{
public:
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *btCalib;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btZero;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *btCheck;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_8;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_10;
    QLabel *lbLc;
    QLabel *lbLoadcell;
    QSpacerItem *horizontalSpacer_11;
    QSpacerItem *horizontalSpacer_9;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *btOk;

    void setupUi(QWidget *CalibSelectFrm)
    {
        if (CalibSelectFrm->objectName().isEmpty())
            CalibSelectFrm->setObjectName(QString::fromUtf8("CalibSelectFrm"));
        CalibSelectFrm->resize(664, 459);
        CalibSelectFrm->setCursor(QCursor(Qt::BlankCursor));
        CalibSelectFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout = new QVBoxLayout(CalibSelectFrm);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer = new QSpacerItem(20, 44, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        btCalib = new QPushButton(CalibSelectFrm);
        btCalib->setObjectName(QString::fromUtf8("btCalib"));
        btCalib->setMinimumSize(QSize(110, 90));
        btCalib->setMaximumSize(QSize(110, 90));
        btCalib->setText(QString::fromUtf8("Calibrate"));

        horizontalLayout_2->addWidget(btCalib);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        btZero = new QPushButton(CalibSelectFrm);
        btZero->setObjectName(QString::fromUtf8("btZero"));
        btZero->setMinimumSize(QSize(110, 90));
        btZero->setMaximumSize(QSize(110, 90));
        btZero->setText(QString::fromUtf8("Zero\n"
">0<"));

        horizontalLayout_3->addWidget(btZero);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);

        btCheck = new QPushButton(CalibSelectFrm);
        btCheck->setObjectName(QString::fromUtf8("btCheck"));
        btCheck->setMinimumSize(QSize(110, 90));
        btCheck->setMaximumSize(QSize(110, 90));
        btCheck->setText(QString::fromUtf8("Weight\n"
"Check"));

        horizontalLayout_4->addWidget(btCheck);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_6);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_8);

        groupBox = new QGroupBox(CalibSelectFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMinimumSize(QSize(400, 70));
        groupBox->setMaximumSize(QSize(400, 70));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        QBrush brush1(QColor(126, 126, 126, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        groupBox->setPalette(palette);
        groupBox->setAutoFillBackground(true);
        horizontalLayout_6 = new QHBoxLayout(groupBox);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_10);

        lbLc = new QLabel(groupBox);
        lbLc->setObjectName(QString::fromUtf8("lbLc"));
        QFont font;
        font.setPointSize(14);
        lbLc->setFont(font);
        lbLc->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        lbLc->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_6->addWidget(lbLc);

        lbLoadcell = new QLabel(groupBox);
        lbLoadcell->setObjectName(QString::fromUtf8("lbLoadcell"));
        lbLoadcell->setFont(font);
        lbLoadcell->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        lbLoadcell->setText(QString::fromUtf8("20kg / 50kg"));

        horizontalLayout_6->addWidget(lbLoadcell);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_11);


        horizontalLayout->addWidget(groupBox);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_9);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_7);

        btOk = new QPushButton(CalibSelectFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout_5->addWidget(btOk);


        verticalLayout->addLayout(horizontalLayout_5);


        retranslateUi(CalibSelectFrm);

        QMetaObject::connectSlotsByName(CalibSelectFrm);
    } // setupUi

    void retranslateUi(QWidget *CalibSelectFrm)
    {
        groupBox->setTitle(QString());
        lbLc->setText(QApplication::translate("CalibSelectFrm", "Loadcell:", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(CalibSelectFrm);
    } // retranslateUi

};

namespace Ui {
    class CalibSelectFrm: public Ui_CalibSelectFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_CALIBSELECT_H

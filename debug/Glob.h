/*===========================================================*/
/* File        : Glob.h                                      */
/* Description : Global routines and data sturcture class    */
/*               Accessible through the global pointer glob. */
/*                                                           */
/*               Example : glob->ActUnitIndex()              */
/*===========================================================*/
#ifndef GLOBALITEMS_H
#define GLOBALITEMS_H

#include <QNetworkInterface>
#include <QtGui/QWidget>
#include <QDateTime>
#include <QMessageBox>
#include <QPushButton>
#include <QTranslator>
#include <QDateTime>
#include <QProcess>
#include <QObject>
#include "ImageDef.h"
#include "UsbAccess.h"
#include "Com.h"

struct UnderBoardVersionStruct
{
  unsigned char SoftwareVersion;
  unsigned char HardwareVersion;
};

struct EventLogItemStruct
{
  unsigned long eventID;
  int msgType;
  unsigned int dateTim;
  int unitIdx;
  int alarmNr;
  float oldValue;
  float newValue;
};

struct PrimeCfg                                                                 // Prime config variables
{
  int time;
  float rpm;
};

struct PrimeSts                                                                 // Prime status variables
{
  bool activate;
  bool activeSts;                                                               // active status, is prime currently active y/n
  bool preCal;                                                                  // prime called from pre-cal y/n
  int timVal;                                                                   // value for timer
  int maxVal;                                                                   // max val for timer
};

struct TestVal                                                                  // Test function variables
{
  bool active;
  eCalCommand cmd;
};

struct CalStruct
{
  eCalCommand cmd;
  eCalStatus sts;
  unsigned short status;
  unsigned short command;
};

struct MaterialStruct
{
  QString name;
  bool isDefault;
};

struct PreCalStruct
{
  CalStruct calib;
  float colPct;
  float ShotWeight;
  float ShotTime;
  float ExtCap;
  float MaxTacho;
  float preCalDosAct;
  float rpmCorrFac;
  bool rpmCorrFacValid;
  MaterialStruct material;
};

struct FillLevelsStruct
{
  float LoLo;
  float Lo;
  float Hi;
  float HiHi;
};



struct RegrindParmStruct
{
    float levelHigh;                                                              /* Hihg level, default 4000 g */
    float levelRegrind;                                                           /* Regrind high level, default 3500 g */
    byte levelPct[MAX_REGR_LEVELS];                                               /* Regrind levels, default B0=0, B1=25, B2=50, B3=75 and B4=100 % */
    float regrindPctExtra;                                                        /* Extra regrind percentage (level 4), default 5 % */
    float regPctB1;                                                               /* Regrind Band 1 percentage, default 80 % */
    word fillTimeMax;                                                             /* Max. fill time, default 20 s */
    word fillInterval;                                                            /* Fill interval time, default 300 s */
};

struct FillSysStruct
{
  bool fillOnOff;
  eFillSys fillSysType;
  float alarmTime;
  float fillTimeME;
  float fillTimeMV;
  float emptyTime;
  bool alarmMode;
  int fillCycles;
  int fillAlarmCycles;
  float MVDelay;
  bool manualFill;
  bool knifeGateValve;
};

struct SysInjectCfgStruct
{
  float shotWeight;
  float shotTime;
  float meteringStart;
};

struct SysInjectStsStruct
{
  float shotTimeAct;
  float meteringValid;
  float meteringAct;
};

struct SysExtCfgStruct
{
  float extCapacity;
  float tachoMax;
};

struct SysExtStsStruct
{
  float extCapacityAct;
  float tachoAct;
};

struct SysAlarmStruct
{
  int nr;
  bool active;
  int mode;
  bool reset;
};

struct ManualIOStruct
{
  bool active;
  bool outputs[MAX_DIG_OUT];
  float motorSpeed;
};


/*----- Actual I/O status -----*/
struct ActualIOStruct
{
  bool outputs[MAX_DIG_OUT];
  bool inputs[MAX_DIG_IN];
};


struct LedStatusStruct
{
  eLedStatus alarm;
  eLedStatus input;
  eLedStatus power;
};

struct SystemStart
{
  bool start;
  bool execCmd;
  bool active;
  byte cmd;
  int ack;
  eSystemStatus sysActSts;
};

/*------ Regrind status data -----*/
typedef struct
{
  float regPctAct;                                                              /* Actual regrind % */
    byte regBandAct;                                                              /* Actual regrind band nr */
    float fillStartLevel;                                                         /* Fill start level [g] */
} tRegrindStatus;

/*------ McWeight status data -----*/
typedef struct
{
  float extCap;                                                                 /* McWeight cap kg/h (without add.)*/
  float extCapTotal;                                                            /* Total ext. cap. kg/h */
  float mcWeightPct;                                                            /* McWeight % of the total cap */
} tMcWeightStatus;

struct SysConfig
{
  /*-------------------------------------*/
  /* System configuration RAM structure  */
  /*-------------------------------------*/
  QString MC_ID;
  eMcDeviceType deviceType;
  eDosTool dosTool;
  eMotor motor;
  eGranulate granulate;
  FillSysStruct fillSystem;
  FillLevelsStruct fillLevels;
  bool sysEnabled;                                                              // Configuration if a unit enabled, and allowed to start at all. Configurable by user: (multi unit select form)
  float colPct;                                                                 // Color percentage
  float setRpm;                                                                 // Set RPM for grRPM production mode
  // Adv system Config
  eGraviRpm GraviRpm;
  float calibDev;                                                               // TODO: Check value type
  PrimeCfg prime;
  TestVal test;
  int mcWeightType;                                                             /* McWeight type (MCW50,MCW100,MCW1000) */
  SysInjectCfgStruct injection;
  SysExtCfgStruct extrusion;
  tCurveStruct curve;
  MaterialStruct material;
  SettingMvcStruct MvcSettings;
  RegrindParmStruct regrindParm;
};


struct SysStatus
{
  /*-------------*/
  /* Unit status */
  /*-------------*/
  eMCStatus MCStatus;
  float rpm;
  float mass;
  float fastMass;
  int fillSts;
  int motorSts;
  SystemStart sysActive;
  bool remoteStart;
  bool curveChanged;
  float actProd;
  float setProd;
  bool balanceCalibrated;
  bool actIsSet;
  float consumption;
  float consumptionDay;
  PrimeSts prime;
  SysInjectStsStruct injection;
  SysExtStsStruct extrusion;
  float productsToGo;
  PreCalStruct preCalibration;
  ProcessDiagStruct procDiagData;
  SysAlarmStruct alarmSts;
  ManualIOStruct manIO;
  ActualIOStruct actIO;
  tMcWeightStatus mcWeightSts;
  tRegrindStatus regrSts;
  UnderBoardVersionStruct uBoardVersion;
};

struct MasterConfig
{
  /*-------------------------------------*/
  /* Master configuration RAM structure  */
  /*-------------------------------------*/
  eLanguages language;                                                          /* Languages: UK = 0, NL = 1, DE = 2 */
  eMode mode;                                                                   /* Inj/Ext */
  eType inpType;                                                                /* Timer/Relay/Tacho/MVC_ID */
  bool autoStart;                                                               /* Y/N */
  bool rcpEnable;                                                               /* Y/N */
  bool keyBeep;                                                                 /* Y/N */
  bool dateSet;                                                                 /* Y/N -> date/time has been changed via Qt */
  int wghtUnits;                                                                // TODO: create Enum
  int slaveCount;                                                               /* Number of connected MC devices */
  bool twinMode;                                                                /* Y/N */
  eUserType defLogin;
  QString SuperPasswd;
  QString ToolPasswd;
  QString recipe;
  int IPAddress[4];                                                             /* IP address*/
  int Netmask[4];                                                               /* Netmask */
  int Gateway[4];                                                               /* Gateway */
  int modbusAddr;                                                               /* Modbus device address */
  QDateTime sysTime;
  eCalState calState;
  SettingMvcMstStruct MvcSettings;
  bool motorAlarmEnb;                                                           /* Y/N */
  bool profibusEnb;
  bool imperialMode;
  bool units_kgh;                                                               /* Y/N */
  float shortMeasTime;                                                          /* McWeight short measurement time */
  float hysterese;                                                              /* McWeight hysterese */
  float tachoRatio;                                                             /* McWeight tacho ratio */
  float minExtCap;                                                              /* Minimum extruder cap. kg/h */
  bool canWdDis;                                                                /* Canbus WD disable */
};

struct MasterStatus
{
  bool isTwin;
  int actUnitIndex;
  bool sysStart;                                                                // Start = commando
  bool sysActive;                                                               // Active = actual status of the system (is system realy active?)
  bool popupActive;
  bool primeAvailable;                                                          // for GUI interface (is prime button allowed to display y/n
  int inputSts;
  eBuzzerStatus buzzerCtrl;
  LedStatusStruct ledStatus;
  bool learnOffLineActive;                                                      // for McWeight multi unit system

};

struct ImageStruct
{
  QString imgPath;
  QString imgText;
  int fontSize;
  int xOffset,yOffset;
};

struct ItemFormatStruct
{
  int fieldWidth;
  QChar format;
  int precision;
};

struct FormatStruct
{
  ItemFormatStruct WeightFmt;
  ItemFormatStruct ShotWeightFmt;
  ItemFormatStruct TimeFmt;
  ItemFormatStruct ExtruderFmt;
  ItemFormatStruct TachoFmt;
  ItemFormatStruct PercentageFmt;
  ItemFormatStruct LevelFmt;
  ItemFormatStruct RpmFmt;
  ItemFormatStruct DosageFmt;
  ItemFormatStruct DosageKgHFmt;
  ItemFormatStruct CalDevFmt;
  ItemFormatStruct AlarmPctFmt;
};

class global : public QObject
{
  Q_OBJECT
public:
  global(QObject * parent = 0);
  ~global();
  QWidget *menu;                                                              // a pointer to the mainMenu widget.
  QProcess * DataProc;
  QPalette * Palette();
  QString * GetImagePath(int,int);                                            // Function to return the full path of a given image (imgType,imgIndex)
  QString * AppPath();                                                        // Function to return the current path to the application executable. "/opt/MovaColor/"
  QString fileSeparator;                                                      // System file seperator. (In linux "/" is used)
  QString selectedRecipe;                                                     // Placeholder for sending the selected recipe from RecipeSelectForm to RecipeForm. "" means: New Recipe.

  void SetAppPath(QString*);
  void GetDefaultMaterialName(QString *,int, bool extract = false);           // Returns the default material (display)name of unit with idx (int), according to configuration (cylinder type/granulate type). Value is placed in QString *.
  void GetDefaultMaterial(QString *,int);                                     // Returns the default material (file)name of unit with idx(int), according to configuration (cylinder type,granulate type). Value is placed in QString *.
  void ClearImages();
  void LanguageUpdate();

  /*-----These sturctures are defined as static for global access (ModbusSever) -----*/
  static SysStatus sysSts[CFG_MAX_UNIT_COUNT];                                // System status structure (array)
  static SysConfig sysCfg[CFG_MAX_UNIT_COUNT];                                // System configuration structures (array)
  static MasterStatus mstSts;                                                 // Master status structure
  static MasterConfig mstCfg;                                                 // Master configuration structure

  FormatStruct formats;                                                       // Formatting information (array) for display values
  UsbAccess * usbStick;

  QString extrUnits;
  QString shotUnits;
  QString timerUnits;
  QString weightUnits;
  QString tachoUnits;
  QString rpmUnits;
  QNetworkInterface netwInt;

  void SetButtonImage(QPushButton*_btn,int imgType,int imgIdx,int borderPx = IMG_PIX_BORDER);
  void SetLabelImage(QLabel* _lbl,int imgType,int imgIdx);
  void TriggerUpdate(int);
  void SetUser(eUserType,bool startup = false);
  eUserType GetUser();
  eCalState GetCalState();
  void GetDispMaterial(QString *, int);
  void GetDispMaterial(QString *);
  void GetEnumeratedText(eEnumType,int,QString *);
  void GetAlarmText(int,float,QString *,bool);
  int ActUnitIndex();
  void SysCfg_SetKnifeGate(eKnifeGate kn,int idx);
  void MstCfg_SetMcwType(eMcwtype mcwType,int idx);
  void MstCfg_SetBalHOType(eBalHOtype balHOType,int idx);

  // Event handling
  int GetEventCount();
  EventLogItemStruct* GetEventByIdx(int);
  EventLogItemStruct* GetEventById(unsigned long);
  void GetEventText(int,int,float,float,QString *);

  QFont * baseFont;
  QFont * captionFont;
  QFont * configFont;

  // TODO: CalStruct moet nog per unit uitgevoerd worden.
  CalStruct calStruct;
  void SetTranslator(QTranslator *trans);

  void SigSendUdpData();
  void SignalSaveConfigToFile(int idx);
  void SetMcwDefaults(eMcwtype mcwType, int idx);
  void SetBalHODefaults(eBalHOtype balHOtype, int idx);

private:
  QTranslator *translator;
  QString * _appPath;
  QPalette *_palette;
  QList<ImageStruct*> _list;
  QString * ImagePaths(int);

  QList<EventLogItemStruct*> eventList;
  unsigned long eventId;
  int dbgCounter;
  int dummy[4];
  int sendInterval;

  QTimer * dateTim;
  QTimer * stsTim;
  QTimer * comTim;
  Com *com;
  eUserType currentUser;

  void LoadImages();
  int GetImageCount(int,int);
  void SetCorrectUnits();
  void SetPreCalStatus(unsigned char, int);
  void SetMinMaxDefaults();
  byte SystemStartHandler(int);
  void SetSystemStatus(int);

signals:
  /* --- System Config signals --- */
  // System config production configuration
  void SysCfg_RpmChanged(float _rpm, int);
  void SysCfg_ColorPctChanged(float, int);
  void SysCfg_DosSetChanged(float, int);
  void SysCfg_ShotWeightChanged(float, int);
  void SysCfg_ShotTimeChanged(float, int);
  void SysCfg_MeteringStartChanged(float,int);
  void SysCfg_ExtCapChanged(float, int);
  void SysCfg_TachoMaxChanged(float, int);
  // Sys Config unit configuration
  void SysCfg_IDChanged(const QString &_id, int);
  void SysCfg_MaterialChanged(const QString &str, int);
  void SysCfg_DosToolChanged(eDosTool, int);
  void SysCfg_MotorChanged(eMotor, int);
  void SysCfg_GranulateChanged(eGranulate,int);
  void SysCfg_LoadCellChanged(int,int);
  void SysCfg_GraviRpmChanged(eGraviRpm,int);
  // Prime Configuration signals
  void SysCfg_PrimeRpmChanged(float,int);
  void SysCfg_PrimeTimeChanged(int,int);
  // Save to file
  void SysCfg_SaveConfigToFile(int);

  /* --- System Status signals --- */
  // Production signals
  void SysSts_RpmChanged(float _rpm, int);
  void SysSts_MassChanged(float _mass, int);
  void SysSts_FastMassChanged(float _mass, int);
  void SysSts_FillChanged(int _fillSts, int);
  void SysSts_MotorStsChanged(int _motorSts, int);
  void SysSts_MCStatusChanged(eMCStatus _sts, int);
  void SysSts_DosActChanged(float, int);
  void SysSts_ShotTimeChanged(float,int);
  void SysSts_MeteringActChanged(float,int);
  void SysSts_MeteringValidChanged(bool,int);
  void SysSts_ExtCapChanged(float, int);
  void SysSts_TachoChanged(float, int);
  void SysSts_ProdToGoChanged(float,int);
  void SysSts_LearnStsChanged(bool,int);
  // Prime status signals
  void SysSts_PrimeActiveChanged(bool,int);
  void SysSts_PrimeStatusChanged(bool,int);
  // Material Calibration status
  void SysSts_PreCalDosActChanged(float,int);
  void SysSts_PreCalStatusChanged(unsigned char,int);
  void SysSts_SysActiveChanged(bool,int);
  void SysSts_SysEnabledChanged(bool,int);
  void SysSts_McWeightCapChanged(float, float, float, int);

  /* --- Global Config signals --- */
  // Global Production settings
  void MstCfg_ExtUnitsChanged(const QString &_str);
  void MstCfg_ShotUnitsChanged(const QString &_str);
  void MstCfg_TimeUnitsChanged(const QString &_str);
  void MstCfg_WeightUnitsChanged(const QString &_str);
  void MstCfg_RpmUnitsChanged(const QString &_str);
  void MstCfg_TachoUnitsChanged(const QString &_str);
  void MstCfg_UserChanged(eUserType);
  void MstCfg_RecipeChanged(const QString &);
  void MstCfg_SlaveCountChanged(int);
  // ProcesLogItemAddedAdvanced Configuration settings
  void MstCfg_LanguageChanged(eLanguages);
  void MstCfg_ProdModeChanged(eMode);
  void MstCfg_InputTypeChanged(eType);
  void MstCfg_AutoStartChanged(bool);
  void MstCfg_RcpEnabledChanged(bool);
  void MstCfg_DefaultUserChanged(eUserType);
  void MstCfg_SuperPassChanged(const QString &);
  void MstCfg_ToolPassChanged(const QString &);
  void MstCfg_IpAddressChanged(int,int,int,int);
  void MstCfg_ModbusAddrChanged(int);
  void MstCfg_SystemTimeChanged(const QDateTime &);
  // Save to file
  void MstCfg_SaveConfigToFile();

  void MvcCfg_LogIntervalChanged(int,int);

  /* --- Global Status signals --- */
  void MstSts_ActUnitChanged(int);
  void MstSts_LedStatusChanged(LedStatusStruct);
  void MstSts_SysStartChanged(bool);
  void MstSts_SysActiveChanged(bool);

  // FillSystem configuration
  void CfgFill_OnOffChanged(bool,int);
  void CfgFill_FillSysChanged(eFillSys,int);
  void CfgFill_AlmTimeChanged(float,int);
  void CfgFill_TimeMeChanged(float,int);
  void CfgFill_TimeMvChanged(float,int);
  void CfgFill_EmptyTimeChanged(float,int);
  void CfgFill_AlarmModeChanged(bool,int);
  void CfgFill_FillCyclesChanged(int,int);
  void CfgFill_FillAlarmCyclesChanged(int,int);
  void CfgFill_MVDelayChanged(float,int);
  void CfgFill_ManualFillChanged(bool,int);

  // All levels in one signal
  void SysCfg_HopperLevelsChanged(float,float,float,float,int);

  // Advanced Configuration signals
  void SysCfg_DevAlarmChanged(float,int);
  void SysCfg_CalDevChanged(float,int);

  void SysSts_AlarmStatusChanged(bool,int);
  void SysSts_AlarmModeChanged(int,int);

  // Communication signals
  void ComReceived(tMsgControlUnit,int);

  // Other signals
  void CalState(eCalState);
  void CalStatus(eCalStatus);

  void AlarmReset(int);
  void AlarmResetAckRcv(int);

  // EventLog signals
  void EventNewItem(unsigned long);

  void SigSendUdp();

public slots:
  void StsTimElapsed();
  void SetMinMaxDefaults(int);
  void SysCfg_MvcSettingsFactoryDefaults(int);
  // Current Status slots                                 // Per unit
  void SysSts_SetRpm(float, int);
  void SysSts_SetMass(float, int);
  void SysSts_SetFastMass(float,int);
  void SysSts_SetFillStatus(int, int);
  void SysSts_SetMotorStatus(int, int);
  void SysSts_SetMCStatus(eMCStatus _sts, byte actIsSet, int idx);
  void SysSts_SetDosAct(float, int);
  void SysSts_SetShotTime(float,int);
  void SysSts_SetMeteringAct(float,int);
  void SysSts_SetMcWeightCap(float,float,float,int);
  void SysSts_SetRegrindStatus(float,byte,float,int);
  void SysSts_SetMeteringValid(bool,int);
  void SysSts_SetExtCap(float,int);
  void SysSts_SetTacho(float,int);
  void SysSts_SetProdToGo(float,int);
  void SysSts_SetLearnSts(bool,int);
  void SysSts_SetSysStart(bool,int);
  void SysSts_SetSysActive(bool,int);
  void SysSts_SetSysStartAck(int,int);
  void SysSts_SetAlarmNr(int,int);
  void SysSts_SetAlarmActive(bool,int);
  void SysSts_SetAlarmMode(int,int);
  void SysSts_SetPrimeStatus(bool,int);

  // System production settings slots
  void SysCfg_SetRpm(float,int);
  void SysCfg_SetID(const QString &_id, int);
  void SysCfg_SetColPct(float, int);
  void SysCfg_SetDosSet(float, int);
  void SysCfg_SetMaterial(const QString &_mat, bool, int, bool loadFromFile = true);
  void SysCfg_SetShotWeight(float, int);
  void SysCfg_SetShotTime(float, int);
  void SysCfg_SetMeteringStart(float,int);
  void SysCfg_SetExtCap(float, int);
  void SysCfg_SetMaxTacho(float, int);

  // Current system configuration slots                          // Per unit
  void SysCfg_SetDosTool(eDosTool,int);
  void SysCfg_SetMotorType(eMotor,int);
  void SysCfg_SetGranulate(eGranulate,int);
  void SysCfg_SetLoadcellType(int,int);

  // Advanced System Configuration slots (Adv Config 1)   // Per unit
  void SysCfg_SetGraviRpm(eGraviRpm, int);
  void SysCfg_SetDevAlarm(float, int);
  void SysCfg_SetCalDev(float, int);
  void SysCfg_SetSysEnabled(bool,int);
  void SysCfg_AlarmConfig(int,int,int);

  // System status
  void MstSts_SetNextUnitIndex();
  void MstSts_SetPrevUnitIndex();
  void MstSts_SetActUnitIndex(int);
  void MstSts_SetCalState(eCalState);
  void MstSts_SetPrimeAvailable(bool);
  void MstSts_SetSysActive(bool);
  void MstSts_SetSysStart(bool);
  void MstSts_CheckSysActive(bool,int);

  void MstCfg_SetExtUnits(const QString &_units);
  void MstCfg_SetShotUnits(const QString &_units);
  void MstCfg_SetTimeUnits(const QString &_units);
  void MstCfg_SetWeightUnits(const QString &_units);
  void MstCfg_SetRpmUnits(const QString &_units);
  void MstCfg_SetTachoUnits(const QString &_units);
  void MstCfg_SetRecipe(const QString &);
  void MstCfg_SetSlaveCount(int);
  void MstCfg_SetTachoRatio(float);

  // MovaColor/Agent settings
  void MvcCfg_SetLogInterval(int,int);

  // Prime Configuration slots
  void SysCfg_SetPrimeTime(int,int);
  void SysCfg_SetPrimeRpm(float,int);
  void SysSts_SetPrimeActive(bool, int, bool preCal = false);
  void SysCfg_SetHopperLevels(float,float,float,float,int);

  // Fill System slots
  void CfgFill_SetOnOff(bool,int);
  void CfgFill_SetFillSys(eFillSys,int);
  void CfgFill_SetAlarmTime(float,int);
  void CfgFill_SetFillTimeMe(float,int);
  void CfgFill_SetFillTimeMv(float,int);
  void CfgFill_SetEmptyTime(float,int);
  void CfgFill_SetAlarmMode(bool,int);
  void CfgFill_SetFillCycles(int,int);
  void CfgFill_SetFillAlarmCycles(int,int);
  void CfgFill_SetMVDelay(float,int);
  void CfgFill_SetManualFill(bool,int);

  // Master Configuration slots (Adv Convig 2);
  void MstCfg_SetLanguage(eLanguages);
  void MstCfg_SetProdMode(eMode);
  void MstCfg_SetInpType(eType);
  void MstCfg_SetAutoStart(bool);
  void MstCfg_SetRecipeEnable(bool);
  void MstCfg_MotorAlarmEnb(bool);
  void MstCfg_ProfibusEnb(bool);
  void MstCfg_ImperialMode(bool);
  void MstCfg_KgH_Mode(bool);
  void MstCfg_SetTwinMode(bool);
  void MstCfg_SetDefaultLogin(eUserType);
  void MstCfg_SetSuperPass(const QString &);
  void MstCfg_SetToolPass(const QString &);

  void MstCfg_SetModbusAddr(int);
  void MstCfg_SetKeyBeep(bool);
  void MstCfg_SetSystemTime(const QDateTime &);                               // Set current application time
  void MstCfg_UpdateSystemTime(const QString &);                              // Update system time

  // Communication slots
  void ComSendData();

  // Calibration slot
  void CalSendCommand(eCalCommand);
  void PreCalSendCommand(eCalCommand,int);
  void StsSetPreCalDosAct(float,int);

  void AlarmResetRcv(int);
  void SysCfg_SaveMaterial(int, const QString &, bool silent = false);
  void MstCfg_SaveSettings();
  void SysCfg_SaveSettings(int);
  void MstCfg_LoadedFromFile();
  void SysCfg_LoadedFromFile(int);
  void FactoryDefaultsExecuted();
  void RebuildProdScreen();

  int Proc_GetMaterialDosTool(const QString &,bool);
  int Proc_LoadMaterial(const QString &, bool, tCurveStruct *);

  // Event log handling
  void ClearEventList();
  void AddEventItem(int,int,int,float,float);

private slots:
  void GBI_UpdateDateTime();
  void GBI_LoadMaterialFromFile(int);
  void ComMsgReceived(tMsgControl);
  //    void ComError(int);
};
#endif                                                                          // GLOBALITEMS_H

/********************************************************************************
** Form generated from reading UI file 'Form_RecipeItem.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_RECIPEITEM_H
#define UI_FORM_RECIPEITEM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RecipeItemFrm
{
public:
    QHBoxLayout *horizontalLayout;
    QLabel *lbDevID;
    QLineEdit *edMaterial;
    QLineEdit *edColPct;

    void setupUi(QWidget *RecipeItemFrm)
    {
        if (RecipeItemFrm->objectName().isEmpty())
            RecipeItemFrm->setObjectName(QString::fromUtf8("RecipeItemFrm"));
        RecipeItemFrm->resize(532, 70);
        RecipeItemFrm->setCursor(QCursor(Qt::BlankCursor));
        RecipeItemFrm->setWindowTitle(QString::fromUtf8("Form"));
        RecipeItemFrm->setStyleSheet(QString::fromUtf8("#lbDevID{ border: 1px solid; border-radius: 3px; }"));
        horizontalLayout = new QHBoxLayout(RecipeItemFrm);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lbDevID = new QLabel(RecipeItemFrm);
        lbDevID->setObjectName(QString::fromUtf8("lbDevID"));
        lbDevID->setMinimumSize(QSize(200, 70));
        lbDevID->setMaximumSize(QSize(200, 70));
        lbDevID->setFocusPolicy(Qt::NoFocus);
        lbDevID->setStyleSheet(QString::fromUtf8(""));
        lbDevID->setText(QString::fromUtf8(""));
        lbDevID->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(lbDevID);

        edMaterial = new QLineEdit(RecipeItemFrm);
        edMaterial->setObjectName(QString::fromUtf8("edMaterial"));
        edMaterial->setMinimumSize(QSize(200, 70));
        edMaterial->setMaximumSize(QSize(200, 70));
        edMaterial->setFocusPolicy(Qt::NoFocus);
        edMaterial->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(edMaterial);

        edColPct = new QLineEdit(RecipeItemFrm);
        edColPct->setObjectName(QString::fromUtf8("edColPct"));
        edColPct->setMinimumSize(QSize(120, 70));
        edColPct->setMaximumSize(QSize(120, 70));
        edColPct->setFocusPolicy(Qt::NoFocus);
        edColPct->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(edColPct);


        retranslateUi(RecipeItemFrm);

        QMetaObject::connectSlotsByName(RecipeItemFrm);
    } // setupUi

    void retranslateUi(QWidget *RecipeItemFrm)
    {
        Q_UNUSED(RecipeItemFrm);
    } // retranslateUi

};

namespace Ui {
    class RecipeItemFrm: public Ui_RecipeItemFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_RECIPEITEM_H

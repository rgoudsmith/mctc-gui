#ifndef MULTIUNITSELECTFRM_H
#define MULTIUNITSELECTFRM_H

#include <QWidget>
#include "Form_QbaseWidget.h"
#include "Form_MultiComponentItem.h"
#include "Form_NumericInput.h"

namespace Ui
{
class MultiUnitSelectFrm;
}


class MultiUnitSelectFrm : public QBaseWidget
{
  Q_OBJECT

public:
  explicit MultiUnitSelectFrm(QWidget *parent = 0);
  ~MultiUnitSelectFrm();
  void FormInit();
  void SetInfoText();
  void UpdateProcesData();


  // Labels
  //    QLabel * lbText1;
  //    QLabel * lbMcWeightTotCap;

protected:
  bool eventFilter(QObject *,QEvent *);
  void resizeEvent(QResizeEvent *);
  void changeEvent(QEvent *e);
  void showEvent(QShowEvent *e);
  void hideEvent(QHideEvent *e);

private:
  Ui::MultiUnitSelectFrm *ui;
  NumericInputFrm * numInput;

  MultiComponentItemFrm * unitList[CFG_MAX_UNIT_COUNT];
  int unitCnt;
  int unitIdx;

  void InitItems();
  void ClearItems();

public slots:
  void SlaveCountChanged(int);
  void SysActiveChanged(bool,int);
  void McWeightCapChanged(float, float, float, int);
  void ColPctClickReceived(int);
  void ValueReceived(float);
  void ComMsgReceived(tMsgControlUnit,int);

};
#endif                                                                          // MULTIUNITSELECTFRM_H

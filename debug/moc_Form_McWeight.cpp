/****************************************************************************
** Meta object code from reading C++ file 'Form_McWeight.h'
**
** Created: Thu Jan 31 11:21:27 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_McWeight.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_McWeight.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_McWeightFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      42,   12,   12,   12, 0x08,
      54,   12,   12,   12, 0x08,
      70,   12,   12,   12, 0x08,
      90,   12,   12,   12, 0x08,
     111,   12,   12,   12, 0x08,
     135,  133,   12,   12, 0x0a,
     177,  171,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_McWeightFrm[] = {
    "McWeightFrm\0\0SysCfg_SaveConfigToFile(int)\0"
    "OkClicked()\0CancelClicked()\0"
    "ManualFillPressed()\0ManualFillReleased()\0"
    "FillSettingsClicked()\0,\0"
    "ComMsgReceived(tMsgControlUnit,int)\0"
    "value\0ValueReturned(float)\0"
};

const QMetaObject McWeightFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_McWeightFrm,
      qt_meta_data_McWeightFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &McWeightFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *McWeightFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *McWeightFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_McWeightFrm))
        return static_cast<void*>(const_cast< McWeightFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int McWeightFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: SysCfg_SaveConfigToFile((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: OkClicked(); break;
        case 2: CancelClicked(); break;
        case 3: ManualFillPressed(); break;
        case 4: ManualFillReleased(); break;
        case 5: FillSettingsClicked(); break;
        case 6: ComMsgReceived((*reinterpret_cast< tMsgControlUnit(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: ValueReturned((*reinterpret_cast< float(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void McWeightFrm::SysCfg_SaveConfigToFile(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

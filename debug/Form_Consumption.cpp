#include "Form_Consumption.h"
#include "ui_Form_Consumption.h"
#include "Form_MainMenu.h"
#include "Rout.h"

ConsumptionFrm::ConsumptionFrm(QWidget *parent):QBaseWidget(parent),ui(new Ui::ConsumptionFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);
}


ConsumptionFrm::~ConsumptionFrm()
{
  delete ui;
}


void ConsumptionFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}


void ConsumptionFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  /*----- Setup fonts -----*/
  ui->lblDeviceId->setFont(*(glob->baseFont));
  ui->lblUnitName->setFont(*(glob->baseFont));

  ui->lblBatchConsumption->setFont(*(glob->baseFont));
  ui->txtBatchConsumption->setFont(*(glob->baseFont));

  ui->lblTotalConsumption->setFont(*(glob->baseFont));
  ui->txtTotalConsumption->setFont(*(glob->baseFont));

  ui->lblResetBatch->setFont(*(glob->baseFont));
  ui->lblResetTotal->setFont(*(glob->baseFont));

  /*----- Setup button images -----*/
  glob->SetButtonImage(ui->btBatchReset,itSelection,selOk);
  glob->SetButtonImage(ui->btTotalReset,itSelection,selOk);

  /*----- Link buttons to routines -----*/
  connect(ui->btBatchReset,SIGNAL(clicked()),this,SLOT(BtBatchResetClicked()));
  connect(ui->btTotalReset,SIGNAL(clicked()),this,SLOT(BtTotalResetClicked()));
}


void ConsumptionFrm::showEvent(QShowEvent *e)
{
  /*-----------------------------------------------------*/
  /* Show window event handler                           */
  /*-----------------------------------------------------*/
  QWidget::showEvent(e);

  /*----- Hide reset buttons at operator level -----*/
  if (glob->GetUser() == utOperator) {
    ui->lblResetBatch->setVisible(false);
    ui->btBatchReset->setVisible(false);
    ui->lblResetTotal->setVisible(false);
    ui->btTotalReset->setVisible(false);
  }
  else {
    ui->lblResetBatch->setVisible(true);
    ui->btBatchReset->setVisible(true);
    ui->lblResetTotal->setVisible(true);
    ui->btTotalReset->setVisible(true);
  }

  /*----- Connect data refresh to msg received -----*/
  connect(glob,SIGNAL(ComReceived(tMsgControlUnit,int)),this,SLOT(ConsumptionDataReceived(tMsgControlUnit,int)));
}


void ConsumptionFrm::hideEvent(QHideEvent *e)
{
  /*---------------------------------------------*/
  /* Hide window event handler                   */
  /*---------------------------------------------*/
  QWidget::hideEvent(e);

  /*----- Dis-connect data refresh to msg received -----*/
  disconnect(glob,SIGNAL(ComReceived(tMsgControlUnit,int)),this,SLOT(ConsumptionDataReceived(tMsgControlUnit,int)));
}


void ConsumptionFrm::ConsumptionDataReceived(tMsgControlUnit data,int idx)
{
  /*-------------------------------------------*/
  /* Update the consumption data on the screen */
  /*-------------------------------------------*/
  if (idx == glob->ActUnitIndex()) {

    /*----- Unit name -----*/
    ui->lblUnitName->setText(glob->sysCfg[idx].MC_ID);

    /*----- Batch consumption -----*/
    ui->lblBatchConsumption->setText(QString::number(data.consumptionDay,'f',3) + " kg");

    /*------ Total consumption -----*/
    ui->lblTotalConsumption->setText(QString::number(data.consumption,'f',3) + " kg");
  }
}


void ConsumptionFrm::BtBatchResetClicked()
{
  /*-------------------------------------------*/
  /*  BUTTON : Reset batch consumption clicked */
  /*-------------------------------------------*/
  int i;
  i = glob->ActUnitIndex();
  Rout::rstCons[i].batch = true;
}


void ConsumptionFrm::BtTotalResetClicked()
{
  /*-------------------------------------------*/
  /*  BUTTON : Reset total consumption clicked */
  /*-------------------------------------------*/
  int i;
  i = glob->ActUnitIndex();
  Rout::rstCons[i].total = true;
}

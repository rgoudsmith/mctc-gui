#include "Form_AlarmConfig.h"
#include "ui_Form_AlarmConfig.h"
#include "Form_AlarmConfigItem.h"
#include "Form_MainMenu.h"
#include "QmVcScrollbar.h"
#include "Rout.h"

bool configItemsAdded;                                                          /* One shot flag for adding config items to list */

AlarmConfigFrm::AlarmConfigFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::AlarmConfigFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);
  layout = new QVBoxLayout(ui->scrollAreaWidgetContents);

  QMvcScrollBar * bar = new QMvcScrollBar(ui->scrollArea);
  connect(bar,SIGNAL(MouseButtonPressed()),this,SLOT(KeyBeep()));
  ui->scrollArea->setVerticalScrollBar(bar);

  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
}


AlarmConfigFrm::~AlarmConfigFrm()
{
  /*------------*/
  /* Destructor */
  /*------------*/
  delete ui;
}


void AlarmConfigFrm::resizeEvent(QResizeEvent *e)
{
  QBaseWidget::resizeEvent(e);
}


void AlarmConfigFrm::FormInit()
{
  /*---------------------------------------*/
  /* Setup the  pointer to the globalitems */
  /*---------------------------------------*/
  QBaseWidget::ConnectKeyBeep();;

  ui->lbTitle->setFont(*(glob->captionFont));
  glob->SetButtonImage(ui->btOk,itSelection,selOk);

  configItemsAdded = false;                                                     /* Enable add config items to list */
}


void AlarmConfigFrm::showEvent(QShowEvent *e)
{
  /*--------------------------------------------------------*/
  /* Show event : The alarm config screen has become active */
  /*--------------------------------------------------------*/
  QWidget::showEvent(e);

  /*----- Force unit idx to unit 0, beacuse at this moment the unit 0 alarm config
          settings are global for the whole system -----*/
//  glob->MstSts_SetActUnitIndex(0);

  /*----- Mag maar een keer gebeuren, knullige one shot methode, kijk of dit beter kan !! -----*/
  if(!configItemsAdded) {
    /*----- Create all alarm/warning configuration items -----*/
    AddConfigItems();
    configItemsAdded = true;
  }
}


void AlarmConfigFrm::AddConfigItems()
{
  /*-------------------------------------------------------*/
  /* Add items (alarm/warning) to a listView on the screen */
  /*-------------------------------------------------------*/
  AlarmConfigItemFrm * item;
  QString txt = "";

  for (int i=1;i<MAX_USER_ALM;i++) {
    txt = "";

    /* Skip Quick Response alarm, which is not available as user alarm but is linked to the normal Deviation alarm */
    if (i != ALM_INDEX_DEV_QR) {

      /* Create new item */
      item = new AlarmConfigItemFrm(this);

      /* Set alarm index */
      item->SetAlarmNr(i);

      /* Get the corresponding text from the text list. (AlarmHandler object) */
      switch(i) {
        case ALM_INDEX_FILL_SYSTEM:
          glob->GetAlarmText(ALM_INDEX_FILL_SYSTEM,0,&txt,false);
          break;
        case ALM_INDEX_LO_LEVEL:
          glob->GetAlarmText(ALM_INDEX_LO_LEVEL,0,&txt,false);
          break;
        case ALM_INDEX_LO_LO_LEVEL:
          glob->GetAlarmText(ALM_INDEX_LO_LO_LEVEL,0,&txt,false);
          break;
        case ALM_INDEX_HI_HI_LEVEL:
          glob->GetAlarmText(ALM_INDEX_HI_HI_LEVEL,0,&txt,false);
          break;
        case ALM_INDEX_MIN_MOTOR_SPEED:
          glob->GetAlarmText(ALM_INDEX_MIN_MOTOR_SPEED,0,&txt,false);
          break;
        case ALM_INDEX_MAX_MOTOR_SPEED:
          glob->GetAlarmText(ALM_INDEX_MAX_MOTOR_SPEED,0,&txt,false);
          break;
        case ALM_INDEX_DEV:
          glob->GetAlarmText(ALM_INDEX_DEV,0,&txt,false);
          break;
        case ALM_INDEX_MASTER_SLAVE_COM:
          glob->GetAlarmText(ALM_INDEX_MASTER_SLAVE_COM,0,&txt,false);
          break;
        default:
          break;
      }

      /* Set the text to the item */
      item->SetAlarmDescription(txt);

      /* Set current mode. Default = false (=warning) */
      bool alm = false;

      alm = (glob->sysCfg[glob->ActUnitIndex()].MvcSettings.alarm.almConfig[i].actVal == 1 ? true : false);
      /* Set actual mode to the item */
      item->SetAlarmMode(alm,false);

      /* Set glob link of the item */
      item->FormInit();

      /* Add item to the graphical layout */
      layout->addWidget(item);

      /* Set item pointer to 0 */
      item = 0;
    }
  }
}


int AlarmConfigFrm::ItemCount()
{
  return (MAX_USER_ALM-1);
}


void AlarmConfigFrm::OkClicked()
{
  /*-------------------*/
  /* OK Button clicked */
  /*-------------------*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiBack);
}

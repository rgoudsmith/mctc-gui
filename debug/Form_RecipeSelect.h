#ifndef RECIPESELECTFRM_H
#define RECIPESELECTFRM_H

#include <QWidget>
#include "Form_QbaseWidget.h"

namespace Ui
{
  class RecipeSelectFrm;
}


class RecipeSelectFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit RecipeSelectFrm(QWidget *parent = 0);
    ~RecipeSelectFrm();
    void FormInit();
    void showEvent(QShowEvent *);

  private:
    Ui::RecipeSelectFrm *ui;
    void GetDefaultRecipeFolder(QString*);

  protected:
    void changeEvent(QEvent *);

  private slots:
    void CancelClicked();
    void SelectClicked(const QString &);
    void NewRecipeClicked();
    void RecipeRenamed(const QString &);

  public slots:
    void RefreshFileList();
    void ShowWithRecipe(const QString &);

    signals:
    void SelectedItem(const QString &);

};
#endif                                                                          // RECIPESELECTFRM_H

/****************************************************************************
** Meta object code from reading C++ file 'Form_Calibration.h'
**
** Created: Thu Jan 31 11:20:24 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_Calibration.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_Calibration.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CalibrationFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      47,   15,   15,   15, 0x08,
      66,   64,   15,   15, 0x0a,
     105,   64,   15,   15, 0x0a,
     136,   64,   15,   15, 0x0a,
     168,   15,   15,   15, 0x0a,
     188,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CalibrationFrm[] = {
    "CalibrationFrm\0\0SetDisplayState(eMvcDispState)\0"
    "CommandClicked()\0,\0"
    "PreCalStatusChanged(unsigned char,int)\0"
    "PreCalDosActChanged(float,int)\0"
    "SysCfg_DosSetChanged(float,int)\0"
    "ShowCalibPopup(int)\0PopupCommandReceived(int)\0"
};

const QMetaObject CalibrationFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_CalibrationFrm,
      qt_meta_data_CalibrationFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CalibrationFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CalibrationFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CalibrationFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CalibrationFrm))
        return static_cast<void*>(const_cast< CalibrationFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int CalibrationFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: SetDisplayState((*reinterpret_cast< eMvcDispState(*)>(_a[1]))); break;
        case 1: CommandClicked(); break;
        case 2: PreCalStatusChanged((*reinterpret_cast< unsigned char(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: PreCalDosActChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: SysCfg_DosSetChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: ShowCalibPopup((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: PopupCommandReceived((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void CalibrationFrm::SetDisplayState(eMvcDispState _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

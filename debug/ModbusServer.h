/*===========================================================================*
 * File        : ModbusServer.h                                              *
 * Project     : MVC0012 - MC-TC                                             *
 * Description : Modbus server defines.                                      *
 *===========================================================================*/

#ifndef MODBUS_H
#define MODBUS_H

#include <QtNetwork>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include "Glob.h"
#include "Common.h"

#define MODBUS_OFFSET  40001


/*----- Data types -----*/
#define BYTE_TYPE   0
#define WORD_TYPE   1
#define LONG_TYPE   2
#define FLOAT_TYPE  3
#define DOUBLE_TYPE 4



/*----- ID  <-> Modbus address translation table structure -----*/
typedef struct
{
  word id;                                                                      /* ID */
  word modbusAddr;                                                              /* Modbus protocol data address */
  word modbusEndAddr;                                                           /* Modbus protocol end data address */
} tmodIdTab;



/*----- Modbus data point structure -----*/
typedef struct
{
  word regAddr;                                                                 /* Modbus register address */
  byte dataType;                                                                /* Data type */
  void *dataPtr;                                                                /* Data address */
  int structLen;                                                                /* Sizeof structure */
  void (*ReadHandler)(byte index);                                              /* Read handler */
  void (*WriteHandler)(byte index, float data);                                 /* Write handler */
} tModData;



#define MAX_REG       125                                                       /* Max number of registers in one modbus message */

/*----- Modbus exception codes -----*/
#define EXCEP_ILLEGAL_FUNCTION          1                                       /* Function not supported */
#define EXCEP_ILLEGAL_DATA_ADDRESS      2                                       /* Illegal data address */
#define EXCEP_ILLEGAL_DATA_VALUE        3                                       /* Illegal data value */

/*-----------------------------------------*/
/* Modbus function code message structures */
/*-----------------------------------------*/

/*--------------------------------------*/
/* Function code 3 = Read register data */
/*--------------------------------------*/

/*----- TCP : Function code 3 query message definition -----*/
struct sFunc3QueryTcp
{
  word transIdent;                                                              /* Transaction indentifier */
  word protIdent;                                                               /* Protocol identiefier */
  word len;                                                                     /* Number of following bytes */
  byte unit;
  byte funcCode;
  word startReg;
  word nrOfRegs;
} __attribute__ ((packed));

typedef struct sFunc3QueryTcp tFunc3QueryTcp;

/*----- TCP : Function code 3 response message definition -----*/
struct sFunc3ResponseTcp
{
  word transIdent;                                                              /* Transaction indentifier */
  word protIdent;                                                               /* Protocol identiefier */
  word len;                                                                     /* Number of following bytes */
  byte unit;
  byte funcCode;
  byte nrOfBytes;
  word reg[MAX_REG];
} __attribute__ ((packed));

typedef struct sFunc3ResponseTcp tFunc3ResponseTcp;

/*------------------------------------------*/
/* Function code 6 = Preset single register */
/*------------------------------------------*/

/*----- TCP : Function code 6 query message definition -----*/
struct sFunc6QueryTcp
{
  word transIdent;                                                              /* Transaction indentifier */
  word protIdent;                                                               /* Protocol identiefier */
  word len;                                                                     /* Number of following bytes */
  byte unit;
  byte funcCode;
  word startReg;
  word data;
} __attribute__ ((packed));

typedef struct sFunc6QueryTcp tFunc6QueryTcp;

/*----- TCP :Function code 6 response message definition -----*/
struct sFunc6ResponseTcp
{
  word transIdent;                                                              /* Transaction indentifier */
  word protIdent;                                                               /* Protocol identiefier */
  word len;                                                                     /* Number of following bytes */
  byte unit;
  byte funcCode;
  word startReg;
  word data;
} __attribute__ ((packed));

typedef struct sFunc6ResponseTcp tFunc6ResponseTcp;

/*---------------------------------------------*/
/* Function code 16 = Write multiple registers */
/*---------------------------------------------*/

/*----- TCP : Function code 16 query message definition -----*/
struct sFunc16QueryTcp
{
  word transIdent;                                                              /* Transaction indentifier */
  word protIdent;                                                               /* Protocol identiefier */
  word len;                                                                     /* Number of following bytes */
  byte unit;
  byte funcCode;
  word startReg;
  word nrOfRegs;
  byte nrOfBytes;
  word reg[MAX_REG];
} __attribute__ ((packed));

typedef struct sFunc16QueryTcp tFunc16QueryTcp;

/*----- TCP : Function code 16 response message definition -----*/
struct sFunc16ResponseTcp
{
  word transIdent;                                                              /* Transaction indentifier */
  word protIdent;                                                               /* Protocol identiefier */
  word len;                                                                     /* Number of following bytes */
  byte unit;
  byte funcCode;
  word startReg;
  word nrOfRegs;
} __attribute__ ((packed));

typedef struct sFunc16ResponseTcp tFunc16ResponseTcp;

/*-------------------*/
/* Exception resonse */
/*-------------------*/

/*----- TCP : Exception response message definition -----*/
struct sExceptionResponseTcp
{
  word transIdent;                                                              /* Transaction indentifier */
  word protIdent;                                                               /* Protocol identiefier */
  word len;                                                                     /* Number of following bytes */
  byte unit;
  byte funcCode;
  byte exceptionCode;
} __attribute__ ((packed));

typedef struct sExceptionResponseTcp tExceptionResponseTcp;

/*--------------------------------------*/
/* Modbus TCP message buffer definition */
/*--------------------------------------*/
union sModBusMsgTcp
{
  tFunc3QueryTcp func3QueryTcp;
  tFunc3ResponseTcp func3ResponseTcp;
  tFunc6QueryTcp func6QueryTcp;
  tFunc6ResponseTcp func6ResponseTcp;
  tFunc16QueryTcp func16QueryTcp;
  tFunc16ResponseTcp func16ResponseTcp;
  tExceptionResponseTcp exceptionResponseTcp;
};
typedef union sModBusMsgTcp tModBusMsgTcp;

class ModbusServer : public QObject
{
  /*----------------*/
  /* Modbus:: Class */
  /*----------------*/
  Q_OBJECT
    public:
    explicit ModbusServer(QObject *parent = 0);

    signals:

  private slots:

  public slots:
    void NewConnection();
    void TcpRead();
    void TcpDisConnected();

  private:
    QTcpServer server;
    QTcpSocket* client;

    /*----- Global variables -----*/
    word modBusLen;                                                                 /* Modbus message length */
    tModBusMsgTcp modBusMsg;                                                        /* Modbus TCP/IP message buffer */

};
#endif                                                                          // MODBUS_H

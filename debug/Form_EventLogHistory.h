#ifndef EVENTLOGHISTORYFRM_H
#define EVENTLOGHISTORYFRM_H

#include "Form_QbaseWidget.h"
#include <QVBoxLayout>
#include <QTimer>
#include <QColor>

namespace Ui
{
  class EventLogHistoryFrm;
}


class EventLogHistoryFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    EventLogHistoryFrm(QWidget *parent = 0);
    ~EventLogHistoryFrm();
    void FormInit();

  protected:
    void changeEvent(QEvent *e);
    //    void resizeEvent(QResizeEvent *);
    void showEvent(QShowEvent *);

  private:
    Ui::EventLogHistoryFrm *ui;
    void LanguageUpdate();
    int blState;
    QVBoxLayout * layout;
    QColor bgColor;
    QList<QWidget*> itemList;

    int maxScroll;
    int curScroll;
    void DrawScrollBox();

  private slots:
    void OkClicked();
    void ResetClicked();
    void LoadAllItems();
    void ClearItems();

  public slots:
    //    void ShowColor();

    signals:
};
#endif                                                                          // EVENTLOGHISTORYFRM_H

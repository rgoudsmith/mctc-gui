#include "Form_FillSystem.h"
#include "ui_Form_FillSystem.h"
#include "Form_MainMenu.h"
#include "Rout.h"

// #define COM_TEST
#define FILLSYS_MVC_OFFSET 0.58
#define FILLSYS_LINE_WIDTH 2
#define FILLSYS_BTN_OFFSET 80
#define FILLSYS_UNIT_WIDTH 20
#define FILLSYS_LOLO_YPOS 310
#define FILLSYS_LINE_X_OFFSET 15
#define FILLSYS_FIRST_Y_OFFSET 93
#define FILLSYS_MVC_IMAGE_X (-200)
#define FILLSYS_MVC_IMAGE_Y (60)

FillSystemFrm::FillSystemFrm(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::FillSystemFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  lineIdx = -1;
  cfgDecimals = 0;
  cfgMinInput = 0;
  cfgMaxInput = 100000;
  mvcXpos = 0;

  numInput = new NumericInputFrm();
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));

  connect(numInput,SIGNAL(InputValue(float)),this,SLOT(ValueReturned(float)));

  m_ui->setupUi(this);

  // Set display for desired info
  m_ui->mvcSystem->SetDisplayType(DISP_TYPE_MULTI_UNIT);
  m_ui->mvcSystem->SetDispIdx(0);
  m_ui->mvcSystem->SetMCStatusVisible(false);
  m_ui->mvcSystem->SetMaterialVisible(false);
  m_ui->mvcSystem->SetColorPctVisible(false);
  m_ui->mvcSystem->SetDosActVisible(false);
  m_ui->mvcSystem->SetDosSetVisible(false);
  m_ui->mvcSystem->SetToGoVisible(false);
  m_ui->mvcSystem->SetMotorVisible(false);
  m_ui->mvcSystem->SetWeightVisible(true);
  m_ui->mvcSystem->SetRPMVisible(false);
  m_ui->mvcSystem->SetMCIDVisible(true);
  m_ui->mvcSystem->SetToGoVisible(false);
  m_ui->mvcSystem->SetEditColorPct(false);
  m_ui->mvcSystem->SetToGoVisible(false);
  m_ui->mvcSystem->setFixedHeight(400);
  m_ui->mvcSystem->setFixedWidth(800);

  btFillSettings = new QPushButton("FILL CFG",this);
  btFillSettings->setFixedSize(90,70);
  // x-pos = 363 + 45 + 9 = 417
  btFillSettings->move((width()-99),(100));

  // Buttons + positioning
  btManFill = new QPushButton("MAN FILL",this);
  btManFill->setFixedSize(90,70);
  // x-pos = 363-45 = 318
  btManFill->move((btFillSettings->pos().x()),(btFillSettings->pos().y())-99);

  QFont lblFont;
  lblFont.setPointSize(15);
  lblFont.setBold(false);

  // Edits + positioning
  edLoLo = new QLineEdit(this);
  edLoLo->setFont(lblFont);
  edLoLo->setAlignment(Qt::AlignRight);
  edLoLo->setFixedSize(100,70);
  edLoLo->setFocusPolicy(Qt::NoFocus);
  edLoLo->move((FILLSYS_BTN_OFFSET-50),FILLSYS_LOLO_YPOS);
  edLoLo->setText("0");

  uLoLo = new QLabel("g",this,0);
  uLoLo->setFont(lblFont);
  uLoLo->setFixedSize(FILLSYS_UNIT_WIDTH,edLoLo->height());
  uLoLo->move((edLoLo->pos().x()+(edLoLo->width()+5)),edLoLo->pos().y());

  edLo = new QLineEdit(this);
  edLo->setFont(lblFont);
  edLo->setAlignment(edLoLo->alignment());
  edLo->setFixedSize(edLoLo->size());
  edLo->setFocusPolicy(Qt::NoFocus);
  edLo->move(edLoLo->x(),(edLoLo->y()-75));
  edLo->setText("0");

  if (!Rout::ImperialMode()) {
    uLo = new QLabel("g",this,0);
  }
  else {
    uLo = new QLabel("lb",this,0);
  }
  uLo->setFont(lblFont);
  uLo->setFixedSize(FILLSYS_UNIT_WIDTH,edLo->height());
  uLo->move((edLo->pos().x()+(edLo->width()+5)),edLo->pos().y());

  edHi = new QLineEdit(this);
  edHi->setFont(lblFont);
  edHi->setAlignment(edLo->alignment());
  edHi->setFixedSize(edLo->size());
  edHi->setFocusPolicy(Qt::NoFocus);
  edHi->move(edLo->x(),(edLo->y()-145));
  edHi->setText("0");

  if (!Rout::ImperialMode()) {
    uHi = new QLabel("g",this,0);
  }
  else {
    uHi = new QLabel("lb",this,0);
  }

  uHi->setFont(lblFont);
  uHi->setFixedSize(FILLSYS_UNIT_WIDTH,edHi->height());
  uHi->move((edHi->pos().x()+(edHi->width()+5)),edHi->pos().y());

  edHiHi = new QLineEdit(this);
  edHiHi->setFont(lblFont);
  edHiHi->setAlignment(edHi->alignment());
  edHiHi->setFixedSize(edHi->size());
  edHiHi->setFocusPolicy(Qt::NoFocus);
  edHiHi->move(edHi->x(),(edHi->y()-75));
  edHiHi->setText("0");

  uHiHi = new QLabel("g",this,0);
  uHiHi->setFont(lblFont);
  uHiHi->setFixedSize(FILLSYS_UNIT_WIDTH,edHiHi->height());
  uHiHi->move((edHiHi->pos().x()+(edHiHi->width()+5)),edHiHi->pos().y());

  connect(btFillSettings,SIGNAL(clicked()),this,SLOT(FillSettingsClicked()));
  //  connect(btManFill,SIGNAL(clicked()),this,SLOT(ManualFillPressed()));
  connect(btManFill,SIGNAL(pressed()),this,SLOT(ManualFillPressed()));
  connect(btManFill,SIGNAL(released()),this,SLOT(ManualFillReleased()));
  connect(m_ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(m_ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));
  this->SetLevelType(fsEX);

  // Link event filter to line edits
  edHiHi->installEventFilter(this);
  edHi->installEventFilter(this);
  edLo->installEventFilter(this);
  edLoLo->installEventFilter(this);

  m_ui->scrollArea->setStyleSheet("background: transparent");
}


FillSystemFrm::~FillSystemFrm()
{
  delete numInput;
  delete m_ui;
}


void FillSystemFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      m_ui->retranslateUi(this);
      LanguageUpdate();
      break;
    default:
      break;
  }
}


void FillSystemFrm::resizeEvent(QResizeEvent *e)
{
  QWidget::resizeEvent(e);
  int xpos = (this->width()/2)+FILLSYS_MVC_IMAGE_X;
  if (xpos != mvcXpos) {
    m_ui->mvcSystem->move(xpos,FILLSYS_MVC_IMAGE_Y);
    mvcXpos = xpos;
  }
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
}


void FillSystemFrm::showEvent(QShowEvent *e)
{
  // init values
  // set correct display
  int idx = glob->ActUnitIndex();
  CfgFillSystemChanged(glob->sysCfg[idx].fillSystem.fillSysType,idx);
  levels.HiHi = glob->sysCfg[idx].fillLevels.HiHi;
  levels.Hi = glob->sysCfg[idx].fillLevels.Hi;
  levels.Lo = glob->sysCfg[idx].fillLevels.Lo;
  levels.LoLo = glob->sysCfg[idx].fillLevels.LoLo;

  // Display levels;
  PrintLevelValues();
  mvcXpos = 0;
  QWidget::showEvent(e);
}


bool FillSystemFrm::eventFilter(QObject *obj, QEvent *ev)
{
  if (ev->type() == QEvent::MouseButtonPress) {
    lineIdx = -1;
    float val = 0;
    float maxVal = cfgMaxInput;
    float minVal = cfgMinInput;
    if (obj == edHiHi) {
      minVal = levels.Hi;
      // Execute code for edHiHi
      lineIdx = 0;
      val = edHiHi->text().toFloat();
    }

    if (obj == edHi) {
      if (edHiHi->isVisible())  maxVal = levels.HiHi;
      minVal = levels.Lo;
      // Execute code for edHi
      lineIdx = 1;
      val = edHi->text().toFloat();
    }

    if (obj == edLo) {
      if (edHi->isVisible())  maxVal = levels.Hi;
      minVal = levels.LoLo;
      // Execute code for edLo
      lineIdx = 2;
      val = edLo->text().toFloat();
    }

    if (obj == edLoLo) {
      if (edLo->isVisible())  maxVal = levels.Lo;
      // Execute code for edLoLo
      lineIdx = 3;
      val = edLoLo->text().toFloat();
    }

    if (lineIdx >= 0) {
      KeyBeep();
      numInput->SetDisplay(true,cfgDecimals,val,minVal,maxVal);
      numInput->show();
      return true;
    }
  }
  return QWidget::eventFilter(obj,ev);
}


void FillSystemFrm::DrawLine(QPainter *p, int startX, int startY, int stopX, int stopY, int lineOffset)
{
  int xOff;

  if (startX > stopX) { xOff = stopX + ((startX-stopX)/2) + lineOffset; }
  else { xOff = startX + ((stopX-startX)/2) + lineOffset; }

  p->drawLine(startX,startY,xOff,startY);
  p->drawLine(xOff,startY,xOff,stopY);
  p->drawLine(xOff,stopY,stopX,stopY);
}


#ifdef UIT
void FillSystemFrm::paintEvent(QPaintEvent *)
{
  /*
  int y;
  int endX,endY;
  QPainter painter(this);

  QWidget::paintEvent(e);

  int x = (this->width()/2)+15;
  if (x != mvcXpos)
  {
      m_ui->mvcSystem->move(x,-20);
  mvcXpos = x;

  QPen redPen(Qt::red);
  redPen.setWidth(FILLSYS_LINE_WIDTH);
  QPen blackPen(Qt::black);
  blackPen.setWidth(FILLSYS_LINE_WIDTH);

  QBrush redBrush(Qt::red);
  QBrush blackBrush(Qt::black);

  painter.setPen(QPen(QBrush(Qt::red,Qt::SolidPattern), FILLSYS_LINE_WIDTH, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin));
  painter.setBrush(QBrush(Qt::black, Qt::SolidPattern));

  // First horizontal part
  x = m_ui->mvcSystem->pos().x()+9;
  y = m_ui->mvcSystem->pos().y()+FILLSYS_FIRST_Y_OFFSET;
  endX = edHiHi->pos().x() + edHiHi->width() + FILLSYS_UNIT_WIDTH + 10;
  endY = edHiHi->pos().y() + (edHiHi->height()/2);

  // HiHi Level
  if (fillSys == fsEX) {
  DrawLine(&painter,x,y,endX,endY,FILLSYS_LINE_X_OFFSET);
  }

  y += 15;
  endY = edHi->pos().y() + (edHi->height()/2);
  // Hi Level
  painter.setPen(blackPen);
  painter.setBrush(blackBrush);
  if (fillSys >= fsME) {
  DrawLine(&painter,x,y,endX,endY,FILLSYS_LINE_X_OFFSET);
  }

  y += 85;
  endY = edLo->pos().y() + (edLo->height()/2);
  // Lo Level
  if (fillSys >= fsNone) {
  DrawLine(&painter,x,y,endX,endY,FILLSYS_LINE_X_OFFSET);
  }

  painter.setPen(redPen);
  painter.setBrush(redBrush);
  y += 15;
  endY = edLoLo->pos().y() + (edLoLo->height()/2);
  // LoLo Level
  if (fillSys >= fsNone) {
  DrawLine(&painter,x,y,endX,endY,(FILLSYS_LINE_X_OFFSET+10));
  }
  }
  */
}
#endif

void FillSystemFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  m_ui->mvcSystem->FormInit();
  numInput->FormInit();

  connect(glob,SIGNAL(MstCfg_WeightUnitsChanged(QString)),this,SLOT(UnitsChanged(QString)));
  connect(glob,SIGNAL(CfgFill_FillSysChanged(eFillSys,int)),this,SLOT(CfgFillSystemChanged(eFillSys,int)));
  connect(glob,SIGNAL(SysCfg_HopperLevelsChanged(float,float,float,float,int)),this,SLOT(CfgLevelsChanged(float,float,float,float,int)));
  connect(glob,SIGNAL(CfgFill_ManualFillChanged(bool,int)),this,SLOT(CfgFill_ManFillChanged(bool,int)));

  LanguageUpdate();

  glob->SetButtonImage(m_ui->btOk,itSelection,selOk);
  glob->SetButtonImage(m_ui->btCancel,itSelection,selCancel);

  CfgFill_ManFillChanged(glob->sysCfg[glob->ActUnitIndex()].fillSystem.manualFill,glob->ActUnitIndex());
}


void FillSystemFrm::LanguageUpdate()
{
  glob->SetButtonImage(btManFill,itGeneral,genFillManual);
  glob->SetButtonImage(btFillSettings,itConfig,cfgFillSystem);

  edHiHi->setFont(*(glob->baseFont));
  edHi->setFont(*(glob->baseFont));
  edLo->setFont(*(glob->baseFont));
  edLoLo->setFont(*(glob->baseFont));

  uHiHi->setFont(*(glob->baseFont));
  uHi->setFont(*(glob->baseFont));
  uLo->setFont(*(glob->baseFont));
  uLoLo->setFont(*(glob->baseFont));
}


void FillSystemFrm::SetLevelType(eFillSys _type)
{
  fillSys = _type;
  switch(fillSys) {
    case fsNone:
      m_ui->mvcSystem->ShowLevels(true,1);
      edHiHi->setVisible(false);
      uHiHi->setVisible(false);
      edHi->setVisible(false);
      uHi->setVisible(false);
      btManFill->setVisible(false);
      btFillSettings->setVisible(false);
      break;
    case fsME:
      m_ui->mvcSystem->ShowLevels(true,2);
      edHiHi->setVisible(false);
      uHiHi->setVisible(false);
      edHi->setVisible(true);
      uHi->setVisible(true);
      btManFill->setVisible(true);
      btFillSettings->setVisible(true);
      break;
    case fsMV:
      m_ui->mvcSystem->ShowLevels(true,2);
      edHiHi->setVisible(false);
      uHiHi->setVisible(false);
      edHi->setVisible(true);
      uHi->setVisible(true);
      btManFill->setVisible(true);
      btFillSettings->setVisible(true);
      break;
    case fsEX:
      m_ui->mvcSystem->ShowLevels(true,3);
      edHiHi->setVisible(true);
      uHiHi->setVisible(true);
      edHi->setVisible(true);
      uHi->setVisible(true);
      btManFill->setVisible(true);
      btFillSettings->setVisible(true);
      break;
    default:
      break;
  }
}


void FillSystemFrm::UnitsChanged(const QString &str)
{
  uHiHi->setText(str);
  uHi->setText(str);
  uLo->setText(str);
  uLoLo->setText(str);
}


void FillSystemFrm::CfgFillSystemChanged(eFillSys fs, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    SetLevelType(fs);
  }
}


void FillSystemFrm::CfgLevelsChanged(float HiHi, float Hi, float Lo, float LoLo, int idx)
{
  if (idx == glob->ActUnitIndex()) {
    levels.HiHi = HiHi;
    levels.Hi = Hi;
    levels.Lo = Lo;
    levels.LoLo = LoLo;
    PrintLevelValues();
  }
}


void FillSystemFrm::FillSettingsClicked()
{
  // Show AdvLoaderSettingsFrm.
  ((MainMenu*)(glob->menu))->DisplayWindow(wiAdvLevels);
}


void FillSystemFrm::ManualFillPressed()
{
  /*-----------------------------*/
  /* Manual fill button pressed  */
  /*-----------------------------*/
  int idx = glob->ActUnitIndex();
  glob->CfgFill_SetManualFill(true,idx);                                        /* Open fill valve */
  glob->AddEventItem(idx,2,TXT_INDEX_MANFILL_START,0,0);
}


void FillSystemFrm::ManualFillReleased()
{
  /*----------------------------*/
  /* Manual fill button releaed */
  /*----------------------------*/
  glob->CfgFill_SetManualFill(false,glob->ActUnitIndex());                      /* Close fill valve */
}


void FillSystemFrm::CfgFill_ManFillChanged(bool fill, int idx)
{
  if (glob->ActUnitIndex() == idx) {
    if (fill == true) {
      glob->SetButtonImage(btManFill,itSelection,selOk);
    }
    else {
      glob->SetButtonImage(btManFill,itGeneral,genFillManual);
    }
  }
}


void FillSystemFrm::OkClicked()
{
  // Store settings
  glob->SysCfg_SetHopperLevels(levels.HiHi,levels.Hi,levels.Lo,levels.LoLo,glob->ActUnitIndex());
  CancelClicked();
}


void FillSystemFrm::CancelClicked()
{
  if (isActiveWindow())
    ((MainMenu*)glob->menu)->DisplayWindow(wiUnitCfgStd);
}


void FillSystemFrm::PrintLevelValues()
{
  edHiHi->setText(QString("%1").arg(levels.HiHi,glob->formats.LevelFmt.fieldWidth,glob->formats.LevelFmt.format.toAscii(),glob->formats.LevelFmt.precision));
  edHi->setText(QString("%1").arg(levels.Hi,glob->formats.LevelFmt.fieldWidth,glob->formats.LevelFmt.format.toAscii(),glob->formats.LevelFmt.precision));
  edLo->setText(QString("%1").arg(levels.Lo,glob->formats.LevelFmt.fieldWidth,glob->formats.LevelFmt.format.toAscii(),glob->formats.LevelFmt.precision));
  edLoLo->setText(QString("%1").arg(levels.LoLo,glob->formats.LevelFmt.fieldWidth,glob->formats.LevelFmt.format.toAscii(),glob->formats.LevelFmt.precision));
}


void FillSystemFrm::ValueReturned(float value)
{
  switch(lineIdx) {
    case 0:
      levels.HiHi = value;
      break;

    case 1:
      levels.Hi = value;
      break;

    case 2:
      levels.Lo = value;
      break;
    case 3:
      levels.LoLo = value;
      break;
  }

  // Check if invisible items need to be updated to prevent invalid input
  if (!(edLo->isVisible())) {
    if (levels.Lo < value) levels.Lo = value;
  }
  if (!(edHi->isVisible())) {
    if (levels.Hi < value) levels.Hi = value;
  }
  if (!(edHiHi->isVisible())) {
    if (levels.HiHi < value) levels.HiHi = value;
  }

  PrintLevelValues();
}

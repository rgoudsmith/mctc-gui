#ifndef SELECTIONFRM_H
#define SELECTIONFRM_H

#include <QtGui/QWidget>
#include <QLabel>
#include "Form_QbaseWidget.h"

struct TSelectValue
{
  int itemNr;
  int sortIdx;
};

namespace Ui
{
  class selectionFrm;
}


class selectionFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    selectionFrm(QWidget *parent = 0);
    ~selectionFrm();
    void FormInit();
    void SetFormCaption(const QString&);
    void SetCurrentMode(const eMode&);
    void SetCurrentMotor(const eMotor&);
    void SetCurrentDosTool(const eDosTool&);

  protected:
    void changeEvent(QEvent *e);
    void paintEvent(QPaintEvent *e);
    void showEvent(QShowEvent *);
    void LanguageUpdate();

  private:
    Ui::selectionFrm *m_ui;
    int selectionType,curSelection,unitIdx;
    QLabel * selLabel;
    eMode currentMode;
    TSelectValue items[9];

    void SetSelectionBorder();
    void intSelectionMade(int);
    void CheckDependancies();
    void HideItem(int);
    void ClearItems();
    void DisplayItems();
    void SortItems();

  private slots:
    void bt11_clicked() { intSelectionMade(0); }
    void bt12_clicked() { intSelectionMade(1); }
    void bt13_clicked() { intSelectionMade(2); }

    void bt21_clicked() { intSelectionMade(3); }
    void bt22_clicked() { intSelectionMade(4); }
    void bt23_clicked() { intSelectionMade(5); }

    void bt31_clicked() { intSelectionMade(6); }
    void bt32_clicked() { intSelectionMade(7); }
    void bt33_clicked() { intSelectionMade(8); }

    void btOk_clicked();
    void btCancel_clicked();
    void btNext_clicked();

  public slots:
    void DisplaySelection(int,int,int _unitIdx = 0);

    signals:
    void selectionMade(int,int);
};
#endif                                                                          // SELECTIONFRM_H

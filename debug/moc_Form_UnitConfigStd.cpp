/****************************************************************************
** Meta object code from reading C++ file 'Form_UnitConfigStd.h'
**
** Created: Thu Jan 31 11:21:12 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_UnitConfigStd.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_UnitConfigStd.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_UnitConfigStdFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      38,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   18,   17,   17, 0x05,
      53,   48,   17,   17, 0x05,
      89,   48,   17,   17, 0x05,
     122,   48,   17,   17, 0x05,
     154,   48,   17,   17, 0x05,
     194,   48,   17,   17, 0x05,
     226,   17,   17,   17, 0x05,
     257,   48,   17,   17, 0x05,

 // slots: signature, parameters, type, tag, flags
     295,   17,   17,   17, 0x08,
     316,   17,   17,   17, 0x08,
     336,   17,   17,   17, 0x08,
     356,   17,   17,   17, 0x08,
     378,   17,   17,   17, 0x08,
     399,   17,   17,   17, 0x08,
     420,   17,   17,   17, 0x08,
     443,   17,   17,   17, 0x08,
     455,   17,   17,   17, 0x08,
     478,   17,   17,   17, 0x08,
     496,   17,   17,   17, 0x08,
     512,   17,   17,   17, 0x08,
     531,   17,   17,   17, 0x08,
     553,  551,   17,   17, 0x0a,
     579,   17,   17,   17, 0x0a,
     593,   17,   17,   17, 0x0a,
     611,   17,   17,   17, 0x0a,
     631,  551,   17,   17, 0x0a,
     660,   17,   17,   17, 0x0a,
     688,   17,   17,   17, 0x0a,
     711,   17,   17,   17, 0x0a,
     732,   17,   17,   17, 0x0a,
     749,   17,   17,   17, 0x0a,
     768,  551,   17,   17, 0x0a,
     800,  551,   17,   17, 0x0a,
     832,  551,   17,   17, 0x0a,
     868,  551,   17,   17, 0x0a,
     896,   17,   17,   17, 0x0a,
     921,  551,   17,   17, 0x0a,
     955,   17,   17,   17, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_UnitConfigStdFrm[] = {
    "UnitConfigStdFrm\0\0,,\0selectionMade(int,int,int)\0"
    ",idx\0SysCfg_DosToolChanged(eDosTool,int)\0"
    "SysCfg_KnifeGate(eKnifeGate,int)\0"
    "SysCfg_MotorChanged(eMotor,int)\0"
    "SysCfg_GranulateChanged(eGranulate,int)\0"
    "CfgFillSysChanged(eFillSys,int)\0"
    "MstCfg_InputTypeChanged(eType)\0"
    "SysCfg_GraviRpmChanged(eGraviRpm,int)\0"
    "BtMotorTypeClicked()\0BtCilinderClicked()\0"
    "BtGranulesClicked()\0BtFillSystemClicked()\0"
    "btGraviRpm_clicked()\0btLoadCell_Clicked()\0"
    "btTolerances_clicked()\0OkClicked()\0"
    "inputReceived(QString)\0UnitNameClicked()\0"
    "BtDiagClicked()\0BtServiceClicked()\0"
    "BtManualIoClicked()\0,\0ReceiveSelection(int,int)\0"
    "ShowSelForm()\0BtLevelsCLicked()\0"
    "ShowAdvUnitConfig()\0UnitNameChanged(QString,int)\0"
    "ActiveUnitIndexChanged(int)\0"
    "SlaveCountChanged(int)\0BtKnifeGateClicked()\0"
    "BtTachoClicked()\0BtMcwTypeClicked()\0"
    "SysCfg_SetDosTool(eDosTool,int)\0"
    "SysCfg_SetMotorType(eMotor,int)\0"
    "SysCfg_SetGranulate(eGranulate,int)\0"
    "CfgSetFillSys(eFillSys,int)\0"
    "MstCfg_SetInpType(eType)\0"
    "SysCfg_SetGraviRpm(eGraviRpm,int)\0"
    "CfgCurrentUserChanged(eUserType)\0"
};

const QMetaObject UnitConfigStdFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_UnitConfigStdFrm,
      qt_meta_data_UnitConfigStdFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &UnitConfigStdFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *UnitConfigStdFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *UnitConfigStdFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_UnitConfigStdFrm))
        return static_cast<void*>(const_cast< UnitConfigStdFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int UnitConfigStdFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: selectionMade((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 1: SysCfg_DosToolChanged((*reinterpret_cast< eDosTool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: SysCfg_KnifeGate((*reinterpret_cast< eKnifeGate(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: SysCfg_MotorChanged((*reinterpret_cast< eMotor(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: SysCfg_GranulateChanged((*reinterpret_cast< eGranulate(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: CfgFillSysChanged((*reinterpret_cast< eFillSys(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: MstCfg_InputTypeChanged((*reinterpret_cast< eType(*)>(_a[1]))); break;
        case 7: SysCfg_GraviRpmChanged((*reinterpret_cast< eGraviRpm(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: BtMotorTypeClicked(); break;
        case 9: BtCilinderClicked(); break;
        case 10: BtGranulesClicked(); break;
        case 11: BtFillSystemClicked(); break;
        case 12: btGraviRpm_clicked(); break;
        case 13: btLoadCell_Clicked(); break;
        case 14: btTolerances_clicked(); break;
        case 15: OkClicked(); break;
        case 16: inputReceived((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 17: UnitNameClicked(); break;
        case 18: BtDiagClicked(); break;
        case 19: BtServiceClicked(); break;
        case 20: BtManualIoClicked(); break;
        case 21: ReceiveSelection((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 22: ShowSelForm(); break;
        case 23: BtLevelsCLicked(); break;
        case 24: ShowAdvUnitConfig(); break;
        case 25: UnitNameChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 26: ActiveUnitIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 27: SlaveCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 28: BtKnifeGateClicked(); break;
        case 29: BtTachoClicked(); break;
        case 30: BtMcwTypeClicked(); break;
        case 31: SysCfg_SetDosTool((*reinterpret_cast< eDosTool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 32: SysCfg_SetMotorType((*reinterpret_cast< eMotor(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 33: SysCfg_SetGranulate((*reinterpret_cast< eGranulate(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 34: CfgSetFillSys((*reinterpret_cast< eFillSys(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 35: MstCfg_SetInpType((*reinterpret_cast< eType(*)>(_a[1]))); break;
        case 36: SysCfg_SetGraviRpm((*reinterpret_cast< eGraviRpm(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 37: CfgCurrentUserChanged((*reinterpret_cast< eUserType(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 38;
    }
    return _id;
}

// SIGNAL 0
void UnitConfigStdFrm::selectionMade(int _t1, int _t2, int _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void UnitConfigStdFrm::SysCfg_DosToolChanged(eDosTool _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void UnitConfigStdFrm::SysCfg_KnifeGate(eKnifeGate _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void UnitConfigStdFrm::SysCfg_MotorChanged(eMotor _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void UnitConfigStdFrm::SysCfg_GranulateChanged(eGranulate _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void UnitConfigStdFrm::CfgFillSysChanged(eFillSys _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void UnitConfigStdFrm::MstCfg_InputTypeChanged(eType _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void UnitConfigStdFrm::SysCfg_GraviRpmChanged(eGraviRpm _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_END_MOC_NAMESPACE

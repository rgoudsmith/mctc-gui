#ifndef MULTICOMPONENTITEMFRM_H
#define MULTICOMPONENTITEMFRM_H

//#include <QWidget>
//#include "Form_QbaseWidget.h"

#include <QWidget>
#include <QLineEdit>
#include "Rout.h"
#include "ComShare.h"
#include "Form_NumericInput.h"
#include "Form_QbaseWidget.h"
#include "Form_Selection.h"

namespace Ui
{
  class MultiComponentItemFrm;
}


class MultiComponentItemFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit MultiComponentItemFrm(QWidget *parent = 0);
    ~MultiComponentItemFrm();
    void FormInit();
    void SetUnitIndex(int);

  protected:
    void showEvent(QShowEvent *);
    bool eventFilter(QObject *, QEvent *);
    void resizeEvent(QResizeEvent *e);

  private:
    Ui::MultiComponentItemFrm *ui;
    NumericInputFrm * numInput;
    selectionFrm *selForm;
    int unitIdx;
    bool connected;

    //    bool eventFilter(QObject *, QEvent *);

    void UpdateAllItems();
    void UpdateUnitActiveSts();
    void SetupDisplay(eMcDeviceType);
    void ConnectSignals();

  private slots:
    void btOnOffClicked();
    void SelectionReceived(int,int);

  public slots:
    void UnitNameChanged(const QString &, int);
    void UnitStatusChanged(eMCStatus, int);
    void AlarmStatusChanged(bool ,int);
    void SysCfg_ColorPctChanged(float,int);
    void McWeightCapChanged(float,float,float, int);
    void SysSts_DosActChanged(float,int);
    void SysCfg_DosSetChanged(float,int);
    void SysActiveChanged(bool,int);
    void ImageClicked();
    void AlarmClicked();
    //    void ColPctClicked();
    void ValueReturned(float value);

    signals:
    void CfgUnitStatusChanged(eMCStatus,int);
    void CfgSetColorPct(float,int);
    void ColPctClicked(int);
    void UnitSelected();
};
#endif                                                                          // MULTICOMPONENTITEMFRM_H

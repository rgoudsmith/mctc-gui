#include "Form_ProcessDiagnostic.h"
#include "ui_Form_ProcessDiagnostic.h"
#include "Form_MainMenu.h"
#include "Rout.h"

ProcessDiagnosticFrm::ProcessDiagnosticFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::ProcessDiagnosticFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);
}


ProcessDiagnosticFrm::~ProcessDiagnosticFrm()
{
  delete ui;
}


void ProcessDiagnosticFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  glob->SetButtonImage(ui->btNext,itGeneral,genArrowKeyRight);
  glob->SetButtonImage(ui->btPrev,itGeneral,genArrowKeyLeft);
  glob->SetButtonImage(ui->btOk,itSelection,selOk);

  ui->btNext->setVisible((glob->mstCfg.slaveCount > 0));
  ui->btPrev->setVisible((glob->mstCfg.slaveCount > 0));

  connect(glob,SIGNAL(MstCfg_SlaveCountChanged(int)),this,SLOT(SlaveCountChanged(int)));
  connect(ui->btNext,SIGNAL(clicked()),glob,SLOT(MstSts_SetNextUnitIndex()));
  connect(ui->btPrev,SIGNAL(clicked()),glob,SLOT(MstSts_SetPrevUnitIndex()));
  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
}


void ProcessDiagnosticFrm::showEvent(QShowEvent *e)
{
  /*-----------------------------------------------------*/
  /* Show window event handler                           */
  /*-----------------------------------------------------*/
  /* The Process diagnostic screen has become active.    */
  /*-----------------------------------------------------*/
  QWidget::showEvent(e);

  /*----- Connect data refresh to msg received -----*/
  connect(glob,SIGNAL(ComReceived(tMsgControlUnit,int)),this,SLOT(DiagDataReceived(tMsgControlUnit,int)));
}


void ProcessDiagnosticFrm::hideEvent(QHideEvent *e)
{
  /*-----------------------------------------------------*/
  /* Hide window event handler                           */
  /*-----------------------------------------------------*/
  /* The Process diagnostic screen has become in-active. */
  /*-----------------------------------------------------*/
  QWidget::hideEvent(e);

  /*----- Dis connect data refresh to msg received -----*/
  disconnect(glob,SIGNAL(ComReceived(tMsgControlUnit,int)),this,SLOT(DiagDataReceived(tMsgControlUnit,int)));
}


void ProcessDiagnosticFrm::OkClicked()
{
  ((MainMenu*)(glob->menu))->DisplayWindow(wiBack);
}


void ProcessDiagnosticFrm::SlaveCountChanged(int)
{
  ui->btNext->setVisible((glob->mstCfg.slaveCount > 0));
  ui->btPrev->setVisible((glob->mstCfg.slaveCount > 0));
}


void ProcessDiagnosticFrm::DiagDataReceived(tMsgControlUnit msgData,int idx)
{
  /*-----------------------------------------*/
  /* Message received, refresh diagnose data */
  /*-----------------------------------------*/

  /*----- Refresh only for the active unit -----*/
  if (glob->ActUnitIndex() == idx) {

    int balProd = (int)(msgData.balProdStatus);
    ui->lbBalProdSts->setText(QString::number(balProd));

    if (msgData.almActive != 0) {
      ui->lbAlmCode->setText(QString::number(msgData.alarmNr));
    }
    else {
      ui->lbAlmCode->setText("--");
    }

    ui->lbMeasRstCnt->setText(QString::number(msgData.metRstHystCnt));
    ui->lbActBndNr->setText(QString::number(msgData.bandNr));
    ui->lbInBndCnt->setText(QString::number(msgData.inBand));
    ui->lbOutBndCnt->setText(QString::number(msgData.outBand));
    ui->lbMaxBndCnt->setText(QString::number(msgData.bandCnt));
    ui->lbCorSts->setText(QString::number(msgData.corSts));
    ui->lbMcSts->setText(QString::number(msgData.mcSts));
    ui->lbPutWeightCnt->setText(QString::number(msgData.putIst));
    ui->lbGetWeightCnt->setText(QString::number(msgData.getIst));
    ui->lbQrState->setText(QString::number(msgData.mcStsQR));
    ui->lbQrPutWeight->setText(QString::number(msgData.putIstQR));
    ui->lbQrGetWeight->setText(QString::number(msgData.getIstQR));
    ui->lbQrRetryCnt->setText(QString::number(msgData.devRetryQR));

    ui->lbMeasDos->setText(QString::number(msgData.metTimeAct));
    ui->lbMeasAct->setText(QString::number(msgData.metTimeMeas));
    ui->lbMeasAvg->setText(QString::number(msgData.metTimeCalc));
    ui->lbColPctAct->setText(QString::number(msgData.colCapIst));
    ui->lbCapDevAbs->setText(QString::number(msgData.vidColDevPct));
    ui->lbCapDevRel->setText(QString::number(msgData.colDevPctRel));
    ui->lbCorCyc->setText(QString::number(msgData.corCycle));
    ui->lbCor->setText(QString::number(msgData.vidCorrection));
    ui->lbRpmCor->setText(QString::number(msgData.rpmCorr));
    ui->lbBndFacAdj->setText(QString::number(msgData.corFacAdjust));
    ui->lbBndMinTimeCor->setText(QString::number(msgData.minTimeCorFac));
    ui->lbMeasLenSet->setText(QString::number(msgData.sampleLen));
    ui->lbMeasLenAct->setText(QString::number(msgData.timeLeft));
    ui->lbQrMeasSet->setText(QString::number(msgData.sampleLenQR));
    ui->lbQrMeasAct->setText(QString::number(msgData.timeLeftQR));
    ui->lbQrDeviation->setText(QString::number(msgData.colDevPctQR));
    ui->lbTachoRatio->setText(QString::number(msgData.tachoRatioAct));

    ui->lbMotSpeedAct->setText(QString::number(msgData.actRpm));
    ui->lbColPctVidAct->setText(QString::number(msgData.colCapIst,'f',3));
    ui->lbColPctSet->setText(QString::number(glob->sysSts[idx].setProd,'f',3));
    ui->lbHopWeight->setText(QString::number(msgData.weight,'f',1));

    QString tmp = "";
    glob->GetEnumeratedText(etMcStatus,glob->sysSts[idx].MCStatus,&tmp);
    ui->lbSts->setText(tmp);

    ui->lbMCID->setText(glob->sysCfg[idx].MC_ID);
  }
}

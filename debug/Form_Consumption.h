#ifndef FORM_CONSUMPTION_H
#define FORM_CONSUMPTION_H

#include <QtGui/QWidget>
#include "Form_QbaseWidget.h"
#include "CalibrationPopup.h"

namespace Ui
{
  class ConsumptionFrm;
}


class ConsumptionFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit ConsumptionFrm(QWidget *parent = 0);
    ~ConsumptionFrm();
    void FormInit();

  protected:
    void showEvent(QShowEvent *e);
    void hideEvent(QHideEvent *);
    void changeEvent(QEvent *e);

  private:
    Ui::ConsumptionFrm *ui;

  public slots:
    void ConsumptionDataReceived(tMsgControlUnit,int);
    void BtBatchResetClicked();
    void BtTotalResetClicked();
};
#endif                                                                          // FORM_CONSUMPTION_H

#ifndef DATETIMEEDITORFRM_H
#define DATETIMEEDITORFRM_H

#include <QWidget>
#include "Form_QbaseWidget.h"

namespace Ui
{
  class DateTimeEditorFrm;
}


class DateTimeEditorFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit DateTimeEditorFrm(QWidget *parent = 0);
    ~DateTimeEditorFrm();
    void FormInit();

  protected:
    void changeEvent(QEvent *);

  private:
    Ui::DateTimeEditorFrm *ui;
    bool popActive;
    int hour,minute,day,month,year;
    int newHour,newMinute,newDay,newMonth,newYear;

    void SetCurrentDateTime();
    void EncodeDateTime();
    void ConvertDateTime(QString &,int , bool);
    void LanguageUpdate();

    signals:
    void DateTimeChanged(const QString &);

  private slots:
    void OkClicked();
    void CancelClicked();

    void HourUpClicked();
    void HourDownClicked();
    void MinuteUpClicked();
    void MinuteDownClicked();

    void DayUpClicked();
    void DayDownClicked();
    void MonthUpClicked();
    void MonthDownClicked();
    void YearUpClicked();
    void YearDownClicked();

  public slots:
    void ShowDialog();
};
#endif                                                                          // DATETIMEEDITORFRM_H

#ifndef NUMKEYBOARDFRM_H
#define NUMKEYBOARDFRM_H

#include <QtGui/QWidget>
#include "Form_QbaseWidget.h"

namespace Ui
{
  class NumKeyboardFrm;
}


class NumKeyboardFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    NumKeyboardFrm(QWidget *parent = 0);
    ~NumKeyboardFrm();
    void FormInit();
    void Initialize(bool,int,float,float,float,bool = false);
    void Clear();

  protected:
    void changeEvent(QEvent *e);
    void showEvent(QShowEvent *);

  private:
    Ui::NumKeyboardFrm *m_ui;
    bool isPass;
    bool hasDot;
    bool firstInput;
    bool isFloat;
    int decimals;
    QString dispText;
    float value;
    float minValue;
    float maxValue;
    void ValueToScreen(bool);
    bool RangeCheck(float);

  private slots:
    void ButtonClicked();
    void DotClicked();
    void ClearClicked();
    void BackspaceClicked();
    void SignClicked();
    void OkClicked();
    void CancelClicked();

    signals:
    void InputValue(float);
    void PassInput(const QString &);
};
#endif                                                                          // NUMKEYBOARDFRM_H

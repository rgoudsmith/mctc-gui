/********************************************************************************
** Form generated from reading UI file 'Form_McWeightSettings.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_MCWEIGHTSETTINGS_H
#define UI_FORM_MCWEIGHTSETTINGS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_McWeightSettingsFrm
{
public:
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btOk_3;
    QPushButton *btCancel_3;
    QWidget *layoutWidget_2;
    QGridLayout *gridLayout_3;
    QHBoxLayout *horizontalLayout_9;
    QLineEdit *edTachoRatio;
    QSpacerItem *horizontalSpacer_15;
    QSpacerItem *horizontalSpacer_16;
    QLabel *lbTachoRatioUnits;
    QLabel *lbTachoRatio;
    QLabel *lbCaption;

    void setupUi(QWidget *McWeightSettingsFrm)
    {
        if (McWeightSettingsFrm->objectName().isEmpty())
            McWeightSettingsFrm->setObjectName(QString::fromUtf8("McWeightSettingsFrm"));
        McWeightSettingsFrm->resize(800, 492);
        layoutWidget = new QWidget(McWeightSettingsFrm);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 420, 791, 72));
        horizontalLayout_7 = new QHBoxLayout(layoutWidget);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_3);

        btOk_3 = new QPushButton(layoutWidget);
        btOk_3->setObjectName(QString::fromUtf8("btOk_3"));
        btOk_3->setMinimumSize(QSize(90, 70));
        btOk_3->setMaximumSize(QSize(90, 70));
        btOk_3->setText(QString::fromUtf8("OK"));

        horizontalLayout_7->addWidget(btOk_3);

        btCancel_3 = new QPushButton(layoutWidget);
        btCancel_3->setObjectName(QString::fromUtf8("btCancel_3"));
        btCancel_3->setMinimumSize(QSize(90, 70));
        btCancel_3->setMaximumSize(QSize(90, 70));
        btCancel_3->setText(QString::fromUtf8("CANCEL"));

        horizontalLayout_7->addWidget(btCancel_3);

        layoutWidget_2 = new QWidget(McWeightSettingsFrm);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(9, 160, 781, 71));
        gridLayout_3 = new QGridLayout(layoutWidget_2);
        gridLayout_3->setSpacing(9);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        edTachoRatio = new QLineEdit(layoutWidget_2);
        edTachoRatio->setObjectName(QString::fromUtf8("edTachoRatio"));
        edTachoRatio->setMinimumSize(QSize(95, 60));
        edTachoRatio->setMaximumSize(QSize(95, 60));
        QFont font;
        font.setPointSize(15);
        edTachoRatio->setFont(font);
        edTachoRatio->setCursor(QCursor(Qt::BlankCursor));
        edTachoRatio->setFocusPolicy(Qt::NoFocus);
        edTachoRatio->setText(QString::fromUtf8("0"));
        edTachoRatio->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_9->addWidget(edTachoRatio);


        gridLayout_3->addLayout(horizontalLayout_9, 0, 4, 1, 1);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_15, 0, 6, 1, 1);

        horizontalSpacer_16 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_16, 0, 2, 1, 1);

        lbTachoRatioUnits = new QLabel(layoutWidget_2);
        lbTachoRatioUnits->setObjectName(QString::fromUtf8("lbTachoRatioUnits"));
        lbTachoRatioUnits->setFont(font);
        lbTachoRatioUnits->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(lbTachoRatioUnits, 0, 5, 1, 1);

        lbTachoRatio = new QLabel(layoutWidget_2);
        lbTachoRatio->setObjectName(QString::fromUtf8("lbTachoRatio"));
        lbTachoRatio->setFont(font);
        lbTachoRatio->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(lbTachoRatio, 0, 3, 1, 1);

        lbCaption = new QLabel(McWeightSettingsFrm);
        lbCaption->setObjectName(QString::fromUtf8("lbCaption"));
        lbCaption->setGeometry(QRect(0, 10, 781, 23));
        lbCaption->setFont(font);
        lbCaption->setAlignment(Qt::AlignCenter);

        retranslateUi(McWeightSettingsFrm);

        QMetaObject::connectSlotsByName(McWeightSettingsFrm);
    } // setupUi

    void retranslateUi(QWidget *McWeightSettingsFrm)
    {
        McWeightSettingsFrm->setWindowTitle(QApplication::translate("McWeightSettingsFrm", "Form", 0, QApplication::UnicodeUTF8));
        lbTachoRatioUnits->setText(QApplication::translate("McWeightSettingsFrm", "KgH/V", "label (Deviation Alarm)", QApplication::UnicodeUTF8));
        lbTachoRatio->setText(QApplication::translate("McWeightSettingsFrm", "Tacho ratio", "label (Deviation Alarm)", QApplication::UnicodeUTF8));
        lbCaption->setText(QApplication::translate("McWeightSettingsFrm", "McWeight Settings", "Form Title", QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class McWeightSettingsFrm: public Ui_McWeightSettingsFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_MCWEIGHTSETTINGS_H

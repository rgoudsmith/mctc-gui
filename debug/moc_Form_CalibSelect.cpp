/****************************************************************************
** Meta object code from reading C++ file 'Form_CalibSelect.h'
**
** Created: Thu Jan 31 11:20:26 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_CalibSelect.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_CalibSelect.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CalibSelectFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      30,   28,   15,   15, 0x0a,
      59,   15,   15,   15, 0x0a,
      78,   15,   15,   15, 0x0a,
      92,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CalibSelectFrm[] = {
    "CalibSelectFrm\0\0OkClicked()\0,\0"
    "LoadcellTypeChanged(int,int)\0"
    "CalibrateClicked()\0ZeroClicked()\0"
    "WeightCheckClicked()\0"
};

const QMetaObject CalibSelectFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_CalibSelectFrm,
      qt_meta_data_CalibSelectFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CalibSelectFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CalibSelectFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CalibSelectFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CalibSelectFrm))
        return static_cast<void*>(const_cast< CalibSelectFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int CalibSelectFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: OkClicked(); break;
        case 1: LoadcellTypeChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: CalibrateClicked(); break;
        case 3: ZeroClicked(); break;
        case 4: WeightCheckClicked(); break;
        default: ;
        }
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

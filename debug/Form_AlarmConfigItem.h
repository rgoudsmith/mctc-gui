#ifndef ALARMCONFIGITEMFRM_H
#define ALARMCONFIGITEMFRM_H

#include "Form_QbaseWidget.h"

namespace Ui
{
  class AlarmConfigItemFrm;
}


class AlarmConfigItemFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit AlarmConfigItemFrm(QWidget *parent = 0);
    ~AlarmConfigItemFrm();
    void FormInit();

  protected:
    void showEvent(QShowEvent *);

  private:
    int alarmNr;
    QString description;
    bool mode;

    Ui::AlarmConfigItemFrm *ui;

  private slots:
    void ButtonClicked();

  public slots:
    void SetAlarmNr(int);
    void SetAlarmDescription(const QString &);
    void SetAlarmMode(bool,bool);
    void SetupUnitAlarm(eMcDeviceType,bool);
};
#endif                                                                          // ALARMCONFIGITEMFRM_H

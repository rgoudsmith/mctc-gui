/********************************************************************************
** Form generated from reading UI file 'Form_RecipeConfigItem.ui'
**
** Created: Tue Jan 17 14:26:40 2012
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_RECIPECONFIGITEM_H
#define UI_FORM_RECIPECONFIGITEM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RecipeConfigItemFrm
{
public:
    QHBoxLayout *horizontalLayout;
    QLabel *lbDevID;
    QLineEdit *edMaterial;
    QLineEdit *edColPct;

    void setupUi(QWidget *RecipeConfigItemFrm)
    {
        if (RecipeConfigItemFrm->objectName().isEmpty())
            RecipeConfigItemFrm->setObjectName(QString::fromUtf8("RecipeConfigItemFrm"));
        RecipeConfigItemFrm->resize(532, 70);
        RecipeConfigItemFrm->setCursor(QCursor(Qt::BlankCursor));
        RecipeConfigItemFrm->setWindowTitle(QString::fromUtf8("Form"));
        RecipeConfigItemFrm->setStyleSheet(QString::fromUtf8("#lbDevID{ border: 1px solid; border-radius: 3px; }"));
        horizontalLayout = new QHBoxLayout(RecipeConfigItemFrm);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lbDevID = new QLabel(RecipeConfigItemFrm);
        lbDevID->setObjectName(QString::fromUtf8("lbDevID"));
        lbDevID->setMinimumSize(QSize(200, 70));
        lbDevID->setMaximumSize(QSize(200, 70));
        lbDevID->setFocusPolicy(Qt::NoFocus);
        lbDevID->setStyleSheet(QString::fromUtf8(""));
        lbDevID->setText(QString::fromUtf8(""));
        lbDevID->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(lbDevID);

        edMaterial = new QLineEdit(RecipeConfigItemFrm);
        edMaterial->setObjectName(QString::fromUtf8("edMaterial"));
        edMaterial->setMinimumSize(QSize(200, 70));
        edMaterial->setMaximumSize(QSize(200, 70));
        edMaterial->setFocusPolicy(Qt::NoFocus);
        edMaterial->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(edMaterial);

        edColPct = new QLineEdit(RecipeConfigItemFrm);
        edColPct->setObjectName(QString::fromUtf8("edColPct"));
        edColPct->setMinimumSize(QSize(120, 70));
        edColPct->setMaximumSize(QSize(120, 70));
        edColPct->setFocusPolicy(Qt::NoFocus);
        edColPct->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(edColPct);


        retranslateUi(RecipeConfigItemFrm);

        QMetaObject::connectSlotsByName(RecipeConfigItemFrm);
    } // setupUi

    void retranslateUi(QWidget *RecipeConfigItemFrm)
    {
        Q_UNUSED(RecipeConfigItemFrm);
    } // retranslateUi

};

namespace Ui {
    class RecipeConfigItemFrm: public Ui_RecipeConfigItemFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_RECIPECONFIGITEM_H

#ifndef USBTRANSFERFRM_H
#define USBTRANSFERFRM_H

#include "Form_QbaseWidget.h"
#include "Form_ErrorDlg.h"

namespace Ui
{
  class UsbTransferFrm;
}


enum eTransferDirection
{
  /*-----------------------*/
  /* USB tranfer direction */
  /*-----------------------*/
  etdCtrlToUsb,                                                                 /* Controller to USB */
  etdUsbToCtrl                                                                  /* USB to controller */
  //  etdCount
};

class UsbTransferFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit UsbTransferFrm(QWidget *parent = 0);
    ~UsbTransferFrm();
    void FormInit();

  private:
    Ui::UsbTransferFrm *ui;
    ErrorDlg * errorForm;
    QStringList fileList;
    QString sourcePath;
    QString destPath;
    int itemIndex;
    bool yesAll;
    bool noAll;
    bool waitForResponse;
    eTransferDirection mode;

    void SetupDisplay();
    void DisplayWidget(int);

    // transfer routines (int param is for copy direction)
    int TransferMaterials(eTransferDirection);
    int TransferRecipes(eTransferDirection);
    int TransferConfiguration(eTransferDirection);
    int TransferEventLog(eTransferDirection);
    int TransferProcesLog(eTransferDirection);

    void SetItemProgressLimit(int,int);
    void SetItemProgressPos(int);

    void SetTypeProgressLimit(int,int);
    void SetTypeProgressPos(int);

    bool CopyFile(const QString &, const QString &);
    void CopyNextFile();

  private slots:
    void MessageResult(int);

  public slots:
    void UsbTransferMode(eTransferDirection);
    void ButtonOneClicked();
    void ButtonTwoClicked();
    void ButtonAllClicked();
    void ButtonDoneClicked();
    void ButtonOkClicked();

    signals:
    void CopyDone();
};
#endif                                                                          // USBTRANSFERFRM_H

/********************************************************************************
** Form generated from reading UI file 'Form_Keyboard.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_KEYBOARD_H
#define UI_FORM_KEYBOARD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_KeyboardFrm
{
public:
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_2;
    QGridLayout *gridLayout;
    QLabel *inputField;
    QPushButton *bt1;
    QPushButton *bt2;
    QPushButton *bt3;
    QPushButton *bt4;
    QPushButton *bt5;
    QPushButton *bt6;
    QPushButton *bt7;
    QPushButton *bt8;
    QPushButton *bt9;
    QPushButton *bt0;
    QPushButton *btQ;
    QPushButton *btW;
    QPushButton *btE;
    QPushButton *btR;
    QPushButton *btT;
    QPushButton *btY;
    QPushButton *btU;
    QPushButton *btI;
    QPushButton *btO;
    QPushButton *btP;
    QPushButton *btA;
    QPushButton *btS;
    QPushButton *btD;
    QPushButton *btF;
    QPushButton *btG;
    QPushButton *btH;
    QPushButton *btJ;
    QPushButton *btK;
    QPushButton *btL;
    QPushButton *btBackspace;
    QPushButton *btShift;
    QPushButton *btZ;
    QPushButton *btX;
    QPushButton *btC;
    QPushButton *btV;
    QPushButton *btB;
    QPushButton *btN;
    QPushButton *btM;
    QPushButton *btDash;
    QPushButton *btClear;
    QPushButton *btSpace;
    QPushButton *btOk;
    QPushButton *btCancel;

    void setupUi(QWidget *KeyboardFrm)
    {
        if (KeyboardFrm->objectName().isEmpty())
            KeyboardFrm->setObjectName(QString::fromUtf8("KeyboardFrm"));
        KeyboardFrm->setWindowModality(Qt::ApplicationModal);
        KeyboardFrm->resize(775, 461);
        KeyboardFrm->setCursor(QCursor(Qt::BlankCursor));
        KeyboardFrm->setWindowTitle(QString::fromUtf8("Keyboard"));
        horizontalLayout = new QHBoxLayout(KeyboardFrm);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        groupBox = new QGroupBox(KeyboardFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox{border: 5px solid black; border-radius: 7px; background: white;}"));
        groupBox->setAlignment(Qt::AlignCenter);
        horizontalLayout_2 = new QHBoxLayout(groupBox);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setHorizontalSpacing(5);
        gridLayout->setVerticalSpacing(6);
        inputField = new QLabel(groupBox);
        inputField->setObjectName(QString::fromUtf8("inputField"));
        inputField->setMinimumSize(QSize(70, 35));
        inputField->setMaximumSize(QSize(16777215, 35));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        inputField->setPalette(palette);
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setWeight(75);
        inputField->setFont(font);
        inputField->setAutoFillBackground(false);
        inputField->setStyleSheet(QString::fromUtf8("border: 1px solid black; border-radius: 3px;"));
        inputField->setFrameShape(QFrame::Box);
        inputField->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(inputField, 0, 0, 1, 10);

        bt1 = new QPushButton(groupBox);
        bt1->setObjectName(QString::fromUtf8("bt1"));
        bt1->setMinimumSize(QSize(70, 60));
        bt1->setMaximumSize(QSize(70, 60));
        bt1->setText(QString::fromUtf8("1"));

        gridLayout->addWidget(bt1, 1, 0, 1, 1);

        bt2 = new QPushButton(groupBox);
        bt2->setObjectName(QString::fromUtf8("bt2"));
        bt2->setMinimumSize(QSize(70, 60));
        bt2->setMaximumSize(QSize(70, 60));
        bt2->setText(QString::fromUtf8("2"));

        gridLayout->addWidget(bt2, 1, 1, 1, 1);

        bt3 = new QPushButton(groupBox);
        bt3->setObjectName(QString::fromUtf8("bt3"));
        bt3->setMinimumSize(QSize(70, 60));
        bt3->setMaximumSize(QSize(70, 60));
        bt3->setText(QString::fromUtf8("3"));

        gridLayout->addWidget(bt3, 1, 2, 1, 1);

        bt4 = new QPushButton(groupBox);
        bt4->setObjectName(QString::fromUtf8("bt4"));
        bt4->setMinimumSize(QSize(70, 60));
        bt4->setMaximumSize(QSize(70, 60));
        bt4->setText(QString::fromUtf8("4"));

        gridLayout->addWidget(bt4, 1, 3, 1, 1);

        bt5 = new QPushButton(groupBox);
        bt5->setObjectName(QString::fromUtf8("bt5"));
        bt5->setMinimumSize(QSize(70, 60));
        bt5->setMaximumSize(QSize(70, 60));
        bt5->setText(QString::fromUtf8("5"));

        gridLayout->addWidget(bt5, 1, 4, 1, 1);

        bt6 = new QPushButton(groupBox);
        bt6->setObjectName(QString::fromUtf8("bt6"));
        bt6->setMinimumSize(QSize(70, 60));
        bt6->setMaximumSize(QSize(70, 60));
        bt6->setText(QString::fromUtf8("6"));

        gridLayout->addWidget(bt6, 1, 5, 1, 1);

        bt7 = new QPushButton(groupBox);
        bt7->setObjectName(QString::fromUtf8("bt7"));
        bt7->setMinimumSize(QSize(70, 60));
        bt7->setMaximumSize(QSize(70, 60));
        bt7->setText(QString::fromUtf8("7"));

        gridLayout->addWidget(bt7, 1, 6, 1, 1);

        bt8 = new QPushButton(groupBox);
        bt8->setObjectName(QString::fromUtf8("bt8"));
        bt8->setMinimumSize(QSize(70, 60));
        bt8->setMaximumSize(QSize(70, 60));
        bt8->setText(QString::fromUtf8("8"));

        gridLayout->addWidget(bt8, 1, 7, 1, 1);

        bt9 = new QPushButton(groupBox);
        bt9->setObjectName(QString::fromUtf8("bt9"));
        bt9->setMinimumSize(QSize(70, 60));
        bt9->setMaximumSize(QSize(70, 60));
        bt9->setText(QString::fromUtf8("9"));

        gridLayout->addWidget(bt9, 1, 8, 1, 1);

        bt0 = new QPushButton(groupBox);
        bt0->setObjectName(QString::fromUtf8("bt0"));
        bt0->setMinimumSize(QSize(70, 60));
        bt0->setMaximumSize(QSize(70, 60));
        bt0->setText(QString::fromUtf8("0"));

        gridLayout->addWidget(bt0, 1, 9, 1, 1);

        btQ = new QPushButton(groupBox);
        btQ->setObjectName(QString::fromUtf8("btQ"));
        btQ->setMinimumSize(QSize(70, 60));
        btQ->setMaximumSize(QSize(70, 60));
        btQ->setText(QString::fromUtf8("q"));

        gridLayout->addWidget(btQ, 2, 0, 1, 1);

        btW = new QPushButton(groupBox);
        btW->setObjectName(QString::fromUtf8("btW"));
        btW->setMinimumSize(QSize(70, 60));
        btW->setMaximumSize(QSize(70, 60));
        btW->setText(QString::fromUtf8("w"));

        gridLayout->addWidget(btW, 2, 1, 1, 1);

        btE = new QPushButton(groupBox);
        btE->setObjectName(QString::fromUtf8("btE"));
        btE->setMinimumSize(QSize(70, 60));
        btE->setMaximumSize(QSize(70, 60));
        btE->setText(QString::fromUtf8("e"));

        gridLayout->addWidget(btE, 2, 2, 1, 1);

        btR = new QPushButton(groupBox);
        btR->setObjectName(QString::fromUtf8("btR"));
        btR->setMinimumSize(QSize(70, 60));
        btR->setMaximumSize(QSize(70, 60));
        btR->setText(QString::fromUtf8("r"));

        gridLayout->addWidget(btR, 2, 3, 1, 1);

        btT = new QPushButton(groupBox);
        btT->setObjectName(QString::fromUtf8("btT"));
        btT->setMinimumSize(QSize(70, 60));
        btT->setMaximumSize(QSize(70, 60));
        btT->setText(QString::fromUtf8("t"));

        gridLayout->addWidget(btT, 2, 4, 1, 1);

        btY = new QPushButton(groupBox);
        btY->setObjectName(QString::fromUtf8("btY"));
        btY->setMinimumSize(QSize(70, 60));
        btY->setMaximumSize(QSize(70, 60));
        btY->setText(QString::fromUtf8("y"));

        gridLayout->addWidget(btY, 2, 5, 1, 1);

        btU = new QPushButton(groupBox);
        btU->setObjectName(QString::fromUtf8("btU"));
        btU->setMinimumSize(QSize(70, 60));
        btU->setMaximumSize(QSize(70, 60));
        btU->setText(QString::fromUtf8("u"));

        gridLayout->addWidget(btU, 2, 6, 1, 1);

        btI = new QPushButton(groupBox);
        btI->setObjectName(QString::fromUtf8("btI"));
        btI->setMinimumSize(QSize(70, 60));
        btI->setMaximumSize(QSize(70, 60));
        btI->setText(QString::fromUtf8("i"));

        gridLayout->addWidget(btI, 2, 7, 1, 1);

        btO = new QPushButton(groupBox);
        btO->setObjectName(QString::fromUtf8("btO"));
        btO->setMinimumSize(QSize(70, 60));
        btO->setMaximumSize(QSize(70, 60));
        btO->setText(QString::fromUtf8("o"));

        gridLayout->addWidget(btO, 2, 8, 1, 1);

        btP = new QPushButton(groupBox);
        btP->setObjectName(QString::fromUtf8("btP"));
        btP->setMinimumSize(QSize(70, 60));
        btP->setMaximumSize(QSize(70, 60));
        btP->setText(QString::fromUtf8("p"));

        gridLayout->addWidget(btP, 2, 9, 1, 1);

        btA = new QPushButton(groupBox);
        btA->setObjectName(QString::fromUtf8("btA"));
        btA->setMinimumSize(QSize(70, 60));
        btA->setMaximumSize(QSize(70, 60));
        btA->setText(QString::fromUtf8("a"));

        gridLayout->addWidget(btA, 3, 0, 1, 1);

        btS = new QPushButton(groupBox);
        btS->setObjectName(QString::fromUtf8("btS"));
        btS->setMinimumSize(QSize(70, 60));
        btS->setMaximumSize(QSize(70, 60));
        btS->setText(QString::fromUtf8("s"));

        gridLayout->addWidget(btS, 3, 1, 1, 1);

        btD = new QPushButton(groupBox);
        btD->setObjectName(QString::fromUtf8("btD"));
        btD->setMinimumSize(QSize(70, 60));
        btD->setMaximumSize(QSize(70, 60));
        btD->setText(QString::fromUtf8("d"));

        gridLayout->addWidget(btD, 3, 2, 1, 1);

        btF = new QPushButton(groupBox);
        btF->setObjectName(QString::fromUtf8("btF"));
        btF->setMinimumSize(QSize(70, 60));
        btF->setMaximumSize(QSize(70, 60));
        btF->setText(QString::fromUtf8("f"));

        gridLayout->addWidget(btF, 3, 3, 1, 1);

        btG = new QPushButton(groupBox);
        btG->setObjectName(QString::fromUtf8("btG"));
        btG->setMinimumSize(QSize(70, 60));
        btG->setMaximumSize(QSize(70, 60));
        btG->setText(QString::fromUtf8("g"));

        gridLayout->addWidget(btG, 3, 4, 1, 1);

        btH = new QPushButton(groupBox);
        btH->setObjectName(QString::fromUtf8("btH"));
        btH->setMinimumSize(QSize(70, 60));
        btH->setMaximumSize(QSize(70, 60));
        btH->setText(QString::fromUtf8("h"));

        gridLayout->addWidget(btH, 3, 5, 1, 1);

        btJ = new QPushButton(groupBox);
        btJ->setObjectName(QString::fromUtf8("btJ"));
        btJ->setMinimumSize(QSize(70, 60));
        btJ->setMaximumSize(QSize(70, 60));
        btJ->setText(QString::fromUtf8("j"));

        gridLayout->addWidget(btJ, 3, 6, 1, 1);

        btK = new QPushButton(groupBox);
        btK->setObjectName(QString::fromUtf8("btK"));
        btK->setMinimumSize(QSize(70, 60));
        btK->setMaximumSize(QSize(70, 60));
        btK->setText(QString::fromUtf8("k"));

        gridLayout->addWidget(btK, 3, 7, 1, 1);

        btL = new QPushButton(groupBox);
        btL->setObjectName(QString::fromUtf8("btL"));
        btL->setMinimumSize(QSize(70, 60));
        btL->setMaximumSize(QSize(70, 60));
        btL->setText(QString::fromUtf8("l"));

        gridLayout->addWidget(btL, 3, 8, 1, 1);

        btBackspace = new QPushButton(groupBox);
        btBackspace->setObjectName(QString::fromUtf8("btBackspace"));
        btBackspace->setMinimumSize(QSize(70, 60));
        btBackspace->setMaximumSize(QSize(70, 60));
        btBackspace->setText(QString::fromUtf8("<---"));

        gridLayout->addWidget(btBackspace, 3, 9, 1, 1);

        btShift = new QPushButton(groupBox);
        btShift->setObjectName(QString::fromUtf8("btShift"));
        btShift->setMinimumSize(QSize(70, 60));
        btShift->setMaximumSize(QSize(70, 60));
        btShift->setText(QString::fromUtf8("Shift"));

        gridLayout->addWidget(btShift, 4, 0, 1, 1);

        btZ = new QPushButton(groupBox);
        btZ->setObjectName(QString::fromUtf8("btZ"));
        btZ->setMinimumSize(QSize(70, 60));
        btZ->setMaximumSize(QSize(70, 60));
        btZ->setText(QString::fromUtf8("z"));

        gridLayout->addWidget(btZ, 4, 1, 1, 1);

        btX = new QPushButton(groupBox);
        btX->setObjectName(QString::fromUtf8("btX"));
        btX->setMinimumSize(QSize(70, 60));
        btX->setMaximumSize(QSize(70, 60));
        btX->setText(QString::fromUtf8("x"));

        gridLayout->addWidget(btX, 4, 2, 1, 1);

        btC = new QPushButton(groupBox);
        btC->setObjectName(QString::fromUtf8("btC"));
        btC->setMinimumSize(QSize(70, 60));
        btC->setMaximumSize(QSize(70, 60));
        btC->setText(QString::fromUtf8("c"));

        gridLayout->addWidget(btC, 4, 3, 1, 1);

        btV = new QPushButton(groupBox);
        btV->setObjectName(QString::fromUtf8("btV"));
        btV->setMinimumSize(QSize(70, 60));
        btV->setMaximumSize(QSize(70, 60));
        btV->setText(QString::fromUtf8("v"));

        gridLayout->addWidget(btV, 4, 4, 1, 1);

        btB = new QPushButton(groupBox);
        btB->setObjectName(QString::fromUtf8("btB"));
        btB->setMinimumSize(QSize(70, 60));
        btB->setMaximumSize(QSize(70, 60));
        btB->setText(QString::fromUtf8("b"));

        gridLayout->addWidget(btB, 4, 5, 1, 1);

        btN = new QPushButton(groupBox);
        btN->setObjectName(QString::fromUtf8("btN"));
        btN->setMinimumSize(QSize(70, 60));
        btN->setMaximumSize(QSize(70, 60));
        btN->setText(QString::fromUtf8("n"));

        gridLayout->addWidget(btN, 4, 6, 1, 1);

        btM = new QPushButton(groupBox);
        btM->setObjectName(QString::fromUtf8("btM"));
        btM->setMinimumSize(QSize(70, 60));
        btM->setMaximumSize(QSize(70, 60));
        btM->setText(QString::fromUtf8("m"));

        gridLayout->addWidget(btM, 4, 7, 1, 1);

        btDash = new QPushButton(groupBox);
        btDash->setObjectName(QString::fromUtf8("btDash"));
        btDash->setMinimumSize(QSize(70, 60));
        btDash->setMaximumSize(QSize(70, 60));
        btDash->setText(QString::fromUtf8("-"));

        gridLayout->addWidget(btDash, 4, 8, 1, 1);

        btClear = new QPushButton(groupBox);
        btClear->setObjectName(QString::fromUtf8("btClear"));
        btClear->setMinimumSize(QSize(70, 60));
        btClear->setMaximumSize(QSize(70, 60));
        btClear->setText(QString::fromUtf8("Clear"));

        gridLayout->addWidget(btClear, 4, 9, 1, 1);

        btSpace = new QPushButton(groupBox);
        btSpace->setObjectName(QString::fromUtf8("btSpace"));
        btSpace->setMinimumSize(QSize(0, 60));
        btSpace->setMaximumSize(QSize(16777215, 60));
        btSpace->setText(QString::fromUtf8(""));

        gridLayout->addWidget(btSpace, 5, 2, 1, 6);

        btOk = new QPushButton(groupBox);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(70, 60));
        btOk->setMaximumSize(QSize(70, 60));
        btOk->setText(QString::fromUtf8("Ok"));

        gridLayout->addWidget(btOk, 6, 8, 1, 1);

        btCancel = new QPushButton(groupBox);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(70, 60));
        btCancel->setMaximumSize(QSize(70, 60));
        btCancel->setText(QString::fromUtf8("Cancel"));

        gridLayout->addWidget(btCancel, 6, 9, 1, 1);


        horizontalLayout_2->addLayout(gridLayout);


        horizontalLayout->addWidget(groupBox);


        retranslateUi(KeyboardFrm);

        QMetaObject::connectSlotsByName(KeyboardFrm);
    } // setupUi

    void retranslateUi(QWidget *KeyboardFrm)
    {
        groupBox->setTitle(QString());
        inputField->setText(QString());
        Q_UNUSED(KeyboardFrm);
    } // retranslateUi

};

namespace Ui {
    class KeyboardFrm: public Ui_KeyboardFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_KEYBOARD_H

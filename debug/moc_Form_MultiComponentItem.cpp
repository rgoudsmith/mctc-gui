/****************************************************************************
** Meta object code from reading C++ file 'Form_MultiComponentItem.h'
**
** Created: Thu Jan 31 11:20:39 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_MultiComponentItem.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_MultiComponentItem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MultiComponentItemFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      25,   23,   22,   22, 0x05,
      61,   23,   22,   22, 0x05,
      87,   22,   22,   22, 0x05,
     106,   22,   22,   22, 0x05,

 // slots: signature, parameters, type, tag, flags
     121,   22,   22,   22, 0x08,
     138,   23,   22,   22, 0x08,
     165,   23,   22,   22, 0x0a,
     194,   23,   22,   22, 0x0a,
     227,   23,   22,   22, 0x0a,
     256,   23,   22,   22, 0x0a,
     294,  290,   22,   22, 0x0a,
     336,   23,   22,   22, 0x0a,
     368,   23,   22,   22, 0x0a,
     400,   23,   22,   22, 0x0a,
     427,   22,   22,   22, 0x0a,
     442,   22,   22,   22, 0x0a,
     463,  457,   22,   22, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MultiComponentItemFrm[] = {
    "MultiComponentItemFrm\0\0,\0"
    "CfgUnitStatusChanged(eMCStatus,int)\0"
    "CfgSetColorPct(float,int)\0ColPctClicked(int)\0"
    "UnitSelected()\0btOnOffClicked()\0"
    "SelectionReceived(int,int)\0"
    "UnitNameChanged(QString,int)\0"
    "UnitStatusChanged(eMCStatus,int)\0"
    "AlarmStatusChanged(bool,int)\0"
    "SysCfg_ColorPctChanged(float,int)\0,,,\0"
    "McWeightCapChanged(float,float,float,int)\0"
    "SysSts_DosActChanged(float,int)\0"
    "SysCfg_DosSetChanged(float,int)\0"
    "SysActiveChanged(bool,int)\0ImageClicked()\0"
    "AlarmClicked()\0value\0ValueReturned(float)\0"
};

const QMetaObject MultiComponentItemFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_MultiComponentItemFrm,
      qt_meta_data_MultiComponentItemFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MultiComponentItemFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MultiComponentItemFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MultiComponentItemFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MultiComponentItemFrm))
        return static_cast<void*>(const_cast< MultiComponentItemFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int MultiComponentItemFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: CfgUnitStatusChanged((*reinterpret_cast< eMCStatus(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: CfgSetColorPct((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: ColPctClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: UnitSelected(); break;
        case 4: btOnOffClicked(); break;
        case 5: SelectionReceived((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: UnitNameChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: UnitStatusChanged((*reinterpret_cast< eMCStatus(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: AlarmStatusChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 9: SysCfg_ColorPctChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 10: McWeightCapChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 11: SysSts_DosActChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 12: SysCfg_DosSetChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 13: SysActiveChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 14: ImageClicked(); break;
        case 15: AlarmClicked(); break;
        case 16: ValueReturned((*reinterpret_cast< float(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void MultiComponentItemFrm::CfgUnitStatusChanged(eMCStatus _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MultiComponentItemFrm::CfgSetColorPct(float _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MultiComponentItemFrm::ColPctClicked(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MultiComponentItemFrm::UnitSelected()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
QT_END_MOC_NAMESPACE

#ifndef FILESELECTFRM_H
#define FILESELECTFRM_H

#include <QWidget>
#include <QLabel>
#include <QStringList>
#include "Form_QbaseWidget.h"
#include "Form_Keyboard.h"

namespace Ui
{
  class FileSelectFrm;
}


class FileSelectFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit FileSelectFrm(QWidget *parent = 0);
    ~FileSelectFrm();
    void FormInit();
    void SetFileExt(const QString &);
    void SetFilePath(const QString &);
    QString searchString;                                   /* Recipe list search filter */

  private:
    Ui::FileSelectFrm *ui;

    int currentIndex;
    int curScroll;
    int maxScroll;

    int msgIdx;

    bool doSearch;
    bool doNew;
    bool doDelete;
    bool doRename;
    bool doAccept;
    bool forceAccept;

    QString fileExt;
    QString path;
    QString currentItem;
    QStringList fileList;
    QLabel * ScrollBox;

    KeyboardFrm * keyInput;

    void LoadFiles();
    void DisplayUpBoxItems();
    void DisplayDownBoxItems();
    void DisplaySelectedItem();
    void UpdateDisplay();
    int ItemCount();
    void UpdateCurrent(int,bool = false);
    void UpdateCurrent(const QString &,bool = false);
    void UpdateScrollBox();
    void ClipExtension(QString *);
    void RemoveCurrentItem();
    void RemoveAllItems();
    int RenameCurrentItem(const QString &newName);
    void SetCorrectButtons();

  protected:
    void resizeEvent(QResizeEvent *);

  private slots:
    void OkClicked();
    void SearchClicked();
    void NewClicked();
    void DeleteClicked();
    void DeleteAllClicked();
    void RenameClicked();
    void ScrollUp();
    void ScrollDown();

  public slots:
    void CanSearch(bool);
    void CanNew(bool);
    void CanRename(bool);
    void CanDelete(bool);
    void CanAccept(bool);
    void ForceAccept(bool);
    void RefreshDisplay();
    void SetSelectedFile(const QString &);
    void CfgCurrentUserChanged(eUserType);
    void SysActiveChanged(bool);
    void MessageResult(int);
    void TextReceived(const QString &);

    signals:
    void SelectClicked(const QString &);
    void NewFileClicked();
    void ItemRenamed(const QString &);
};
#endif                                                                          // FILESELECTFRM_H

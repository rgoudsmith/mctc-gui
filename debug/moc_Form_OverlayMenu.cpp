/****************************************************************************
** Meta object code from reading C++ file 'Form_OverlayMenu.h'
**
** Created: Thu Jan 31 11:21:10 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_OverlayMenu.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_OverlayMenu.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_OverlayMenu[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,
      34,   12,   12,   12, 0x05,
      56,   12,   12,   12, 0x05,
      77,   12,   12,   12, 0x05,
      98,   12,   12,   12, 0x05,
     117,   12,   12,   12, 0x05,
     140,   12,   12,   12, 0x05,
     170,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
     200,   12,   12,   12, 0x0a,
     214,   12,   12,   12, 0x0a,
     233,  225,   12,   12, 0x0a,
     260,  225,   12,   12, 0x0a,
     288,  225,   12,   12, 0x0a,
     315,  225,   12,   12, 0x0a,
     342,  225,   12,   12, 0x0a,
     367,   12,   12,   12, 0x0a,
     384,   12,   12,   12, 0x0a,
     402,   12,   12,   12, 0x0a,
     419,   12,   12,   12, 0x0a,
     436,   12,   12,   12, 0x0a,
     451,   12,   12,   12, 0x0a,
     470,   12,   12,   12, 0x0a,
     493,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_OverlayMenu[] = {
    "OverlayMenu\0\0MenuBtLoginClicked()\0"
    "MenuBtConfigClicked()\0MenuBtAlarmClicked()\0"
    "MenuBtLearnClicked()\0MenuBtUsbClicked()\0"
    "MenuBtKeyLockClicked()\0"
    "MenuButtonClicked(eWindowIdx)\0"
    "MstCfg_ProdModeChanged(eMode)\0"
    "DisplayMenu()\0FoldMenu()\0caption\0"
    "SetBtLoginCaption(QString)\0"
    "SetBtConfigCaption(QString)\0"
    "SetBtAlarmCaption(QString)\0"
    "SetBtLearnCaption(QString)\0"
    "SetBtUsbCaption(QString)\0BtLoginClicked()\0"
    "BtConfigClicked()\0BtAlarmClicked()\0"
    "BtLearnClicked()\0BtUsbClicked()\0"
    "BtKeyLockClicked()\0BtConsumptionClicked()\0"
    "UserChanged(eUserType)\0"
};

const QMetaObject OverlayMenu::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_OverlayMenu,
      qt_meta_data_OverlayMenu, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OverlayMenu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OverlayMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OverlayMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OverlayMenu))
        return static_cast<void*>(const_cast< OverlayMenu*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int OverlayMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: MenuBtLoginClicked(); break;
        case 1: MenuBtConfigClicked(); break;
        case 2: MenuBtAlarmClicked(); break;
        case 3: MenuBtLearnClicked(); break;
        case 4: MenuBtUsbClicked(); break;
        case 5: MenuBtKeyLockClicked(); break;
        case 6: MenuButtonClicked((*reinterpret_cast< eWindowIdx(*)>(_a[1]))); break;
        case 7: MstCfg_ProdModeChanged((*reinterpret_cast< eMode(*)>(_a[1]))); break;
        case 8: DisplayMenu(); break;
        case 9: FoldMenu(); break;
        case 10: SetBtLoginCaption((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: SetBtConfigCaption((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: SetBtAlarmCaption((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 13: SetBtLearnCaption((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 14: SetBtUsbCaption((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: BtLoginClicked(); break;
        case 16: BtConfigClicked(); break;
        case 17: BtAlarmClicked(); break;
        case 18: BtLearnClicked(); break;
        case 19: BtUsbClicked(); break;
        case 20: BtKeyLockClicked(); break;
        case 21: BtConsumptionClicked(); break;
        case 22: UserChanged((*reinterpret_cast< eUserType(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 23;
    }
    return _id;
}

// SIGNAL 0
void OverlayMenu::MenuBtLoginClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void OverlayMenu::MenuBtConfigClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void OverlayMenu::MenuBtAlarmClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void OverlayMenu::MenuBtLearnClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void OverlayMenu::MenuBtUsbClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void OverlayMenu::MenuBtKeyLockClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}

// SIGNAL 6
void OverlayMenu::MenuButtonClicked(eWindowIdx _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void OverlayMenu::MstCfg_ProdModeChanged(eMode _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_END_MOC_NAMESPACE

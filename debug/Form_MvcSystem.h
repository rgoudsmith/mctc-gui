#ifndef MVCSYSTEM_H
#define MVCSYSTEM_H

#include <QtGui/QWidget>
#include <QtGui/QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QPushButton>
#include "Form_QbaseWidget.h"
#include "Form_NumericInput.h"

struct ResizeVars
{
  QSize origImgSize;
  QString imgPath;
  int xPos;
  float scale;
  int oldWidth;
  int oldHeight;
  int oldDispType;
  QImage bgImage;
};

namespace Ui
{
  class MvcSystem;
}


class MvcSystem : public QBaseWidget
{
  Q_OBJECT
    public:
    MvcSystem(QWidget *parent = 0);
    ~MvcSystem();
    void FormInit();
    void SetDispIdx(int);
    void ShowLevels(bool);
    void ShowLevels(bool,int);
    void SetDisplayType(int);

  protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent * e);
    void paintEvent(QPaintEvent *e);
    bool eventFilter(QObject *, QEvent *);
    void PositionLabels(int xOffset, double scaleFac);
    void PositionLabels1(int xOffset, double scaleFac);
    void PositionLabels2(int xOffset, double scaleFac);
    void PositionLabelsHO();
    void LoadImage();
    void InitValues();

  private:
    // General variables
    Ui::MvcSystem *m_ui;
    bool showLvls;
    int isResized;
    bool rpmStsVisible;
    bool editColPct;
    bool editMaterial;
    bool colPctVis;
    bool matVis;
    int lvlType;
    int dispIdx;
    int dispType;
    int editIdx;
    void DrawLevels(QPainter *,int,double);
    void dispMotorStatus(int,QLabel *lbl);
    void LanguageUpdate();
    void LabelCaptions();
    void showEvent(QShowEvent *);

    ResizeVars resVars;
    QColor levelColors[4];

    // Display unit 1
    QLabel * actMCID1;
    QLabel * actStatus1;
    QLabel * actRPM1;
    QLabel *slideStatus;
    //    QLabel * rpmUnits1;
    QLabel * rpmActive1;
    QLabel * actWght1;
    //    QLabel * wghtUnits1;
    QLabel * material1;
    QPushButton * btMaterial1;
    QLabel * colPct1;
    QLabel * dosAct1;
    QLabel * dosSet1;
    QLineEdit * colPctEdit1;

    // Display unit 2
    QLabel * actMCID2;
    QLabel * actStatus2;
    QLabel * actRPM2;
    //    QLabel * rpmUnits2;
    QLabel * rpmActive2;
    QLabel * actWght2;
    //    QLabel * wghtUnits2;
    QLabel * material2;
    QPushButton * btMaterial2;
    QLabel * colPct2;
    QLabel * dosAct2;
    QLabel * dosSet2;

    QLabel * dosActCap1;
    QLabel * dosSetCap1;
    QLabel * dosActCap2;
    QLabel * dosSetCap2;

    QLabel * dosActLine1;
    QLabel * dosSetLine1;
    QLabel * dosActLine2;
    QLabel * dosSetLine2;

    QLabel * shotsToGo1;
    QLabel * shotsToGoCap1;
    QLabel * shotsToGo2;
    QLabel * shotsToGoCap2;

    QLineEdit * colPctEdit2;

    // Input keyboard
    NumericInputFrm * numInput;

  private slots:
    void ValueReceived(float);
    void Mat1Clicked();
    void Mat2Clicked();

  public slots:
    // Visual representation
    void SetMCIDVisible(bool);
    void SetMCStatusVisible(bool);
    void SetMotorVisible(bool);
    void SetWeightVisible(bool);
    void SetRPMVisible(bool);
    void SetMaterialVisible(bool);
    void SetMaterialEditable(bool);
    void SetColorPctVisible(bool);
    void SetEditColorPct(bool);
    void SetDosActVisible(bool);
    void SetDosSetVisible(bool);
    void SetToGoVisible(bool);

    // Process indication
    void SetMCStatus(eMCStatus,int);
    void SetMotorStatus(int,int);
    void SetWeight(float,int);
    void SetActRPM(float,int);
    void SetMCID(const QString &str,int);
    void SetMaterial(const QString &str,int);
    void SetColorPct(float,int);
    void SetRPM(float,int);
    void SetDosAct(float,int);
    void SetDosSet(float,int);
    void GraviRpmChanged(eGraviRpm,int);
    void UnitIndexChanged(int);
    void SetProdToGo(float,int);
    void ComMsgReceived(tMsgControlUnit ,int);

    signals:
    void Material1Clicked();
    void Material2Clicked();
};
#endif                                                                          // MVCSYSTEM_H

/*--------------------------------------------------------------*/
/* FILE        : Form_AdvConfig2.cpp                            */
/* DESCRIPTION : Routines for the Advanced configuration form   */
/*--------------------------------------------------------------*/

#define MC_WEIGHT

#include <QScrollBar>
#include "QmVcScrollbar.h"
#include "Form_AdvConfig2.h"
#include "ui_Form_AdvConfig2.h"
#include "Form_MainMenu.h"
#include "ImageDef.h"
#include "Rout.h"

/*--- Call modes for IP edit form ---*/
#define IP_EDIT_MODE_IP_ADDR   0
#define IP_EDIT_MODE_NETMASK   1
#define IP_EDIT_MODE_GATEWAY   2

#define MAX_ADV_CFG_ITEMS          17                                           /* 17 items */

AdvConfig2Frm::AdvConfig2Frm(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::AdvConfig2Frm)
{
  /*-------------------------------------------------------------------------*/
  /* List of all advanced configuration, currently 17 items are in the list. */
  /*-------------------------------------------------------------------------*/

  /*-------------*/
  /* Constructor */
  /*-------------*/
  valSelect = 0;

  /*----- Create an input form for numeric input -----*/
  m_ui->setupUi(this);
  inputForm = new NumericInputFrm();
  inputForm->move(((width()/2)-(inputForm->width()/2)),((height()/2)-(inputForm->height()/2)));
  inputForm->hide();

  /*----- Create a selection form -----*/
  selForm = new selectionFrm();
  selForm->move(((width()/2)-(selForm->width()/2)),((height()/2)-(selForm->height()/2)));
  selForm->hide();

  /*----- Create an IP editor form -----*/
  ipForm = new IpEditorFrm();
  ipForm->move(((width()/2)-(ipForm->width()/2)),((height()/2)-(ipForm->height()/2)));
  ipForm->hide();

  /*----- Preset factory defaults timer -----*/
  factDefTim.setSingleShot(true);
  factDefTim.setInterval(500);
  factDefTim.stop();

  /*----- Override the default scrollbar of the scrollArea with the QMvcScrollBar -----*/
  QMvcScrollBar * bar = new QMvcScrollBar(m_ui->scrollArea);
  connect(bar,SIGNAL(MouseButtonPressed()),this,SLOT(KeyBeep()));

  m_ui->scrollArea->setVerticalScrollBar(bar);

  /*----- Settings buttons -----*/
  connect(m_ui->btLanguage,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->btProduction,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->btInput,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->btAutoStart,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->btRcpEnable,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->btToolPass,SIGNAL(clicked()),this,SLOT(ToolPassClicked()));
  connect(m_ui->btSuperPass,SIGNAL(clicked()),this,SLOT(SuperPassClicked()));
  connect(m_ui->btModAdr,SIGNAL(clicked()),this,SLOT(ModAddrClicked()));
  connect(m_ui->btStartupLogin,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->btIP,SIGNAL(clicked()),this,SLOT(IpAddrClicked()));
  connect(m_ui->btNetmask,SIGNAL(clicked()),this,SLOT(NetmaskClicked()));
  connect(m_ui->btGateway,SIGNAL(clicked()),this,SLOT(GatewayClicked()));
  connect(m_ui->btDateTime,SIGNAL(clicked()),this,SLOT(DateTimeClicked()));
  connect(m_ui->btSlaveCnt,SIGNAL(clicked()),this,SLOT(SlaveCntClicked()));
  connect(selForm,SIGNAL(selectionMade(int,int)),this,SLOT(SelectionReceived(int,int)));
  connect(ipForm,SIGNAL(SendIp(int,int,int,int,int)),this,SLOT(IpReceived(int, int,int,int,int)));
  connect(m_ui->btKgH_Mode,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->btProfibusEnb,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->btImperialMode,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->btUnitConfig,SIGNAL(clicked()),this,SLOT(ButtonClicked()));

  /*----- Confirm / Cancel buttons -----*/
  connect(m_ui->btConfirm,SIGNAL(clicked()),this,SLOT(ButtonConfirmClicked()));
  connect(m_ui->btDefaults,SIGNAL(clicked()),this,SLOT(FactoryDefaultsClicked()));
  connect(m_ui->btAlmCfg,SIGNAL(clicked()),this,SLOT(AlarmCfgClicked()));

  m_ui->btUnits->setVisible(false);
  m_ui->lbUnits->setVisible(false);

  /*----- calculate the height of one item, and from there calculate and assign the required height of the entire scrollArea -----*/
  int itemH = m_ui->btAutoStart->height();
  int itemSpacing = 9;
  m_ui->ScrollAreaLayout->setSpacing(itemSpacing);
  int h = (MAX_ADV_CFG_ITEMS * itemH) + (((MAX_ADV_CFG_ITEMS - 1) * itemSpacing) + (2*9));
  m_ui->scrollAreaWidgetContents->setFixedHeight(h);
}


AdvConfig2Frm::~AdvConfig2Frm()
{
  delete selForm;
  delete inputForm;
  delete m_ui;
}


void AdvConfig2Frm::showEvent(QShowEvent *e)
{
  QBaseWidget::showEvent(e);

  /*----- Load images for buttons -----*/
  //  SetButtonImages();

  /*----- Unit configuration buton only visible from userlevel agent -----*/
  if (glob->GetUser() >= utAgent) {
    m_ui->btUnitConfig->setVisible(true);
    m_ui->lbUnitConfig->setVisible(true);
  }
  else {
    m_ui->btUnitConfig->setVisible(false);
    m_ui->lbUnitConfig->setVisible(false);
  }

  /*----- Set initial values for text fields -----*/

  /*----- Date/Time field -----*/
  m_ui->btDateTime->setText(glob->mstCfg.sysTime.toString(FORMAT_DATETIME));
  /*----- Tooling password -----*/
  m_ui->btToolPass->setText(glob->mstCfg.ToolPasswd);
  /*----- Supervisor password -----*/
  m_ui->btSuperPass->setText(glob->mstCfg.SuperPasswd);

  /*----- Set initial values for selection fields -----*/

  /*----- Default login -----*/
  SetStartupUserImage(glob->mstCfg.defLogin);
  /*----- Production mode (inj/ext) -----*/
  SetStartupModeImage(glob->mstCfg.mode);
  /*----- Controller language -----*/
  SetStartupLanguageImage(glob->mstCfg.language);
  /*----- Input type (timer/relay/etc) -----*/
  SetStartupInpTypeImage(glob->mstCfg.inpType);
  /*----- Modbus address -----*/
  SetStartupModbusAddress(glob->mstCfg.modbusAddr);
  /*----- Ethernet settings (IP, Netmask, Gateway) -----*/
  SetStartupEthernetSettings();

  /*----- Number of slave units connected to the master. -----*/
  /*----- This function handles input of unitCount. -----*/
  SetStartupSlaveCount(glob->mstCfg.slaveCount);
}


void AdvConfig2Frm::changeEvent(QEvent *e)
{
  QBaseWidget::changeEvent(e);
  switch (e->type()) {
  /*----- Re-translate UI when there is a language change -----*/
  case QEvent::LanguageChange:
    m_ui->retranslateUi(this);
    LanguageUpdate();
    break;
  default:
    break;
  }
}


void AdvConfig2Frm::resizeEvent(QResizeEvent *e)
{
  QBaseWidget::resizeEvent(e);
  /*----- Reposition input forms to the center of the screen, if the screen is resized. -----*/
  inputForm->move(((width()/2)-(inputForm->width()/2)),((height()/2)-(inputForm->height()/2)));
  selForm->move(((width()/2)-(selForm->width()/2)),((height()/2)-(selForm->height()/2)));
}


void AdvConfig2Frm::LanguageUpdate()
{
  /*----- Update display values when a language selection change is made -----*/
  m_ui->btDateTime->setText(glob->mstCfg.sysTime.toString(QString(FORMAT_DATETIME)));
  m_ui->btToolPass->setText(glob->mstCfg.ToolPasswd);
  m_ui->btSuperPass->setText(glob->mstCfg.SuperPasswd);

  SetStartupUserImage(glob->mstCfg.defLogin);
  SetStartupModeImage(glob->mstCfg.mode);
  SetStartupLanguageImage(glob->mstCfg.language);
  SetStartupInpTypeImage(glob->mstCfg.inpType);
  SetStartupModbusAddress(glob->mstCfg.modbusAddr);
  SetStartupSlaveCount(glob->mstCfg.slaveCount);

  SetButtonImages();
}


void AdvConfig2Frm::SetButtonImages()
{
  /*-----------------------------------------------------------------*/
  /* Load images for buttons, also required after a language change  */
  /* to re-render all texts on buttons.                              */
  /*-----------------------------------------------------------------*/
  glob->SetButtonImage(m_ui->btConfirm,itSelection,selOk);
  glob->SetButtonImage(m_ui->btAutoStart,itActive,(glob->mstCfg.autoStart ? actYes : actNo));
  glob->SetButtonImage(m_ui->btRcpEnable,itActive,(glob->mstCfg.rcpEnable ? actYes : actNo));
  glob->SetButtonImage(m_ui->btProfibusEnb,itActive,(glob->mstCfg.profibusEnb ? actYes : actNo));
  glob->SetButtonImage(m_ui->btImperialMode,itActive,(glob->mstCfg.imperialMode ? actYes : actNo));
  glob->SetButtonImage(m_ui->btKgH_Mode,itActive,(glob->mstCfg.units_kgh ? actYes : actNo));
  glob->SetButtonImage(m_ui->btAlmCfg,itMenu,mnAlarmCfg);
  glob->SetButtonImage(m_ui->btUnitConfig,itConfig,cfgMultiUnit);
}


void AdvConfig2Frm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  /*----- Link _gbi to input forms -----*/
  inputForm->FormInit();
  selForm->FormInit();
  ipForm->FormInit();

  /*----- Update display values -----*/
  SetButtonImages();

  /*----- Set date time on button. -----*/
  m_ui->btDateTime->setText(glob->mstCfg.sysTime.toString(QString(FORMAT_DATETIME)));
  /*----- Set tool passwd on button -----*/
  m_ui->btToolPass->setText(glob->mstCfg.ToolPasswd);
  /*----- Set supervisor passwd on button. -----*/
  m_ui->btSuperPass->setText(glob->mstCfg.SuperPasswd);

  /*----- Set correct font to all labels -----*/
  m_ui->lbLanguage->setFont(*(glob->baseFont));
  m_ui->lbProdMode->setFont(*(glob->baseFont));
  m_ui->lbInputType->setFont(*(glob->baseFont));
  m_ui->lbAutoStart->setFont(*(glob->baseFont));
  m_ui->lbRecipeEn->setFont(*(glob->baseFont));
  m_ui->lbUnits->setFont(*(glob->baseFont));
  m_ui->lbStartLogin->setFont(*(glob->baseFont));
  m_ui->lbToolPass->setFont(*(glob->baseFont));
  m_ui->lbSuperPass->setFont(*(glob->baseFont));
  m_ui->lbIP->setFont(*(glob->baseFont));
  m_ui->lbNetmask->setFont(*(glob->baseFont));
  m_ui->lbGateway->setFont(*(glob->baseFont));
  m_ui->lbModAdr->setFont(*(glob->baseFont));
  m_ui->lbDateTime->setFont(*(glob->baseFont));
  m_ui->lbSlaveCount->setFont(*(glob->baseFont));
  m_ui->lbProfiEnb->setFont(*(glob->baseFont));
  m_ui->lbKgH_Mode->setFont(*(glob->baseFont));
  m_ui->lbImperialMode->setFont(*(glob->baseFont));
  m_ui->lbUnitConfig->setFont(*(glob->baseFont));

  /*----- Connect signals and slots -----*/
  connect(glob,SIGNAL(MstCfg_LanguageChanged(eLanguages)),this,SLOT(LanguageChanged()));
  connect(glob,SIGNAL(MstCfg_InputTypeChanged(eType)),this,SLOT(MstCfg_InputTypeChanged(eType)));
  connect(inputForm,SIGNAL(InputValue(float)),this,SLOT(ValueReceived(float)));
  connect(glob,SIGNAL(MstCfg_SystemTimeChanged(QDateTime)),this,SLOT(MstCfg_DateTimeChanged(QDateTime)));
  connect(&factDefTim,SIGNAL(timeout()),glob,SLOT(FactoryDefaultsExecuted()));
}


void AdvConfig2Frm::SetStartupUserImage(eUserType ut)
{
  /*--------------------------------------------------*/
  /* Convert userType to UserSelection                */
  /* UserSelection is a subset of usertype            */
  /* which limits the possibilities for startup user. */
  /*--------------------------------------------------*/
  eUserSelectType us;

  switch(ut) {
  case utOperator:
    us = usOperator;
    break;
  case utTooling:
    us = usTooling;
    break;
  case utSupervisor:
    us = usSupervisor;
    break;
  default:
    us = usOperator;
    break;
  }

  /*----- Display the corresponding image on the button -----*/
  glob->SetButtonImage(m_ui->btStartupLogin,itUserSelect,us);
}


void AdvConfig2Frm::SetStartupModeImage(eMode mod)
{
  /*---------------------------------------------------------*/
  /* Display the correct image on the production mode button */
  /*---------------------------------------------------------*/
  eMode mo;

  switch(mod) {
  case modInject:
  case modExtrude:
    mo = mod;
    break;
  default:
    mo = modInject;
    break;
  }
  glob->SetButtonImage(m_ui->btProduction,itMode,mo);
}


void AdvConfig2Frm::SetStartupInpTypeImage(eType typ)
{
  /*----------------------------------------------------*/
  /* Display the correct image on the input type button */
  /*----------------------------------------------------*/
  eType tp;

  switch(typ) {
  case ttTimer:
  case ttRelay:
  case ttTacho:
    tp = typ;
    break;
  default:
    tp = ttTimer;
  }

  glob->SetButtonImage(m_ui->btInput,itType,tp);
}


void AdvConfig2Frm::SetStartupLanguageImage(eLanguages lan)
{
  /*----------------------------------------------*/
  /* Initialize with default language (= English) */
  /*----------------------------------------------*/
  eLanguages la = lanUK;

  /*----- Range check input variable. If out of range, English is used (via initialization) -----*/
  if ((lan < lanCount) && ((int)lan >= 0)) {
    la = lan;
  }

  /*----- Set button image to reflect the selected language -----*/
  glob->SetButtonImage(m_ui->btLanguage,itLanguages,la);
}


void AdvConfig2Frm::SetStartupModbusAddress(int add)
{
  /*----- Display current modbus address on button. -----*/
  m_ui->btModAdr->setText(QString::number(add));
}


void AdvConfig2Frm::SetStartupEthernetSettings()
{
  /*-----------------------------------------------------------------------*/
  /* Initialize Ethernet settings (IP, Netmask, Gateway)                   */
  /*-----------------------------------------------------------------------*/
  /*--- Init text fields ---*/
  m_ui->btIP->setText(QString::number(glob->mstCfg.IPAddress[3])+ "." + QString::number(glob->mstCfg.IPAddress[2])+ "." + QString::number(glob->mstCfg.IPAddress[1])+ "." + QString::number(glob->mstCfg.IPAddress[0]));
  m_ui->btNetmask->setText(QString::number(glob->mstCfg.Netmask[3])+ "." + QString::number(glob->mstCfg.Netmask[2])+ "." + QString::number(glob->mstCfg.Netmask[1])+ "." + QString::number(glob->mstCfg.Netmask[0]));
  m_ui->btGateway->setText(QString::number(glob->mstCfg.Gateway[3])+ "." + QString::number(glob->mstCfg.Gateway[2])+ "." + QString::number(glob->mstCfg.Gateway[1])+ "." + QString::number(glob->mstCfg.Gateway[0]));
}


void AdvConfig2Frm::SetStartupSlaveCount(int cnt)
{
  /*----- Display current total number of units (= SlaveCount + 1) -----*/
  m_ui->btSlaveCnt->setText(QString::number((cnt+1)));
}


void AdvConfig2Frm::ToolPassClicked()
{
  /*----- Display numeric input form to change the Tooling password. -----*/
  /*----- Set current passwd as default value and limit input from 0 to 9999. -----*/
  valSelect = 1;                                                                /* Used as indicator which value we are editing, so we can use one generic result handler. */
  inputForm->SetDisplay(false,0,m_ui->btToolPass->text().toFloat(),0,9999);
  inputForm->show();
}


void AdvConfig2Frm::SuperPassClicked()
{
  /*----- Display numeric input form to change the Supervisor password -----*/
  /*----- Set current passwd as default value and limit input from 0 to 9999. -----*/
  valSelect = 2;                                                                /* Used as indicator which value we are editing, so we can use one generic result handler. */
  inputForm->SetDisplay(false,0,m_ui->btSuperPass->text().toFloat(),0,9999);
  inputForm->show();
}


void AdvConfig2Frm::ModAddrClicked()
{
  /*----- Dipslay numeric input form to change the modbus address -----*/
  /*----- Set current address as default value and limit input from 1 to 231. -----*/
  valSelect = 3;                                                                /* Used as indicator which value we are editing, so we can use one generic result handler. */
  inputForm->SetDisplay(false,0,glob->mstCfg.modbusAddr,1,231);
  inputForm->show();
}


void AdvConfig2Frm::IpAddrClicked()
{
  /*-----------------------------*/
  /* BUTTON : IP address clicked */
  /*-----------------------------*/
  int ip0,ip1,ip2,ip3;

  ip0=glob->mstCfg.IPAddress[0];
  ip1=glob->mstCfg.IPAddress[1];
  ip2=glob->mstCfg.IPAddress[2];
  ip3=glob->mstCfg.IPAddress[3];

  /*----- Initialize the editor form, 0 means we are entering an IP address, 1 would be a netmask address (meaning 0s are allowed). -----*/
  /*----- The other 4 values are the split ip address. -----*/
  ipForm->InitIp(IP_EDIT_MODE_IP_ADDR,ip3/*.toInt()*/,ip2/*.toInt()*/,ip1/*.toInt()*/,ip0/*.toInt()*/);
  /*----- Display the form. -----*/
  ipForm->show();

}


void AdvConfig2Frm::NetmaskClicked()
{
  /*--------------------------*/
  /* BUTTON : Netmask clicked */
  /*--------------------------*/
  int ip0,ip1,ip2,ip3;

  ip0=glob->mstCfg.Netmask[0];
  ip1=glob->mstCfg.Netmask[1];
  ip2=glob->mstCfg.Netmask[2];
  ip3=glob->mstCfg.Netmask[3];
  /*----- Initialize the editor form, 0 means we are entering an IP address, 1 would be a netmask address (meaning 0s are allowed). -----*/
  /*----- The other 4 values are the split ip address. -----*/
  ipForm->InitIp(IP_EDIT_MODE_NETMASK,ip3/*.toInt()*/,ip2/*.toInt()*/,ip1/*.toInt()*/,ip0/*.toInt()*/);
  /*----- Display the form. -----*/
  ipForm->show();

}


void AdvConfig2Frm::GatewayClicked()
{
  /*--------------------------*/
  /* BUTTON : Gateway clicked */
  /*--------------------------*/
  int ip0,ip1,ip2,ip3;

  ip0=glob->mstCfg.Gateway[0];
  ip1=glob->mstCfg.Gateway[1];
  ip2=glob->mstCfg.Gateway[2];
  ip3=glob->mstCfg.Gateway[3];
  /*----- Initialize the editor form, 0 means we are entering an IP address, 1 would be a netmask address (meaning 0s are allowed). -----*/
  /*----- The other 4 values are the split ip address. -----*/
  ipForm->InitIp(IP_EDIT_MODE_GATEWAY,ip3/*.toInt()*/,ip2/*.toInt()*/,ip1/*.toInt()*/,ip0/*.toInt()*/);
  /*----- Display the form. -----*/
  ipForm->show();
}


void AdvConfig2Frm::SlaveCntClicked()
{
  /*----- Display numeric input form to change the number of Slave units -----*/
  /*----- Set current slaveCount + 1 as default value, and limit input from 1 to MAX_UNIT_COUNT. -----*/
  valSelect = 4;
#ifndef MC_WEIGHT
  inputForm->SetDisplay(false,0,(glob->mstCfg.slaveCount+1),1,2);
#else
  inputForm->SetDisplay(false,0,(glob->mstCfg.slaveCount+1),1,CFG_MAX_UNIT_COUNT);
#endif
  inputForm->show();
}


void AdvConfig2Frm::ToolPassReceived(float pass)
{
  /*----- New password received, update via _gbi -----*/
  glob->MstCfg_SetToolPass(QString::number(pass));
  m_ui->btToolPass->setText(glob->mstCfg.ToolPasswd);
}


void AdvConfig2Frm::SuperPassReceived(float pass)
{
  /*----- New Supervisor password received, update via _gbi -----*/
  glob->MstCfg_SetSuperPass(QString::number(pass));
  m_ui->btSuperPass->setText(glob->mstCfg.SuperPasswd);
}


void AdvConfig2Frm::ModAddrReceived(float addr)
{
  /*----- New modbus address received, update via _gbi -----*/
  glob->MstCfg_SetModbusAddr((int)addr);
  SetStartupModbusAddress(addr);
}


void AdvConfig2Frm::SlaveCountReceived(float cnt)
{
  /*-------------------------------------------*/
  /* New Slave count received, update via _gbi */
  /*-------------------------------------------*/
  byte rebootMsg = false;

  /*----- Check change in number of controllers -----*/
  if (glob->mstCfg.slaveCount != cnt) {
    rebootMsg = true;                                                           /* Display "reboot" message enable */
  }
  glob->MstCfg_SetSlaveCount((int)cnt);
  SetStartupSlaveCount((int)cnt);

  if (rebootMsg) {
    ((MainMenu*)(glob->menu))->DisplayMessage(-1,0,tr("The number of controllers is changed.\nPlease reboot this MC-TC !"),msgtConfirm);
  }
}


void AdvConfig2Frm::DateTimeClicked()
{
  /*----- Open date/time editor form -----*/
  ((MainMenu*)(glob->menu))->dateTimeForm->ShowDialog();
}


void AdvConfig2Frm::ButtonConfirmClicked()
{
  /*----- Check global debounce delay and jump back one window -----*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiBack);
}


void AdvConfig2Frm::ButtonClicked()
{
  /*-----------------------------------------------------------------*/
  /* Universal button click routine for selection type.              */
  /* Depending on the clicked button, display the selection form     */
  /* with the correct type.                                          */
  /*-----------------------------------------------------------------*/
  btn = (QPushButton*)sender();

  /*----- BUTTON : Production mode -----*/
  if (btn == m_ui->btProduction) {
    selForm->DisplaySelection(itMode,glob->mstCfg.mode);
    selForm->show();
  }

  /*----- BUTTON : Input type -----*/
  if (btn == m_ui->btInput) {
    selForm->SetCurrentMode(glob->mstCfg.mode);
    selForm->DisplaySelection(itType,glob->mstCfg.inpType);
    selForm->show();
  }

  /*----- BUTTON : Auto start -----*/
  if (btn == m_ui->btAutoStart) {
    selForm->DisplaySelection(itActive,(glob->mstCfg.autoStart == true ? actYes : actNo));
    selForm->show();
  }

  /*----- BUTTON : Profibus enabled -----*/
  if (btn == m_ui->btProfibusEnb) {
    selForm->DisplaySelection(itActive,(glob->mstCfg.profibusEnb == true ? actYes : actNo));
    selForm->show();
  }

  /*----- BUTTON : Imperial mode -----*/
  if (btn == m_ui->btImperialMode) {
    selForm->DisplaySelection(itActive,(glob->mstCfg.imperialMode == true ? actYes : actNo));
    selForm->show();
  }

  /*----- BUTTON : units Kg/H mode enabled -----*/
  if (btn == m_ui->btKgH_Mode) {
    selForm->DisplaySelection(itActive,(glob->mstCfg.units_kgh == true ? actYes : actNo));
    selForm->show();
  }

  /*----- BUTTON : Recipe enable -----*/
  if (btn == m_ui->btRcpEnable) {
    selForm->DisplaySelection(itActive,(glob->mstCfg.rcpEnable == true ? actYes : actNo));
    selForm->show();
  }

  /*----- BUTTON : Startup level -----*/
  if (btn == m_ui->btStartupLogin) {
    /* There is a difference between all available userlevels and the selectable startup levels,
       so a conversion is needed from enumerated type: utXXXX (userLevelType)
       to enumerated type: usXXXX (userSelectionType) */
    switch (glob->mstCfg.defLogin) {
    case utOperator:
      selForm->DisplaySelection(itUserSelect,usOperator);
      break;
    case utTooling:
      selForm->DisplaySelection(itUserSelect,usTooling);
      break;
    case utSupervisor:
      selForm->DisplaySelection(itUserSelect,usSupervisor);
      break;
    default:
      selForm->DisplaySelection(itUserSelect,usOperator);
      break;
    }
    selForm->show();
  }

  /*----- BUTTON : Language -----*/
  if (btn == m_ui->btLanguage) {
    selForm->DisplaySelection(itLanguages,glob->mstCfg.language);
    selForm->show();
  }

  /*----- BUTTON : Unit configuration -----*/
  if (btn == m_ui->btUnitConfig) {
    ((MainMenu*)(glob->menu))->DisplayWindow(wiUnitModeConfig);
  }
}


void AdvConfig2Frm::SelectionReceived(int _type,int _sel)
{
  /*---------------------------------------------------*/
  /* When the selection form is closed, the type of    */
  /* item and the selected item (index) is returned to */
  /* this routine.                                     */
  /* According to type and index, the currect values   */
  /* and button are updated.                           */
  /*---------------------------------------------------*/
  switch(_type) {
  case itType:
    glob->SetButtonImage(m_ui->btInput,_type,_sel);

    switch(_sel) {
    case ttRelay:
      glob->MstCfg_SetInpType(ttRelay);
      break;
    case ttTimer:
      glob->MstCfg_SetInpType(ttTimer);
      break;
    case ttTacho:
      glob->MstCfg_SetInpType(ttTacho);
      break;
    }

    break;

  case itMode:
    glob->SetButtonImage(m_ui->btProduction,_type,_sel);

    switch(_sel) {
    case modInject:
      glob->MstCfg_SetProdMode(modInject);
      if (glob->mstCfg.inpType == ttTacho) {
        /*----- if mode is injection, and inpType == ttTacho, force an update to inpType ttRelay -----*/
        SelectionReceived(itType,ttRelay);
      }
      break;
    case modExtrude:
      glob->MstCfg_SetProdMode(modExtrude);
      if (glob->mstCfg.inpType == ttTimer) {
        /*----- if mode is extrusion, and inpType == ttTimer, force an update to inpType ttRelay -----*/
        SelectionReceived(itType,ttRelay);
      }
      break;
    default:
      /*----- Default to injection, with check for input type -----*/
      glob->MstCfg_SetProdMode(modInject);
      if(glob->mstCfg.inpType == ttTacho) {
        SelectionReceived(itType,ttRelay);
      }
      break;
    }
    break;

  case itUserSelect:
    glob->SetButtonImage(m_ui->btStartupLogin,_type,_sel);

    switch(_sel) {
    case usOperator:
      glob->MstCfg_SetDefaultLogin(utOperator);
      break;
    case usTooling:
      glob->MstCfg_SetDefaultLogin(utTooling);
      break;
    case usSupervisor:
      glob->MstCfg_SetDefaultLogin(utSupervisor);
      break;
    default:
      glob->MstCfg_SetDefaultLogin(utOperator);
      break;
    }
    break;

  case itActive:

    glob->SetButtonImage(btn,_type,_sel);
    if (btn == m_ui->btAutoStart) {
      switch (_sel) {
      case actYes:
        glob->MstCfg_SetAutoStart(true);
        break;
      case actNo:
        glob->MstCfg_SetAutoStart(false);
        break;
      }
    }

    if (btn == m_ui->btRcpEnable) {
      switch (_sel) {
      case actYes:
        glob->MstCfg_SetRecipeEnable(true);
        break;
      case actNo:
        glob->MstCfg_SetRecipeEnable(false);
        break;
      }
    }

    if (btn == m_ui->btProfibusEnb) {
      switch (_sel) {
      case actYes:
        glob->MstCfg_ProfibusEnb(true);
        break;
      case actNo:
        glob->MstCfg_ProfibusEnb(false);
        break;
      }
    }

    if (btn == m_ui->btImperialMode) {
      switch (_sel) {
      case actYes:
        glob->MstCfg_ImperialMode(true);
        break;
      case actNo:
        glob->MstCfg_ImperialMode(false);
        break;
      }
    }

    if (btn == m_ui->btKgH_Mode) {
      switch (_sel) {
      case actYes:
        glob->MstCfg_KgH_Mode(true);
        break;
      case actNo:
        glob->MstCfg_KgH_Mode(false);
        break;
      }
    }
    break;

  case itLanguages:
    glob->SetButtonImage(m_ui->btLanguage,_type,_sel);

    if (_sel < (int)lanCount) {
      glob->MstCfg_SetLanguage((eLanguages)(_sel));
    }
    else {
      glob->MstCfg_SetLanguage(lanUK);
    }
    break;
  }
}

void AdvConfig2Frm::ValueReceived(float val)
{
  /*---------------------------------------------------*/
  /* valSelect was set when the numeric input form was */
  /* called.                                           */
  /* Use it to update the correct value.               */
  /*---------------------------------------------------*/
  switch(valSelect) {
  case 1:
    ToolPassReceived(val);
    break;
  case 2:
    SuperPassReceived(val);
    break;
  case 3:
    ModAddrReceived(val);
    break;
  case 4:
    /*----- Input is number of controllers, slave count = controllers-1 -----*/
    SlaveCountReceived((val-1));
    break;
  default:
    break;
  }
  /*----- Reset valSelect to 0, to prevent invalid input -----*/
  valSelect = 0;
}


void AdvConfig2Frm::FactoryDefaultsClicked()
{
  /*----------------------------------------------------*/
  /* Display a message dialog to ask if the user really */
  /* wants to reset to factory defaults.                */
  /* The result value of the dialog defines what to do, */
  /* so the MessageResult routine handles the final     */
  /* steps.                                             */
  /*----------------------------------------------------*/
  if (glob->mstSts.sysActive == false) {
    connect(((MainMenu*)(glob->menu)),SIGNAL(ErrorResult(int)),this,SLOT(MessageResult(int)));
    QString txt;
    ((MainMenu*)(glob->menu))->alarmHndlr->GetMessageText(TXT_INDEX_CONFIRM_FACTORY_DEFAULTS,0,0,&txt);
    ((MainMenu*)(glob->menu))->DisplayMessage(-1,0,txt,msgtConfirm);
  }
}


void AdvConfig2Frm::AlarmCfgClicked()
{
  /* Display the alarm configuration form */
  ((MainMenu*)(glob->menu))->DisplayWindow(wiAlarmConfig);
}


void AdvConfig2Frm::MessageResult(int res)
{
  /*---------------------------------------------------------------------------*/
  /* MessageResult handles "Reset to factory defaults" depending on user input */
  /*---------------------------------------------------------------------------*/

  /*----- Disconnect the ErrorResult from this routine so it won't be called when the message dialog is used in another routine. -----*/
  disconnect(((MainMenu*)(glob->menu)),SIGNAL(ErrorResult(int)),this,SLOT(MessageResult(int)));
  if (res == 1) {                                                               /* Ok clicked */

    /*----- Add "Reset to factory defaults" event to the eventLog -----*/
    glob->AddEventItem(-1,2,TXT_INDEX_FACTORY_DEFAULTS,0,0);

    /*----- Overwrite all system settings (each unit) with factory defaults and trigger an update. -----*/
    for (int i=0;i<CFG_MAX_UNIT_COUNT;i++) {
      ((MainMenu*)(glob->menu))->fileSysCtrl.StoreSystemFactDefaults(glob,i);

      /*----- TriggerUpdate only for configured nr. of units -----*/
      if (i <= glob->mstCfg.slaveCount) {
        glob->TriggerUpdate(i);
      }
    }

    /*----- Overwrite master settings with factory defaults. -----*/
    int tmp;
    ((MainMenu*)(glob->menu))->fileSysCtrl.StoreMasterFactDefaults();
    ((MainMenu*)(glob->menu))->fileSysCtrl.LoadMasterSettings(&(glob->mstCfg),tmp);

    /*----- Configure network -----*/
    Rout::ConfigureNetwork();

    factDefTim.start();
  }
}


/*-------------------------------------------------------------------------------*/
/* Ethernet network setting(s) changed by IpEditor from                          */
/*-------------------------------------------------------------------------------*/
void AdvConfig2Frm::IpReceived(int mode, int val1, int val2, int val3, int val4)
{

  /*--- Update mstCfg ---*/
  switch (mode) {
  case IP_EDIT_MODE_IP_ADDR:
    glob->mstCfg.IPAddress[0]=val4;
    glob->mstCfg.IPAddress[1]=val3;
    glob->mstCfg.IPAddress[2]=val2;
    glob->mstCfg.IPAddress[3]=val1;
    break;

  case IP_EDIT_MODE_NETMASK:
    glob->mstCfg.Netmask[0]=val4;
    glob->mstCfg.Netmask[1]=val3;
    glob->mstCfg.Netmask[2]=val2;
    glob->mstCfg.Netmask[3]=val1;
    break;

  case IP_EDIT_MODE_GATEWAY:
    glob->mstCfg.Gateway[0]=val4;
    glob->mstCfg.Gateway[1]=val3;
    glob->mstCfg.Gateway[2]=val2;
    glob->mstCfg.Gateway[3]=val1;
    break;

  }

  /*--- Save ---*/
  glob->MstCfg_SaveSettings();

  /*--- Change network configuration ---*/
  Rout::ConfigureNetwork();

  /*--- Update buttons ---*/
  switch (mode) {
  case IP_EDIT_MODE_IP_ADDR:
    m_ui->btIP->setText(QString::number(glob->mstCfg.IPAddress[3])+ "." + QString::number(glob->mstCfg.IPAddress[2])+ "." + QString::number(glob->mstCfg.IPAddress[1])+ "." + QString::number(glob->mstCfg.IPAddress[0]));
    break;

  case IP_EDIT_MODE_NETMASK:
    m_ui->btNetmask->setText(QString::number(glob->mstCfg.Netmask[3])+ "." + QString::number(glob->mstCfg.Netmask[2])+ "." + QString::number(glob->mstCfg.Netmask[1])+ "." + QString::number(glob->mstCfg.Netmask[0]));
    break;

  case IP_EDIT_MODE_GATEWAY:
    m_ui->btGateway->setText(QString::number(glob->mstCfg.Gateway[3])+ "." + QString::number(glob->mstCfg.Gateway[2])+ "." + QString::number(glob->mstCfg.Gateway[1])+ "." + QString::number(glob->mstCfg.Gateway[0]));
    break;

  }

}


void AdvConfig2Frm::LanguageChanged()
{
  /* If there was a language change, update all button images */
  SetButtonImages();
}


void AdvConfig2Frm::MstCfg_InputTypeChanged(eType _type)
{
  glob->SetButtonImage(m_ui->btInput,itType,_type);
}


void AdvConfig2Frm::MstCfg_DateTimeChanged(QDateTime date)
{
  m_ui->btDateTime->setText(date.toString(FORMAT_DATETIME));
}

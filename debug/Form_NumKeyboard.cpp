#include "Form_NumKeyboard.h"
#include "ui_Form_NumKeyboard.h"
#include "ImageDef.h"
#include "Form_MainMenu.h"
#include "Rout.h"

NumKeyboardFrm::NumKeyboardFrm(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::NumKeyboardFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  isPass = false;
  firstInput = true;
  m_ui->setupUi(this);

  // Numeric Keys
  connect(m_ui->bt0,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt1,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt2,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt3,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt4,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt5,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt6,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt7,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt8,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(m_ui->bt9,SIGNAL(clicked()),this,SLOT(ButtonClicked()));

  // "Special" buttons
  connect(m_ui->btDot,SIGNAL(clicked()),this,SLOT(DotClicked()));
  connect(m_ui->btSign,SIGNAL(clicked()),this,SLOT(SignClicked()));
  connect(m_ui->btBack,SIGNAL(clicked()),this,SLOT(BackspaceClicked()));
  connect(m_ui->btClear,SIGNAL(clicked()),this,SLOT(ClearClicked()));

  // Ok/Cancel
  connect(m_ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(m_ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));

  /*----- Disable windows title bar -----*/
  this->setWindowFlags(Qt::CustomizeWindowHint);

}


NumKeyboardFrm::~NumKeyboardFrm()
{
  delete m_ui;
}


void NumKeyboardFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      m_ui->retranslateUi(this);
      break;
    default:
      break;
  }
}


void NumKeyboardFrm::showEvent(QShowEvent *e)
{
  QWidget::showEvent(e);
  glob->mstSts.popupActive = true;

  if ((glob->menu) != 0) {
    ((MainMenu*)(glob->menu))->FoldMenu();
  }
}


void NumKeyboardFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  setPalette(*(glob->Palette()));

  glob->SetButtonImage(m_ui->btOk,itSelection,selOk);
  glob->SetButtonImage(m_ui->btCancel,itSelection,selCancel);
  glob->SetButtonImage(m_ui->btClear,itGeneral,genInpClear2);
  glob->SetButtonImage(m_ui->btBack,itGeneral,genInpBackspace2);

  /*
    m_ui->bt0->setFont(*(glob->baseFont));
    m_ui->bt1->setFont(*(glob->baseFont));
    m_ui->bt2->setFont(*(glob->baseFont));
    m_ui->bt3->setFont(*(glob->baseFont));
    m_ui->bt4->setFont(*(glob->baseFont));
    m_ui->bt5->setFont(*(glob->baseFont));
    m_ui->bt6->setFont(*(glob->baseFont));
    m_ui->bt7->setFont(*(glob->baseFont));
    m_ui->bt8->setFont(*(glob->baseFont));
    m_ui->bt9->setFont(*(glob->baseFont));
  m_ui->btBack->setFont(*(glob->baseFont));
  m_ui->btClear->setFont(*(glob->baseFont));
  m_ui->btDot->setFont(*(glob->baseFont));
  m_ui->btSign->setFont(*(glob->baseFont));
  */
}


void NumKeyboardFrm::Initialize(bool isFloat,int decimals,float startValue,float minValue,float maxValue,bool isPass)
{
  firstInput = true;
  this->isPass = isPass;
  if (isPass) {
    this->decimals = 0;
    this->isFloat = false;
  }
  else {
    this->isFloat = isFloat;
    this->decimals = decimals;
  }
  value = startValue;
  dispText = "";
  this->minValue = minValue;
  this->maxValue = maxValue;

  if (!(isFloat)) {
    this->decimals = 0;
  }

  if ((minValue >= 0) && (maxValue > 0)) {
    m_ui->btSign->setVisible(false);
  }
  else {
    if ((minValue < 0) && (maxValue < 0)) {
      m_ui->btSign->setVisible(false);
    }
    else {
      m_ui->btSign->setVisible(true);
    }
  }

  if (decimals == 0) {
    m_ui->btDot->setVisible(false);
  }
  else {
    m_ui->btDot->setVisible(true);
  }

  ValueToScreen(false);
}


void NumKeyboardFrm::Clear()
{
  this->ClearClicked();
}


void NumKeyboardFrm::ValueToScreen(bool preserveDot)
{
  QString str;

  if (preserveDot) {
    QString dot = ".";
    int dotIdx = m_ui->inputValue->text().indexOf(dot);
    int dec;
    if (dotIdx > 0) {
      int strLen = m_ui->inputValue->text().length();
      dec = (strLen-dotIdx)-1;
    }
    else {
      dec = 0;
      hasDot = false;
    }
    str = QString("%1").arg( value,0,'f',dec);
  }
  else {
    if (isFloat) {
      str = QString("%1").arg( value,0,'f',decimals);
    }
    else {
      str = QString("%1").arg( value,0,'f',0);
    }

    if ((isFloat) && (decimals > 0)) {
      hasDot = true;
    }
    else {
      hasDot = false;
    }
  }

  if (!isPass) {
    m_ui->inputValue->setText(str);
  }
  else {
    int len = dispText.length();
    str = "";

    for (int i=0;i<len;i++) {
      str.append("*");
    }
    m_ui->inputValue->setText(str);
  }
}


// Private slots:
void NumKeyboardFrm::ButtonClicked()
{
  if (firstInput) {
    Clear();
    firstInput = false;
  }

  QPushButton *button = qobject_cast<QPushButton*>(sender());
  QString butText = button->text();
  if(m_ui->inputValue->text() == "0") {
    if (butText != ".") {
      m_ui->inputValue->setText(butText);
    }
    else {
      hasDot = true;
      m_ui->inputValue->setText(m_ui->inputValue->text()+butText);
    }
  }
  else {
    if ((butText == ".") && ((!(hasDot)) && (isFloat))) {
      hasDot = true;
      m_ui->inputValue->setText(m_ui->inputValue->text()+butText);
    }
    else {
      if (!hasDot) {
        if (!isPass) {
          m_ui->inputValue->setText(m_ui->inputValue->text()+butText);
        }
        else {
          dispText.append(butText);
          m_ui->inputValue->setText(m_ui->inputValue->text()+"*");
        }
      }
      else {
        QString dot = ".";
        int dotIdx,strLen;
        // Check length of the string after the decimal seperator (dot)
        // If length is equal to numer of decimals allowed, ignore input
        dotIdx = m_ui->inputValue->text().indexOf(dot);
        strLen = m_ui->inputValue->text().length();
        if ((strLen-dotIdx) <= decimals) {
          if (!isPass) {
            m_ui->inputValue->setText(m_ui->inputValue->text()+butText);
          }
          else {
            dispText.append(butText);
            m_ui->inputValue->setText(m_ui->inputValue->text()+"*");
          }
        }
      }
    }
  }

  if (!isPass) {
    value = m_ui->inputValue->text().toDouble();
    /*
            if (value > maxValue)
            {
                value = maxValue;
                ValueToScreen(false);
            }
            if (value < minValue)
            {
                value = minValue;
                ValueToScreen(false);
            }
    */
  }
}


void NumKeyboardFrm::DotClicked()
{
  if (!(hasDot)) {
    m_ui->inputValue->setText(m_ui->inputValue->text() + ".");
    hasDot = true;
  }
}


void NumKeyboardFrm::ClearClicked()
{
  if (!isPass) {
    m_ui->inputValue->setText("0");
  }
  else {
    m_ui->inputValue->setText("");
  }
  value = 0;
  dispText = "";
  hasDot = false;
}


void NumKeyboardFrm::BackspaceClicked()
{
  if ((m_ui->inputValue->text() != "0") && (m_ui->inputValue->text() != "")) {
    if (m_ui->inputValue->text().right(1) == ".") {
      hasDot = false;
    }

    m_ui->inputValue->setText(m_ui->inputValue->text().left(m_ui->inputValue->text().length()-1));

    if (isPass) {
      dispText = dispText.left(dispText.length()-1);
    }
    else {
      if (m_ui->inputValue->text() == "") {
        m_ui->inputValue->setText("0");
      }
    }
    value /= 10;
  }
}


void NumKeyboardFrm::SignClicked()
{
  value *= -1;
  if (value < minValue) {
    value = minValue;
  }
  if (value > maxValue) {
    value = maxValue;
  }
  ValueToScreen(true);
}


bool NumKeyboardFrm::RangeCheck(float val)
{
  if ((val >= minValue) &&
  (val <= maxValue)) {
    return true;
  }
  else return false;
}


void NumKeyboardFrm::OkClicked()
{
  if (!isPass) {
    // RangeCheck????
    if (RangeCheck(value)) {
      emit InputValue(value);
      CancelClicked();
    }
    else {
      firstInput = true;
      if (value < minValue) {
        value = minValue;
        ValueToScreen(false);
      }
      else {
        value = maxValue;
        ValueToScreen(false);
      }
    }
  }
  else {
    emit PassInput(dispText);
    CancelClicked();
  }
}


void NumKeyboardFrm::CancelClicked()
{
  glob->mstSts.popupActive = false;
  this->close();
}

/********************************************************************************
** Form generated from reading UI file 'Form_DateTimeEditor.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_DATETIMEEDITOR_H
#define UI_FORM_DATETIMEEDITOR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStackedWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DateTimeEditorFrm
{
public:
    QVBoxLayout *verticalLayout_9;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_7;
    QStackedWidget *pageControl;
    QWidget *pageTime;
    QVBoxLayout *verticalLayout_8;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_16;
    QVBoxLayout *VertLayoutHour;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *btHourUp;
    QSpacerItem *horizontalSpacer_7;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_3;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_7;
    QLineEdit *edHours;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_8;
    QPushButton *btHourDown;
    QSpacerItem *horizontalSpacer_9;
    QLabel *label_3;
    QVBoxLayout *VertLayoutMinute;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_10;
    QPushButton *btMinUp;
    QSpacerItem *horizontalSpacer_11;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_14;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_8;
    QLineEdit *edMinutes;
    QSpacerItem *horizontalSpacer_15;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_12;
    QPushButton *btMinDown;
    QSpacerItem *horizontalSpacer_13;
    QSpacerItem *horizontalSpacer_17;
    QWidget *pageDate;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_18;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *VertLayoutDay;
    QHBoxLayout *horizontalLayout_12;
    QSpacerItem *horizontalSpacer_24;
    QPushButton *btDayUp;
    QSpacerItem *horizontalSpacer_25;
    QHBoxLayout *horizontalLayout_14;
    QSpacerItem *horizontalSpacer_28;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_5;
    QLineEdit *edDay;
    QSpacerItem *horizontalSpacer_29;
    QHBoxLayout *horizontalLayout_13;
    QSpacerItem *horizontalSpacer_26;
    QPushButton *btDayDown;
    QSpacerItem *horizontalSpacer_27;
    QLabel *label;
    QVBoxLayout *VertLayoutMonth;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_18;
    QPushButton *btMonUp;
    QSpacerItem *horizontalSpacer_19;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *horizontalSpacer_20;
    QVBoxLayout *verticalLayout;
    QLabel *label_4;
    QLineEdit *edMonth;
    QSpacerItem *horizontalSpacer_21;
    QHBoxLayout *horizontalLayout_11;
    QSpacerItem *horizontalSpacer_22;
    QPushButton *btMonDown;
    QSpacerItem *horizontalSpacer_23;
    QLabel *label_2;
    QVBoxLayout *VertLayoutYear;
    QHBoxLayout *horizontalLayout_15;
    QSpacerItem *horizontalSpacer_30;
    QPushButton *btYearUp;
    QSpacerItem *horizontalSpacer_31;
    QHBoxLayout *horizontalLayout_16;
    QSpacerItem *horizontalSpacer_32;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_6;
    QLineEdit *edYear;
    QSpacerItem *horizontalSpacer_33;
    QHBoxLayout *horizontalLayout_17;
    QSpacerItem *horizontalSpacer_34;
    QPushButton *btYearDown;
    QSpacerItem *horizontalSpacer_35;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *btOk;
    QPushButton *btCancel;

    void setupUi(QWidget *DateTimeEditorFrm)
    {
        if (DateTimeEditorFrm->objectName().isEmpty())
            DateTimeEditorFrm->setObjectName(QString::fromUtf8("DateTimeEditorFrm"));
        DateTimeEditorFrm->setWindowModality(Qt::ApplicationModal);
        DateTimeEditorFrm->resize(452, 377);
        DateTimeEditorFrm->setCursor(QCursor(Qt::BlankCursor));
        DateTimeEditorFrm->setWindowTitle(QString::fromUtf8("Form"));
        DateTimeEditorFrm->setStyleSheet(QString::fromUtf8("#groupBox{border: 2px solid black; border-radius:3px; }"));
        verticalLayout_9 = new QVBoxLayout(DateTimeEditorFrm);
        verticalLayout_9->setContentsMargins(0, 0, 0, 0);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        groupBox = new QGroupBox(DateTimeEditorFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setTitle(QString::fromUtf8(""));
        verticalLayout_7 = new QVBoxLayout(groupBox);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        pageControl = new QStackedWidget(groupBox);
        pageControl->setObjectName(QString::fromUtf8("pageControl"));
        pageTime = new QWidget();
        pageTime->setObjectName(QString::fromUtf8("pageTime"));
        verticalLayout_8 = new QVBoxLayout(pageTime);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(0);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalSpacer_16 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_16);

        VertLayoutHour = new QVBoxLayout();
        VertLayoutHour->setSpacing(0);
        VertLayoutHour->setObjectName(QString::fromUtf8("VertLayoutHour"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);

        btHourUp = new QPushButton(pageTime);
        btHourUp->setObjectName(QString::fromUtf8("btHourUp"));
        btHourUp->setMinimumSize(QSize(90, 70));
        btHourUp->setMaximumSize(QSize(90, 70));
        btHourUp->setText(QString::fromUtf8("hUp"));
        btHourUp->setAutoRepeat(true);
        btHourUp->setAutoRepeatInterval(200);

        horizontalLayout_2->addWidget(btHourUp);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_7);


        VertLayoutHour->addLayout(horizontalLayout_2);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(0);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_7 = new QLabel(pageTime);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_7);

        edHours = new QLineEdit(pageTime);
        edHours->setObjectName(QString::fromUtf8("edHours"));
        edHours->setMinimumSize(QSize(110, 70));
        edHours->setMaximumSize(QSize(110, 70));
        edHours->setFocusPolicy(Qt::NoFocus);
        edHours->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(edHours);


        horizontalLayout_8->addLayout(verticalLayout_4);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_4);


        VertLayoutHour->addLayout(horizontalLayout_8);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(0);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_8);

        btHourDown = new QPushButton(pageTime);
        btHourDown->setObjectName(QString::fromUtf8("btHourDown"));
        btHourDown->setMinimumSize(QSize(90, 70));
        btHourDown->setMaximumSize(QSize(90, 70));
        btHourDown->setText(QString::fromUtf8("hDown"));
        btHourDown->setAutoRepeat(true);
        btHourDown->setAutoRepeatInterval(200);

        horizontalLayout_4->addWidget(btHourDown);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_9);


        VertLayoutHour->addLayout(horizontalLayout_4);


        horizontalLayout_9->addLayout(VertLayoutHour);

        label_3 = new QLabel(pageTime);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font;
        font.setPointSize(20);
        font.setBold(true);
        font.setWeight(75);
        label_3->setFont(font);
        label_3->setText(QString::fromUtf8(":"));

        horizontalLayout_9->addWidget(label_3);

        VertLayoutMinute = new QVBoxLayout();
        VertLayoutMinute->setSpacing(0);
        VertLayoutMinute->setObjectName(QString::fromUtf8("VertLayoutMinute"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(0);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_10);

        btMinUp = new QPushButton(pageTime);
        btMinUp->setObjectName(QString::fromUtf8("btMinUp"));
        btMinUp->setMinimumSize(QSize(90, 70));
        btMinUp->setMaximumSize(QSize(90, 70));
        btMinUp->setText(QString::fromUtf8("minUp"));
        btMinUp->setAutoRepeat(true);
        btMinUp->setAutoRepeatInterval(200);

        horizontalLayout_5->addWidget(btMinUp);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_11);


        VertLayoutMinute->addLayout(horizontalLayout_5);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(0);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_14);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        label_8 = new QLabel(pageTime);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(label_8);

        edMinutes = new QLineEdit(pageTime);
        edMinutes->setObjectName(QString::fromUtf8("edMinutes"));
        edMinutes->setMinimumSize(QSize(110, 70));
        edMinutes->setMaximumSize(QSize(110, 70));
        edMinutes->setFocusPolicy(Qt::NoFocus);
        edMinutes->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(edMinutes);


        horizontalLayout_7->addLayout(verticalLayout_5);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_15);


        VertLayoutMinute->addLayout(horizontalLayout_7);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(0);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_12);

        btMinDown = new QPushButton(pageTime);
        btMinDown->setObjectName(QString::fromUtf8("btMinDown"));
        btMinDown->setMinimumSize(QSize(90, 70));
        btMinDown->setMaximumSize(QSize(90, 70));
        btMinDown->setText(QString::fromUtf8("minDown"));
        btMinDown->setAutoRepeat(true);
        btMinDown->setAutoRepeatInterval(200);

        horizontalLayout_6->addWidget(btMinDown);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_13);


        VertLayoutMinute->addLayout(horizontalLayout_6);


        horizontalLayout_9->addLayout(VertLayoutMinute);

        horizontalSpacer_17 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_17);


        verticalLayout_8->addLayout(horizontalLayout_9);

        pageControl->addWidget(pageTime);
        pageDate = new QWidget();
        pageDate->setObjectName(QString::fromUtf8("pageDate"));
        verticalLayout_6 = new QVBoxLayout(pageDate);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_18->addItem(horizontalSpacer);

        VertLayoutDay = new QVBoxLayout();
        VertLayoutDay->setSpacing(0);
        VertLayoutDay->setObjectName(QString::fromUtf8("VertLayoutDay"));
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(0);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalSpacer_24 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_24);

        btDayUp = new QPushButton(pageDate);
        btDayUp->setObjectName(QString::fromUtf8("btDayUp"));
        btDayUp->setMinimumSize(QSize(90, 70));
        btDayUp->setMaximumSize(QSize(90, 70));
        btDayUp->setText(QString::fromUtf8("DayUp"));
        btDayUp->setAutoRepeat(true);
        btDayUp->setAutoRepeatInterval(200);

        horizontalLayout_12->addWidget(btDayUp);

        horizontalSpacer_25 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_25);


        VertLayoutDay->addLayout(horizontalLayout_12);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(0);
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        horizontalSpacer_28 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_28);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_5 = new QLabel(pageDate);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_5);

        edDay = new QLineEdit(pageDate);
        edDay->setObjectName(QString::fromUtf8("edDay"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(edDay->sizePolicy().hasHeightForWidth());
        edDay->setSizePolicy(sizePolicy);
        edDay->setMinimumSize(QSize(110, 70));
        edDay->setMaximumSize(QSize(110, 70));
        edDay->setFocusPolicy(Qt::NoFocus);
        edDay->setText(QString::fromUtf8(""));
        edDay->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(edDay);


        horizontalLayout_14->addLayout(verticalLayout_2);

        horizontalSpacer_29 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_29);


        VertLayoutDay->addLayout(horizontalLayout_14);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(0);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        horizontalSpacer_26 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_26);

        btDayDown = new QPushButton(pageDate);
        btDayDown->setObjectName(QString::fromUtf8("btDayDown"));
        btDayDown->setMinimumSize(QSize(90, 70));
        btDayDown->setMaximumSize(QSize(90, 70));
        btDayDown->setText(QString::fromUtf8("DayDown"));
        btDayDown->setAutoRepeat(true);
        btDayDown->setAutoRepeatInterval(200);

        horizontalLayout_13->addWidget(btDayDown);

        horizontalSpacer_27 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_27);


        VertLayoutDay->addLayout(horizontalLayout_13);


        horizontalLayout_18->addLayout(VertLayoutDay);

        label = new QLabel(pageDate);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);
        label->setText(QString::fromUtf8("/"));

        horizontalLayout_18->addWidget(label);

        VertLayoutMonth = new QVBoxLayout();
        VertLayoutMonth->setSpacing(0);
        VertLayoutMonth->setObjectName(QString::fromUtf8("VertLayoutMonth"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_18 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_18);

        btMonUp = new QPushButton(pageDate);
        btMonUp->setObjectName(QString::fromUtf8("btMonUp"));
        btMonUp->setMinimumSize(QSize(90, 70));
        btMonUp->setMaximumSize(QSize(90, 70));
        btMonUp->setText(QString::fromUtf8("MonthUp"));
        btMonUp->setAutoRepeat(true);
        btMonUp->setAutoRepeatInterval(200);

        horizontalLayout->addWidget(btMonUp);

        horizontalSpacer_19 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_19);


        VertLayoutMonth->addLayout(horizontalLayout);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(0);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalSpacer_20 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_20);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_4 = new QLabel(pageDate);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_4);

        edMonth = new QLineEdit(pageDate);
        edMonth->setObjectName(QString::fromUtf8("edMonth"));
        sizePolicy.setHeightForWidth(edMonth->sizePolicy().hasHeightForWidth());
        edMonth->setSizePolicy(sizePolicy);
        edMonth->setMinimumSize(QSize(110, 70));
        edMonth->setMaximumSize(QSize(110, 70));
        edMonth->setFocusPolicy(Qt::NoFocus);
        edMonth->setText(QString::fromUtf8(""));
        edMonth->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(edMonth);


        horizontalLayout_10->addLayout(verticalLayout);

        horizontalSpacer_21 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_21);


        VertLayoutMonth->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(0);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        horizontalSpacer_22 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_22);

        btMonDown = new QPushButton(pageDate);
        btMonDown->setObjectName(QString::fromUtf8("btMonDown"));
        btMonDown->setMinimumSize(QSize(90, 70));
        btMonDown->setMaximumSize(QSize(90, 70));
        btMonDown->setText(QString::fromUtf8("MonthDown"));
        btMonDown->setAutoRepeat(true);
        btMonDown->setAutoRepeatInterval(200);

        horizontalLayout_11->addWidget(btMonDown);

        horizontalSpacer_23 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_23);


        VertLayoutMonth->addLayout(horizontalLayout_11);


        horizontalLayout_18->addLayout(VertLayoutMonth);

        label_2 = new QLabel(pageDate);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);
        label_2->setText(QString::fromUtf8("/"));

        horizontalLayout_18->addWidget(label_2);

        VertLayoutYear = new QVBoxLayout();
        VertLayoutYear->setSpacing(0);
        VertLayoutYear->setObjectName(QString::fromUtf8("VertLayoutYear"));
        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(0);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        horizontalSpacer_30 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_30);

        btYearUp = new QPushButton(pageDate);
        btYearUp->setObjectName(QString::fromUtf8("btYearUp"));
        btYearUp->setMinimumSize(QSize(90, 70));
        btYearUp->setMaximumSize(QSize(90, 70));
        btYearUp->setText(QString::fromUtf8("YearUp"));
        btYearUp->setAutoRepeat(true);
        btYearUp->setAutoRepeatInterval(200);

        horizontalLayout_15->addWidget(btYearUp);

        horizontalSpacer_31 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_31);


        VertLayoutYear->addLayout(horizontalLayout_15);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(0);
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        horizontalSpacer_32 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_16->addItem(horizontalSpacer_32);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_6 = new QLabel(pageDate);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_6);

        edYear = new QLineEdit(pageDate);
        edYear->setObjectName(QString::fromUtf8("edYear"));
        sizePolicy.setHeightForWidth(edYear->sizePolicy().hasHeightForWidth());
        edYear->setSizePolicy(sizePolicy);
        edYear->setMinimumSize(QSize(110, 70));
        edYear->setMaximumSize(QSize(110, 70));
        edYear->setFocusPolicy(Qt::NoFocus);
        edYear->setText(QString::fromUtf8(""));
        edYear->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(edYear);


        horizontalLayout_16->addLayout(verticalLayout_3);

        horizontalSpacer_33 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_16->addItem(horizontalSpacer_33);


        VertLayoutYear->addLayout(horizontalLayout_16);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(0);
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        horizontalSpacer_34 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_34);

        btYearDown = new QPushButton(pageDate);
        btYearDown->setObjectName(QString::fromUtf8("btYearDown"));
        btYearDown->setMinimumSize(QSize(90, 70));
        btYearDown->setMaximumSize(QSize(90, 70));
        btYearDown->setText(QString::fromUtf8("YearDown"));
        btYearDown->setAutoRepeat(true);
        btYearDown->setAutoRepeatInterval(200);

        horizontalLayout_17->addWidget(btYearDown);

        horizontalSpacer_35 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_35);


        VertLayoutYear->addLayout(horizontalLayout_17);


        horizontalLayout_18->addLayout(VertLayoutYear);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_18->addItem(horizontalSpacer_2);


        verticalLayout_6->addLayout(horizontalLayout_18);

        pageControl->addWidget(pageDate);

        verticalLayout_7->addWidget(pageControl);

        verticalSpacer = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        btOk = new QPushButton(groupBox);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("ok"));

        horizontalLayout_3->addWidget(btOk);

        btCancel = new QPushButton(groupBox);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("cancel"));

        horizontalLayout_3->addWidget(btCancel);


        verticalLayout_7->addLayout(horizontalLayout_3);


        verticalLayout_9->addWidget(groupBox);


        retranslateUi(DateTimeEditorFrm);

        pageControl->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(DateTimeEditorFrm);
    } // setupUi

    void retranslateUi(QWidget *DateTimeEditorFrm)
    {
        label_7->setText(QApplication::translate("DateTimeEditorFrm", "Hours", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("DateTimeEditorFrm", "Minutes", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("DateTimeEditorFrm", "Day", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("DateTimeEditorFrm", "Month", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("DateTimeEditorFrm", "Year", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(DateTimeEditorFrm);
    } // retranslateUi

};

namespace Ui {
    class DateTimeEditorFrm: public Ui_DateTimeEditorFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_DATETIMEEDITOR_H

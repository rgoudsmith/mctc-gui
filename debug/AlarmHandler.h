#ifndef ALARMHANDLER_H
#define ALARMHANDLER_H

#include <QObject>
#include <QList>
#include <QDateTime>
#include "Glob.h"

class AlarmHandler : public QObject
{
  Q_OBJECT
    public:
    explicit AlarmHandler(QObject *parent = 0);
    void FormInit();
    void GetAlarmText(int, float, QString *, bool);
    void GetMessageText(int, float, float, QString *);
    void GetEnumeratedText(eEnumType, int, QString *, int);
    void GetEnumeratedText(eEnumType, int, QString *);

  private:

  public slots:
    void AlarmModeChanged(int,int);
    void AlarmActiveChanged(bool,int);
    void AlarmReset(int);
    void AlarmResetAck(int);
    void ActiveAlarmHandler(void);
};
#endif                                                                          // ALARMHANDLER_H

#include "Form_IpEditor.h"
#include "ui_Form_IpEditor.h"
#include "Form_MainMenu.h"
#include "Rout.h"

IpEditorFrm::IpEditorFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::IpEditorFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  actField = 1;
  ipType = 0;

  firstInp = true;

  val1 = 0;
  val2 = 0;
  val3 = 0;
  val4 = 0;

  ui->setupUi(this);

  ui->edVal1->installEventFilter(this);
  ui->edVal2->installEventFilter(this);
  ui->edVal3->installEventFilter(this);
  ui->edVal4->installEventFilter(this);

  selBox = new QLabel(this);
  selBox->setFrameStyle(QFrame::Box);
  selBox->setLineWidth(2);

  /*----- Disable windows title bar -----*/
  this->setWindowFlags(Qt::CustomizeWindowHint);

  SetSelBox(actField);

  connect(ui->bt0,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(ui->bt1,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(ui->bt2,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(ui->bt3,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(ui->bt4,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(ui->bt5,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(ui->bt6,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(ui->bt7,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(ui->bt8,SIGNAL(clicked()),this,SLOT(ButtonClicked()));
  connect(ui->bt9,SIGNAL(clicked()),this,SLOT(ButtonClicked()));

  connect(ui->btDot,SIGNAL(clicked()),this,SLOT(DotClicked()));
  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));
  connect(ui->btBack,SIGNAL(clicked()),this,SLOT(BackClicked()));
}


IpEditorFrm::~IpEditorFrm()
{
  delete ui;
}


void IpEditorFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  ui->edVal1->setFont(*(glob->baseFont));
  ui->edVal2->setFont(*(glob->baseFont));
  ui->edVal3->setFont(*(glob->baseFont));
  ui->edVal4->setFont(*(glob->baseFont));

  ui->bt0->setFont(*(glob->captionFont));
  ui->bt1->setFont(*(glob->captionFont));
  ui->bt2->setFont(*(glob->captionFont));
  ui->bt3->setFont(*(glob->captionFont));
  ui->bt4->setFont(*(glob->captionFont));
  ui->bt5->setFont(*(glob->captionFont));
  ui->bt6->setFont(*(glob->captionFont));
  ui->bt7->setFont(*(glob->captionFont));
  ui->bt8->setFont(*(glob->captionFont));
  ui->bt9->setFont(*(glob->captionFont));
  ui->btDot->setFont(*(glob->captionFont));

  ui->label->setFont(*(glob->captionFont));
  ui->label_2->setFont(*(glob->captionFont));
  ui->label_3->setFont(*(glob->captionFont));

  glob->SetButtonImage(ui->btOk,itSelection,selOk);
  glob->SetButtonImage(ui->btCancel,itSelection,selCancel);

  glob->SetButtonImage(ui->btBack,itGeneral,genInpBackspace2);
}


bool IpEditorFrm::eventFilter(QObject *obj, QEvent *ev)
{
  int field = 0;

  if (ev->type() == QEvent::MouseButtonPress) {
    field = actField;
    if (obj == ui->edVal1) {
      if (actField != 0) {
        if (CheckInput(actField)) {
          actField = 1;
        }
      }
    }

    if (obj == ui->edVal2) {
      if (actField != 0) {
        if (CheckInput(actField)) {
          actField = 2;
        }
      }
    }

    if (obj == ui->edVal3) {
      if (actField != 0) {
        if (CheckInput(actField)) {
          actField = 3;
        }
      }
    }

    if (obj == ui->edVal4) {
      if (actField != 0) {
        if (CheckInput(actField)) {
          actField = 4;
        }
      }
    }

    if (field != actField) {
      firstInp = true;
    }
    DisplayValue(field);
    SetSelBox(actField);
  }
  return QBaseWidget::eventFilter(obj,ev);
}


void IpEditorFrm::resizeEvent(QResizeEvent *e)
{
  QBaseWidget::resizeEvent(e);
  SetSelBox(actField);
}


void IpEditorFrm::showEvent(QShowEvent *e)
{
  QBaseWidget::showEvent(e);

  if ((glob->menu) != 0) {
    ((MainMenu*)(glob->menu))->FoldMenu();
  }
  SetSelBox(actField);
}


void IpEditorFrm::SetSelBox(int field)
{
  QLineEdit * edit;
  switch(field) {
    case 1:
      edit = ui->edVal1;
      break;
    case 2:
      edit = ui->edVal2;
      break;
    case 3:
      edit = ui->edVal3;
      break;
    case 4:
      edit = ui->edVal4;
      break;
    default: edit = 0;
  }

  if (edit != 0) {
    selBox->setGeometry(QRect((edit->pos().x()-1),(edit->pos().y()-1),(edit->width()+2),(edit->height()+2)));
  }
}


bool IpEditorFrm::CheckInput(int field)
{
  int val;

  switch(field) {

    /*--- Field 1 ---*/
    case 1:
      val = ui->edVal1->text().toInt();
      if ((val >= 0) && (val <= 255)) {
        val1 = val;
        return true;
      }
      else return false;
      break;

      /*--- Field 2 ---*/
    case 2:
      val = ui->edVal2->text().toInt();
      if ((val >= 0) && (val <= 255)) {
        val2 = val;
        return true;
      }
      else return false;
      break;

      /*--- Field 3 ---*/
    case 3:
      val = ui->edVal3->text().toInt();
      if ((val >= 0) && (val <= 255)) {
        val3 = val;
        return true;
      }
      else return false;

      /*--- Field 4 ---*/
    case 4:
      val = ui->edVal4->text().toInt();

      if ((val >= 0) && (val <= 255)) {
        val4 = val;
        return true;
      }
      else return false;
      break;

    default: return false;
  }
}


void IpEditorFrm::ButtonClicked()
{
  QString txt;

  txt = "";
  switch (actField) {
    case 1:
      if (!firstInp) {
        txt = ui->edVal1->text();
      }

      if (txt.toInt() < 255) {
        txt = txt + ((QPushButton*)sender())->text();
      }

      if (txt.toInt() > 255) {
        txt = "255";
      }
      ui->edVal1->setText(txt);
      break;

    case 2:
      if (!firstInp) {
        txt = ui->edVal2->text();
      }

      if (txt.toInt() < 255) {
        txt = txt + ((QPushButton*)sender())->text();
      }

      if (txt.toInt() > 255) {
        txt = "255";
      }
      ui->edVal2->setText(txt);
      break;

    case 3:
      if (!firstInp) {
        txt = ui->edVal3->text();
      }

      if (txt.toInt() < 255) {
        txt = txt + ((QPushButton*)sender())->text();
      }

      if (txt.toInt() > 255) {
        txt = "255";
      }
      ui->edVal3->setText(txt);
      break;

    case 4:
      if (!firstInp) {
        txt = ui->edVal4->text();
      }

      if (txt.toInt() < 255) {
        txt = txt + ((QPushButton*)sender())->text();
      }

      if (txt.toInt() > 255) {
        txt = "255";
      }
      ui->edVal4->setText(txt);
      break;
  }
  firstInp = false;
}


void IpEditorFrm::OkClicked()
{
  bool inpOk = true;

  if (!CheckInput(1)) inpOk = false;
  if (!CheckInput(2)) inpOk = false;
  if (!CheckInput(3)) inpOk = false;
  if (!CheckInput(4)) inpOk = false;

  if (inpOk) {

    emit this->SendIp(ipType,val1,val2,val3,val4);
    CancelClicked();
  }
}


void IpEditorFrm::CancelClicked()
{
  actField = 1;
  firstInp = true;
  close();
}


void IpEditorFrm::DotClicked()
{
  // switch to next input field
  if (CheckInput(actField)) {
    if (actField < 4) {
      actField++;
      SetSelBox(actField);
      firstInp = true;
    }
  }
}


void IpEditorFrm::BackClicked()
{
  QString txt;

  switch(actField) {
    case 1:
      txt = ui->edVal1->text();
      txt = txt.left(txt.length()-1);
      if (txt == "") {
        txt = "0";
        firstInp = true;
      }
      ui->edVal1->setText(txt);
      break;
    case 2:
      txt = ui->edVal2->text();
      txt = txt.left(txt.length()-1);
      if (txt == "") {
        txt = "0";
        firstInp = true;
      }
      ui->edVal2->setText(txt);
      break;
    case 3:
      txt = ui->edVal3->text();
      txt = txt.left(txt.length()-1);
      if (txt == "") {
        txt = "0";
        firstInp = true;
      }
      ui->edVal3->setText(txt);
      break;
    case 4:
      txt = ui->edVal4->text();
      txt = txt.left(txt.length()-1);
      if (txt == "") {
        txt = "0";
        firstInp = true;
      }
      ui->edVal4->setText(txt);
      break;
  }
}


void IpEditorFrm::DisplayValue(int field)
{
  switch(field) {
    case 1:
      ui->edVal1->setText(QString::number(val1));
      break;
    case 2:
      ui->edVal2->setText(QString::number(val2));
      break;
    case 3:
      ui->edVal3->setText(QString::number(val3));
      break;
    case 4:
      ui->edVal4->setText(QString::number(val4));
      break;

    default: break;
  }
}


void IpEditorFrm::InitIp(int type, int ip1, int ip2, int ip3, int ip4)
{
  ipType = type;

  val1 = ip1;
  val2 = ip2;
  val3 = ip3;
  val4 = ip4;

  for (int i=1;i<5;i++) {
    DisplayValue(i);
  }
  SetSelBox(1);
}

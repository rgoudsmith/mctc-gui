/********************************************************************************
** Form generated from reading UI file 'Form_ProcessDiagnostic.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_PROCESSDIAGNOSTIC_H
#define UI_FORM_PROCESSDIAGNOSTIC_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ProcessDiagnosticFrm
{
public:
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btPrev;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLabel *lbMCID;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *btNext;
    QHBoxLayout *horizontalLayout_42;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QLabel *lbSts;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_5;
    QLabel *lbColPctSet;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_7;
    QLabel *lbColPctAct;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_9;
    QLabel *lbColPctVidAct;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_11;
    QLabel *lbCapDevAbs;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_13;
    QLabel *lbCapDevRel;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_15;
    QLabel *lbBalProdSts;
    QHBoxLayout *horizontalLayout_40;
    QLabel *label_74;
    QLabel *lbCorCyc;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_17;
    QLabel *lbCor;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_19;
    QLabel *lbRpmCor;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_21;
    QLabel *lbMotSpeedAct;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_29;
    QLabel *lbCorSts;
    QHBoxLayout *horizontalLayout_18;
    QLabel *label_31;
    QLabel *lbMcSts;
    QHBoxLayout *horizontalLayout_19;
    QLabel *label_33;
    QLabel *lbPutWeightCnt;
    QHBoxLayout *horizontalLayout_23;
    QLabel *label_41;
    QLabel *lbGetWeightCnt;
    QHBoxLayout *horizontalLayout_20;
    QLabel *label_35;
    QLabel *lbMeasLenSet;
    QHBoxLayout *horizontalLayout_21;
    QLabel *label_37;
    QLabel *lbMeasLenAct;
    QHBoxLayout *horizontalLayout_24;
    QLabel *label_43;
    QLabel *lbAlmCode;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_25;
    QLabel *lbHopWeight;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_4;
    QLabel *lbTachoRatio;
    QVBoxLayout *verticalLayout_5;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_25;
    QLabel *label_45;
    QLabel *lbActBndNr;
    QHBoxLayout *horizontalLayout_26;
    QLabel *label_47;
    QLabel *lbInBndCnt;
    QHBoxLayout *horizontalLayout_33;
    QLabel *label_60;
    QLabel *lbOutBndCnt;
    QHBoxLayout *horizontalLayout_34;
    QLabel *label_62;
    QLabel *lbMaxBndCnt;
    QHBoxLayout *horizontalLayout_35;
    QLabel *label_64;
    QLabel *lbBndFacAdj;
    QHBoxLayout *horizontalLayout_36;
    QLabel *label_66;
    QLabel *lbBndMinTimeCor;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_23;
    QLabel *lbMeasAct;
    QHBoxLayout *horizontalLayout_37;
    QLabel *label_68;
    QLabel *lbMeasAvg;
    QHBoxLayout *horizontalLayout_38;
    QLabel *label_70;
    QLabel *lbMeasDos;
    QHBoxLayout *horizontalLayout_39;
    QLabel *label_72;
    QLabel *lbMeasRstCnt;
    QSpacerItem *verticalSpacer_4;
    QVBoxLayout *verticalLayout_6;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_22;
    QLabel *label_2;
    QLabel *lbQrDeviation;
    QHBoxLayout *horizontalLayout_27;
    QLabel *label_40;
    QLabel *lbQrMeasAct;
    QHBoxLayout *horizontalLayout_30;
    QLabel *label_54;
    QLabel *lbQrMeasSet;
    QHBoxLayout *horizontalLayout_28;
    QLabel *label_50;
    QLabel *lbQrRetryCnt;
    QHBoxLayout *horizontalLayout_29;
    QLabel *label_52;
    QLabel *lbQrState;
    QHBoxLayout *horizontalLayout_31;
    QLabel *label_56;
    QLabel *lbQrPutWeight;
    QHBoxLayout *horizontalLayout_32;
    QLabel *label_58;
    QLabel *lbQrGetWeight;
    QSpacerItem *verticalSpacer_5;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btOk;

    void setupUi(QWidget *ProcessDiagnosticFrm)
    {
        if (ProcessDiagnosticFrm->objectName().isEmpty())
            ProcessDiagnosticFrm->setObjectName(QString::fromUtf8("ProcessDiagnosticFrm"));
        ProcessDiagnosticFrm->resize(861, 703);
        ProcessDiagnosticFrm->setCursor(QCursor(Qt::BlankCursor));
        ProcessDiagnosticFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout_7 = new QVBoxLayout(ProcessDiagnosticFrm);
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        btPrev = new QPushButton(ProcessDiagnosticFrm);
        btPrev->setObjectName(QString::fromUtf8("btPrev"));
        btPrev->setMinimumSize(QSize(90, 70));
        btPrev->setMaximumSize(QSize(90, 70));
        btPrev->setText(QString::fromUtf8("Prev"));

        horizontalLayout_2->addWidget(btPrev);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(ProcessDiagnosticFrm);
        label->setObjectName(QString::fromUtf8("label"));
        label->setText(QString::fromUtf8("Device ID:"));

        horizontalLayout->addWidget(label);

        lbMCID = new QLabel(ProcessDiagnosticFrm);
        lbMCID->setObjectName(QString::fromUtf8("lbMCID"));
        lbMCID->setText(QString::fromUtf8("TextLabel"));

        horizontalLayout->addWidget(lbMCID);


        horizontalLayout_2->addLayout(horizontalLayout);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        btNext = new QPushButton(ProcessDiagnosticFrm);
        btNext->setObjectName(QString::fromUtf8("btNext"));
        btNext->setMinimumSize(QSize(90, 70));
        btNext->setMaximumSize(QSize(90, 70));
        btNext->setText(QString::fromUtf8("Next"));

        horizontalLayout_2->addWidget(btNext);


        verticalLayout_7->addLayout(horizontalLayout_2);

        horizontalLayout_42 = new QHBoxLayout();
        horizontalLayout_42->setObjectName(QString::fromUtf8("horizontalLayout_42"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(1);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_3 = new QLabel(ProcessDiagnosticFrm);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(165, 0));
        label_3->setText(QString::fromUtf8("Unit Status:"));

        horizontalLayout_4->addWidget(label_3);

        lbSts = new QLabel(ProcessDiagnosticFrm);
        lbSts->setObjectName(QString::fromUtf8("lbSts"));
        lbSts->setText(QString::fromUtf8("lbSts"));
        lbSts->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(lbSts);


        verticalLayout_4->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_5 = new QLabel(ProcessDiagnosticFrm);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(165, 0));
        label_5->setText(QString::fromUtf8("Color capacity SET:"));

        horizontalLayout_5->addWidget(label_5);

        lbColPctSet = new QLabel(ProcessDiagnosticFrm);
        lbColPctSet->setObjectName(QString::fromUtf8("lbColPctSet"));
        lbColPctSet->setText(QString::fromUtf8("lbColPctSet"));
        lbColPctSet->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_5->addWidget(lbColPctSet);


        verticalLayout_4->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_7 = new QLabel(ProcessDiagnosticFrm);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(165, 0));
        label_7->setText(QString::fromUtf8("Color capacity ACT:"));

        horizontalLayout_6->addWidget(label_7);

        lbColPctAct = new QLabel(ProcessDiagnosticFrm);
        lbColPctAct->setObjectName(QString::fromUtf8("lbColPctAct"));
        lbColPctAct->setText(QString::fromUtf8("lbColPctAct"));
        lbColPctAct->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_6->addWidget(lbColPctAct);


        verticalLayout_4->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_9 = new QLabel(ProcessDiagnosticFrm);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(165, 0));
        label_9->setText(QString::fromUtf8("Vid. Color capacity ACT:"));
        label_9->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_7->addWidget(label_9);

        lbColPctVidAct = new QLabel(ProcessDiagnosticFrm);
        lbColPctVidAct->setObjectName(QString::fromUtf8("lbColPctVidAct"));
        lbColPctVidAct->setText(QString::fromUtf8("lbColPctVidAct"));
        lbColPctVidAct->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_7->addWidget(lbColPctVidAct);


        verticalLayout_4->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_11 = new QLabel(ProcessDiagnosticFrm);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setMinimumSize(QSize(165, 0));
        label_11->setText(QString::fromUtf8("Capacity deviation ABS:"));

        horizontalLayout_8->addWidget(label_11);

        lbCapDevAbs = new QLabel(ProcessDiagnosticFrm);
        lbCapDevAbs->setObjectName(QString::fromUtf8("lbCapDevAbs"));
        lbCapDevAbs->setText(QString::fromUtf8("lbCapDevAbs"));
        lbCapDevAbs->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_8->addWidget(lbCapDevAbs);


        verticalLayout_4->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_13 = new QLabel(ProcessDiagnosticFrm);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setMinimumSize(QSize(165, 0));
        label_13->setText(QString::fromUtf8("Capacity deviation REL:"));

        horizontalLayout_9->addWidget(label_13);

        lbCapDevRel = new QLabel(ProcessDiagnosticFrm);
        lbCapDevRel->setObjectName(QString::fromUtf8("lbCapDevRel"));
        lbCapDevRel->setText(QString::fromUtf8("lbCapDevRel"));
        lbCapDevRel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_9->addWidget(lbCapDevRel);


        verticalLayout_4->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_15 = new QLabel(ProcessDiagnosticFrm);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setMinimumSize(QSize(165, 0));
        label_15->setText(QString::fromUtf8("Bal. Production Status:"));

        horizontalLayout_10->addWidget(label_15);

        lbBalProdSts = new QLabel(ProcessDiagnosticFrm);
        lbBalProdSts->setObjectName(QString::fromUtf8("lbBalProdSts"));
        lbBalProdSts->setText(QString::fromUtf8("lbBalProdSts"));
        lbBalProdSts->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_10->addWidget(lbBalProdSts);


        verticalLayout_4->addLayout(horizontalLayout_10);

        horizontalLayout_40 = new QHBoxLayout();
        horizontalLayout_40->setObjectName(QString::fromUtf8("horizontalLayout_40"));
        label_74 = new QLabel(ProcessDiagnosticFrm);
        label_74->setObjectName(QString::fromUtf8("label_74"));
        label_74->setMinimumSize(QSize(165, 0));
        label_74->setText(QString::fromUtf8("Correction Cycle:"));

        horizontalLayout_40->addWidget(label_74);

        lbCorCyc = new QLabel(ProcessDiagnosticFrm);
        lbCorCyc->setObjectName(QString::fromUtf8("lbCorCyc"));
        lbCorCyc->setText(QString::fromUtf8("lbCorCyc"));
        lbCorCyc->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_40->addWidget(lbCorCyc);


        verticalLayout_4->addLayout(horizontalLayout_40);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        label_17 = new QLabel(ProcessDiagnosticFrm);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setMinimumSize(QSize(165, 0));
        label_17->setText(QString::fromUtf8("Correction:"));

        horizontalLayout_11->addWidget(label_17);

        lbCor = new QLabel(ProcessDiagnosticFrm);
        lbCor->setObjectName(QString::fromUtf8("lbCor"));
        lbCor->setText(QString::fromUtf8("lbCor"));
        lbCor->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_11->addWidget(lbCor);


        verticalLayout_4->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_19 = new QLabel(ProcessDiagnosticFrm);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setMinimumSize(QSize(165, 0));
        label_19->setText(QString::fromUtf8("RPM Correction:"));

        horizontalLayout_12->addWidget(label_19);

        lbRpmCor = new QLabel(ProcessDiagnosticFrm);
        lbRpmCor->setObjectName(QString::fromUtf8("lbRpmCor"));
        lbRpmCor->setText(QString::fromUtf8("lbRpmCor"));
        lbRpmCor->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_12->addWidget(lbRpmCor);


        verticalLayout_4->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label_21 = new QLabel(ProcessDiagnosticFrm);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setMinimumSize(QSize(165, 0));
        label_21->setText(QString::fromUtf8("Motor speed ACT:"));

        horizontalLayout_13->addWidget(label_21);

        lbMotSpeedAct = new QLabel(ProcessDiagnosticFrm);
        lbMotSpeedAct->setObjectName(QString::fromUtf8("lbMotSpeedAct"));
        lbMotSpeedAct->setText(QString::fromUtf8("lbMotSpeedAct"));
        lbMotSpeedAct->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_13->addWidget(lbMotSpeedAct);


        verticalLayout_4->addLayout(horizontalLayout_13);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        label_29 = new QLabel(ProcessDiagnosticFrm);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setMinimumSize(QSize(165, 0));
        label_29->setText(QString::fromUtf8("CorSts:"));

        horizontalLayout_17->addWidget(label_29);

        lbCorSts = new QLabel(ProcessDiagnosticFrm);
        lbCorSts->setObjectName(QString::fromUtf8("lbCorSts"));
        lbCorSts->setText(QString::fromUtf8("lbCorSts"));
        lbCorSts->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_17->addWidget(lbCorSts);


        verticalLayout_4->addLayout(horizontalLayout_17);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        label_31 = new QLabel(ProcessDiagnosticFrm);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setMinimumSize(QSize(165, 0));
        label_31->setText(QString::fromUtf8("McSts:"));

        horizontalLayout_18->addWidget(label_31);

        lbMcSts = new QLabel(ProcessDiagnosticFrm);
        lbMcSts->setObjectName(QString::fromUtf8("lbMcSts"));
        lbMcSts->setText(QString::fromUtf8("lbMcSts"));
        lbMcSts->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_18->addWidget(lbMcSts);


        verticalLayout_4->addLayout(horizontalLayout_18);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        label_33 = new QLabel(ProcessDiagnosticFrm);
        label_33->setObjectName(QString::fromUtf8("label_33"));
        label_33->setMinimumSize(QSize(165, 0));
        label_33->setText(QString::fromUtf8("Put weight counter:"));

        horizontalLayout_19->addWidget(label_33);

        lbPutWeightCnt = new QLabel(ProcessDiagnosticFrm);
        lbPutWeightCnt->setObjectName(QString::fromUtf8("lbPutWeightCnt"));
        lbPutWeightCnt->setText(QString::fromUtf8("lbPutWeightCnt"));
        lbPutWeightCnt->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_19->addWidget(lbPutWeightCnt);


        verticalLayout_4->addLayout(horizontalLayout_19);

        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setObjectName(QString::fromUtf8("horizontalLayout_23"));
        label_41 = new QLabel(ProcessDiagnosticFrm);
        label_41->setObjectName(QString::fromUtf8("label_41"));
        label_41->setMinimumSize(QSize(165, 0));
        label_41->setText(QString::fromUtf8("Get weight counter:"));

        horizontalLayout_23->addWidget(label_41);

        lbGetWeightCnt = new QLabel(ProcessDiagnosticFrm);
        lbGetWeightCnt->setObjectName(QString::fromUtf8("lbGetWeightCnt"));
        lbGetWeightCnt->setText(QString::fromUtf8("lbGetWeightCnt"));
        lbGetWeightCnt->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_23->addWidget(lbGetWeightCnt);


        verticalLayout_4->addLayout(horizontalLayout_23);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        label_35 = new QLabel(ProcessDiagnosticFrm);
        label_35->setObjectName(QString::fromUtf8("label_35"));
        label_35->setMinimumSize(QSize(165, 0));
        label_35->setText(QString::fromUtf8("Measurement length SET:"));

        horizontalLayout_20->addWidget(label_35);

        lbMeasLenSet = new QLabel(ProcessDiagnosticFrm);
        lbMeasLenSet->setObjectName(QString::fromUtf8("lbMeasLenSet"));
        lbMeasLenSet->setText(QString::fromUtf8("lbMeasLenSet"));
        lbMeasLenSet->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_20->addWidget(lbMeasLenSet);


        verticalLayout_4->addLayout(horizontalLayout_20);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        label_37 = new QLabel(ProcessDiagnosticFrm);
        label_37->setObjectName(QString::fromUtf8("label_37"));
        label_37->setMinimumSize(QSize(165, 0));
        label_37->setText(QString::fromUtf8("Measurement length ACT:"));

        horizontalLayout_21->addWidget(label_37);

        lbMeasLenAct = new QLabel(ProcessDiagnosticFrm);
        lbMeasLenAct->setObjectName(QString::fromUtf8("lbMeasLenAct"));
        lbMeasLenAct->setText(QString::fromUtf8("lbMeasLenAct"));
        lbMeasLenAct->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_21->addWidget(lbMeasLenAct);


        verticalLayout_4->addLayout(horizontalLayout_21);

        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setObjectName(QString::fromUtf8("horizontalLayout_24"));
        label_43 = new QLabel(ProcessDiagnosticFrm);
        label_43->setObjectName(QString::fromUtf8("label_43"));
        label_43->setMinimumSize(QSize(165, 0));
        label_43->setText(QString::fromUtf8("Alarm code:"));
        label_43->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_24->addWidget(label_43);

        lbAlmCode = new QLabel(ProcessDiagnosticFrm);
        lbAlmCode->setObjectName(QString::fromUtf8("lbAlmCode"));
        lbAlmCode->setText(QString::fromUtf8("lbAlmCode"));
        lbAlmCode->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_24->addWidget(lbAlmCode);


        verticalLayout_4->addLayout(horizontalLayout_24);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        label_25 = new QLabel(ProcessDiagnosticFrm);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setMinimumSize(QSize(165, 0));
        label_25->setText(QString::fromUtf8("Hopper weight:"));

        horizontalLayout_15->addWidget(label_25);

        lbHopWeight = new QLabel(ProcessDiagnosticFrm);
        lbHopWeight->setObjectName(QString::fromUtf8("lbHopWeight"));
        lbHopWeight->setText(QString::fromUtf8("lbHopWeight"));
        lbHopWeight->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_15->addWidget(lbHopWeight);


        verticalLayout_4->addLayout(horizontalLayout_15);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        label_4 = new QLabel(ProcessDiagnosticFrm);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_16->addWidget(label_4);

        lbTachoRatio = new QLabel(ProcessDiagnosticFrm);
        lbTachoRatio->setObjectName(QString::fromUtf8("lbTachoRatio"));
        lbTachoRatio->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_16->addWidget(lbTachoRatio);


        verticalLayout_4->addLayout(horizontalLayout_16);


        horizontalLayout_42->addLayout(verticalLayout_4);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        groupBox_2 = new QGroupBox(ProcessDiagnosticFrm);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setTitle(QString::fromUtf8("Band Data"));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setSpacing(1);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_25 = new QHBoxLayout();
        horizontalLayout_25->setObjectName(QString::fromUtf8("horizontalLayout_25"));
        label_45 = new QLabel(groupBox_2);
        label_45->setObjectName(QString::fromUtf8("label_45"));
        label_45->setMinimumSize(QSize(165, 0));
        label_45->setText(QString::fromUtf8("Act band nr:"));

        horizontalLayout_25->addWidget(label_45);

        lbActBndNr = new QLabel(groupBox_2);
        lbActBndNr->setObjectName(QString::fromUtf8("lbActBndNr"));
        lbActBndNr->setText(QString::fromUtf8("lbActBndNr"));
        lbActBndNr->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_25->addWidget(lbActBndNr);


        verticalLayout_2->addLayout(horizontalLayout_25);

        horizontalLayout_26 = new QHBoxLayout();
        horizontalLayout_26->setObjectName(QString::fromUtf8("horizontalLayout_26"));
        label_47 = new QLabel(groupBox_2);
        label_47->setObjectName(QString::fromUtf8("label_47"));
        label_47->setMinimumSize(QSize(165, 0));
        label_47->setText(QString::fromUtf8("In band cnt:"));

        horizontalLayout_26->addWidget(label_47);

        lbInBndCnt = new QLabel(groupBox_2);
        lbInBndCnt->setObjectName(QString::fromUtf8("lbInBndCnt"));
        lbInBndCnt->setText(QString::fromUtf8("lbInBndCnt"));
        lbInBndCnt->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_26->addWidget(lbInBndCnt);


        verticalLayout_2->addLayout(horizontalLayout_26);

        horizontalLayout_33 = new QHBoxLayout();
        horizontalLayout_33->setObjectName(QString::fromUtf8("horizontalLayout_33"));
        label_60 = new QLabel(groupBox_2);
        label_60->setObjectName(QString::fromUtf8("label_60"));
        label_60->setMinimumSize(QSize(165, 0));
        label_60->setText(QString::fromUtf8("Out band cnt:"));

        horizontalLayout_33->addWidget(label_60);

        lbOutBndCnt = new QLabel(groupBox_2);
        lbOutBndCnt->setObjectName(QString::fromUtf8("lbOutBndCnt"));
        lbOutBndCnt->setText(QString::fromUtf8("lbOutBndCnt"));
        lbOutBndCnt->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_33->addWidget(lbOutBndCnt);


        verticalLayout_2->addLayout(horizontalLayout_33);

        horizontalLayout_34 = new QHBoxLayout();
        horizontalLayout_34->setObjectName(QString::fromUtf8("horizontalLayout_34"));
        label_62 = new QLabel(groupBox_2);
        label_62->setObjectName(QString::fromUtf8("label_62"));
        label_62->setMinimumSize(QSize(165, 0));
        label_62->setText(QString::fromUtf8("Maximum cnt:"));

        horizontalLayout_34->addWidget(label_62);

        lbMaxBndCnt = new QLabel(groupBox_2);
        lbMaxBndCnt->setObjectName(QString::fromUtf8("lbMaxBndCnt"));
        lbMaxBndCnt->setText(QString::fromUtf8("lbMaxBndCnt"));
        lbMaxBndCnt->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_34->addWidget(lbMaxBndCnt);


        verticalLayout_2->addLayout(horizontalLayout_34);

        horizontalLayout_35 = new QHBoxLayout();
        horizontalLayout_35->setObjectName(QString::fromUtf8("horizontalLayout_35"));
        label_64 = new QLabel(groupBox_2);
        label_64->setObjectName(QString::fromUtf8("label_64"));
        label_64->setMinimumSize(QSize(165, 0));
        label_64->setText(QString::fromUtf8("Fac Adjust:"));

        horizontalLayout_35->addWidget(label_64);

        lbBndFacAdj = new QLabel(groupBox_2);
        lbBndFacAdj->setObjectName(QString::fromUtf8("lbBndFacAdj"));
        lbBndFacAdj->setText(QString::fromUtf8("lbBndFacAdj"));
        lbBndFacAdj->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_35->addWidget(lbBndFacAdj);


        verticalLayout_2->addLayout(horizontalLayout_35);

        horizontalLayout_36 = new QHBoxLayout();
        horizontalLayout_36->setObjectName(QString::fromUtf8("horizontalLayout_36"));
        label_66 = new QLabel(groupBox_2);
        label_66->setObjectName(QString::fromUtf8("label_66"));
        label_66->setMinimumSize(QSize(165, 0));
        label_66->setText(QString::fromUtf8("Min Time Cor.:"));

        horizontalLayout_36->addWidget(label_66);

        lbBndMinTimeCor = new QLabel(groupBox_2);
        lbBndMinTimeCor->setObjectName(QString::fromUtf8("lbBndMinTimeCor"));
        lbBndMinTimeCor->setText(QString::fromUtf8("lbBndMinTimeCor"));
        lbBndMinTimeCor->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_36->addWidget(lbBndMinTimeCor);


        verticalLayout_2->addLayout(horizontalLayout_36);


        verticalLayout_5->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(ProcessDiagnosticFrm);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setTitle(QString::fromUtf8("Metering time"));
        verticalLayout_3 = new QVBoxLayout(groupBox_3);
        verticalLayout_3->setSpacing(1);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_23 = new QLabel(groupBox_3);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setMinimumSize(QSize(120, 0));
        label_23->setText(QString::fromUtf8("Meas ACT:"));

        horizontalLayout_14->addWidget(label_23);

        lbMeasAct = new QLabel(groupBox_3);
        lbMeasAct->setObjectName(QString::fromUtf8("lbMeasAct"));
        lbMeasAct->setText(QString::fromUtf8("lbMeasAct"));
        lbMeasAct->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_14->addWidget(lbMeasAct);


        verticalLayout_3->addLayout(horizontalLayout_14);

        horizontalLayout_37 = new QHBoxLayout();
        horizontalLayout_37->setObjectName(QString::fromUtf8("horizontalLayout_37"));
        label_68 = new QLabel(groupBox_3);
        label_68->setObjectName(QString::fromUtf8("label_68"));
        label_68->setMinimumSize(QSize(120, 0));
        label_68->setText(QString::fromUtf8("Meas AVG:"));

        horizontalLayout_37->addWidget(label_68);

        lbMeasAvg = new QLabel(groupBox_3);
        lbMeasAvg->setObjectName(QString::fromUtf8("lbMeasAvg"));
        lbMeasAvg->setText(QString::fromUtf8("lbMeasAvg"));
        lbMeasAvg->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_37->addWidget(lbMeasAvg);


        verticalLayout_3->addLayout(horizontalLayout_37);

        horizontalLayout_38 = new QHBoxLayout();
        horizontalLayout_38->setObjectName(QString::fromUtf8("horizontalLayout_38"));
        label_70 = new QLabel(groupBox_3);
        label_70->setObjectName(QString::fromUtf8("label_70"));
        label_70->setMinimumSize(QSize(120, 0));
        label_70->setText(QString::fromUtf8("Meas DOS:"));

        horizontalLayout_38->addWidget(label_70);

        lbMeasDos = new QLabel(groupBox_3);
        lbMeasDos->setObjectName(QString::fromUtf8("lbMeasDos"));
        lbMeasDos->setText(QString::fromUtf8("lbMeasDos"));
        lbMeasDos->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_38->addWidget(lbMeasDos);


        verticalLayout_3->addLayout(horizontalLayout_38);

        horizontalLayout_39 = new QHBoxLayout();
        horizontalLayout_39->setObjectName(QString::fromUtf8("horizontalLayout_39"));
        label_72 = new QLabel(groupBox_3);
        label_72->setObjectName(QString::fromUtf8("label_72"));
        label_72->setMinimumSize(QSize(120, 0));
        label_72->setText(QString::fromUtf8("Reset Cnt:"));

        horizontalLayout_39->addWidget(label_72);

        lbMeasRstCnt = new QLabel(groupBox_3);
        lbMeasRstCnt->setObjectName(QString::fromUtf8("lbMeasRstCnt"));
        lbMeasRstCnt->setText(QString::fromUtf8("lbMeasRstCnt"));
        lbMeasRstCnt->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_39->addWidget(lbMeasRstCnt);


        verticalLayout_3->addLayout(horizontalLayout_39);


        verticalLayout_5->addWidget(groupBox_3);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_4);


        horizontalLayout_42->addLayout(verticalLayout_5);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        groupBox = new QGroupBox(ProcessDiagnosticFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setTitle(QString::fromUtf8("QR Data"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setSpacing(1);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setObjectName(QString::fromUtf8("horizontalLayout_22"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(100, 0));
        label_2->setText(QString::fromUtf8("Deviation:"));

        horizontalLayout_22->addWidget(label_2);

        lbQrDeviation = new QLabel(groupBox);
        lbQrDeviation->setObjectName(QString::fromUtf8("lbQrDeviation"));
        lbQrDeviation->setText(QString::fromUtf8("lbQrDeviation"));
        lbQrDeviation->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_22->addWidget(lbQrDeviation);


        verticalLayout->addLayout(horizontalLayout_22);

        horizontalLayout_27 = new QHBoxLayout();
        horizontalLayout_27->setObjectName(QString::fromUtf8("horizontalLayout_27"));
        label_40 = new QLabel(groupBox);
        label_40->setObjectName(QString::fromUtf8("label_40"));
        label_40->setMinimumSize(QSize(100, 0));
        label_40->setText(QString::fromUtf8("Meas time act:"));

        horizontalLayout_27->addWidget(label_40);

        lbQrMeasAct = new QLabel(groupBox);
        lbQrMeasAct->setObjectName(QString::fromUtf8("lbQrMeasAct"));
        lbQrMeasAct->setText(QString::fromUtf8("lbQrMeasAct"));
        lbQrMeasAct->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_27->addWidget(lbQrMeasAct);


        verticalLayout->addLayout(horizontalLayout_27);

        horizontalLayout_30 = new QHBoxLayout();
        horizontalLayout_30->setObjectName(QString::fromUtf8("horizontalLayout_30"));
        label_54 = new QLabel(groupBox);
        label_54->setObjectName(QString::fromUtf8("label_54"));
        label_54->setMinimumSize(QSize(100, 0));
        label_54->setText(QString::fromUtf8("Meas time set:"));

        horizontalLayout_30->addWidget(label_54);

        lbQrMeasSet = new QLabel(groupBox);
        lbQrMeasSet->setObjectName(QString::fromUtf8("lbQrMeasSet"));
        lbQrMeasSet->setText(QString::fromUtf8("lbQrMeasSet"));
        lbQrMeasSet->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_30->addWidget(lbQrMeasSet);


        verticalLayout->addLayout(horizontalLayout_30);

        horizontalLayout_28 = new QHBoxLayout();
        horizontalLayout_28->setObjectName(QString::fromUtf8("horizontalLayout_28"));
        label_50 = new QLabel(groupBox);
        label_50->setObjectName(QString::fromUtf8("label_50"));
        label_50->setMinimumSize(QSize(100, 0));
        label_50->setText(QString::fromUtf8("Retry cnt:"));

        horizontalLayout_28->addWidget(label_50);

        lbQrRetryCnt = new QLabel(groupBox);
        lbQrRetryCnt->setObjectName(QString::fromUtf8("lbQrRetryCnt"));
        lbQrRetryCnt->setText(QString::fromUtf8("lbQrRetryCnt"));
        lbQrRetryCnt->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_28->addWidget(lbQrRetryCnt);


        verticalLayout->addLayout(horizontalLayout_28);

        horizontalLayout_29 = new QHBoxLayout();
        horizontalLayout_29->setObjectName(QString::fromUtf8("horizontalLayout_29"));
        label_52 = new QLabel(groupBox);
        label_52->setObjectName(QString::fromUtf8("label_52"));
        label_52->setMinimumSize(QSize(100, 0));
        label_52->setText(QString::fromUtf8("State:"));

        horizontalLayout_29->addWidget(label_52);

        lbQrState = new QLabel(groupBox);
        lbQrState->setObjectName(QString::fromUtf8("lbQrState"));
        lbQrState->setText(QString::fromUtf8("lbQrState"));
        lbQrState->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_29->addWidget(lbQrState);


        verticalLayout->addLayout(horizontalLayout_29);

        horizontalLayout_31 = new QHBoxLayout();
        horizontalLayout_31->setObjectName(QString::fromUtf8("horizontalLayout_31"));
        label_56 = new QLabel(groupBox);
        label_56->setObjectName(QString::fromUtf8("label_56"));
        label_56->setMinimumSize(QSize(100, 0));
        label_56->setText(QString::fromUtf8("Put weight cnt:"));

        horizontalLayout_31->addWidget(label_56);

        lbQrPutWeight = new QLabel(groupBox);
        lbQrPutWeight->setObjectName(QString::fromUtf8("lbQrPutWeight"));
        lbQrPutWeight->setText(QString::fromUtf8("lbQrPutWeight"));
        lbQrPutWeight->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_31->addWidget(lbQrPutWeight);


        verticalLayout->addLayout(horizontalLayout_31);

        horizontalLayout_32 = new QHBoxLayout();
        horizontalLayout_32->setObjectName(QString::fromUtf8("horizontalLayout_32"));
        label_58 = new QLabel(groupBox);
        label_58->setObjectName(QString::fromUtf8("label_58"));
        label_58->setMinimumSize(QSize(100, 0));
        label_58->setText(QString::fromUtf8("Get weight cnt:"));

        horizontalLayout_32->addWidget(label_58);

        lbQrGetWeight = new QLabel(groupBox);
        lbQrGetWeight->setObjectName(QString::fromUtf8("lbQrGetWeight"));
        lbQrGetWeight->setText(QString::fromUtf8("lbQrGetWeight"));
        lbQrGetWeight->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_32->addWidget(lbQrGetWeight);


        verticalLayout->addLayout(horizontalLayout_32);


        verticalLayout_6->addWidget(groupBox);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_5);


        horizontalLayout_42->addLayout(verticalLayout_6);


        verticalLayout_7->addLayout(horizontalLayout_42);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        btOk = new QPushButton(ProcessDiagnosticFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout_3->addWidget(btOk);


        verticalLayout_7->addLayout(horizontalLayout_3);


        retranslateUi(ProcessDiagnosticFrm);

        QMetaObject::connectSlotsByName(ProcessDiagnosticFrm);
    } // setupUi

    void retranslateUi(QWidget *ProcessDiagnosticFrm)
    {
        label_4->setText(QApplication::translate("ProcessDiagnosticFrm", "Tacho ratio:", 0, QApplication::UnicodeUTF8));
        lbTachoRatio->setText(QApplication::translate("ProcessDiagnosticFrm", "lbTachoRatio", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(ProcessDiagnosticFrm);
    } // retranslateUi

};

namespace Ui {
    class ProcessDiagnosticFrm: public Ui_ProcessDiagnosticFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_PROCESSDIAGNOSTIC_H

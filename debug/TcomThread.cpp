#include "TcomThread.h"
#include "errno.h"

#define COM_DELAY_TIME 5
#define COM_MSG_SIZE 864
#define SRV_QUEUE_ID 1234
#define CLT_QUEUE_ID 4321

void Com::OpenMsgQueue()
{
  /*------------------------------------------------------------------*/
  /* Open a message queue to send data from and to the MC_TC_arm task */
  /*------------------------------------------------------------------*/
  this->srvQueueID = msgget((key_t)SRV_QUEUE_ID, 0666 | IPC_CREAT);
  this->cltQueueID = msgget((key_t)CLT_QUEUE_ID, 0666 | IPC_CREAT);
}


Com::Com(QObject *parent) :QObject(parent)
{
  /*----- Constuctor -----*/

  this->end = false;

  /*----- Clear incomming message -----*/
  this->msgIn.dosSet = 0;
  this->msgIn.dosAct = 0;
  this->msgIn.actRpm = 0;
  this->msgIn.weight = 0;
  for (int i=0; i<COM_FLOATBUF_SIZE; i++) {
    msgIn.floatBuf[i]=0;
  }
  for(int i=0; i<COM_BYTEBUF_SIZE; i++) {
    msgIn.byteBuf[i]=0;
  }

  /*----- Clear Outgoing message -----*/
  this->msgOut.colPct = 0;
  this->msgOut.KgH = 0;
  this->msgOut.primeRpm = 0;
  this->msgOut.sysStartCmd = 0;

  for (int i=0; i<COM_FLOATBUF_SIZE; i++) {
    msgOut.floatBuf[i]=0;
  }
  for(int i=0; i<COM_BYTEBUF_SIZE; i++) {
    msgOut.byteBuf[i]=0;
  }

  memset(&(msgOut.AppParameters),0x00,sizeof(AppParamStruct));

  // Other variables
  this->srvQueueID = -1;
  this->cltQueueID = -1;
  this->msgReceive.msg_id = getpid();
  this->msgSend.msg_id = msgReceive.msg_id;
  this->doSend = false;

  /*----- Open message queue -----*/
  OpenMsgQueue();

}


void Com::Terminate()
{
  this->end = true;
}


int t,r;

void Com::SendMsg(MsgDataOut data)
{

  memcpy(&msgOut,&data,sizeof(MsgDataOut));

  /*----- Copy message into the send buffer -----*/
  memcpy(&(msgSend.dosSet),&msgOut,sizeof(MsgDataOut));

  /*----- Try to send a message -----*/
  if (msgsnd(srvQueueID, (void*)&(this->msgSend),(sizeof(MsgStruct)-sizeof(long int)),0) <= -1) {
    /*----- If sending fails, send a msgQueue error signal -----*/
    emit this->MsgQueueError(ceNoSend);
  }
  else {
    if (msgOut.byteBuf[23] != 0) {
      qDebug("Msg TX %u buf[23]=%u",t,msgOut.byteBuf[23]);
    }
    t++;
    /* Clear message variable buffer !!!(NOT entire message struct)!!!
       else "long int msg_id" will be cleared. */
    this->msgReceive.dosSet = 0;
    this->msgReceive.dosAct = 0;
    this->msgReceive.actRpm = 0;
    this->msgReceive.weight = 0;
    this->msgReceive.shotWeightSet = 0;
    this->msgReceive.dosTimeSet = 0;
    this->msgReceive.prodType = 0;
    this->msgReceive.InjInputMode = 0;
    this->msgReceive.ExtInputMode = 0;
    this->msgReceive.OperatingMode = 0;
    this->msgReceive.calib = 0;

    for (int i=0; i<COM_FLOATBUF_SIZE; i++) {
      this->msgReceive.floatBuf[i] = 0;
    }
    for (int i=0; i<COM_BYTEBUF_SIZE; i++) {
      this->msgReceive.byteBuf[i] = 0;
    }
    memset(&(msgReceive.AppParameters),0x00,sizeof(AppParamStruct));

    //if (msgrcv(cltQueueID, (void*) &(this->msgReceive),(sizeof(MsgStruct)-sizeof(long int)),this->msgReceive.msg_id,IPC_NOWAIT) <= -1)
    if (msgrcv(cltQueueID, (void*) &(this->msgReceive),(sizeof(MsgStruct)-sizeof(long int)),this->msgReceive.msg_id,0) <= -1) {
      if (errno != ENOMSG) {
        emit this->MsgQueueError(ceNoReceive);
      }
    }
    else {
      qDebug("Msg RX %u",r++);

      /*----- Copy RX message data to internal struct -----*/
      msgIn.dosSet = msgReceive.dosSet;
      msgIn.dosAct = msgReceive.dosAct;
      msgIn.actRpm = msgReceive.actRpm;
      msgIn.weight = msgReceive.weight;
      msgIn.ShotWeight = msgReceive.shotWeightSet;
      msgIn.DosTime = msgReceive.dosTimeSet;
      msgIn.ProductionType = msgReceive.prodType;
      msgIn.InjInputMode = msgReceive.InjInputMode;
      msgIn.ExtInputMode = msgReceive.ExtInputMode;
      msgIn.OperatingMode = msgReceive.OperatingMode;
      msgIn.calib = msgReceive.calib;

      for (int i=0; i<COM_FLOATBUF_SIZE; i++) {
        msgIn.floatBuf[i] = msgReceive.floatBuf[i];
      }

      for (int i=0; i<COM_BYTEBUF_SIZE; i++) {
        msgIn.byteBuf[i] = msgReceive.byteBuf[i];
      }
      memcpy(&(msgIn.AppParameters),&(msgReceive.AppParameters),sizeof(AppParamStruct));

      emit ComReceived(msgIn);
    }

    doSend = true;
  }
}

/*===========================================================================*
 * File        : LogServer.h                                                 *
 * Project     : MVC0012 - MC-TC                                             *
 * Description : Log TCP/IP server defines.                                  *
 *===========================================================================*/

#ifndef LOG_SERVER_H
#define LOG_SERVER_H

#include <QtNetwork>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include "Glob.h"
#include "Common.h"

/*----- Static log codes -----*/
#define LOG_MOTOR_TYPE               1
#define LOG_CYLINDER_TYPE            2
#define LOG_MATERIAL_TYPE            3
#define LOG_CONTROL_MODE             4
#define LOG_PROD_MODE                5
#define LOG_INJ_INP_FILTER           6
#define LOG_ALARM_NR                 7
#define LOG_MASTER_RESET             8
#define LOG_MV_FILL_CYCLES           9
#define LOG_MV_ALARM_CYCLES          10
#define LOG_FILL_START               11
#define LOG_MV_EMPTY_TIME            12
#define LOG_COLOR_PCT                13
#define LOG_SHOT_WTH                 14
#define LOG_DOS_TIME_SET             15
#define LOG_RPM_SET                  16
#define LOG_EXT_CAP                  17
#define LOG_TACHO_MAX                18
#define LOG_ME_FILL_TIME             19
#define LOG_ME_ALARM_TIME            20
#define LOG_MV_FILL_TIME             21
#define LOG_TOOLING_PASSW            22
#define LOG_SUPERVISOR_PASSW         23
#define LOG_USERLEVEL                24
#define LOG_FILL_SYSTEM              25
#define LOG_JOB_ENB                  26
#define LOG_AUTO_START               27
#define LOG_REF_CRV_TYPE             28
#define LOG_CAL_TYPE                 29
#define LOG_CAL_DEV_SP               30
#define LOG_DEV_ALM_B4               31
#define LOG_DATE_TIME                32
#define LOG_LOADCELL_CAL             33
#define LOG_LANGUAGE                 34
#define LOG_HOP_EMPTYWEIGHT          35
#define LOG_HOP_STARTWEIGHT          36
#define LOG_START_USER               37
#define LOG_MAN_FILL                 38
#define LOG_FILL_ALM_MODE            39
#define LOG_CONSUMPT_RESET           40
//V3.0
#define LOG_HOSE_TYPE                41                                         // Not used in V4.0
#define LOG_DRUM_EMPTYWEIGHT         42                                         // Not used in V4.0

/*---- Reserved ----*/
#define LOG_REGRIND_PCT              43                                         /* TWIN */
#define LOG_RESERVED_TWIN0           44                                         /* TWIN */
#define LOG_RESERVED_TWIN1           45                                         /* TWIN */
#define LOG_RESERVED_TWIN2           46                                         /* TWIN */
#define LOG_RESERVED_TWIN3           47                                         /* TWIN */
#define LOG_RESERVED_TWIN4           48                                         /* TWIN */
#define LOG_RESERVED_TWIN5           49                                         /* TWIN */
#define LOG_LEVEL_LL                 50                                         /* McFlow */
#define LOG_LEVEL_L                  51                                         /* McFlow */
#define LOG_LEVEL_H                  52                                         /* McFlow */
#define LOG_LEVEL_HH                 53                                         /* McFlow */
#define LOG_TIM_MEAS                 54                                         /* McFlow */
#define LOG_BAL_UNITS                55                                         /* McFlow */
#define LOG_START_FLOW               56                                         /* McFlow */
#define LOG_DAC_SCALE1               57                                         /* McFlow */
#define LOG_DAC_SCALE2               58                                         /* McFlow */
#define LOG_PROD_WGHT                59                                         /* McFlow */
//3 /*---new McService V2.3c----*/
#define LOG_RPM_SET_FINE             60                                         /* EPS */
#define LOG_UNLOADER                 61                                         /* EPS */
#define LOG_BLOW_DLY                 62                                         /* EPS */
#define LOG_BLOW_TIME                63                                         /* EPS */
#define LOG_BLOCK_SIZE               32                                         /* Number of log lines in a PC replay block */

/*----- Log file line layout -----*/
struct sLogLine
{
  byte logCode;
  byte time0;
  byte time1;
  byte time2;
  float data;
} __attribute__ ((packed));
typedef struct sLogLine tLogLine;

/*----- TcpIp PC log data cmd  -----*/
struct sTcpIpLogCmd
{
  byte cmd;
  word blockNr;
} __attribute__ ((packed));
typedef struct sTcpIpLogCmd tTcpIpLogCmd;

/*----- TcpIp PC log data reply  -----*/
struct sTcpIpLogRpl
{
  byte cmd;
  word blockNr;
  tLogLine log[LOG_BLOCK_SIZE];
} __attribute__ ((packed));
typedef struct sTcpIpLogRpl tTcpIpLogRpl;

class LogServer: public QObject
{
  Q_OBJECT
    public:
    LogServer(QObject * parent = 0);
    void FormInit();
    ~LogServer();
  public slots:
    void NewConnection();
    void TcpRead();
    void TcpDisConnected();
  private:
    QTcpServer server;
    QTcpSocket* client;
    //  global * _gbi;
};
#endif                                                                          // LOG_SERVER_H

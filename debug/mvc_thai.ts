<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="th">
<context>
    <name>AdvConfig2Frm</name>
    <message>
        <location filename="Form_AdvConfig2.ui" line="62"/>
        <source>Language:</source>
        <comment>label</comment>
        <translation type="unfinished">**ภาษา**:</translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="105"/>
        <source>Production mode:</source>
        <comment>label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="148"/>
        <source>Input Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="191"/>
        <source>Auto start:</source>
        <comment>label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="234"/>
        <source>Recipe function:</source>
        <comment>label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="277"/>
        <source>Units:</source>
        <comment>label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="322"/>
        <source>Startup login:</source>
        <comment>label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="378"/>
        <source>Tooling password:</source>
        <comment>label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="436"/>
        <source>Supervisor password:</source>
        <comment>label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="481"/>
        <source>IP Address:</source>
        <comment>label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="526"/>
        <source>Netmask :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="573"/>
        <source>Gateway:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="602"/>
        <source>192.168.128.001</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="618"/>
        <source>Date / Time:</source>
        <comment>label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="663"/>
        <source>Number of controllers:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="708"/>
        <source>kg/h units:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="749"/>
        <source>McWeight:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="790"/>
        <source>Profibus :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.ui" line="831"/>
        <source>Device Address:</source>
        <comment>label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvConfig2.cpp" line="513"/>
        <source>The number of controllers is changed.
Please reboot this MC-TC !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AdvLoaderSettingsFrm</name>
    <message>
        <location filename="Form_AdvLoaderSettings.ui" line="39"/>
        <source>Advanced Loader Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvLoaderSettings.ui" line="86"/>
        <source>Fill time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvLoaderSettings.ui" line="168"/>
        <source>Empty time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvLoaderSettings.ui" line="256"/>
        <source>Alarm time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvLoaderSettings.ui" line="341"/>
        <source>Fill alarm mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AdvLoaderSettings.cpp" line="211"/>
        <source>Select Alarm Mode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AlarmConfigFrm</name>
    <message>
        <location filename="Form_AlarmConfig.ui" line="41"/>
        <source>Alarm Configuration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AlarmConfigItemFrm</name>
    <message>
        <location filename="Form_AlarmConfigItem.ui" line="71"/>
        <source>[nr]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_AlarmConfigItem.ui" line="81"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AlarmHandler</name>
    <message>
        <location filename="AlarmHandler.cpp" line="43"/>
        <source>Filling system unable to load material!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="46"/>
        <source>Low level!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="50"/>
        <source>Empty level!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="54"/>
        <source>High high level!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="56"/>
        <source>Level is higher than</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="62"/>
        <source>Minimum motor speed! (&lt;0.1 RPM)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="65"/>
        <source>Maximum RPM exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="67"/>
        <source>, change dosing tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="67"/>
        <source>for higher capacity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="72"/>
        <location filename="AlarmHandler.cpp" line="75"/>
        <source>Maximum deviation exceeded!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="79"/>
        <source>Master/Slave connection failure!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="82"/>
        <source>Slide not closed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="85"/>
        <location filename="AlarmHandler.cpp" line="88"/>
        <source>Motor connection failure!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="91"/>
        <source>Loadcell connection failure!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="101"/>
        <source>changed from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="102"/>
        <source>to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="110"/>
        <source>LEARN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="114"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="119"/>
        <source>Color pct</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="128"/>
        <source>Shotweight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="137"/>
        <source>Shottime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="146"/>
        <source>Rpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="155"/>
        <source>Extruder capacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="164"/>
        <source>Max tacho</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="174"/>
        <source>User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="198"/>
        <source>Fillsystem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="220"/>
        <source>Motor type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="242"/>
        <source>Dosing tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="264"/>
        <source>Granulate type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="287"/>
        <source>GraviRpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="309"/>
        <source>System stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="312"/>
        <source>System started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="318"/>
        <source>Low hopper level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="327"/>
        <source>Low Low hopper level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="336"/>
        <source>Manual fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="340"/>
        <source>Filling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="344"/>
        <source>Fill alarm mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="365"/>
        <source>FillCycles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="374"/>
        <source>ME Fill time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="383"/>
        <source>Fill alarm time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="392"/>
        <source>MV Fill time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="401"/>
        <source>MV Empty time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="417"/>
        <source>Loadcell calibrated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="424"/>
        <source>Date time changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="428"/>
        <source>Production mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="449"/>
        <source>Input type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="470"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="492"/>
        <source>Auto start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="514"/>
        <source>Recipe enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="536"/>
        <source>Startup user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="557"/>
        <source>Reset to Factory Defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="560"/>
        <source>Factory Defaults restored.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="560"/>
        <source>Please reboot the controller.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="563"/>
        <source>Are you sure you want to reset the system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="563"/>
        <source>to factory defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="567"/>
        <source>Production settings error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="570"/>
        <source>Default material file not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="573"/>
        <source>Material file could not be loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="573"/>
        <location filename="AlarmHandler.cpp" line="576"/>
        <source>Default material for current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="573"/>
        <location filename="AlarmHandler.cpp" line="576"/>
        <source>configuration will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="576"/>
        <source>Material file could not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="579"/>
        <source>Material saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="583"/>
        <source>Logging to internal memory not possible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="583"/>
        <source>Log interval &lt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="583"/>
        <source>Please insert USB-stick.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="603"/>
        <source>Operator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="606"/>
        <source>Tooling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="609"/>
        <source>Supervisor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="635"/>
        <source>Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="648"/>
        <source>GX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="651"/>
        <source>GLX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="654"/>
        <source>HX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="657"/>
        <source>A30</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="660"/>
        <source>A20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="663"/>
        <source>A15</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="673"/>
        <source>LT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="676"/>
        <source>HT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="686"/>
        <source>NG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="689"/>
        <source>MG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="699"/>
        <source>INJ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="702"/>
        <source>EXT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="715"/>
        <source>RELAY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="718"/>
        <source>TIMER</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="721"/>
        <source>TACHO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="731"/>
        <source>RPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="734"/>
        <source>GRAVI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="744"/>
        <source>NONE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="747"/>
        <source>ME</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="750"/>
        <source>MV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="753"/>
        <source>EX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="756"/>
        <source>RG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="766"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="769"/>
        <source>Dutch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="772"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="783"/>
        <source>YES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="786"/>
        <source>NO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="796"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="799"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="818"/>
        <source> NONE  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="821"/>
        <source>  OFF  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="824"/>
        <source>STANDBY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="827"/>
        <source>DOSING </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="831"/>
        <source>FILLING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="834"/>
        <source>UNLOADING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="837"/>
        <source>*******</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="845"/>
        <source>MW100</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="848"/>
        <source>MW500</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AlarmHandler.cpp" line="851"/>
        <source>MW1000</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalibSelectFrm</name>
    <message>
        <location filename="Form_CalibSelect.ui" line="309"/>
        <source>Loadcell:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_CalibSelect.cpp" line="82"/>
        <source>invalid configuration</source>
        <comment>text for label &quot;Loadcell configuration&quot;</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalibrationFrm</name>
    <message>
        <location filename="Form_Calibration.ui" line="92"/>
        <source>set:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.ui" line="135"/>
        <source>act:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.ui" line="198"/>
        <location filename="Form_Calibration.ui" line="208"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.cpp" line="23"/>
        <location filename="Form_Calibration.cpp" line="112"/>
        <source>Enter material name,</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.cpp" line="23"/>
        <location filename="Form_Calibration.cpp" line="112"/>
        <source>production data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.cpp" line="23"/>
        <location filename="Form_Calibration.cpp" line="112"/>
        <source>and press &quot;START&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.cpp" line="62"/>
        <location filename="Form_Calibration.cpp" line="298"/>
        <source>START</source>
        <comment>PreCal command button text</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.cpp" line="249"/>
        <source>Wrong production settings.
Cannot perform calibration !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.cpp" line="299"/>
        <source>Idle</source>
        <comment>PreCal status text</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.cpp" line="305"/>
        <source>STOP &amp;&amp; SAVE</source>
        <comment>PreCal command button text</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.cpp" line="306"/>
        <location filename="Form_Calibration.cpp" line="313"/>
        <location filename="Form_Calibration.cpp" line="322"/>
        <source>Learning</source>
        <comment>PreCal status text 1/2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.cpp" line="306"/>
        <source>Busy</source>
        <comment>PreCal status text 2/2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.cpp" line="307"/>
        <source>please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.cpp" line="312"/>
        <source>FINISH</source>
        <comment>PreCal command button text</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.cpp" line="313"/>
        <source>Ready</source>
        <comment>PreCal status text 2/2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Calibration.cpp" line="322"/>
        <source>Interrupted</source>
        <comment>PreCal status text 2/2</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalibrationPopup</name>
    <message>
        <location filename="CalibrationPopup.ui" line="66"/>
        <source>Learn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="CalibrationPopup.ui" line="113"/>
        <location filename="CalibrationPopup.cpp" line="49"/>
        <source>Prime unit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="CalibrationPopup.ui" line="188"/>
        <source>Learn interrupted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="CalibrationPopup.cpp" line="48"/>
        <source>Check if the hopper is filled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="CalibrationPopup.cpp" line="48"/>
        <source>and the motor is connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="CalibrationPopup.cpp" line="109"/>
        <source>Prime busy (15sec - 50rpm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="CalibrationPopup.cpp" line="109"/>
        <source>Please wait...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConsumptionFrm</name>
    <message>
        <location filename="Form_Consumption.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Consumption.ui" line="49"/>
        <source>Consumption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Consumption.ui" line="99"/>
        <source>Unit ID : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Consumption.ui" line="174"/>
        <source>Total consumption : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Consumption.ui" line="236"/>
        <location filename="Form_Consumption.ui" line="432"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Consumption.ui" line="305"/>
        <source>Day consumption : </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DateTimeEditorFrm</name>
    <message>
        <location filename="Form_DateTimeEditor.ui" line="149"/>
        <source>Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_DateTimeEditor.ui" line="358"/>
        <source>Minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_DateTimeEditor.ui" line="587"/>
        <source>Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_DateTimeEditor.ui" line="805"/>
        <source>Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_DateTimeEditor.ui" line="1023"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ErrorDlg</name>
    <message>
        <location filename="Form_ErrorDlg.ui" line="151"/>
        <source>Device ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_ErrorDlg.ui" line="213"/>
        <source>Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_ErrorDlg.ui" line="237"/>
        <source>Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_ErrorDlg.ui" line="265"/>
        <source>Code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_ErrorDlg.cpp" line="165"/>
        <source>WARNING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_ErrorDlg.cpp" line="169"/>
        <source>ALARM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_ErrorDlg.cpp" line="173"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_ErrorDlg.cpp" line="181"/>
        <location filename="Form_ErrorDlg.cpp" line="197"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_ErrorDlg.cpp" line="189"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_ErrorDlg.cpp" line="215"/>
        <location filename="Form_ErrorDlg.cpp" line="245"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ErrorFrm</name>
    <message>
        <location filename="Form_Error.ui" line="151"/>
        <source>Device ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Error.ui" line="213"/>
        <source>Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Error.ui" line="237"/>
        <source>Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Error.ui" line="265"/>
        <source>Code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Error.cpp" line="188"/>
        <location filename="Form_Error.cpp" line="396"/>
        <source>WARNING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Error.cpp" line="193"/>
        <location filename="Form_Error.cpp" line="400"/>
        <source>ALARM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Error.cpp" line="198"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Error.cpp" line="206"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Error.cpp" line="214"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Error.cpp" line="269"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EventLogHistoryFrm</name>
    <message>
        <location filename="Form_EventLogHistory.ui" line="44"/>
        <source>Alarm History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_EventLogHistory.cpp" line="136"/>
        <source>Working</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_EventLogHistory.cpp" line="136"/>
        <source>Please wait.... </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EventLogItemFrm</name>
    <message>
        <location filename="Form_EventLogItem.ui" line="17"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_EventLogItem.cpp" line="67"/>
        <source>sys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_EventLogItem.cpp" line="82"/>
        <source>warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_EventLogItem.cpp" line="85"/>
        <source>alarm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_EventLogItem.cpp" line="88"/>
        <source>event</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileSelectFrm</name>
    <message>
        <location filename="Form_FileSelect.cpp" line="385"/>
        <source>will be erased.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_FileSelect.cpp" line="385"/>
        <source>Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_FileSelect.cpp" line="561"/>
        <source>Filename already exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_FileSelect.cpp" line="564"/>
        <source>Unable to rename file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LearnOnlineFrm</name>
    <message>
        <location filename="Form_LearnOnline.ui" line="41"/>
        <source>Save Learned Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LearnOnlineItemFrm</name>
    <message>
        <location filename="Form_LearnOnlineItem.cpp" line="156"/>
        <source>Material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_LearnOnlineItem.cpp" line="156"/>
        <source>already exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_LearnOnlineItem.cpp" line="156"/>
        <source>Overwrite existing material?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_LearnOnlineItem.cpp" line="180"/>
        <source>Data saved.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="Form_MainMenu.cpp" line="855"/>
        <source>No access, unit is active !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MainMenu.cpp" line="1005"/>
        <source>USB Stick not present !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManualFrm</name>
    <message>
        <location filename="Form_Manual.ui" line="45"/>
        <source>Device ID : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.ui" line="52"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.ui" line="84"/>
        <source>Manual mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.ui" line="214"/>
        <source>Motor speed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.ui" line="307"/>
        <source>Act Weight:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.ui" line="328"/>
        <source>Act Tacho:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.ui" line="353"/>
        <source>Input status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.ui" line="359"/>
        <source>START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.ui" line="440"/>
        <location filename="Form_Manual.ui" line="809"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.ui" line="521"/>
        <location filename="Form_Manual.ui" line="692"/>
        <location filename="Form_Manual.ui" line="720"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.ui" line="602"/>
        <location filename="Form_Manual.ui" line="926"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.ui" line="686"/>
        <source>Output control:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.ui" line="1043"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.cpp" line="149"/>
        <location filename="Form_Manual.cpp" line="214"/>
        <location filename="Form_Manual.cpp" line="334"/>
        <location filename="Form_Manual.cpp" line="397"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Manual.cpp" line="209"/>
        <location filename="Form_Manual.cpp" line="331"/>
        <location filename="Form_Manual.cpp" line="394"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MaterialSelectFrm</name>
    <message>
        <location filename="Form_MaterialSelect.ui" line="60"/>
        <source>Materials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MaterialSelect.ui" line="323"/>
        <source>999/999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MaterialSelect.cpp" line="276"/>
        <source>Default Material:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MaterialSelect.cpp" line="408"/>
        <source>Current cylinder does not match the material.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MaterialSelect.cpp" line="408"/>
        <source>Change configuration?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MaterialSelect.cpp" line="523"/>
        <source>Material &apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MaterialSelect.cpp" line="523"/>
        <source>&apos; will be erased.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MaterialSelect.cpp" line="523"/>
        <source>Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MaterialSelect.cpp" line="557"/>
        <source>Configuration changed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>McWeightFrm</name>
    <message>
        <location filename="Form_McWeight.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_McWeight.ui" line="26"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_McWeight.ui" line="39"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_McWeight.ui" line="52"/>
        <source>ManFill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_McWeight.ui" line="65"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_McWeight.cpp" line="479"/>
        <source>Extruder act :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_McWeight.cpp" line="489"/>
        <source>Slide closed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_McWeight.cpp" line="492"/>
        <source>Slide open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>McWeightSettingsFrm</name>
    <message>
        <location filename="Form_McWeightSettings.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_McWeightSettings.ui" line="166"/>
        <source>KgH/V</source>
        <comment>label (Deviation Alarm)</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_McWeightSettings.ui" line="181"/>
        <source>Tacho ratio</source>
        <comment>label (Deviation Alarm)</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_McWeightSettings.ui" line="205"/>
        <source>McWeight Settings</source>
        <comment>Form Title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MultiComponentItemFrm</name>
    <message>
        <location filename="Form_MultiComponentItem.ui" line="132"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MultiComponentItem.ui" line="141"/>
        <source>UnitName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MultiComponentItem.ui" line="170"/>
        <source>ColPct</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MultiComponentItem.ui" line="193"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MultiComponentItem.cpp" line="318"/>
        <location filename="Form_MultiComponentItem.cpp" line="332"/>
        <source>kg/h</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MultiUnitSelectFrm</name>
    <message>
        <location filename="Form_MultiUnitSelect.cpp" line="79"/>
        <source>               Please select a unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MultiUnitSelect.cpp" line="91"/>
        <location filename="Form_MultiUnitSelect.cpp" line="177"/>
        <source>Total capacity : </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MvcSystem</name>
    <message>
        <location filename="Form_MvcSystem.cpp" line="180"/>
        <location filename="Form_MvcSystem.cpp" line="751"/>
        <source>Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MvcSystem.cpp" line="186"/>
        <location filename="Form_MvcSystem.cpp" line="752"/>
        <source>Act</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MvcSystem.cpp" line="1093"/>
        <location filename="Form_MvcSystem.cpp" line="1120"/>
        <source>shots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MvcSystem.cpp" line="1097"/>
        <location filename="Form_MvcSystem.cpp" line="1124"/>
        <source>min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_MvcSystem.cpp" line="1549"/>
        <location filename="Form_MvcSystem.cpp" line="1599"/>
        <source>kg/h</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopupSettingConfirmFrm</name>
    <message>
        <location filename="Form_PopupSettingConfirm.ui" line="39"/>
        <source>Device ID:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrimeFrm</name>
    <message>
        <location filename="Form_Prime.ui" line="324"/>
        <location filename="Form_Prime.ui" line="744"/>
        <location filename="Form_Prime.cpp" line="30"/>
        <source>PRIME</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Prime.cpp" line="30"/>
        <source>ALL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProcessDiagnosticFrm</name>
    <message>
        <location filename="Form_ProcessDiagnostic.ui" line="640"/>
        <source>Tacho ratio:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_ProcessDiagnostic.ui" line="647"/>
        <source>lbTachoRatio</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProductionFrm</name>
    <message>
        <location filename="Form_Production.cpp" line="114"/>
        <location filename="Form_Production.cpp" line="1619"/>
        <source>Tacho set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Production.cpp" line="120"/>
        <location filename="Form_Production.cpp" line="1636"/>
        <source>Ext Cap set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Production.cpp" line="126"/>
        <source>Ext Cap act</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Production.cpp" line="138"/>
        <location filename="Form_Production.cpp" line="1620"/>
        <source>Tacho act</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Production.cpp" line="1637"/>
        <source>Extruder act</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Production.cpp" line="1666"/>
        <source>rpm set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Production.cpp" line="1700"/>
        <location filename="Form_Production.cpp" line="1731"/>
        <source>set time</source>
        <comment>label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Production.cpp" line="1701"/>
        <source>act time</source>
        <comment>label</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Production.cpp" line="1705"/>
        <source>shot weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Production.cpp" line="1754"/>
        <source>relay time</source>
        <comment>label</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecipeCheckItemFrm</name>
    <message>
        <location filename="Form_RecipeCheckItem.cpp" line="49"/>
        <source>Material file missing!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecipeCheckPopupFrm</name>
    <message>
        <location filename="Form_RecipeCheckPopup.ui" line="38"/>
        <source>Configuration Change !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RecipeCheckPopup.ui" line="69"/>
        <source>Current mode : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RecipeCheckPopup.ui" line="105"/>
        <source> Recipe mode :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RecipeCheckPopup.ui" line="170"/>
        <source>Change configuration ? </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RecipeCheckPopup.cpp" line="77"/>
        <location filename="Form_RecipeCheckPopup.cpp" line="78"/>
        <source>Configured number of units : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RecipeCheckPopup.cpp" line="86"/>
        <location filename="Form_RecipeCheckPopup.cpp" line="99"/>
        <source>INJ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RecipeCheckPopup.cpp" line="89"/>
        <location filename="Form_RecipeCheckPopup.cpp" line="102"/>
        <source>EXT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RecipeCheckPopup.cpp" line="92"/>
        <location filename="Form_RecipeCheckPopup.cpp" line="105"/>
        <location filename="Form_RecipeCheckPopup.cpp" line="121"/>
        <location filename="Form_RecipeCheckPopup.cpp" line="136"/>
        <source>INVALID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RecipeCheckPopup.cpp" line="112"/>
        <location filename="Form_RecipeCheckPopup.cpp" line="127"/>
        <source>RELAY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RecipeCheckPopup.cpp" line="115"/>
        <location filename="Form_RecipeCheckPopup.cpp" line="130"/>
        <source>TIMER</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RecipeCheckPopup.cpp" line="118"/>
        <location filename="Form_RecipeCheckPopup.cpp" line="133"/>
        <source>TACHO</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecipeFrm</name>
    <message>
        <location filename="Form_Recipe.ui" line="48"/>
        <source>Recipe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Recipe.ui" line="150"/>
        <source>Select  : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Recipe.ui" line="373"/>
        <source>Device ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Recipe.ui" line="402"/>
        <source>Material:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Recipe.cpp" line="352"/>
        <source>Recipe &apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Recipe.cpp" line="352"/>
        <source>&apos; already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Recipe.cpp" line="352"/>
        <source>Overwrite existing material?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Recipe.cpp" line="368"/>
        <source>Recipe saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Recipe.cpp" line="603"/>
        <source>Recipe not saved.

Recipe name is not correct !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Recipe.cpp" line="608"/>
        <source>Receipe not saved.

Recipe line not correct for unit : </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecipeSelectFrm</name>
    <message>
        <location filename="Form_RecipeSelect.ui" line="63"/>
        <source>Recipes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegrindConfigFrm</name>
    <message>
        <location filename="Form_RegrindConfig.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="55"/>
        <source>Regrind Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="102"/>
        <source>Levels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="139"/>
        <source>Level 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="183"/>
        <source>Level 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="227"/>
        <source>Level 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="271"/>
        <source>Level 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="315"/>
        <source>Level 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="410"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="447"/>
        <source>Level 4 cap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="491"/>
        <source>Grinder ME fill time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="535"/>
        <source>Grinder fill time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="579"/>
        <source>Grinder fast fill time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="623"/>
        <source>Hopper high level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_RegrindConfig.ui" line="667"/>
        <source>Regrind hysterese </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsFrm</name>
    <message>
        <location filename="Form_Settings.cpp" line="55"/>
        <source>Working</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Settings.cpp" line="55"/>
        <source>Please wait.... </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SystemCalibrationFrm</name>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="44"/>
        <source>MSG LABEL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="152"/>
        <source>Be sure that:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="152"/>
        <source> - Loadcell is connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="152"/>
        <source> - Motor is connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="152"/>
        <source> - Hopper is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="152"/>
        <source>Start loadcell calibration?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="155"/>
        <source>Actual weight will be reset to zero.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="155"/>
        <source>This affects:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="155"/>
        <source>Level alarms and filling levels.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="155"/>
        <source>Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="168"/>
        <location filename="Form_SystemCalibration.cpp" line="196"/>
        <source>Calibration busy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="168"/>
        <location filename="Form_SystemCalibration.cpp" line="196"/>
        <source>Please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="171"/>
        <source>Actual weight reset to zero.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="185"/>
        <source>Place weight and</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="185"/>
        <source>continue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="206"/>
        <source>Calibration ready,</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_SystemCalibration.cpp" line="206"/>
        <source>Press Ok to continue.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TachoFrm</name>
    <message>
        <location filename="Form_Tacho.ui" line="48"/>
        <location filename="Form_Tacho.cpp" line="214"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Tacho.ui" line="124"/>
        <source>MANUAL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Tacho.ui" line="148"/>
        <source>SYNCHRONIZE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Tacho.ui" line="201"/>
        <source>Set Tacho</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Tacho.ui" line="280"/>
        <location filename="Form_Tacho.ui" line="417"/>
        <location filename="Form_Tacho.cpp" line="74"/>
        <source>Extruder Cap.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Tacho.ui" line="379"/>
        <source>Act Tacho</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Tacho.ui" line="437"/>
        <source>Actual:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Tacho.ui" line="543"/>
        <source>New:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Tacho.cpp" line="21"/>
        <source>Tacho Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Tacho.cpp" line="252"/>
        <source>Synchronise</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TolerancesFrm</name>
    <message>
        <location filename="Form_Tolerances.ui" line="31"/>
        <source>Tolerance Settings</source>
        <comment>Form Title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Tolerances.ui" line="144"/>
        <source>Calibration Deviation:</source>
        <comment>label (Calibration Deviation)</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_Tolerances.ui" line="159"/>
        <source>Deviation Alarm:</source>
        <comment>label (Deviation Alarm)</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>USBSelectFrm</name>
    <message>
        <location filename="Form_UsbSelect.ui" line="17"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbSelect.ui" line="41"/>
        <source>USB Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbSelect.ui" line="95"/>
        <source>Copy files from USB to Controller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbSelect.ui" line="125"/>
        <source>Copy files from Controller to USB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbSelect.ui" line="158"/>
        <source>Remove USB stick</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbSelect.cpp" line="92"/>
        <source>USB Stick can be safely removed !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnitConfigStdFrm</name>
    <message>
        <location filename="Form_UnitConfigStd.ui" line="87"/>
        <source>Device Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UnitConfigStd.ui" line="123"/>
        <source>Device ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UnitConfigStd.ui" line="687"/>
        <source>Manual I/O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UnitConfigStd.ui" line="712"/>
        <source>Diagnose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UnitConfigStd.ui" line="731"/>
        <source>Service</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UsbTransferFrm</name>
    <message>
        <location filename="Form_UsbTransfer.ui" line="17"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.ui" line="222"/>
        <source>Copying files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="105"/>
        <source>Transfer from Controller to USB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="106"/>
        <source>Unit Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="107"/>
        <location filename="Form_UsbTransfer.cpp" line="125"/>
        <source>Configuration and Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="114"/>
        <source>Transfer from USB to Controller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="118"/>
        <source>Recipes and Materials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="121"/>
        <source>Materials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="513"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="516"/>
        <source>already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="516"/>
        <source>Overwrite existing file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="614"/>
        <location filename="Form_UsbTransfer.cpp" line="693"/>
        <source>Type: Materials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="620"/>
        <location filename="Form_UsbTransfer.cpp" line="699"/>
        <source>Type: Recipes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="625"/>
        <location filename="Form_UsbTransfer.cpp" line="632"/>
        <location filename="Form_UsbTransfer.cpp" line="669"/>
        <source>Type: Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="663"/>
        <location filename="Form_UsbTransfer.cpp" line="711"/>
        <source>Type: Event Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UsbTransfer.cpp" line="705"/>
        <source>Type: Process Log</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UserLoginFrm</name>
    <message>
        <location filename="Form_UserLogin.ui" line="105"/>
        <source>Current Userlevel:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UserLogin.cpp" line="97"/>
        <source>Operator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UserLogin.cpp" line="101"/>
        <source>Tooling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UserLogin.cpp" line="105"/>
        <source>Supervisor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UserLogin.cpp" line="109"/>
        <source>Supervisor (overall)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UserLogin.cpp" line="113"/>
        <source>Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UserLogin.cpp" line="117"/>
        <source>MovaColor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UserLogin.cpp" line="121"/>
        <source>Syrinx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_UserLogin.cpp" line="125"/>
        <source>unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WeightCheckFrm</name>
    <message>
        <location filename="Form_WeightCheck.ui" line="39"/>
        <source>Weight Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_WeightCheck.ui" line="77"/>
        <source>Device ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Form_WeightCheck.ui" line="167"/>
        <source>Actual weight: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>global</name>
    <message>
        <location filename="Glob.cpp" line="953"/>
        <source>LOADER</source>
        <comment>fillSystem type subtext (NONE) 1/2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Glob.cpp" line="953"/>
        <source>OFF</source>
        <comment>fillSystem type subtext (NONE) 2/2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Glob.cpp" line="1104"/>
        <source>NO_IMG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Glob.cpp" line="1430"/>
        <source>loadcell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Glob.cpp" line="1434"/>
        <source>calibrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Glob.cpp" line="1438"/>
        <source>zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Glob.cpp" line="1442"/>
        <source>check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Glob.cpp" line="1977"/>
        <source>rpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Glob.cpp" line="3049"/>
        <source>System mode is changed.
Please reboot this MC-TC !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>selectionFrm</name>
    <message>
        <location filename="Form_Selection.cpp" line="111"/>
        <source>HT motor selected.
Check if the connected motor is a HT type !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

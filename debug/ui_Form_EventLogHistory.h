/********************************************************************************
** Form generated from reading UI file 'Form_EventLogHistory.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_EVENTLOGHISTORY_H
#define UI_FORM_EVENTLOGHISTORY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QScrollArea>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EventLogHistoryFrm
{
public:
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QLabel *lbTitle;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_3;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QHBoxLayout *horizontalLayout;
    QPushButton *btReset;
    QSpacerItem *horizontalSpacer;
    QPushButton *btOk;

    void setupUi(QWidget *EventLogHistoryFrm)
    {
        if (EventLogHistoryFrm->objectName().isEmpty())
            EventLogHistoryFrm->setObjectName(QString::fromUtf8("EventLogHistoryFrm"));
        EventLogHistoryFrm->resize(728, 487);
        EventLogHistoryFrm->setCursor(QCursor(Qt::BlankCursor));
        EventLogHistoryFrm->setWindowTitle(QString::fromUtf8("Form"));
        EventLogHistoryFrm->setStyleSheet(QString::fromUtf8("#scrollAreaWidgetContents {background-color:white;}"));
        verticalLayout_2 = new QVBoxLayout(EventLogHistoryFrm);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        lbTitle = new QLabel(EventLogHistoryFrm);
        lbTitle->setObjectName(QString::fromUtf8("lbTitle"));

        horizontalLayout_2->addWidget(lbTitle);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        scrollArea = new QScrollArea(EventLogHistoryFrm);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setLayoutDirection(Qt::LeftToRight);
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 711, 377));
        scrollArea->setWidget(scrollAreaWidgetContents);

        horizontalLayout_3->addWidget(scrollArea);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        btReset = new QPushButton(EventLogHistoryFrm);
        btReset->setObjectName(QString::fromUtf8("btReset"));
        btReset->setMinimumSize(QSize(90, 70));
        btReset->setMaximumSize(QSize(90, 70));
        btReset->setText(QString::fromUtf8("Reset"));

        horizontalLayout->addWidget(btReset);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btOk = new QPushButton(EventLogHistoryFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("OK"));

        horizontalLayout->addWidget(btOk);


        verticalLayout_2->addLayout(horizontalLayout);


        retranslateUi(EventLogHistoryFrm);

        QMetaObject::connectSlotsByName(EventLogHistoryFrm);
    } // setupUi

    void retranslateUi(QWidget *EventLogHistoryFrm)
    {
        lbTitle->setText(QString());
        Q_UNUSED(EventLogHistoryFrm);
    } // retranslateUi

};

namespace Ui {
    class EventLogHistoryFrm: public Ui_EventLogHistoryFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_EVENTLOGHISTORY_H

#include "Form_UserLogin.h"
#include "ui_Form_UserLogin.h"
#include "Form_MainMenu.h"
#include "Rout.h"

UserLoginFrm::UserLoginFrm(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::UserLoginFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  m_ui->setupUi(this);

  keyboard = new NumKeyboardFrm();
  keyboard->move(((width()/2)-(keyboard->width()/2)),((height()/2)-(keyboard->height()/2)));

  connect(keyboard,SIGNAL(PassInput(QString)),this,SLOT(LoginReceived(QString)));

  connect(m_ui->btLogin,SIGNAL(clicked()),this,SLOT(LoginClicked()));
  connect(m_ui->btOk,SIGNAL(clicked()),this,SLOT(okClicked()));
}


UserLoginFrm::~UserLoginFrm()
{
  disconnect(keyboard,SIGNAL(PassInput(QString)),this,SLOT(LoginReceived(QString)));
  delete keyboard;
  delete m_ui;
}


void UserLoginFrm::changeEvent(QEvent *e)
{
  QBaseWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      m_ui->retranslateUi(this);
      break;
    default:
      break;
  }
}


void UserLoginFrm::resizeEvent(QResizeEvent *e)
{
  QBaseWidget::resizeEvent(e);
  keyboard->move(((width()/2)-(keyboard->width()/2)),((height()/2)-(keyboard->height()/2)));
}


void UserLoginFrm::showEvent(QShowEvent *e)
{
  QBaseWidget::showEvent(e);

  #ifdef QT_NO_DEBUG
  // Release build
  QString version = "Version: "+QString(APP_VERSION)+"r (S"+QString::number(glob->sysSts[0].uBoardVersion.SoftwareVersion)+".H"+QString::number(glob->sysSts[0].uBoardVersion.HardwareVersion)+")";
  #else
  // Debug build
  QString version = "Version: "+QString(APP_VERSION)+"d (S"+QString::number(glob->sysSts[0].uBoardVersion.SoftwareVersion)+".H"+QString::number(glob->sysSts[0].uBoardVersion.HardwareVersion)+")";
  #endif

  m_ui->lbVersion->setText(version);
  m_ui->lbVersion->setVisible(true);
}


void UserLoginFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  keyboard->FormInit();

  glob->SetButtonImage(m_ui->btOk,itSelection,selOk);

  m_ui->label->setFont(*(glob->baseFont));
  m_ui->lbCurLvl->setFont(*(glob->baseFont));
  m_ui->btLogin->setFont(*(glob->baseFont));

  connect(glob,SIGNAL(MstCfg_UserChanged(eUserType)),this,SLOT(UserChanged(eUserType)));
  UserChanged(glob->GetUser());
}


void UserLoginFrm::LoginClicked()
{
  keyboard->Initialize(false,0,0,0,100000,true);
  keyboard->show();
}


void UserLoginFrm::UserChanged(eUserType ut)
{

  switch(ut) {
    case utOperator:
      m_ui->lbCurLvl->setText(tr("Operator"));
      break;

    case utTooling:
      m_ui->lbCurLvl->setText(tr("Tooling"));
      break;

    case utSupervisor:
      m_ui->lbCurLvl->setText(tr("Supervisor"));
      break;

    case utSupervisorBackdoor:
      m_ui->lbCurLvl->setText(tr("Supervisor (overall)"));
      break;

    case utAgent:
      m_ui->lbCurLvl->setText(tr("Service"));
      break;

    case utMovaColor:
      m_ui->lbCurLvl->setText(tr("MovaColor"));
      break;

    case utSyrinx:
      m_ui->lbCurLvl->setText(tr("Syrinx"));
      break;

    default:
      m_ui->lbCurLvl->setText(tr("unknown"));
      break;
  }
}


void UserLoginFrm::LoginReceived(const QString &str)
{
  if (str == glob->mstCfg.ToolPasswd) {
    glob->SetUser(utTooling);
  }
  else {
    if (str == glob->mstCfg.SuperPasswd) {
      glob->SetUser(utSupervisor);
    }
    else {
      if (str == "1689") {
        glob->SetUser(utSupervisorBackdoor);
      }
      else {
        if (str == "3016") {
          glob->SetUser(utAgent);
        }
        else {
          if (str == "1870") {
            glob->SetUser(utMovaColor);
          }
          else {
            if (str == "24073") {
              glob->SetUser(utSyrinx);
            }
            else {
              glob->SetUser(utOperator);
            }
          }
        }
      }
    }
  }
}


void UserLoginFrm::okClicked()
{
  ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
}

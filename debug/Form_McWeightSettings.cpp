#include "Form_McWeightSettings.h"
#include "ui_Form_McWeightSettings.h"
#include "ImageDef.h"
#include "Form_MainMenu.h"
#include "Rout.h"

McWeightSettingsFrm::McWeightSettingsFrm(QWidget *parent) : QBaseWidget(parent), m_ui(new Ui::McWeightSettingsFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/

  m_ui->setupUi(this);

  numKeys = new NumericInputFrm();
  numKeys->move(((width()/2)-(numKeys->width()/2)),((height()/2)-(numKeys->height()/2)));
  numKeys->hide();

  connect(m_ui->btCancel_3,SIGNAL(clicked()),this,SLOT(CancelClicked()));
  connect(m_ui->btOk_3,SIGNAL(clicked()),this,SLOT(OkClicked()));

  m_ui->edTachoRatio->installEventFilter(this);

}


McWeightSettingsFrm::~McWeightSettingsFrm()
{
  delete m_ui;
}


void McWeightSettingsFrm::FormInit()
{
  /*-----------*/
  /* Form init */
  /*-----------*/
  MainMenu* m;
  QBaseWidget::ConnectKeyBeep();;

  m = ((MainMenu*)(glob->menu));
  //connect(this,SIGNAL(DisplayWindow(eWindowIdx)),m,SLOT(DisplayWindow(eWindowIdx)));

  numKeys->FormInit();
  connect(numKeys,SIGNAL(InputValue(float)),this,SLOT(UnitValReceived(float)));

  m_ui->lbCaption->setFont(*(glob->captionFont));
  m_ui->lbTachoRatio->setFont(*(glob->baseFont));
  m_ui->edTachoRatio->setFont(*(glob->baseFont));
  m_ui->lbTachoRatioUnits->setFont(*(glob->baseFont));

  SetButtonImages();
}


bool McWeightSettingsFrm::eventFilter(QObject *obj, QEvent *ev)
{
  /*--------------*/
  /* Event filter */
  /*--------------*/
  if (ev->type() == QEvent::MouseButtonPress) {
    if (obj == m_ui->edTachoRatio) {
      KeyBeep();
      TachoRatioClicked();
    }
  }
  return QWidget::eventFilter(obj,ev);
}


void McWeightSettingsFrm::showEvent(QShowEvent *e)
{
  /*------------*/
  /* Show event */
  /*------------*/
  QWidget::showEvent(e);

  m_ui->edTachoRatio->setText(QString::number(glob->mstCfg.tachoRatio,'f',2));
}


void McWeightSettingsFrm::resizeEvent(QResizeEvent *e)
{
  /*--------------*/
  /* Resize event */
  /*--------------*/
  QWidget::resizeEvent(e);
  numKeys->move(((width()/2)-(numKeys->width()/2)),((height()/2)-(numKeys->height()/2)));
}


void McWeightSettingsFrm::SetButtonImages()
{
  glob->SetButtonImage(m_ui->btCancel_3,itSelection,selCancel);
  glob->SetButtonImage(m_ui->btOk_3,itSelection,selOk);
}


void McWeightSettingsFrm::UnitValReceived(float val)
{
  /*----------------*/
  /* Value received */
  /*-----------------*/
  glob->mstCfg.tachoRatio = val;
  m_ui->edTachoRatio->setText(QString::number(glob->mstCfg.tachoRatio));
}


void McWeightSettingsFrm::CancelClicked()
{
  /*-------------------------*/
  /* Button : Cancel clicked */
  /*-------------------------*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiUnitCfgStd);
}


void McWeightSettingsFrm::OkClicked()
{
  /*---------------------*/
  /* Button : OK clicked */
  /*---------------------*/
  glob->MstCfg_SetTachoRatio(glob->mstCfg.tachoRatio);
  CancelClicked();
}


void McWeightSettingsFrm::TachoRatioClicked()
{
  /*----------------------------*/
  /* Tacho ratio button clicked */
  /*----------------------------*/
  numKeys->SetDisplay(true,2,glob->mstCfg.tachoRatio,0,100);
  numKeys->show();
}

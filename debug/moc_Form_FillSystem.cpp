/****************************************************************************
** Meta object code from reading C++ file 'Form_FillSystem.h'
**
** Created: Thu Jan 31 11:20:33 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_FillSystem.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_FillSystem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FillSystemFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x08,
      37,   14,   14,   14, 0x08,
      57,   14,   14,   14, 0x08,
      78,   14,   14,   14, 0x08,
      90,   14,   14,   14, 0x08,
     110,  106,   14,   14, 0x0a,
     137,  132,   14,   14, 0x0a,
     172,   14,   14,   14, 0x2a,
     211,  203,   14,   14, 0x0a,
     261,  257,   14,   14, 0x2a,
     305,  303,   14,   14, 0x0a,
     344,  338,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_FillSystemFrm[] = {
    "FillSystemFrm\0\0FillSettingsClicked()\0"
    "ManualFillPressed()\0ManualFillReleased()\0"
    "OkClicked()\0CancelClicked()\0str\0"
    "UnitsChanged(QString)\0,idx\0"
    "CfgFillSystemChanged(eFillSys,int)\0"
    "CfgFillSystemChanged(eFillSys)\0,,,,idx\0"
    "CfgLevelsChanged(float,float,float,float,int)\0"
    ",,,\0CfgLevelsChanged(float,float,float,float)\0"
    ",\0CfgFill_ManFillChanged(bool,int)\0"
    "value\0ValueReturned(float)\0"
};

const QMetaObject FillSystemFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_FillSystemFrm,
      qt_meta_data_FillSystemFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FillSystemFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FillSystemFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FillSystemFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FillSystemFrm))
        return static_cast<void*>(const_cast< FillSystemFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int FillSystemFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: FillSettingsClicked(); break;
        case 1: ManualFillPressed(); break;
        case 2: ManualFillReleased(); break;
        case 3: OkClicked(); break;
        case 4: CancelClicked(); break;
        case 5: UnitsChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: CfgFillSystemChanged((*reinterpret_cast< eFillSys(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: CfgFillSystemChanged((*reinterpret_cast< eFillSys(*)>(_a[1]))); break;
        case 8: CfgLevelsChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])),(*reinterpret_cast< float(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 9: CfgLevelsChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])),(*reinterpret_cast< float(*)>(_a[4]))); break;
        case 10: CfgFill_ManFillChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 11: ValueReturned((*reinterpret_cast< float(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 12;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

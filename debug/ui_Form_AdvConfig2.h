/********************************************************************************
** Form generated from reading UI file 'Form_AdvConfig2.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_ADVCONFIG2_H
#define UI_FORM_ADVCONFIG2_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QScrollArea>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AdvConfig2Frm
{
public:
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *ScrollAreaLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_18;
    QLabel *lbUnitConfig;
    QPushButton *btUnitConfig;
    QHBoxLayout *horizontalLayout;
    QLabel *lbLanguage;
    QPushButton *btLanguage;
    QHBoxLayout *horizontalLayout_2;
    QLabel *lbProdMode;
    QPushButton *btProduction;
    QHBoxLayout *horizontalLayout_12;
    QLabel *lbInputType;
    QPushButton *btInput;
    QHBoxLayout *horizontalLayout_3;
    QLabel *lbAutoStart;
    QPushButton *btAutoStart;
    QHBoxLayout *horizontalLayout_4;
    QLabel *lbRecipeEn;
    QPushButton *btRcpEnable;
    QHBoxLayout *horizontalLayout_5;
    QLabel *lbUnits;
    QPushButton *btUnits;
    QHBoxLayout *horizontalLayout_6;
    QLabel *lbStartLogin;
    QPushButton *btStartupLogin;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_12;
    QLabel *lbToolPass;
    QPushButton *btToolPass;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_11;
    QLabel *lbSuperPass;
    QPushButton *btSuperPass;
    QHBoxLayout *horizontalLayout_9;
    QLabel *lbIP;
    QPushButton *btIP;
    QHBoxLayout *horizontalLayout_15;
    QLabel *lbNetmask;
    QPushButton *btNetmask;
    QHBoxLayout *horizontalLayout_16;
    QLabel *lbGateway;
    QPushButton *btGateway;
    QHBoxLayout *horizontalLayout_10;
    QLabel *lbDateTime;
    QPushButton *btDateTime;
    QHBoxLayout *horizontalLayout_13;
    QLabel *lbSlaveCount;
    QPushButton *btSlaveCnt;
    QHBoxLayout *horizontalLayout_19;
    QLabel *lbKgH_Mode;
    QPushButton *btKgH_Mode;
    QHBoxLayout *horizontalLayout_17;
    QLabel *lbImperialMode;
    QPushButton *btImperialMode;
    QHBoxLayout *horizontalLayout_14;
    QLabel *lbProfiEnb;
    QPushButton *btProfibusEnb;
    QHBoxLayout *horizontalLayout_11;
    QLabel *lbModAdr;
    QPushButton *btModAdr;
    QHBoxLayout *OkCancelLayout;
    QPushButton *btAlmCfg;
    QPushButton *btDefaults;
    QSpacerItem *horizontalSpacer;
    QPushButton *btConfirm;

    void setupUi(QWidget *AdvConfig2Frm)
    {
        if (AdvConfig2Frm->objectName().isEmpty())
            AdvConfig2Frm->setObjectName(QString::fromUtf8("AdvConfig2Frm"));
        AdvConfig2Frm->resize(779, 541);
        AdvConfig2Frm->setCursor(QCursor(Qt::BlankCursor));
        AdvConfig2Frm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout_2 = new QVBoxLayout(AdvConfig2Frm);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        ScrollAreaLayout = new QHBoxLayout();
        ScrollAreaLayout->setObjectName(QString::fromUtf8("ScrollAreaLayout"));
        scrollArea = new QScrollArea(AdvConfig2Frm);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setStyleSheet(QString::fromUtf8("#scrollAreaWidgetContents{background: white;}"));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, -1415, 759, 1874));
        verticalLayout = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(-1, -1, 60, -1);
        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        lbUnitConfig = new QLabel(scrollAreaWidgetContents);
        lbUnitConfig->setObjectName(QString::fromUtf8("lbUnitConfig"));
        QFont font;
        font.setPointSize(15);
        lbUnitConfig->setFont(font);
        lbUnitConfig->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_18->addWidget(lbUnitConfig);

        btUnitConfig = new QPushButton(scrollAreaWidgetContents);
        btUnitConfig->setObjectName(QString::fromUtf8("btUnitConfig"));
        btUnitConfig->setMinimumSize(QSize(110, 90));
        btUnitConfig->setMaximumSize(QSize(110, 90));
        btUnitConfig->setFont(font);

        horizontalLayout_18->addWidget(btUnitConfig);


        verticalLayout->addLayout(horizontalLayout_18);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lbLanguage = new QLabel(scrollAreaWidgetContents);
        lbLanguage->setObjectName(QString::fromUtf8("lbLanguage"));
        lbLanguage->setFont(font);
        lbLanguage->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(lbLanguage);

        btLanguage = new QPushButton(scrollAreaWidgetContents);
        btLanguage->setObjectName(QString::fromUtf8("btLanguage"));
        btLanguage->setMinimumSize(QSize(110, 90));
        btLanguage->setMaximumSize(QSize(110, 90));
        btLanguage->setFont(font);
        btLanguage->setText(QString::fromUtf8("EN"));

        horizontalLayout->addWidget(btLanguage);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lbProdMode = new QLabel(scrollAreaWidgetContents);
        lbProdMode->setObjectName(QString::fromUtf8("lbProdMode"));
        lbProdMode->setFont(font);
        lbProdMode->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(lbProdMode);

        btProduction = new QPushButton(scrollAreaWidgetContents);
        btProduction->setObjectName(QString::fromUtf8("btProduction"));
        btProduction->setMinimumSize(QSize(110, 90));
        btProduction->setMaximumSize(QSize(110, 90));
        btProduction->setFont(font);
        btProduction->setText(QString::fromUtf8("Injection"));

        horizontalLayout_2->addWidget(btProduction);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        lbInputType = new QLabel(scrollAreaWidgetContents);
        lbInputType->setObjectName(QString::fromUtf8("lbInputType"));
        lbInputType->setFont(font);
        lbInputType->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_12->addWidget(lbInputType);

        btInput = new QPushButton(scrollAreaWidgetContents);
        btInput->setObjectName(QString::fromUtf8("btInput"));
        btInput->setMinimumSize(QSize(110, 90));
        btInput->setMaximumSize(QSize(110, 90));
        btInput->setFont(font);
        btInput->setText(QString::fromUtf8("Input"));

        horizontalLayout_12->addWidget(btInput);


        verticalLayout->addLayout(horizontalLayout_12);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        lbAutoStart = new QLabel(scrollAreaWidgetContents);
        lbAutoStart->setObjectName(QString::fromUtf8("lbAutoStart"));
        lbAutoStart->setFont(font);
        lbAutoStart->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(lbAutoStart);

        btAutoStart = new QPushButton(scrollAreaWidgetContents);
        btAutoStart->setObjectName(QString::fromUtf8("btAutoStart"));
        btAutoStart->setMinimumSize(QSize(110, 90));
        btAutoStart->setMaximumSize(QSize(110, 90));
        btAutoStart->setFont(font);
        btAutoStart->setText(QString::fromUtf8("YES"));

        horizontalLayout_3->addWidget(btAutoStart);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        lbRecipeEn = new QLabel(scrollAreaWidgetContents);
        lbRecipeEn->setObjectName(QString::fromUtf8("lbRecipeEn"));
        lbRecipeEn->setFont(font);
        lbRecipeEn->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(lbRecipeEn);

        btRcpEnable = new QPushButton(scrollAreaWidgetContents);
        btRcpEnable->setObjectName(QString::fromUtf8("btRcpEnable"));
        btRcpEnable->setMinimumSize(QSize(110, 90));
        btRcpEnable->setMaximumSize(QSize(110, 90));
        btRcpEnable->setFont(font);
        btRcpEnable->setText(QString::fromUtf8("YES"));

        horizontalLayout_4->addWidget(btRcpEnable);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        lbUnits = new QLabel(scrollAreaWidgetContents);
        lbUnits->setObjectName(QString::fromUtf8("lbUnits"));
        lbUnits->setFont(font);
        lbUnits->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_5->addWidget(lbUnits);

        btUnits = new QPushButton(scrollAreaWidgetContents);
        btUnits->setObjectName(QString::fromUtf8("btUnits"));
        btUnits->setMinimumSize(QSize(110, 90));
        btUnits->setMaximumSize(QSize(110, 90));
        QFont font1;
        font1.setPointSize(15);
        font1.setBold(true);
        font1.setWeight(75);
        btUnits->setFont(font1);
        btUnits->setText(QString::fromUtf8("g/s"));

        horizontalLayout_5->addWidget(btUnits);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        lbStartLogin = new QLabel(scrollAreaWidgetContents);
        lbStartLogin->setObjectName(QString::fromUtf8("lbStartLogin"));
        lbStartLogin->setFont(font);
        lbStartLogin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_6->addWidget(lbStartLogin);

        btStartupLogin = new QPushButton(scrollAreaWidgetContents);
        btStartupLogin->setObjectName(QString::fromUtf8("btStartupLogin"));
        btStartupLogin->setMinimumSize(QSize(110, 90));
        btStartupLogin->setMaximumSize(QSize(110, 90));
        btStartupLogin->setFont(font);
        btStartupLogin->setText(QString::fromUtf8("Tooling"));

        horizontalLayout_6->addWidget(btStartupLogin);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_12 = new QLabel(scrollAreaWidgetContents);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setPixmap(QPixmap(QString::fromUtf8("images/user2_simple.png")));
        label_12->setScaledContents(false);

        horizontalLayout_7->addWidget(label_12);

        lbToolPass = new QLabel(scrollAreaWidgetContents);
        lbToolPass->setObjectName(QString::fromUtf8("lbToolPass"));
        lbToolPass->setFont(font);
        lbToolPass->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_7->addWidget(lbToolPass);

        btToolPass = new QPushButton(scrollAreaWidgetContents);
        btToolPass->setObjectName(QString::fromUtf8("btToolPass"));
        btToolPass->setMinimumSize(QSize(110, 90));
        btToolPass->setMaximumSize(QSize(110, 90));
        btToolPass->setFont(font1);
        btToolPass->setText(QString::fromUtf8("Change"));

        horizontalLayout_7->addWidget(btToolPass);


        verticalLayout->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_11 = new QLabel(scrollAreaWidgetContents);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setPixmap(QPixmap(QString::fromUtf8("images/user3_simple.png")));
        label_11->setScaledContents(false);

        horizontalLayout_8->addWidget(label_11);

        lbSuperPass = new QLabel(scrollAreaWidgetContents);
        lbSuperPass->setObjectName(QString::fromUtf8("lbSuperPass"));
        lbSuperPass->setFont(font);
        lbSuperPass->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_8->addWidget(lbSuperPass);

        btSuperPass = new QPushButton(scrollAreaWidgetContents);
        btSuperPass->setObjectName(QString::fromUtf8("btSuperPass"));
        btSuperPass->setMinimumSize(QSize(110, 90));
        btSuperPass->setMaximumSize(QSize(110, 90));
        btSuperPass->setFont(font1);
        btSuperPass->setText(QString::fromUtf8("Change"));

        horizontalLayout_8->addWidget(btSuperPass);


        verticalLayout->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        lbIP = new QLabel(scrollAreaWidgetContents);
        lbIP->setObjectName(QString::fromUtf8("lbIP"));
        lbIP->setFont(font);
        lbIP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_9->addWidget(lbIP);

        btIP = new QPushButton(scrollAreaWidgetContents);
        btIP->setObjectName(QString::fromUtf8("btIP"));
        btIP->setMinimumSize(QSize(210, 90));
        btIP->setMaximumSize(QSize(210, 90));
        btIP->setFont(font1);
        btIP->setText(QString::fromUtf8("192.168.128.255"));

        horizontalLayout_9->addWidget(btIP);


        verticalLayout->addLayout(horizontalLayout_9);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        lbNetmask = new QLabel(scrollAreaWidgetContents);
        lbNetmask->setObjectName(QString::fromUtf8("lbNetmask"));
        lbNetmask->setFont(font);
        lbNetmask->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_15->addWidget(lbNetmask);

        btNetmask = new QPushButton(scrollAreaWidgetContents);
        btNetmask->setObjectName(QString::fromUtf8("btNetmask"));
        btNetmask->setMinimumSize(QSize(210, 90));
        btNetmask->setMaximumSize(QSize(210, 90));
        btNetmask->setFont(font1);
        btNetmask->setText(QString::fromUtf8("255.255.255.0"));

        horizontalLayout_15->addWidget(btNetmask);


        verticalLayout->addLayout(horizontalLayout_15);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        lbGateway = new QLabel(scrollAreaWidgetContents);
        lbGateway->setObjectName(QString::fromUtf8("lbGateway"));
        QFont font2;
        font2.setPointSize(15);
        font2.setBold(false);
        font2.setWeight(50);
        lbGateway->setFont(font2);
        lbGateway->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_16->addWidget(lbGateway);

        btGateway = new QPushButton(scrollAreaWidgetContents);
        btGateway->setObjectName(QString::fromUtf8("btGateway"));
        btGateway->setMinimumSize(QSize(210, 90));
        btGateway->setMaximumSize(QSize(210, 90));
        btGateway->setFont(font1);

        horizontalLayout_16->addWidget(btGateway);


        verticalLayout->addLayout(horizontalLayout_16);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        lbDateTime = new QLabel(scrollAreaWidgetContents);
        lbDateTime->setObjectName(QString::fromUtf8("lbDateTime"));
        lbDateTime->setFont(font);
        lbDateTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_10->addWidget(lbDateTime);

        btDateTime = new QPushButton(scrollAreaWidgetContents);
        btDateTime->setObjectName(QString::fromUtf8("btDateTime"));
        btDateTime->setMinimumSize(QSize(210, 90));
        btDateTime->setMaximumSize(QSize(210, 90));
        btDateTime->setFont(font1);
        btDateTime->setText(QString::fromUtf8("01/01/2010 13:55"));

        horizontalLayout_10->addWidget(btDateTime);


        verticalLayout->addLayout(horizontalLayout_10);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        lbSlaveCount = new QLabel(scrollAreaWidgetContents);
        lbSlaveCount->setObjectName(QString::fromUtf8("lbSlaveCount"));
        lbSlaveCount->setFont(font);
        lbSlaveCount->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_13->addWidget(lbSlaveCount);

        btSlaveCnt = new QPushButton(scrollAreaWidgetContents);
        btSlaveCnt->setObjectName(QString::fromUtf8("btSlaveCnt"));
        btSlaveCnt->setMinimumSize(QSize(110, 90));
        btSlaveCnt->setMaximumSize(QSize(110, 90));
        btSlaveCnt->setFont(font1);

        horizontalLayout_13->addWidget(btSlaveCnt);


        verticalLayout->addLayout(horizontalLayout_13);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        lbKgH_Mode = new QLabel(scrollAreaWidgetContents);
        lbKgH_Mode->setObjectName(QString::fromUtf8("lbKgH_Mode"));
        lbKgH_Mode->setFont(font);
        lbKgH_Mode->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_19->addWidget(lbKgH_Mode);

        btKgH_Mode = new QPushButton(scrollAreaWidgetContents);
        btKgH_Mode->setObjectName(QString::fromUtf8("btKgH_Mode"));
        btKgH_Mode->setMinimumSize(QSize(110, 90));
        btKgH_Mode->setMaximumSize(QSize(110, 90));

        horizontalLayout_19->addWidget(btKgH_Mode);


        verticalLayout->addLayout(horizontalLayout_19);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        lbImperialMode = new QLabel(scrollAreaWidgetContents);
        lbImperialMode->setObjectName(QString::fromUtf8("lbImperialMode"));
        lbImperialMode->setEnabled(true);
        lbImperialMode->setFont(font);
        lbImperialMode->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_17->addWidget(lbImperialMode);

        btImperialMode = new QPushButton(scrollAreaWidgetContents);
        btImperialMode->setObjectName(QString::fromUtf8("btImperialMode"));
        btImperialMode->setEnabled(true);
        btImperialMode->setMinimumSize(QSize(110, 90));
        btImperialMode->setMaximumSize(QSize(110, 16777215));

        horizontalLayout_17->addWidget(btImperialMode);


        verticalLayout->addLayout(horizontalLayout_17);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        lbProfiEnb = new QLabel(scrollAreaWidgetContents);
        lbProfiEnb->setObjectName(QString::fromUtf8("lbProfiEnb"));
        lbProfiEnb->setFont(font);
        lbProfiEnb->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_14->addWidget(lbProfiEnb);

        btProfibusEnb = new QPushButton(scrollAreaWidgetContents);
        btProfibusEnb->setObjectName(QString::fromUtf8("btProfibusEnb"));
        btProfibusEnb->setMinimumSize(QSize(110, 90));
        btProfibusEnb->setMaximumSize(QSize(110, 16777215));

        horizontalLayout_14->addWidget(btProfibusEnb);


        verticalLayout->addLayout(horizontalLayout_14);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        lbModAdr = new QLabel(scrollAreaWidgetContents);
        lbModAdr->setObjectName(QString::fromUtf8("lbModAdr"));
        lbModAdr->setFont(font);
        lbModAdr->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_11->addWidget(lbModAdr);

        btModAdr = new QPushButton(scrollAreaWidgetContents);
        btModAdr->setObjectName(QString::fromUtf8("btModAdr"));
        btModAdr->setMinimumSize(QSize(110, 90));
        btModAdr->setMaximumSize(QSize(110, 90));
        btModAdr->setFont(font1);
        btModAdr->setText(QString::fromUtf8(""));

        horizontalLayout_11->addWidget(btModAdr);


        verticalLayout->addLayout(horizontalLayout_11);

        scrollArea->setWidget(scrollAreaWidgetContents);

        ScrollAreaLayout->addWidget(scrollArea);


        verticalLayout_2->addLayout(ScrollAreaLayout);

        OkCancelLayout = new QHBoxLayout();
        OkCancelLayout->setObjectName(QString::fromUtf8("OkCancelLayout"));
        btAlmCfg = new QPushButton(AdvConfig2Frm);
        btAlmCfg->setObjectName(QString::fromUtf8("btAlmCfg"));
        btAlmCfg->setMinimumSize(QSize(90, 70));
        btAlmCfg->setMaximumSize(QSize(90, 70));
        btAlmCfg->setText(QString::fromUtf8("ALARM CFG"));

        OkCancelLayout->addWidget(btAlmCfg);

        btDefaults = new QPushButton(AdvConfig2Frm);
        btDefaults->setObjectName(QString::fromUtf8("btDefaults"));
        btDefaults->setMinimumSize(QSize(90, 70));
        btDefaults->setMaximumSize(QSize(90, 70));
        btDefaults->setText(QString::fromUtf8("Fact.Default"));

        OkCancelLayout->addWidget(btDefaults);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        OkCancelLayout->addItem(horizontalSpacer);

        btConfirm = new QPushButton(AdvConfig2Frm);
        btConfirm->setObjectName(QString::fromUtf8("btConfirm"));
        btConfirm->setMinimumSize(QSize(90, 70));
        btConfirm->setMaximumSize(QSize(90, 70));
        btConfirm->setText(QString::fromUtf8("ENTER"));

        OkCancelLayout->addWidget(btConfirm);


        verticalLayout_2->addLayout(OkCancelLayout);


        retranslateUi(AdvConfig2Frm);

        QMetaObject::connectSlotsByName(AdvConfig2Frm);
    } // setupUi

    void retranslateUi(QWidget *AdvConfig2Frm)
    {
        lbUnitConfig->setText(QApplication::translate("AdvConfig2Frm", "Unit configuration:", 0, QApplication::UnicodeUTF8));
        btUnitConfig->setText(QApplication::translate("AdvConfig2Frm", "UnitCfg", 0, QApplication::UnicodeUTF8));
        lbLanguage->setText(QApplication::translate("AdvConfig2Frm", "Language:", "label", QApplication::UnicodeUTF8));
        lbProdMode->setText(QApplication::translate("AdvConfig2Frm", "Production mode:", "label", QApplication::UnicodeUTF8));
        lbInputType->setText(QApplication::translate("AdvConfig2Frm", "Input Type:", 0, QApplication::UnicodeUTF8));
        lbAutoStart->setText(QApplication::translate("AdvConfig2Frm", "Auto start:", "label", QApplication::UnicodeUTF8));
        lbRecipeEn->setText(QApplication::translate("AdvConfig2Frm", "Recipe function:", "label", QApplication::UnicodeUTF8));
        lbUnits->setText(QApplication::translate("AdvConfig2Frm", "Units:", "label", QApplication::UnicodeUTF8));
        lbStartLogin->setText(QApplication::translate("AdvConfig2Frm", "Startup login:", "label", QApplication::UnicodeUTF8));
        label_12->setText(QString());
        lbToolPass->setText(QApplication::translate("AdvConfig2Frm", "Tooling password:", "label", QApplication::UnicodeUTF8));
        label_11->setText(QString());
        lbSuperPass->setText(QApplication::translate("AdvConfig2Frm", "Supervisor password:", "label", QApplication::UnicodeUTF8));
        lbIP->setText(QApplication::translate("AdvConfig2Frm", "IP Address:", "label", QApplication::UnicodeUTF8));
        lbNetmask->setText(QApplication::translate("AdvConfig2Frm", "Netmask :", 0, QApplication::UnicodeUTF8));
        lbGateway->setText(QApplication::translate("AdvConfig2Frm", "Gateway:", 0, QApplication::UnicodeUTF8));
        btGateway->setText(QApplication::translate("AdvConfig2Frm", "192.168.128.001", 0, QApplication::UnicodeUTF8));
        lbDateTime->setText(QApplication::translate("AdvConfig2Frm", "Date / Time:", "label", QApplication::UnicodeUTF8));
        lbSlaveCount->setText(QApplication::translate("AdvConfig2Frm", "Number of controllers:", 0, QApplication::UnicodeUTF8));
        btSlaveCnt->setText(QString());
        lbKgH_Mode->setText(QApplication::translate("AdvConfig2Frm", "kg/h units:", 0, QApplication::UnicodeUTF8));
        btKgH_Mode->setText(QString());
        lbImperialMode->setText(QApplication::translate("AdvConfig2Frm", "Imperial units:", 0, QApplication::UnicodeUTF8));
        btImperialMode->setText(QString());
        lbProfiEnb->setText(QApplication::translate("AdvConfig2Frm", "Profibus :", 0, QApplication::UnicodeUTF8));
        btProfibusEnb->setText(QString());
        lbModAdr->setText(QApplication::translate("AdvConfig2Frm", "Device Address:", "label", QApplication::UnicodeUTF8));
        Q_UNUSED(AdvConfig2Frm);
    } // retranslateUi

};

namespace Ui {
    class AdvConfig2Frm: public Ui_AdvConfig2Frm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_ADVCONFIG2_H

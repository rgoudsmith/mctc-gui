#ifndef ADVCONFIG2FRM_H
#define ADVCONFIG2FRM_H

#include <QtGui/QWidget>
#include <QMessageBox>
#include <QProcess>
#include <QLabel>
#include <QTimer>

#include "Form_QbaseWidget.h"
#include "Form_Selection.h"
#include "Form_NumericInput.h"
#include "Form_IpEditor.h"

namespace Ui
{
  class AdvConfig2Frm;
}


class AdvConfig2Frm : public QBaseWidget
{
  Q_OBJECT
    public:
    AdvConfig2Frm(QWidget *parent = 0);
    ~AdvConfig2Frm();
    void FormInit();

  protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *);
    void showEvent(QShowEvent *);

  private:
    Ui::AdvConfig2Frm *m_ui;
    NumericInputFrm *inputForm;
    selectionFrm * selForm;
    IpEditorFrm * ipForm;
    QPushButton * btn;
    QTimer factDefTim;
    int valSelect;
    void SetStartupUserImage(eUserType);
    void SetStartupModeImage(eMode);
    void SetStartupLanguageImage(eLanguages);
    void SetStartupInpTypeImage(eType);
    void SetStartupModbusAddress(int);
    void SetStartupSlaveCount(int);
    void SetButtonImages();
    void SetStartupEthernetSettings();                                          /* Ethernet settings (IP, Netmask, Gateway) */

  private slots:
    void LanguageUpdate();

    void IpReceived(int,int,int,int,int);
    void ToolPassClicked();
    void SuperPassClicked();
    void ModAddrClicked();
    void IpAddrClicked();
    void NetmaskClicked();
    void GatewayClicked();
    void SlaveCntClicked();
    void ToolPassReceived(float);
    void SuperPassReceived(float);
    void ModAddrReceived(float);
    void SlaveCountReceived(float);
    void ButtonConfirmClicked();
    void DateTimeClicked();
    void SelectionReceived(int,int);
    void ValueReceived(float);
    void MessageResult(int);
    void FactoryDefaultsClicked();
    void AlarmCfgClicked();
    void ButtonClicked();

  public slots:
    void LanguageChanged();
    void MstCfg_InputTypeChanged(eType _type);
    void MstCfg_DateTimeChanged(QDateTime);
};
#endif                                                                          // ADVCONFIG2FRM_H

#ifndef LEARNONLINEFRM_H
#define LEARNONLINEFRM_H

#include "Form_QbaseWidget.h"
#include "Form_LearnOnlineItem.h"
#include "Form_Keyboard.h"

namespace Ui
{
  class LearnOnlineFrm;
}


class LearnOnlineFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit LearnOnlineFrm(QWidget *parent = 0);
    ~LearnOnlineFrm();
    void FormInit();

  protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
    void DisplayUnitData();

  private:
    Ui::LearnOnlineFrm *ui;
    KeyboardFrm * keyInput;

  private slots:
    void OkButtonClicked();
    void NewUnitSelected();
};
#endif                                                                          // LEARNONLINEFRM_H

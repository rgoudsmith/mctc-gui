#ifndef TACHOFRM_H
#define TACHOFRM_H

#include <QWidget>
#include "Form_QbaseWidget.h"
#include "Form_NumericInput.h"

namespace Ui
{
  class TachoFrm;
}


class TachoFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    TachoFrm(QWidget *parent = 0);
    ~TachoFrm();
    void FormInit();
    void SetNumInput(NumericInputFrm* numInp);

  protected:
    void changeEvent(QEvent *e);
    void showEvent(QShowEvent *);
    bool eventFilter(QObject *, QEvent *);

  private:
    Ui::TachoFrm *ui;
    NumericInputFrm * numInput;

    float tacho;
    float extCapacity;
    float curExtCap;

    int editIdx;
    int selIdx;

    void ShowSelection(int);
    void LanguageChange();

  private slots:
    void btCancelClicked();
    void btOkClicked();
    void btManualClicked();
    void btSyncClicked();

  public slots:
    void ValueReceived(float);
    void SysSts_TachoChanged(float,int);
    void SysSts_ExtCapChanged(float,int);
    void SysSts_RpmChanged(float,int);
};
#endif                                                                          // TACHOFRM_H

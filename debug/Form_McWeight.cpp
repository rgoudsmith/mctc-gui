#include "Form_McWeight.h"
#include "ui_Form_McWeight.h"
#include "Form_MainMenu.h"
#include "Form_QbaseWidget.h"
#include "Glob.h"
#include "Rout.h"

/*----- Image size constants -----*/
#define IMG_LT_X      450                                                       /* X position */
#define IMG_LT_Y      50                                                        /* Y position */
#define IMG_WIDTH     250                                                       /* Width */
#define IMG_HEIGHT    400                                                       /* Height */

#define LINE_LEN      50                                                        /* Lenght of level line */

#define LL_LEVEL      0
#define  L_LEVEL      1
#define  H_LEVEL      2
#define HH_LEVEL      3

QLabel *ll_lvl;
QLabel *l_lvl;
QLabel *h_lvl;
QLabel *hh_lvl;

QLineEdit * edLoLo;
QLineEdit * edLo;
QLineEdit * edHi;
QLineEdit * edHiHi;

QLabel *unitName;
QLabel *slideStatus;
QLabel *capacity;
QLabel *capacityTotalLbl;
QLabel *capacityTotal;
QLabel *status;
QLabel *weight;

//QImage imageMcWeight;                                                           /* McWeight image */

static int lineIdx;
static int imgXPos;                                                             /* Image left top x posision */

float hihi,hi,lo,lolo;                                                          /* Edit level value */

McWeightFrm::McWeightFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::McWeightFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  ui->setupUi(this);

  QFont lblFont;
  lblFont.setPointSize(15);
  lblFont.setBold(false);

  /*----- Low low level -----*/
  edLoLo = new QLineEdit(this);
  edLoLo->setFont(lblFont);
  edLoLo->setAlignment(Qt::AlignRight);
  edLoLo->setFixedSize(100,70);
  edLoLo->setFocusPolicy(Qt::NoFocus);
  edLoLo->setText("0");

  ll_lvl = new QLabel("",this,0);
  ll_lvl->setFixedSize(130,55);
  ll_lvl->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
  ll_lvl->setFont(lblFont);
  ll_lvl->show();

  /*----- Low level -----*/
  edLo = new QLineEdit(this);
  edLo->setFont(lblFont);
  edLo->setAlignment(edLoLo->alignment());
  edLo->setFixedSize(edLoLo->size());
  edLo->setFocusPolicy(Qt::NoFocus);
  edLo->setText("0");

  l_lvl = new QLabel("",this,0);
  l_lvl->setFixedSize(130,55);
  l_lvl->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
  l_lvl->setFont(lblFont);
  l_lvl->show();

  /*----- High level -----*/
  edHi = new QLineEdit(this);
  edHi->setFont(lblFont);
  edHi->setAlignment(edLo->alignment());
  edHi->setFixedSize(edLo->size());
  edHi->setFocusPolicy(Qt::NoFocus);
  edHi->setText("0");

  h_lvl = new QLabel("",this,0);
  h_lvl->setFixedSize(130,55);
  h_lvl->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
  h_lvl->setFont((*(glob->baseFont)));
  h_lvl->show();

  /*----- High high level -----*/
  edHiHi = new QLineEdit(this);
  edHiHi->setFont(lblFont);
  edHiHi->setAlignment(edHi->alignment());
  edHiHi->setFixedSize(edHi->size());
  edHiHi->setFocusPolicy(Qt::NoFocus);
  edHiHi->setText("0");

  hh_lvl = new QLabel("",this,0);
  hh_lvl->setFixedSize(130,55);
  hh_lvl->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
  hh_lvl->setFont(lblFont);
  hh_lvl->show();

  unitName = new QLabel("",this,0);
  unitName->setFixedSize(180,30);
  unitName->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  unitName->setFont(*(glob->baseFont));
  unitName->show();

  slideStatus = new QLabel("",this,0);
  slideStatus->setFixedSize(180,30);
  slideStatus->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  slideStatus->setFont(*(glob->baseFont));
  slideStatus->show();

  capacity = new QLabel("",this,0);
  capacity->setFixedSize(180,30);
  capacity->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  capacity->setFont(*(glob->baseFont));
  capacity->show();

  capacityTotal = new QLabel("",this,0);
  capacityTotal->setFixedSize(180,30);
  capacityTotal->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
  capacityTotal->setFont(*(glob->baseFont));
  capacityTotal->show();

  capacityTotalLbl = new QLabel("",this,0);
  capacityTotalLbl->setFixedSize(180,30);
  capacityTotalLbl->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
  capacityTotalLbl->setFont(*(glob->baseFont));
  capacityTotalLbl->show();

  status = new QLabel("",this,0);
  status->setFixedSize(180,30);
  status->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  status->setFont(*(glob->baseFont));
  status->show();

  weight = new QLabel("",this,0);
  weight->setFixedSize(180,30);
  weight->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  weight->setFont(*(glob->baseFont));
  weight->show();

  /*----- Link event filter to level edits -----*/
  edHiHi->installEventFilter(this);
  edHi->installEventFilter(this);
  edLo->installEventFilter(this);
  edLoLo->installEventFilter(this);

  /*----- Fonts -----*/
  ll_lvl->setFont(*(glob->baseFont));
  l_lvl->setFont(*(glob->baseFont));
  h_lvl->setFont(*(glob->baseFont));
  hh_lvl->setFont(*(glob->baseFont));

  edHiHi->setFont(*(glob->baseFont));
  edHi->setFont(*(glob->baseFont));
  edLo->setFont(*(glob->baseFont));
  edLoLo->setFont(*(glob->baseFont));
}


McWeightFrm::~McWeightFrm()
{
  /*-------------*/
  /* Destructor  */
  /*-------------*/
  delete numInput;
  delete ui;
}


void McWeightFrm::FormInit()
{
  /*-----------*/
  /* Form init */
  /*-----------*/
  QBaseWidget::ConnectKeyBeep();;

  /*----- Setup numeric keyboard -----*/
  numInput = new NumericInputFrm();
  numInput->move(((width()/2)-(numInput->width()/2)),((height()/2)-(numInput->height()/2)));
  connect(numInput,SIGNAL(InputValue(float)),this,SLOT(ValueReturned(float)));
  numInput->FormInit();

  /*----- Button images -----*/
  glob->SetButtonImage(ui->btOk,itSelection,selOk);
  glob->SetButtonImage(ui->btCancel,itSelection,selCancel);
  glob->SetButtonImage(ui->btManFill,itGeneral,genFillManual);
  glob->SetButtonImage(ui->btFillSettings,itConfig,cfgFillSystem);

  /*----- Connect data refresh to msg received -----*/
  connect(glob,SIGNAL(ComReceived(tMsgControlUnit,int)),this,SLOT(ComMsgReceived(tMsgControlUnit,int)));

  /*----- Buttons -----*/
  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));
  connect(ui->btManFill,SIGNAL(pressed()),this,SLOT(ManualFillPressed()));
  connect(ui->btManFill,SIGNAL(released()),this,SLOT(ManualFillReleased()));
  connect(ui->btFillSettings,SIGNAL(clicked()),this,SLOT(FillSettingsClicked()));
}


void McWeightFrm::paintEvent(QPaintEvent *e)
{
  /*-------------*/
  /* Paint event */
  /*-------------*/
  QWidget::paintEvent(e);

  QPainter painter(this);

  DisplayImage(&painter);                                                       /* Draw McWeight image */
  DrawLevels(&painter);                                                         /* Draw level the lines */
}


void McWeightFrm::resizeEvent(QResizeEvent *e)
{
  /*--------------*/
  /* Resize event */
  /*--------------*/
  QWidget::resizeEvent(e);
}


void McWeightFrm::showEvent(QShowEvent *e)
{
  /*------------*/
  /* Show event */
  /*------------*/
  QWidget::showEvent(e);

  /*----- Auto start unit -----*/
  Rout::AutoStart();

  /*----- Setup left top x position of image -----*/
  if (!Rout::mcWeightEditModeActive) {                                          /* No level edit mode */
    imgXPos = IMG_LT_X;                                                         /* Right side of the screen */

    ui->btOk->setVisible(false);                                                /* Hide the buttons */
    ui->btCancel->setVisible(false);
    ui->btManFill->setVisible(false);
    ui->btFillSettings->setVisible(false);
  }
  else {                                                                        /* Level edit mode */
    imgXPos = IMG_LT_X-200;                                                     /* Centre pos of the screen */
    ui->btOk->setVisible(true);                                                 /* Show the buttons */
    ui->btCancel->setVisible(true);
    ui->btManFill->setVisible(true);
    ui->btFillSettings->setVisible(true);
  }

  /*----- Setup label data -----*/
  hihi = glob->sysCfg[glob->ActUnitIndex()].fillLevels.HiHi;
  hi = glob->sysCfg[glob->ActUnitIndex()].fillLevels.Hi;
  lo = glob->sysCfg[glob->ActUnitIndex()].fillLevels.Lo;
  lolo = glob->sysCfg[glob->ActUnitIndex()].fillLevels.LoLo;

  /*----- Setup edit data fields -----*/
  SetEditFieldData();

  ll_lvl->setText(QString::number(lolo)+glob->weightUnits);
  l_lvl->setText(QString::number(lo)+glob->weightUnits);
  h_lvl->setText(QString::number(hi)+glob->weightUnits);
  hh_lvl->setText(QString::number(hihi)+glob->weightUnits);

  /*----- Update unit name -----*/
  unitName->setText(glob->sysCfg[glob->ActUnitIndex()].MC_ID);

  /*----- Hide status and capacity labels when in edit mode -----*/
  if (Rout::mcWeightEditModeActive==true) {
    capacity->setVisible(false);
    capacityTotalLbl->setVisible(false);
    capacityTotal->setVisible(false);
    status->setVisible(false);
    slideStatus->setVisible(false);
  }
  else {
    capacity->setVisible(true);
    capacityTotalLbl->setVisible(true);
    capacityTotal->setVisible(true);
    status->setVisible(true);
    slideStatus->setVisible(true);
  }
}


bool McWeightFrm::eventFilter(QObject *obj, QEvent *ev)
{
  /*--------------*/
  /* Event filter */
  /*--------------*/
  if (ev->type() == QEvent::MouseButtonPress) {

    lineIdx = -1;

    float val = 0;

    if (obj == edHiHi) {
      lineIdx = 0;
      val = edHiHi->text().toFloat();
    }

    if (obj == edHi) {
      lineIdx = 1;
      val = edHi->text().toFloat();
    }

    if (obj == edLo) {
      lineIdx = 2;
      val = edLo->text().toFloat();
    }

    if (obj == edLoLo) {
      lineIdx = 3;
      val = edLoLo->text().toFloat();
    }

    if (lineIdx >= 0) {
      KeyBeep();
      //numInput->SetDisplay(true,0,val,0,10000);
      numInput->SetDisplay(true,0,val,0,99999);
      numInput->show();
      return true;
    }
  }
  return QWidget::eventFilter(obj,ev);
}


void McWeightFrm::DisplayImage(QPainter *p)
{
  /*----------------------------*/
  /* Display the McWeight image */
  /*----------------------------*/
  QImage imageMcWeight;
  QString imgPath;

  /*----- Setup image path -----*/
  imgPath = glob->AppPath()->toAscii().data() + QString(SYSTEM_MC_WEIGHT_IMG);

  /*----- Load image file -----*/
  imageMcWeight.load(imgPath);

  /*----- Define the image rectangle -----*/
  QRectF r1(imgXPos,IMG_LT_Y,IMG_WIDTH,IMG_HEIGHT);

  p->setRenderHint(QPainter::Antialiasing, true);

  /*----- Display McWeight image -----*/
  p->drawImage(r1,imageMcWeight);

  /*----- Align labels -----*/
  unitName->move(imgXPos+30,IMG_LT_Y+70);
  slideStatus->move(imgXPos+30,IMG_LT_Y+110);
  capacity->move(imgXPos+30,IMG_LT_Y+150);

  capacityTotalLbl->move(imgXPos-400,IMG_LT_Y+130);
  capacityTotal->move(imgXPos-400,IMG_LT_Y+150);

  status->move(imgXPos+30,IMG_LT_Y+200);
  weight->move(imgXPos+40,IMG_LT_Y+260);
}


void DrawLevelLine(QPainter *p,int level, int xOffset,int yOffset,QColor color)
{
  /*-----------------------------------------------------------*/
  /* Draw the a line with a relative offset from image X,Y pos */
  /*-----------------------------------------------------------*/
  QLabel *lvlLabel;
  QLineEdit *edLabel;
  QPen pen(color);
  QBrush brush(color);
  int editOffset;

  p->setPen(pen);
  p->setBrush(brush);

  p->drawLine(imgXPos-LINE_LEN,IMG_LT_Y+yOffset,imgXPos+xOffset,IMG_LT_Y+yOffset);
  p->drawEllipse(QPoint(imgXPos+xOffset,IMG_LT_Y+yOffset),3,3);

  /*----- Set the level label position -----*/
  switch (level) {

  case LL_LEVEL :
    lvlLabel = ll_lvl;
    edLabel = edLoLo;
    editOffset = 15;
    break;

  case L_LEVEL :
    lvlLabel = l_lvl;
    edLabel = edLo;
    editOffset = 0;
    break;

  case H_LEVEL :
    lvlLabel = h_lvl;
    edLabel = edHi;
    editOffset = 15;
    break;

  case HH_LEVEL :
    lvlLabel = hh_lvl;
    edLabel = edHiHi;
    editOffset = 0;
    break;
  }

  /*----- Position the labels -----*/
  lvlLabel->move(imgXPos-LINE_LEN-150,IMG_LT_Y+yOffset-30);

  /*----- Edit labels are higher so an y offset is added -----*/
  edLabel->move(imgXPos-LINE_LEN-150,IMG_LT_Y+yOffset-30+editOffset);

  /*----- Detect edit mode active -----*/
  if(Rout::mcWeightEditModeActive) {
    /*----- Enable edit labels ----*/
    lvlLabel->setVisible(false);
    edLabel->setVisible(true);
  }
  else {
    /*----- Enable display labels -----*/
    lvlLabel->setVisible(true);
    edLabel->setVisible(false);
  }

}


void McWeightFrm::DrawLevels(QPainter *p)
{
  /*---------------------------*/
  /* Draw the LL,L,H,HL levels */
  /*---------------------------*/
  DrawLevelLine(p,HH_LEVEL,0,100,Qt::red);
  DrawLevelLine(p,H_LEVEL,0,150,Qt::black);
  DrawLevelLine(p,L_LEVEL,25,300,Qt::black);
  DrawLevelLine(p,LL_LEVEL,55,350,Qt::red);
}


void McWeightFrm::ComMsgReceived(tMsgControlUnit msg,int idx)
{
  /*--------------------------------------------*/
  /* Comm message received, update dynamic data */
  /*--------------------------------------------*/
  QString statusText;

  if (idx == glob->ActUnitIndex()) {

    /*----- Weight -----*/
    weight->setText(QString::number(msg.weight,'f',0)+' '+glob->weightUnits);
    capacity->setText(QString::number(msg.mcWeightData.extCap,'f',2)+' '+glob->extrUnits);

    /*----- Total capacity -----*/
    capacityTotalLbl->setText(tr("Extruder act :"));

    capacityTotal->setText(QString::number(msg.mcWeightData.extCapTotal,'f',2) +' '+glob->extrUnits);

    /*----- Get unit status text -----*/
    glob->GetEnumeratedText(etMcStatus,glob->sysSts[idx].MCStatus,&statusText);
    status->setText(statusText);

    /*----- Slide status -----*/
    if (glob->sysSts[idx].actIO.inputs[1]) {
      slideStatus->setText(tr("Slide closed"));
    }
    else {
      slideStatus->setText(tr("Slide open"));
    }
  }
}


void McWeightFrm::SetEditFieldData()
{
  /*----------------------*/
  /* Setup the level data */
  /*----------------------*/
  edHiHi->setText(QString::number(hihi)+glob->weightUnits);
  edHi->setText(QString::number(hi)+glob->weightUnits);
  edLo->setText(QString::number(lo)+glob->weightUnits);
  edLoLo->setText(QString::number(lolo)+glob->weightUnits);
}


void McWeightFrm::ValueReturned(float value)
{
  /*---------------------------------*/
  /* Numeric keyboard value returned */
  /*---------------------------------*/
  switch(lineIdx) {
  case 0:
    hihi = value;
    break;

  case 1:
    hi = value;
    break;

  case 2:
    lo = value;
    break;
  case 3:
    lolo = value;
    break;
  }
  SetEditFieldData();                                                           /* Refresh edit field data */
}


void McWeightFrm::OkClicked()
{
  /*---------------------------------------------*/
  /* OK button clicked, Store the level settings */
  /*---------------------------------------------*/
  int idx,res;

  /*----- Copy edit data to system structure -----*/
  glob->sysCfg[glob->ActUnitIndex()].fillLevels.HiHi = hihi;
  glob->sysCfg[glob->ActUnitIndex()].fillLevels.Hi = hi;
  glob->sysCfg[glob->ActUnitIndex()].fillLevels.Lo = lo;
  glob->sysCfg[glob->ActUnitIndex()].fillLevels.LoLo = lolo;

  /*----- Store levels -----*/
  idx = glob->ActUnitIndex();

  ((MainMenu*)(glob->menu))->fileSysCtrl.StoreSystemSettings(idx,glob,0,res);
  CancelClicked();
}


void McWeightFrm::CancelClicked()
{
  /*-----------------------*/
  /* CANCEL button clicked */
  /*-----------------------*/
  if (isActiveWindow()) {
    ((MainMenu*)glob->menu)->DisplayWindow(wiHome);
  }
}


void McWeightFrm::ManualFillPressed()
{
  /*-----------------------------*/
  /* Manual fill button pressed  */
  /*-----------------------------*/
  int idx = glob->ActUnitIndex();
  glob->CfgFill_SetManualFill(true,idx);                                        /* Open fill valve */
}


void McWeightFrm::ManualFillReleased()
{
  /*----------------------------*/
  /* Manual fill button releaed */
  /*----------------------------*/
  glob->CfgFill_SetManualFill(false,glob->ActUnitIndex());                      /* Close fill valve */
}


void McWeightFrm::FillSettingsClicked()
{
  /*-----------------------------------------*/
  /* Advanced loader settings button clicked */
  /*-----------------------------------------*/
  ((MainMenu*)(glob->menu))->DisplayWindow(wiAdvLevels);
}

/****************************************************************************
** Meta object code from reading C++ file 'Form_WeightCheck.h'
**
** Created: Thu Jan 31 11:21:01 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_WeightCheck.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_WeightCheck.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_WeightCheckFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      30,   28,   15,   15, 0x0a,
      64,   28,   15,   15, 0x0a,
      89,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_WeightCheckFrm[] = {
    "WeightCheckFrm\0\0OkClicked()\0,\0"
    "SysSts_FastMassChanged(float,int)\0"
    "McIdChanged(QString,int)\0"
    "ActUnitIdxChanged(int)\0"
};

const QMetaObject WeightCheckFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_WeightCheckFrm,
      qt_meta_data_WeightCheckFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &WeightCheckFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *WeightCheckFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *WeightCheckFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_WeightCheckFrm))
        return static_cast<void*>(const_cast< WeightCheckFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int WeightCheckFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: OkClicked(); break;
        case 1: SysSts_FastMassChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: McIdChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: ActUnitIdxChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

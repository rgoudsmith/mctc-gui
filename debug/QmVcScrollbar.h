#ifndef QMVCSCROLLBAR_H
#define QMVCSCROLLBAR_H

#include <QScrollBar>

class QMvcScrollBar : public QScrollBar
{
  Q_OBJECT
    public:
    explicit QMvcScrollBar(QWidget *parent = 0);

  protected:
    void mousePressEvent(QMouseEvent *);

    signals:
    void MouseButtonPressed();

  public slots:

};
#endif                                                                          // QMVCSCROLLBAR_H

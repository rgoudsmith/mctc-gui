/********************************************************************************
** Form generated from reading UI file 'Form_Splash.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_SPLASH_H
#define UI_FORM_SPLASH_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SplashFrm
{
public:
    QLabel *label;
    QLabel *lbAddress;
    QLabel *label_3;
    QLabel *lbGuiVersion;
    QLabel *lbSystemVersion;
    QLabel *lbBoardVersion;
    QLabel *lbHwVersion;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_2;
    QLabel *label_13;
    QPushButton *pushButton;

    void setupUi(QWidget *SplashFrm)
    {
        if (SplashFrm->objectName().isEmpty())
            SplashFrm->setObjectName(QString::fromUtf8("SplashFrm"));
        SplashFrm->setWindowModality(Qt::ApplicationModal);
        SplashFrm->resize(708, 497);
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        QBrush brush1(QColor(201, 201, 201, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        SplashFrm->setPalette(palette);
        SplashFrm->setCursor(QCursor(Qt::BlankCursor));
        SplashFrm->setWindowTitle(QString::fromUtf8("Form"));
        SplashFrm->setAutoFillBackground(false);
        label = new QLabel(SplashFrm);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(150, -33, 411, 371));
        label->setText(QString::fromUtf8(""));
        label->setPixmap(QPixmap(QString::fromUtf8("images/the_balance.png")));
        label->setScaledContents(true);
        lbAddress = new QLabel(SplashFrm);
        lbAddress->setObjectName(QString::fromUtf8("lbAddress"));
        lbAddress->setGeometry(QRect(50, 299, 621, 171));
        QFont font;
        font.setPointSize(12);
        lbAddress->setFont(font);
        lbAddress->setText(QString::fromUtf8("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:28pt; font-weight:600;\">MovaColor B.V.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">P.O. Box 3016		Phone: +31(0)515 570020</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-i"
                        "ndent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">8600 DA Sneek		Fax: +31(0)515 570021</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">The Netherlands		E-mail: info@movacolor.com</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">	</span></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600; text-decoration: underline;\">www.movacolor.com</span></p></td></tr></table></body></html>"));
        lbAddress->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_3 = new QLabel(SplashFrm);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(0, 0, 708, 497));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::Base, brush);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        label_3->setPalette(palette1);
        label_3->setAutoFillBackground(false);
        label_3->setFrameShape(QFrame::Box);
        label_3->setLineWidth(2);
        label_3->setText(QString::fromUtf8(""));
        label_3->setIndent(0);
        lbGuiVersion = new QLabel(SplashFrm);
        lbGuiVersion->setObjectName(QString::fromUtf8("lbGuiVersion"));
        lbGuiVersion->setGeometry(QRect(610, 190, 61, 16));
        lbGuiVersion->setText(QString::fromUtf8("TextLabel"));
        lbSystemVersion = new QLabel(SplashFrm);
        lbSystemVersion->setObjectName(QString::fromUtf8("lbSystemVersion"));
        lbSystemVersion->setGeometry(QRect(610, 210, 62, 17));
        lbSystemVersion->setText(QString::fromUtf8("TextLabel"));
        lbBoardVersion = new QLabel(SplashFrm);
        lbBoardVersion->setObjectName(QString::fromUtf8("lbBoardVersion"));
        lbBoardVersion->setGeometry(QRect(610, 230, 62, 17));
        lbBoardVersion->setText(QString::fromUtf8("TextLabel"));
        lbHwVersion = new QLabel(SplashFrm);
        lbHwVersion->setObjectName(QString::fromUtf8("lbHwVersion"));
        lbHwVersion->setGeometry(QRect(610, 250, 62, 17));
        lbHwVersion->setText(QString::fromUtf8("TextLabel"));
        label_7 = new QLabel(SplashFrm);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(540, 190, 62, 17));
        label_7->setText(QString::fromUtf8("GUI:"));
        label_7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_8 = new QLabel(SplashFrm);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(540, 210, 62, 17));
        label_8->setText(QString::fromUtf8("System:"));
        label_8->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_9 = new QLabel(SplashFrm);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(540, 230, 62, 17));
        label_9->setText(QString::fromUtf8("Board:"));
        label_9->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_2 = new QLabel(SplashFrm);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(530, 250, 71, 20));
        label_2->setText(QString::fromUtf8("Hardware:"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_13 = new QLabel(SplashFrm);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(520, 150, 171, 121));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        label_13->setFont(font1);
        label_13->setFrameShape(QFrame::Box);
        label_13->setFrameShadow(QFrame::Plain);
        label_13->setLineWidth(2);
        label_13->setText(QString::fromUtf8("Software versions:"));
        label_13->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        pushButton = new QPushButton(SplashFrm);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(610, 420, 93, 70));
        pushButton->setMinimumSize(QSize(90, 70));
        pushButton->setText(QString::fromUtf8("OK"));

        retranslateUi(SplashFrm);

        QMetaObject::connectSlotsByName(SplashFrm);
    } // setupUi

    void retranslateUi(QWidget *SplashFrm)
    {
        Q_UNUSED(SplashFrm);
    } // retranslateUi

};

namespace Ui {
    class SplashFrm: public Ui_SplashFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_SPLASH_H

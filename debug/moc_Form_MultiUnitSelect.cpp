/****************************************************************************
** Meta object code from reading C++ file 'Form_MultiUnitSelect.h'
**
** Created: Thu Jan 31 11:20:40 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_MultiUnitSelect.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_MultiUnitSelect.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MultiUnitSelectFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   19,   19,   19, 0x0a,
      45,   43,   19,   19, 0x0a,
      76,   72,   19,   19, 0x0a,
     118,   19,   19,   19, 0x0a,
     143,   19,   19,   19, 0x0a,
     164,   43,   19,   19, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MultiUnitSelectFrm[] = {
    "MultiUnitSelectFrm\0\0SlaveCountChanged(int)\0"
    ",\0SysActiveChanged(bool,int)\0,,,\0"
    "McWeightCapChanged(float,float,float,int)\0"
    "ColPctClickReceived(int)\0ValueReceived(float)\0"
    "ComMsgReceived(tMsgControlUnit,int)\0"
};

const QMetaObject MultiUnitSelectFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_MultiUnitSelectFrm,
      qt_meta_data_MultiUnitSelectFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MultiUnitSelectFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MultiUnitSelectFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MultiUnitSelectFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MultiUnitSelectFrm))
        return static_cast<void*>(const_cast< MultiUnitSelectFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int MultiUnitSelectFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: SlaveCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: SysActiveChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: McWeightCapChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 3: ColPctClickReceived((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: ValueReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 5: ComMsgReceived((*reinterpret_cast< tMsgControlUnit(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

#include "Form_SystemCalibration.h"
#include "ui_Form_SystemCalibration.h"
#include <QMessageBox>
#include "Form_MainMenu.h"
#include "Rout.h"

SystemCalibrationFrm::SystemCalibrationFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::SystemCalibrationFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  showButton = false;

  calibrate = true;
  calibDone = false;
  calState = 0;

  ui->setupUi(this);
  ui->mvcSystem->SetDispIdx(0);
  ui->mvcSystem->SetDisplayType(DISP_TYPE_SINGLE_UNIT);
  ui->mvcSystem->SetMCStatusVisible(false);
  ui->mvcSystem->SetColorPctVisible(false);
  ui->mvcSystem->SetEditColorPct(false);
  ui->mvcSystem->SetDosActVisible(false);
  ui->mvcSystem->SetDosSetVisible(false);
  ui->mvcSystem->SetMaterialVisible(false);
  ui->mvcSystem->SetRPMVisible(false);
  ui->mvcSystem->SetMotorVisible(false);
  ui->mvcSystem->SetToGoVisible(false);
  ui->mvcSystem->SetMCIDVisible(true);
  ui->mvcSystem->SetWeightVisible(false);
  ui->mvcSystem->SetToGoVisible(false);
  ui->pbProgress->setVisible(false);

  btCancel = new QPushButton("CANCEL",this);
  btCancel->setFixedSize(90,70);
  btCancel->move((this->width()-99),(this->height()-btCancel->height()));

  btOk = new QPushButton("OK",this);
  btOk->setFixedSize(90,70);
  btOk->move((btCancel->pos().x()-(btOk->width()+9)),btCancel->pos().y());

  lbMsgDisplay = new QLabel(this);
  lbMsgDisplay->setText(tr("MSG LABEL"));
  lbMsgDisplay->setFixedSize(400,400);
  lbMsgDisplay->move(this->width()-lbMsgDisplay->width(),this->height()-(lbMsgDisplay->height()+80));

  ui->mvcSystem->SetMotorStatus(0,0);
  ui->mvcSystem->SetMotorStatus(0,1);

  calTim = new QTimer(this);

  calTim->setSingleShot(false);
  calTim->setInterval(500);
  connect(calTim,SIGNAL(timeout()),this,SLOT(timerInterval()));
  calTim->stop();
  ui->pbProgress->setMaximum(10);
  ui->pbProgress->setValue(0);

  connect(btOk,SIGNAL(clicked()),this,SLOT(ButtonClick()));
  connect(btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));
}


SystemCalibrationFrm::~SystemCalibrationFrm()
{
  calTim->stop();
  delete ui;
}


void SystemCalibrationFrm::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}


void SystemCalibrationFrm::resizeEvent(QResizeEvent *e)
{
  QWidget::resizeEvent(e);
  ui->mvcSystem->SetDisplayType(DISP_TYPE_SINGLE_UNIT);
  btCancel->move(this->width()-(btCancel->width()+9),this->height()-btCancel->height());
  btOk->move((btCancel->pos().x()-(btOk->width()+9)),btCancel->pos().y());
  lbMsgDisplay->move(this->width()-lbMsgDisplay->width(),this->height()-(lbMsgDisplay->height()+80));
}


void SystemCalibrationFrm::showEvent(QShowEvent *e)
{
  QWidget::showEvent(e);
  SetDisplay();
}


void SystemCalibrationFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;

  ui->mvcSystem->FormInit();

  setPalette(*(glob->Palette()));

  glob->SetButtonImage(btOk,itSelection,selOk);
  glob->SetButtonImage(btCancel,itSelection,selCancel);

  connect(glob,SIGNAL(CalState(eCalState)),this,SLOT(CalStateChanged(eCalState)));

  // Signals / Slots to communication.
  connect(glob,SIGNAL(CalStatus(eCalStatus)),this,SLOT(CalStatusReceived(eCalStatus)));
  connect(this,SIGNAL(SendCalCommand(eCalCommand)),glob,SLOT(CalSendCommand(eCalCommand)));
  lbMsgDisplay->setFont(*(glob->baseFont));
}


void SystemCalibrationFrm::DoCalibration(bool cal)
{
  calibrate = cal;
  ui->mvcSystem->SetWeightVisible(!cal);
  if (calState == 9999) {
    calState = 0;
  }
  SetDisplay();
}


void SystemCalibrationFrm::SetDisplay()
{
  QString txt;

  /*----- Setup unit name for active unit -----*/
  ui->mvcSystem->SetDispIdx(glob->ActUnitIndex());

  if (btCancel->isVisible() == true) {
    btOk->move((btCancel->pos().x()-(btOk->width()+9)),btCancel->pos().y());
  }
  else {
    btOk->move(btCancel->pos());
  }

  switch(calState) {
    case 0:                                                                     // Startup / Idle
      calTim->start();
      ui->pbProgress->setVisible(false);
      if (calibrate) {
        lbMsgDisplay->setText(tr("Be sure that:")+"\n"+tr(" - Loadcell is connected")+"\n"+tr(" - Motor is connected")+"\n"+tr(" - Hopper is empty")+"\n\n"+tr("Start loadcell calibration?"));
      }
      else {
        lbMsgDisplay->setText(tr("Actual weight will be reset to zero.")+"\n\n"+tr("This affects:")+"\n"+tr("Level alarms and filling levels.")+"\n\n"+tr("Do you want to continue?"));
      }
      break;

    case 1:                                                                     // Calibration started / busy
      // Hide ok button
      ui->pbProgress->setValue(0);
      btOk->setVisible(false);
      showButton = true;
      btCancel->setVisible(false);
      if (calibrate) {
        // Activate progressBar
        ui->pbProgress->setVisible(true);
        lbMsgDisplay->setText(tr("Calibration busy")+"\n"+tr("Please wait..."));
      }
      else {
        lbMsgDisplay->setText(tr("Actual weight reset to zero."));
      }
      break;

    case 2:                                                                     // Place reference weight / pause
      // Hide progress bar
      ui->pbProgress->setVisible(false);
      // Show ok button
      if (showButton == true) {
        showButton = false;
        btOk->setVisible(true);
      }
      // Show image of calibration weight
      //        ((MainMenu*)(glob->menu))->ActivateAlarmBuzzer();
      lbMsgDisplay->setText(tr("Place weight and")+"\n"+tr("continue."));
      break;

    case 3:                                                                     // Continue calibration / busy
      // Show progress bar
      // Hide ok button
      btOk->setVisible(false);
      showButton = true;

      ui->pbProgress->setValue(0);
      ui->pbProgress->setVisible(true);
      lbMsgDisplay->setText(tr("Calibration busy")+"\n"+tr("Please wait..."));
      break;

    case 4:                                                                     // Calibration finished / ready
      if (!calibDone) {
        calTim->stop();
        ui->pbProgress->setValue(ui->pbProgress->maximum());
        ui->pbProgress->setVisible(true);
        btOk->setVisible(true);
        // ((MainMenu*)(glob->menu))->ActivateAlarmBuzzer();
        lbMsgDisplay->setText(tr("Calibration ready,")+"\n"+tr("Press Ok to continue."));
      }
      break;

    case 5:                                                                     // Calibration error
      // Geef een melding weer en zorg ervoor dat de calibratie correct wordt afgebroken.
      //lbMsgDisplay->setText(tr("Calibration error")+"\n"+tr(" - check loadcell connection")+"\n\n"+tr("Press continue"));
      // Loadcell connection error, abort calibration
      emit SendCalCommand(calAbort);

      // Display error
      glob->GetAlarmText(ALM_INDEX_LOADCELL_CON,0,&txt,true);
      ((MainMenu*)(glob->menu))->DisplayMessage(glob->ActUnitIndex(),ALM_INDEX_LOADCELL_CON,txt,msgtAlarm);
      ui->pbProgress->setVisible(false);
      btOk->setVisible(true);
      calState = 0;
      SetDisplay();
      break;

    case 9999:                                                                  // Save calibration
      // Calibration completed: save results
      //        QMessageBox::warning(this,"Notification","Save results (not yet implemented)",QMessageBox::Ok);
      // and close the window

      // If the window is freed, re-initialization is not required.
      // Window will be re-initialized on creation.

      ui->pbProgress->setValue(0);
      ui->pbProgress->setVisible(false);
      btCancel->setVisible(true);
      showButton = true;
      // calState = 0;

      ((MainMenu*)(glob->menu))->DisplayWindow(wiHome);
      break;
  }
}


void SystemCalibrationFrm::ButtonClick()
{
  if (calibrate == csCalibration) {
    if (calState == 4) {
      glob->calStruct.sts = calsReady;
    }

    switch(glob->calStruct.sts) {
      case calsIdle:
        btCancel->setVisible(false);
        btOk->setVisible(false);
        emit SendCalCommand(calStart);
        break;
      case calsWaitForRef:
        btOk->setVisible(false);
        emit SendCalCommand(calRefPlaced);
        break;
      case calsReady:
        emit SendCalCommand(calDone);
        glob->AddEventItem(glob->ActUnitIndex(),2,TXT_INDEX_LOADCELL_CALIBRATED,0,0);
        btCancel->setVisible(true);
        glob->calStruct.sts = calsIdle;
        calibDone = true;
        calState = 9999;
        break;
      case calsErr:
        // Eventueel handling voor afbreken van calibratie.
        break;
      default:
        break;
    }
  }
  else {
    calState++;
  }
  SetDisplay();
}


void SystemCalibrationFrm::CancelClicked()
{
  ((MainMenu*)(glob->menu))->DisplayWindow(wiBack);
}


void SystemCalibrationFrm::CalStateChanged(eCalState cal)
{
  switch (cal) {
    case csNone:
      // Abort calibration!!!
      //@@ SystemCalibrationFrm - Calibration abort not yet implemented
      QMessageBox::warning(this,"Calibration warning","Calibration abort (not yet implemented)",QMessageBox::Ok);
      // Go back to previous window
      (((MainMenu*)glob->menu))->DisplayWindow(wiBack);
      break;

    case csCalibration:
      DoCalibration(true);
      break;

    case csZero:
      DoCalibration(false);
      break;
  }
}


void SystemCalibrationFrm::CalStatusReceived(eCalStatus sts)
{
  if (glob->calStruct.cmd != calNone) {
    emit SendCalCommand(calNone);
  }

  // Status tijdens loadcell calibratie
  switch(sts) {
    case calsIdle:
      if (calibDone) {
        calibDone = false;
        calState = 0;
      }

      if (calState < 3) {
        calState = 0;
      }
      else {
        if (calState == 3) {
          calState++;
        }
      }
      break;
    case calsBusy:
      calState = 1;
      break;
    case calsWaitForRef:
      calState = 2;
      break;
    case calsBusy2:
      calState = 3;
      break;
    case calsReady:
      calState = 4;
      break;

    case calsErr:
      calState = 5;                                                             // Calibration error state.
      break;

    default:
      QMessageBox::warning(this,"Calibration warning","Invalid calibration status received",QMessageBox::Ok);
      break;
  }
  SetDisplay();
}


void SystemCalibrationFrm::timerInterval()
{
  if (ui->pbProgress->isVisible()) {
    int max = ui->pbProgress->maximum();
    int pos = ui->pbProgress->value();

    if (ui->pbProgress->layoutDirection() == Qt::LeftToRight) {
      if (pos == max) {
        ui->pbProgress->setLayoutDirection(Qt::RightToLeft);
      }
      ui->pbProgress->setValue(pos+1);
    }
    else {
      if (pos == 0) {
        ui->pbProgress->setLayoutDirection(Qt::LeftToRight);
      }
      else {
        ui->pbProgress->setValue(pos-1);
      }
    }
  }
}

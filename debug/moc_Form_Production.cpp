/****************************************************************************
** Meta object code from reading C++ file 'Form_Production.h'
**
** Created: Thu Jan 31 11:20:56 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Form_Production.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_Production.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ProductionFrm[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      36,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,
      39,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      65,   14,   14,   14, 0x08,
      84,   14,   14,   14, 0x08,
     105,   14,   14,   14, 0x08,
     127,   14,   14,   14, 0x08,
     144,   14,   14,   14, 0x08,
     163,   14,   14,   14, 0x08,
     182,   14,   14,   14, 0x08,
     198,   14,   14,   14, 0x0a,
     229,   14,   14,   14, 0x0a,
     259,   14,   14,   14, 0x0a,
     283,   14,   14,   14, 0x0a,
     309,   14,   14,   14, 0x0a,
     340,  335,   14,   14, 0x0a,
     376,   14,   14,   14, 0x2a,
     408,  335,   14,   14, 0x0a,
     442,   14,   14,   14, 0x2a,
     474,  472,   14,   14, 0x0a,
     508,  472,   14,   14, 0x0a,
     545,  472,   14,   14, 0x0a,
     584,  472,   14,   14, 0x0a,
     622,  335,   14,   14, 0x0a,
     654,   14,   14,   14, 0x2a,
     682,  472,   14,   14, 0x0a,
     714,  472,   14,   14, 0x0a,
     738,  472,   14,   14, 0x0a,
     771,   14,   14,   14, 0x0a,
     805,   14,   14,   14, 0x0a,
     824,  472,   14,   14, 0x0a,
     855,   14,   14,   14, 0x0a,
     874,   14,   14,   14, 0x0a,
     902,  893,   14,   14, 0x0a,
     933,   14,   14,   14, 0x0a,
     956,   14,   14,   14, 0x0a,
     981,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ProductionFrm[] = {
    "ProductionFrm\0\0RecipeSelected(QString)\0"
    "MaterialSelected(QString)\0btDisplayClicked()\0"
    "ValueReceived(float)\0TextReceived(QString)\0"
    "btTachoClicked()\0Material1Clicked()\0"
    "Material2Clicked()\0RecipeClicked()\0"
    "SetDisplayState(eMvcDispState)\0"
    "MstCfg_UserChanged(eUserType)\0"
    "RcpEnabledChanged(bool)\0"
    "MstCfg_ShotUnitsChanged()\0"
    "MstCfg_TimeUnitsChanged()\0,idx\0"
    "SysCfg_ShotWeightChanged(float,int)\0"
    "SysCfg_ShotWeightChanged(float)\0"
    "SysCfg_ShotTimeChanged(float,int)\0"
    "SysCfg_ShotTimeChanged(float)\0,\0"
    "SysSts_ShotTimeChanged(float,int)\0"
    "SysSts_MeteringActChanged(float,int)\0"
    "SysCfg_MeteringStartChanged(float,int)\0"
    "SysSts_MeteringValidChanged(bool,int)\0"
    "SysCfg_ExtCapChanged(float,int)\0"
    "SysCfg_ExtCapChanged(float)\0"
    "SysSts_ExtCapChanged(float,int)\0"
    "TachoChanged(float,int)\0"
    "SysSts_LearnStsChanged(bool,int)\0"
    "SetBalanceCalibratedVisible(bool)\0"
    "ModeChanged(eMode)\0GraviRpmChanged(eGraviRpm,int)\0"
    "TypeChanged(eType)\0ResetCalibValues()\0"
    "_sts,idx\0MCStatusChanged(eMCStatus,int)\0"
    "SlaveCountChanged(int)\0ActUnitIndexChanged(int)\0"
    "RecipeChanged(QString)\0"
};

const QMetaObject ProductionFrm::staticMetaObject = {
    { &QBaseWidget::staticMetaObject, qt_meta_stringdata_ProductionFrm,
      qt_meta_data_ProductionFrm, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ProductionFrm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ProductionFrm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ProductionFrm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ProductionFrm))
        return static_cast<void*>(const_cast< ProductionFrm*>(this));
    return QBaseWidget::qt_metacast(_clname);
}

int ProductionFrm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBaseWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: RecipeSelected((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: MaterialSelected((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: btDisplayClicked(); break;
        case 3: ValueReceived((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 4: TextReceived((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: btTachoClicked(); break;
        case 6: Material1Clicked(); break;
        case 7: Material2Clicked(); break;
        case 8: RecipeClicked(); break;
        case 9: SetDisplayState((*reinterpret_cast< eMvcDispState(*)>(_a[1]))); break;
        case 10: MstCfg_UserChanged((*reinterpret_cast< eUserType(*)>(_a[1]))); break;
        case 11: RcpEnabledChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: MstCfg_ShotUnitsChanged(); break;
        case 13: MstCfg_TimeUnitsChanged(); break;
        case 14: SysCfg_ShotWeightChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 15: SysCfg_ShotWeightChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 16: SysCfg_ShotTimeChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 17: SysCfg_ShotTimeChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 18: SysSts_ShotTimeChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 19: SysSts_MeteringActChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 20: SysCfg_MeteringStartChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 21: SysSts_MeteringValidChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 22: SysCfg_ExtCapChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 23: SysCfg_ExtCapChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 24: SysSts_ExtCapChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 25: TachoChanged((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 26: SysSts_LearnStsChanged((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 27: SetBalanceCalibratedVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 28: ModeChanged((*reinterpret_cast< eMode(*)>(_a[1]))); break;
        case 29: GraviRpmChanged((*reinterpret_cast< eGraviRpm(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 30: TypeChanged((*reinterpret_cast< eType(*)>(_a[1]))); break;
        case 31: ResetCalibValues(); break;
        case 32: MCStatusChanged((*reinterpret_cast< eMCStatus(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 33: SlaveCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 34: ActUnitIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 35: RecipeChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 36;
    }
    return _id;
}

// SIGNAL 0
void ProductionFrm::RecipeSelected(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ProductionFrm::MaterialSelected(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE

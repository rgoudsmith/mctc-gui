/********************************************************************************
** Form generated from reading UI file 'Form_Selection.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_SELECTION_H
#define UI_FORM_SELECTION_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_selectionFrm
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_2;
    QGridLayout *gridLayout;
    QPushButton *bt11;
    QPushButton *bt12;
    QPushButton *bt13;
    QPushButton *bt21;
    QPushButton *bt22;
    QPushButton *bt23;
    QPushButton *bt31;
    QPushButton *bt32;
    QPushButton *bt33;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QPushButton *btNext;
    QSpacerItem *horizontalSpacer;
    QPushButton *btOk;
    QPushButton *btCancel;

    void setupUi(QWidget *selectionFrm)
    {
        if (selectionFrm->objectName().isEmpty())
            selectionFrm->setObjectName(QString::fromUtf8("selectionFrm"));
        selectionFrm->setWindowModality(Qt::ApplicationModal);
        selectionFrm->resize(400, 410);
        selectionFrm->setMaximumSize(QSize(16777215, 416));
        selectionFrm->setSizeIncrement(QSize(0, 0));
        selectionFrm->setBaseSize(QSize(0, 0));
        selectionFrm->setCursor(QCursor(Qt::BlankCursor));
        selectionFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout_2 = new QVBoxLayout(selectionFrm);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox = new QGroupBox(selectionFrm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox{border: 2px solid black; border-radius: 3px; background: white}"));
        groupBox->setAlignment(Qt::AlignCenter);
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        bt11 = new QPushButton(groupBox);
        bt11->setObjectName(QString::fromUtf8("bt11"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(bt11->sizePolicy().hasHeightForWidth());
        bt11->setSizePolicy(sizePolicy);
        bt11->setMinimumSize(QSize(120, 90));
        bt11->setMaximumSize(QSize(120, 90));
        bt11->setSizeIncrement(QSize(1, 1));
        bt11->setBaseSize(QSize(50, 50));
        bt11->setText(QString::fromUtf8(""));
        bt11->setIconSize(QSize(100, 100));

        gridLayout->addWidget(bt11, 0, 0, 1, 1);

        bt12 = new QPushButton(groupBox);
        bt12->setObjectName(QString::fromUtf8("bt12"));
        bt12->setMinimumSize(QSize(120, 90));
        bt12->setMaximumSize(QSize(120, 90));
        bt12->setSizeIncrement(QSize(1, 1));
        bt12->setBaseSize(QSize(50, 50));
        bt12->setText(QString::fromUtf8(""));
        bt12->setIconSize(QSize(100, 100));
        bt12->setCheckable(false);
        bt12->setChecked(false);
        bt12->setFlat(false);

        gridLayout->addWidget(bt12, 0, 1, 1, 1);

        bt13 = new QPushButton(groupBox);
        bt13->setObjectName(QString::fromUtf8("bt13"));
        bt13->setMinimumSize(QSize(120, 90));
        bt13->setMaximumSize(QSize(120, 90));
        bt13->setSizeIncrement(QSize(1, 1));
        bt13->setBaseSize(QSize(50, 50));
        bt13->setText(QString::fromUtf8(""));
        bt13->setIconSize(QSize(100, 100));

        gridLayout->addWidget(bt13, 0, 2, 1, 1);

        bt21 = new QPushButton(groupBox);
        bt21->setObjectName(QString::fromUtf8("bt21"));
        bt21->setMinimumSize(QSize(120, 90));
        bt21->setMaximumSize(QSize(120, 90));
        bt21->setSizeIncrement(QSize(1, 1));
        bt21->setBaseSize(QSize(50, 50));
        bt21->setText(QString::fromUtf8(""));
        bt21->setIconSize(QSize(100, 100));

        gridLayout->addWidget(bt21, 1, 0, 1, 1);

        bt22 = new QPushButton(groupBox);
        bt22->setObjectName(QString::fromUtf8("bt22"));
        bt22->setMinimumSize(QSize(120, 90));
        bt22->setMaximumSize(QSize(120, 90));
        bt22->setSizeIncrement(QSize(1, 1));
        bt22->setBaseSize(QSize(50, 50));
        bt22->setText(QString::fromUtf8(""));
        bt22->setIconSize(QSize(100, 100));

        gridLayout->addWidget(bt22, 1, 1, 1, 1);

        bt23 = new QPushButton(groupBox);
        bt23->setObjectName(QString::fromUtf8("bt23"));
        bt23->setMinimumSize(QSize(120, 90));
        bt23->setMaximumSize(QSize(120, 90));
        bt23->setSizeIncrement(QSize(1, 1));
        bt23->setBaseSize(QSize(50, 50));
        bt23->setText(QString::fromUtf8(""));
        bt23->setIconSize(QSize(100, 100));

        gridLayout->addWidget(bt23, 1, 2, 1, 1);

        bt31 = new QPushButton(groupBox);
        bt31->setObjectName(QString::fromUtf8("bt31"));
        bt31->setMinimumSize(QSize(120, 90));
        bt31->setMaximumSize(QSize(120, 90));
        bt31->setSizeIncrement(QSize(1, 1));
        bt31->setBaseSize(QSize(50, 50));
        bt31->setText(QString::fromUtf8(""));
        bt31->setIconSize(QSize(100, 100));

        gridLayout->addWidget(bt31, 2, 0, 1, 1);

        bt32 = new QPushButton(groupBox);
        bt32->setObjectName(QString::fromUtf8("bt32"));
        bt32->setMinimumSize(QSize(120, 90));
        bt32->setMaximumSize(QSize(120, 90));
        bt32->setSizeIncrement(QSize(1, 1));
        bt32->setBaseSize(QSize(50, 50));
        bt32->setText(QString::fromUtf8(""));
        bt32->setIconSize(QSize(100, 100));

        gridLayout->addWidget(bt32, 2, 1, 1, 1);

        bt33 = new QPushButton(groupBox);
        bt33->setObjectName(QString::fromUtf8("bt33"));
        bt33->setMinimumSize(QSize(120, 90));
        bt33->setMaximumSize(QSize(120, 90));
        bt33->setSizeIncrement(QSize(1, 1));
        bt33->setBaseSize(QSize(50, 50));
        bt33->setText(QString::fromUtf8(""));
        bt33->setIconSize(QSize(100, 100));

        gridLayout->addWidget(bt33, 2, 2, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        btNext = new QPushButton(groupBox);
        btNext->setObjectName(QString::fromUtf8("btNext"));
        btNext->setMinimumSize(QSize(90, 70));
        btNext->setMaximumSize(QSize(90, 70));
        btNext->setText(QString::fromUtf8("Next"));

        horizontalLayout->addWidget(btNext);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btOk = new QPushButton(groupBox);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("Ok"));

        horizontalLayout->addWidget(btOk);

        btCancel = new QPushButton(groupBox);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("Cancel"));

        horizontalLayout->addWidget(btCancel);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_2->addWidget(groupBox);


        retranslateUi(selectionFrm);

        QMetaObject::connectSlotsByName(selectionFrm);
    } // setupUi

    void retranslateUi(QWidget *selectionFrm)
    {
        groupBox->setTitle(QString());
        Q_UNUSED(selectionFrm);
    } // retranslateUi

};

namespace Ui {
    class selectionFrm: public Ui_selectionFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_SELECTION_H

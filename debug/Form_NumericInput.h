#ifndef NUMERICINPUTFRM_H
#define NUMERICINPUTFRM_H

#include <QtGui/QWidget>
#include "Form_QbaseWidget.h"

namespace Ui
{
  class NumericInputFrm;
}


class NumericInputFrm : public QBaseWidget
{
  Q_OBJECT
    public:
    NumericInputFrm(QWidget *parent = 0);
    ~NumericInputFrm();
    void SetDisplay(bool isFloat, int decimals,float value, float minValue, float maxValue, int callerID = 0);
    void SetMinIncVal(float val);
    void FormInit();

  protected:
    void changeEvent(QEvent *e);
    void showEvent(QShowEvent *);
    void DisplayValue();
    void DisplayValue2(bool);

  private:
    Ui::NumericInputFrm *m_ui;
    bool isFloat;
    bool hasDot;
    bool hasDot2;
    bool firstInput;
    int decimals;
    float value;
    float minIncValue;
    float incValue;
    float minValue,maxValue;
    int curPos;
    void ShiftCursor(bool);
    void posCursor();
    float RangeCheck(float);
    int calID;
    void SetInputType(int);

  private slots:
    // Page 1 slots (Arrow control)
    void Increment();
    void Decrement();
    void ClearDisplay();
    void ShiftLeft();
    void ShiftRight();
    void ValueReceived(float);
    // Page 2 slots (Keyboard control)
    void ClearClicked();
    void ButtonClicked();
    void DotClicked();
    void BackspaceClicked();

    void CancelClicked();
    void OkClicked();
    void SwitchClicked();

    signals:
    void InputValue(float);
    void InputValue(float,int);
};
#endif                                                                          // NUMERICINPUTFRM_H

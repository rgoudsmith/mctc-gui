/********************************************************************************
** Form generated from reading UI file 'Form_WeightCheck.ui'
**
** Created: Thu Jan 31 11:18:00 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_WEIGHTCHECK_H
#define UI_FORM_WEIGHTCHECK_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WeightCheckFrm
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *lbCaption;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_4;
    QLabel *lbIDCap;
    QSpacerItem *horizontalSpacer_5;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_8;
    QLabel *lbIDVal;
    QSpacerItem *horizontalSpacer_9;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QLabel *lbValCap;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_6;
    QLabel *lbValVal;
    QSpacerItem *horizontalSpacer_7;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btOk;

    void setupUi(QWidget *WeightCheckFrm)
    {
        if (WeightCheckFrm->objectName().isEmpty())
            WeightCheckFrm->setObjectName(QString::fromUtf8("WeightCheckFrm"));
        WeightCheckFrm->resize(576, 451);
        WeightCheckFrm->setCursor(QCursor(Qt::BlankCursor));
        WeightCheckFrm->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout = new QVBoxLayout(WeightCheckFrm);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lbCaption = new QLabel(WeightCheckFrm);
        lbCaption->setObjectName(QString::fromUtf8("lbCaption"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lbCaption->sizePolicy().hasHeightForWidth());
        lbCaption->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(35);
        font.setBold(true);
        font.setWeight(75);
        lbCaption->setFont(font);
        lbCaption->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lbCaption);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);

        lbIDCap = new QLabel(WeightCheckFrm);
        lbIDCap->setObjectName(QString::fromUtf8("lbIDCap"));

        horizontalLayout_3->addWidget(lbIDCap);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_8);

        lbIDVal = new QLabel(WeightCheckFrm);
        lbIDVal->setObjectName(QString::fromUtf8("lbIDVal"));
        lbIDVal->setText(QString::fromUtf8("lbIDVal"));

        horizontalLayout_5->addWidget(lbIDVal);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_9);


        verticalLayout->addLayout(horizontalLayout_5);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        lbValCap = new QLabel(WeightCheckFrm);
        lbValCap->setObjectName(QString::fromUtf8("lbValCap"));

        horizontalLayout_2->addWidget(lbValCap);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_6);

        lbValVal = new QLabel(WeightCheckFrm);
        lbValVal->setObjectName(QString::fromUtf8("lbValVal"));
        lbValVal->setMinimumSize(QSize(110, 35));
        lbValVal->setMaximumSize(QSize(110, 35));
        lbValVal->setStyleSheet(QString::fromUtf8("#lbValVal{border: 1px solid black; border-radius: 3px;}"));
        lbValVal->setText(QString::fromUtf8("TextLabel"));
        lbValVal->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(lbValVal);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_7);


        verticalLayout->addLayout(horizontalLayout_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btOk = new QPushButton(WeightCheckFrm);
        btOk->setObjectName(QString::fromUtf8("btOk"));
        btOk->setMinimumSize(QSize(90, 70));
        btOk->setMaximumSize(QSize(90, 70));
        btOk->setText(QString::fromUtf8("Ok"));

        horizontalLayout->addWidget(btOk);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(WeightCheckFrm);

        QMetaObject::connectSlotsByName(WeightCheckFrm);
    } // setupUi

    void retranslateUi(QWidget *WeightCheckFrm)
    {
        lbCaption->setText(QApplication::translate("WeightCheckFrm", "Weight Check", 0, QApplication::UnicodeUTF8));
        lbIDCap->setText(QApplication::translate("WeightCheckFrm", "Device ID:", 0, QApplication::UnicodeUTF8));
        lbValCap->setText(QApplication::translate("WeightCheckFrm", "Actual weight: ", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(WeightCheckFrm);
    } // retranslateUi

};

namespace Ui {
    class WeightCheckFrm: public Ui_WeightCheckFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_WEIGHTCHECK_H

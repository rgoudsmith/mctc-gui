#ifndef POPUPSETTINGCONFIRMFRM_H
#define POPUPSETTINGCONFIRMFRM_H

#include "Form_QbaseWidget.h"

namespace Ui
{
  class PopupSettingConfirmFrm;
}


class PopupSettingConfirmFrm : public QBaseWidget
{
  Q_OBJECT

    public:
    explicit PopupSettingConfirmFrm(QWidget *parent = 0);
    ~PopupSettingConfirmFrm();
    void FormInit();
    int LastSelection();

  private:
    Ui::PopupSettingConfirmFrm *ui;
    void showEvent(QShowEvent *);
    void changeEvent(QEvent *);
    int lastSel;
    int unitIdx;

    void LanguageUpdate();

  private slots:
    void OkClicked();
    void CancelClicked();
    void CloseForm(int);

  public slots:
    void SetMessageText(const QString &);
    void SetItemImage(int,int);
    void SetUnitIndex(int);
    void ShowConfigMessage(const QString &, int, int , int);

    signals:
    void MessageResult(int);
};
#endif                                                                          // POPUPSETTINGCONFIRMFRM_H

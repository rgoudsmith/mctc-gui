/********************************************************************************
** Form generated from reading UI file 'Form_Prime.ui'
**
** Created: Wed Feb 6 11:12:25 2013
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_PRIME_H
#define UI_FORM_PRIME_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PrimeFrm
{
public:
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_10;
    QVBoxLayout *PrevUnitLayout;
    QPushButton *btLeft;
    QSpacerItem *verticalSpacer_8;
    QGroupBox *frame;
    QVBoxLayout *verticalLayout_7;
    QSpacerItem *verticalSpacer_10;
    QLabel *lbUnitName1;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_11;
    QLabel *Progress1Spacer;
    QProgressBar *pbTimer;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_7;
    QLineEdit *edTime;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_8;
    QLineEdit *edRpm;
    QSpacerItem *horizontalSpacer_6;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_4;
    QLabel *lbPrime;
    QPushButton *btPrime;
    QSpacerItem *verticalSpacer_6;
    QFrame *line;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_9;
    QSpacerItem *verticalSpacer_13;
    QVBoxLayout *verticalLayout_8;
    QLabel *lbTestTimeVal1;
    QLabel *lbTestRpmVal1;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *btTest1;
    QGroupBox *frame_2;
    QVBoxLayout *verticalLayout_4;
    QSpacerItem *verticalSpacer_11;
    QLabel *lbUnitName2;
    QSpacerItem *verticalSpacer_7;
    QHBoxLayout *horizontalLayout_12;
    QLabel *Progress2Spacer;
    QProgressBar *pbTimer_2;
    QHBoxLayout *horizontalLayout_7;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_13;
    QLineEdit *edTime_2;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_12;
    QLineEdit *edRpm_2;
    QSpacerItem *horizontalSpacer_3;
    QVBoxLayout *verticalLayout_5;
    QSpacerItem *verticalSpacer_5;
    QLabel *lbPrime2;
    QPushButton *btPrime_2;
    QSpacerItem *verticalSpacer_3;
    QFrame *line_2;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_10;
    QSpacerItem *verticalSpacer_12;
    QVBoxLayout *verticalLayout_10;
    QLabel *lbTestTimeVal2;
    QLabel *lbTestRpmVal2;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *btTest2;
    QVBoxLayout *NextUnitLayout;
    QPushButton *btRight;
    QSpacerItem *verticalSpacer_9;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_11;
    QPushButton *btCancel;

    void setupUi(QWidget *PrimeFrm)
    {
        if (PrimeFrm->objectName().isEmpty())
            PrimeFrm->setObjectName(QString::fromUtf8("PrimeFrm"));
        PrimeFrm->setWindowModality(Qt::ApplicationModal);
        PrimeFrm->resize(897, 507);
        PrimeFrm->setCursor(QCursor(Qt::BlankCursor));
        PrimeFrm->setContextMenuPolicy(Qt::DefaultContextMenu);
        PrimeFrm->setWindowTitle(QString::fromUtf8("Prime"));
        PrimeFrm->setAutoFillBackground(false);
        verticalLayout_6 = new QVBoxLayout(PrimeFrm);
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        PrevUnitLayout = new QVBoxLayout();
        PrevUnitLayout->setObjectName(QString::fromUtf8("PrevUnitLayout"));
        btLeft = new QPushButton(PrimeFrm);
        btLeft->setObjectName(QString::fromUtf8("btLeft"));
        btLeft->setMinimumSize(QSize(70, 70));
        btLeft->setMaximumSize(QSize(70, 70));
        btLeft->setText(QString::fromUtf8("<-"));

        PrevUnitLayout->addWidget(btLeft);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        PrevUnitLayout->addItem(verticalSpacer_8);


        horizontalLayout_10->addLayout(PrevUnitLayout);

        frame = new QGroupBox(PrimeFrm);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMaximumSize(QSize(16777215, 415));
        verticalLayout_7 = new QVBoxLayout(frame);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalSpacer_10 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_10);

        lbUnitName1 = new QLabel(frame);
        lbUnitName1->setObjectName(QString::fromUtf8("lbUnitName1"));
        lbUnitName1->setText(QString::fromUtf8("TextLabel"));
        lbUnitName1->setAlignment(Qt::AlignCenter);

        verticalLayout_7->addWidget(lbUnitName1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(0);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        Progress1Spacer = new QLabel(frame);
        Progress1Spacer->setObjectName(QString::fromUtf8("Progress1Spacer"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Progress1Spacer->sizePolicy().hasHeightForWidth());
        Progress1Spacer->setSizePolicy(sizePolicy);
        Progress1Spacer->setMinimumSize(QSize(0, 40));
        Progress1Spacer->setMaximumSize(QSize(0, 40));
        Progress1Spacer->setText(QString::fromUtf8(""));

        horizontalLayout_11->addWidget(Progress1Spacer);

        pbTimer = new QProgressBar(frame);
        pbTimer->setObjectName(QString::fromUtf8("pbTimer"));
        pbTimer->setMinimumSize(QSize(0, 40));
        pbTimer->setMaximumSize(QSize(16777215, 40));
        pbTimer->setMaximum(30);
        pbTimer->setValue(20);
        pbTimer->setFormat(QString::fromUtf8("%v s"));

        horizontalLayout_11->addWidget(pbTimer);


        verticalLayout_7->addLayout(horizontalLayout_11);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(0);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(9);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_7);

        edTime = new QLineEdit(frame);
        edTime->setObjectName(QString::fromUtf8("edTime"));
        edTime->setMinimumSize(QSize(130, 70));
        edTime->setMaximumSize(QSize(130, 70));
        edTime->setCursor(QCursor(Qt::BlankCursor));
        edTime->setText(QString::fromUtf8(""));
        edTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(edTime);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_8);

        edRpm = new QLineEdit(frame);
        edRpm->setObjectName(QString::fromUtf8("edRpm"));
        edRpm->setMinimumSize(QSize(130, 70));
        edRpm->setMaximumSize(QSize(130, 70));
        edRpm->setCursor(QCursor(Qt::BlankCursor));
        edRpm->setText(QString::fromUtf8(""));
        edRpm->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(edRpm);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);


        verticalLayout_3->addLayout(horizontalLayout_2);


        horizontalLayout_6->addLayout(verticalLayout_3);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_4);

        lbPrime = new QLabel(frame);
        lbPrime->setObjectName(QString::fromUtf8("lbPrime"));
        lbPrime->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lbPrime);

        btPrime = new QPushButton(frame);
        btPrime->setObjectName(QString::fromUtf8("btPrime"));
        btPrime->setMinimumSize(QSize(90, 70));
        btPrime->setMaximumSize(QSize(90, 70));
        btPrime->setText(QString::fromUtf8("PRIME"));

        verticalLayout->addWidget(btPrime);


        horizontalLayout_6->addLayout(verticalLayout);


        verticalLayout_7->addLayout(horizontalLayout_6);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_6);

        line = new QFrame(frame);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_7->addWidget(line);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(0);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_9);

        verticalSpacer_13 = new QSpacerItem(20, 70, QSizePolicy::Minimum, QSizePolicy::Fixed);

        horizontalLayout_8->addItem(verticalSpacer_13);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        lbTestTimeVal1 = new QLabel(frame);
        lbTestTimeVal1->setObjectName(QString::fromUtf8("lbTestTimeVal1"));
        lbTestTimeVal1->setMinimumSize(QSize(130, 0));
        lbTestTimeVal1->setMaximumSize(QSize(130, 16777215));
        lbTestTimeVal1->setText(QString::fromUtf8("TimeVal1"));
        lbTestTimeVal1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_8->addWidget(lbTestTimeVal1);

        lbTestRpmVal1 = new QLabel(frame);
        lbTestRpmVal1->setObjectName(QString::fromUtf8("lbTestRpmVal1"));
        lbTestRpmVal1->setMinimumSize(QSize(130, 0));
        lbTestRpmVal1->setMaximumSize(QSize(130, 16777215));
        lbTestRpmVal1->setText(QString::fromUtf8("RpmVal2"));
        lbTestRpmVal1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_8->addWidget(lbTestRpmVal1);


        horizontalLayout_8->addLayout(verticalLayout_8);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_4);

        btTest1 = new QPushButton(frame);
        btTest1->setObjectName(QString::fromUtf8("btTest1"));
        btTest1->setMinimumSize(QSize(90, 70));
        btTest1->setMaximumSize(QSize(90, 70));
        btTest1->setText(QString::fromUtf8("TEST"));

        horizontalLayout_8->addWidget(btTest1);


        verticalLayout_7->addLayout(horizontalLayout_8);


        horizontalLayout_10->addWidget(frame);

        frame_2 = new QGroupBox(PrimeFrm);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setMaximumSize(QSize(16777215, 415));
        verticalLayout_4 = new QVBoxLayout(frame_2);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalSpacer_11 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_11);

        lbUnitName2 = new QLabel(frame_2);
        lbUnitName2->setObjectName(QString::fromUtf8("lbUnitName2"));
        lbUnitName2->setText(QString::fromUtf8("TextLabel"));
        lbUnitName2->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(lbUnitName2);

        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_7);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(0);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        Progress2Spacer = new QLabel(frame_2);
        Progress2Spacer->setObjectName(QString::fromUtf8("Progress2Spacer"));
        sizePolicy.setHeightForWidth(Progress2Spacer->sizePolicy().hasHeightForWidth());
        Progress2Spacer->setSizePolicy(sizePolicy);
        Progress2Spacer->setMinimumSize(QSize(0, 40));
        Progress2Spacer->setMaximumSize(QSize(0, 40));
        Progress2Spacer->setText(QString::fromUtf8(""));

        horizontalLayout_12->addWidget(Progress2Spacer);

        pbTimer_2 = new QProgressBar(frame_2);
        pbTimer_2->setObjectName(QString::fromUtf8("pbTimer_2"));
        pbTimer_2->setMinimumSize(QSize(0, 40));
        pbTimer_2->setMaximumSize(QSize(16777215, 40));
        pbTimer_2->setMaximum(30);
        pbTimer_2->setValue(20);
        pbTimer_2->setFormat(QString::fromUtf8("%v s"));

        horizontalLayout_12->addWidget(pbTimer_2);


        verticalLayout_4->addLayout(horizontalLayout_12);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(0);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(9);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_13);

        edTime_2 = new QLineEdit(frame_2);
        edTime_2->setObjectName(QString::fromUtf8("edTime_2"));
        edTime_2->setMinimumSize(QSize(130, 70));
        edTime_2->setMaximumSize(QSize(130, 70));
        edTime_2->setCursor(QCursor(Qt::BlankCursor));
        edTime_2->setText(QString::fromUtf8(""));
        edTime_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(edTime_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(0);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_12);

        edRpm_2 = new QLineEdit(frame_2);
        edRpm_2->setObjectName(QString::fromUtf8("edRpm_2"));
        edRpm_2->setMinimumSize(QSize(130, 70));
        edRpm_2->setMaximumSize(QSize(130, 70));
        edRpm_2->setCursor(QCursor(Qt::BlankCursor));
        edRpm_2->setText(QString::fromUtf8(""));
        edRpm_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(edRpm_2);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        verticalLayout_2->addLayout(horizontalLayout_4);


        horizontalLayout_7->addLayout(verticalLayout_2);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_5);

        lbPrime2 = new QLabel(frame_2);
        lbPrime2->setObjectName(QString::fromUtf8("lbPrime2"));
        lbPrime2->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(lbPrime2);

        btPrime_2 = new QPushButton(frame_2);
        btPrime_2->setObjectName(QString::fromUtf8("btPrime_2"));
        btPrime_2->setMinimumSize(QSize(90, 70));
        btPrime_2->setMaximumSize(QSize(90, 70));
        btPrime_2->setText(QString::fromUtf8("PRIME"));

        verticalLayout_5->addWidget(btPrime_2);


        horizontalLayout_7->addLayout(verticalLayout_5);


        verticalLayout_4->addLayout(horizontalLayout_7);

        verticalSpacer_3 = new QSpacerItem(20, 41, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_3);

        line_2 = new QFrame(frame_2);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout_4->addWidget(line_2);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(0);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_10);

        verticalSpacer_12 = new QSpacerItem(20, 70, QSizePolicy::Minimum, QSizePolicy::Fixed);

        horizontalLayout_9->addItem(verticalSpacer_12);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        lbTestTimeVal2 = new QLabel(frame_2);
        lbTestTimeVal2->setObjectName(QString::fromUtf8("lbTestTimeVal2"));
        lbTestTimeVal2->setMinimumSize(QSize(130, 0));
        lbTestTimeVal2->setMaximumSize(QSize(130, 16777215));
        lbTestTimeVal2->setText(QString::fromUtf8("TimeVal2"));
        lbTestTimeVal2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_10->addWidget(lbTestTimeVal2);

        lbTestRpmVal2 = new QLabel(frame_2);
        lbTestRpmVal2->setObjectName(QString::fromUtf8("lbTestRpmVal2"));
        lbTestRpmVal2->setMinimumSize(QSize(130, 0));
        lbTestRpmVal2->setMaximumSize(QSize(130, 16777215));
        lbTestRpmVal2->setText(QString::fromUtf8("RpmVal2"));
        lbTestRpmVal2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_10->addWidget(lbTestRpmVal2);


        horizontalLayout_9->addLayout(verticalLayout_10);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_5);

        btTest2 = new QPushButton(frame_2);
        btTest2->setObjectName(QString::fromUtf8("btTest2"));
        btTest2->setMinimumSize(QSize(90, 70));
        btTest2->setMaximumSize(QSize(90, 70));
        btTest2->setText(QString::fromUtf8("TEST"));

        horizontalLayout_9->addWidget(btTest2);


        verticalLayout_4->addLayout(horizontalLayout_9);


        horizontalLayout_10->addWidget(frame_2);

        NextUnitLayout = new QVBoxLayout();
        NextUnitLayout->setObjectName(QString::fromUtf8("NextUnitLayout"));
        btRight = new QPushButton(PrimeFrm);
        btRight->setObjectName(QString::fromUtf8("btRight"));
        btRight->setMinimumSize(QSize(70, 70));
        btRight->setMaximumSize(QSize(70, 70));
        btRight->setText(QString::fromUtf8("->"));

        NextUnitLayout->addWidget(btRight);

        verticalSpacer_9 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        NextUnitLayout->addItem(verticalSpacer_9);


        horizontalLayout_10->addLayout(NextUnitLayout);


        verticalLayout_6->addLayout(horizontalLayout_10);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_2);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_11);

        btCancel = new QPushButton(PrimeFrm);
        btCancel->setObjectName(QString::fromUtf8("btCancel"));
        btCancel->setMinimumSize(QSize(90, 70));
        btCancel->setMaximumSize(QSize(90, 70));
        btCancel->setText(QString::fromUtf8("CANCEL"));

        horizontalLayout_5->addWidget(btCancel);


        verticalLayout_6->addLayout(horizontalLayout_5);


        retranslateUi(PrimeFrm);

        QMetaObject::connectSlotsByName(PrimeFrm);
    } // setupUi

    void retranslateUi(QWidget *PrimeFrm)
    {
        frame->setTitle(QString());
        lbPrime->setText(QApplication::translate("PrimeFrm", "PRIME", 0, QApplication::UnicodeUTF8));
        frame_2->setTitle(QString());
        lbPrime2->setText(QApplication::translate("PrimeFrm", "PRIME", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(PrimeFrm);
    } // retranslateUi

};

namespace Ui {
    class PrimeFrm: public Ui_PrimeFrm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_PRIME_H

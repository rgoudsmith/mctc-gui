#include "Form_MaterialSelect.h"
#include "ui_Form_MaterialSelect.h"
#include "Form_MainMenu.h"
#include <QDir>
#include "Rout.h"

//QString matSearchString;                          /* Search key for material file list */

#define MAT_DISPLAY_BOX_ITEMS 5


MaterialSelectFrm::MaterialSelectFrm(QWidget *parent) : QBaseWidget(parent), ui(new Ui::MaterialSelectFrm)
{
  /*-------------*/
  /* Constructor */
  /*-------------*/
  msgIdx = -1;
  currentIndex = -1;
  currentItem = "";
  currentDefault = false;
  dosToolChange = -1;
  calWindow = wiNone;
  ui->setupUi(this);

  keyInput = new KeyboardFrm();
  keyInput->hide();

  ext = QString(MATERIAL_EXTENSION);

  /*----- New material not available -----*/
  ui->btNew->setVisible(false);

  connect(ui->btUp,SIGNAL(clicked()),this,SLOT(ScrollUp()));
  connect(ui->btDown,SIGNAL(clicked()),this,SLOT(ScrollDown()));

  connect(ui->btDefault,SIGNAL(clicked()),this,SLOT(DefaultMatsClicked()));
  connect(ui->btDelete,SIGNAL(clicked()),this,SLOT(DeleteClicked()));
  connect(ui->btDeleteAll,SIGNAL(clicked()),this,SLOT(DeleteAllClicked()));
  connect(ui->btCancel,SIGNAL(clicked()),this,SLOT(CancelClicked()));
  connect(ui->btRename,SIGNAL(clicked()),this,SLOT(RenameClicked()));
  connect(ui->btOk,SIGNAL(clicked()),this,SLOT(OkClicked()));
  connect(ui->btSearch,SIGNAL(clicked()),this,SLOT(SearchClicked()));

  searchString = "";
}


MaterialSelectFrm::~MaterialSelectFrm()
{
  matList.clear();
  delete keyInput;
  delete ui;
}


void MaterialSelectFrm::showEvent(QShowEvent *e)
{
  QBaseWidget::showEvent(e);

  searchString = "";                             /* Clear search string */
  LoadMaterials();

  /*----- Display 'Delete all' button only from superVisor user level -----*/
  if (glob->GetUser() >= utSupervisor) {
    ui->btDeleteAll->setVisible(TRUE);
  }
  else {
    ui->btDeleteAll->setVisible(FALSE);
  }

  keyInput->move(((width()/2)-(keyInput->width()/2)),((height()/2)-(keyInput->height()/2)));

  if (currentItem.isEmpty()) {
    UpdateCurrent(0,true);
    currentDefault = false;
  }
  else {
    UpdateCurrent(currentItem,true);
    currentDefault = false;
  }
  UpdateDisplay();
}


void MaterialSelectFrm::changeEvent(QEvent *e)
{
  QBaseWidget::changeEvent(e);

  if (e->type() == QEvent::LanguageChange) {
    ui->retranslateUi(this);
  }
}


void MaterialSelectFrm::FormInit()
{
  QBaseWidget::ConnectKeyBeep();;
  keyInput->FormInit();
  keyInput->SetMaxLength(MAX_LENGTH_FILENAME_INPUT);

  glob->SetButtonImage(ui->btOk,itSelection,selOk);
  glob->SetButtonImage(ui->btCancel,itSelection,selCancel);
  glob->SetButtonImage(ui->btSearch,itFileHandle,fhSearch);
  glob->SetButtonImage(ui->btNew,itFileHandle,fhNew);
  glob->SetButtonImage(ui->btDelete,itFileHandle,fhDelete);
  glob->SetButtonImage(ui->btDeleteAll,itFileHandle,fhDeleteAll);
  glob->SetButtonImage(ui->btRename,itFileHandle,fhRename);
  glob->SetButtonImage(ui->btDown,itGeneral,genArrowKeyDown);
  glob->SetButtonImage(ui->btUp,itGeneral,genArrowKeyUp);
  glob->SetButtonImage(ui->btSearch,itFileHandle,fhSearch);

  ui->lbCaption->setFont(*(glob->captionFont));
  ui->boxSelect->setFont(*(glob->baseFont));
  ui->lbIndex->setFont(*(glob->baseFont));

  connect(glob,SIGNAL(MstSts_ActUnitChanged(int)),this,SLOT(ActiveIndexChanged(int)));
  connect(glob,SIGNAL(MstCfg_UserChanged(eUserType)),this,SLOT(CfgCurrentUserChanged(eUserType)));
  connect(keyInput,SIGNAL(inputText(QString)),this,SLOT(TextReceived(QString)));
  connect(glob,SIGNAL(MstSts_SysActiveChanged(bool)),this,SLOT(SysActiveChanged(bool)));

  LoadMaterials();
  UpdateCurrent(0);
}


void MaterialSelectFrm::SetCallerWindow(eWindowIdx window)
{
  calWindow = window;
}


void MaterialSelectFrm::LoadMaterials()
{
  QString fileFilter;
  QString * path = new QString("");
  QDir matDir;

  /*----- Build file filter -----*/
  if (searchString.isEmpty()) {
    fileFilter = "*" + ext;                                   /* No search string */
  }
  else {
    fileFilter = "*" + searchString + "*" + ext;           /* Add search string */
  }

  matList.clear();
  GetMaterialFolder(path);
  if (matDir.exists(*path)) {
    matDir.setPath(*path);
    matList = matDir.entryList(QStringList(fileFilter),QDir::Files | QDir::NoSymLinks);
  }
  delete path;
  /*----- Sort the list on alphabetical order -----*/
  QMap<QString,QString> strMap;

  /*----- Add all items in the QMap. The first String is the index, so we add the string in lowercase (str.toLower()). -----*/
  /*----- the 2nd String is the value, which will be sorted case-insensitively now due to all the indexes being lowercase. -----*/
  foreach (QString str, matList) {
    strMap.insert(str.toLower(),str);
  }
  /*----- Last we load the sorted value Strings into the matList. -----*/
  matList = strMap.values();
}


int MaterialSelectFrm::ItemCount()
{
  return matList.count();
}


void MaterialSelectFrm::GetMaterialFolder(QString * path)
{
  *path = *(glob->AppPath()) + QString(MATERIAL_FOLDER);
}


void MaterialSelectFrm::GetDefaultMaterialFolder(QString * path)
{
  GetMaterialFolder(path);
  *path = *path + QString(MATERIAL_DEFAULT_FOLDER);
}


void MaterialSelectFrm::GetDefaultMaterial(QString * matName)
{
  *matName="";
  glob->GetDefaultMaterial(matName,glob->ActUnitIndex());
}


void MaterialSelectFrm::GetDefaultMaterialText(QString * matName)
{
  /*----- Initialize default material name. -----*/
  *matName="";

  glob->GetDefaultMaterialName(matName,glob->ActUnitIndex());
}


void MaterialSelectFrm::UpdateCurrent(const QString &material,bool forceUpdate)
{
  if ((material != currentItem) || (forceUpdate)) {
    currentItem = material;
    currentIndex = matList.indexOf(currentItem);
    UpdateDisplay();
  }
}


void MaterialSelectFrm::UpdateCurrent(int idx, bool forceUpdate)
{
  if ((currentIndex != idx) || (forceUpdate)) {
    if ((idx < ItemCount()) && (idx >= 0)) {
      currentIndex = idx;
      currentItem = (QString)(matList.at(currentIndex));
    }
    else {
      currentIndex = -1;
      currentItem = QString("");
    }
    UpdateDisplay();
  }
}


void MaterialSelectFrm::ClipMaterialExtension(QString * matName)
{
  QString mat = *matName;
  mat = mat.left(mat.length()-ext.length());
  *matName = mat;
}


void MaterialSelectFrm::DisplaySelectedItem()
{
  QString item;

  if ((!currentItem.isEmpty()) && (!currentItem.isNull())) {
    item = currentItem;
    ClipMaterialExtension(&item);
    ui->lbIndex->setText(QString::number(currentIndex+1)+"/"+QString::number(ItemCount()));
  }
  else {
    item = "";
    ui->lbIndex->setText("-/"+QString::number(ItemCount()));
  }
  ui->boxSelect->setText(item);
}


void MaterialSelectFrm::DisplayUpBoxItems()
{
  QString upItems = "";
  QString item;
  int start = currentIndex - MAT_DISPLAY_BOX_ITEMS;
  if (start < 0) start = 0;

  for (int i = start; i<currentIndex;i++) {
    if ((i == start) && (i > 0)) {
      upItems = upItems + "...\n";
    }
    item = QString(matList.at(i));                                              /* Get the item as QString */
    ClipMaterialExtension(&item);
    if (i != (currentIndex -1)) upItems = upItems + item + "\n";                /* Add the item to the list with \n if more items follow */
    else upItems = upItems + item;                                              /* Add the item to the list without \n if it is the last item */
  }
  ui->boxUp->setText(upItems);
}


void MaterialSelectFrm::DisplayDownBoxItems()
{
  QString downItems = "";
  QString item;
  int eind = (currentIndex+1) + MAT_DISPLAY_BOX_ITEMS;
  if (eind > ItemCount()) eind = ItemCount();

  for (int i = (currentIndex+1); i<eind;i++) {
    item = QString(matList.at(i));                                              /* Get the item as QString */
    ClipMaterialExtension(&item);
    if (i != (eind-1)) downItems = downItems + item + "\n";                     /* Add item to the list with \n if more items follow */
    else downItems = downItems + item;                                          /* Add item to the list without \n if this is the last item */

    if (((i == (eind-1)) && (eind != ItemCount()))) downItems = downItems + "\n...";
  }
  ui->boxDown->setText(downItems);
}


void MaterialSelectFrm::DisplayDefaultMaterial()
{
  QString mat;
  GetDefaultMaterialText(&mat);
  mat = QString(tr("Default Material:")+"\n")+mat;
  ui->btDefault->setText(mat);
}


void MaterialSelectFrm::UpdateDisplay()
{
  /*----- Hide material OK button when system is active. -----*/
  DisplayUpBoxItems();
  DisplaySelectedItem();
  DisplayDownBoxItems();
  DisplayDefaultMaterial();
}


void MaterialSelectFrm::CreateNewItem(const QString &, int &)                   /*res*/
{
  //@@ Create new material with default settings. (disabled)
}


void MaterialSelectFrm::RemoveCurrentItem()
{
  /*----- Remove file from filesystem -----*/
  if (!(currentItem.isEmpty())) {
    QString path;
    QString fileName;

    GetMaterialFolder(&path);
    fileName = path + glob->fileSeparator + currentItem;

    if (QFile::exists(fileName)) {
      if (QFile::remove(fileName)) {
        /*----- file removed -----*/
        /*----- Remove item from list -----*/
        matList.removeAt(currentIndex);
        while ((currentIndex >= ItemCount()) && (currentIndex > 0)) {
          currentIndex--;
        }
        UpdateCurrent(currentIndex,true);
        /*----- Refresh display -----*/
        UpdateDisplay();
      }
    }
  }
}


void MaterialSelectFrm::RemoveAllItems()
{
  /*-------------------------------------------*/
  /* Remove all material files from filesystem */
  /*-------------------------------------------*/
  QString path;
  QString fileName;
  int i;

  /*----- Load actual material file list -----*/
  LoadMaterials();

  /*----- Setup material path -----*/
  GetMaterialFolder(&path);

  /*----- Remove material files -----*/
  for (i=0; i<ItemCount(); i++) {
    fileName = path + glob->fileSeparator + (QString)(matList.at(i));
    if (QFile::exists(fileName)) {
      QFile::remove(fileName);
    }
  }

  searchString = "";                         /* Clear search string */

  /*----- Refresh display -----*/
  LoadMaterials();
  UpdateDisplay();
  UpdateCurrent(0,true);
}


void MaterialSelectFrm::RenameCurrentItem(const QString &newName)
{
  if (!(currentItem.isEmpty())) {
    QString path;
    QString fileName;
    QString newFileName;
    QString newFile;

    newFile = newName + ext;
    GetMaterialFolder(&path);
    fileName = path + glob->fileSeparator + currentItem;
    newFileName = path + glob->fileSeparator + newFile;

    if (QFile::exists(fileName)) {
      if (!QFile::exists(newFileName)) {
        if (QFile::rename(fileName,newFileName)) {
          /*----- Search the current file -----*/
          currentItem = newFile;
          matList.replace(currentIndex,newFile);
          UpdateCurrent(currentIndex,true);
          DisplaySelectedItem();
        }
      }
    }
  }
}


void MaterialSelectFrm::ScrollUp()
{
  if (currentIndex > 0) {
    UpdateCurrent((currentIndex-1));
  }
}


void MaterialSelectFrm::ScrollDown()
{
  if (currentIndex < (matList.count()-1)) {
    UpdateCurrent((currentIndex+1));
  }
}


void MaterialSelectFrm::CloseForm()
{
  if (calWindow != wiNone) {
    ((MainMenu*)(glob->menu))->DisplayWindow(calWindow, wiMaterials);
    calWindow = wiNone;
  }
}


void MaterialSelectFrm::CancelClicked()
{
  CloseForm();
}


void MaterialSelectFrm::OkClicked(void)
{
  /*--------------------------------------------------------------*/
  /* BUTTON : OK button clicked, copy selected material in recipe */
  /*--------------------------------------------------------------*/
  int res = 0;
  int idx = glob->ActUnitIndex();
  int dosTool = 0;

  switch (calWindow) {
  case wiHome:
    /*----- Mogelijkheid om geselecteerde materiaal door te geven aan production form -----*/
    /*----- Check material configuration with currently selected dosTool -----*/
    res = glob->Proc_GetMaterialDosTool(currentItem,currentDefault);

    if (res >= 0) {                                                           /* Valid result ? */
      if (res == glob->sysCfg[idx].dosTool) {
        /*----- Force reload of material -----*/
        glob->SysCfg_SetMaterial("",false,idx,false);
        glob->SysCfg_SetMaterial(currentItem,currentDefault,idx);
      }
      else {
        msgIdx = 3;
        connect(((MainMenu*)(glob->menu)),SIGNAL(ErrorResult(int)),this,SLOT(MessageResult(int)));
        dosTool = res;
        ((MainMenu*)(glob->menu))->DisplayMessage(tr("Current cylinder does not match the material.")+"\n"+tr("Change configuration?"),itDosTool,dosTool,idx);
      }
    }
    break;
  case wiRcpConfig:
    /*----- Mogelijkheid om geselecteerde materiaal door te geven aan rcpConfig scherm -----*/
    emit MaterialSelected(currentItem,currentDefault);
    break;

  default:
    break;
  }
  CloseForm();
}

void MaterialSelectFrm::SearchClicked(void)
/*----------------*/
/* Search clicked */
/*----------------*/
{
  msgIdx = 3;

  /*----- Start text input form -----*/
  keyInput->SetText(searchString);
  keyInput->show();
}



void MaterialSelectFrm::ActiveIndexChanged(int)
{
  /*----- Wanneer active unit veranderd moet het actuele materiaal opnieuw geselecteerd worden -----*/
  UpdateDisplay();
}


void MaterialSelectFrm::SetSelectedMaterial(const QString &mat)
{
  int idx = matList.indexOf(mat);
  if (idx >= 0) {
    currentIndex = idx;
    currentItem = mat;
  }
  else {
    currentIndex = -1;
    currentItem = "";
  }
  currentDefault = false;
}


void MaterialSelectFrm::SetCorrectButtons()
{
  eUserType ut = glob->GetUser();
  bool act = glob->mstSts.sysActive;

  if (ut < utTooling) {
    ui->btOk->setVisible(false);
    ui->btNew->setVisible(false);
    ui->btDelete->setVisible(false);
    ui->btRename->setVisible(false);
    ui->btDefault->setVisible(false);
  }
  else {
    ui->btOk->setVisible((act == false ? true : false));
    ui->btNew->setVisible(false);
    ui->btDelete->setVisible((act == false ? true : false));
    ui->btRename->setVisible((act == false ? true : false));
    ui->btDefault->setVisible((act == false ? true : false));
  }
}


void MaterialSelectFrm::CfgCurrentUserChanged(eUserType )                       /* ut */
{
  /* btNew is totally disabled */
  SetCorrectButtons();
}


void MaterialSelectFrm::DefaultMatsClicked()
{
  /*===========================*/
  /* Default material selected */
  /*===========================*/
  QString dir;
  QStringList list;
  QString mat;

  /*----- Get default material according to configuration -----*/
  GetDefaultMaterial(&mat);
  mat += ext;
  /*----- And load it into the system. -----*/
  GetDefaultMaterialFolder(&dir);

  QDir myDir(dir);
  if (myDir.exists()) {
    list = myDir.entryList();

    if (list.count() > 0) {
      int idx = list.indexOf(mat);
      if (idx != -1) {
        currentItem = mat;
        currentDefault = true;
        OkClicked();
      }
      else {
        QString txt;
        ((MainMenu*)(glob->menu))->alarmHndlr->GetMessageText(TXT_INDEX_DEF_MAT_FILE_NOT_FOUND,0,0,&txt);
        ((MainMenu*)(glob->menu))->DisplayMessage(glob->ActUnitIndex(),0,txt,msgtAccept);
      }
    }
  }
}


void MaterialSelectFrm::NewClicked()
{
}


void MaterialSelectFrm::DeleteClicked()
{
  /*----- Remove file from filesystem -----*/
  msgIdx = 1;
  connect(((MainMenu*)(glob->menu)),SIGNAL(ErrorResult(int)),this,SLOT(MessageResult(int)));
  QString item = currentItem;
  ClipMaterialExtension(&item);
  ((MainMenu*)(glob->menu))->DisplayMessage(glob->ActUnitIndex(),0,tr("Material '")+item+tr("' will be erased.")+"\n"+tr("Do you want to continue?"),msgtConfirm);
}


void MaterialSelectFrm::DeleteAllClicked()
{
  /*----- Remove all files from filesystem -----*/
  msgIdx = 4;
  connect(((MainMenu*)(glob->menu)),SIGNAL(ErrorResult(int)),this,SLOT(MessageResult(int)));
  QString item = currentItem;
  ClipMaterialExtension(&item);
  ((MainMenu*)(glob->menu))->DisplayMessage(glob->ActUnitIndex(),0,tr("All materials as shown in the list will be erased!")+"\n"+tr("Do you want to continue?"),msgtConfirm);
}


void MaterialSelectFrm::RenameClicked()
{
  msgIdx = 2;
  /*----- Display textInput form. -----*/
  QString item = currentItem;
  /*----- Remove extension -----*/
  ClipMaterialExtension(&item);
  /*----- Shot text input form -----*/
  keyInput->SetText(item);
  keyInput->show();
}


void MaterialSelectFrm::MessageResult(int res)
{
  disconnect(((MainMenu*)(glob->menu)),SIGNAL(ErrorResult(int)),this,SLOT(MessageResult(int)));
  if (res == 1) {                                                               /* ok */
    switch(msgIdx) {
    case 1:
      RemoveCurrentItem();
      break;
      /*----- User agreed to change configuration -----*/
    case 3:
      tCurveStruct curve;
      /*----- Load curve to fetch dosTool -----*/
      glob->Proc_LoadMaterial(currentItem,currentDefault,&curve);
      /*----- Overwrite current configuration -----*/
      glob->SysCfg_SetDosTool(((eDosTool)curve.cylSel),glob->ActUnitIndex());
      /*---- Load material -----*/
      glob->SysCfg_SetMaterial(currentItem,currentDefault,glob->ActUnitIndex());
      ((MainMenu*)(glob->menu))->DisplayMessage(glob->ActUnitIndex(),0,tr("Configuration changed"),msgtAccept);
      break;
    case 4:
      RemoveAllItems();
      break;


    default:
      break;
    }
  }
  msgIdx = -1;
}


void MaterialSelectFrm::TextReceived(const QString &txt)
/*------------------------------*/
/* Ascii keyboard text received */
/*------------------------------*/
{
  switch (msgIdx) {
  /*----- RenameCurrentItem -----*/
  case 2:
    /*----- RenameCurrentItem adds extension. We don't have to do that here. -----*/
    RenameCurrentItem(txt);
    break;

    /*----- Search -----*/
  case 3:
    searchString = txt;

    LoadMaterials();
    UpdateDisplay();
    UpdateCurrent(0,true);


  default:
    break;
  }
  msgIdx = -1;
}


void MaterialSelectFrm::SysActiveChanged(bool  )                                /* act */
{
  SetCorrectButtons();
}
